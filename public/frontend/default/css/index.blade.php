<!DOCTYPE>
<html lang="en">
<head >
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
   
    <title>{{ CNF_APPNAME }}  {{ $pageTitle}}</title>
    <meta name="keywords" content="{{ $pageMetakey }}" />
    <meta name="description" content="{{ $pageMetadesc }}" />
    <meta name="Author" content="Mangopik [www.mangopik.com]" />

      <link href="{{ asset('frontend') }}/default/css/owl.theme.default.min.css" rel="stylesheet">
      <link href="{{ asset('frontend') }}/default/css/style.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/animate.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/lightslider.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/agency.css" rel="stylesheet">

         <!-- video upload Styles -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/reset.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('magelur/css/video/style.css')}}"> -->
    <link rel="stylesheet" href="{{ url('/') }}/content/assets/css/watch.css">
    <link rel="stylesheet" type="text/css"  href="{{ asset('magelur/css/video/forms.css')}}">

    <link rel="stylesheet" type="text/css"  href="{{ asset('magelur/css/video/black.css')}}">

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <link rel="stylesheet" href="{{ asset('magelur/css/video/tooltip.css')}}">

    <link rel="stylesheet" href="{{ asset('magelur/css/video/custombox.css')}}">

    <!-- HTML5 Player Style -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/html5player.css')}}" type="text/css" media="screen"/>

    <!-- HTML5 Player Font Awesome -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/player-font-awesome.css')}}" type="text/css" media="screen"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/toastr.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/jquery.fileuploader.min.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/jquery.fileuploader-theme-dragdrop.css')}}" type="text/css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('magelur/js/jquery-ui.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/bootstrap.min.js"></script>


    <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script> -->

    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/lightslider.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/custom.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/modernizr.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/owl.carousel.js"></script>

    <script type="text/javascript" src="{{ asset('magelur/js/slim.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
    <script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>

    <script>
      new WOW().init();
    </script>
      <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBm_C6H7PSs46kG6BxxgR4BzmRU_BJ8Nmo&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script>
<!-- Message -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<link href="{{ asset('frontend') }}/default/css/lightslider.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/lightslider.js"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js')}}"></script>
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/autocomplete.js')}}"></script>

<!-- <script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script> -->
<!-- <script src="{{ asset('magelur/js/valid.js')}}"></script> -->
<script src="{{ asset('magelur/js/jquery_1.11.1.validate.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="{{ asset('magelur/message/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-process.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-validate.js')}}"></script>
<script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
<link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>

</head>
<body data-root="{{ url('/') }}">
  <header>
  <div class="header_block col-md-12 col-sm-12 col-xs-12">
<div class="logo">
            <h1><a href="javascript:void(0)"><img src="{{ asset('images/Asset 2.png') }}"></a></h1>
          </div>  
<div class="menu clearfix wow SlideInDown">
  <ul class="nav navbar-nav magelur_menulist">
            <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/model') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ouragencies') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/agent') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a href="{{ url('events/list') }}">EVENTS</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
           <li class="header_menu_list">
            <a href="{!! url('aboutus') !!}">ABOUT</a>
             <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('contactus') }}">Contactus</a></li>
                <li><a tabindex="-1" href="{{ url('tellfriend') }}">Tell a friend</a></li>
                <li><a tabindex="-1" href="{{ url('toc') }}">Terms & conditions</a></li>
                <li><a tabindex="-1" href="{{ url('privacy') }}">Privay policy</a></li>
                <li><a tabindex="-1" href="{{ url('faqs') }}">FAQs</a></li>
                <li><a tabindex="-1" href="{{ url('feedback') }}">Feedbacks/Suggestions</a></li>
                <li><a tabindex="-1" href="{{ url('advertise') }}">Advertise with us</a></li>
                <li><a tabindex="-1" href="{{ url('blogs/list') }}">Blogs</a></li>
                
              </ul>
            </li>
            
            @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))

              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('model-dashboard/'.Session::get('model_id'))}}" >My Dashboard</a></li>
              <li><a href="{{ URL::to('myprofile/'.Session::get('model_id'))}}"> Model {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('agent-dashboard/'.Session::get('agent_id'))}}" >Dashboard</a></li>
              <li><a href="{{ URL::to('agent-profile/'.Session::get('agent_id'))}}">Agent {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list">
              <a  href="{{ url('login') }}">JOIN US</a>
       <!--        <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a tabindex="-1" href="{{ url('/model/create') }}">Model Register</a></li>
              <li><a tabindex="-1" href="{{ url('/agent/create') }}">Agent Register</a></li>

              </ul> -->
              </li>

            @endif

       
            

  </ul>
  <div class="search"><a href="javascript:void(0)" id="search-toggle"></a></div>
  <div class="search_box">
                    <form action="">
                      <input type="search" placeholder="Search.." name="search">
                    </form>
  </div>
</div>

<!-- Mobile View -->
          <div class="mobile_menu">
              <div id="main" class="main_mobile">
                <ul class="mob-contant">
                  <li>
                  <div class="menu_btn" style="font-size:13px;cursor:pointer" onclick="openNav()">
                  <div class="mener">Menu</div>
                  <div class="rights_bar"><div class="icon-bar"></div>
                  <div class="icon-bar last_led"></div>
                </div>
                </div>
                </li>
                <li>
                  <div class="menu">
                    <div class="search"><a href="javascript:void(0)" id="search-toggle-mobile"></a></div>
                     <div class="search_box">
                          <form action="">
                              <input type="search" placeholder="Search.." name="search">
                          </form>
                    </div>
                </div>
                </li>
              </ul>
            </div>
              
            <div id="mySidenav" class="sidenav">
              <ul class="menus magelur_menulist"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                 <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{ url('album/model') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ouragencies') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/agent') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a  href="">EVENTS</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
           <li class="header_menu_list">
              <a  href="">ABOUT</a>
             <!--  <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">About Us</a></li>
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Be a Model</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
              @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))

              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('model-dashboard/'.Session::get('model_id'))}}" >My Dashboard</a></li>
              <li><a href="{{ URL::to('myprofile/'.Session::get('model_id'))}}"> My {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('agent-dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list">
              <a  href="{{ url('login') }}">JOIN US</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a tabindex="-1" href="{{ url('/model/create') }}">Model Register</a></li>
              <li><a tabindex="-1" href="{{ url('/agent/create') }}">Agent Register</a></li>

              </ul> -->
              </li>
              </ul>

            @endif
            </div>
          </div>
          </div>
</header>

@include($pages)

<div class="map">
 <a href=""><img src="{{ asset('images/map.jpg') }}"></a>
</div>

<footer>
<div class="footer">
 <div class="container">
  <div class="clearfix">
   <div class="col-sm-7 wow SlideInUp">
    <div class="post">
     <p>Po Box 315000 Great Avn New York / USA</p>
     <ul>
      <li><a href="javascript:void(0)">HOME</a></li>
            <li><a href="aboutus.htm">ABOUT US</a></li>
            <li><a href="ourmodel.htm">OUR MODELS</a></li>
            <li><a href="applynow.htm">APPLY NOW</a></li>
            <li><a href="javascript:void(0)">NEWS & PRESS</a></li>
            <li><a href="contactus.htm">CONTACT</a></li>
     </ul>
    </div>
   </div>
   <div class="col-sm-5 wow SlideInDown">
    <div class="mail_adress clearfix">
     <div class="leftmail"><input type="text" name="" placeholder="Email Address"><!-- <p>Email Address</p> --></div>
     <div class="submit_mail"><a href="">Submit</a></div>
    </div>
    <div class="social_icons">
     <ul>
      <li>we are social</li>
      <li><a href=""><i class="fa fa-facebook"></i></a></li>
      <li><a href=""><i class="fa fa-twitter"></i></a></li>
      <li><a href=""><i class="fa fa-instagram"></i></a></li>
      <li><a href=""><i class="fa fa-youtube"></i></a></li>
     </ul>
    </div>
   </div>
  </div>
 </div>
</div>
</footer>
</body>

<script type="text/javascript">
  $('.owl-carousel.models').owlCarousel({
    loop:true,
    margin:25,
    nav:false,
    autoplay:true,
    responsive:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
         800:{
            items:3
        },
        1000:{
            items:3
        }
    }
})

$(document).ready(function(){
    $(".customNavigation .btn.btn-primary.next").click(function(){
      $('#owl-properties .owl-next')[0].click();
    })
    $(".customNavigation .btn.btn-primary.prev").click(function(){
      $('#owl-properties .owl-prev')[0].click();
    })
    var winhgt = $(document).height();
    var winbody = $("body").height();
    if (winbody < winhgt){
      $('body').addClass('footer_absolute');
    }
});
</script>

<script type="text/javascript">
        $(document).ready(function() {
        $('#vertical').lightSlider({
          item:1,
          vertical:true,
          verticalHeight:150
        });  
      });
</script>

<script>
// document.getElementById("uploadBtn").onchange = function () {
// document.getElementById("uploadFile").value = this.value; 
// };

// document.getElementById("uploadProfileBtn").onchange = function () {
// document.getElementById("uploadProfileFile").value = this.value; 
// };

// document.getElementById("uploadLengthBtn").onchange = function () {
// document.getElementById("uploadLengthFile").value = this.value; 
// };
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
    document.getElementById("main").style.marginLeft= "";
}

</script>
</html>

