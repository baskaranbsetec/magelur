
	$.validator.addMethod("checkWebsite", function(value, element) { 
    if(value.substr(0,7) != 'http://'){
        value = 'http://' + value;
    }
    if(value.substr(value.length-1, 1) != '/'){
        value = value + '/';
    }
    return this.optional(element) || /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/i.test(value); 
}, "Not valid url");

 $.validator.addMethod("checkName",function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
  },"Alphabets and spaces only allowed");


 $.validator.addMethod("checkUsername",function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z]*$/);
  },"Alphabets only allowed");