$(document).ready(function() {
  /******************** AJAX COMMENT ********************/
  $("#commentsBox").click(function() {
    /**** Show Submit comment Button ****/
    $('#postComment').show();
        $('.rate_article').show();

    // show rating 
      // set comment rating
      $('#comment_rating').barrating({
        theme: 'fontawesome-stars',
        initialRating: '0',

      });

    $("#SubmitComment").submit(function(e) {

          // if form is in process , stop the form
          if($('form#SubmitComment').hasClass('process'))
            return false;

          // add class to keep form in process
          $('form#SubmitComment').addClass('process');


          // Get Variables
          var postComment  = document.getElementById('postComment');
          var videoID      = postComment.getAttribute('data-video');
          var bodyAttr     = document.getElementsByTagName("body")[0];
          var dataRoot     = bodyAttr.getAttribute("data-root");
          var rating       = $('.br-current-rating').html();
         
          var url          = dataRoot+"/comment/"+videoID;
          if($('#comment_type').val() == 'photo'){
               url         = dataRoot+"/photo/comment/"+videoID;
          }

          e.preventDefault();

          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          $.ajax({
            type : "POST",
            url : url,
            data: {
              'comment':$('#commentsBox').val(),
              'rating' : rating
            },
            beforeSend: function() {
            $('.loader').show();
            },
            success : function(response) {
              if (response.status == 'error') {
                var hideOldError = document.getElementById('toast-container');
                if (hideOldError) {
                  // $(hideOldError).hide();
                }
                  // Show Error Notification
                  toastr["error"](response.msg)
                  toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"
                  }
                $('.loader').hide();
                }else{

                  var hideOldError = document.getElementById('toast-container');
                  console.log(hideOldError)
                  if (hideOldError) {
                    // $(hideOldError).hide();
                  }
                  // Success Postting 
                  toastr["success"](response.msg)
                  toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"


                  }
            $('.loader').hide();

                  // empty the textare
                  $('#commentsBox').val('');

                  // remove no message content
                  $('.comments-list').find('.no_data_msg').remove();

                  // append new comments
                  $('.comments-list').prepend(response.html);
                  
                }
                // remove process , once action completed
                $('form#SubmitComment').removeClass('process');
                return false;
              }

            });
          e.preventDefault();
        });
  });
});