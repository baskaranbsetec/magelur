$(document).ready(function() {

  /******************** AJAX Like Vote ********************/
  $("#like").click(function(e) {

      // Get Variables
      var btnLike  = document.getElementById('like');
      var videoID  = btnLike.getAttribute('data-video');
      var bodyAttr = document.getElementsByTagName("body")[0];
      var dataRoot = bodyAttr.getAttribute("data-root");

      // check url
      var url          = dataRoot+"/like";
      console.log($('#vote_type').val())
      if($('#vote_type').val() == 'photo'){
       url         = dataRoot+"/like/photo";
     }      
      e.preventDefault();

      $.ajax({
          type : "GET",
          url : url,
          data: {'videoID':videoID},
          success : function(response) {
            if (response.status == 'error') {

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["error"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
            }else{
              btnLike.innerHTML     = '<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span>'+response.likes;
              btnLike.style.opacity = 1;

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["success"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
              
            }
            
          }

      });
      e.preventDefault();
  });


  /******************** AJAX Dislike Vote ********************/
  $("#dislike").click(function(e) {

      // Get Variables
      var dislikeBtn  = document.getElementById('dislike');
      var videoID     = dislikeBtn.getAttribute('data-video');
      var bodyAttr    = document.getElementsByTagName("body")[0];
      var dataRoot    = bodyAttr.getAttribute("data-root");
      // check url
      var url          = dataRoot+"/dislike/";
      console.log($('#vote_type').val())
      if($('#vote_type').val() == 'photo'){
       url         = dataRoot+"/dislike/photo/";
     } 

      e.preventDefault();

      $.ajax({
          type : "GET",
          url : url,
          data: {'videoID':videoID},
          success : function(response) {
            if (response.status == 'error') {

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["error"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
            }else{
              dislikeBtn.innerHTML     = '<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>'+response.dislikes;
              dislikeBtn.style.opacity = 1;

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["success"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
              
            }
          }
      });
      e.preventDefault();
  });

});