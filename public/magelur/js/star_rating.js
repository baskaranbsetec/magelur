function setRating(ele,rating){
	// console.log(ele)
	ele.barrating({
		theme: 'fontawesome-stars',
		readonly: true,
		initialRating: rating
	});
	// remove not rated class
	ele.removeClass('not_rated');
	// set rate
	ele.addClass('rated');
}

function setLoadMoreRating(){
	$('.not_rated').each(function(){
		var id      = $(this).attr('id');
		var rate    = $(this).attr('data-rate');

		// stop the function , if there is no rate
		if(rate <= 0)
			return false;

		var ele  = $('#'+id);

		ele.barrating({
			theme: 'fontawesome-stars',
			readonly: true,
			initialRating: rate
		});
		// remove not rated class
		ele.removeClass('not_rated');
		// set rate
		ele.addClass('rated');
		console.log(this);
	})
}	