/***********************************************************************************************************
* Preview and upload multiple files using Jquery and PHP
* Written by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: vasplusblog@gmail.com or info@vasplus.info

**********************************Copyright Information*****************************************************
* This script has been released with the aim that it will be useful.
* Please, do not remove this copyright information from the top of this page.
* If you want the copyright info to be removed from the script then you have to buy this script.
* This script must not be sold.
* All Copy Rights Reserved by Vasplus Programming Blog
*************************************************************************************************************/


// Preview File(s)
function vpb_image_preview(vpb_selector_) 
{
	// hide the upload button
	$('div.photo_blk_upload_blk').hide();

	// check user has an option to upload images
	var $fileUpload = $("input[name='vpb-data-file']");
	// get the limit count
	var limit       = $("input[name='remaining_media_count']").val();
	var uploadlimit = $('#uploadlimit').val();
	console.log(uploadlimit);

	// removed the disabled attribute for Upload

/*	if (parseInt($fileUpload.get(0).files.length) > limit){
		alert("Your File Limit Reached.Please upgrade your plan");
		return;
	}	*/
	if(vpb_selector_.files.length>uploadlimit)
	{
    swal({
      // title: "Are you sure?",
      text: "Please select only "+uploadlimit+" images",
      // icon: "warning",
      // buttons: true,
      // dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('div.photo_blk_upload_blk').show();
        	$('#vpb-data-file').val(null); 
        	$('div#errorToShow').show();

      } else {
        swal("Your Album is safe!");
      }
    }); 
		// swal("Please select only "+uploadlimit+" images");
		// $('div.photo_blk_upload_blk').show();
	}
	else
	{
	var id = 1, last_id = last_cid = '',init_files = 1;
	$.each(vpb_selector_.files, function(vpb_o_, file)
	{
		var style    = (init_files != vpb_selector_.files.length)?'style="display:none;"':'';
		var actClass = (init_files == vpb_selector_.files.length)?'active':'';

		// make increment
		init_files++;
		var ext = $('#vpb-data-file').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
    swal("Invalid extension!");
$('.photo_details_preview').hide();
$('#vpb-display-preview').hide();
$('div.photo_blk_upload_blk').show();
$('#vpb-data-file').val(null); 
$('div#errorToShow').show();
}	
else{
	

		if (file.name.length>0) 
		{
$('.photo_details_preview').show();
$('#vpb-display-preview').show();
			if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
			else
			{
			   // Clear previous previewed files and start again
			   $('#vpb-display-preview').html(''); 


		
			   var reader = new FileReader();
			   
			   reader.onload = function(e) 
			   {
			   	var image = new Image();
    image.src = e.target.result;

    image.onload = function() {
        // access image size here 
      
if(this.width >= 900 || this.height >= 900 )
    {
    	// $('.input_button_div').show();
    	
		   	  // form dynamic fields 
			   	  $('.photo_details_preview').append('<div '+style+' data-id="'+vpb_o_+'" class="photo_info_details photo_info-'+vpb_o_+' '+actClass+'"><input type="text" placeholder="Photos Title" name="title-'+vpb_o_+'" value="'+file.name+'" id="title'+vpb_o_+'"><textarea placeholder="Photos Description" name="description-'+vpb_o_+'" id="description"></textarea><input type="text"class="photo_tags" placeholder="Photos Tags" name="tags-'+vpb_o_+'" id="tags" ></div>');
			   	 
			   	  // prepare preview image
				   $('#vpb-display-preview').append(
				   '<div id="selector_'+vpb_o_+'" class="ss vpb_wrapper  '+actClass+'" data-id="'+vpb_o_+'"> \
				   <div class="popup_overlay1"><i class="fa fa-spinner fa-spin" title="Loading"></i><i class="fa fa-check" title="Success"></i></div><img class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
				   title="'+ escape(file.name) +'" /> \
				   <div class="image_icons"><a  style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
				   onclick="vpb_remove_selected(\''+vpb_o_+'\',\''+file.name+'\')"><i class="fa fa-trash"></i>Remove</a></div> \
				   </div>');
				}

           else{
            	$('.input_button_div').hide();
            	$('.photo_details_preview').append('<div '+style+' data-id="'+vpb_o_+'" class="photo_info_details photo_info-'+vpb_o_+' '+actClass+'"><input type="text" placeholder="Photos Title" name="title-'+'undefined'+'" value="'+file.name+'" id="title'+'undefined'+'"><textarea placeholder="Photos Description" name="description-'+'undefined'+'" id="description"></textarea><input type="text"class="photo_tags" placeholder="Photos Tags" name="tags-'+'undefined'+'" id="tags" ></div>');
			   	 
			   	  // prepare preview image
				   $('#vpb-display-preview').append(
				   '<div id="selector_'+vpb_o_+'" class="dd vpb_wrapper '+actClass+'" data-id="'+vpb_o_+'" data-val="'+'invalid'+'"> \
				   <div class="popup_overlay1"><i class="fa fa-spinner fa-spin" title="Loading"></i><i class="fa fa-remove" title="Failed">Failed</i></div><img class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
				   title="'+ escape(file.name) +'" /> \
				   <div class="image_icons photo_valid-'+vpb_o_+'"><a  style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
				   onclick="vpb_remove_selecteda(\''+vpb_o_+'\',\''+file.name+'\')"><i class="fa fa-remove"></i>Invalid size</a></div> \
				   </div>');
				   
            }
};

			   }
			   reader.readAsDataURL(file);
		   }
		}
		}
		// else {  
		// 	return false; 
		// }
	});
	}
	triggerTags();
}
// $(document).on('click','.popup_overlay1',function(){
// 	$('.add_details').hide();

// });

//  show form based on selected images
$(document).on('click','.vpb_wrapper',function(){

	var curId = $(this).attr('data-id');
	var curIds = $(this).attr('data-val');
	if(curIds=='invalid'){
		swal('Invalid Size! Upload Image with minimum 900*900 px')
		$('.photo_info-'+curId).hide()
		$('.photo_info_details').hide();

	}
	else{
		// $('.add_details').hide();
	// if value not exists
	if(!curId)
	  return false;	
	// remove active class
	$('.vpb_wrapper').removeClass('active');
	// add active class for selected content
	$(this).addClass('active');

	// hide all fields
	$('.photo_info_details').hide();
	// show the selected fields
	$('.photo_info-'+curId).show()
	// $('.add_details').show();
}
if($('.vpb_wrapper').length == 0){
		$('.add_details').hide();
	}

});

//Remove Previewed File only from the screen but will be uploaded when you click on the start upload button
function vpb_remove_selected(id,name)
{
$('.input_button_div').show();

// swal("Click image to add details");
	// remove form details
	$('.photo_info-'+id).remove();
	// remove image
	$('#v-add-'+id).remove();
	$('#selector_'+id).remove();
	$('.add_details').show();
	// alert($('.ss').length);
	if($('.ss').length == 0){
	$('.input_button_div').hide();
	 $('.add_details').hide();
	 // $('div#errorToShow').show();
	
	}
	if($('.vpb_wrapper').length == 0){
		$('.add_details').hide();
		$('#vpb-data-file').val(null); 
		$('div#errorToShow').show();
	}
	if($('.vpb_wrapper').length <= 0){
		$('.add_details').show();
		$('.input_button_div').show();
		// $('#vpb-data-file').val(null); 
		// show the upload button
		$('div.photo_blk_upload_blk').show();
		$('div#errorToShow').show();
	}

}
function vpb_remove_selecteda(id,name)
{
// swal('invalid size');
$('.input_button_div').hide();
$('.photo_info-'+id).remove();
	// remove image
	$('#v-add-'+id).remove();
	$('#selector_'+id).remove();
	$('.add_details').show();

// alert($('.dd').length);
if($('.dd').length == 0){
	$('.input_button_div').show();
	// $('#vpb-data-file').val(null); 
	// $('.add_details').hide();
	// $('div#errorToShow').show();

}
if($('.vpb_wrapper').length == 0){
$('.add_details').hide();
		$('#vpb-data-file').val(null); 
		$('div#errorToShow').show();
			}
	if($('.vpb_wrapper').length <= 0){
		$('div.photo_blk_upload_blk').show();
		$('div#errorToShow').show();

	}
	
}
function getSelectedFilesCount(){
	var $fileUpload = $("input[type='file']");
	if (parseInt($fileUpload.get(0).files.length)>2){
		alert("You can only upload a maximum of 2 files");
	}
}


//Upload Files
function vpb_upload_previewed_files(url)
{
	// change button to disabled
	$('input[type="submit"]').prop('disabled',true).val('Loading...');
	$('.submit-wave').addClass('submit-wave-loader');

	// If no file is selected then do not proceed
	if(document.getElementById('vpb-data-file').value == "") 
	{
		$("#vpb-display-preview").fadeIn(2000).html('<div class="vpb_display_info" align="center">Please browse for some files to proceed.</div>'); 
		$('input[type="submit"]').prop('disabled',false).val('POST');
		return false 
	} 
	else 
	{
		//Proceed now because a user has selected some files
		var vpb_files = document.getElementById('vpb-data-file').files;
				
		// Create a formdata object and append the files
		var vpb_data = new FormData();
		
		$.each(vpb_files, function(keys, values)
		{
			// get form values & push to array
			var title = $('input[name="title-'+keys+'"]').val();
			var desc  = $('textarea[name="description-'+keys+'"]').val();
			var tags  = $('input[name="tags-'+keys+'"]').val();
			// set value in array
			vpb_data.append("photo_title-"+keys, title);
			vpb_data.append("photo_desc-"+keys, desc);
			vpb_data.append("photo_tags-"+keys, tags);
			vpb_data.append(keys, values);
		});
		// get album values
		var album_type  = $('input[name="album_type"]:checked').val();
		vpb_data.append("album_type", album_type);

		if(album_type == 'new'){
			var album_name  = $('input[name="album_name"]').val();
			var album_desc  = $('textarea[name="album_desc"]').val();
			// add album values based on user selected option
			vpb_data.append("album_name", album_name);
			vpb_data.append("album_desc", album_desc);
		}else{
			var album_id    = $('select[name="album_id"]').val();
			// add album values based on user selected option
			vpb_data.append("album_id", album_id);
		}

		$.ajax({
			url: url,
			type: 'POST',
			data: vpb_data,
			cache: false,
			processData: false,
			contentType: false,
			beforeSend: function()
			{
				$('.vpb_wrapper').each(function(){
					// add loading class
					$(this).addClass('loading');
				});
			},
			success: function(response)
			{
				var res = JSON.parse(response);
				// console.log(res.arr)
				// check the responce
				if(res.status == 1){
					// parse the JSON responce
					var arr = JSON.parse(res.arr);

					for (var i = 0; i < arr.length; i++) {
						var key    = arr[i]['key'];
						var ele    = $('#selector_'+key);
						var imgStatus = arr[i]['status'];
						
						// remove loading class
						ele.removeClass('loading');

						if(imgStatus == 1){
						// add completed class
						ele.addClass('completed');
						}else{
						// add completed class
						ele.addClass('failed');
						}

						$('input[type="submit"]').prop('disabled',true).val('Photo Updated...');

						setTimeout(function(){
							if(res.url)
								window.location.href = res.url;
							else
								location.reload();
						},200);
						  
						  // return;	

					}
				}else{
					location.reload();
				}
			}
		});
	}
}