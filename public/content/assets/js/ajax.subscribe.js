$(document).ready(function() {

  	/******************** AJAX Subscribe ********************/
  	$("#subscribeBTN").click(function(e) {

      	// Get Variables
		var subscribeBTN     = document.getElementById('subscribeBTN');
		var username         = subscribeBTN.getAttribute('data-user');
		var bodyAttr         = document.getElementsByTagName("body")[0];
      	var dataRoot         = bodyAttr.getAttribute("data-root");
      	var totalSubscribers = document.getElementById('totalSubscribers');
      	e.preventDefault();

		$.ajax({
          	type : "GET",
          	url : dataRoot+"/subscribe",
          	data: {'username':username},
          	success : function(response) {
	            if (response.status == 'error') {

	            	// Hide any Error
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  hideOldError.hide();
	              	}

	              	// Show Error Notification
	              	toastr["error"](response.msg)
	              	toastr.options = {
		                "closeButton": false,
		                "debug": false,
		                "newestOnTop": true,
		                "progressBar": false,
		                "positionClass": "toast-top-full-width",
		                "preventDuplicates": false,
		                "onclick": null,
		                "showDuration": "300",
		                "hideDuration": "1000",
		                "timeOut": "3000",
		                "extendedTimeOut": "1000",
		                "showEasing": "linear",
		                "hideEasing": "swing",
		                "showMethod": "slideDown"
	              	}
	            }else{

	            	// Hide Toastr Msg
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  	hideOldError.hide();
	              	}

	              	// Change Subscribe Button style
	            	totalSubscribers.innerHTML         = response.totalSub;
	            	subscribeBTN.innerHTML             = 'Unsubscribe';
	            	subscribeBTN.style.backgroundColor = '#C0C0C0';
	            	subscribeBTN.style.backgroundImage = "url("+dataRoot+"/content/assets/img/unsubscribe.png)";

	              	// Show Success Notification
	              	toastr["success"](response.msg)
	              	toastr.options = {
		                "closeButton": false,
		                "debug": false,
		                "newestOnTop": true,
		                "progressBar": false,
		                "positionClass": "toast-top-full-width",
		                "preventDuplicates": false,
		                "onclick": null,
		                "showDuration": "300",
		                "hideDuration": "1000",
		                "timeOut": "3000",
		                "extendedTimeOut": "1000",
		                "showEasing": "linear",
		                "hideEasing": "swing",
		                "showMethod": "slideDown"
	              	}
	              
	            }
        	}
    	});
    	e.preventDefault();

	});


	/******************** AJAX Unsubscribe ********************/
  	$("#unsubscribeBTN").click(function(e) {

      	// Get Variables
		var unsubscribeBTN   = document.getElementById('unsubscribeBTN');
		var username         = unsubscribeBTN.getAttribute('data-user');
		var bodyAttr         = document.getElementsByTagName("body")[0];
      	var dataRoot         = bodyAttr.getAttribute("data-root");
      	var totalSubscribers = document.getElementById('totalSubscribers');
      	e.preventDefault();

		$.ajax({
          	type : "GET",
          	url : dataRoot+"/unsubscribe",
          	data: {'username':username},
          	success : function(response) {
	            if (response.status == 'error') {

	            	// Hide any Error
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  hideOldError.hide();
	              	}

	              	// Show Error Notification
	              	toastr["error"](response.msg)
	              	toastr.options = {
		                "closeButton": false,
		                "debug": false,
		                "newestOnTop": true,
		                "progressBar": false,
		                "positionClass": "toast-top-full-width",
		                "preventDuplicates": false,
		                "onclick": null,
		                "showDuration": "300",
		                "hideDuration": "1000",
		                "timeOut": "3000",
		                "extendedTimeOut": "1000",
		                "showEasing": "linear",
		                "hideEasing": "swing",
		                "showMethod": "slideDown"
	              	}
	            }else{

	            	// Hide Toastr Msg
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  	hideOldError.hide();
	              	}

	              	// Change Subscribe Button style
	            	totalSubscribers.innerHTML           = response.totalSub;
	            	unsubscribeBTN.innerHTML             = 'Subscribe';
	            	unsubscribeBTN.style.backgroundColor = '#1784c7';
	            	unsubscribeBTN.style.backgroundImage = "url("+dataRoot+"/content/assets/img/play.png)";

	              	// Show Success Notification
	              	toastr["success"](response.msg)
	              	toastr.options = {
		                "closeButton": false,
		                "debug": false,
		                "newestOnTop": true,
		                "progressBar": false,
		                "positionClass": "toast-top-full-width",
		                "preventDuplicates": false,
		                "onclick": null,
		                "showDuration": "300",
		                "hideDuration": "1000",
		                "timeOut": "3000",
		                "extendedTimeOut": "1000",
		                "showEasing": "linear",
		                "hideEasing": "swing",
		                "showMethod": "slideDown"
	              	}
	              
	            }
        	}
    	});
    	e.preventDefault();

	});

});