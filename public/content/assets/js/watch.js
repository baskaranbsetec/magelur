$(document).ready(function() {

	/**** Show More Button ****/
	$('#showMore').click(function () {
		var description = document.getElementById('videoDescription');
		var showMore    = document.getElementById('showMore');
		if (description.style.height == '50px') {
			
			showMore.innerHTML="Show Less";
        	description.style.height="auto";

		}else{
			showMore.innerHTML="Show More";
        	description.style.height="50px";
		}
	});

  /*********** Show Embed Code ************/
  $('#shareVid').click(function(){
    $('.embedVideo').slideToggle();
  });

	/******* AJAX Report Video ******/
	$("#reportVid").click(function(e) {

      // Get Variables
      var reportBtn  = document.getElementById('reportVid');
      var videoID    = reportBtn.getAttribute('data-video');
      var bodyAttr   = document.getElementsByTagName("body")[0];
      var dataRoot   = bodyAttr.getAttribute("data-root");
      e.preventDefault();

      $.ajax({
          type : "GET",
          url : dataRoot+"/report/"+videoID,
          data: {'videoID':videoID},
          success : function(response) {
            if (response.status == 'error') {

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["error"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
            }else{

              var hideOldError = document.getElementById('toast-container');
              if (hideOldError) {
                  hideOldError.hide();
              }
              // Show Error Notification
              toastr["success"](response.msg)
              toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
              }
              
            }
          }
      });
      e.preventDefault();
  	});


  	/******** Share Toggle ********/
  	$('.a_share').click(function() {
  		var videoShare = document.getElementById('video_share');
  		var shareVid   = document.getElementById('shareVid');

  		if (videoShare.style.display == 'none') {

  			shareVid.style.opacity = 1;
  			$('#video_share').show();

  		}else{
  			shareVid.style.opacity = 0.6;
  			$('#video_share').hide();
  		}
  	});

});