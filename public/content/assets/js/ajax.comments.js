$(document).ready(function() {

  /******************** AJAX COMMENT ********************/
  $("#commentsBox").click(function() {

      /**** Show Submit comment Button ****/
      $('#postComment').show();

      $("#SubmitComment").submit(function(e) {

          // Get Variables
          var postComment  = document.getElementById('postComment');
          var videoID      = postComment.getAttribute('data-video');
          var bodyAttr     = document.getElementsByTagName("body")[0];
          var dataRoot     = bodyAttr.getAttribute("data-root");

          e.preventDefault();

          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
          });

          $.ajax({
              type : "POST",
              url : dataRoot+"/comment/"+videoID,
              data: {'comment':$('#commentsBox').val()},
              success : function(response) {
                if (response.status == 'error') {
                  var hideOldError = document.getElementById('toast-container');
                  if (hideOldError) {
                      hideOldError.hide();
                  }
                  // Show Error Notification
                  toastr["error"](response.msg)
                  toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"
                  }

                }else{
                  
                  var hideOldError = document.getElementById('toast-container');
                  if (hideOldError) {
                      hideOldError.hide();
                  }
                  // Success Postting 
                  toastr["success"](response.msg)
                  toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"
                  }
                  
                }
              }
          });
          e.preventDefault();
      });

  });

});