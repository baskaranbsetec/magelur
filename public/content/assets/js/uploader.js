

    var ajax = new XMLHttpRequest();  

    function _(id){
        return document.getElementById(id);
    }

    function upload(e){

        e.preventDefault();

        
        var title          = $('#title').val();
        var tags           = $('#tags').val();
        var category       = $('#category').val();
        var vip            = $('#vip').val();
        var description    = $('#description').val();
        var fileUP         = _('fileUP').files[0];


        var allowedExt   = /^(video\/mp4|video\/x-msvideo|video\/webm|video\/x-flv|video\/quicktime|video\/x-ms-wmv|video\/mpeg|video\/3gpp)$/i;
        var formdata = new FormData();
        var hideOldError = _('toast-container');

        if (_('video_sites').value != '' ) {

                var video_sites    = $('#video_sites').val();

                formdata.append('title', title);
                formdata.append('tags', tags);
                formdata.append('description', description);
                formdata.append('category', category);
                formdata.append('vip', vip);
                formdata.append('video_sites', video_sites);
                formdata.append('cover', fileUP);
                ajax.upload.addEventListener('progress', progressHandler, false);
                ajax.addEventListener('load', completeHandler, false);
                ajax.addEventListener('abort', abortHandler, false);
                ajax.addEventListener('error', errorHandler, false);
                ajax.open('POST', 'upload');
                ajax.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));  
                ajax.setRequestHeader("Cache-Control", "max-age=0");
                ajax.send(formdata);


        }else {

            var file           = _("video_file").files[0];

            // Check file Size
            if (file.size > MaxFilesize) {

            toastr["error"]('Oops! Video Size is too Large.')
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
            }
            // Check file Extension
            }else if ( ! allowedExt.test(file.type) ){

                if (hideOldError) {
                    hideOldError.hide();
                }
                // Show Error Notification
                toastr["error"]('Oops! Please Select a valid Video File.')
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"
                }

            }else{

                formdata.append('video_file', file);
                formdata.append('title', title);
                formdata.append('tags', tags);
                formdata.append('description', description);
                formdata.append('category', category);
                formdata.append('vip', vip);
                formdata.append('cover', fileUP);
                ajax.upload.addEventListener('progress', progressHandler, false);
                ajax.addEventListener('load', completeHandler, false);
                ajax.addEventListener('abort', abortHandler, false);
                ajax.addEventListener('error', errorHandler, false);
                ajax.open('POST', 'upload');
                ajax.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));  
                ajax.setRequestHeader("Cache-Control", "max-age=0");
                ajax.send(formdata);
            }
        }
    }

    function progressHandler(e){

        $('.toast_upload').show();
        var percent = (e.loaded / e.total) * 100;
        percent = Math.round(percent);
        _('progressbar').style.width = Math.round(percent)+'%';
        _('progressAJAX').innerHTML = percent + '%';
    }
     function completeHandler(){
        var hideOldError = _('toast-container');
        if (hideOldError) {
            hideOldError.hide();
        }

        response = JSON.parse(ajax.responseText);

        // If Error 
        if (response.status == 'error') {

            // Show Error Notification
            toastr["error"](response.msg)
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "linear",
                "hideEasing": "swing",
                "showMethod": "slideDown"
            }

        } else {

            if (response.redirect) {
                // Redirect to video
                window.location.replace(response.redirect);
            } else {
                // Show Success Notification
                toastr["success"](response.msg)
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-full-width",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "swing",
                    "showMethod": "slideDown"
                }
            }

        }
        
     }

     function abortHandler(){
        alert('file upload aborted');
     }

     function errorHandler(){
        alert('file upload has an error');
     }
    _('uploaderForm').addEventListener('submit', upload, false);
    //$('#uploaderForm').addEventListener('submit', upload, false);


$(document).ready(function(){

    /********* Fetch Video ************/
    $('#FetchVideo').click(function(e){

        e.preventDefault();

        // Get Video ID
        var vidID        = $('#youtube_id').val();

        // Get Root 
        var bodyAttr     = document.getElementsByTagName("body")[0];
        var dataRoot     = bodyAttr.getAttribute("data-root");

        var hideOldError = document.getElementById('toast-container');

        // Hide Old Messages
        if (hideOldError) {
            hideOldError.hide();
        }

        // Send AJAX Request
        $.ajax({

            type : "GET",
            url : dataRoot+"/youtube",
            data: {'youtube_vid':vidID},
            success : function(response) {
                if (response.status == 'error') {

                    // Show Error Message
                    toastr["error"](response.msg)
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": false,
                        "positionClass": "toast-top-full-width",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "3000",
                        "extendedTimeOut": "1000",
                        "showEasing": "linear",
                        "hideEasing": "swing",
                        "showMethod": "slideDown"
                    }

                }else{
                    
                    // Get Variables
                    var video_title = _('title');
                    var video_desc  = _('description');

                    video_title.value = response.title;
                    video_desc.value  = response.descr;
                    $('#coverIMG').hide();

                }
            }
        });

        e.preventDefault();

    });

});