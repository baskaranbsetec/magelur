$(document).ready(function () {
	
	$(function(){

		var appendthis     =  ("<div class='modal-overlay js-modal-close'></div>");
		var readMsgBtn     = document.getElementById('readMsg');
		var ajaxMsgContent = document.getElementById('ajaxMsgContent');
		var bodyAttr       = document.getElementsByTagName("body")[0];
      	var dataRoot       = bodyAttr.getAttribute("data-root");

		$('.readMsg').click(function(e){

			var MsgID          = $(this).attr('data-msg-id');
      		e.preventDefault();

			$.ajax({
          	type : "GET",
          	url : dataRoot+"/mailbox/read",
          	data: {'id':MsgID},
          	success : function(response) {
	            if (response.status == 'error') {

	            	// Hide any Error
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  hideOldError.hide();
	              	}

	              	// Show Error Notification
	              	toastr["error"](response.msg)
	              	toastr.options = {
		                "closeButton": false,
		                "debug": false,
		                "newestOnTop": true,
		                "progressBar": false,
		                "positionClass": "toast-top-full-width",
		                "preventDuplicates": false,
		                "onclick": null,
		                "showDuration": "300",
		                "hideDuration": "1000",
		                "timeOut": "3000",
		                "extendedTimeOut": "1000",
		                "showEasing": "linear",
		                "hideEasing": "swing",
		                "showMethod": "slideDown"
	              	}
	            }else{

	            	// Hide Toastr Msg
	              	var hideOldError = document.getElementById('toast-container');
	              	if (hideOldError) {
	                  	hideOldError.hide();
	              	}

	              	// Change Subscribe Button style
	            	ajaxMsgContent.innerHTML         = response.msg;
	            	$("body").append(appendthis);
			        $(".modal-overlay").fadeTo(500, 0.7);
			        //$(".js-modalbox").fadeIn(500);
			        var modalBox = $(this).attr('data-modal-id');
			        $('#MsgContent').fadeIn($(this).data());
	              
	            }
        	}
    	});

    	e.preventDefault();

		});
      
    $(".js-modal-close, .modal-overlay").click(function() {
        $(".modal-box, .modal-overlay").fadeOut(500, function() {
            $(".modal-overlay").remove();
        });
     
    });
     
    $(window).resize(function() {
        $(".modal-box").css({
            top: '80px'
        });
    });
     
    $(window).resize();
     
    });

})