   
// Mobile Menu Script Start
    $(document).ready(function(){
        $('#search-toggle').click(function() {
            $('.search_box').slideToggle();
        });
        $('#search-toggle-mobile').click(function() {
            $('.search_box').slideToggle();
        });
    });
// Mobile Menu Script End


// What We Do Slider Script Start
      $(document).ready(function() {
        $('#what_vertical').lightSlider({
          gallery:true,
          item:1,
          vertical:true,
          verticalHeight:305,
          vThumbWidth:50,
          thumbItem:8,
          thumbMargin:4,
          slideMargin:0
        });  
      });
// What We Do Slider Script End


