<?php
namespace App\Helper;

use App\Helper\Helper as Helper;
use \SimpleXmlElement as SimpleXmlElement;
use Intervention\Image\ImageManagerStatic as Image;

/**************************************************************

		<------------------------------------------------>
		TTube Script - by Abdelhak Ezzaroual "MendelMan"
					< ezzaroual@mail.com >
					< www.mendelman.host >
		<------------------------------------------------>

***************************************************************/

class Helper
{

	/* Check PHP VERSION */
	public static function check_phpversion()
	{
		// Need PHP VERSION 5.5.9 or greater
		if (version_compare(PHP_VERSION, "5.5.9", ">=")) {
			return TRUE;
		}
		return FALSE;
	}


	/* Check if PDO Extension loaded or not */
	public static function PDOext()
	{
		if (extension_loaded("pdo")) {
			return TRUE;
		}
		return FALSE;
	}


	/* Check if Allow_url_fopen Enabled or not */
	public static function Allow_url_fopen()
	{
		if( ini_get('allow_url_fopen') ) {
			return TRUE;
		} 
		return FALSE;
	}


	/* Channel user must be numeric and Alpha */
	public static function secure_channel_user($username)
	{
		// Check if username content only alpha 
		if (ctype_alpha($username)) {
			return TRUE;
		}

		// Check if username content only numbers and alpha
		elseif (ctype_alnum($username)) {
			return TRUE;
		}

		// wrong username characters, return false
		else{
			return FALSE;
		}

	}

	/* Validate Image Function */
	public static function secure_image($image, $extension, $size, $allowed_size, $mime)
	{
		// Allowed Extentions 
		$allowed_ext = array('jpg', 'jpeg', 'png');
		if (!in_array($extension, $allowed_ext)) {
			return FALSE;
		}

		// Check image Size
		if ( ($size > $allowed_size) OR ($size < 1000) ) {
			return FALSE;
		}

		// Check Mime type
		$allowed_mime = array('image/jpeg', 'image/pjpeg', 'image/png');
		if (!in_array($mime, $allowed_mime)) {
			return FALSE;
		}

		// Check Image With and Height
		$minwidth    = 30;
		$minheight   = 30;
		$image_sizes = getimagesize($image);

		if ($image_sizes[0] < $minwidth OR $image_sizes[1] < $minheight){
		    return FALSE;
		}

		return TRUE;
	}


	/************** Get site Title **************/
	public static function site_title()
	{
		// Get Settings
		$settings = \DB::table('settings_general')->where('id', 1)->first();

		return $settings->site_title;

	}


	/************** Get site Title Description **************/
	public static function title_description()
	{
		// Get Settings
		$settings = \DB::table('settings_general')->where('id', 1)->first();

		return $settings->site_description;

	}


	/************** Get site Description **************/
	public static function seo_description()
	{
		// Get Settings
		$settings = \DB::table('settings_seo')->where('id', 1)->first();

		return $settings->meta_description;

	}


	/**** If Username Already taken ****/
	public static function double_users($input_user)
	{
		// Online User Id
		$uid = \Auth::user()->id;

		// Check double username
		$users = \DB::table('users')->where('id', '!=', $uid)->first();

		if ( ($users) AND ($input_user == ($users->username) ) ) {
			return TRUE;
		}else{
			return FALSE;
		}

	}

	/***** Convert Numbers Format: K M B ******/
	public static function convert_num_format($num, $precision = 2){

	    if ($num >= 1000 && $num < 1000000) {
	    	$new_number = number_format($num/1000,$precision).' K';

	    } else if ($num >= 1000000 && $num < 1000000000) {
	    	$new_number = number_format($num/1000000,$precision).' M';

	    } else if ($num >= 1000000000) {
	    	$new_number = number_format($num/1000000000,$precision).' B';

	    } else {
	    	$new_number = $num;
	    }

	  	return $new_number;
	}

	/************* Change Database Date Format *************/
	public static function change_date_format($date)
	{
		$new_fomat = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(); 
		return $new_fomat;
	}


	/************* Change Description Date Format *************/
	public static function description_date($date)
	{
		$new_fomat = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->toFormattedDateString(); 
		return $new_fomat;
	}

	/************* Check User Storage *************/
	public static function check_user_storage()
	{
		// Get User ID
		$user_id = \Auth::user()->id;

		// Get Total Videos Size
		$total_videos_size = \DB::table('videos')
						     ->where('user_id', $user_id)
						     ->sum('size');

		// Get Allowed Size Per User
		$settings_upload = \DB::table('settings_upload')
						   ->where('id', 1)->first();

		// Convert from bit to Gigabyte
		$giga = $total_videos_size * 0.000000001;

		if (Self::is_vip()) {
			// Check size
			if ( $giga > (Self::settings_vip()->storage_space) ) {
				return FALSE;
			}else{
				return TRUE;
			}
		}else{
			// Check size
			if ( $giga > ($settings_upload->storage_space) ) {
				return FALSE;
			}
			return TRUE;
		}

	}

	/************* Check Video Size *************/
	public static function good_video_size($size)
	{
		// Get Allowed Size
		$allowed_video_size = \DB::table('settings_upload')
							  ->where('id', 1)
							  ->first();

		// Convert Given size to human readable
		$miga_size = number_format( $size / 1048576, 2);

		if (Self::is_vip()){

			if ($miga_size > Self::settings_vip()->video_size) {
				return FALSE;
			}

			// It's a Good Size To be Upload
			return TRUE;

		}else{
			if ($miga_size > $allowed_video_size->video_size) {
				return FALSE;
			}

			// It's a Good Size To be Upload
			return TRUE;
		}
	}

	/************* Check Video Duration *************/
	public static function video_duration($seconds)
	{
		// Change Seconds to Minutes
		$divisor_for_minutes = $seconds % (60 * 60);
		$minutes = number_format($divisor_for_minutes / 60, 2);

		// Get Allowed Duration
		$allowed_video_duration = \DB::table('settings_upload')
							    ->where('id', 1)
							    ->first();

		if (Self::is_vip()) {
			if ($minutes > Self::settings_vip()->video_time) {
				return FALSE;
			}

			return TRUE;
		}else{
			if ($minutes > $allowed_video_duration->video_time) {
				return FALSE;
			}

			return TRUE;
		}
		
	}


	/******************** youtube Video Duration *****************/
	public static function youtube_duration($y_duration)
	{
		$duration = new \DateInterval($y_duration);
		$p = explode(':', $duration->format('%H:%I:%S'));
		if ($p[0] == 00) {
			return $duration->format('%H:%I:%S');
		}else{
			return $duration->format('%H:%I:%S');
		}
	}


	/*********** Fetch Highest Youtube Preview **********/
	public static function fetch_highest_res($video_id) {
	    $resolutions = array('maxresdefault', 'hqdefault', 'mqdefault');     
	    foreach($resolutions as $res) {
	        $imgUrl = "http://i.ytimg.com/vi/$video_id/$res.jpg";
	        if(@getimagesize(($imgUrl))) 
	            return $imgUrl;
		}
	}


	/******** Check Youtube Video Quality ********/
	public static function is_youtube_hd($quality)
	{
		if ($quality == 'hd') {
			return TRUE;
		}else{
			return FALSE;
		}

	}

	/*********** Youtube video Cover ******************/
	public static function youtube_cover($id)
	{
		return "http://img.youtube.com/vi/".$id."/0.jpg";
	}


	/************** Vimeo Video Cover ****************/
	public static function vimeo_cover($id)
	{
		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));

		return $hash[0]['thumbnail_large'];
	}

	/************** Dailymotion Video Cover **************/
	public static function dailymotion_cover($id)
	{
		$thumbnail_medium_url = 'https://api.dailymotion.com/video/'.$id.'?fields=thumbnail_480_url';

		$json_thumbnail = file_get_contents($thumbnail_medium_url);

		$get_thumbnail = json_decode($json_thumbnail, TRUE);

		return $get_thumbnail['thumbnail_480_url'];
	}


	/************* Secure Uploaded Video *************/
	public static function secure_video($video)
	{
		// Require GetID3 Library
		require_once app_path()."/Helper/getid3/getid3/getid3.php";

		// Get Video File To ANALYSE
		$videoFile = $video;
		$getId3    = new \GetID3();
		$analyze   = $getId3
		             ->analyze($videoFile);	             
		// Allowed Video Extensions
		$extensions = array('mp4', 'avi', 'webm', 'flv', 'mov', 'wmv', 'mpg', 'mpeg', '3gp');

		if (isset($analyze['fileformat'])) {

			if (!in_array($analyze['fileformat'], $extensions)) {
				// Invalid Extension
				return FALSE;;
			}
		}else{
			// Invalid File
			return FALSE;;
		}

		// Check Video Size
		if ( (!isset($analyze['filesize'])) OR (Self::good_video_size($analyze['filesize']) == FALSE ) ) {
			return FALSE;
		}

		// Check Video Duration
		if ( (!isset($analyze['playtime_seconds'])) OR (Self::video_duration($analyze['playtime_seconds']) == FALSE ) ) {
			return FALSE;
		}

		// Send Video Data To save in Database
		$data = array(
			'video_type'     => $analyze['fileformat'],
			'video_size'     => $analyze['filesize'],
			'video_duration' => $analyze['playtime_string'],
			'video_width'    => $analyze['video']['resolution_x'],
			'video_height'   => $analyze['video']['resolution_y'],
		);

		return $data;
	}


	/*************** Get Movie Info ***************/
	public static function imdb_info($id)
	{
		// Require IMDB Library
		require_once app_path()."/Helper/imdb/Movie.php";

		// Get Video File To ANALYSE
		$movie    = new \Movie($id);

		if ($movie->title) {
			return $movie;
		}else{
			return FALSE;
		}
	}

	/**************** Check if Movies Section Active *******************/
	public static function is_movies()
	{
		$settings_general = \DB::table('settings_general')->where('id', 1)->first();

    	if ($settings_general->movies_section == 0) {
    		return TRUE;
    	}else{
    		return FALSE;
    	}
	}


	/******************* Get Mime Type from Extension ****************/
	public static function get_mime($type)
	{
		// our list of mime types
        $mime_types = array(
            "mp4"  => "video/mp4",
            "avi"  => "video/x-msvideo",
            "webm" => "video/webm",
            "flv"  => "video/x-flv",
            "mov"  => "video/quicktime",
            "wmv"  => "video/x-ms-wmv",
            "mpg"  => "video/mpeg",
            "mpeg" => "video/mpeg",
            "3gp"  => "video/3gpp",
        );

        return $mime_types[$type];
	}


	/************* Generate a Random String for unique Video *************/
	public static function random_string()
	{
		// Random String of 10 Characters
		return substr(strtoupper(md5(uniqid().rand().microtime())), 0, 10);
	}


	/******************* Movie Poster ******************/
	public static function imdb_poster($poster, $movie_id)
	{
		// Get Movies Path 
		$movie_path   = public_path().'/../../content/uploads/movies/'.$movie_id;

		// Create Folder to save video
		$new_folder = mkdir($movie_path, 0777);

		// Move Image to new folder
		$output = $movie_path.'/poster.jpg';

		file_put_contents($output, file_get_contents($poster));

		$poster_url = url('/')."/content/uploads/movies/".$movie_id."/poster.jpg";

		return $poster_url;

	}
	/************* Move Video *************/
	public static function move_video($video, $random, $extension,$vid_path = '')
	{
		// Get Videos Path 
		//$vid_path   = public_path().'/../../content/uploads/videos/'.$random;
		if(empty($vid_path))
		  $vid_path   = public_path().'/uploads/video/'.$random;
			
		// Create Folder to save video
		$new_folder = mkdir($vid_path, 0777);

		// Move Uploaded File
		$move_vid   = $video->move($vid_path, "video.".$extension);

		return $move_vid;

	}

	/************* Check video status *************/
	public static function video_status()
	{
		// Check if Admin Allow Auto Approve Videos or not
		$settings_upload = \DB::table('settings_upload')
						   ->where('id', 1)->first();

		// Check if Admin Login
		// if ((\Auth::user()->id == 1) AND (\Auth::user()->level == 1)) {
		// 	$status = 1;
		// 	return $status;
		// }

		// Else Check Settings
		if ($settings_upload->auto_approve_videos == 0) {

			$status = 1;

		}else{

			$status = 0;

		}

		return $status;

	} 


	/************* Check video status *************/
	public static function video_preview_thumb($image, $random, $img_ext)
	{
		// Get Path to Save Preview  $vid_path   = public_path()."\uploads\video/".$random;
		$preview_path = public_path().'/uploads/video/images/preview/'.$random;

		$thumb_path   = public_path().'/uploads/video/images/thumbnail/'.$random;

		// Create Folder to save Preview Image
		$preview_folder = mkdir($preview_path, 0777);

		// Create Folder to save Thumbnail Image
		$thumb_folder   = mkdir($thumb_path, 0777);

		// create Preview & Thumb URLs
		$preview_url = $preview_path."/preview.".$img_ext;
		$thumb_url   = $thumb_path."/thumbnail.".$img_ext;

		// Save Preview
		\Image::make($image->getRealPath())->resize(1280, 720)->save($preview_url);

		// Save Thumbnail
		\Image::make($image->getRealPath())->resize(480, 270)->save($thumb_url);
	}


	/************ Change Video Preview ***********/
	public static function change_video_preview($vid, $img, $img_ext)
	{
		// Get Path to Save Preview
		$preview_path = public_path().'/../../content/uploads/images/preview/'.$vid;

		$thumb_path   = public_path().'/../../content/uploads/images/thumbnail/'.$vid;

		// Delete Old Preview
		$deletePreview = glob($preview_path.'/*');
		foreach($deletePreview as $delprev){ 
		  	if(is_file($delprev)){
		  		unlink($delprev); 
		  	}
		}

		// Delete Old Preview
		$deleteThumb = glob($thumb_path.'/*');
		foreach($deleteThumb as $delthumb){ 
		  	if(is_file($delthumb)){
		  		unlink($delthumb); 
		  	}
		}

		// create Preview & Thumb URLs
		$preview_url = $preview_path."/preview.".$img_ext;
		$thumb_url   = $thumb_path."/thumbnail.".$img_ext;

		// Save Thumbnail
		\Image::make($img->getRealPath())->resize(480, 270)->save($thumb_url);

		// Save Preview
		\Image::make($img->getRealPath())->resize(1280, 720)->save($preview_url);

		
	}


	/************ Delete Video from Uploads Folder ***********/
	public static function delete_video($vid, $ext)
	{
		// Check video
		$video = \DB::table('videos')->where('video_id', $vid)->first();

		if (($video->youtube_id != null) OR ($video->vimeo_id != null) OR ($video->dailymotion_id != null)) {

			\DB::table('videos')->where('video_id', $vid)->delete();
			return;
			
		}else{

			// Get Path to Video
			// $video_path = public_path().'/../../content/uploads/videos/'.$vid.'/video.'.$ext;
			$video_path = public_path().'/uploads/video/'.$vid.'/video.'.$ext; //changed the video path

			// Delete Video Folder
			// $video_dir = public_path().'/../../content/uploads/videos/'.$vid;
			$video_dir = public_path().'/uploads/video/'.$vid; //changed the video path


			unlink($video_path);

			rmdir($video_dir);

		}
	}


	/************ Count User Videos **************/
	public static function count_user_videos($id)
	{
		// Count User Videos
		$videos = \DB::table('videos')->where('user_id', $id)->count();

		return number_format($videos);
	}


	/************ Get Video Details ************/
	public static function video_details($vid)
	{
		$video = \DB::table('videos')->where('video_id', $vid)->first();

		return $video;
	}


	/************ Count User Subscribers **************/
	public static function count_user_subscribers($user)
	{
		// Count User Subscribers
		$subscribers = \DB::table('subscribers')->where('channel_id', $user)->count();

		return number_format($subscribers);
	}


	/************ Count User Total Videos views **************/
	public static function count_user_views($user_id)
	{
		// Get User Videos
		$views = \DB::table('video_views')->where('owner', $user_id)->count();

		return number_format($views);
	}



	/************* Convert From Byte to MB *************/
	public static function covert_to_bytes($miga)
	{
		// Convert from bit to Gigabyte
		$bytes = $miga * 1048576;

		return $bytes;
	}


	/************* Convert From Bit to GB *************/
	public static function covert_to_giga($bits)
	{
		// Convert from bit to Gigabyte
		$giga = $bits * 0.000000001;

		return number_format($giga, 2);
	}


	/************* Convert From Bit to GB *************/
	public static function covert_to_miga($bits)
	{
		// Convert from bit to Gigabyte
		$miga = $bits * 0.000001;

		return number_format($miga, 2);
	}



	/************* Get per cent Storage of User *************/
	public static function storage_percent($allowed, $user_storage)
	{
		// Covert Storage to GB
		$storage_giga = Self::covert_to_giga($user_storage);

		$percent = (number_format($storage_giga, 2) * 100) / $allowed;

		return $percent;
	}


	/**************** Make Links Clickable *******************/
	public static function active_links($text, $vid){

		// Encode Video ID
		$from = base64_encode($vid);

		// Check if Short Links Active
		$plugin_redirect = \DB::table('plugin_redirect')->where('id', 1)->first();

		if ($plugin_redirect->is_active == 0) {

			if (Self::is_vip()) {
				return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $text);
			}else{

		    	return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="'.url('/redirect?url=').'$1'.'&from='.$from.'" target="_blank">$1</a>', $text); 
			}

		}else{

			return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $text);

		}
	    
	}


	/************ Convert Icon text to images in comments ************/
	public static function emoticons($text) 
	{
	    $icons = array(
	            ':)'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/smile.png">',
	            ':D'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/smile.png">',
	            '8|'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/cool.png">',
	            ':\('    =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/cry.png">',
	            '(love)'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/heart.png">',
	            ';)'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/wink.png">',
	            ':poop:'  =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/shit.png">',
	            ';p'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/stuck-out.png">',
	            ':-@'     =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/angry.png">',
	            '(F)'     =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/flower.png">',
	            '(f)'     =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/flower.png">',
	            '$'       =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/money.png">',
	            '(think)' =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/think.png">',
	            ':o'      =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/wow.png">',
	            '(sleep)' =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/sleep.png">',
	            '(sweat)' =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/worry.png">',
	            '=))'     =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/laugh.png">',
	            '(mm)'    =>  '<img class="emoticon" src="'.url('/').'/content/assets/img/smiles/smirk.png">',
	    );
	 	foreach($icons as $icon => $image) {
	      $text = str_replace($icon,$image,$text);
	 	}

		 return $text;
	}


	/************* Get Visitor Ip *************/
	public static function get_visitorIP() 
	{
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}


	/************* Get Visitor Country & City *************/
	public static function get_country($ip)
	{
		$ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));    

	    if($ip_data && $ip_data->geoplugin_countryName != null){
	    	$country_code =  $ip_data->geoplugin_countryCode;
			$country_name =  $ip_data->geoplugin_countryName;
			$city         =  $ip_data->geoplugin_city;
	    }else{
	    	$country_code =  "unknown";
			$country_name =  "unknown";
			$city         =  "unknown";
	    }

	    return array($country_code, $country_name, $city);
	}


	/************* Get Visitor Browser *************/
	public static function get_browser() 
	{
		// Get User agent
		$user_agent = env('HTTP_USER_AGENT');

		// If Browser Uknown
		$browser        =   "Unknown Browser";

		// Most Popular Browser List
		$browser_array  =   array(
       		'/msie/i'       =>  'Internet Explorer',
       		'/firefox/i'    =>  'Firefox',
       		'/safari/i'     =>  'Safari',
       		'/chrome/i'     =>  'Chrome',
            '/opera/i'      =>  'Opera',
            '/netscape/i'   =>  'Netscape',
            '/maxthon/i'    =>  'Maxthon',
            '/konqueror/i'  =>  'Konqueror',
            '/mobile/i'     =>  'Handheld Browser'
	    );

	    // If preg match browser the array content
		foreach ($browser_array as $regex => $value) {
	  		if (preg_match($regex, $user_agent)) {
	     		$browser    =   $value;
	  		}
		}

		return $browser;
	}


	/************* Get Visitor Platform *************/
	public static function get_platform()
	{
		// Get User agent
		$user_agent = env('HTTP_USER_AGENT');

		// If Platform Uknown
		$os_platform    =   "Unknown OS Platform";

		// List of Platforms in array
		$os_array       =   array(
            '/windows nt 10/i'     =>  'Windows 10',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.3/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/macintosh|mac os x/i' =>  'Mac OS',
            '/mac_powerpc/i'        =>  'Mac OS',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/android/i'            =>  'Android',
            '/webos/i'              =>  'Mobile'
	    );

		// If Preg match platfomr
		foreach ($os_array as $regex => $value) {

	 		if (preg_match($regex, $user_agent)) {
	    		$os_platform    =   $value;
	 		}

		}

		return $os_platform;
	}


	/************* Check if browse from Mobile or Computer *************/
	public static function is_mobile()
	{

		// Get User agent
		$user_agent = env('HTTP_USER_AGENT');

		$mobiles = array ( 
			'/iphone/i'     => 'iPhone', 
	        '/ipod/i'       => 'iPod', 
	        '/ipad/i'       => 'iPad', 
	        '/android/i'    => 'Android', 
	        '/blackberry/i' => 'BlackBerry', 
	        '/webos/i'      => 'Mobile'
		); 

		// If preg mach device
		foreach($mobiles as $MobileKey => $MobileOS){
        	if(preg_match($MobileKey, $user_agent)){
            	return 1;
        	}
    	}
    	return 0;
	}


	/************* Get username by user id *************/
	public static function username_by_id($id,$sUserType)
	{
		// Check user
		if($sUserType == 'model'){
			$user   = \DB::table('tb_models')->where('id', $id)->first();
		}elseif($sUserType == 'agent'){
			$user   = \DB::table('tb_agency')->where('id', $id)->first();
		}elseif($sUserType == 'admin'){
			$user   = \DB::table('tb_users')->where('id', $id)->first();
		}	

		if(empty($user))
			return '';

		return $user->username;
	}

	/************* Get user link *************/
	public static function get_user_profile_link($id,$sUserType,$Name)
	{
	  $url_id = \SiteHelpers::getEncodeUrlID($id,$Name); 
 	  $username_str =str_replace(' ','',$Name);
		$href = '';

		// Check user
		if($sUserType == 'model'){
			$href   = url('/model-profile/'.$username_str.'/'.$url_id);
		}elseif($sUserType == 'agent'){
			$href   = url('agent-profile/'.$username_str.'/'.$url_id);
		}	

		return $href;
	}	
		/************* Get user link Old *************/

	public static function get_user_profile_links($id,$sUserType)
	{
	  
		$href = '';

		// Check user
		if($sUserType == 'model'){

			$href   = url('/model-profile/'.$sUserType.'/'.$id);
		}elseif($sUserType == 'agent'){
			$href   = url('agent-profile/'.$sUserType.'/'.$id);
		}	

		return $href;
	}	


	/************* Get username by user id *************/
	public static function channel_by_id($id,$sUserType)
	{
		// Check user
		if($sUserType == 'model'){
			$user   = \DB::table('tb_models')->where('id', $id)->first();
			if(empty($user))
			return '';
			return $user->firstname;
		}elseif($sUserType == 'agent'){
			$user   = \DB::table('tb_agency')->where('id', $id)->first();
			if(empty($user))
			return '';

			return $user->username;
		}elseif($sUserType == 'admin'){
			$user   = \DB::table('tb_users')->where('id', $id)->first();
		}	

		if(empty($user))
			return '';

		return $user->username;
	}


	/************* Check if user registed or not *************/
	public static function is_registed($username)
	{
		// Check user
		$user = \DB::table('users')->where('username', $username)->first();

		if ($user) {
			return TRUE;
		}else{
			return FALSE;
		}
	}


	/************* Get Admin Username *************/
	public static function admin_username()
	{
		
		$username = \DB::table('users')->where('id', 1)->first();

		return $username->username;
	}

	/************* Check if admin *************/
	public static function is_admin()
	{
		
		if (\Auth::check()) {
			if (\Auth::user()->level == 1 && \Auth::user()->id == 1) {
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	/************* Check if VIP User *************/
	public static function is_vip()
	{
		return;
		if (\Auth::check()) {

			// User ID
			$user_id = \Auth::user()->id;

			$user = \DB::table('users')->where('id', $user_id)->first();

			// check if enabled
			if (Self::settings_vip()->enable == 0) {
				
				if ($user->id == 1 && $user->level == 1) {
					return TRUE;
				}else{
					if ($user->is_vip == 1) {
						return TRUE;
					}else{
						return FALSE;
					}
				}

			}else{
				return FALSE;
			}

			

		}else{
			return FALSE;
		}
	}



	/************* Get user Avatar by id *************/
	public static function user_avatar($id,$sUserType)
	{
		if(empty($id))
			return '';

		$avatar = '';

		// Check user is model
		if($sUserType == 'model'){
			$user   = \DB::table('tb_models')->where('id', $id)->first();
			if(!empty($user))
			$avatar = asset('/uploads/model_profile/'.$user->profile_photo);
			else			
			$avatar = asset('/content/uploads/avatars/avatar.png');
		}elseif($sUserType == 'agent'){
			$user   = \DB::table('tb_agency')->where('id', $id)->first();
			if(!empty($user))
			$avatar = asset('uploads/agency_profile/'.$user->profile_photo);	
			else
			$avatar = asset('/content/uploads/avatars/avatar.png');		
		}elseif($sUserType == 'admin'){
			$user   = \DB::table('tb_users')->where('id', $id)->first();
			if(!empty($user))
			$avatar = asset('uploads/users/'.$user->avatar);
			// if(!is_file('uploads/users/'.$user->profile_photo))
			// $avatar = asset('/content/uploads/avatars/avatar.png');			
		}

		return $avatar;
	}


	/************* Get Site Logo *************/
	public static function get_logo()
	{
		// Check user
		$settings = \DB::table('settings_general')->where('id', 1)->first();

		$logo = $settings->logo;

		if ($logo == "logo.png") {
			$default = url('/')."/content/uploads/settings/logo.png";
			return $default;
		}else{
			return $logo;
		}
	}

	/************* Get Site Favicon *************/
	public static function get_favicon()
	{
		// Check user
		$settings = \DB::table('settings_general')->where('id', 1)->first();

		$favicon = $settings->favicon;

		if ($favicon == "favicon.png") {
			$default = url('/')."/content/uploads/settings/favicon.png";
			return $default;
		}else{
			return $favicon;
		}
	}


	/******************* Google Analytics Code **********************/
	public static function google_analytics_code()
	{
		$settings = \DB::table('settings_seo')->where('id', 1)->first();

		echo $settings->google_analytics;
	}


	/******************* Check Adsense Revenue **********************/
	public static function is_adsense()
	{
		// Check if Admin Allowed ARSS
        $settings_general = \DB::table('settings_general')->where('id', 1)->first();

        if ($settings_general->adsense_revenue == 0) {
        	return TRUE;
        }else{
        	return FALSE;
        }
	}


	/******************* Check API **********************/
	public static function is_api()
	{
		// Check if Admin Allowed ARSS
        $settings_general = \DB::table('settings_general')->where('id', 1)->first();

        if ($settings_general->is_api == 1) {
        	return TRUE;
        }else{
        	return FALSE;
        }
	}


	/************* Get Category name by id *************/
	public static function category_by_id($id)
	{
		// Check user
		$category = \DB::table('categories')->where('id', $id)->first();

		if ($category AND $category->name) {
			return $category->name;
		}else{
			return "uncategorized";
		}
		
	}


	/*************** Check if valid vido url ****************/
	public static function check_vid_url( $url )
    {
        switch( $url )
        {
    
            // check if the url begins with Per
            case ( preg_match('/dailymotion\.com\/video\/+/', $url)  ? true : false ):
            $video_type = "dailymotion";
            break;

            case ( preg_match('/vimeo\.com\/+/', $url)  ? true : false ):
            $video_type = "vimeo";
            break;

            case ( preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url)  ? true : false ):
            $video_type = "youtube";
            break;

            // if there is no match, throw exception with error message
            default: 
            $video_type = FALSE;
        }

        // return the value
        return $video_type;
    }


    /******************* Get Vimeo ID *****************/
    public static function vimeo_id($link)
    {
    	$regexstr = '~
			# Match Vimeo link and embed code
			(?:&lt;iframe [^&gt;]*src=")?		# If iframe match up to first quote of src
			(?:							# Group vimeo url
				https?:\/\/				# Either http or https
				(?:[\w]+\.)*			# Optional subdomains
				vimeo\.com				# Match vimeo.com
				(?:[\/\w]*\/videos?)?	# Optional video sub directory this handles groups links also
				\/						# Slash before Id
				([0-9]+)				# $1: VIDEO_ID is numeric
				[^\s]*					# Not a space
			)							# End group
			"?							# Match end quote if part of src
			(?:[^&gt;]*&gt;&lt;/iframe&gt;)?		# Match the end of the iframe
			(?:&lt;p&gt;.*&lt;/p&gt;)?		        # Match any title information stuff
			~ix';
		
		          
			$matches=explode("/", $link);

		
		return end($matches);
    }

    /**************** Vimeo Video Duration ***************/
    public static function vimeoVideoDuration($video_url) {

	   $video_id = (int)substr(parse_url($video_url, PHP_URL_PATH), 1);

		$matches=explode("/", $video_url);
		$video_id = end($matches);

	   $json_url = 'http://vimeo.com/api/v2/video/' . $video_id . '.xml';

	   $ch = curl_init($json_url);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   curl_setopt($ch, CURLOPT_HEADER, 0);
	   $data = curl_exec($ch);
	   curl_close($ch);
	   $data = new SimpleXmlElement($data, LIBXML_NOCDATA);

	   if (!isset($data->video->duration)) {
	       return null;
	   }

	   $duration = $data->video->duration;

	   if ($duration > 3599) {
	   		$t = round($duration);
	   		return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	   }else{
	   		$t = round($duration);
	   		return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	   }
	}

	/****************** Vimeo Video Height, Width *************/
	public static function vimeo_quality($video_url)
	{
	  
	  $video_id = (int)substr(parse_url($video_url, PHP_URL_PATH), 1);
	  $matches=explode("/", $video_url);
		$video_id = end($matches);

	   $json_url = 'http://vimeo.com/api/v2/video/' . $video_id . '.xml';

	   $ch = curl_init($json_url);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   curl_setopt($ch, CURLOPT_HEADER, 0);
	   $data = curl_exec($ch);
	   curl_close($ch);
	   $data = new SimpleXmlElement($data, LIBXML_NOCDATA);

	   if (!isset($data->video->duration)) {
	       return null;
	   }

	   $quality = array (
	   		'height' =>$data->video->height,
	   		'width'  =>$data->video->width,
	   );

	   return $quality;
	}


    /**************** Youtube Video ID *********************/
    public static function youtube_id($link)
    {
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $link, $matches);
		return $matches[1];
    }


    /**************** Youtube Video ID *********************/
    public static function dailymotion_id($link)
    {
		$id = strtok(basename($link), '_');
		$id =  strtok($id, '?');
		
		return $id;
    }


    /*************** Dailymotion Video Duration ***************/
    public static function 	dailymotionDuration($id)
    {

    	$dailymotion = file_get_contents("https://api.dailymotion.com/video/".$id."?fields=duration");

		$results  = json_decode($dailymotion, true);

		$duration =  $results['duration'];

    	if ($duration > 3599) {
	   		$t = round($duration);
	   		return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	   }else{
	   		$t = round($duration);
	   		return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	   }
    }


    /*************** Dailymotion Quality **************/
    public static function dailymotion_quality($id)
    {
    	$json = file_get_contents('https://www.dailymotion.com/services/oembed?format=json&url=https://www.dailymotion.com/embed/video/'.$id);

		$data = json_decode($json) ;
		return $data;
    }


	/************* Count Visitors of video from this country *************/
	public static function video_visitors_by_country($vid, $countryCode)
	{
		// Check user
		$visitors = \DB::table('video_views')->where('video_id', $vid)
											 ->where('country_code', $countryCode)
											 ->count();

		return $visitors;
	}


	/************* Check for New Updates *************/
	public static function check_updates()
	{
		// This is version number
		$version = "1.3s";

		// Get Existing version
		$check   = 'http://www.ezzaroual.com/version.txt';

		define('REMOTE_VERSION', $check);
		$remoteVersion = trim(file_get_contents(REMOTE_VERSION));
		if ($version < $remoteVersion) {
	    	return TRUE;
	    }else{
	    	return FALSE;
	    }
	}


	/************* Check if user already like video *************/
	public static function already_liked_photos($vid)
	{
		$u_id   = \Session::get('logged_id');
		$u_type = \Session::get('user_type');
		$like   = \DB::table('video_votes')->where('video_id', $vid)
										->where('user_id', $u_id)
										->where('user_type', $u_type)
										->where('video_like', 1)
										->first();
		if ($like) {
			return TRUE;
		}

		return FALSE;
	}


	/************* Check if user already like video *************/
	public static function already_disliked_photos($vid)
	{
		$u_id    = \Session::get('logged_id');
		$u_type  = \Session::get('user_type');
		$dislike = \DB::table('video_votes')->where('video_id', $vid)
										   ->where('user_id', $u_id)
										   ->where('user_type', $u_type)
										   ->where('video_dislike', 1)
										   ->first();
		if ($dislike) {
			return TRUE;
		}

		return FALSE;
	}



	/************* Check if user already like video *************/
	public static function already_liked($vid)
	{
		$u_id   = \Session::get('logged_id');
		$u_type = \Session::get('user_type');
		$like   = \DB::table('video_votes')->where('video_id', $vid)
										->where('user_id', $u_id)
										->where('user_type', $u_type)
										->where('video_like', 1)
										->first();
		if ($like) {
			return TRUE;
		}

		return FALSE;
	}


	/************* Check if user already like video *************/
	public static function already_disliked($vid)
	{
		$u_id    = \Session::get('logged_id');
		$u_type  = \Session::get('user_type');
		$dislike = \DB::table('video_votes')->where('video_id', $vid)
										   ->where('user_id', $u_id)
										   ->where('user_type', $u_type)
										   ->where('video_dislike', 1)
										   ->first();
		if ($dislike) {
			return TRUE;
		}

		return FALSE;
	}


	/************* Get Likes per cent for style css *************/
	public static function likes_percent($likes, $dislikes)
	{
		// Count Likes and Dislikes
		$total = $likes + $dislikes;

		// If total = 0
		if ($total == 0) {
			return 0;
		}else{
			// Percent = ?
			$likes_percent = ($likes * 100) / $total;
			return $likes_percent;
		}


		
	}


	/************* Get Likes per cent for style css *************/
	public static function dislikes_percent($likes, $dislikes)
	{
		// Count Likes and Dislikes
		$total = $likes + $dislikes;

		// If total = 0
		if ($total == 0) {
			return 0;
		}else{
			// Percent = ?
			$dislikes_percent = ($dislikes * 100) / $total;

			return $dislikes_percent;
		}
		
	}


	/************* Check if User Verified or not *************/
	public static function is_verified($user_id,$sUserType)
	{
		// Check Verified
		if($sUserType == 'model'){
			$user   = \DB::table('tb_models')->where('id', $user_id)->first();
		}elseif($sUserType == 'agent'){
			$user   = \DB::table('tb_agency')->where('id', $user_id)->first();
		}elseif($sUserType == 'admin'){
			$user   = \DB::table('tb_users')->where('id', $user_id)->first();
		}
		
		if(empty($user))
		return FALSE;		

		if ($user->status == 1) {
			// Verified
			return TRUE;
		}else{
			// Not Verified
			return FALSE;
		}
	}



	/************* Count Total Subscribers *************/
	public static function count_subscribers($user_id)
	{
		// Get Username By Id
		$username = Self::username_by_id($user_id);

		// Count Subscribers
		$subscribers = \DB::table('subscribers')
					   ->where('channel_id', $username)->count();

		return $subscribers;
	}


	/************* Check if User Subscribe to a Channel *************/
	public static function is_subscribe($channel_id)
	{
		$user_id = \Auth::user()->id;
		$channel = Self::username_by_id($channel_id);

		$is_subscribe = \DB::table('subscribers')
						->where('channel_id', $channel)
						->where('user_id', $user_id)
						->first();

		if ($is_subscribe) {
			return TRUE;
		}else{
			return FALSE;
		}

	}

	/************* Count Photo Views *************/
	public static function count_photo_views($vid)
	{
		// Count Video Views
		$views = \DB::table('photo_views')->where('photo_id', $vid)->count();
		
		return number_format($views);
	}


	/************* Count Video Views *************/
	public static function count_views($vid)
	{
		// Count Video Views
		$views = \DB::table('video_views')->where('video_id', $vid)->count();
		
		return number_format($views);
	}


	/************* Check video Quality *************/
	public static function video_quality($vid)
	{
		// Get width and height of video
		$quality = \DB::table('videos')->where('video_id', $vid)->first();
		
		// Check Quality
		if ((($quality->width) >= 1280) AND (($quality->height) >= 720)) {
			// HD Video
			return TRUE;
		}else{
			// SD Video
			return FALSE;
		}
	}


	/************* Check if video NEW *************/
	public static function is_new($vid)
	{
		// Get width and height of video
		$video = \DB::table('videos')->where('video_id', $vid)->first();
		
		$videoDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $video->created_at)->toDateString();

		$nowDate = \Carbon\Carbon::now()->toDateString();

		$date1 = new \DateTime($videoDate);
		$date2 = new \DateTime($nowDate);

		$days = $date2->diff($date1)->format("%a"); 

		if ($days > 7) {
			return FALSE;
		}else{
			return TRUE;
		}

	}



	/************* Get Categories *************/
	public static function get_categories()
	{
		// Categories 
		$categories = \DB::table('categories')->get();

		if ($categories) {
			return $categories;
		}else{
			return FALSE;
		}
	}



	/************* Get Pages *************/
	public static function get_pages()
	{
		// Categories 
		$pages = \DB::table('pages')->get();

		if ($pages) {
			return $pages;
		}else{
			return FALSE;
		}
	}

	/*********** Get Videos Browsers **********/
	public static function get_browsers($vid)
	{
		// Unknown browses
		$unknown   = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Unknown Browser')->count();

		// Internet Explorer
		$ie        = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Internet Explorer')->count();

		// Mozilla Firefox
		$firefox   = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Firefox')->count();

		// Safari
		$safari    = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Safari')->count();

		// Chrome
		$chrome    = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Chrome')->count();

		// Opera
		$opera     = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Opera')->count();

		// Netscape
		$netscape  = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Netscape')->count();

		// Maxthon
		$maxthon   = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Maxthon')->count();

		// Konqueror
		$konqueror = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Konqueror')->count();

		// Handheld Browser
		$handheld  = \DB::table('video_views')->where('video_id', $vid)->where('browser', 'Handheld Browser')->count();

		// send as array
		$data = array(
			$unknown, 
			$ie, 
			$firefox, 
			$safari, 
			$chrome, 
			$opera, 
			$netscape, 
			$maxthon, 
			$konqueror, 
			$handheld, 
		);

		return $data;

	}


	/*********** Get Videos Devices **********/
	public static function get_devices($vid)
	{
		// Count Mobiles
		$mobiles   = \DB::table('video_views')->where('video_id', $vid)->where('device', 1)->count();

		// Count Comuters
		$computers  = \DB::table('video_views')->where('video_id', $vid)->where('device', 0)->count();

		// Send array
		$data = array($mobiles, $computers);
		return $data;
	}


	/*********** Get Videos Devices **********/
	public static function get_platforms($vid)
	{
		// Unknown OS Platform
		$unknown   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Unknown OS Platform')->count();

		// Windows 10
		$win10   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Windows 10')->count();

		// Windows 8
		$win8   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Windows 8')->count();

		// Windows 7
		$win7   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Windows 7')->count();

		// Windows XP
		$winxp   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Windows XP')->count();

		// Mac OS
		$mac   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Mac OS')->count();

		// Linux
		$linux   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Linux')->count();

		// Ubuntu
		$ubuntu   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Ubuntu')->count();

		// iPhone
		$iphone   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'iPhone')->count();

		// Android
		$android   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Android')->count();

		// Mobile
		$mobile   = \DB::table('video_views')->where('video_id', $vid)->where('platform', 'Mobile')->count();

		// Send array
		$data = array(
			$unknown,
			$win10,
			$win8,
			$win7,
			$winxp,
			$mac,
			$linux,
			$ubuntu,
			$iphone,
			$android,
			$mobile,
		);

		return $data;
	}


	/******** Get a website title ***********/
	public static function fetch_title($url)
	{
	    if (!preg_match('~^https?://~i', $url))
	    {
	        trigger_error('Invalid URL given in ' . __FUNCTION__, E_USER_NOTICE);
	        return false;
	    }

	    if (!$content = @file_get_contents($url))
	    {
	        return "Untitled Page";
	    }
	    else if (!preg_match('~<title>(.*?)</title>~si', $content, $title))
	    {
	        return "Untitled Page";
	    }

	    return $title[1];
	}


	/************ Count Unread Messages ************/
	public static function count_unread_messages()
	{
		// Get User ID
		$user_id = \Auth::user()->id;

		// Get Unreaded Messages 
		$unread_messages = \DB::table('messages')->where('msg_to', $user_id)->where('is_read', 0)->count();

		return $unread_messages;
	}


	/************ Count Unread Notifications ************/
	public static function count_unread_notifications()
	{
		// Get User ID
		$username = \Auth::user()->username;

		// Get Unreaded Notifications 
		$unread_notifications = \DB::table('notifications')->where('notice_to', $username)->where('is_read', 0)->count();

		return number_format($unread_notifications);
	}


	/************ Notification Type ************/
	public static function notification_type($type)
	{
		// Check Type
		switch ($type) {
			case 'subscribe':
				return '<span class="notice_type dark_color">New Subscribe</span>';
				break;

			case 'like':
				return '<span class="notice_type green_color">New Like</span>';
				break;

			case 'dislike':
				return '<span class="notice_type red_color">New Dislike</span>';
				break;

			case 'comment':
				return '<span class="notice_type blue_color">New Comment</span>';
				break;

			case 'video':
				return '<span class="notice_type yellow_color">New Video Uploaded</span>';
				break;

			case 'report':
				return '<span class="notice_type pink_color">Report</span>';
				break;

			case 'wait':
				return '<span class="notice_type marin_color">Waiting Approve</span>';
				break;

			case 'payment_accepted':
				return '<span class="notice_type pay_accept_color">Payment Accepted</span>';
				break;

			case 'payment_canceled':
				return '<span class="notice_type pay_cancel_color">Payment Canceled</span>';
				break;
			
			default:
				return '<span class="notice_type green_color">Undefined</span>';
				break;
		}
	}

	/************ Notification Type ************/
	public static function notification_details($type, $user, $details)
	{
		// Check Type
		switch ($type) {
			case 'subscribe':
				return $user.' has been subscribed in your channel.';
				break;

			case 'like':
				return 'You have new like for <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a> video';
				break;

			case 'dislike':
				return 'You have new dislike for <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a> video';
				break;

			case 'comment':
				return 'You have new comment in <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a> video';
				break;

			case 'wait':
				return 'New video waiting approval <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a>';
				break;

			case 'video':
				return $user.' uploaded new video: <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a> video';
				break;

			case 'report':
				return $user.' Flagged this video  <a class="notice_link" href="'.url('/watch').'/'.$details.'">'.$details.'</a>';
				break;

			case 'payment_canceled':
				return 'Your payment has been canceled. Please contact us for more details.';
				break;

			case 'payment_accepted':
				return 'Congratulations! You are a vip user now.';
				break;
			
			default:
				return '<span class="notice_type green">Undefined</span>';
				break;
		}
	}

	/***************** Video Owner *******************/
	public static function get_video_row($vid,$sType)
	{	
		// Get Video
		$video = \DB::table('videos')->where($sType, $vid)->first();

		return $video;
	}


	/***************** Video Owner *******************/
	public static function video_owner($vid)
	{
		// Get Video
		$video = \DB::table('videos')->where('video_id', $vid)->first();
		$username = Self::username_by_id($video->user_id,$video->user_type);
		return $username;
	}

	/***************** Photo Owner *******************/
	public static function photo_owner($id)
	{
		// Get Video
		$sImage = \DB::table('tb_images')->where('ID', $id)->first();
		$username = Self::username_by_id($sImage->OwnerId,$sImage->user_type);
		return $username;
	}

	/***************** Notice Subscribers New Video *******************/
	public static function notice_subscribers($uid, $vid,$sType)
	{
		// Username
		$username = Self::username_by_id($uid,$sType);

		// Get Subscribers
		$subscribers = \DB::table('subscribers')->where('channel_id', $username)->get();

		foreach ($subscribers as $subscriber) {

			// Send Notifications
            \DB::table('notifications')->insert([
                'notice_to'   => Self::username_by_id($subscriber->user_id),
                'notice_from' => $username,
                'type'        => 'video',
                'details'     => $vid,
                'is_read'     => 0,
                'created_at'  => \Carbon\Carbon::now(),
                'updated_at'  => \Carbon\Carbon::now(),
            ]);

		}
	}


	/***************** Count Videos in a Categories ******************/
	public static function count_videos_by_category($cat_id)
	{
		// Count Videos
		$videos = \DB::table('videos')->where('category', $cat_id)->count();

		return number_format($videos);
	}



	/******************* Check Spam Emails *********************/
	public static function prevent_spams($email)
	{
		$spam_response = @file_get_contents("http://www.stopforumspam.com/api?email=".$email."");
		$spam_check = strpos($spam_response, "yes");

		if($spam_check !== false){
			return TRUE;
		}else{
			return FALSE;
		}

	}


	/******************* Check If username in blacklist *********************/
	public static function blacklist_username($username, $blacklist)
	{

		$bad_username = explode(',', $blacklist);

		if (in_array($username, $bad_username)) {
			return TRUE;
		}else{
			return FALSE;
		}

	}


	/******************* Post Video Tags *****************/
	public static function video_tags($vid)
	{
		// Get Video 
		$video = \DB::table('videos')->where('video_id', $vid)->first();

		// Explode Tags 
		$tags = explode(',', $video->tags);

		if(empty($tags))
			echo '<li>No More Tags.....</li>';

		if(!empty($tags)){
			foreach ($tags as $key => $tag) {
				echo '<li><a href="'.url('/video/tag').'/'.$tag.'">'.$tag.'</a></li>';
			}
		}

	}

	/******************* Post Video Tags *****************/
	public static function photo_tags($iImage)
	{
		// Get Video 
		$aImage = \DB::table('tb_images')->where('ID', $iImage)->first();

		// Explode Tags 
		$tags = explode(',', $aImage->Tags);

		if(empty($tags))
			echo '<li>No More Tags.....</li>';

		if(!empty($tags)){
			foreach ($tags as $key => $tag) {
				echo '<li><a href="'.url('/tag/photo/').'/'.$tag.'">'.$tag.'</a></li>';
			}
		}

	}

	/********************** Last Added Photos ***************************/
	public static function getLastPhoto($iAlbumId){
		
		$aImage = \DB::table('tb_images')
		        ->where('AlbumId', $iAlbumId)
		        ->orderby('ID','desc')
		        ->first();

		 if(empty($aImage))
		   return '';     

		return $aImage->ID;        
	}
	/*Last Added Active photos*/
public static function getLastedPhoto($iAlbumId){
		
		$aImage = \DB::table('tb_images')
		        ->where('AlbumId', $iAlbumId)
		        ->where('Status','Active')
		        ->orderby('ID','desc')
		        ->first();

		 if(empty($aImage))
		   return '';     

		return $aImage->ID;
		// return $aImage->Status;         
	}
	public static function getLastedExt($iAlbumId){
		
		$aImage = \DB::table('tb_images')
		        ->where('AlbumId', $iAlbumId)
		        ->where('Status','Active')
		        ->orderby('ID','desc')
		        ->first();

		 if(empty($aImage))
		   return '';     

		return $aImage->Ext;
		// return $aImage->Status;         
	}



	/************* get VIP Settings **************/
	public static function settings_vip()
	{
		$vip = \DB::table('settings_vip')->where('id', 1)->first();

		return $vip;
	}

	/************* get VIP Settings **************/
	public static function settings_upload()
	{
		$upload = \DB::table('settings_upload')->where('id', 1)->first();

		return $upload;
	}
	
	/********* check new Payments *********/
	public static function new_payments()
	{
		$payments = \DB::table('payments')->where('approve', 0)->count();

		return $payments;
	}

	public static function getPhotosByAlbum($iId,$sStatus){

		return  \DB::table('tb_images')
        ->where('status', $sStatus)
        ->where('AlbumId', '=', $iId)
        ->get();
	}

	public static function getAlbumInfo($iId,$sColumn){
		return  \DB::table('tb_albums')
		->where($sColumn, '=', $iId)
		->first();
	}

	public static function getPhotoInfo($iId,$sColumn){
		return  \DB::table('tb_images')
		->where($sColumn, '=', $iId)
		->first();
	}	

	/************** Check Video Duration with Membership Plan video duration ************/
	public static function checkVideoDuration($user_id,$user_type,$video_duration)
	{
		// Check video duration with duration limit mentioned in membership Plan
        if($user_type=='agent')
        {
        // get agency details
        $agent_details = \DB::table('tb_agency')->where('id', '=', $user_id)->where('status','=','1')->where('verification_status','=','1')->first();
        $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $agent_details->plan_id)->where('status','=','1')->first();
        
        }
        elseif ($user_type=='model') 
        {
           // get model details
        $model_details = \DB::table('tb_models')->where('id', '=', $user_id)->where('status','=','1')->where('approved_status','=','1')->first();
        $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $model_details->plan_id)->where('status','=','1')->first();
        }
        //check the duration got and change its format accordingly
        $dur2 =0;

        //  if(strpos($video_duration,":")>0)
        // {
        //     $dur1 = explode(":", $video_duration);
        //     $dur2 = implode(".", $dur1);
             
        // }
        // else
        // {
        //     $dur2=$video_duration;
        // }

        $check_duration = date('H:i:s', mktime(0,$membership_details->video_duration_limit,0));


        // compare duration with membership plan
        if(strtotime($video_duration) > strtotime($check_duration))
        {
            return FALSE;
        }
        else
        {
        	return TRUE;
        }
        //end

	}

	

}
