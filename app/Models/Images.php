<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class images extends Sximo  {
	
	protected $table = 'tb_images';
	protected $primaryKey = 'ID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_images.* FROM tb_images  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_images.ID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
