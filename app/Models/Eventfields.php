<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class eventfields extends Sximo  {
	
	protected $table = 'tb_event_fields';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_event_fields.* FROM tb_event_fields  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_event_fields.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
