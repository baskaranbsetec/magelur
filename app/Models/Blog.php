<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class blog extends Sximo  {
	
	protected $table = 'tb_blog';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_blog.* FROM tb_blog  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_blog.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
	public static function summaryCategory()
	{
		$result = \DB::table('tb_blogcategories')
					->select('tb_blogcategories.*',\DB::raw("COUNT('tb_blog.id') as blogCount"))
					->join('tb_blog', 'tb_blogcategories.cat_id', '=', 'tb_blog.id')
					->where('enable',1)
					->orderBy('tb_blogcategories.cat_id','desc')
					->groupBy('tb_blogcategories.cat_id')
					->get();
		return $result;	
	}	

}
