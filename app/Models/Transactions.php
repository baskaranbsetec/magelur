<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class transactions extends Sximo  {
	
	protected $table = 'tb_transactions';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_transactions.* FROM tb_transactions  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_transactions.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
