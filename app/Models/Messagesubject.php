<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class messagesubject extends Sximo  {
	
	protected $table = 'tb_message_subjects';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_message_subjects.* FROM tb_message_subjects  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_message_subjects.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
