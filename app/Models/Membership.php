<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class membership extends Sximo  {
	
	protected $table = 'tb_membership_plan';
	protected $primaryKey = 'plan_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_membership_plan.* FROM tb_membership_plan  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_membership_plan.plan_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
