<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class event extends Sximo  {
	
	protected $table = 'tb_events';
	protected $primaryKey = 'ID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_events.* FROM tb_events  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_events.ID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
