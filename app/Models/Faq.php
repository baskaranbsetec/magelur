<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class faq extends Sximo  {
	
	protected $table = 'tb_faq';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_faq.* FROM tb_faq  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_faq.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
