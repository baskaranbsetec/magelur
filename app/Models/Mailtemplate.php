<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class mailtemplate extends Sximo  {
	
	protected $table = 'tb_mail_template';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		return "  SELECT tb_mail_template.* FROM tb_mail_template  ";
	}	

	public static function queryWhere(  ){
				return "  WHERE tb_mail_template.id IS NOT NULL ";

	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
