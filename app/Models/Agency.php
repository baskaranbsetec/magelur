<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class agency extends Sximo  {
	
	protected $table = 'tb_agency';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_agency.* FROM tb_agency  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_agency.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	public static function agentquery(){
		return "  ";
	}
	

	

}
