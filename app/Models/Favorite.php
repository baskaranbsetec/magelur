<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Sximo  {
	
	protected $table = 'tb_favorites';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

}
