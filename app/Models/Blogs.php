<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class blogs extends Sximo  {
	
	protected $table = 'tb_blogs';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
	}


	public static function getRows( $args )
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;
	   
        extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> 1	  
        ), $args ));
		
		$offset = ($page-1) * $limit ;	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';	
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = with(new static)->table;
		if($global == 0 )
				$params .= " AND {$table}.entry_by ='".\Session::get('uid')."'"; 	
		// End Update permission global / own access new ver 1.1			
			// echo  self::querySelect() . self::queryWhere(). " 
			// {$params} ". self::queryGroup() ." {$orderConditional}  {$limitConditional} "; exit;
		$rows = array();
	    $result = \DB::select( self::querySelect() . self::queryWhere(). " 
				{$params} ". self::queryGroup() ." {$orderConditional}  {$limitConditional} ");
		
		if($key =='' ) { $key ='*'; } else { $key = $table.".".$key ; }	
		$total = \DB::select( self::querySelect() . self::queryWhere(). " 
				{$params} ". self::queryGroup() ." {$orderConditional}  ");
		$total = count($total);

//		$total = $res[0]->total;


		return $results = array('rows'=> $result , 'total' => $total);	

	
	}		

	public static function querySelect(  ){
		
		return " 
		SELECT 
		tb_blogs.* , COUNT(commentID) AS comments ,tb_users.username
		FROM tb_blogs 
		LEFT JOIN tb_comments ON tb_comments.pageID = tb_blogs.id
		LEFT JOIN tb_users ON tb_users.id = tb_blogs.userid
		";
	}	

	public static function queryWhere(  ){
		
		return " WHERE tb_blogs.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return " GROUP BY tb_blogs.id ";
	}
	

	public static function comments( $postID)
	{
		$sql = \DB::select("
			SELECT * 
			FROM tb_comments 
			WHERE 1 AND `type` = 'blogs' AND `pageID` = $postID
			");

		foreach ($sql as $key => $val) {
			// if no user type defined , skip the loop
			if(empty($val->userType))
				continue;
			//  get user detail of model
			if($val->userType == 'model'){

			   $aUser =	\DB::table('tb_models')
				->select('firstname', 'email','profile_photo','city','country')
				->where('id',$val->userID)
				->first();
			}elseif($val->userType == 'agent'){
				//  get user detail of agent
			   $aUser =	\DB::table('tb_agency')
				->select('username', 'email','profile_photo','city','country')
				->where('id',$val->userID)
				->first();
			}elseif($val->userType == 'user'){
				//  get user detail of user
			   $aUser =	\DB::table('tb_users')
				->select('username', 'email','avatar as profile_photo')
				->where('id',$val->userID)
				->first();	
			}
			//  convert object to array
			$aUser = (array)$aUser; 

			if(empty($aUser))
				continue;
			if($val->userType == 'model'){
				$sql[$key]->username      = isset($aUser['firstname'])?$aUser['firstname']:'';
			}
			else{
				$sql[$key]->username      = isset($aUser['username'])?$aUser['username']:'';
			}
			// $sql[$key]->username      = isset($aUser['username'])?$aUser['username']:'';
			$sql[$key]->email         = isset($aUser['email'])?$aUser['email']:'';
			$sql[$key]->avatar        = isset($aUser['profile_photo'])?$aUser['profile_photo']:'';
			$sql[$key]->city          = isset($aUser['city'])?$aUser['city']:'';
			$sql[$key]->country       = isset($aUser['country'])?$aUser['country']:'';
			 
			}
		return $sql;
	}


	public static function latestposts( )
	{
		$sql = \DB::select("
			SELECT * FROM tb_blogs WHERE 1 ORDER BY created DESC LIMIT 5
			");
		return $sql;
	}

}
