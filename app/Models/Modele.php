<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class modele extends Sximo  {
	
	protected $table = 'tb_models';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_models.* FROM tb_models  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_models.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public function inserImage($iUserId,$sUserType,$sTitle,$sDesc,$sTags,$sExt,$sType,$sStatus,$iAlbumId,$iUserName){
		// create slug for make it unique
		$sSlug = \SiteHelpers::genrate_slug($sTitle,'tb_images','MediaUri');

		$id = \DB::table('tb_images')->insertGetId(
			[
			'OwnerId'  => $iUserId,
			'ownername'=> $iUserName,
			'user_type'=> $sUserType, 
			'AlbumId'  => $iAlbumId,	
			'MediaUri' => $sSlug, 
			'Title'    => $sTitle,
			'Description' => $sDesc,
			'Tags'     => $sTags,
			'Ext'      => $sExt,
			'Type'     => $sType,
			'Status'   => $sStatus,
			'Created'  => date('Y-m-d H:i:s'),
			]
		);
		return $id;
	}

	public function insertAlbum($sName,$sDesc,$iUserId,$sType){
		// create slug for make it unique
		$sSlug = \SiteHelpers::genrate_slug($sName,'tb_albums','uri');
		$id = \DB::table('tb_albums')->insertGetId(
			[
				'title'       => $sName,	
				'uri'         => $sSlug, 
				'description' => $sDesc,
				'user_type'   => $sType,
				'ownerid'     => $iUserId,
				'status'      => 'active',
				'created'     => date('Y-m-d H:i:s'),
			]
		);
		return $id;
	}

	function getModelInfo($iUserId){
		return $this->where('id',$iUserId)->where('status','1')->where('approved_status','1')->get();
	}

	public function getModelImages($iModelId,$args){
		//  prepare limit
        extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> 1	  
        ), $args ));		
        $offset = ($page-1) * $limit ;	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';	

		// get selected results
		$result = \DB::select( "SELECT * FROM `tb_images` WHERE 1 $limitConditional " );

		$total  = count(\DB::select( "SELECT * FROM `tb_images` WHERE 1 " ));

		return $results = array('rows'=> $result , 'total' => $total);	

	}

	public function updateMediaCount($iUserId,$sCount,$sColumn){
	   return \DB::table('tb_models')
	   ->where('id', $iUserId)
	   ->increment($sColumn, $sCount);
	}



	

}
