<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class subscriber extends Sximo  {
	
	protected $table = 'tb_subscriber';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
				return "  SELECT tb_subscriber.* FROM tb_subscriber  ";

	}	

	public static function queryWhere(  ){
				return "  WHERE tb_subscriber.id IS NOT NULL ";

	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
