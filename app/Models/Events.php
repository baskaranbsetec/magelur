<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class events extends Sximo  {
	
	protected $table = 'tb_events';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}
 
	public static function querySelect(  ){
		
		return "  SELECT tb_events.* FROM tb_events  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_events.id IS NOT NULL  ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

	public static function comments( $postID)
	{
		$sql = \DB::select("
			SELECT * 
			FROM tb_comments 
			WHERE 1 AND `type` = 'events' AND `pageID` = $postID
			");

		foreach ($sql as $key => $val) {
			// if no user type defined , skip the loop
			if(empty($val->userType))
				continue;
			//  get user detail of model
			if($val->userType == 'model'){

			   $aUser =	\DB::table('tb_models')
				->select('firstname', 'email','profile_photo','city','country')
				->where('id',$val->userID)
				->first();
			}elseif($val->userType == 'agent'){
				//  get user detail of agent
			   $aUser =	\DB::table('tb_agency')
				->select('username', 'email','profile_photo','city','country')
				->where('id',$val->userID)
				->first();
			}elseif($val->userType == 'user'){
				//  get user detail of user
			   $aUser =	\DB::table('tb_users')
				->select('username', 'email','avatar as profile_photo')
				->where('id',$val->userID)
				->first();	
			}
			//  convert object to array
			$aUser = (array)$aUser; 

			if(empty($aUser))
				continue;
			if($val->userType == 'model'){
				$sql[$key]->username      = isset($aUser['firstname'])?$aUser['firstname']:'';
			}
			else{
				$sql[$key]->username      = isset($aUser['username'])?$aUser['username']:'';
			}
			$sql[$key]->email         = isset($aUser['email'])?$aUser['email']:'';
			$sql[$key]->avatar        = isset($aUser['profile_photo'])?$aUser['profile_photo']:'';
			$sql[$key]->city          = isset($aUser['city'])?$aUser['city']:'';
			$sql[$key]->country       = isset($aUser['country'])?$aUser['country']:'';
			 
			}
		return $sql;
	}

	public static function getRecentPost($sWhere , $sLimit )
	{
		if(\Auth::check()){
			$sql = \DB::select("
				SELECT * FROM tb_events WHERE 1 AND `status` = 'Active'  $sWhere ORDER BY created DESC $sLimit
				");
		}
		elseif(\SiteHelpers::checkLogged() == ''){
			$sql = \DB::select("
				SELECT * FROM tb_events WHERE 1 AND `status` = 'Active' AND `publishID` = 'public' AND `startdate` >= CURDATE() ORDER BY created DESC LIMIT 3
				");

		}
		elseif(\SiteHelpers::checkLogged() == 1){

			$sql = \DB::select("
				SELECT * FROM tb_events WHERE 1  AND publishID !='onlyme' AND status ='active' $sWhere ORDER BY created DESC $sLimit
				");
		}
		return $sql;
	}	



	public static function latestposts( )
	{
		$sql = \DB::select("
			SELECT * FROM tb_events WHERE 1 AND `status` = 'Active' ORDER BY created DESC LIMIT 5
			");
		return $sql;
	}	
public static function checks( )
	{

		$table = with(new static)->table;
		if(\Auth::check()){
			$params = " AND {$table}.publishID ='public' AND {$table}.status ='Active' OR {$table}.publishID ='registered' OR {$table}.publishID ='onlyme' " ; 
		}
		elseif(\SiteHelpers::checkLogged() == ''){
			$params = " AND {$table}.publishID ='public' AND {$table}.status ='Active'" ; 
		}
		elseif(\SiteHelpers::checkLogged() == 1){
			$params = " AND {$table}.publishID ='public' AND {$table}.status ='Active' OR {$table}.publishID ='registered'  " ; 
		}

		$result = \DB::select( self::querySelect() . self::queryWhere(). " 
			{$params} ". self::queryGroup());
// echo "<pre>";print_r($result);exit;
		return $result;
	}
}
