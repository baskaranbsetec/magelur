<?php namespace App\Http\Controllers\magelur;

use App\Http\Controllers\controller;
use App\Models\Core\Groups;
use App\User;
use Illuminate\Http\Request;
use Validator, Input, Redirect; 
use App\Models\Sximo;
class ConfigController extends Controller {

    public function __construct()
    {
    	parent::__construct();
		if( \Auth::check() or \Session::get('gid') != '1')
		{
		//	echo 'redirect';
			return Redirect::to('dashboard');
		};
        $this->data = array(
            'pageTitle' =>  'General Setting',
            'pageNote'  =>  'Configuration Site',
            
        );
        $this->option = new Sximo();			
       
    }

	public function getIndex()
	{	
        $this->data = array(
            'pageTitle' =>  'General Setting',
            'pageNote'  =>  'Configuration Site',
            
        );			
		$this->data['active'] = '';
		return view('magelur.config.index',$this->data);	
	}


	static function postSave( Request $request )
	{
		
		$rules = array(
			'cnf_appname'=>'required|min:2',
			'cnf_appdesc'=>'required|min:2',
			// 'cnf_comname'=>'required|min:2',
			// 'cnf_email'=>'required|email',
		);
		$validator = Validator::make($request->all(), $rules);	
		if (!$validator->fails()) 
		{
			$logo = '';
			if(!is_null(Input::file('logo')))
			{

				$file = Input::file('logo'); 
			 	$destinationPath = public_path().'/magelur/images/'; 
				$filename = $file->getClientOriginalName();
				$extension =$file->getClientOriginalExtension(); //if you need extension of the file
				$logo = 'backend-logo.'.$extension;
				$uploadSuccess = $file->move($destinationPath, $logo);
			}
			$flogo='';
			if(!is_null(Input::file('flogo')))
			{

				$file = Input::file('flogo'); 
			 	$destinationPath = public_path().'/magelur/images/'; 
				$filename = $file->getClientOriginalName();
				$extension =$file->getClientOriginalExtension(); //if you need extension of the file
				$flogo = 'frontend-logo.'.$extension;
				$uploadSuccess = $file->move($destinationPath, $flogo);
			}
			$fav_icon='';
			if(!is_null(Input::file('fav_icon')))
			{

				$file = Input::file('fav_icon'); 
			 	$destinationPath = public_path().'/uploads/images/'; 
				$filename = $file->getClientOriginalName();
				$extension =$file->getClientOriginalExtension(); //if you need extension of the file
				$fav_icon = 'fav-logo.'.$extension;
				$uploadSuccess = $file->move($destinationPath, $fav_icon);
			}


			$val  =		"<?php \n"; 
			$val .= 	"define('CNF_APPNAME','".$request->input('cnf_appname')."');\n";
			$val .= 	"define('CNF_APPDESC','".$request->input('cnf_appdesc')."');\n";
			$val .= 	"define('CNF_COMNAME','".$request->input('cnf_comname')."');\n";
			$val .= 	"define('CNF_EMAIL','".$request->input('cnf_email')."');\n";	
			$val .= 	"define('CNF_METAKEY','".$request->input('cnf_metakey')."');\n";	
			$val .= 	"define('CNF_METADESC','".$request->input('cnf_metadesc')."');\n";		
			$val .= 	"define('CNF_GROUP','".CNF_GROUP."');\n";	
			$val .= 	"define('CNF_ACTIVATION','".CNF_ACTIVATION."');\n";	
			$val .= 	"define('CNF_MULTILANG','".(!is_null($request->input('cnf_multilang')) ? 1 : 0 )."');\n";
			$val .= 	"define('CNF_LANG','".$request->input('cnf_lang')."');\n";
			$val .= 	"define('CNF_REGIST','".CNF_REGIST."');\n";	
			$val .= 	"define('CNF_FRONT','".CNF_FRONT."');\n";		
			$val .= 	"define('CNF_RECAPTCHA','".CNF_RECAPTCHA."');\n";	
			$val .= 	"define('CNF_THEME','".$request->input('cnf_theme')."');\n";		
			$val .= 	"define('CNF_RECAPTCHAPUBLICKEY','".CNF_RECAPTCHAPUBLICKEY."');\n";
			$val .= 	"define('CNF_RECAPTCHAPRIVATEKEY','".CNF_RECAPTCHAPRIVATEKEY."');\n";
			$val .= 	"define('CNF_MODE','".(!is_null($request->input('cnf_mode')) ? 'production' : 'development' )."');\n";	
			$val .= 	"define('CNF_LOGO','".($logo !=''  ? $logo : CNF_LOGO )."');\n";		
			$val .= 	"define('CNF_FLOGO','".($flogo !=''  ? $flogo : CNF_FLOGO )."');\n";
			$val .= 	"define('CNF_FAV','".($fav_icon !=''  ? $fav_icon : CNF_FAV )."');\n";		
			$val .= 	"define('CNF_ALLOWIP','".CNF_ALLOWIP."');\n";
			$val .= 	"define('CNF_RESTRICIP','".CNF_RESTRICIP."');\n";	
			$val .= 	"define('CNF_MAIL','".(defined('CNF_MAIL') ? CNF_MAIL:'phpmail')."');\n";	
			$val .= 	"define('CNF_DATE','".(!is_null($request->input('cnf_date')) ? $request->input('cnf_date') : 'Y-m-d' )."');\n";								
			$val .= 	"define('CNF_ALBUM','".$request->input('cnf_album')."');\n";
			$val .= 	"?>";
	
			$filename = base_path().'/setting.php';
			$fp=fopen($filename,"w+"); 
			fwrite($fp,$val); 
			fclose($fp); 
			return Redirect::to('magelur/config')->with('messagetext','Setting Has Been Save Successful')->with('msgstatus','success');
		} else {
			return Redirect::to('magelur/config')->with('messagetext', 'The following errors occurred')->with('msgstatus','success')
			->withErrors($validator)->withInput();
		}			
	
	}




	public function getEmail()
	{
		
		$regEmail = base_path()."/resources/views/user/emails/registration.blade.php";
		$resetEmail = base_path()."/resources/views/user/emails/auth/reminder.blade.php";
		/*contactus*/
		$contactusEmail = base_path()."/resources/views/user/emails/contactus.blade.php";
		/*Tell a friend*/
		$invite_friend = base_path()."/resources/views/user/emails/invite_friend.blade.php";
		/*Feedback*/
		$feed_back = base_path()."/resources/views/user/emails/feed_back.blade.php";
		/*Admin Email Template*/
		$email_user = base_path()."/resources/views/user/emails/email_user.blade.php";
		/*Subscriber Template*/
		$sub_email = base_path()."/resources/views/subscriber/email.blade.php";

		$this->data = array(
			'groups'	=> Groups::all(),
			'pageTitle'	=> 'Email Setting',
			'pageNote'	=> 'Email Template',
			'regEmail' 	=> file_get_contents($regEmail),
			'resetEmail'	=> 	file_get_contents($resetEmail),
			'contactusEmail' 	=> file_get_contents($contactusEmail),
			'invite_friend' 	=> file_get_contents($invite_friend),
			'feed_back' 	=> file_get_contents($feed_back),
			'email_user' 	=> file_get_contents($email_user),
			'sub_email' 	=> file_get_contents($sub_email),
			'active'		=> 'email',
		);	
		return view('magelur.config.email',$this->data);		
	
	}
	
	function postEmail( Request $request)
	{
		
		// print_r($_POST);exit;
		$rules = array(
			'regEmail'		=> 'required|min:10',
			'resetEmail'		=> 'required|min:10',
			'contactusEmail'		=> 'required|min:10',
			'invite_friend'		=> 'required|min:10',
			'feed_back'		=> 'required|min:10',
			'email_user'		=> 'required|min:10',
			'sub_email'		=> 'required|min:10',

		);	
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) 
		{
			$regEmailFile = base_path()."/resources/views/user/emails/registration.blade.php";
			$resetEmailFile = base_path()."/resources/views/user/emails/auth/reminder.blade.php";			
			
			$contactusEmailFile = base_path()."/resources/views/user/emails/contactus.blade.php";
			$invite_friend = base_path()."/resources/views/user/emails/invite_friend.blade.php";
			$feed_back = base_path()."/resources/views/user/emails/feed_back.blade.php";
			$email_user = base_path()."/resources/views/user/emails/email_user.blade.php";
			$sub_email = base_path()."/resources/views/subscriber/email.blade.php";
		
		
			$fp=fopen($regEmailFile,"w+"); 				
			fwrite($fp,$_POST['regEmail']); 
			fclose($fp);	
			
			$fp=fopen($resetEmailFile,"w+"); 				
			fwrite($fp,$_POST['resetEmail']); 
			fclose($fp);

			$fp=fopen($contactusEmailFile,"w+"); 				
			fwrite($fp,$_POST['contactusEmail']); 
			fclose($fp);
			
			$fp=fopen($invite_friend,"w+"); 				
			fwrite($fp,$_POST['invite_friend']); 
			fclose($fp);

			$fp=fopen($feed_back,"w+"); 				
			fwrite($fp,$_POST['feed_back']); 
			fclose($fp);

			$fp=fopen($email_user,"w+"); 				
			fwrite($fp,$_POST['email_user']); 
			fclose($fp);
			$fp=fopen($sub_email,"w+"); 				
			fwrite($fp,$_POST['sub_email']); 
			fclose($fp);
			
			return Redirect::to('magelur/config/email')->with('messagetext', 'Email Has Been Updated')->with('msgstatus','success');	
			
		}	else {

			return Redirect::to('magelur/config/email')->with('messagetext', 'The following errors occurred')->with('msgstatus','success')
			->withErrors($validator)->withInput();
		}
	
	}
	
	public function getSecurity()
	{
		
		$this->data = array(
			'groups'	=> Groups::all(),
			'pageTitle'	=> 'Login And Security',
			'pageNote'	=> 'Login Configuration and Setting',
			'active'	=> 'security'
			
		);
		
	
		return view('magelur.config.security',$this->data);		
	
	}	
	
		

	
	public function postLogin( Request $request)
	{

		$rules = array(

		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$val  =		"<?php \n"; 
			$val .= 	"define('CNF_APPNAME','".CNF_APPNAME."');\n";
			$val .= 	"define('CNF_APPDESC','".CNF_APPDESC."');\n";
			$val .= 	"define('CNF_COMNAME','".CNF_COMNAME."');\n";
			$val .= 	"define('CNF_EMAIL','".CNF_EMAIL."');\n";
			$val .= 	"define('CNF_METAKEY','".CNF_METAKEY."');\n";	
			$val .= 	"define('CNF_METADESC','".CNF_METADESC."');\n";					
			$val .= 	"define('CNF_GROUP','".$request->input('CNF_GROUP')."');\n";	
			$val .= 	"define('CNF_ACTIVATION','".$request->input('CNF_ACTIVATION')."');\n";	
			$val .= 	"define('CNF_MULTILANG','".CNF_MULTILANG."');\n";
			$val .= 	"define('CNF_LANG','".CNF_LANG."');\n";		
			$val .= 	"define('CNF_REGIST','".(!is_null($request->input('CNF_REGIST')) ? 'true':'false')."');\n";		
			$val .= 	"define('CNF_FRONT','".(!is_null($request->input('CNF_FRONT')) ? 'true':'false')."');\n";		
			$val .= 	"define('CNF_RECAPTCHA','".(!is_null($request->input('CNF_RECAPTCHA')) ? 'true':'false')."');\n";	
			$val .= 	"define('CNF_THEME','".CNF_THEME."');\n";	
			$val .= 	"define('CNF_RECAPTCHAPUBLICKEY','');\n";
			$val .= 	"define('CNF_RECAPTCHAPRIVATEKEY','');\n";
			$val .= 	"define('CNF_MODE','".CNF_MODE."');\n";	
			$val .= 	"define('CNF_LOGO','".CNF_LOGO."');\n";
			$val .= 	"define('CNF_FLOGO','".CNF_FLOGO."');\n";
			$val .= 	"define('CNF_FAV','".CNF_FAV."');\n";
			$val .= 	"define('CNF_ALBUM','".CNF_ALBUM."');\n";
			$val .= 	"define('CNF_ALLOWIP','".$request->input('CNF_ALLOWIP')."');\n";
			$val .= 	"define('CNF_RESTRICIP','".$request->input('CNF_RESTRICIP')."');\n";	
			$val .= 	"define('CNF_MAIL','".(!is_null($request->input('CNF_MAIL')) ? $request->input('CNF_MAIL'):'phpmail')."');\n";	
			$val .= 	"define('CNF_DATE','".(defined('CNF_DATE') ? CNF_DATE: 'Y-m-d' )."');\n";										
			$val .= 	"?>";
	
			$filename = '../setting.php';
			$fp=fopen($filename,"w+"); 
			fwrite($fp,$val); 
			fclose($fp); 
			return Redirect::to('magelur/config/security')->with('messagetext','Setting Has Been Save Successful')->with('msgstatus','success');
		} else {
			return Redirect::to('magelur/config/security')->with('messagetext', 'The following errors occurred')->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	}
	
	public function getLog( $type = null)
	{
	
		
		$this->data = array(
			'pageTitle'	=> 'Clear caches',
			'pageNote'	=> 'Remove Current Caches',
			'active'	=> 'log'
		);	
		return view('magelur.config.log',$this->data);	
	}
		
	
	public function getClearlog()
	{
		
		$dir = base_path()."/storage/logs";	
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
			{
				//removedir($file);
			} else {

				unlink($file);
			}
		}

		$dir = base_path()."/storage/framework/views";	
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
			{
				//removedir($file);
			} else {
				
				unlink($file);
			}
		}		

		return Redirect::to('magelur/config/log')->with('messagetext','Cache has been cleared !')->with('msgstatus','success');	
	}
	
	function removeDir($dir) {
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
				removedir($file);
			else
				unlink($file);
		}
		rmdir($dir);
	}
	
	public function getTranslation( Request $request, $type = null)
	{
		if(!is_null($request->input('edit')))
		{
			$file = (!is_null($request->input('file')) ? $request->input('file') : 'core.php'); 
			$files = scandir(base_path()."/resources/lang/".$request->input('edit')."/");

			//$str = serialize(file_get_contents('./protected/app/lang/'.$request->input('edit').'/core.php'));
			$str = \File::getRequire(base_path()."/resources/lang/".$request->input('edit').'/'.$file);
			
			
			$this->data = array(
				'pageTitle'	=> 'Translation',
				'pageNote'	=> 'Add Multilangues Option',
				'stringLang'	=> $str,
				'lang'			=> $request->input('edit'),
				'files'			=> $files ,
				'file'			=> $file ,
			);	
			$template = 'edit';
		
		} else {

			$this->data = array(
				'pageTitle'	=> 'Translation',
				'pageNote'	=> 'Add Multilangues Option',
			);	
			$template = 'index';		
		
		}

		return view('magelur.config.translation.'.$template,$this->data);	
	}
	
	public function getAddtranslation()
	{
		return view("magelur.config.translation.create");
	} 
	
	public function postAddtranslation( Request $request)
	{
		$rules = array(
			'name'		=> 'required',
			'folder'	=> 'required|alpha',
			'author'	=> 'required',
		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$template = base_path();

			$folder = $request->input('folder');
			mkdir( $template."/resources/lang/".$folder ,0777 );	
			
			$info = json_encode(array("name"=> $request->input('name'),"folder"=> $folder , "author" => $request->input('author')));
			$fp=fopen(  $template.'/resources/lang/'.$folder.'/info.json',"w+"); 
			fwrite($fp,$info); 
			fclose($fp); 	
					
			$files = scandir( $template .'/resources/lang/en/');
			foreach($files as $f)
			{
				if($f != "." and $f != ".." and $f != 'info.json')
				{
					copy( $template .'/resources/lang/en/'.$f, $template .'/resources/lang/'.$folder.'/'.$f);
				}







				
			}
			return Redirect::to('magelur/config/translation')->with('messagetect','New Translation has been added !')->with('msgstatus','success');	;			
			
		} else {
			return Redirect::to('magelur/config/translation')->with('messagetext','Failed to add translation !' )->with('msgstatus','error')->withErrors($validator)->withInput();
		}		


	
	}
	
	public function postSavetranslation( Request $request)
	{
		$template = base_path();
		
		$form  	= "<?php \n"; 
		$form 	.= "return array( \n";
		foreach($_POST as $key => $val)
		{
			if($key !='_token' && $key !='lang' && $key !='file') 
			{
				if(!is_array($val))
				{
					$form .= '"'.$key.'"			=> "'.strip_tags($val).'", '." \n ";
				
				} else {
					$form .= '"'.$key.'"			=> array( '." \n ";
					foreach($val as $k=>$v)
					{
							$form .= '      "'.$k.'"			=> "'.strip_tags($v).'", '." \n ";
					}
					$form .= "), \n";
				}
			}		
		
		}
		$form .= ');';
		//echo $form; exit;
		$lang = $request->input('lang');
		$file	= $request->input('file');
		$filename = $template .'/resources/lang/'.$lang.'/'.$file;
	//	$filename = 'lang.php';
		$fp=fopen($filename,"w+"); 
		fwrite($fp,$form); 
		fclose($fp); 	
		return Redirect::to('magelur/config/translation?edit='.$lang.'&file='.$file)
		->with('messagetext','Translation has been saved !')->with('msgstatus','success');	
	
	} 	
	
	public function getRemovetranslation( $folder )
	{
		self::removeDir( base_path()."/resources/lang/".$folder);
		return Redirect::to('magelur/config/translation')->with('messagetext','Translation has been removed !')->with('msgstatus','success');	
		
	}	
	public function getLinks()
		{
			// if( \Auth::check() || \Session::get('gid') != '1'){
			// return Redirect::to('user/login');
			// }
			$pay = $this->option->get_options('links');
print_r($pay);exit;
				$this->data = array(
				'pageTitle'	=> 	'Contact',
				'pageNote'	=>  'About Contact',
				'pageModule'=>  'payment',
				'active' 	=> 'footer-options',				
				);	
			 	foreach ($pay as $key=> $value) {
			 		$this->data[$key] = $value;

				}		

				
			//echo "<pre>";print_r($this->data);exit;
			return view('bsetec.config.contact',$this->data);
		
		}	


}