<?php  namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Modele;
use App\Models\Agency;
use App\Library\Markdown;
use Mail;
use App\Models\Testimonial;
use Validator, Input, Redirect ; 
use App\Models\Sximo;

class HomeController extends Controller {

	public function __construct()
	{
		parent::__construct();

		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

			$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
			\App::setLocale($lang);
		} 

		$this->data['pageLang'] = 'en';
		if(\Session::get('lang') != '')
		{
			$this->data['pageLang'] = \Session::get('lang');
		}	
		$this->testimonial = new Testimonial();	
		$this->modele = new Modele();
		$this->option = new Sximo();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index(  Request $request  )
	{
		if(CNF_FRONT =='false' && $request->segment(1) =='' ) :
			return Redirect::to('dashboard');
		endif; 	

		$page = $request->segment(1);
		if($page !='') :
			$content = \DB::table('tb_pages')->where('alias','=',$page)->where('status','=','enable')->get();


			if(count($content) >=1)
			{

				$row = $content[0];
				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;
				$this->data['pageMetakey'] = ($row->metakey !='' ? $row->metakey : CNF_METAKEY) ;
				$this->data['pageMetadesc'] = ($row->metadesc !='' ? $row->metadesc : CNF_METADESC) ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');
				$this->data['breadcrumb'] = 'active';					
				
				if($row->access !='')
				{
					$access = json_decode($row->access,true)	;	
				} else {
					$access = array();
				}	

				// If guest not allowed 
				if($row->allow_guest !=1)
				{	
					$group_id = \Session::get('gid');				
					$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );	
					if($isValid ==0)
					{
						return Redirect::to('')
							->with('message', \SiteHelpers::alert('error',Lang::get('core.note_restric')));				
					}
				}				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}
				
				$this->data['content'] = \PostHelpers::formatContent($row->note);
				$this->data['filename'] = $row->filename;
				$this->data['links']=$this->option->get_options('links');

			//	echo '<pre>';print_r($this->data);echo '</pre>'; exit;
				if($row->template =='backend')
				{
					$this->data['pageNote'] = 'View';
					 return view($page_template,$this->data);
				} else {

					$this->data['pages'] = $page_template;
					$page = 'layouts.'.CNF_THEME.'.index';
					return view($page,$this->data);
				}				
			
				
				
				
			} else {
				return Redirect::to('')
					->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_noexists')));	
			}
			
			
		else :

			$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Home';
				$this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   $row->metakey ;
				$this->data['pageMetadesc'] =  $row->metadesc ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
          		  $this->data['fbimg'] = '';
          		  $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  $row->filename;				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['testimonials'] = $this->testimonial->where('status','1')->orderBy('id', 'desc')->get();
				$this->data['models'] = $this->modele->where('status','1')->where('approved_status','1')->where('publishes','1')->orderBy('id','desc')->limit(6)->get();
				// echo "<pre>";print_r($data['models']);exit;
				//$this->data['content'] = \PostHelpers::formatContent($row->note);
				// $this->data['content'] = view('pages.home',$data);
				$this->data['pages'] ='pages.home' ;
				$page = 'layouts.'.CNF_THEME.'.index';
				return view($page,$this->data);	
			

			} else {

				return ' No Default page set up !';
			}

		endif;

	}
	
	public function  getLang($lang='en')
	{
		\Session::put('lang', $lang);
		return  Redirect::back();
	}

	public function  getSkin($skin='sximo')
	{
		\Session::put('themes', $skin);
		return  Redirect::back();
	}		

	public  function  postContact( Request $request)
	{
	
		$this->beforeFilter('csrf', array('on'=>'post'));
		$rules = array(
				'name'		=>'required',
				'subject'	=>'required',
				'message'	=>'required|min:20',
				'sender'	=>'required|email'			
		);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) 
		{
			
			$data = array('name'=>$request->input('name'),'sender'=>$request->input('sender'),'subject'=>$request->input('subject'),'notes'=>$request->input('message')); 
			$message = view('emails.contact', $data); 		
			$data['to'] = CNF_EMAIL;			
			if(defined('CNF_MAIL') && CNF_MAIL =='swift')
			{ 
				Mail::send('user.emails.contact', $data, function ($message) use ($data) {
		    		$message->to($data['to'])->subject($data['subject']);
		    	});	

			}  else {

				$headers  	= 'MIME-Version: 1.0' . "\r\n";
				$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers 	.= 'From: '.$request->input('name').' <'.$request->input('sender').'>' . "\r\n";
					//mail($data['to'],$data['subject'], $message, $headers);		
			}


	

			return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('success','Thank You , Your message has been sent !'));	
				
		} else {
			return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('error','The following errors occurred'))
			->withErrors($validator)->withInput();
		}		
	}	




	function postProccess( Request $request , $formID )
	{
		//$row = $this->model->find($formID);
		$sql = \DB::table('tb_forms')->where('formID',$formID)->get();
		if(count($sql)<=0)
			return Redirect::back()->with('message', \SiteHelpers::alert('error','Form not Found !'));
		
		$row = $sql[0];
		$configuration = json_decode($row->configuration,true);

		$rules = \FormHelpers::validateForm( $configuration);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = \FormHelpers::validatePost($request , $configuration);	
			if($row->method =='table')
			{

				\DB::table($row->tablename)->insert($data);
				if($row->redirect !='')
				{
					echo '<script> window.location.href= "'.$row->redirect.'" </script>';
				} else {
					return Redirect::back()->with('message', \SiteHelpers::alert('success',$row->success));	
				}
				

			} else {
				// Send all input into specific email address

				$message = '';
				foreach($configuration as $conf)
				{
					$message .='
						<b>'.$conf['label'].'</b> : '. $request->input($conf['field']).' <br />
					';
				}				
				
				$data			= array('email'=>$row->email,'name'=>$row->name);
				$data['message'] = $message;
				
				$message = view('core.forms.templateforms', $data); 				
				//Mail::send('core.forms.templateforms', $data, function ($message) use ($data) {

		    		//$message->to($data['email'])->subject('Submited Form :  '. $data['name']);
		    //	});
		    
				if($row->redirect !='')
				{
					echo '<script> window.location.href= "'.$row->redirect.'" </script>';
				} else {
					return Redirect::back()->with('message', \SiteHelpers::alert('success',$row->success));	
				}
				

			}

			
		} else {

			//Redirect::back();
			return Redirect::back()->with('message', \SiteHelpers::alert('error','The following errors occurred'))
			->withErrors($validator)->withInput();	

		}	
	
	}
	public function  getAbout()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'About Us';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
	            $this->data['fbimg'] = '';
	            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'pages.aboutus';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}
				$this->data['pages'] = 'pages.aboutus';
				
				$this->data['testimonials'] = $this->testimonial->where('status','1')->orderBy('sort_order', 'asc')->get();
				$this->data['content'] = view('pages.aboutus',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
	
		
	public function  getContactus()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Contact Us';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
           		 $this->data['fbimg'] = '';
           		 $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'contactus';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] ='pages.contactus';
				// $this->data['content'] = view('pages.contactus');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
	public function  postSaveContactus(Request $request)
	{
		$data = array(
			'name'		=> $request->name ,
			'city'	 	=> $request->city ,
			'country'	=> $request->country ,
			'hai' 		=> $request->hai,
			'email' 	=> $request->email,
			);
		// echo "<pre>";print_r($data);exit;
			Mail::send('user.emails.contactus',$data ,function($message) use ($data){
					$message->from($data['email']);
					$message->to(CNF_EMAIL)->subject('Contact-us Mail');

			});	
	return Redirect::back();
	}
	public function  getTellfriend()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Invite Friends';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'tellfriend';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = 'pages.tellfriend';
				// $this->data['content'] = view('pages.tellfriend');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
	}	
	public function  postInvitefriend(Request $request)
	{
		$model_user=Modele::where('email', '=', Input::get('email'))
			->orwhere('username', '=', Input::get('your_name'))
			->first();
		$agent_user=Agency::where('email', '=', Input::get('email'))
			->orwhere('username', '=', Input::get('your_name'))
			->first();

		if(\Session::get('model_id')){$uid = \Session::get('model_id');}
		elseif(\Session::get('agent_id')){$uid = \Session::get('agent_id');}
		elseif($model_user != null){ $uid =$model_user->id;}
		elseif($agent_user != null){$uid =$agent_user->id;}
		else{ $uid=0;}
			$invite = array();
			$invite['your_name'] = $request->input('name');
			$invite['your_email'] = $request->input('email');
			$invite['your_id'] = $uid;
			$invite['friend_email'] = $request->input('friend_email');
			$invite['friend_name'] = $request->input('friend_name');
			$invite['description'] = $request->input('description');
			$invite['user_type'] = ' ';

			$invite['created'] =date("Y-m-d H:i:s");
			$invite_id = \DB::table('tb_tell_a_friend')->insertGetId($invite);
			$encoded_id = base64_encode($invite_id);
			$invite['encoded_id'] =$encoded_id;

			Mail::send('user.emails.invite_friend',$invite ,function($message) use ($invite){
					$message->from($invite['your_email']);
					$message->to($invite['friend_email'])->subject('Invitation mail');
		
			});
			return $encoded_id; 
			

	}	
	public function getInvitedfriends(Request $request)
	{
	    if(!\Auth::check() ) return \Redirect::to('login');
	    if($request->input('rows') != '' && $request->input('sort')=='' && $request->input('order')==''){
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = 'id';
	    	$order ='desc';
	    }
	    else{
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
	    	$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
	    }

	    $query = \DB::table('tb_tell_a_friend');
		if($request->get('order'))
	    {
	       $query->orderBy($sort,$order);
	    }
	       if($request->get('I_name'))
    {
        $query->where('your_name','like','%'.$request->get('I_name').'%')->orderBy($sort,$order);
    }
       if($request->get('R_name'))
    {
        $query->where('friend_name','like','%'.$request->get('R_name').'%')->orderBy($sort,$order);
    }
       if($request->get('status'))
    {
        $query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);
    }
       if($request->get('user_type'))
    {
        $query->where('user_type','like','%'.$request->get('user_type').'%')->orderBy($sort,$order);
    }
       if($request->get('invited_on'))
    {
        $query->where('created','like','%'.$request->get('invited_on').'%')->orderBy($sort,$order);
    }
        if($request->get('registered_on'))
    {
        $query->where('registered','like','%'.$request->get('registered_on').'%')->orderBy($sort,$order);
    }
    	$this->data['registered_on'] = 	(!empty($request->get('registered_on')))?$request->get('registered_on'):'';
		$this->data['invited_on'] = (!empty($request->get('invited_on')))?$request->get('invited_on'):'';
		$this->data['user_type'] = (!empty($request->get('user_type')))?$request->get('user_type'):'';
		$this->data['status'] = (!empty($request->get('status')))?$request->get('status'):'';
		$this->data['R_name'] = (!empty($request->get('R_name')))?$request->get('R_name'):'';
		$this->data['I_name'] = (!empty($request->get('I_name')))?$request->get('I_name'):'';
	    $this->data['results'] = $query->orderBy($sort,$order)->paginate($rows); 
	      $this->data['pager'] 		= $this->injectPaginate();
	  	$this->data['pageModule'] = '' ;  
	    return view('tell_a_friend.index',$this->data);
	}
	public function  getTerms()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Terms & Conditions';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
           		$this->data['fbimg'] = '';
           		$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'terms';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = 'pages.terms';
				// $this->data['content'] = view('pages.terms');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}		
	public function  getPrivacyPolicy()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Privacy Policy';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
	            $this->data['fbimg'] = '';
	            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'privacy_policy';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] ='pages.privacy_policy';
				// $this->data['content'] = view('pages.privacy_policy');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}		
	
	public function  getFeedback()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Feedback';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'feedbacksuggestion';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = 'pages.feedbacksuggestion';
				// $this->data['content'] = view('pages.feedbacksuggestion');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
		public function  postFeedbackform(Request $request)
	{
		$UserType=\Session::get('user_type');
		if($UserType==''){
		$UserType = 'guest';
		}
		
		// echo "<pre>";print_r($_POST);exit;
		 $feed = array();
			$feed['name'] = $request->input('name');
			$feed['email'] = $request->input('email');
			$feed['user_type'] = $UserType;
			$feed['suggestion'] = $request->input('suggestion');
			$feed['star_rate'] = $request->input('star_rate');
			$feed['created_Date'] =date("Y-m-d H:i:s");
			$feed_id = \DB::table('tb_feed_back')->insertGetId($feed);
			$status = Mail::send('user.emails.feed_back',$feed ,function($message) use ($feed){
				$message->from($feed['email']);
				$message->to('ramyabsetec@gmail.com')->subject('Feedback suggestion');

			});
			if($status==1)
			{
				\DB::table('tb_feed_back')
				->where('id', $feed_id )
				->update(array(
					'mail_status' => 'send',
					));
			}	
			else{
				\DB::table('tb_feed_back')
				->where('id', $feed_id )
				->update(array(
					'mail_status' => 'notsend',
					));
			}		

	}	
	public function getSuggestedlist(Request $request)
	{
	   if(!\Auth::check() ) return \Redirect::to('login');
		if($request->input('rows') != '' && $request->input('sort')=='' && $request->input('order')==''){
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = 'id';
	    	$order ='desc';
	    }
	    else{
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
	    	$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
	    }
	    $query = \DB::table('tb_feed_back');
		if($request->get('order'))
	    {
	       $query->orderBy($sort,$order);
	    }

    if($request->get('name'))
    {
      // echo $request->get('name');
        $query->where('name','like','%'.$request->get('name').'%')

        	->orderBy($sort,$order);
    }
     if($request->get('email'))
    {
        $query->where('email','like','%'.$request->get('email').'%')->orderBy($sort,$order);
    }

        $query->where('user_type','like','%'.$request->get('user_type').'%')->orderBy($sort,$order);
   
            $query->where('mail_status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

	    $this->data['results'] = $query->orderBy($sort,$order)->paginate($rows); 
	  	$this->data['pageModule'] = '' ;  
	  	$this->data['name'] = 	(!empty($request->get('name')))?$request->get('name'):'';
		$this->data['email'] = (!empty($request->get('email')))?$request->get('email'):'';
		$this->data['user_type'] = $request->get('user_type');
		$this->data['status'] = $request->get('status');
		$this->data['pager'] 		= $this->injectPaginate();
	    return view('feed_back_suggestion.index',$this->data);
	}
	
	public function  getAdvertisement()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Advertise with us';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
	            $this->data['fbimg'] = '';
	            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'advertise';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] ='pages.advertise';
				// $this->data['content'] = view('pages.advertise');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
	public function  getMembershipPlan()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Membership';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'membershipplan';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$this->data['content'] = view('pages.membershipplan');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
		public function  getBlog()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Blogs';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
	            $this->data['fbimg'] = '';
	            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'blogs';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				// $this->data['pages'] = $page_template;
				// $this->data['content'] = view('pages.blogs');
				$this->data['pages'] ='pages.blogs' ;
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
	public function  getBlogDetails()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Blogs';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
	            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'blog_detail';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				// $this->data['pages'] = $page_template;
				// $this->data['content'] = view('pages.blog_detail');
				$this->data['pages'] ='pages.blog_detail' ;
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}	
	     		public function getLinks()
		{
			// if( \Auth::check() || \Session::get('gid') != '1'){
			// return Redirect::to('user/login');
			// }
				$pay = $this->option->get_options('links');
				$this->data = array(
				'pageTitle'	=> 	'Footer',
				'pageNote'	=>  'Footer Links',
				'pageModule'=>  'footer-links',
				'active' 	=> 'footer-options',				
				);	
			 	foreach ($pay as $key=> $value) {
			 		$this->data[$key] = $value;

				}		

				
			// echo "<pre>";print_r($this->data);exit;
			return view('magelur.config.footer_links',$this->data);
		
		}
		public function postUpdatelinks(Request $request)
		{

			  
    	$data = array(
			
			'fb'=>$request->get('fb'),
			'twitter'=>$request->get('twitter'),
			'ytube'=>$request->get('ytube'),
			'insta'=>$request->get('insta'),
			'address'=>$request->get('address'),
			
			
			);
	
		 $this->option->save_options('links',$data);

		
				return  Redirect::back()->with('messagetext','Saved Successfully')->with('msgstatus','success');
		}

}
