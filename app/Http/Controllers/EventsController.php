<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Events;
use App\Models\Agency;
use App\Models\Eventfields;
use App\Helper\Helper;
use App\Library\Slimdown;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Sximo;

class EventsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'events';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Events();
		$this->agents = new Agency();
		$this->option = new Sximo();
		$this->eventfields = new Eventfields();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'events',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

			$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
			\App::setLocale($lang);
		}  

	}


	public function getIndex( Request $request )
	{
		// check user logged or not
		$sChkLogged = \SiteHelpers::checkLogged();

		// get user type
		$sUserType  = \Session::get('user_type');

		if($sUserType == 'model' || $sUserType == 'agent'){
			return redirect::to('/events/list');
		}

		if(empty($sChkLogged))
			return \SiteHelpers::gotologin('user');


		if(\Auth::check()){
			if($this->access['is_view'] ==0) 
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

			$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
			$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
			// End Filter sort and order for query 
			// Filter Search for query		
			$filter = '';	
			if(!is_null($request->input('search')))
			{
				$search = 	$this->buildSearch('maps');
				$filter = $search['param'];
				$this->data['search_map'] = $search['maps'];
			} 

			
			$page = $request->input('page', 1);
			$params = array(
				'page'		=> $page ,
				'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
				'sort'		=> $sort ,
				'order'		=> $order,
				'params'	=> $filter,
				'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
			);
			// Get Query 
			$results = $this->model->getRows( $params );	

			// Build pagination setting
			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
			$pagination->setPath('events');
	$rows = $request->input('rows') ? $request->input('rows') : 10 ;

    $query = \DB::table('tb_events');
    if($request->get('title'))
    {
        $query->where('title','like','%'.$request->get('title').'%')->orderBy($sort,$order);
        	
    }
    if($request->get('categories'))
    {
        $query->where('categories','like','%'.$request->get('categories').'%')->orderBy($sort,$order);
        	
    }
     if($request->get('owner'))
    {
        $query->where('ownername','like','%'.$request->get('owner').'%')->orderBy($sort,$order);
        	
    }
    if($request->get('note'))
    {
        $query->where('note','like','%'.$request->get('note').'%')->orderBy($sort,$order);
        	
    }
    if($request->get('status') == 'Active')
    {
    	$query->where('status','Active')->orderBy($sort,$order);
    }
        $query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

    if($request->get('startdate'))
    {
    	$date= date('Y-m-d H',strtotime($request->get('startdate')));
        $query->where('startdate','like','%'.$date.'%')->orderBy($sort,$order);
        	
    }
     if($request->get('enddate'))
    {
    	$date= date('Y-m-d H',strtotime($request->get('enddate')));
        $query->where('enddate','like','%'.$date.'%')->orderBy($sort,$order);
        	
    }
    	if($request->get('Tags'))
	    {
	    	// for multiple tags
	    	$alltags = explode(",",$request->get('Tags') );
	    	foreach ($alltags as $value) {

	    		if($value!="")
	    		{
	    			$query->Where('tags','like','%'.$value.'%')
	    				->orderBy($sort,$order);

	    		}
	    	}
	       // $query->where('tags','like','%'.$request->get('Tags').'%');
	    }
  
   	 $this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows); 
			$this->data['rowData']		= $results['rows'];
			//Update new entry from 1 to 0
			$test=Events::select('new','id')->whereIn('new',[1])->lists('new','id')->toArray();
			$new_id = '1';
			$list_new_ids = $test;
			if(in_array($new_id, $list_new_ids))
			{
				\DB::table('tb_events')
		        ->whereIn('new',[1])  
		        ->update(array('new' => '0'));
		    }
		    /*Prepopular filter fields*/
		$this->data['startdate'] = 	(!empty($request->get('startdate')))?$request->get('startdate'):'';
		$this->data['enddate'] = (!empty($request->get('enddate')))?$request->get('enddate'):'';
		$this->data['note'] = (!empty($request->get('note')))?$request->get('note'):'';
		$this->data['owner'] = (!empty($request->get('owner')))?$request->get('owner'):'';
		$this->data['title'] = (!empty($request->get('title')))?$request->get('title'):'';
		$this->data['categories'] = (!empty($request->get('categories')))?$request->get('categories'):'';
		$this->data['status'] = $request->get('status');
		$this->data['Tags'] = (!empty($request->get('Tags')))?$request->get('Tags'):'';	

			// Build Pagination 
			$this->data['pagination']	= $pagination;
			// Build pager number and append current param GET
			$this->data['pager'] 		= $this->injectPaginate();	
			// Row grid Number 
			$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
			// Grid Configuration 
			$this->data['tableGrid'] 	= $this->info['config']['grid'];
			$this->data['tableForm'] 	= $this->info['config']['forms'];	
			// Group users permission
			$this->data['access']		= $this->access;
			// Detail from master if any
			
			// Master detail link if any 
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

			// Render into template
			return view('events.index',$this->data);
		}
		else{
			$sql = \DB::table('tb_events')->where('status','Active')->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$data['pageTitle'] 	= 'Events';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'events';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->image.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				
				$page = isset($_GET['page']) ? $_GET['page'] : 1;
				$params = array(
					'page'		=> $page ,
					'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
					'sort'		=> 'created' ,
					'order'		=> 'desc',
					'global'	=> 1 
				);		

				$model  = new Events();
				$this->option = new Sximo();

				$info = $model::makeInfo('events');
				$data['pageLang'] = 'en';
				if(\Session::get('lang') != '')
					{
						$data['pageLang'] = \Session::get('lang');
					}		


					$result = $model::getRows( $params );
					$data['rowData'] 	= $result['rows'];	

					$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
					$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
					$pagination->setPath('');
					$data['i']			= ($page * $params['limit'])- $params['limit']; 
					$data['pagination'] = $pagination;

					
					
					$data['pageNote'] 	=  'View All';
					$data['breadcrumb'] 	= 'false';	
					$data['pageMetakey'] 	=  '' ;
					$data['pageMetadesc'] =  '' ;
					$data['gplusimg'] = '';
					$data['tweetimg'] = '';
					$data['v_url'] = ''; 
					$data['urls'] = '';
            		$data['fbimg'] = '';
            		 $data['links']=$this->option->get_options('links');
					$data['filename'] 	=  '';	
					

					
					
					$this->data['content'] = view('events.main',$data);
					$page = 'layouts.'.CNF_THEME.'.index';
					return view($page,$this->data);			
				}
				
			}

		}	

		public function getMyEventsList( Request $request )
		{
			// get usertype
			$usertype = \session::get('user_type');
			$loggedId = \session::get('logged_id');
			if(empty($loggedId)){
				return Redirect::to('login');
			}

			if($usertype == 'model' || $usertype == 'admin'  ){
				// show page restriction
				echo "You can't able to access this page ";
				exit;
			}
			$data['images'] = $this->agents->where('id',$loggedId )->where('status','1')->where('verification_status','1')->first();
			$page   = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'created',
				'order'		=> 'desc',
				'params'	=> "AND `ownerid` = '".$loggedId."' AND `usertype` = '".$usertype."' AND `status` != 'deleted'" ,
				'global'	=> 1 
			);		
			return self::articles( $params , $page ,'all','my_events')->with('images', $data);
		}


		public function getEventsList( Request $request )
		{
			if(\Auth::check()){
			$params = " AND publishID ='public' AND status ='active' OR publishID ='registered' AND status ='active' OR publishID ='onlyme' AND status ='active'" ; 
		}
		elseif(\SiteHelpers::checkLogged() == ''){
			$params = " AND publishID ='public' AND status ='active'" ; 
		}
		elseif(\SiteHelpers::checkLogged() == 1){
			$params = " AND publishID ='public' AND status ='active' OR publishID ='registered' AND status ='active' " ; 
		}

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'created' ,
				'order'		=> 'desc',
				'params'	=> $params,
				'global'	=> 1 
			);		
			return self::articles( $params , $page , 'all' );
		}

		public function getView( Request $request, $id = null)
		{

			$row = $this->model->getRow(base64_decode($id));

			if($row)
			{	
				// update the view count 
				\DB::table('tb_events')->where('id',$row->id)->update(array('views'=> \DB::raw('views+1')));

				$data['pageLang'] = 'en';
				if(\Session::get('lang') != '')
					{
						$data['pageLang'] = \Session::get('lang');
					}	

					$data['conpost'] = json_decode(file_get_contents(base_path().'/resources/views/core/posts/config.json'),true);		

					$data['pageTitle'] 		= 	$row->title ;
					$data['pageNote'] 		=  	'View All';
					$data['breadcrumb'] 	= 	'inactive';	
					// $data['pageMetakey'] 	=  	$row->metakey;
					// $data['pageMetadesc'] 	=  	$row->metadesc ;
					$data['pageMetakey'] 	=  	'';
					$data['pageMetadesc'] 	=  	'';
					$data['gplusimg'] = '';
					$data['tweetimg'] = '';
					$data['v_url'] = ''; 
					$data['urls'] = '';
            		$data['fbimg'] = '';	
            		 $data['links']=$this->option->get_options('links');	
					$data['filename'] 		=  	'';	
					$row->custom_address    = explode(',', $row->address);
					$data['row'] 			=  $row;
					$sCateWhere             = '`id` IN ('.$row->categories.')';
					$aCategory 				= \SiteHelpers::getCategories($sCateWhere);

					$sCategory = '';
					if(!empty($aCategory))
						$sCategory = implode(',', array_column($aCategory, 'name'));

					$data['author']         =  \SiteHelpers::getName('username',$row->ownerid);

					$data['cate']			= $sCategory;
					$data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
					$data['id'] 			= $id;
					$data['access']			= $this->access; 
					$data['prevnext'] 		= $this->model->prevNext($id);
					$data['labels']			= self::splitLabels($row->tags);
					$data['comments']		= $this->model->comments($row->id);
					// echo "<pre>";print_r($data['comments']);exit;
					$data['clouds']			= self::cloudtags();
					$data['recentposts']	= $this->model->getRecentPost('AND `startdate` >= CURDATE() ','LIMIT 5');
 					// get Sum ratings
					$ratedCounts =\DB::table('tb_comments')
					->selectRaw('sum(rating) as sum,count(rating) as total_count')
					->where('pageID',$id)
					->where('type','events')
					->first();
					$sum=$ratedCounts->sum;
					if($sum>0)
						$total=number_format((float)($sum)/($ratedCounts->total_count), 1, '.', '');
					else
						$total='Not yet Rated';

                //Total counts for particular videos 
					$data['avg_ratings']=$total;
					
					$page = 'layouts.'.CNF_THEME.'.index';

					$data['pages'] = 'events.event_view';

					return view($page,$data);
				} else {
					return Redirect::to('events/list')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
				}
			}		


			public static function articles($params , $page , $title = 'all', $sTemplate = 'list')
			{
				// print_r($params);exit;

				// get usertype
				$usertype = \session::get('user_type');
				$loggedId = \session::get('logged_id');

				$model  = new Events();
				$option = new Sximo();
				$info   = $model::makeInfo('events');

				$data['pageLang'] = 'en';
				if(\Session::get('lang') != '')
					{
						$data['pageLang'] = \Session::get('lang');
					}		

					$result = $model::getRows( $params );
					$result_count = count($result['rows']); // get count of total events

					foreach ($result['rows'] as $iKey => $val) {

						// check if admin approval is set to 1 or 0
						if($val->status=='Active')
						{
							$sCateWhere             = '`id` IN ('.$val->categories.')';
							$aCategory 				= \SiteHelpers::getCategories($sCateWhere);
							$sCategory = '';
							if(!empty($aCategory))
								$sCategory = implode(',', array_column($aCategory, 'name'));

							$result['rows'][$iKey]->cate   = $sCategory;
							$result['rows'][$iKey]->author = \SiteHelpers::getName('username',$val->ownerid);
						}
						else if($val->status=='Inactive' and $val->ownerid!=$loggedId)
						{
							unset($result['rows'][$iKey]);
							
						}
						//end
					}
					// if($sTemplate == 'my_events'){
					// $data['rowData'] 	= $result['rows'];
					// }

					if($sTemplate == 'my_events'){
					$data['rowData'] 	= $result['rows'];
				
					}elseif($sTemplate == 'list'){
					// $data['rowData'] 	= $model::checks();
					$data['rowData'] 	= $result['rows'];
					}

					// $data['rowData'] 	= $result['rows'];	
					$data['total_events'] 	= $result_count; //send total count of events
					
					$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
					$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
					$pagination->setPath('');
					$data['i']			= ($page * $params['limit'])- $params['limit']; 
					$data['pagination'] = $pagination;

					if($title !='all')
					{
						$data['pageTitle'] 	= 'Label :  '.$title;

					} elseif($sTemplate == 'my_events') {
						$data['pageTitle'] 	= 'My Events';
					}
					else{
						$data['pageTitle'] 	= 'Events';
					}


					$data['pageNote'] 	    =  'View All';
					$data['breadcrumb'] 	= 'false';	
					$data['pageMetakey'] 	=  '' ;
					$data['pageMetadesc']   =  '' ;
					$data['gplusimg'] = '';
					$data['tweetimg'] = '';
					$data['v_url'] = ''; 
					$data['urls'] = '';
            		$data['fbimg'] = '';
            		$data['links']=$option->get_options('links');
					$data['filename'] 		=  '';	

					if($sTemplate == 'my_events' && $usertype == 'agent'){
					$oAgent  			= new Agency();
					$data['agents']     = $oAgent->where('id',1)->where('status',$loggedId)->where('verification_status','1')->get();
					$data['agentname']  = \SiteHelpers::getName($usertype,$loggedId);
					}

					$data['clouds']			=  self::cloudtags();
					$data['latestposts']	= $model::latestposts();
					$data['recentposts']	= $model->getRecentPost('AND `startdate` >= CURDATE() ','LIMIT 3');
					$data['conpost'] 		= json_decode(file_get_contents(base_path().'/resources/views/core/posts/config.json'),true);
					$page = 'layouts.'.CNF_THEME.'.index';
					$data['pages'] = 'events.'.$sTemplate;

					// echo "<pre>"; print_r($data['agents']); exit;	
					return view($page,$data);			

				}	



				function getUpdate(Request $request, $id = null)
				{

					$iLoggedId = \session::get('logged_id');
					$sUserType = \session::get('user_type');

					// check user logged
					if(empty($iLoggedId)){
						return Redirect::to('login');
					}

					// // check the user is agent 
					// if(\Session::has('model_id')){
					// 	// show error message
					// 	echo "Model Can't able to access this page"; exit;
					// }

					// get records list
	                $row 	   = $this->model->find($id);
	                $iOwnerId  = isset($row->ownerid)?$row->ownerid:'';
	                $usertype  = isset($row->usertype)?$row->usertype:'';
	                if( ($iLoggedId == $iOwnerId && ucfirst($sUserType) == $usertype) || empty($id) || $sUserType == 'admin'  ){

	                	//if usertype is agent checkmembership plan
	                	$event_status=1;
	                	if($sUserType=='agent')
	                	{
	                		// get agency details
	                		$agent_details = \DB::table('tb_agency')->where('id', '=', \Session::get('agent_id'))->where('status','=','1')->where('verification_status','=','1')->first();
	                		// get event details
	                		$event_details = \DB::table('tb_events')->where('ownerid', '=', \Session::get('agent_id'))->where('usertype','=',$sUserType)->get();
	                		//get membership plan details
	                		$membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $agent_details->plan_id)->where('status','=','1')->first();
	                		//Check whether Plan is Basic
                			// if(isset($membership_details->plan_name) and $membership_details->plan_name=="Basic")
                			// {
                			// 	$event_status=0;
                			// }
                			// elseif(!isset($membership_details->plan_name))
                			// {
                			// 	$event_status=0;
                			// }
	                		//check how many events agent has created
	                		if(count($event_details)>=0)
	                		{
	                			
	                			//check event limit with number of events created by agents
	                			if($membership_details->event_limit!="unlimited" && $agent_details->eventplan==count($event_details))
	                			{
	                				$event_status=0;
	                			}
	                		}
	                		 // echo "<pre>";print_r(count($event_details));
	                		 // echo "<pre>";print_r($agent_details->eventplan);die;

	                	}

	                	// check event status
	                	if($event_status==1)
	                	{
		                	if($row)
		                	{
		                		$this->data['row'] =  $row;
		                	} else {
		                		$this->data['row'] = $this->model->getColumnTable('tb_events'); 
		                	}
		                	$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

		                	$this->data['id'] = $id;

							// check the user is agent 
		                	if(\Session::has('agent_id') || \Session::has('model_id')){
		                		$sTemplate = 'fullpage';

		                		if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$sTemplate.'.blade.php') && $sTemplate !='')
		                		{
		                			$page_template = 'layouts.'.CNF_THEME.'.template.'.$sTemplate;
		                		} else {
		                			$page_template = 'layouts.'.CNF_THEME.'.template.page';
		                		}		
		                		$this->data['pageTitle'] 	= 'Post Events';
		                		$this->data['pageMetakey'] 	=   '' ;
		                		$this->data['pageMetadesc'] =  '' ;
		                		$this->data['gplusimg'] = '';
		                		$this->data['tweetimg'] = '';
		                		$this->data['v_url'] = ''; 
		                		$this->data['urls'] = '';
           						$this->data['fbimg'] = '';
           						$this->data['links']=$this->option->get_options('links');
		                		$this->data['pages'] = $page_template;
		                		$this->data['agents'] = $this->agents->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		                		$this->data['images'] = $this->agents->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		                		$this->data['content'] = view('events.public.form',$this->data);
		                		$page = 'layouts.'.CNF_THEME.'.index';
								// var_dump($page_template); exit;
								// echo "<pre>"; print_r($this->data); exit;
		                		return view($page,$this->data);
		                	}


		                	return view('events.form',$this->data);
		                }

	                 	elseif($row['id'] =='' && $event_status==0)
	                 	{
	                 		// echo "You're not allowed to Create more Events"; exit;
	                 		return Redirect::to('my/events')->with('messagetext','Upgrade your membership to add more events')->with('msgstatus','error');
	                 	}
	                 	else{
	                 		if($row)
		                	{
		                		$this->data['row'] =  $row;
		                	} else {
		                		$this->data['row'] = $this->model->getColumnTable('tb_events'); 
		                	}
		                	$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

		                	$this->data['id'] = $id;

							// check the user is agent 
		                	if(\Session::has('agent_id') || \Session::has('model_id')){
		                		$sTemplate = 'fullpage';

		                		if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$sTemplate.'.blade.php') && $sTemplate !='')
		                		{
		                			$page_template = 'layouts.'.CNF_THEME.'.template.'.$sTemplate;
		                		} else {
		                			$page_template = 'layouts.'.CNF_THEME.'.template.page';
		                		}		
		                		$this->data['pageTitle'] 	= 'Post Events';
		                		$this->data['pageMetakey'] 	=   '' ;
		                		$this->data['pageMetadesc'] =  '' ;
		                		$this->data['gplusimg'] = '';
		                		$this->data['tweetimg'] = '';
		                		$this->data['v_url'] = ''; 
		                		$this->data['urls'] = '';
           						$this->data['fbimg'] = '';
           						$this->data['links']=$this->option->get_options('links');
		                		$this->data['pages'] = $page_template;
		                		$this->data['agents'] = $this->agents->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		                		$this->data['images'] = $this->agents->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		                		$this->data['content'] = view('events.public.form',$this->data);
		                		$page = 'layouts.'.CNF_THEME.'.index';
								// var_dump($page_template); exit;
								// echo "<pre>"; print_r($this->data); exit;
		                		return view($page,$this->data);
		                	}


		                	return view('events.form',$this->data);
						}
                 	//end

	                }else{
	                	echo "You're not allowed to edit this event"; exit;
	                }	
				}	

				public function getShow( Request $request, $id = null)
				{
					if($this->access['is_detail'] ==0) 
						return Redirect::to('dashboard')
					->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
					$row = $this->model->getRow($id);
					if($row)
					{
						$this->data['row'] =  $row;
						$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
						$this->data['id'] = $id;
						$this->data['access']		= $this->access;
						$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
						$this->data['prevnext'] = $this->model->prevNext($id);
						return view('events.view',$this->data);
					} else {
						return Redirect::to('events')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
					}
				}	

				function postSave( Request $request)
				{
					// get usertype
					$usertype = \session::get('user_type');
					$loggedId = \session::get('logged_id');

					if(empty($loggedId)){
						return Redirect::to('login');
					}

					// echo "<pre>"; print_r($_POST);
					// echo "<pre>"; print_r($_FILES);
					// exit;

					if(\Session::get('uid')){
						$iOwnerId   = \Session::get('uid');
						$iOwnerName  = \Session::get('username');
						$sCreatedby = 'admin';
					}
					elseif(\Session::get('agent_id')){
						$iOwnerName  = \Session::get('agent_name');
						$iOwnerId   = \Session::get('agent_id');
						$sCreatedby = 'agent';
					}
					else{
						// show error message
						echo "Model Unable to create event"; exit;
					}	

				

					$rules = $this->validateForm();
					$rules =array(
						'note'=>'required',
						'title'=>'required',
						'categories'=>'required',
						// 'status'=>'required',
						// 'image'=>'required|mimes:jpeg,jpg,png',
						'startdate'=>'required',
						'enddate'=>'required',
						'link'=>'required|url',
						'email'=>'required|email',
						// 'geocomplete'=>'required',
						// 'video'=>'required',
						'formatted_address'=>'required',
						'locality'=>'required',
						'postal_code'=>'required',
						'administrative_area_level_1'=>'required',
						'country'=>'required',
						'mobile'=>'required'
					);
		
					$validator = Validator::make($request->all(), $rules);	
					if ($validator->passes()) {
						// set file type for image
						$this->info['config']['forms'][11]['type'] = 'file';	
						$this->info['config']['forms'][11]['option']['path_to_upload'] = '/uploads/events/';
						// \\192.168.1.65\magelur\app\Http\Controllers\Controller.php	

						$data = $this->validatePost('tb_events');

						// get video id & ext
						$video_id  = $request->input('video_id');
						

						if(!empty($_FILES['video']['tmp_name'])){

						$video = Input::file('video');

           	    		 // Only Valid Video Will be uploaded
						$secure = Helper::secure_video($video);

						$video_ext = $secure['video_type'];	
						if ($secure == FALSE) {
							$response = array(
								'status' => 'error', 
								'msg'    => 'Oops! Please Upload a Valid Video File.', 
							);
							Redirect::to('/')->with('messagetext',\Lang::get('Oops! Please Upload a Valid Video File.'))->with('msgstatus','error');
						}	

						// if(!empty($video_id)){
						// 	unlink(public_path().'/uploads/events/videos/'.$video_id);
						// }

						// Generate New Video Random String
						$video_id  = Helper::random_string();
	                    // Video Extension
	                    $video_ext = $secure['video_type'];	
	                    // video path
						$vid_path   = public_path().'/uploads/events/videos/'.$video_id;		 
			
                    	// Move Video
	                    $move 	   = Helper::move_video($video, $video_id, $video_ext,$vid_path);	

						// set video field 
						$data['video']      = $video_id;
						// set video ext
						$data['video_ext']  = $video_ext;

	                	}
	                    
						


						// set ownerId
						$data['title']      = $request->input('title');
						
						if(empty($request->input('post_id'))){
							$data['ownerid']    = $iOwnerId;
							$data['usertype']   = $sCreatedby;
							$data['ownername']   = $iOwnerName;
						}

						// set category
						$sCategory = '';
						if(!empty($data['categories']))
							$sCategory = implode(',', $data['categories']);

						$data['categories'] = $sCategory;

						//  get status
						// $data['status']   = ($request->input('status') !== null)?$request->input('status'):'Inactive';
						if($request->input('status')==''){
							$data['status']   = 'Inactive';}
							else{
								$data['status']   = $request->input('status');
							}
						// print_r($data['status']);exit;
						//  get tags
						$data['tags']  	  = $request->input('tags');						

						// set address field
						$data['address']   = $request->input('formatted_address');
						$data['city']      = $request->input('locality');
						$data['state']     = $request->input('administrative_area_level_1');
						$data['country']   = $request->input('country');
						$data['zipcode']   = $request->input('postal_code');
						$data['latitude']  = $request->input('lat');
						$data['longitude'] = $request->input('lng');

						// set event link
						$data['event_link'] = $request->input('link');
						// set slug
						$data['uri']	    = str_slug($data['title']);	
						// set slug
						$data['created']    = date("Y-m-d H:i:s");	

						// set mobile field 
						$data['mobile']     = $request->input('mobile');
						// set email field 
						$data['email']      = $request->input('email');	
						$data['get_tickets']      = $request->input('get_ticket');					
						$data['talent_needed']      = $request->input('talent_needed');	
						$data['travel_package']      = $request->input('travel_package');
						$data['accomm_package']      = $request->input('accomm_package');	
						$data['benefit_package']      = $request->input('benefit_package');
						$data['dresscode']      = $request->input('dress_code');					
						$data['need_model']      = $request->input('need_models');
						$data['publishID']      = $request->input('publishID');
						$data['entrycost']      = $request->input('entrycost');	
						$data['new']           = 1;
						if($request->input('agree') == 0){
						$data['agree']         = 1;}
						else{
							$data['agree']         = $request->input('agree');
						}
						// print_r($request->input('agree'));exit;

						if($request->input('id'))
							$data['updated']   = date("Y-m-d H:i:s");		
						$id = $this->model->insertRow($data , $request->input('post_id'));
						if(\Session::get('agent_id')){
							$return = '/my/events/'.self::returnUrl();
							return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
						}
						else{
							$return = 'events/update/'.$id.'?return='.self::returnUrl();
							return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
						}
						// var_dump($id); exit;
						if(!is_null($request->input('apply')))
						{
							$return = 'events/update/'.$id.'?return='.self::returnUrl();
						} else {
							$return = 'events?return='.self::returnUrl();
						}

						// Insert logs into database
						if($request->input('id') =='')
						{
							\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
						} else {
							\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
						}

						return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

					} else {

						return Redirect::to('events/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
						->withErrors($validator)->withInput();
					}	

				}	

				//to delete the events

				function getDelete(Request $request, $id = null)
				{
					$iLoggedId = \session::get('logged_id');
					$sUserType = \session::get('user_type');
					// echo $id;die;
					// check user logged
					if(empty($iLoggedId)){
						return Redirect::to('login');
					}

					$row 	   = $this->model->find($id);
	                $iOwnerId  = isset($row->ownerid)?$row->ownerid:'';
	                $usertype  = isset($row->usertype)?$row->usertype:'';
	                if( ($iLoggedId == $iOwnerId && ucfirst($sUserType) == $usertype) || empty($id) || $sUserType == 'admin'  ){

		                	\DB::table('tb_events')->where('id',$id)
		                						   ->where('ownerid',$iOwnerId)
		                						   ->where('usertype',$usertype)->update(['status'=>'deleted','publishID'=>'onlyme']);

							
		                return Redirect::to('my/events')->with('messagedelete','Your Event has been deleted Successfully !');

	                }else{
	                	echo "You're not allowed to Delete this event"; exit;
	                }
				}
				//end

				public function postDelete( Request $request)
				{

					if($this->access['is_remove'] ==0) 
						return Redirect::to('dashboard')
					->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
					if(count($request->input('ids')) >=1)
					{
						$this->model->destroy($request->input('ids'));

						\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
						return Redirect::to('events?return='.self::returnUrl())
						->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 

					} else {
						return Redirect::to('events?return='.self::returnUrl())
						->with('messagetext','No Item Deleted')->with('msgstatus','error');				
					}

				}	

				public static function splitLabels($value='')
				{
					$value = explode(',',$value);
					$vals = '';
					foreach($value as $val)
					{
						$vals .= '<a href="'.url('post/label/'.trim($val)).'" class="btn btn-xs btn-default"> '.trim($val).' </a> ';
					}
					return $vals;
				}

				public static function cloudtags()
				{
					$tags = array();	
					$keywords = array();
					$word = '';
					$data = \DB::table('tb_events')->get();
					foreach($data as $row)
					{
						$clouds = explode(',',$row->tags);
						foreach($clouds as $cld)
						{
							$cld = strtolower($cld);
							if (isset($tags[$cld]))
							{
								$tags[$cld] += 1;
							} else {
								$tags[$cld] = 1;
							}
							//$tags[$cld] = trim($cld);
						}
					}

					ksort($tags);
					foreach($tags as $tag=>$size)
					{
						//$size += 12;
						$word .= "<a href='".url('post/label/'.trim($tag))."'><span class='cloudtags' ><i class='fa fa-tag'></i> ".ucwords($tag)." (".$size.") </span></a> ";
					}

					return $word;
				}

				public static function latestpost()
				{

				}
				public static function display( )
				{
					$mode  = isset($_GET['view']) ? 'view' : 'default' ;
					$model  = new Events();
					$info = $model::makeInfo('events');

					$data = array(
						'pageTitle'	=> 	$info['title'],
						'pageNote'	=>  $info['note']

					);

					if($mode == 'view')
					{
						$id = $_GET['view'];
						$row = $model::getRow($id);
						if($row)
						{
							$data['row'] =  $row;
							$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
							$data['id'] = $id;
							return view('events.public.view',$data);
						} 

					} else {

						$page = isset($_GET['page']) ? $_GET['page'] : 1;
						$params = array(
							'page'		=> $page ,
							'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
							'sort'		=> 'id' ,
							'order'		=> 'asc',
							'params'	=> '',
							'global'	=> 1 
						);

						$result = $model::getRows( $params );
						$data['tableGrid'] 	= $info['config']['grid'];
						$data['rowData'] 	= $result['rows'];	

						$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
						$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
						$pagination->setPath('');
						$data['i']			= ($page * $params['limit'])- $params['limit']; 
						$data['pagination'] = $pagination;
						return view('events.public.index',$data);			
					}


				}


				public function postComment( Request $request)
				{
					// check there is comments
					if(empty($request->input('comments')))
						return Redirect::to('events/view/'.$request->input('pageID').'/'.$request->input('alias'))
					->with('messagetext',\SiteHelpers::alert('error','The following errors occurred'))->with('msgstatus','error');	

					if(\Session::get('gid')){
						$iUserId    = \Session::get('gid');
						$sUserMode  = 'user';
					}
					elseif(\Session::get('model_id')){
						$iUserId = \Session::get('model_id');
						$sUserMode  = 'model';
					}
					elseif(\Session::get('agent_id')){
						$iUserId = \Session::get('agent_id');		 		
						$sUserMode  = 'agent';
					}

					$rules = array();
					$validator = Validator::make($request->all(), $rules);	
					// echo "<pre>"; print_r($validator->passes()); exit;
					if ($validator->passes()) {
						$data = array(
							'userID'		=> $iUserId,
							'posted'		=> date('Y-m-d H:i:s') ,
							'type'          => 'events',
							'status'        => 'active',
							'userType'      => $sUserMode,
							'rating'      	=> $request->input('rating'),
							'comments'		=> $request->input('comments'),
							'pageID'		=> $request->input('pageID')
						);
					// echo "<pre>"; print_r($data); exit;	
						\DB::table('tb_comments')->insert($data);
						return Redirect::to('events/view/'.base64_encode($request->input('pageID')).'/'.$request->input('alias'))
						->with('messagetext','Thank You , Your comment has been sent !')->with('msgstatus','success');
					} else {
						return Redirect::to('events/view/'.base64_encode($request->input('pageID')).'/'.$request->input('alias'))
						->with('messagetext','The following errors occurred')->with('msgstatus','error');	
					}
				}

				public function getRemove( Request $request, $pageID , $alias , $commentID )
				{
					if($commentID !='')
					{

						\DB::table('tb_comments')->where('commentID',$commentID)->delete();
						return Redirect::to('events/view/'.$pageID.'/'.$alias)
						->with('messagetext', \SiteHelpers::alert('success','Comment has been deleted !'))->with('msgstatus','success');

					} else {

						return Redirect::to('events/view/'.$pageID.'/'.$alias)
						->with('messagetext', \SiteHelpers::alert('error','Failed to remove comment !'))->with('msgstatus','error');
					}
				}
				function postSavepublic( Request $request)
				{

					$rules = $this->validateForm();
					$validator = Validator::make($request->all(), $rules);	
					if ($validator->passes()) {
						$data = $this->validatePost('tb_events');		
						$this->model->insertRow($data , $request->input('id'));
						return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
					} else {

						return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
						->withErrors($validator)->withInput();

					}	

				}	
	public function getFields(Request $request)
	{
	    if(!\Auth::check() ) return \Redirect::to('login');
	    if($request->input('rows') != '' && $request->input('sort')=='' && $request->input('order')==''){
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = 'id';
	    	$order ='desc';
	    }
	    else{
	    	$rows = $request->input('rows') ? $request->input('rows') : 10 ;
	    	$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
	    	$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
	    }
	   // $rows = $request->input('rows') ? $request->input('rows') : 10 ;
	   // $sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
	   // $order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
	   
	   $this->data['pager'] 		= $this->injectPaginate();
// print_r($this->data['pager']);exit;
	    $query = \DB::table('tb_event_fields');
		if($request->get('order'))
	    {
	       $query->orderBy($sort,$order);
	    }
	    if($request->get('status') == 'active')
    {
    	$query->where('status','active')->orderBy($sort,$order);
    }
	  $query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

    if($request->get('field_categories'))
    {
        $query->where('field_categories','like','%'.$request->get('field_categories').'%')->orderBy($sort,$order);
        	
    }
     if($request->get('field_values'))
    {
        $query->where('field_values','like','%'.$request->get('field_values').'%')->orderBy($sort,$order);
        	
    }
	    $this->data['results'] = $query->paginate($rows); 
	  	$this->data['pageModule'] = '' ; 
	  	$this->data['field_categories'] = (!empty($request->get('field_categories')))?$request->get('field_categories'):'';
		$this->data['field_values'] = (!empty($request->get('field_values')))?$request->get('field_values'):'';
		$this->data['status'] = $request->get('status'); 
	    return view('event_fields.index',$this->data);
	}
	function getEventUpdate(Request $request, $id = null)
	{
	
		// if($id =='')
		// {
		// 	if($this->access['is_add'] ==0 )
		// 	return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		// }	
		
		// if($id !='')
		// {
		// 	if($this->access['is_edit'] ==0 )
		// 	return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		// }
		$row = $this->eventfields->find($id);

		if($id !='')
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->eventfields->getColumnTable('tb_event_fields');
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		return view('event_fields.form',$this->data);
	}	

	function postEventSave( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_event_fields');
			$data['field_categories'] = $request->input('field_categories');
			$data['field_values'] = $request->input('field_values');
			$data['status'] = $request->input('status');
			$data['created_date'] =date("Y-m-d H:i:s");
			$id = $this->eventfields->insertRow($data,$request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'event/fields/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'event/fields?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail($request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('event/fields/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
		}
		public function postDeleteEvent( Request $request)
		{
		
		
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->eventfields->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('event/fields?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('event/fields?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	
			

	
	}	
	
