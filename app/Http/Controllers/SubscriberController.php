<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Mail;
use App\Models\Sximo;

class SubscriberController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'subscriber';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Subscriber();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		$this->option = new Sximo();
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'subscriber',
			'return'	=> self::returnUrl()
			
		);
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		  if($sort=='status' && $order=='desc')
            $order='asc';
        else if($sort=='status' && $order=='asc')
            $order='desc';
        
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		if(!empty($request->input('search')))
		{
			if(empty( $results['rows']))
			{
		$this->data['no_record']	= \Lang::get('core.no_records');	}
		} 
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('tb_subscriber');
		$rows = $request->input('rows') ? $request->input('rows') : 10 ;
		$query = \DB::table('tb_subscriber');

		if($request->get('title'))
		{
			$query->where('email','like','%'.$request->get('title').'%')->orderBy($sort,$order);

		}
					$query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

// print_r($request->get('email'));exit;

		$this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['email'] = (!empty($request->get('title')))?$request->get('title'):'';	
		$this->data['status'] = $request->get('status');	
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('subscriber.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_subscriber'); 
		}

		$this->data['id'] = $id;
		return view('subscriber.form',$this->data);
	}	

	public function getShow( $id = null)
	{
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_subscriber'); 
		}
		
		$this->data['id'] = $id;
		$this->data['prevnext'] = $this->model->prevNext($id);
		$this->data['access']		= $this->access;
		return view('subscriber.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		$status = Input::get('status');
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_subscriber');
			$data['status'] = ($status=='on' ? '1':'0');
			if($id==''){
				$newID = $this->model->insertRow($data , $request->input('id'));
			}else{
				\DB::table('tb_subscriber')->where('id',$id)->update(['email'=>$data['email'],'status'=>$data['status']]);
			}
			
			if(!is_null($request->input('apply')))
			{
				$return = 'subscriber/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'subscriber?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('subscriber/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('subscriber')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('subscriber')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	
	function getSelected(Request $request){
		
		$ids = Input::get('id');
		$ids = explode(',', $ids);
		
		$emails = $this->model->select('email')->whereIn('id', $ids)->get();
		
		foreach($emails as $email){
			$usermail[] = $email->email; 
		}
		$mail = implode(',', $usermail);
		$this->data['email']=$mail;
		$this->data['umail']=$usermail;
		$this->data['template'] = \DB::table('tb_mail_template')->where('status',1)->get();
		return view('subscriber.mail',$this->data);
	}
	
	function getMail(Request $request, $id = 0){
		$this->data['email']='';
		$this->data['template'] = \DB::table('tb_mail_template')->where('status',1)->get();
		return view('subscriber.mail',$this->data);
	}
	function postSendmail(Request $request){
		
		$tomail = Input::get('tomail');
		$message = Input::get('message');
		$subject = Input::get('subject');

		if(empty($tomail)){
			$UserEmails = $this->model->where('status', '=', 1)->get();
		}else{
			$email = explode(',', $tomail);
			$UserEmails = $this->model->whereIn('email', $email)->get();			
		}
			
	
		if($UserEmails){
			foreach($UserEmails as $email)
			{
				// echo "<pre>";print_r(\Auth::user()->email);exit;

				$emails = $email->email;

				// echo "<pre>";print_r($message);

				// echo "<pre>";print_r($emails);
				// echo "<pre>";print_r(str_replace('{email}', $emails, $message));exit;

				$emessage = str_replace('{email}', $emails, $message);
				// $msg='<p>unsubscriber <a href="'.\URL::to('unsubscribe/'.$email->id).'">click Here</a></p>';
				$data = array( 'content' =>strip_tags($emessage),
					'links' =>$this->option->get_options('links'),);
				// print_r($data);exit;
		  		Mail::send('subscriber.email', $data, function($message) use($emails,$subject)
				{
				    $message->from('ramyabsetec@gmail.com');
				    $message->subject($subject);
				    $message->to($emails);
				});
			}

			// $mail = implode(',', $emails);
			// $result = \DB::table('cron_mail')->insert(['users'=>$mail,'message'=>$message,'subject'=>$subject]);
					
		}else{
		   return Redirect::to('subscriber')->with('flashErro', 'There is no users');  
		}
		if(empty($tomail)){
		return Redirect::to('subscriber')->with('messagetext',\Lang::get('core.t_blastemailsmall'))->with('msgstatus','success');
		}
		else{
		return Redirect::to('subscriber')->with('messagetext','Send mail to selected users')->with('msgstatus','success');	
		}
	}

       public function postAddsubscribe(Request $request)
	{
		$email = $request->input('email');
		$new = 0;

        if($email)
        {  
			if($email != '')
			{
				$check_exist = $this->model->where('email',$email)->first();
				if(isset($check_exist['id']) && $check_exist['id'] != '')
				{
					echo 'You have already subscribed';
					die;
				}else{
					$subscribe = new Subscriber();
					$subscribe->email = $email;
					$subscribe->created_at = date('Y-m-d H:i:s');
					$subscribe->updated_at = date('Y-m-d H:i:s');
					$subscribe->status = 1;
					$subscribe->save();	
					$new = 1;
					 echo 1;
				}
			}
			// if($new)
			// {
			// 	if($email)
			// 	{
			// 		$fromMail = CNF_EMAIL;
			// 		$to = $email;
			// 		$subject = "[ " .CNF_APPNAME." ] Thank you for subscribed to our news letter";
			// 		$data = array( 'unsub_id' => $subscribe['id'] );
			// 		$tempname1   =  'emails.subscribed';
			// 		$this->notify->sendMail($fromMail,$to,$subject,$data,$tempname1);
			// 	}
			// 	echo 1;
			// }			
		}else{
			echo 'Please enter your Email address';
		}
	}




}
