<?php namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
//use Input;
//use Validator;
use DB;
use App\Helper\Helper;
use Carbon\Carbon;
use SEOMeta;
use Purifier;
use Response;
use Youtube;
//use Image;
use App\Models\Sximo;


use App\Http\Controllers\controller;
use App\Models\Modele;
use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Intervention\Image\ImageManagerStatic as Image;



class UploaderController extends Controller
{

    protected $layout = "layouts.main";
    protected $data = array();  
    public $module = 'model';
    static $per_page    = '10';

    protected $iLoggedId            = false;
    protected $sUserType            = false;
    protected $bLogged              = false;
    protected $aUserInfo   = array();      

    public function __construct()
    {
        
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->model = new Modele();
        $this->message = new Message();
        $this->option = new Sximo();

        $this->info = $this->model->makeInfo( $this->module);
        $this->access = $this->model->validAccess($this->info['id']);
        // user details
        $this->iLoggedId  = \Session::get('logged_id');
        $this->sUserType  = \Session::get('user_type');
        $this->bLogged    = !empty($this->iLoggedId)?true:false;
        $this->aUserInfo  = \SiteHelpers::getUserInfo($this->iLoggedId,$this->sUserType);    

        $this->data = array(
            'pageTitle' =>  $this->info['title'],
            'pageNote'  =>  $this->info['note'],
            'pageModule'=> 'model',
            'return'    => self::returnUrl()
            
        );
        
        \App::setLocale(CNF_LANG);
        if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

        $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
        \App::setLocale($lang);
        }  

    }

    public function photoView(Request $request,$uri){
				$photo_enc_id = $uri;
               $uri=\SiteHelpers::getSecurityurlID($uri);

            // get Album Info
        // $aImage     = Helper::getPhotoInfo($uri,'MediaUri');
        $aImage     = Helper::getPhotoInfo($uri,'ID'); // get image by Id instead of URI
            // echo "<pre>"; print_r($aImage); exit;
        if(!empty($aImage)){

             // set album id
            $iImageId   = $aImage->ID;

            // Get Visitor IP
            $ip          = Helper::get_visitorIP();

            // Get Visitor Data 
            $data        = Helper::get_country($ip);

            // country Code
            $countryCode = $data[0];

            // Country Name
            $country     = $data[1];

            // Visitor City
            $city        = $data[2];

            // Visitor Browser 
            $browser     = Helper::get_browser();

            // Get Visitor Plat Form
            $platform    = Helper::get_platform();

            // Check if Mobile or Computer
            $device      = Helper::is_mobile();


            // Check if Already view this video or it's first time
            $check_view  = DB::table('photo_views')
            ->where('photo_id', $iImageId)
            ->where('ip', $ip)
            ->first();
            // check user already visited the page
            if (!$check_view) {
            // It's First Time Insert Data
                DB::table('photo_views')->insert([
                    'photo_id'     => $iImageId,
                    'owner'        => $aImage->OwnerId,
                    'ip'           => $ip,
                    'country'      => $country,
                    'country_code' => $countryCode,
                    'city'         => $city,
                    'browser'      => $browser,
                    'platform'     => $platform,
                    'device'       => $device,
                    'created_at'   => Carbon::now(),
                    'updated_at'   => Carbon::now(),
                ]);
            }

            // Count Total Views
            $views = DB::table('photo_views')->where('photo_id', $iImageId)->count();

            // Count Likes 
            $likes = DB::table('photo_votes')->where('photo_id', $iImageId)
            ->where('photo_like', 1)
            ->count(); 

            // Count Dislikes 
            $dislikes = DB::table('photo_votes')->where('photo_id', $iImageId)
            ->where('photo_dislike', 1)
            ->count();

            // Get All video Comments
            $comments = DB::table('photo_comments')->where('photo_id', $iImageId)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->paginate(5);
            // Count Comments
            $total_comments = DB::table('photo_comments')->where('photo_id', $iImageId)
            ->where('status', 1)
            ->count();

            $aImages = DB::table('tb_images')
            ->where('AlbumId', $aImage->AlbumId)
            ->where('ID', '!=' , $aImage->ID)
             ->where('Status', '=' , 'Active')
            ->orderBy('id', 'desc')
            ->get();

            $aAlbums = DB::table('tb_albums')
            ->where('ownerid',   '=' , $aImage->OwnerId)
            ->where('user_type', '=' , $aImage->user_type)
            ->where('status', '=' , 'active')
            ->orderBy('album_id', 'desc')->get();

            // echo "<pre>"; print_r($aAlbums); exit;

           

           $this->data = array();

            // echo "<pre>"; print_r($aImage); exit;
            $this->data['pageTitle']    = 'Photo View';
            $this->data['breadcrumb']   = 'inactive';   
            $this->data['pageMetakey']  = '' ;
            $this->data['pageMetadesc'] = ''; 
            $this->data['gplusimg'] = '';
            $this->data['tweetimg'] = '';
            $this->data['v_url'] = ''; 
            $this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');           
            $this->data['pages']        = 'layouts.'.CNF_THEME.'.template.page';
            $sql = \DB::table('tb_pages')->where('default',1)->get();

            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Photos';
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] = '';
                $this->data['gplusimg'] = '';
                $this->data['tweetimg'] = '';
                $this->data['v_url'] = ''; 
                $this->data['urls'] = '';
                $this->data['fbimg'] = '';
                $this->data['links']=$this->option->get_options('links');
                $this->data['filename']     =  'watch';             

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }


                //$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

                // image info
               $this->data['image']             = $aImage;
                // get comments block
               $this->data['comments']          = $comments;
                // get total comments
               $this->data['total_comments']    = $total_comments;
                // get photo likes
               $this->data['likes']             = $likes;
                // get photo likes
               $this->data['dislikes']          = $dislikes;   
                // Realate album images
               $this->data['images']            = $aImages; 
                // albums
               $this->data['albums']            = $aAlbums; 
               $this->data['favourite'] = \SiteHelpers::getfavourites($aImage->ID,'image');
               $this->data['userId'] =$this->iLoggedId;// sending userid

                // get Sum ratings
                $ratedCounts =\DB::table('photo_comments')
                ->selectRaw('sum(rating) as sum,count(rating) as total_count')
                ->where('photo_id',$iImageId)
                ->first();
                $sum=$ratedCounts->sum;
                 if($sum>0)
                    $total=number_format((float)($sum)/($ratedCounts->total_count), 1, '.', '');
                else
                    $total='Not yet Rated';

                //Total counts for particular videos 
               $this->data['avg_ratings']=$total;

                $img =  \SiteHelpers::user_albums($aImage->ID,'',$aImage->Ext);

                //$u =  url('/photo') .'/'. $aImage->MediaUri;
                $u =  url('/photo') .'/'. $photo_enc_id;

                //$this->data["fbimg"] = (!empty($img))?$img:'';

                //$this->data['tweetimg'] = (!empty($img))?$img:'';

                //$this->data['gplusimg'] = (!empty($img))?$img:'';

                //$this->data["urls"] = (!empty($u))?$u:'';

                $this->data['pages'] = 'photo.view';
				$this->data['photo_enc_id'] = $photo_enc_id;
				
                // $this->data['content'] = view('photo.view',$data);
                $page = 'layouts.'.CNF_THEME.'.index';

                return view($page,$this->data);   
                //return view('videos.watch')->with($video_data);
            }        

        }
    }


    /************* Like a Video **************/
    public function PhotoLike(Request $request)
    {
        if ($request->ajax()) {
            // Check if User Login or Not
            if ($this->bLogged) {

                // Get User ID
                $user_id = $this->iLoggedId;

                // Get username
                $username = $this->aUserInfo->username;
                
                // Get Video ID
                $iId = $request->get('videoID');

                // Check if video exists
                $checkVid = DB::table('tb_images')->where('ID', $iId)->where('status', 'Active')->first();

                if (!$checkVid) {
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Image not exists.',
                    );
                    return Response::json($response);
                }else{
                    // if (!Helper::is_vip()) {
                    //     if ($checkVid->vip == 1) {
                    //         $response = array(
                    //             'status' => 'error',
                    //             'msg'    => 'Oops! Please upgrade your account to like this video.',
                    //         );
                    //         return Response::json($response);
                    //     }
                    // }
                }

                // Check if Already liked or disliked
                $check_vote = DB::table('photo_votes')
                              ->where('user_id', $user_id)
                              ->where('user_type', $this->sUserType)
                              ->where('photo_id', $iId)
                              ->first();

                if ($check_vote) {

                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! You already Liked this image.',
                    );
                    return Response::json($response);
                }else{
                    // Insert Video in liked list
                    DB::table('photo_votes')->insert([
                        'photo_id'      => $iId,
                        'user_id'       => $user_id,
                        'user_type'     => $this->sUserType,
                        'photo_like'    => 1,
                        'photo_dislike' => 0,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);

                    // Count Likes
                    $likes = DB::table('photo_votes')
                             ->where('photo_id', $iId)
                             ->where('photo_like', 1)
                             ->count();


                    // Send Notification
                    if ((Helper::photo_owner($iId)) != ($username)) {

                        DB::table('notifications')->insert([
                            // 'notice_to'   => Helper::photo_owner($videoID),
                            'notice_to'   => Helper::photo_owner($iId),
                            'notice_from' => $username,
                            'type'        => 'like',
                            // 'details'     => $videoID,
                            'details'     => $iId,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);

                    }

                    // Return Success
                    $response = array(
                        'status' => 'success',
                        'likes'  => $likes,
                        'msg'    => 'Success! image added to liked list.',
                    );
                    return Response::json($response);
                }
                

            }else{
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Please login to rate this image.',
                );
                return Response::json($response);
            }
        }
    }    



    /************* Dislike a Video **************/
    public function PhotoDislike(Request $request)
    {
        if ($request->ajax()) {
            // Check if User Login or Not
            if ($this->bLogged) {

                // Get User ID
                $user_id = $this->iLoggedId;

                // Username
                $username = $this->aUserInfo->username;
                
                // Get Video ID
                $iId = $request->get('videoID');

                // Check if video exists
                $checkVid = DB::table('tb_images')->where('ID', $iId)->where('status', 'Active')->first();

                if (!$checkVid) {
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! image not exists.',
                    );
                    return Response::json($response);
                }else{
                    if (!Helper::is_vip()) {
                        if ($checkVid->vip == 1) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! Please upgrade your account to dislike this image.',
                            );
                            return Response::json($response);
                        }
                    }
                }

                // Check if Already liked or disliked
                $check_vote = DB::table('photo_votes')
                              ->where('user_id', $user_id)
                              ->where('user_type', $this->sUserType)
                              ->where('video_id', $iId)
                              ->first();

                if ($check_vote) {

                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! You already Liked this image.',
                    );
                    return Response::json($response);
                }else{
                    // Insert Video in disliked list
                    DB::table('photo_votes')->insert([
                        'photo_id'      => $iId,
                        'user_id'       => $user_id,
                        'photo_like'    => 0,
                        'photo_dislike' => 1,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);

                    // Send Notification
                    if ((Helper::photo_owner($iId)) != ($username)) {

                        DB::table('notifications')->insert([
                            'notice_to'   => Helper::photo_owner($iId),
                            'notice_from' => $username,
                            'type'        => 'dislike',
                            'details'     => $iId,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);
                        
                    }

                    // Count Dislikes
                    $dislikes = DB::table('photo_votes')
                                ->where('photo_id', $iId)
                                ->where('photo_dislike', 1)
                                ->count();

                    // Return Success
                    $response = array(
                        'status'    => 'success',
                        'dislikes'  => $dislikes,
                        'msg'       => 'Success! Image added to disliked list.',
                    );
                    return Response::json($response);
                }
                

            }else{
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Please login to rate this image.',
                );
                return Response::json($response);
            }
        }
    }




    /***** Submit New Comment *****/
    public function PhotoComment(Request $request, $id)
    {
        // Check if AJAX Request
        if ($request->ajax()) {

            // Check if Video Exists
            $aImage = DB::table('tb_images')->where('ID', $id)->first();

            if ($aImage) {

/*                if (!Helper::is_vip()) {
                    if ($aImage->vip == 1) {
                        $response = array(
                            'status' => 'error',
                            'msg'    => 'Oops! Please upgrade your account to like this video.',
                        );
                        return Response::json($response);
                    }
                }*/

                // Get Comment Value
                $comment = Purifier::clean($request->get('comment'));
                // get rating value
                // $rating  = Purifier::clean($request->get('rating'));
                   $rating  =$request->get('rating');
                // Check if User Login or Not
                if (!$this->bLogged) {
                    // Not Login
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Please login to add a comment.',
                    );
                    return Response::json($response);
                }else{
                    // Username
                    $username = $this->aUserInfo->username;

                    // if(empty($rating)){
                    //     $response = array(
                    //         'status' => 'error',
                    //         'msg'    => 'Oops! Please Choose Your Rating.',
                    //     );
                    //     return Response::json($response);
                    // }                    
                    
                    // Make a Validation
                    $validator = Validator::make($request->all(), [
                        'comment' => 'required|max:500' 
                    ]);

                    if ($validator->fails() || empty($comment)) {
                        $response = array(
                            'status' => 'error',
                            'msg'    => 'Oops! Comment required and max characters is 500.',
                        );
                        return Response::json($response);
                    }else{
                        // check if already post the same comment
                        $double_cm = DB::table('photo_comments')
                        ->where('photo_id', $id)
                        ->where('user_id', $this->iLoggedId)
                        ->where('comment', $comment)
                        ->first();

                        if ($double_cm) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! You already added the same comment.',
                            );
                            return Response::json($response);
                        }

                        // Check Comment status
                        if ($this->aUserInfo->status == 1 && $this->sUserType != 'admin') {
                            $status = 1;
                        }

                        $auto_approve_comments = DB::table('settings_upload')
                        ->where('id', 1)
                        ->first();

                        if ($auto_approve_comments->auto_approve_comments == 0) {
                            $status = 1;
                        }else{
                            $status = 0;
                        }

                        // Insert Comment
                        $iCmtId = DB::table('photo_comments')->insertGetId([
                            'photo_id'   => $id,
                            'user_id'    => $this->iLoggedId,
                            'user_type'  => $this->sUserType,
                            'status'     => $status,
                            'comment'    => $comment,
                            'rating'    => $rating,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);

                        // Send Notification
                        if ((Helper::photo_owner($id)) != ($username)) {

                            DB::table('notifications')->insert([
                                'notice_to'   => Helper::photo_owner($id),
                                'notice_from' => $username,
                                'type'        => 'comment',
                                'details'     => $id,
                                'is_read'     => 0,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now(),
                            ]);

                        }

                       $response = array(
                            'status' => 'success',
                            'msg'    => 'Congartulations! Comment has been successfully added.',
                            'cmt_id' =>  $iCmtId,
                            'html'   => '<li class="commentScroll"><div class="comment-main-level">
                             <div class="comment-avatar">
                                    <img src="'.\App\Helper\Helper::user_avatar(\Session::get('logged_id'),\Session::get('user_type')).'" alt=""></div>
                                    <!-- Comment Details -->
                                    <div class="comment-box">
                                        <div class="comment-head">
                                            <!-- Uploader -->
                                            <h6 class="comment-name by-author"><a href="http://192.168.1.65:81/magelur/public/user/Baskaran" target="_blank">'.\App\Helper\Helper::channel_by_id($this->iLoggedId,$this->sUserType).'</a>
                                             <!-- Comment Date -->
                                            <span class="cm_date">'.\App\Helper\Helper::change_date_format(Carbon::now()).'</span>
                                            </h6>
                                           
                                        </div>

                                        <select data-rate="'.$rating.'" id="comment_id_'.$iCmtId.'" style="display: none;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        </select>

                                        <div class="comment-content">
                                           '.nl2br( \App\Helper\Helper::emoticons(\App\Helper\Helper::active_links($comment, $id))) .'
                                        </div>
                                    </div>
                             </div>
                         </li>'
                        );
                        return Response::json($response);
                    }

                }

            }else{
                // Video Doesn't exists
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Image Doesn\'t Exists.',
                );
                return Response::json($response);
            }
        }   
    }

    public function getModelPhotoAlbum(){
        //  show this page , only to logged users
        if(empty($this->iLoggedId)){
            return Redirect::to('/login');
        }        
        return $this->getAlbumListing('model');
    }

    public function getAgentPhotoAlbum(){
        //  show this page , only to logged users
        if(empty($this->iLoggedId)){
            return Redirect::to('/login');
        }        
        return $this->getAlbumListing('agent');
    }

     // to delete album by user
    public function getDeleteAlbum($album_id){
         //  get current user id & user type
        $iLoggedId = $this->iLoggedId;
        $sUserType = $this->sUserType;

        if(empty($iLoggedId)){
        return Redirect::to('login');
        }

        \DB::table('tb_albums')->where('album_id',$album_id)
                               ->where('ownerid',$iLoggedId)
                               ->where('user_type',$sUserType)->update(['status'=>'deleted']);
       \DB::table('tb_images')
       ->where('AlbumId','=',$album_id)
       ->update(['Status'=>'deleted']);
       $iLoggedId =urlencode( base64_encode($iLoggedId));
        if($sUserType=="agent")
        {
            return Redirect::to('agent/album/'.$iLoggedId)->with('messagedelete','Your Album has been deleted Successfully !');
        }  
        else if($sUserType=="model")   
        {
            return Redirect::to('model/album/'.$iLoggedId)->with('messagedelete','Your Album has been deleted Successfully !');
        }             
    

    }
    //end


    // to delete images by user
    public function getDeletePhoto($image_id){
         //  get current user id & user type
        $iLoggedId = $this->iLoggedId;
        $sUserType = $this->sUserType;

        if(empty($iLoggedId)){
        return Redirect::to('login');
        }

         // Delete photo stats
        \DB::table('photo_views')->where('photo_id', $image_id)->delete();

        // Delete photo Votes
        \DB::table('photo_votes')->where('photo_id', $image_id)->delete();

        // Delete photo Comments
        \DB::table('photo_comments')->where('photo_id', $image_id)->delete();

        //Delete from favorites
            $fav_image = DB::table('tb_favorites')->where('favourite_id', $image_id)
                                        ->get();
            if($fav_image)
                {
                foreach ($fav_image as $key => $value) {
                     DB::table('tb_favorites')->where('favourite_id', $image_id)->delete();
                }
            }
            
        
        \DB::table('tb_images')->where('ID',$image_id)
                               ->where('OwnerId',$iLoggedId)
                               ->where('user_type',$sUserType)->delete();

        $iLoggedId =urlencode( base64_encode($iLoggedId));
        if($sUserType=="agent")
        {
            return Redirect::to('agent/album/'.$iLoggedId)->with('messagedelete','Your Image has been deleted Successfully !');
        }  
        else if($sUserType=="model")   
        {
            return Redirect::to('model/album/'.$iLoggedId)->with('messagedelete','Your Image has been deleted Successfully !');
        }             
    

    }
    //end

    public function getAlbumListing($sType){

        if(empty($this->iLoggedId) || empty($sType) ){
            return Redirect::to('/login');
        }



          $sql = \DB::table('tb_pages')->where('default',1)->get();
          if(count($sql)>=1)
          {
            $row = $sql[0];

            $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
            $this->data['breadcrumb']   = 'inactive';   
            $this->data['pageMetakey']  =   '' ;
            $this->data['pageMetadesc'] = '';
            $this->data['gplusimg'] = '';
            $this->data['tweetimg'] = '';
            $this->data['v_url'] = ''; 
            $this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');
            $this->data['filename']     =  'videos';             

            if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
            {
                $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
            } else {
                $page_template = 'layouts.'.CNF_THEME.'.template.page';
            }


    
            // Get Videos
           $this->data['albums'] = DB::table('tb_albums')
                              ->where('status','active')
                              ->where('user_type',$sType)
                               ->orderBy('album_id', 'desc')
                              ->paginate(20);

            $this->data['user_type'] = $sType;   
            $this->data['pages'] = 'photo.album_listing';
                           
            // $this->data['content'] = view('photo.album_listing',$data);
            $page = 'layouts.'.CNF_THEME.'.index';

            return view($page,$this->data);   

        }else{
            // video does not exists redirect to home page
            return redirect('/'); 
        }

    }


    public function getPhotosAlbum(Request $request, $iAlbumUri){
$sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];
        if(empty($this->iLoggedId)){
            return Redirect::to('/login');
        }
        
        // get Album Info
        $aAlbum     = Helper::getAlbumInfo($iAlbumUri,'uri');
        // echo "<pre>";print_r($aAlbum);exit;

        if($aAlbum){
            // set album id
            $iAlbumId   = $aAlbum->album_id;

            // Get Visitor IP
            $ip          = Helper::get_visitorIP();

            // Get Visitor Data 
            $data        = Helper::get_country($ip);

            // country Code
            $countryCode = $data[0];

            // Country Name
            $country     = $data[1];

            // Visitor City
            $city        = $data[2];

            // Visitor Browser 
            $browser     = Helper::get_browser();

            // Get Visitor Plat Form
            $platform    = Helper::get_platform();

            // Check if Mobile or Computer
            $device      = Helper::is_mobile();


            // Check if Already view this video or it's first time
            $check_view  = DB::table('album_views')
            ->where('album_id', $iAlbumId)
            ->where('album_type', 'photos')
            ->where('ip', $ip)
            ->first();

            if (!$check_view) {
            // It's First Time Insert Data
            DB::table('album_views')->insert([
            'album_id'     => $iAlbumId,
            'album_type'   => 'photos',
            'owner'        => $aAlbum->ownerid,
            'ip'           => $ip,
            'country'      => $country,
            'country_code' => $countryCode,
            'city'         => $city,
            'browser'      => $browser,
            'platform'     => $platform,
            'device'       => $device,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
            ]);
            }


        // get Album Related Images
        $aResults   = Helper::getPhotosByAlbum($iAlbumId,'Active');
        $aResults1   = Helper::getPhotosByAlbum($iAlbumId,'Inactive'); // check how many inactive
        $aResults2   = Helper::getPhotosByAlbum($iAlbumId,'Deactivated'); // Check how many deactivated
        // echo "<pre>";print_r($aResults);exit;
        $this->data = array(
            'results' => $aResults,
            'album'   => $aAlbum,
            'total_images'=>count($aResults)+count($aResults1)+count($aResults2), //get total images value
            'userId'=>$this->iLoggedId //get userid
        );

        }
        // $this->data['content']      = view('videos.photo_album',$this->data);
        $this->data['pageTitle']    = 'Photo Album';
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['v_url'] = ''; 
        $this->data['urls'] = '';
        $this->data['fbimg'] = '';     
        $this->data['links']=$this->option->get_options('links');
        $this->data['filename']  =  'photo_album';                

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                $this->data['pages'] ='videos.photo_album';

        $page = 'layouts.'.CNF_THEME.'.index';

        return view($page,$this->data);  
            }       
    }

    public function getPhotoUpload(){

     if(empty($this->iLoggedId)){
           return Redirect::to('/login');
     }   

     //Restrict users to upload Images based on Image Limit

    $iLoggedId = $this->iLoggedId;
    $sUserType = $this->sUserType; 

            
    $image_status=1;
    $album_limit = 0;
    if($sUserType=='agent')
    {
        // get agency details
        $agent_details = \DB::table('tb_agency')->where('id', '=', $this->iLoggedId)->where('status','=','1')->where('verification_status','=','1')->first();
        // get album details
        $image_details = \DB::table('tb_albums')->where('ownerid', '=', $this->iLoggedId)->where('user_type','=',$sUserType)->get();
        //get membership plan details
        $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $agent_details->plan_id)->where('status','=','1')->first();
        $album_limit = $agent_details->photoplan;
         // echo "<pre>";print_r($album_limit);die;
         // echo "<pre>";print_r($membership_details);die;
        //check how many images agent has uploaded
         $image_count=0;
        if(count($image_details)>=0)
        {
            
            //check image limit with number of images uploaded by agents
           if(isset($membership_details->image_limit)){
            //check image limit with number of images uploaded by agents
            if($membership_details->image_limit!="unlimited" && count($image_details)>$album_limit)
            {
                $image_status=1;
            }

            elseif($membership_details->image_limit==count($image_details) ||  $album_limit>count($image_details)) 
            {


                foreach ($image_details as  $value) 
                {

// echo "<pre>";print_r('Image Limit: '.$value->count);

                    
                        $image_count=$image_count+$value->count;

// echo "<pre>";print_r('calc Image Limit: '. $image_count);

                    
                }
                
                if($image_count>=CNF_ALBUM*count($image_details) &&  $album_limit==count($image_details))
                {
                    $image_status=0;
                }
            }
        }
        }
       
    }
    // check for model
    else if($sUserType=='model')
    {
        // get model details
        $model_details = \DB::table('tb_models')->where('id', '=', $this->iLoggedId)->where('status','=','1')->where('approved_status','=','1')->first();
        // get images details
        $image_details = \DB::table('tb_albums')->where('ownerid', '=', $this->iLoggedId)->where('user_type','=',$sUserType)->get();
        //get membership plan details
        $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $model_details->plan_id)->where('status','=','1')->first();
        $album_limit = $model_details->photoplan;
         // echo "<pre>";print_r($image_details);
         // echo "<pre>";print_r($membership_details);die;
        //check how many images agent has uploaded
         $image_count=0;
        if(count($image_details)>=0)
        {
            
            //check image limit with number of images uploaded by agents
           if(isset($membership_details->image_limit)){
            //check image limit with number of images uploaded by agents
                          
            // if($membership_details->image_limit!="unlimited" && ($album_limit==count($image_details) || count($image_details)>$album_limit))
            
                 if($membership_details->image_limit!="unlimited" && (count($image_details)>$album_limit))
            {
                $image_status=1;
            }

            elseif($membership_details->image_limit==count($image_details) || $album_limit>count($image_details)) 
            {

                foreach ($image_details as  $value) 
                {

                    // echo "<pre>";print_r('Image Limit: '.$value->count);
                    $image_count=$image_count+$value->count;
                    // echo "<pre>";print_r('calc Image Limit: '. $image_count);

                }
                
                if($image_count>=CNF_ALBUM*count($image_details) && $album_limit==count($image_details))
                {
                    $image_status=0;
                }
            }
        }
        }
          // echo "<pre>";print_r('Membership Image Limit: '.$membership_details->image_limit);

         // echo "<pre>";print_r('User Album Limit: '.$album_limit);

         // echo "<pre>";print_r('USed Album Limit: '.count($image_details));die;
        
    }
    
       $iLoggedId =urlencode( base64_encode($iLoggedId));

    if($image_status==0 && $sUserType=='agent')
    {
        return Redirect::to('agent/album/'.$iLoggedId)->with('messagetext','Upgrade Your membership to Upload more Images !')->with('msgstatus','error');
    }
    elseif ($image_status==0 && $sUserType=='model') 
    {

       return Redirect::to('model/album/'.$iLoggedId)->with('messagetext','Upgrade Your membership to Upload more Images !')->with('msgstatus','error');
    }

    //end

     $sql = \DB::table('tb_pages')->where('default',1)->get();
     if(count($sql)>=1)
     {
        $row = $sql[0];

        $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['v_url'] = ''; 
        $this->data['urls'] = '';
        $this->data['fbimg'] = '';
        $this->data['links']=$this->option->get_options('links');
        $this->data['filename']     =  'upload';             

        $this->data['pages'] = 'layouts.'.CNF_THEME.'.template.page';
        $this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

            $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] = '';
                $this->data['gplusimg'] = '';
                $this->data['tweetimg'] = '';
                $this->data['v_url'] = ''; 
                $this->data['urls'] = '';
                $this->data['fbimg'] = '';
                $this->data['links']=$this->option->get_options('links');
                $this->data['filename']     =  'upload';             

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                $this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

                $this->data['albums'] = \DB::table('tb_albums')
                                 ->where('ownerid',$this->iLoggedId)
                                 ->where('user_type',$this->sUserType)
                                 ->get();
                $this->data['albumss'] = \DB::table('tb_albums')
                                 ->where('ownerid',$this->iLoggedId)
                                 ->where('user_type',$this->sUserType)
                                 ->where('status','active')
                                 ->get();                 
                $this->data['album_limit'] =$album_limit; //sending Image Limit
                $this->data['iLoggedId'] =$iLoggedId; //sending logged id Limit
                $this->data['sUserType'] =$sUserType; //sending logged user type

                // $this->data['gallery'] = $this->model->getModelImages($iModelId);
                $this->data['pages'] ='model.photo_upload';

                // $this->data['content'] = view('model.photo_upload',$this->data);
                $page = 'layouts.'.CNF_THEME.'.index';


                return view($page,$this->data);         
                 }

        $this->data['pages'] ='model.photo_upload';
        $page = 'layouts.'.CNF_THEME.'.index';


        return view($page,$this->data);         
    }
}


    /* View Upload page */
    public function index()
    {  
        // Check if user online
        
        if(\Session::has('model_id') || \Session::has('agent_id')) { 
           //Restrict users to upload videos based on Video Limit

            $iLoggedId = $this->iLoggedId;
            $sUserType = $this->sUserType; 

            
        $video_status=1;
        if($sUserType=='agent')
        {
            // get agency details
            $agent_details = \DB::table('tb_agency')->where('id', '=', $this->iLoggedId)->where('status','=','1')->where('verification_status','=','1')->first();
            // get video details
            $video_details = \DB::table('videos')->where('user_id', '=', $this->iLoggedId)->where('user_type','=',$sUserType)->get();
            //get membership plan details
            $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $agent_details->plan_id)->where('status','=','1')->first();
             // echo "<pre>";print_r($membership_details);die;
            //check how many events agent has created
            if(count($video_details)>=0 )
            {
                if(isset($membership_details->video_limit))
                {
                    //check event limit with number of events created by agents
                if($membership_details->video_limit!="unlimited" && ($agent_details->videoplan==count($video_details)|| count($video_details)>$agent_details->videoplan))
                    {
                        $video_status=0;
                    }
                }
            }
             //  echo "<pre>";print_r(count($video_details));

             // echo "<pre>";print_r($agent_details->videoplan);die;
        }
        // check for model
        else if($sUserType=='model')
        {
            // get model details
            $model_details = \DB::table('tb_models')->where('id', '=', $this->iLoggedId)->where('status','=','1')->where('approved_status','=','1')->first();
            // get video details
            $video_details = \DB::table('videos')->where('user_id', '=', $this->iLoggedId)->where('user_type','=',$sUserType)->get();
            //get membership plan details
            $membership_details = \DB::table('tb_membership_plan')->where('plan_id', '=', $model_details->plan_id)->where('status','=','1')->first();
            
            //check how many events agent has created
            if(count($video_details)>=0  )
            {
                if(isset($membership_details->video_limit))
                {
                    //check event limit with number of events created by agents
                    // if($membership_details->video_limit!="unlimited" && ($membership_details->video_limit==count($video_details)|| count($video_details)>$membership_details->video_limit))
                     if($membership_details->video_limit!="unlimited" && ($model_details->videoplan==count($video_details)|| count($video_details)>$model_details->videoplan))
                    {
                        $video_status=0;
                    }
                }
            }
             

        }
       

       $iLoggedId =urlencode( base64_encode($iLoggedId));

        if($video_status==0 && $sUserType=='agent')
        {
            return Redirect::to('agent/video/'.$iLoggedId)->with('messagetext','Upgrade your membership to Upload more Videos !')->with('msgstatus','error');
        }
        elseif ($video_status==0 && $sUserType=='model') 
        {
            return Redirect::to('model/video/'.$iLoggedId)->with('messagetext','Upgrade your membership to Upload more Videos !')->with('msgstatus','error');
        }

        //end
            // Get User ID
          //  $user_id           = Auth::user()->id;
        $iModelId         = \Session::get('logged_id');

        $aModelInfo       = $this->aUserInfo;
        // $iTotalUserFoto   = $aModelInfo->photo_count;

        // $iFotoLimit       = $aModelInfo->photo_limit;

            // Get All Categories
            $categories        = DB::table('categories')->get();

            // Get Security Settings
            $settings_security = DB::table('settings_security')->where('id', 1)->first();

            // Count User Storage
            $total_videos_size = DB::table('videos')
                                 ->where('user_id', $iModelId)
                                 ->sum('size');

            // Get Upload Settings
            $settings_upload = DB::table('settings_upload')
                               ->where('id', 1)->first();

            // Send Data as Array
            $this->data = array(
                'categories'        => $categories,
                'total_videos_size' => $total_videos_size,
                'settings_upload'   => $settings_upload,
                'settings_security' => $settings_security,
            );
            
            $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] = '';
                $this->data['gplusimg'] = '';
                $this->data['tweetimg'] = '';
                $this->data['v_url'] = ''; 
                $this->data['urls'] = '';
                $this->data['fbimg'] = '';
                $this->data['links']=$this->option->get_options('links');
                $this->data['filename']     =  'upload';             

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                $this->data['pages'] = 'model.upload';
                $data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

                // $data['gallery'] = $this->model->getModelImages($iModelId);

                // $this->data['content'] = view('model.upload',$data);
                $page = 'layouts.'.CNF_THEME.'.index';


                return view($page,$this->data);         
                 }

           // return view('upload')->with($data);

        }else{
            //return redirect('/login');
            return Redirect::to('/login');
        }
    }


    /******* Start Uploading Videos *******/
    public function StartUpload(Request $request)
    {
        $description = $request->get('description');
         
        //  get current user id & user type
        $iLoggedId = $this->iLoggedId;

        $sUserType = $this->sUserType;  
        if($sUserType == 'model'){
        $iLoggedName= \Session::get('model_name');      
        }else{
         $iLoggedName= \Session::get('agent_name');   }
        // check if admin 
        if (Helper::is_admin()) {
            $vip = $request->get('vip');
        }else{
            $vip = 0;
        }

        // Get Upload Settings
        $settings_upload = DB::table('settings_upload')->where('id', 1)->first();

        // Check if Admin Allow Upload from sites
        if ( ($settings_upload->upload_type == 1) OR $settings_upload->upload_type == 2 ) {
            
            // Get Video URL 
            $video_sites = $request->get('video_sites');

            if ($video_sites) {

                // Validate Video URL
                $validateVidURL = Validator::make($request->all(), [
                    // 'video_sites' => 'required|url'
                ]);

                if ($validateVidURL->fails()) {
                    
                    // Error
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Video URL is Invalid', 
                    );
                   return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video URL is Invalid'))->with('msgstatus','error');

                   return Response::json($response);

                }

                $video_url_type = Helper::check_vid_url($video_sites);
                // Check if valid URL
                if (!$video_url_type) {

                    // Error
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Only dailymotion, vimeo and youtube url allow.', 
                    );

                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Only dailymotion, vimeo and youtube url allow.'))->with('msgstatus','error');

                    // return Response::json($response);

                }else{

                    // Youtube ID
                    if ($video_url_type == 'youtube') {

                        // Youtube ID
                        $youtube_id = Helper::youtube_id($video_sites);

                        // Check if Valid YouTube ID
                        $youtube = Youtube::getVideoInfo($youtube_id);

                        // Get Youtube Video
                        $youtube_vid = $request->get('youtube_id');

                        // Video Duration
                        $video_duration = Helper::youtube_duration($youtube->contentDetails->duration);
                        // echo "Video Duration for youtube:".$video_duration;die;

                        //Check Duration with membership Plan video duration limit
                        $check_duration = Helper::checkVideoDuration($iLoggedId,$sUserType,$video_duration);
                        if($check_duration==FALSE)
                        {
                            $response = array(
                                            'status' => 'error', 
                                            'msg'    => 'Oops! Video File Duration is more than expected.', 
                                        );
                            return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video File Duration is more than expected.'))->with('msgstatus','error');
                        }
                        //end

                        // Video Preview & Thumb
                        $preview_url = Helper::youtube_cover($youtube_id);
                        $thumb_url   = Helper::youtube_cover($youtube_id);

                        // Youtube Video Quality
                        $youtube_quality     = Helper::is_youtube_hd($youtube->contentDetails->definition);

                        if ($youtube_quality == TRUE) {

                            $video_width  = '1280';
                            $video_height = '720';

                        }else{

                            $video_width  = '640';
                            $video_height = '480';

                        }

                    }else{
                        $youtube_id = null;
                    }

                    // Vimeo ID
                    if ($video_url_type == 'vimeo') {


                        // Vimeo ID
                        $vimeo_id = Helper::vimeo_id($video_sites);

                        // Video Duration
                        $video_duration = Helper::vimeoVideoDuration($video_sites);

                        //Check Duration with membership Plan video duration limit
                        $check_duration = Helper::checkVideoDuration($iLoggedId,$sUserType,$video_duration);
                        if($check_duration==FALSE)
                        {
                            $response = array(
                                            'status' => 'error', 
                                            'msg'    => 'Oops! Video File Duration is more than expected.', 
                                        );
                            return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video File Duration is more than expected.'))->with('msgstatus','error');
                        }
                        //end

                        // Video Preview & Thumb
                        $preview_url = Helper::vimeo_cover($vimeo_id);
                        $thumb_url   = Helper::vimeo_cover($vimeo_id);

                        // Get Video Height and Width
                        $vimeo_quality = Helper::vimeo_quality($video_sites);

                        // Video Height
                        $video_height = $vimeo_quality['height'];

                        // Video Width
                        $video_width  = $vimeo_quality['width'];
                    }else{
                        $vimeo_id = null;
                    }

                    // Dailymotion ID
                    if ($video_url_type == 'dailymotion') {
                        // Dailymotion ID
                        $dailymotion_id = Helper::dailymotion_id($video_sites);

                        // Video Duration
                        $video_duration = Helper::dailymotionDuration($dailymotion_id);

                        //Check Duration with membership Plan video duration limit
                        $check_duration = Helper::checkVideoDuration($iLoggedId,$sUserType,$video_duration);
                        if($check_duration==FALSE)
                        {
                            $response = array(
                                            'status' => 'error', 
                                            'msg'    => 'Oops! Video File Duration is more than expected.', 
                                        );
                            return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video File Duration is more than expected.'))->with('msgstatus','error');
                        }
                        //end

                        // Video Preview & Thumb
                        $preview_url = Helper::dailymotion_cover($dailymotion_id);
                        $thumb_url   = Helper::dailymotion_cover($dailymotion_id);

                        // Video Quality
                        $video_quality  = Helper::dailymotion_quality($dailymotion_id);

                        // Video Height
                        $video_height   = $video_quality->height;

                        // Video Width
                        $video_width    = $video_quality->width;

                    }else{
                        $dailymotion_id = null;
                    }

                    // Get Other Video Details: Title, Description, Cover, Tags...
                    $title       = $request->get('title');
                    $tags        = $request->get('tags');
                    $category    = $request->get('category');
                    $description = $request->get('description');
                    $cover       = Input::file('cover');

                    // Make Validation
                    $validatorInfo = Validator::make($request->all(), [
                        'title'       => 'required|max:3000|min:2',
                        'category'    => 'required|numeric',
                        'description' => 'required',
                    ]);

                    if ($validatorInfo->fails()) {

                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! All Fields are required.', 
                        );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! All Fields are required.'))->with('msgstatus','error');
                    }else{
                        // Generate New Video Random String
                        $video_id = Helper::random_string();

                        // Get Current User ID
                        $user_id = $iLoggedId;
                        $user_name = $iLoggedName;

                        // Video Extension
                        $video_ext = 'mp4';

                        // Insert Video in Database
                        DB::table('videos')->insert([
                            'user_name'        => $user_name,
                            'user_id'        => $user_id,
                            'user_type'      => $sUserType,
                            'video_id'       => $video_id,
                            'video_path'     => $video_sites,
                            'title'          => $title,
                            'description'    => $description,
                            'tags'           => $tags,
                            'category'       => $category,
                            // 'status'         => Helper::video_status(),
                            'status'         => 0,
                            'vip'            => $vip,
                            'preview'        => $preview_url,
                            'thumb'          => $thumb_url,
                            'duration'       => $video_duration,
                            'width'          => $video_width,
                            'height'         => $video_height,
                            'size'           => 00,
                            'type'           => 'mp4',
                            'youtube_id'     => $youtube_id,
                            'vimeo_id'       => $vimeo_id,
                            'dailymotion_id' => $dailymotion_id,
                            'created_at'     => Carbon::now(),
                            'updated_at'     => Carbon::now(),

                        ]);

                        // Send Notifications To subscribers
                        Helper::notice_subscribers($user_id, $video_id,$this->sUserType);

                        // If Video In Wait List Show this Message
                        if (Helper::video_status() == 0) {

                            // Send Notification to admin
                            DB::table('notifications')->insert([
                                'notice_to'   => Helper::admin_username(),
                                'notice_from' => \SiteHelpers::getName($sUserType,$iLoggedId),
                                'type'        => 'wait',
                                'details'     => $video_id,
                                'is_read'     => 0,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now(),
                            ]);

                            $response = array(
                                'status' => 'success', 
                                'msg'    => 'Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.', 
                            );
                            return Redirect::to('upload')->with('messagetext',\Lang::get('Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.'))->with('msgstatus','error');
                        }else{
                            // Else Show that one

                            $href = url('/')."/watch".'/'.$video_id;

                            $response = array(
                                'status'   => 'success', 
                                'redirect' => $href 
                            );
                           return Redirect::to('upload')->with('messagetext',\Lang::get('Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.'))->with('msgstatus','error');
                        }
                    }
                }
            }elseif (Input::file('video_file')){
                
                // Get Video File
                $video = Input::file('video_file');

                // Get Security Settings
                $settings_security = DB::table('settings_security')->where('id', 1)->first();

                // If Not Admin
/*                if ((Auth::user()->level != 1) AND (Auth::user()->id != 1)) {

                    // First Check if User Use All of his storage space
                    $check_storage = Helper::check_user_storage();

                    if ($check_storage == FALSE) {
                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! You don\'t have enough storage space.', 
                        );
                        return Response::json($response);
                    }
                } */

                // Check Doesn't Send any File
                $validator = Validator::make($request->all(), [
                    'video_file' => 'required'
                ]);

                if ($validator->fails()) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Please Select a Video to be Uploaded.', 
                    );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Please Select a Video to be Uploaded.'))->with('msgstatus','error');
                }                  

                // Only Valid Video Will be uploaded
                $secure = Helper::secure_video($video);

                 // echo "<pre>";print_r($secure);exit;


                if ($secure == FALSE) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Please Upload a Valid Video File.', 
                    );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Please Upload a Valid Video File.'))->with('msgstatus','error');
                }

                //Check Duration with membership Plan video duration limit
                $check_duration = Helper::checkVideoDuration($iLoggedId,$sUserType,$secure['video_duration']);
                if($check_duration==FALSE)
                {
                    $response = array(
                                    'status' => 'error', 
                                    'msg'    => 'Oops! Video File Duration is more than expected.', 
                                );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video File Duration is more than expected.'))->with('msgstatus','error');
                }
                //end

                // Get Other Video Details: Title, Description, Cover, Tags...
                $title       = $request->get('title');
                $tags        = $request->get('tags');
                $category    = $request->get('category');
                $description = $request->get('description');
                $description = $request->get('description');
                $cover       = Input::file('cover');

                // Make Validation
                $validatorInfo = Validator::make($request->all(), [
                    'title'       => 'required|max:300| min:2',
                    'category'    => 'required|numeric',
                    'description' => 'required',
                    'cover'       => 'required|image|max:5000',
                ]);

                if ($validatorInfo->fails()) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! All Fields are required.', 
                    );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! All Fields are required.'))->with('msgstatus','error');
                }else{

                    // Generate New Video Random String
                    $video_id = Helper::random_string();

                    // Get Current User ID
                    $user_id = $this->iLoggedId;
                    $user_name = $iLoggedName;
                    // Video Extension
                    $video_ext = $secure['video_type'];

                    // Move Video
                    $move = Helper::move_video($video, $video_id, $video_ext);

                    // Get Video Location to save in Database
                    $video_url = url('/')."/uploads/video/".$video_id."/video.".$video_ext;

                    // Get Cover Image Extension
                    $cover_ext = $cover->getClientOriginalExtension();

                    // Let's work on Video Cover Image
                    $preview_thumb = Helper::video_preview_thumb($cover, $video_id, $cover_ext);

                    // After Moving Image Create URL to save in database
                    $preview_url = url('/').'/uploads/video/images/preview/'.$video_id. '/preview.'.$cover_ext;

                    $thumb_url   = url('/').'/uploads/video/images/thumbnail/'.$video_id. '/thumbnail.'.$cover_ext;
 $user_name = $iLoggedName;
                    // Insert Video in Database
                    DB::table('videos')->insert([
                        'user_name'     => $user_name,
                        'user_id'     => $user_id,
                        'user_type'   => $sUserType,
                        'video_id'    => $video_id,
                        'video_path'  => $video_url,
                        'title'       => $title,
                        'description' => $description,
                        'tags'        => $tags,
                        'category'    => $category,
                        // 'status'      => Helper::video_status(),
                        'status'         => 0,
                        'vip'         => $vip,
                        'preview'     => $preview_url,
                        'thumb'       => $thumb_url,
                        'duration'    => $secure['video_duration'],
                        'width'       => $secure['video_width'],
                        'height'      => $secure['video_height'],
                        'size'        => $secure['video_size'],
                        'type'        => $secure['video_type'],
                        'created_at'  => Carbon::now(),
                        'updated_at'  => Carbon::now(),

                    ]);

                    // Send Notifications To subscribers
                    Helper::notice_subscribers($user_id, $video_id,$this->sUserType);

                    // If Video In Wait List Show this Message
                    if (Helper::video_status() == 0) {

                        // Send Notification to admin
                        DB::table('notifications')->insert([
                            'notice_to'   => Helper::admin_username(),
                            'notice_from' => \SiteHelpers::getName($sUserType,$iLoggedId),
                            'type'        => 'wait',
                            'details'     => $video_id,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);

                        $response = array(
                            'status' => 'success', 
                            'msg'    => 'Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.', 
                        );
                        return Redirect::to('upload')->with('messagetext',\Lang::get('Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.'))->with('msgstatus','success');
                    }else{
                        // Else Show that one

                        $href = url('/')."/watch".'/'.$video_id;

                        $response = array(
                            'status'   => 'success', 
                            'redirect' => $href 
                        );
                        return Redirect::to('upload')->with('messagetext',\Lang::get('Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.'))->with('msgstatus','success');

                        // return Redirect::to('video/'.$video_id)->with('messagetext',\Lang::get('Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.'))->with('msgstatus','success');

                    }
                }
            }

        }
        

        // Check if Only Upload From Youtube
        if ($settings_upload->upload_type == 1) {
            
            // Get Video URL 
            $video_sites = $request->get('video_sites');

            if ($video_sites) {

                // Validate Video URL
                $validateVidURL = Validator::make($request->all(), [
                    'video_sites' => 'required|url'
                ]);

                if ($validateVidURL->fails()) {
                    
                    // Error
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Video URL is Invalid', 
                    );
                    
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Video URL is Invalid'))->with('msgstatus','error');

                }

                $video_url_type = Helper::check_vid_url($video_sites);
                // Check if valid URL
                if (!$video_url_type) {

                    // Error
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Only dailymotion, vimeo and youtube url allow.', 
                    );
                    return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! Only dailymotion, vimeo and youtube url allow.'))->with('msgstatus','error');

                }else{

                    // Youtube ID
                    if ($video_url_type == 'youtube') {

                        // Youtube ID
                        $youtube_id = Helper::youtube_id($video_sites);

                        // Check if Valid YouTube ID
                        $youtube = Youtube::getVideoInfo($youtube_id);

                        // Get Youtube Video
                        $youtube_vid = $request->get('youtube_id');

                        // Video Duration
                        $video_duration = Helper::youtube_duration($youtube->contentDetails->duration);

                        // Video Preview & Thumb
                        $preview_url = Helper::youtube_cover($youtube_id);
                        $thumb_url   = Helper::youtube_cover($youtube_id);

                        // Youtube Video Quality
                        $youtube_quality     = Helper::is_youtube_hd($youtube->contentDetails->definition);

                        if ($youtube_quality == TRUE) {

                            $video_width  = '1280';
                            $video_height = '720';

                        }else{

                            $video_width  = '640';
                            $video_height = '480';

                        }

                    }else{
                        $youtube_id = null;
                    }

                    // Vimeo ID
                    if ($video_url_type == 'vimeo') {
                        // Vimeo ID
                        $vimeo_id = Helper::vimeo_id($video_sites);

                        // Video Duration
                        $video_duration = Helper::vimeoVideoDuration($video_sites);

                        // Video Preview & Thumb
                        $preview_url = Helper::vimeo_cover($vimeo_id);
                        $thumb_url   = Helper::vimeo_cover($vimeo_id);

                        // Get Video Height and Width
                        $vimeo_quality = Helper::vimeo_quality($video_sites);

                        // Video Height
                        $video_height = $vimeo_quality['height'];

                        // Video Width
                        $video_width  = $vimeo_quality['width'];
                    }else{
                        $vimeo_id = null;
                    }

                    // Dailymotion ID
                    if ($video_url_type == 'dailymotion') {
                        // Dailymotion ID
                        $dailymotion_id = Helper::dailymotion_id($video_sites);

                        // Video Duration
                        $video_duration = Helper::dailymotionDuration($dailymotion_id);

                        // Video Preview & Thumb
                        $preview_url = Helper::dailymotion_cover($dailymotion_id);
                        $thumb_url   = Helper::dailymotion_cover($dailymotion_id);

                        // Video Quality
                        $video_quality  = Helper::dailymotion_quality($dailymotion_id);

                        // Video Height
                        $video_height   = $video_quality->height;

                        // Video Width
                        $video_width    = $video_quality->width;

                    }else{
                        $dailymotion_id = null;
                    }

                    // Get Other Video Details: Title, Description, Cover, Tags...
                    $title       = $request->get('title');
                    $tags        = $request->get('tags');
                    $category    = $request->get('category');
                    $description =$request->get('description');
                    $cover       = Input::file('cover');

                    // Make Validation
                    $validatorInfo = Validator::make($request->all(), [
                        'title'       => 'required|max:300| min:2',
                        'category'    => 'required|numeric',
                        'description' => 'required',
                    ]);

                    if ($validatorInfo->fails()) {
                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! All Fields are required.', 
                        );
                        return Response::json($response);
                    }else{
                        // Generate New Video Random String
                        $video_id = Helper::random_string();

                        // Get Current User ID
                        $user_id = $this->iLoggedId;

                        // Video Extension
                        $video_ext = 'mp4';
 $user_name = $iLoggedName;
                        // Insert Video in Database
                        DB::table('videos')->insert([
                            'user_name'     => $user_name,
                            'user_id'        => $user_id,
                            'video_id'       => $video_id,
                            'video_path'     => $video_sites,
                            'title'          => $title,
                            'description'    => $description,
                            'tags'           => $tags,
                            'category'       => $category,
                            // 'status'         => Helper::video_status(),
                            'status'         => 0,
                            'vip'            => $vip,
                            'preview'        => $preview_url,
                            'thumb'          => $thumb_url,
                            'duration'       => $video_duration,
                            'width'          => $video_width,
                            'height'         => $video_height,
                            'size'           => 00,
                            'type'           => 'mp4',
                            'youtube_id'     => $youtube_id,
                            'vimeo_id'       => $vimeo_id,
                            'dailymotion_id' => $dailymotion_id,
                            'created_at'     => Carbon::now(),
                            'updated_at'     => Carbon::now(),

                        ]);

                        // Send Notifications To subscribers
                        Helper::notice_subscribers($user_id, $video_id,$this->sUserType);

                        // If Video In Wait List Show this Message
                        if (Helper::video_status() == 0) {

                        	// Send Notification to admin
                            DB::table('notifications')->insert([
                                'notice_to'   => Helper::admin_username(),
                                'notice_from' => \SiteHelpers::getName($sUserType,$iLoggedId),
                                'type'        => 'wait',
                                'details'     => $video_id,
                                'is_read'     => 0,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now(),
                            ]);

                            $response = array(
                                'status' => 'success', 
                                'msg'    => 'Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.', 
                            );
                            return Response::json($response);
                        }else{
                            // Else Show that one

                            $href = url('/')."/watch".'/'.$video_id;

                            $response = array(
                                'status'   => 'success', 
                                'redirect' => $href 
                            );
                            return Redirect::to('video/'.$video_id)->with('messagetext',\Lang::get('Video Updated Successfully'))->with('msgstatus','success');  
                        }
                    }
                }
            }

        }elseif (($settings_upload->upload_type == 0) OR ($settings_upload->upload_type == 2)) {

            // Get Video File
            $video = Input::file('video_file'); 

            // Upload From computer
            if ($video) {

                // Get Security Settings
                $settings_security = DB::table('settings_security')->where('id', 1)->first();

                // If Not Admin
                if ((Auth::user()->level != 1) AND (Auth::user()->id != 1)) {

                    // First Check if User Use All of his storage space
                    $check_storage = Helper::check_user_storage();

                    if ($check_storage == FALSE) {
                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! You don\'t have enough storage space.', 
                        );
                        return Redirect::to('upload')->with('messagetext',\Lang::get('Oops! You don\'t have enough storage space.'))->with('msgstatus','error');  
                    }
                } 

                // Check Doesn't Send any File
                $validator = Validator::make($request->all(), [
                    'video_file' => 'required'
                ]);

                if ($validator->fails()) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Please Select a Video to be Uploaded.', 
                    );
                    return Redirect::to('upload')->with('messagetext','Oops! Please Select a Video to be Uploaded.')->with('msgstatus','error'); 
                }                  

                // Only Valid Video Will be uploaded
                $secure = Helper::secure_video($video);

                if ($secure == FALSE) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! Please Upload a Valid Video File.', 
                    );
                    return Redirect::to('upload')->with('messagetext','Oops! Please Upload a Valid Video File.')->with('msgstatus','error'); 
                }

                // Get Other Video Details: Title, Description, Cover, Tags...
                $title       = $request->get('title');
                $tags        = $request->get('tags');
                $category    = $request->get('category');
                $description = $request->get('description');
                $cover       = Input::file('cover');

                // Make Validation
                $validatorInfo = Validator::make($request->all(), [
                    'title'       => 'required|max:300| min:2',
                    'category'    => 'required|numeric',
                    'description' => 'required',
                    'cover'       => 'required|image|max:5000',
                ]);

                if ($validatorInfo->fails()) {
                    $response = array(
                        'status' => 'error', 
                        'msg'    => 'Oops! All Fields are required.', 
                    );
                     return Redirect::to('upload')->with('messagetext','Oops! All Fields are required.')->with('msgstatus','error');
                }else{

                    // Generate New Video Random String
                    $video_id = Helper::random_string();

                    // Get Current User ID
                    $user_id = $this->iLoggedId;

                    // Video Extension
                    $video_ext = $secure['video_type'];

                    // Move Video
                    $move = Helper::move_video($video, $video_id, $video_ext);

                    // Get Video Location to save in Database
                    $video_url = url('/')."/uploads/videos/".$video_id."/video.".$video_ext;

                    // Get Cover Image Extension
                    $cover_ext = $cover->getClientOriginalExtension();

                    // Let's work on Video Cover Image
                    $preview_thumb = Helper::video_preview_thumb($cover, $video_id, $cover_ext);

                    // After Moving Image Create URL to save in database
                    $preview_url = url('/').'/uploads/video/images/preview/'.$video_id. '/preview.'.$cover_ext;

                    $thumb_url   = url('/').'/uploads/video/images/thumbnail/'.$video_id. '/thumbnail.'.$cover_ext;
 $user_name = $iLoggedName;
                    // Insert Video in Database
                    DB::table('videos')->insert([
                        'user_name'     => $user_name,
                        'user_id'     => $user_id,
                        'video_id'    => $video_id,
                        'video_path'  => $video_url,
                        'title'       => $title,
                        'description' => $description,
                        'tags'        => $tags,
                        'category'    => $category,
                        // 'status'      => Helper::video_status(),
                        'status'         => 0,
                        'vip'         => $vip,
                        'preview'     => $preview_url,
                        'thumb'       => $thumb_url,
                        'duration'    => $secure['video_duration'],
                        'width'       => $secure['video_width'],
                        'height'      => $secure['video_height'],
                        'size'        => $secure['video_size'],
                        'type'        => $secure['video_type'],
                        'created_at'  => Carbon::now(),
                        'updated_at'  => Carbon::now(),

                    ]);

                    // Send Notifications To subscribers
                    Helper::notice_subscribers($user_id, $video_id,$this->sUserType);

                    // If Video In Wait List Show this Message
                    if (Helper::video_status() == 0) {

                    	// Send Notification to admin
                        DB::table('notifications')->insert([
                            'notice_to'   => Helper::admin_username(),
                            'notice_from' => \SiteHelpers::getName($sUserType,$iLoggedId),
                            'type'        => 'wait',
                            'details'     => $video_id,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);

                        $response = array(
                            'status' => 'success', 
                            'msg'    => 'Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.', 
                        );
                        return Redirect::to('video/'.$video_id)->with('messagetext','Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.')->with('msgstatus','success');
                    }else{
                        // Else Show that one

                        $href = url('/')."/watch".'/'.$video_id;

                        $response = array(
                            'status'   => 'success', 
                            'redirect' => $href 
                        );
                         return Redirect::to('video/'.$video_id)->with('messagetext','Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.')->with('msgstatus','success');
                    }
                }
            }elseif ($request->get('video_sites')) {

                // Get Video URL 
                $video_sites = $request->get('video_sites');

                if ($video_sites) {

                    // Validate Video URL
                    $validateVidURL = Validator::make($request->all(), [
                        'video_sites' => 'required|url'
                    ]);

                    if ($validateVidURL->fails()) {
                        
                        // Error
                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! Video URL is Invalid', 
                        );
                         return Redirect::to('upload/')->with('messagetext','Oops! Video URL is Invalid')->with('msgstatus','error');

                    }

                    $video_url_type = Helper::check_vid_url($video_sites);
                    // Check if valid URL
                    if (!$video_url_type) {

                        // Error
                        $response = array(
                            'status' => 'error', 
                            'msg'    => 'Oops! Only dailymotion, vimeo and youtube url allow.', 
                        );
                         return Redirect::to('upload/')->with('messagetext','Oops! Only dailymotion, vimeo and youtube url allow.')->with('msgstatus','error');

                    }else{

                        // Youtube ID
                        if ($video_url_type == 'youtube') {

                            // Youtube ID
                            $youtube_id = Helper::youtube_id($video_sites);

                            // Check if Valid YouTube ID
                            $youtube = Youtube::getVideoInfo($youtube_id);

                            // Video Preview & Thumb
                            $preview_url = Helper::youtube_cover($youtube_id);
                            $thumb_url   = Helper::youtube_cover($youtube_id);

                            // Get Youtube Video
                            $youtube_vid = $request->get('youtube_id');

                            // Video Duration
                            $video_duration = Helper::youtube_duration($youtube->contentDetails->duration);

                            // Youtube Video Quality
                            $youtube_quality     = Helper::is_youtube_hd($youtube->contentDetails->definition);

                            if ($youtube_quality == TRUE) {

                                $video_width  = '1280';
                                $video_height = '720';

                            }else{

                                $video_width  = '640';
                                $video_height = '480';

                            }

                        }else{
                            $youtube_id = null;
                        }

                        // Vimeo ID
                        if ($video_url_type == 'vimeo') {
                            // Vimeo ID
                            $vimeo_id = Helper::vimeo_id($video_sites);

                            // Video Duration
                            $video_duration = Helper::vimeoVideoDuration($video_sites);

                            // Video Preview & Thumb
                            $preview_url = Helper::vimeo_cover($vimeo_id);
                            $thumb_url   = Helper::vimeo_cover($vimeo_id);

                            // Get Video Height and Width
                            $vimeo_quality = Helper::vimeo_quality($video_sites);

                            // Video Height
                            $video_height = $vimeo_quality['height'];

                            // Video Width
                            $video_width  = $vimeo_quality['width'];
                        }else{
                            $vimeo_id = null;
                        }

                        // Dailymotion ID
                        if ($video_url_type == 'dailymotion') {
                            // Dailymotion ID
                            $dailymotion_id = Helper::dailymotion_id($video_sites);

                            // Video Duration
                            $video_duration = Helper::dailymotionDuration($dailymotion_id);

                            // Video Preview & Thumb
                            $preview_url = Helper::dailymotion_cover($dailymotion_id);
                            $thumb_url   = Helper::dailymotion_cover($dailymotion_id);

                            // Video Quality
                            $video_quality  = Helper::dailymotion_quality($dailymotion_id);

                            // Video Height
                            $video_height   = $video_quality->height;

                            // Video Width
                            $video_width    = $video_quality->width;

                        }else{
                            $dailymotion_id = null;
                        }

                        // Get Other Video Details: Title, Description, Cover, Tags...
                        $title       = $request->get('title');
                        $tags        = $request->get('tags');
                        $category    = $request->get('category');
                        $description = $request->get('description');
                        $cover       = Input::file('cover');

                        // Make Validation
                        $validatorInfo = Validator::make($request->all(), [
                            'title'       => 'required|max:300| min:2',
                            'tags'        => 'required',
                            'category'    => 'required|numeric',
                            'description' => 'required',
                        ]);

                        if ($validatorInfo->fails()) {
                            $response = array(
                                'status' => 'error', 
                                'msg'    => 'Oops! All Fields are required.', 
                            );
                            return Redirect::to('upload/')->with('messagetext','Oops! All Fields are required.')->with('msgstatus','error');
                        }else{
                            // Generate New Video Random String
                            $video_id = Helper::random_string();

                            // Get Current User ID
                            $user_id = $this->iLoggedId;

                            // Video Extension
                            $video_ext = 'mp4';
 $user_name = $iLoggedName;
                            // Insert Video in Database
                            DB::table('videos')->insert([
                                'user_name'     => $user_name,
                                'user_id'        => $user_id,
                                'video_id'       => $video_id,
                                'video_path'     => $video_sites,
                                'title'          => $title,
                                'description'    => $description,
                                'tags'           => $tags,
                                'category'       => $category,
                                // 'status'         => Helper::video_status(),
                                'status'         => 0,
                                'vip'            => $vip,
                                'preview'        => $preview_url,
                                'thumb'          => $thumb_url,
                                'duration'       => $video_duration,
                                'width'          => $video_width,
                                'height'         => $video_height,
                                'size'           => 00,
                                'type'           => 'mp4',
                                'youtube_id'     => $youtube_id,
                                'vimeo_id'       => $vimeo_id,
                                'dailymotion_id' => $dailymotion_id,
                                'created_at'     => Carbon::now(),
                                'updated_at'     => Carbon::now(),

                            ]);

                            // Send Notifications To subscribers
                            Helper::notice_subscribers($user_id, $video_id,$this->sUserType);

                            // If Video In Wait List Show this Message
                            if (Helper::video_status() == 0) {

                            	// Send Notification to admin
	                            DB::table('notifications')->insert([
	                                'notice_to'   => Helper::admin_username(),
	                                'notice_from' => \SiteHelpers::getName($sUserType,$iLoggedId),
	                                'type'        => 'wait',
	                                'details'     => $video_id,
	                                'is_read'     => 0,
	                                'created_at'  => Carbon::now(),
	                                'updated_at'  => Carbon::now(),
	                            ]);
                            
                                $response = array(
                                    'status' => 'success', 
                                    'msg'    => 'Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.', 
                                );
                                 return Redirect::to('video/'.$video_id)->with('messagetext','Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.')->with('msgstatus','success');
                            }else{
                                // Else Show that one

                                $href = url('/')."/watch".'/'.$video_id;

                                $response = array(
                                    'status'   => 'success', 
                                    'redirect' => $href 
                                );
                                return Redirect::to('video/'.$video_id)->with('messagetext','Congratulations! video has been successfully uploaded. You will receive a notice when your video get accept.')->with('msgstatus','success');
                            }
                        }
                    }
                }

            }else{

                // Error, No File Selected or No Youtube Video
                $response = array(
                    'status' => 'error', 
                    'msg'    => 'Oops! Please upload a video from your computer or insert video url.' 
                );
                return Redirect::to('upload')->with('messagetext','Oops! Please upload a video from your computer or insert video url.')->with('msgstatus','error');

            }
        }
    }
    public function getPhotoTags(Request $request, $Tags){
        if(empty($this->iLoggedId)){
            return Redirect::to('/login');
        }
        
       $this->data['results'] = DB::table('tb_images')
                   ->where('status', 'Active')
                   ->Where('Tags', 'LIKE', '%'.$Tags.'%')
                   ->get();
       // echo "<pre>";print_r($data['results']);exit;

        $this->data['pageTitle']    = 'Photo Album';
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['v_url'] = '';   
        $this->data['urls'] = '';
        $this->data['fbimg'] = '';  
        $this->data['links']=$this->option->get_options('links'); 
        $this->data['pages'] = 'photo.tags_photo';
        // $this->data['content']      = view('photo.tags_photo',$data);

        $page = 'layouts.'.CNF_THEME.'.index';

        return view($page,$this->data);  
    }
    public function postCheckuniqueAlbumname(Request $request)
    {
        $result = $_POST['album_name'];
        
        
        $email=\DB::table('tb_albums')->where('title',$result)->where('ownerid',$this->iLoggedId)->where('user_type',$this->sUserType)->get();
        if(count($email)>0)
        {
        return '"Album name already taken"'; 
        }
        else
        {
            return 'true';
        }
    }
}
