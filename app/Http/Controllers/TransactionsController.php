<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Transactions;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class TransactionsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'transactions';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Transactions();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'transactions',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  


		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('transactions');

		$this->data['rowData']		= $results['rows'];

		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['status'] = $request->get('status');
		$this->data['user_type'] = $request->get('user_type');
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template


		// echo '<pre>';print_r($this->data);exit;
		return view('transactions.index',$this->data);
	}	

	//---- get transactions for Agent
	public function getAgentTrans(Request $request)
	{

		
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 
 
		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// echo "<pre>";print_r($params);die;
		// Get Query 
		// $results = $this->model->getRows( $params );	

	

		$results = \DB::table('tb_transactions')->where('user_type','=','agent')->get();
		
		 //echo "<pre>";print_r($results);die;
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		// $pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination = new Paginator($results, count($results), $params['limit']);	
		$pagination->setPath('agent');
				$rows = $request->input('rows') ? $request->input('rows') : 10 ;

    $query = \DB::table('tb_transactions')->where('user_type','=','agent');

    if($sort=='username')
    {
   	$query->join('tb_agency', 'tb_transactions.user_id', '=', 'tb_agency.id')->orderBy($sort,$order);

    }
	elseif($sort=='plan_name')
    {
   	$query->join('tb_membership_plan', 'tb_transactions.membership_id', '=', 'tb_membership_plan.plan_id')->orderBy($sort,$order);

    }

    if($request->get('username'))
    {
        $query->where('user_id','like','%'.$request->get('username').'%')->orderBy($sort,$order);
        	
    }

        $query->where('purchase_period','like','%'.$request->get('period').'%')->orderBy($sort,$order);

    if($request->get('membership'))
    {
        $query->where('membership_id','like','%'.$request->get('membership').'%')->orderBy($sort,$order);
        	
    }

     if($request->get('created_At'))
    {
    	$date= date('Y-m-d H',strtotime($request->get('created_At')));
        $query->where('created_at','like','%'.$date.'%')->orderBy($sort,$order);
        	
    }
     if($request->get('amount'))
    {
        $query->where('amount','like','%'.$request->get('amount').'%')->orderBy($sort,$order);
        	
    }
 

        $query->where('tb_transactions.status','like','%'.$request->get('status').'%')->orderBy($sort,$order);
 	// echo '<pre>';print_r($sort);exit;
       $this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);

		// $this->data['rowData']		= $results['rows'];
		$this->data['rowData']		= $results;
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		$this->data['user_type']		= 'agent';
		// Detail from master if any
		$this->data['username'] = (!empty($request->get('username')))?$request->get('username'):'';		
		$this->data['period'] = (!empty($request->get('period')))?$request->get('period'):'';		
		$this->data['membership'] = (!empty($request->get('membership')))?$request->get('membership'):'';	;	
		$this->data['created_At'] = (!empty($request->get('created_At')))?$request->get('created_At'):'';	
		$this->data['amount'] = (!empty($request->get('amount')))?$request->get('amount'):'';	
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template


		
		return view('transactions.index',$this->data);
	

	}

	//---- get transactions for Model
	public function getModelTrans(Request $request)
	{


		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		// $results = $this->model->getRows( $params );	
		$results = \DB::table('tb_transactions')->where('user_type','=','model')->get();	
		
		// echo "<pre>";print_r($results);die;
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		// $pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination = new Paginator($results, count($results), $params['limit']);	
		$pagination->setPath('model');
				$rows = $request->input('rows') ? $request->input('rows') : 10 ;

    $query = \DB::table('tb_transactions')->where('user_type','=','model');

      if($sort=='username')
    {
   	$query->join('tb_agency', 'tb_transactions.user_id', '=', 'tb_agency.id')->orderBy($sort,$order);

    }
	elseif($sort=='plan_name')
    {
   	$query->join('tb_membership_plan', 'tb_transactions.membership_id', '=', 'tb_membership_plan.plan_id')->orderBy($sort,$order);

    }
    
     if($request->get('username'))
    {
        $query->where('user_id','like','%'.$request->get('username').'%')->orderBy($sort,$order);
        	
    }

        $query->where('purchase_period','like','%'.$request->get('period').'%')->orderBy($sort,$order);

    if($request->get('membership'))
    {
        $query->where('membership_id','like','%'.$request->get('membership').'%')->orderBy($sort,$order);
        	
    }
    if($request->get('created_At'))
    {
$date= date('Y-m-d H',strtotime($request->get('created_At')));
        $query->where('created_at','like','%'.$date.'%')->orderBy($sort,$order);        	
    }
     if($request->get('amount'))
    {
        $query->where('amount','like','%'.$request->get('amount').'%')->orderBy($sort,$order);
        	
    }


        $query->where('tb_transactions.status','like','%'.$request->get('status').'%')->orderBy($sort,$order);
 	
       $this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);
		// $this->data['rowData']		= $results['rows'];
		$this->data['rowData']		= $results;
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		//Set values after load
		$this->data['username'] = (!empty($request->get('username')))?$request->get('username'):'';		
		$this->data['period'] = (!empty($request->get('period')))?$request->get('period'):'';		
		$this->data['membership'] = (!empty($request->get('membership')))?$request->get('membership'):'';	;	
		$this->data['created_At'] = (!empty($request->get('created_At')))?$request->get('created_At'):'';	
		$this->data['amount'] = (!empty($request->get('amount')))?$request->get('amount'):'';	

		// Group users permission
		$this->data['access']		= $this->access;
		$this->data['user_type']		= 'model';
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template


		// echo '<pre>';print_r($this->data);exit;
		return view('transactions.index',$this->data);
	

	}



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_transactions'); 
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		return view('transactions.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{
		// get usertype
		$user_type = isset($_REQUEST['user_type'])?$_REQUEST['user_type']:"";

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			$this->data['user_type'] = $user_type; // Send user type to view

			// $this->data['row']->order_details = json_decode($this->data['row']->order_details);
			// foreach($this->data['row']->order_details as $abcd){
			// 	echo '<pre>';print_r($this->data['row']->order_details);exit;
			// }
			// echo '<pre>';print_r($this->data['row']->order_details->customerReference);exit;

			return view('transactions.view',$this->data);
		} else {
			return Redirect::to('transactions')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_transactions');
				
			$id = $this->model->insertRow($data , $request->input('id'));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'transactions/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'transactions?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('transactions/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

				$user_type=$request->input('user_type');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{

			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('transactions/'.$user_type.'?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('transactions?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Transactions();
		$info = $model::makeInfo('transactions');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('transactions.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('transactions.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_transactions');		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}	




}