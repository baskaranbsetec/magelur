<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Message;
use App\Models\Modele;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Sximo;

class MessageController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'message';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Message();
		$this->modele = new Modele();
		$this->option = new Sximo();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'message',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  


		
	}

	public function getIndex( Request $request )
	{
		
		$this->data["membership"]="";
			
	if(\Session::get('model_id')){
		$uid = \Session::get('model_id');
	}
	elseif(\Session::get('agent_id')){
		$uid = \Session::get('agent_id');
		$getagentdetail=\SiteHelpers::getIsmember($uid);		
		$this->data["membership"]=$getagentdetail[0]->membership;
		// echo "<pre>";print_r($getagentdetail);exit;
	}
	elseif(\Auth::check())
	{
		return Redirect::to('/login');
	}
	else
	{
		return Redirect::to('/login');
	}

		$composeUser = (!is_null($request->input('ctu')) ? $request->input('ctu') : '0');
		$type = (!is_null($request->input('ty')) ? $request->input('ty') : '0');
		$label = (!is_null($request->input('lb')) ? $request->input('lb') : '0');
		
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getMessageRows( $params, $type, $label, $uid );	
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('message');
		
		$this->data['rowData']		= $results['rows'];
		$this->data['messages']		=$this->model->get();
		$this->data['totalType']		= $results['totalType'];
		$totalType=$results['totalType'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Message';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = '';
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	 $this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'message';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}
				/*Agent list*/
				$agent_users_data = $this->model->getMessageUsersAgent();
				$usersAgentAvatar = array();
				foreach($agent_users_data as $user_data){
					$usersAgentNameList[$user_data->id] = $user_data->username;
					$usersAgentAvatar[$user_data->id] = $user_data->profile_photo;
				}	
				$this->data['usersAgentAvatar'] = $usersAgentAvatar;
				$this->data['usersAgentNameList'] = $usersAgentNameList;
				/*Model list*/
				$model_users_data = $this->model->getMessageUsersModel();
				$usersModelAvatar = array();
				foreach($model_users_data as $user_data){
					$usersModelNameList[$user_data->id] = $user_data->firstname;
					$usersModelAvatar[$user_data->id] = $user_data->profile_photo;
				}	
		$this->data['usersModelAvatar'] = $usersModelAvatar;
		$this->data['usersModelNameList'] = $usersModelNameList;

		$users_data = $this->model->getMessageUsers( $uid );
		

		$users = array(''=>'Select an User');
		$username = array();
		$usersNameList = array();
		$usersEmailList = array();
		$usersAvatar = array();
		foreach($users_data as $user_data){
			// print_r($user_data->firstname);exit;
			if(\Session::get('model_id')){
			$users[$user_data->id] = $user_data->username;
			$usersNameList[$user_data->id] = $user_data->username;
			$username[$user_data->id] = $user_data->username;
		}
		elseif(\Session::get('agent_id')){
			$users[$user_data->id] = $user_data->firstname;
			$usersNameList[$user_data->id] = $user_data->firstname;
			$username[$user_data->id] = $user_data->firstname;
		}
			$usersEmailList[$user_data->id] = $user_data->email;
			$usersIdList[$user_data->id] = $user_data->id;
			$usersAvatar[$user_data->id] = $user_data->profile_photo;
		}
		$this->data['username'] = $username;
		$this->data['users'] = $users;
		// echo "<pre>";print_r($this->data['users']);exit;		
		$this->data['usersNameList'] = $usersNameList;
		$this->data['usersEmailList'] = $usersEmailList;
		$this->data['usersIdList'] = $usersIdList;
		$this->data['usersAvatar'] = $usersAvatar;
		// echo "<pre>";print_r($usersAvatar);exit;
		$labels_data = $this->model->getLabels( $uid );
		$labels = array();
		foreach($labels_data as $label_data){
			$labels[$label_data->id] = $label_data->name;
		}
		$this->data['labels'] = $labels;
		$this->data['type'] = $type;
		$this->data['label'] = $label;
		$this->data['uid'] = $uid;
		$this->data['composeUser'] = $composeUser;	
		if(\Session::get('agent_id')){
		$this->data['pages'] = 'agency.agent_message';
		$this->data['agents'] = \DB::table('tb_agency')->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		$this->data['images'] = \DB::table('tb_agency')->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
		$this->data['models'] = \DB::table('tb_models')->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
		
		$this->data['user_type']='agent';
		// $this->data['images'] = \DB::table('tb_models')->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
			//print_r($row);exit;

		}
		elseif(\Session::get('model_id')){
		$this->data['user_type']='model';
		$this->data['pages'] = 'model.model_message';
		$this->data['models'] = \DB::table('tb_models')->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
		$this->data['images'] = \DB::table('tb_models')->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
		}
					//echo '<pre>';print_r($this->data);exit;
				$page = 'layouts.'.CNF_THEME.'.index';
				return view($page,$this->data);	
		}
				
	}	

function postMessageUpdate( Request $request)
	{
			if($request->input('update_function') == 'starred'){
			$this->model->postStar($request->input('msg_id'), $request->input('starred'), $request->input('msg_type'));
			echo $request->input('starred');
		} else if($request->input('update_function') == 'read'){
			$this->model->postRead($request->input('msg_id'));
		}
	}

function postLabelSave( Request $request, $id =0)
	{
		
		$user = \Auth::user();
		$uid = $user->id;
		$name = $request->input('label');		
		$rules = array(
			'label' => 'unique:message_labels,name');
		$validator = Validator::make($request->all(), $rules);			
		if ($validator->passes()) {
			//$data = $this->validatePost('message_labels');
			
			$data['entry_by'] = $uid;
			$data['name'] = $request->input('label');
			$now_date = date("Y-m-d H:i:s");
			$data['createdOn'] = $now_date;
			$data['updatedOn'] = $now_date;
			$newID = $this->model->insertLabelRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'message/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'message?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}
			return json_encode(array('status' => '1','newid' => $newID));
			//return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
			
		} else {
			return json_encode(array('status' => '0'));
			//return Redirect::to('message/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
		}	
	
	}	
function postLabelDelete( Request $request)
	{
		$this->model->postDeleteLabel($request->input('label_id'));
		echo '1';
	}
	function postGroupMessageUpdate( Request $request)
	{
		$msg_ids = $request->input('msg_ids');
		$is_sender = $request->input('is_sender');
		$ids = explode(',', $msg_ids);
		$isSender = explode(',', $is_sender);
		
		if($request->input('group_update_function') == 'delete'){
			$c = 0;
			foreach($ids as $id){
				$this->model->postTrash($id, $isSender[$c]);
				$c++;
			}
			echo $c;
		} else if($request->input('group_update_function') == 'deleteTrash'){
			$c = 0;
			foreach($ids as $id){
				$this->model->postDeleteTrash($id, $isSender[$c]);
				$c++;
			}
			echo $c;
		} else if($request->input('group_update_function') == 'label'){
			$d = 0;
			foreach($ids as $id){
				$this->model->postApplyLabel($id, $isSender[$d], $request->input('label_id'));
				$d++;
			}
			echo 'label';
		} else if($request->input('group_update_function') == 'removelabel'){
			$e = 0;
			foreach($ids as $id){
				$this->model->postRemoveLabel($id, $isSender[$e]);
				$e++;
			}
			echo 'label';
		}
	}
	public function postMessageFilter( Request $request)
	{
		$module = $this->module;
		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : '');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : '');
		$rows 	= (!is_null($request->input('rows')) ? $request->input('rows') : '');
		$md 	= (!is_null($request->input('md')) ? $request->input('md') : '');
		$ty 	= (!is_null($request->input('ty')) ? $request->input('ty') : '');
		$lb 	= (!is_null($request->input('lb')) ? $request->input('lb') : '');
		
		$filter = '?';
		if($sort!='') $filter .= '&sort='.$sort; 
		if($order!='') $filter .= '&order='.$order; 
		if($rows!='') $filter .= '&rows='.$rows; 
		if($md !='') $filter .= '&md='.$md;
		if($ty !='') $filter .= '&ty='.$ty;
		if($lb !='') $filter .= '&lb='.$lb;
		
		return Redirect::to($module . $filter);
	}
	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_message'); 
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		return view('message.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			return view('message.view',$this->data);
		} else {
			return Redirect::to('message')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request, $id =0)
	{

		if(\Session::get('model_id')){
		$uid = \Session::get('model_id');
		}
		elseif(\Session::get('agent_id'))
		{
		$uid = \Session::get('agent_id');
		}

		elseif(\Session::get('logged_id'))
		{
		$uid = \Session::get('logged_id');
		}
		 

		$rules = $this->validateForm();
		
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			
			$data = $this->validatePost('message');
			$input=$request->all();

			$images=array();
			if($request->hasFile('images')) {
			$files=$request->file('images');
			foreach($files as $file){
			// $name=$file->getClientOriginalExtension();
			$name=date('His').str_replace(' ', '', $file->getClientOriginalName());
			$file->move('uploads/message',$name);
			$images[]=$name;

			}
			}

						// echo "<pre>";print_r($images);exit;

			if($uid==\Session::get('model_id'))
			{
			$data['model_agent'] = 1; //model
			}
			else if($uid==\Session::get('agent_id'))
			{
			$data['model_agent'] = 2; //agent
			}
			else if($uid==\Session::get('logged_id'))
			{
			$data['model_agent'] = 3; //admin
			}
			$data['entry_by'] = $uid;
			$data['recipient'] = $request->input('recipient');
			if($request->input('recipient_typee') !='')
			$data['recipient_type'] = $request->input('recipient_typee');
			else
			$data['recipient_type'] = $request->input('recipient_type');	
			// print_r($data);exit;
// $i=str_ireplace('<p>&nbsp;</p>', '', $request->input('message'));
			$text = strip_tags($request->input('message'));
			$data['message'] = $text;
			$data['draft'] = $request->input('draft');
			$now_date = date("Y-m-d H:i:s");
			$data['createdOn'] = $now_date;
			$data['updatedOn'] = $now_date;
			if($request->input('remaining')!=''){

			$data['attachments']=$request->input('remaining');
			}
			else{
			$data['attachments'] = implode(",",$images);

			}
	
if($request->input('draft')==1 && $request->input('messageid')!='')
{

		if ($request->input('remaining')!='' ) {
		$data['attachments'] = $request->input('remaining');
		}
		else if ($request->input('duplicate')!='' || $request->file('images')!='') {
		$data['attachments'] = trim($request->input('duplicate').','.implode(",",$images),',');
		}
		elseif($request->file('images')!=''){
		$data['attachments'] =implode(",",$images);
		}

		if($request->input('deleted')!=''){
			// $draft_image= explode(',',$request->input('deleted'));
			// print_r($draft_image);exit;
			//foreach ($request->input('deleted') as $key => $value) {
			$path='uploads/message/'.$request->input('deleted');
			unlink($path);
			//}
			}
		$newID = \DB::table('tb_message')
        ->where('id', $request->input('messageid'))  
        ->update($data);
			// return Redirect::back()
			// 		->with('messagetext', 'Draft saved Successfully')
			// 		->with('msgstatus','success')
			// 		->withInput();


}
if($request->input('draft')==0 && $request->input('messageid')!='')
{
			if ($request->input('remaining')!='' ) {
			$data['attachments'] = $request->input('remaining');
			}
			elseif($request->input('duplicate')!='' || $request->file('images')!=''){
			$data['attachments'] = trim($request->input('duplicate').','.implode(",",$images),',');
			}
			elseif($request->input('duplicate')){
			$data['attachments']=$request->input('duplicate');
			}
			if($request->input('deleted')){
				// $draft_image= explode(',',$request->input('deleted'));
				// foreach ($draft_image as $key => $value) {
				$path='uploads/message/'.$request->input('deleted');
				unlink($path);
			// }
			}
		$newID = \DB::table('tb_message')
        ->where('id', $request->input('messageid'))  
        ->delete();
        $newID = $this->model->insertRow($data, '');
			// return Redirect::back()
			// 		->with('messagetext', 'Draft saved Successfully')
			// 		->with('msgstatus','success')
			// 		->withInput();

}
else{
if ($request->input('remaining')!='' ) {
$data['attachments'] = $request->input('remaining');
}
elseif($request->input('duplicate')!='' && $request->file('images')!=''){
$data['attachments'] = trim($request->input('duplicate').','.implode(",",$images),',');
}
elseif($request->input('duplicate')){
$data['attachments']=$request->input('duplicate');
}
			
			// print_r($request->all());exit;

			if($request->has('messageid')){
				$newID = $this->model->insertRow($data, '');
			} 
			else{
				$newID = $this->model->insertRow($data , $request->input('messageid'));
			}				
			if(!is_null($request->input('apply')))
			{
				$return = 'message/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'message?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}
			// return json_encode(array('status' => '1'));
			// return  Redirect::back();
			
			
		
		}
		if($request->input('typee')==3){
		return Redirect::back()
					->with('messagetext', 'Your message saved as Draft')
					->with('msgstatus','success')
					->withInput();
		}
		else{
			return Redirect::back()
					->with('messagetext', 'Your message send Successfully')
					->with('msgstatus','success')
					->withInput();
		}
	} else {
			return json_encode(array('status' => '0'));

			//return Redirect::to('message/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
		}	
	
	}	
	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('message?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('message?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Message();
		$info = $model::makeInfo('message');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('message.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('message.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_message');		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}
// 	public function getShows( Request $request, $id = null)
//     {
// if($request->ajax()){
// $output='';
// $user = \Auth::user();
//         $uid = $user->id;
//         $users_data = $this->model->getMessageUsers( $uid );
// $usersNameList = array();
//         foreach($users_data as $user_data){
            
//             $usersNameList[$user_data->id] = $user_data->first_name.' '.$user_data->last_name;
            

//         }
    
//         $this->data['usersNameList'] = $usersNameList;

// $customers=\DB::table('tb_message')->where('message','LIKE','%'.$request->search.'%')->get();
//                 // echo "<pre>";print_r($rowUser );exit;

// foreach($customers as $key =>$customer){
//                 $rowUser = $usersNameList[$customer->recipient]; 
//                 // echo "<pre>";print_r($rowUser );exit;

// $output.='<div class="fullmsg_blg">'.

// '<ul>'.
// '<li>'. 
// '<div class="usertext">'.
// '<a href="javascript:void(0)">'. $rowUser.'</a>'.  
// '<p>'.$customer->message .'...</p>'.'</div>'.
// '<div class="more_dots">'.
// '<a href="javascript:void(0)"></a>'.
// '<p>5 min</p>'.
// '</div>'.
// '</li> '.
// '</ul> '.
// '</div> ';

// }
// return Response($output);
// }
// }	
  
  public function postResumeSave($uid,Request $request)
    {
    	    $document = $request->file('cvupload');
            $file_type = $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size = $document->getSize();
			
			if($file_type == 'pdf'){
				$pdftext = file_get_contents($document);
				$pdfPages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
			} else {
				$pdfPages = '';
			}
			$pathName = $uid.time().'.'.$file_type;
			$request->file('cvupload')->move('./uploads/cv/',$pathName);

       			$cv = \DB::table('tb_message')->where('id','=',$uid)->update(['pdf' => $pathName]);
	   
	                $return_data = array(
	                    'status'=>true,
	                    'file_id'=>"",
	                    'file_title'=> $file_title,
	                    'file_size'=> \SiteHelpers::HumanFileSize($file_size),
	                    
	                );
	            
        	

       return response()->json($return_data);
    }
      public function postPPTSave($uid,Request $request)
    {
    	    $document = $request->file('pptupload');
            $file_type = $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size = $document->getSize();
			
			if($file_type == 'ppt'){
				$pdftext = file_get_contents($document);
				$pdfPages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
			} else {
				$pdfPages = '';
			}
			$pathName = $uid.time().'.'.$file_type;
			$request->file('pptupload')->move('./uploads/ppt/',$pathName);
			
       			$cv = \DB::table('tb_message')->where('id','=',$uid)->update(['ppt' => $pathName]);
	   
	                $return_data = array(
	                    'status'=>true,
	                    'file_id'=>"",
	                    'file_title'=> $file_title,
	                    'file_size'=> \SiteHelpers::HumanFileSize($file_size),
	                    
	                );
	            
        	

       return response()->json($return_data);
    }

	public function getAjaxData(Request $request)
		{
			
			$candidates = $this->model->autocomplete_model($request->input('name'));
			echo json_encode($candidates);
		}
	public function getAutoData(Request $request)
		{
			$candidates = $this->model->autocomplete_agent($request->input('name'));
			echo json_encode($candidates);
		}	

		

}