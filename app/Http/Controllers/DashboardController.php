<?php namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends Controller {

	public function __construct()
	{
		parent::__construct();
        $this->data = array(
            'pageTitle' =>  CNF_APPNAME,
            'pageNote'  =>  'Welcome to Dashboard',
            
        );			
	}

	public function getIndex( Request $request )
	{		
		$this->data['online_users'] = \DB::table('tb_users')->orderBy('last_activity','desc')->limit(10)->get(); 
		//get latest chat from model
		$this->data['message']= \DB::table('tb_message')
			->Where('draft','=','0')
		// ->select('tb_models.id','tb_models.firstname','tb_models.lastname','tb_models.profile_photo','tb_message.message','tb_message.updatedOn')
  //       ->leftJoin('tb_models', 'tb_models.id', '=', 'tb_message.entry_by')
  //       ->Where('tb_models.status','1')
  //       ->Where('tb_message.model_agent','1')
		->orderBy('tb_message.id','desc')->limit(3)->get(); 

		//get latest event
			$this->data['events']= \DB::table('tb_events')
			->Where('status','Active')
			->Where('usertype','Agent')
			
			->orderBy('id','desc')->limit(10)->get(); 
					//get latest Blogs
			$this->data['Blogs']= \DB::table('tb_blogs')
			->Where('status','enable')
			->orderBy('id','desc')->limit(3)->get(); 
			//get latest Images of agents & models
			$this->data['gallery'] = \DB::table('tb_images')
			->where('status','Active')
			->orderBy('id', 'desc')
			->paginate(9);

			//get latest Videos

			$this->data['videos'] =\DB::table('videos')->where('status',1)->orderBy('id', 'desc')->first();
			// echo "<pre>";print_r($this->data['videos']);exit;


		$this->data['active'] = '';
		return view('dashboard.index',$this->data);
	}	

	public function getDashboard()
	{
		
	}


}