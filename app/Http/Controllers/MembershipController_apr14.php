<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Membership;
use App\Models\Modele;
use App\Models\Agency;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Http\Controllers\StripeController;

class MembershipController extends Controller {

    protected $layout = "layouts.main";
    protected $data = array();  
    public $module = 'membership';
    static $per_page    = '10';

    public function __construct()
    {
        
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->model = new Membership();
        $this->modele = new Modele();
        $this->agency = new Agency();
        
        $this->info = $this->model->makeInfo( $this->module);
        $this->access = $this->model->validAccess($this->info['id']);
    
        $this->data = array(
            'pageTitle' =>  $this->info['title'],
            'pageNote'  =>  $this->info['note'],
            'pageModule'=> 'membership',
            'return'    => self::returnUrl()
            
        );
        
        \App::setLocale(CNF_LANG);
        if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

        $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
        \App::setLocale($lang);
        }  

    }

    public function getIndex( Request $request )
    {

        if($this->access['is_view'] ==0) 
            return Redirect::to('dashboard')
                ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

        $sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'plan_id'); 
        $order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
        // End Filter sort and order for query 
        // Filter Search for query      
        $filter = '';   
        if(!is_null($request->input('search')))
        {
            $search =   $this->buildSearch('maps');
            $filter = $search['param'];
            $this->data['search_map'] = $search['maps'];
        } 

        
        $page = $request->input('page', 1);
        $params = array(
            'page'      => $page ,
            'limit'     => (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
            'sort'      => $sort ,
            'order'     => $order,
            'params'    => $filter,
            'global'    => (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
        );
        // Get Query 
        $results = $this->model->getRows( $params );        
        
        // Build pagination setting
        $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1; 
        $pagination = new Paginator($results['rows'], $results['total'], $params['limit']); 
        $pagination->setPath('membership');
        
        $this->data['rowData']      = $results['rows'];
        // Build Pagination 
        $this->data['pagination']   = $pagination;
        // Build pager number and append current param GET
        $this->data['pager']        = $this->injectPaginate();  
        // Row grid Number 
        $this->data['i']            = ($page * $params['limit'])- $params['limit']; 
        // Grid Configuration 
        $this->data['tableGrid']    = $this->info['config']['grid'];
        $this->data['tableForm']    = $this->info['config']['forms'];   
        // Group users permission
        $this->data['access']       = $this->access;
        // Detail from master if any
        
        // Master detail link if any 
        $this->data['subgrid']  = (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
        // Render into template
        return view('membership.index',$this->data);
    }   



    function getUpdate(Request $request, $id = null)
    {
    
        if($id =='')
        {
            if($this->access['is_add'] ==0 )
            return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
        }   
        
        if($id !='')
        {
            if($this->access['is_edit'] ==0 )
            return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
        }               
                
        $row = $this->model->find($id);
        if($row)
        {
            $this->data['row'] =  $row;
        } else {
            $this->data['row'] = $this->model->getColumnTable('tb_membership_plan'); 
        }
        $this->data['fields']       =  \SiteHelpers::fieldLang($this->info['config']['forms']);
        
        $this->data['id'] = $id;
        return view('membership.form',$this->data);
    }   

    public function getShow( Request $request, $id = null)
    {

        if($this->access['is_detail'] ==0) 
        return Redirect::to('dashboard')
            ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
                    
        $row = $this->model->getRow($id);
        if($row)
        {
            $this->data['row'] =  $row;
            $this->data['fields']       =  \SiteHelpers::fieldLang($this->info['config']['grid']);
            $this->data['id'] = $id;
            $this->data['access']       = $this->access;
            $this->data['subgrid']  = (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
            $this->data['prevnext'] = $this->model->prevNext($id);
            return view('membership.view',$this->data);
        } else {
            return Redirect::to('membership')->with('messagetext','Record Not Found !')->with('msgstatus','error');                 
        }
    }   

    function postSave( Request $request)
    {
        // echo '<pre>';print_r($request->input());exit;
        $rules = $this->validateForm();
        $validator = Validator::make($request->all(), $rules);  
        if ($validator->passes()) {
            $data = $this->validatePost('tb_membership');
            $data['image_limit'] = $request->get('image_limit');
            $data['video_limit'] = $request->get('video_limit');
            $data['event_limit'] = $request->get('event_limit');
            $data['type'] = $request->get('agree');


                $gateway = \Omnipay::gateway('stripe');
                $options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
                $stripe = array();
                foreach($options as $results)
                {
                    $stripe[$results->option_key]   = $results->options;
                }
                $gateway->setApiKey($stripe['secret_key']);

                $monthly_id = str_random(12);
                $monthly_name = $request->input('plan_name').' Monthly';
                $monthly_amt = number_format($request->input('plan_amount'),2)*100;

                 // $product = $gateway->createProduct(array(
                 // "name" => $monthly_name,
                 // "type" => "service",
                 //    ));
                // monthly plan
                $gateway->createPlan([
                    "id" => $monthly_id,
                    "name" => $monthly_name,
                    "amount" => $monthly_amt,
                    "interval" => "month",
                    "currency" => 'USD'
                ])->send();
                $data['stripe_plan'] = $monthly_id;

                $yearly_id = str_random(12);
                $yearly_name = $request->input('plan_name').' Yearly';
                $yearly_amt = number_format($request->input('plan_amount_y'),2)*100;
                // yearly plan
                $gateway->createPlan([
                    "id" => $yearly_id,
                    "name" => $yearly_name,
                    "amount" => $yearly_amt,
                    "interval" => "year",
                    "currency" => 'USD'
                ])->send();
                $data['stripe_plan_y'] = $yearly_id;

            
            $id = $this->model->insertRow($data , $request->input('plan_id'));
            
            if(!is_null($request->input('apply')))
            {
                $return = 'membership/update/'.$id.'?return='.self::returnUrl();
            } else {
                $return = 'membership?return='.self::returnUrl();
            }

            // Insert logs into database
            if($request->input('plan_id') =='')
            {
                \SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
            } else {
                \SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
            }

            return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
            
        } else {

            return Redirect::to('membership/update/'.$request->input('plan_id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
            ->withErrors($validator)->withInput();
        }   
    
    }    

    public function postDelete( Request $request)
    {
        
        if($this->access['is_remove'] ==0) 
            return Redirect::to('dashboard')
                ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
        // delete multipe rows 
        if(count($request->input('ids')) >=1)
        {
            $this->model->destroy($request->input('ids'));
            
            \SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
            // redirect
            return Redirect::to('membership?return='.self::returnUrl())
                ->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
    
        } else {
            return Redirect::to('membership?return='.self::returnUrl())
                ->with('messagetext','No Item Deleted')->with('msgstatus','error');             
        }

    }   

    public static function display( )
    {
        $mode  = isset($_GET['view']) ? 'view' : 'default' ;
        $model  = new Membership();
        $info = $model::makeInfo('membership');

        $data = array(
            'pageTitle' =>  $info['title'],
            'pageNote'  =>  $info['note']
            
        );

        if($mode == 'view')
        {
            $id = $_GET['view'];
            $row = $model::getRow($id);
            if($row)
            {
                $data['row'] =  $row;
                $data['fields']         =  \SiteHelpers::fieldLang($info['config']['grid']);
                $data['id'] = $id;
                return view('membership.public.view',$data);
            } 

        } else {

            $page = isset($_GET['page']) ? $_GET['page'] : 1;
            $params = array(
                'page'      => $page ,
                'limit'     =>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
                'sort'      => 'plan_id' ,
                'order'     => 'asc',
                'params'    => '',
                'global'    => 1 
            );

            $result = $model::getRows( $params );
            $data['tableGrid']  = $info['config']['grid'];
            $data['rowData']    = $result['rows'];  

            $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1; 
            $pagination = new Paginator($result['rows'], $result['total'], $params['limit']);   
            $pagination->setPath('');
            $data['i']          = ($page * $params['limit'])- $params['limit']; 
            $data['pagination'] = $pagination;
            return view('membership.public.index',$data);           
        }


    }

    function postSavepublic( Request $request)
    {
        
        $rules = $this->validateForm();
        $validator = Validator::make($request->all(), $rules);  
        if ($validator->passes()) {
            $data = $this->validatePost('tb_membership_plan');      
             $this->model->insertRow($data , $request->input('plan_id'));
            return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
        } else {

            return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
            ->withErrors($validator)->withInput();

        }   
    
    }   
    public function  getMembershipPlan()
    {


        // get usertype
        $usertype = \session::get('user_type');
        $loggedId = \session::get('logged_id'); 

        if($usertype == 'admin'){
            echo 'Admin cant able to access this page '; exit;
        }

        $sql = \DB::table('tb_pages')->where('default',1)->get();
        if(count($sql)>=1)
        {
            $row = $sql[0];

            $this->data['pageTitle']    = 'Membership Plan';
                // $this->data['pageNote']  =  $row->note;
            $this->data['breadcrumb']   = 'inactive';   
            $this->data['pageMetakey']  =   '' ;
            $this->data['pageMetadesc'] =  '' ;
            $this->data['filename']     =  'membershipplan';                
            if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
            {
                $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
            } else {
                $page_template = 'layouts.'.CNF_THEME.'.template.page';
            }
            $this->data['pages'] = $page_template;
            if($usertype == 'model' )
            {
                 $data['memberships'] = $this->model->where('type','model')->get();
            }
            elseif( $usertype == 'agent')
            {
                 $data['memberships'] = $this->model->where('type','agent')->get();
            }
            else{
                 $data['memberships'] = $this->model->get();
            }

            $sTableName  = ($usertype == 'model')?'tb_models':'tb_agency';
            $userDetail = \DB::table($sTableName)->where('id', '=', $loggedId)->first();

            $data['loggedId'] = $loggedId;
            $data['usertype'] = $usertype;

            if($userDetail){
                $data['plan_id'] = $userDetail->plan_id;
            }
            // echo '<pre>';print_r($data);exit;
            $this->data['content'] = view('membership.membershipplan',$data);
            $page = 'layouts.'.CNF_THEME.'.index';
            return view($page,$this->data);         
                }
            }
    public function  getBuyMembershipPlan(Request $request)
    {
        $id=$request->id;
        $data['plan_id']=$request->id;
        if(\Session::get('model_id'))
            \DB::table('tb_models')
            ->where('id', \Session::get('model_id'))
            ->update(['plan_id' => $id]);
        elseif(\Session::get('agent_id')){
        \DB::table('tb_agency')
            ->where('id', \Session::get('agent_id'))
            ->update(['plan_id' => $id]);
        }

    }

    public function  postBuyMembershipPlan(Request $request)
    {
        $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Membership Plan';
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] =  '' ;
                $this->data['filename']     =  'membershipplan';                
                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }
                $this->data['pages'] = $page_template;

                $options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
        
                $stripe = array();
                foreach($options as $results)
                {
                    $stripe[$results->option_key]   = $results->options;
                }
                $data['publishable_key'] =$stripe['publishable_key'];
                $data['plan_membership'] = $request->input('plan_membership');
                $data['membership_type'] = $request->input('membership_type');


                $this->data['content'] = view('membership.payform',$data);
                $page = 'layouts.'.CNF_THEME.'.index';

                 // echo '<pre>';print_r($data);exit;
                return view($page,$this->data);         
        
    }
        // $id=$request->id;
        // $data['plan_id']=$request->id;
        // if(\Session::get('model_id'))
        //  \DB::table('tb_models')
  //           ->where('id', \Session::get('model_id'))
  //           ->update(['plan_id' => $id]);
        // elseif(\Session::get('agent_id')){
        // \DB::table('tb_agency')
  //           ->where('id', \Session::get('agent_id'))
  //           ->update(['plan_id' => $id]);
  //       }

    }


    public function postStripe( Request $request ){
        // get the curren logged user details
        $userId = $iLoggedId   = \Session::get('logged_id');
        $sUserType   = \Session::get('user_type');
        $sTableName  = ($sUserType == 'model')?'tb_models':'tb_agency';

        $plan_id = $request->input('plan_membership');
        $plan_type = $request->input('membership_type');
        $stripeToken = $request->get('stripeToken');
        $purchase_type = 'membership';
        $created = date('Y-m-d H:i:s');

        $userDetail = \DB::table($sTableName)->where('id', '=', $iLoggedId)->first();
        $user_email = $userDetail->email;

        if(!empty($plan_id)){
            $membership_plan = \DB::table('tb_membership_plan')->where('plan_id',$plan_id )->first();
            if($membership_plan){
                $stripes_plan_detail = \DB::table('tb_options')->where('code', '=', 'stripe')->where('option_key', '=', 'TestMode')->first();

                if($plan_type == 'm'){
                    $amount = $membership_plan->plan_amount;
                    if($stripes_plan_detail->options == 'true'){
                        $stripe_plan = $membership_plan->stripe_plan;
                    }else{
                        $stripe_plan = $membership_plan->stripe_plan_live;
                    }
                    $plan_amount = $membership_plan->plan_amount;
                    $plan_period = 'month';
                } else if($plan_type == 'y'){
                    $amount = $membership_plan->plan_amount_y;
                    if($stripes_plan_detail->options == 'true'){
                        $stripe_plan = $membership_plan->stripe_plan_y;
                    }else{
                        $stripe_plan = $membership_plan->stripe_plan_y_live;
                    }
                    $plan_amount = $membership_plan->plan_amount_y;
                }
                $plan_name = $membership_plan->plan_name;
                $plan_period = 'year';
                
            }
        }

        
        $gateway = \Omnipay::gateway('stripe');
        $options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
        $stripe = array();
        foreach($options as $results)
        {
            $stripe[$results->option_key]   = $results->options;
        }
        $gateway->setApiKey($stripe['secret_key']);

        $responseisSuccessful = false;


        // prepare and store card details on stripe
        $nameOnCard = $request->get('nameOnCard');
        $nameparts = explode(" ", $nameOnCard);
        $lastName = array_pop($nameparts);
        if(!empty($nameparts)){
            $firstName = implode(" ", $nameparts);
        } else {
            $firstName = $lastName;
            $lastName = '';
        }

        $cardExpire = explode(" / ", $request->get('card-expiry-year'));

        $firstName = trim($firstName);
        $lastName = trim($lastName);
        $cardnumber = $request->get('card-number');
        $expiryMonth = $cardExpire[0];
        $expiryYear = $cardExpire[1];
        $cvv = $request->get('card-cvc');

        $card = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'number' => $cardnumber,
            'expiryMonth' => $expiryMonth,
            'expiryYear' => $expiryYear,
            'cvv' => $cvv,
            'email' => $user_email,
        );

        $course_plan_id = $plan_id;
        $cust_desc = "Member for ".$user_email;

        if(isset($userDetail->username)){
            $username = $userDetail->username;
        }else{
            $username = '';
        }
        
        if(isset($userdetails->stripe_customer_id)){
           $stripe_customer_id = $userdetails->stripe_customer_id;
        }else{
           $stripe_customer_id = 'test';
        }


        $stripe_customer = $gateway->fetchCustomer(['customerReference'=>$stripe_customer_id])->send();
        $customer_data = $stripe_customer->getdata();
        if(isset($customer_data['error'])){
            $c_response = $gateway->createCustomer([
                "description" => $cust_desc,
                "email" => $user_email,
                'metadata' => ['First Name' => $firstName,'Last Name'=>$lastName,'User Name'=>$username],
                ])->send();
            $customerid = $c_response->getCustomerReference();
            $cardid = '';                   
        }else{
            $customerid = $customer_data['id'];
            if(isset($customer_data['sources']['data']['0']['id'])){
                $cardid = $customer_data['sources']['data']['0']['id'];
            }else{
                $cardid = '';
            }
        }
        if($cardid && $customerid){
            $delete_response = $gateway->deleteCard(array(
                'customerReference' => $customerid,
                'cardReference'        => $cardid,
                'token'         => $stripeToken,
                ))->send();
        }
        if($customerid){
            $response_card = $gateway->createCard(array(
                'card'              => $card,
                'customerReference' => $customerid,
                'token'         => $stripeToken,
                ))->send();
        }

        if($stripe_plan){
            $plan = $gateway->createPlan([
            'id' => $stripe_plan,
            'amount' => floatval($amount)*100,
            'currency' => 'USD',
            'interval' => $plan_period,
            'name' => $plan_name,
            'token'         => $stripeToken,
            ])->send();
        }else{ 
            $stripe_plan = str_random(12);
            $plan = $gateway->createPlan([
            'id' => $stripe_plan,
            'amount' => floatval($plan_amount)*100,
            'currency' => 'USD',
            'interval' => $plan_period,
            'name' => $plan_name,
            'token'         => $stripeToken,
            ])->send();
            if($stripes_plan_detail->options == 'true'){
                if($plan_type == 'm'){
                    \DB::table('tb_membership_plan')->where('plan_id',$plan_id )->update(array('stripe_plan'=>$stripe_plan));
                }else{
                    \DB::table('tb_membership_plan')->where('plan_id',$plan_id )->update(array('stripe_plan_y'=>$stripe_plan));
                }

            }else{
                if($plan_type == 'm'){
                    \DB::table('tb_membership_plan')->where('plan_id',$plan_id )->update(array('stripe_plan_live'=>$stripe_plan));
                }else{
                    \DB::table('tb_membership_plan')->where('plan_id',$plan_id )->update(array('stripe_plan_y_live'=>$stripe_plan));
                }
            }
            

        }


        \DB::table($sTableName)->where('id', '=', $iLoggedId)->update(array('stripe_customer_id'=>$customerid));

        $order_details['customerReference'] = $customerid;

        $new_stripe = new StripeController();
        $new_stripe_data["plan"] = $stripe_plan;
        
        $response = $new_stripe->send('customers/'.$customerid.'/subscriptions',$new_stripe_data,'POST');
        $token_response = '';
        if(isset($response['id'])){
            $token_response = $response['id'];
            $responseisSuccessful = true;
        }
        $order_details['subscriptionReference'] = $token_response;
        $order_details['subscriptionEvent'] = $response;

        if ($responseisSuccessful) {
            $transaction['user_id'] = $userId;
            $transaction['user_type'] = $sUserType;
            $transaction['membership_id'] = $plan_id;
            $transaction['amount'] = floatval($amount);
            $transaction['status'] = 'active';
            $transaction['transaction_status'] = 'completed';
            $transaction['payment_method'] = 'stripe';
            $transaction['purchase_period'] = $plan_period;

            $transaction['order_details'] =  json_encode($order_details);


            $transaction_id = \DB::table('tb_transactions')->insertGetId($transaction);

            \DB::table($sTableName)->where('id',  $iLoggedId)->update(['plan_id' => $plan_id]);

            //update user plan
            $plan_level = \DB::table('tb_plan_level')->where('user_id', '=', $userId)->where('user_type', '=', $sUserType)->first();


            if($plan_level){
                $data['pruchased_on'] = $created;
                $data['plan_id'] = $plan_id;
                $data['create_on'] = $created;

                \DB::table('tb_plan_level')->where('id', $plan_level->id)->update($data);
            }else{
                $data['user_id'] = $userId;
                $data['user_type'] = $sUserType;
                $data['plan_id'] = $plan_id;
                $data['pruchased_on'] = $created;
                $data['status'] = 'active';
                $data['create_on'] = time();
                $plan_level_id = \DB::table('tb_plan_level')->insertGetId($data);
            }

            return Redirect::to('membership/payment/'.$transaction_id);
                
        }else{
            $transaction = array();
            $transaction['user_id'] = $userId;
            $transaction['user_type'] = $sUserType;
            $transaction['membership_id'] = $plan_id;
            $transaction['amount'] = floatval($amount);
            $transaction['status'] = 'cancelled';
            $transaction['transaction_status'] = 'cancelled';
            $transaction['purchase_period'] = $plan_period;
            $transaction['payment_method'] = 'stripe';
            $transaction['order_details'] = json_encode($order_details);

            $transaction_id = \DB::table('tb_transactions')->insertGetId($transaction);


            //update user plan
            $plan_level = \DB::table('tb_plan_level')->where('user_id', '=', $userId)->where('user_type', '=', $sUserType)->first();


            if(!$plan_level){
                $data['user_id'] = $userId;
                $data['user_type'] = $sUserType;
                $data['plan_id'] = $plan_id;
                $data['pruchased_on'] = $created;
                $data['status'] = 'active';
                $data['create_on'] = $created;
                $plan_level_id = \DB::table('tb_plan_level')->insertGetId($data);
            }


            return Redirect::to('membership/payment/'.$transaction_id);
        }
    }

    public function getPaymentform( Request $request, $id = null ){
        $sql = \DB::table('tb_pages')->where('default',1)->get();
        if(count($sql)>=1)
        {
            $row = $sql[0];

            $this->data['pageTitle']    = 'Membership Plan';
            // $this->data['pageNote']  =  $row->note;
            $this->data['breadcrumb']   = 'inactive';   
            $this->data['pageMetakey']  =   '' ;
            $this->data['pageMetadesc'] =  '' ;
            $this->data['filename']     =  'payment_form';              
            if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
            {
                $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
            } else {
                $page_template = 'layouts.'.CNF_THEME.'.template.page';
            }
            $this->data['pages'] = $page_template;

            $transaction = \DB::table('tb_transactions')
            ->select('*')
            ->leftJoin('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_transactions.membership_id')
            ->where('id', '=', $id)->first();

            if($transaction->transaction_status == 'completed'){
                $transaction->statusMessage = 'Subscription for this membership is successful.';
            }else{
                $transaction->statusMessage = 'Failed to subscribe for this membership.';
            }

            $sTableName  = ($transaction->user_type == 'model')?'tb_models':'tb_agency';
            $user = \DB::table($sTableName)->select('id','username')->where('id',$transaction->user_id)->first();
            $transaction->username = $user->username;


            $data['transaction'] = $transaction;


            $this->data['content'] = view('membership.payment_form',$data);
            $page = 'layouts.'.CNF_THEME.'.index';

            return view($page,$this->data);         
    
        }
    }



    public function postStripenotify( Request $request )
    {

      // retrieve the request's body and parse it as JSON
        $body   = @file_get_contents('php://input');
        $event_json = json_decode($body,true);
        if(isset($event_json['data'])){ 
            $created = date('Y-m-d H:i:s');

            if(isset($event_json['data']['object']['lines']['data']) && !empty($event_json['data']['object']['lines']['data'])){


                $lines_data = $event_json['data']['object']['lines']['data'];
                foreach ($lines_data as $key => $data) {
                    echo $subscription_id = $data['id'];
                    $transactions = \DB::table('tb_transactions')->whereRaw(\bsetecHelpers::getdbprefix().'transactions.order_details REGEXP "'.$subscription_id.'"')->orderBy('id','DESC')->first();

                    if($transactions){

                        if($transactions->transaction_status == 'completed' && $transactions->status == 'active' && $transactions->webhook_check_status == 1){
                            $new_transaction_id = 0;
                            $transaction_id = $transactions->id;
                            $user_id = $transactions->user_id;
                            $odr_details = json_decode($transactions->order_details,true);

                            if($event_json['type'] == 'invoice.payment_failed'){ // subscription renewal failed
                                $odr_details['failedEvent'.date('_Y_m_d')] = $event_json;
                                $odr_details['last_failed_charge'] = $event_json['data']['object']['charge'];
                                $transactionupdate['order_details'] = json_encode($odr_details);
                                $transactionupdate['status'] = 'failed';
                                $transactionupdate['transaction_status'] = 'failed';
                                $transactionupdate['webhook_check_status'] = '1';

                                $old_odr_details = json_decode($transactions->order_details,true);

                                if(isset($old_odr_details['subscriptionReference']) && $old_odr_details['subscriptionReference'] && isset($old_odr_details['customerReference']) && $old_odr_details['customerReference']){

                                    $subscriptionReference = $old_odr_details['subscriptionReference'];
                                    $customerReference = $old_odr_details['customerReference'];

                                    $gateway = \Omnipay::gateway('stripe');
                                    $options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
                                    $stripe = array();
                                    foreach($options as $results)
                                    {
                                        $stripe[$results->option_key]   = $results->options;
                                    }
                                    $gateway->setApiKey($stripe['secret_key']);
                                    
                                    $response   = $gateway->cancelSubscription([
                                        "subscriptionReference" => $subscriptionReference,
                                        "customerReference" => $customerReference
                                    ])->send();
                                }                                       
                                
                                $transaction = array();
                                $transaction['user_id'] = $user_id;
                                $transaction['user_type'] = $transactions->user_type;
                                $transaction['membership_id'] = $transactions->membership_id;
                                $transaction['amount'] = 0;
                                $transaction['status'] = 'cancelled';
                                $transaction['transaction_status'] = 'cancelled';
                                $transaction['purchase_period'] = $transactions->purchase_period;
                                $transaction['payment_method'] = $transactions->payment_method;
                                $transaction['order_details'] = $transactions->order_details;
                                $transaction_id = \DB::table('tb_transactions')->insertGetId($transaction);

                                //update user plan
                                $plan_level = \DB::table('tb_plan_level')->where('user_id', '=', $user_id)->where('user_type', '=', $transactions->user_type)->first();

                                if(!$plan_level){
                                    $data['user_id'] = $user_id;
                                    $data['user_type'] =  $transactions->user_type;
                                    $data['plan_id'] = '1';
                                    $data['pruchased_on'] = $created;
                                    $data['status'] = 'active';
                                    $data['create_on'] = $created;
                                    $plan_level_id = \DB::table('tb_plan_level')->insertGetId($data);
                                }

                                //update the old transaction to completed
                                \DB::table('tb_transactions')->where('id',$transaction_id)->update(array('status'=>'closed','webhook_check_status'=>'1'));
                            } else if($event_json['type'] == 'invoice.payment_succeeded') { // subscription renewal success

                                $odr_details['successEvent'.date('_Y_m_d')] = $event_json;
                                $odr_details['last_success_charge'] = $event_json['data']['object']['charge'];
                                $odr_details['last_success_charge_date'] = date('Y-m-d H:i:s');
                                $transactionupdate['order_details'] = json_encode($odr_details);
                                $transactionupdate['status'] = 'active';
                                $transactionupdate['transaction_status'] = 'completed';
                                $transactionupdate['webhook_check_status'] = '1';

                                $amount_paid = $event_json['data']['object']['amount_paid'];
                                $amount_paid = floatval($amount_paid/100);
                                $customerid = $event_json['data']['object']['customer'];
                                $original_amount = $event_json['data']['object']['lines']['data']['0']['amount'];
                                if(isset($event_json['data']['object']['lines']['data']['0']['plan']['name'])){
                                    $purchase = $event_json['data']['object']['lines']['data']['0']['plan']['name'];
                                }else{
                                    $purchase = '';
                                }
                                
                                $original_amount = floatval($original_amount/100);
                                $discount = '0.00';
                                if(isset($event_json['data']['object']['discount'])){
                                 $discount = $original_amount - $amount_paid;
                                 $discount = floatval($discount);
                                }
                                $sTableName  = ($transactions->user_type == 'model')?'tb_models':'tb_agency';
                                $user = \DB::table($sTableName)->select('id','username')->where('id',$user_id)->first();
                                if($purchase){
                                    $amount_paid = number_format( (float) $amount_paid, 2, '.', '');
                                    $original_amount = number_format( (float) $original_amount, 2, '.', '');

                                    //save the transaction details in DB
                                    $transaction['user_id'] = $user_id;
                                    $transaction['user_type'] = $transactions->user_type;
                                    $transaction['membership_id'] = $transactions->membership_id;
                                    $transaction['amount'] = $transactions->amount;
                                    $transaction['status'] = 'active';
                                    $transaction['transaction_status'] = 'completed';
                                    $transaction['purchase_period'] = $transactions->purchase_period;
                                    $transaction['payment_method'] = $transactions->payment_method;
                                    $transaction['order_details'] = json_encode($odr_details);

                                    $new_transaction_id = \DB::table('tb_transactions')->insertGetId($transaction);

                                    //update the old transaction to completed
                                    \DB::table('tb_transactions')->where('id',$transaction_id)->update(array('status'=>'closed','webhook_check_status'=>'1'));
                                }
                            }

                            if($transactionupdate['transaction_status'] != '' && $new_transaction_id != 0){
                                \DB::table('tb_transactions')->where('id',$new_transaction_id)->update($transactionupdate);
                            }

                        }else{
                            //update the transaction details in DB
                            $transaction_id = $transactions->id;
                            $user_id = $transactions->user_id;
                            $odr_details = json_decode($transactions->order_details,true);

                            if($event_json['type'] == 'invoice.payment_failed'){ // subscription failed
                                $odr_details['failedEvent'.date('_Y_m_d')] = $event_json;
                                $odr_details['last_failed_charge'] = $event_json['data']['object']['charge'];
                                $transactionupdate['order_details'] = json_encode($odr_details);
                                $transactionupdate['status'] = 'failed';
                                $transactionupdate['transaction_status'] = 'failed';
                                $transactionupdate['webhook_check_status'] = '1';
                            } else if($event_json['type'] == 'invoice.payment_succeeded') { // subscription success
                                $odr_details['successEvent'.date('_Y_m_d')] = $event_json;
                                $odr_details['last_success_charge'] = $event_json['data']['object']['charge'];
                                $odr_details['last_success_charge_date'] = date('Y-m-d H:i:s');
                                $transactionupdate['order_details'] = json_encode($odr_details);
                                $transactionupdate['status'] = 'active';
                                $transactionupdate['transaction_status'] = 'completed';
                                $transactionupdate['webhook_check_status'] = '1';

                                $amount_paid = $event_json['data']['object']['amount_paid'];
                                $amount_paid = floatval($amount_paid/100);
                                $customerid = $event_json['data']['object']['customer'];
                                $original_amount = $event_json['data']['object']['lines']['data']['0']['amount'];
                                if(isset($event_json['data']['object']['lines']['data']['0']['plan']['name'])){
                                    $purchase = $event_json['data']['object']['lines']['data']['0']['plan']['name'];
                                }else{
                                    $purchase = '';
                                }
                                
                                $original_amount = floatval($original_amount/100);
                                $discount = '0.00';
                                if(isset($event_json['data']['object']['discount'])){
                                 $discount = $original_amount - $amount_paid;
                                 $discount = floatval($discount);
                                }
                                $sTableName  = ($transactions->user_type == 'model')?'tb_models':'tb_agency';
                                $user = \DB::table($sTableName)->select('id','username')->where('stripe_customer_id',$customerid)->first();
                                if($purchase){
                                    $amount_paid = number_format( (float) $amount_paid, 2, '.', '');
                                    $original_amount = number_format( (float) $original_amount, 2, '.', '');
                                }
                            }

                            if($transactionupdate['transaction_status'] != ''){
                                \DB::table('tb_transactions')->where('id',$transaction_id)->update($transactionupdate);
                            }
                        }

                    }
                }
            }

            header("HTTP/1.1 200 Transaction Success");
        } else {
            header("HTTP/1.1 201 Transaction Fail");
        }
    }

    public function getPayment(){
        $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Payment';
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] =  '' ;
                $this->data['filename']     =  'payment_form';              

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                $this->data['pages'] = $page_template;
                //$data['encoded_id'] = '';
                $this->data['content'] = view('membership.payment_form');
                $page = 'layouts.'.CNF_THEME.'.index';


                return view($page,$this->data);         
            }

    }



}