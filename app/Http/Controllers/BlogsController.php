<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Blogs;
use App\Library\Slimdown;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Sximo;


class BlogsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'blogs';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Blogs();
		$this->option = new Sximo();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'blogs',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

			$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
			\App::setLocale($lang);
		}  


		
	}

	public function getBlogList( Request $request)
	{

		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$params = array(
			'page'		=> $page ,
			'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
			'sort'		=> 'id' ,
			'order'		=> 'desc',
			// 'params'	=> " AND pagetype ='blogs'  " ,
			'global'	=> 1 
		);		
		return self::articles( $params , $page , 'all' );

	}

	public function getIndex( Request $request )
	{
		if(\Auth::check()){
			if($this->access['is_view'] ==0) 
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

			$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
			$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');

			// End Filter sort and order for query 
			// Filter Search for query		
			$filter = '';	
			if(!is_null($request->input('search')))
			{
				$search = 	$this->buildSearch('maps');
				$filter = $search['param'];
				$this->data['search_map'] = $search['maps'];
			} 

			
			$page = $request->input('page', 1);
			$params = array(
				'page'		=> $page ,
				'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
				'sort'		=> $sort ,
				'order'		=> $order,
				'params'	=> $filter,
				'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
			);
			// Get Query 
			$results = $this->model->getRows( $params );		
			
			// Build pagination setting
			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
			$pagination->setPath('blogs');

			$rows = $request->input('rows') ? $request->input('rows') : 5 ;

			$query = \DB::table('tb_blogs');
			if($request->get('title'))
			{
				$query->where('title','like','%'.$request->get('title').'%')->orderBy($sort,$order);
			}
			if($request->get('categories'))
			{
				$query->where('categories','like','%'.$request->get('categories').'%')->orderBy($sort,$order);
			}
			if($request->get('status'))
			{
				$query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);
			}
			if($request->get('description'))
			{
				$query->where('note','like','%'.$request->get('description').'%')->orderBy($sort,$order);
			}
			if($request->get('Tags'))
	    {

	    	// for multiple tags
	    	$alltags = explode(",",$request->get('Tags') );
	    	 // echo "<pre>";print_r($alltags);die;
	    	foreach ($alltags as $value) {
	    		if($value!="")
	    		{
	    			$query->orWhere('labels','like','%'.$value.'%')
	    				->orderBy($sort,$order);
	    		}
	    	}
	        // $query->where('Tags','like','%'.$request->get('Tags').'%');
	    }
			

			$this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows); 			
			$this->data['rowData']		= $results['rows'];
			// Build Pagination 
		$this->data['title'] = 	(!empty($request->get('title')))?$request->get('title'):'';
		$this->data['categories'] = (!empty($request->get('categories')))?$request->get('categories'):'';
		$this->data['description'] = (!empty($request->get('description')))?$request->get('description'):'';
		$this->data['labels'] = (!empty($request->get('Tags')))?$request->get('Tags'):'';	
			$this->data['pagination']	= $pagination;
			// Build pager number and append current param GET
			$this->data['pager'] 		= $this->injectPaginate();	
			// Row grid Number 
			$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
			// Grid Configuration 
			$this->data['tableGrid'] 	= $this->info['config']['grid'];
			$this->data['tableForm'] 	= $this->info['config']['forms'];	
			// Group users permission
			$this->data['access']		= $this->access;
			// Detail from master if any
			
			// Master detail link if any 
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			
			

			// Render into template
			return view('blogs.index',$this->data);
		}
		else{
			$sql = \DB::table('tb_blogs')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$data['pageTitle'] 	= 'Blogs';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'blogs';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				
				$page = isset($_GET['page']) ? $_GET['page'] : 1;
				$params = array(
					'page'		=> $page ,
					'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
					'sort'		=> 'created' ,
					'order'		=> 'desc',
					'params'	=> " AND pagetype ='blogs'  " ,
					'global'	=> 1 
				);		

				$model  = new Blogs();

				$info = $model::makeInfo('blogs');
				$data['pageLang'] = 'en';
				if(\Session::get('lang') != '')
					{
						$data['pageLang'] = \Session::get('lang');
					}		


					$result = $model::getRows( $params );
					$data['rowData'] 	= $result['rows'];	

					$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
					$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
					$pagination->setPath('');
					$data['i']			= ($page * $params['limit'])- $params['limit']; 
					$data['pagination'] = $pagination;

					
					
					$data['pageNote'] 	=  'View All';
					$data['breadcrumb'] 	= 'false';	
					$data['pageMetakey'] 	=  '' ;
					$data['pageMetadesc'] =  '' ;
					$data['gplusimg'] = '';
					$data['tweetimg'] = '';
					$data['v_url'] = ''; 
					$data['urls'] = '';
           			 $data['fbimg'] = '';
           			 $data['links']=$this->option->get_options('links');
					$data['filename'] 	=  '';	
					
					$this->data['content'] = view('blogs.main',$data);
					$page = 'layouts.'.CNF_THEME.'.index';

					return view($page,$this->data);			
				}
				
			}

		}	

	public  function getTags( Request $request , $label )
	{
		$iModelId = \SiteHelpers::getCategoryId($label); 

		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$params = array(
			'page'		=> $page ,
			'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
			'sort'		=> 'created' ,
			'order'		=> 'desc',
			'params'	=>  "AND labels REGEXP '". $label."' " ,
			'global'	=> 1 
		);		
		return self::articles( $params , $page , $label );

	}


	public  function getLabel( Request $request , $label )
	{

		$iModelId = \SiteHelpers::getCategoryId($label); 

		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$params = array(
			'page'		=> $page ,
			'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
			'sort'		=> 'created' ,
			'order'		=> 'desc',
			'params'	=> "AND categories REGEXP CONCAT('(^|,)(', REPLACE('".$iModelId->id."' , ',', '|'), ')(,|$)') " ,
			'global'	=> 1 
		);		
		return self::articles( $params , $page , $label );

	}	

	public function getView( Request $request, $title, $id = null)
	{
		$id=\SiteHelpers::getSecurityurlID($id);

		$row = $this->model->getRow($id);

		if($row)
		{	
			// update the view count 
			\DB::table('tb_blogs')->where('id',$row->id)->update(array('views'=> \DB::raw('views+1')));




			$data['pageLang'] = 'en';
			if(\Session::get('lang') != '')
			{
				$data['pageLang'] = \Session::get('lang');
			}	

			$data['conpost'] = json_decode(file_get_contents(base_path().'/resources/views/core/posts/config.json'),true);		

			$data['pageTitle'] 		= 	$row->title ;
			$data['pageNote'] 		=  	'View All';
			$data['breadcrumb'] 	= 	'inactive';	
			// $data['pageMetakey'] 	=  	$row->metakey;
			// $data['pageMetadesc'] 	=  	$row->metadesc ;
			$data['pageMetakey'] 	=  	'';
			$data['pageMetadesc'] 	=  	'';	
			$data['tweetimg'] = '';		
			$data['gplusimg'] = '';
			$data['filename'] 		=  	'';
			$data['v_url'] = ''; 	
			$data['urls'] = '';
            $data['fbimg'] = '';
            $data['links']=$this->option->get_options('links');

			$data['row'] 			=  $row;
			$sCateWhere             = '`id` IN ('.$row->categories.')';
			$aCategory 				= \SiteHelpers::getCategories($sCateWhere);

			$sCategory = '';
			if(!empty($aCategory))
			 $sCategory = implode(',', array_column($aCategory, 'name'));

			$data['author']         =  \SiteHelpers::getName('username',$row->userid);

			$data['cate']			= $sCategory;
			$data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$data['id'] 			= $id;
			$data['access']			= $this->access; 
			$data['prevnext'] 		= $this->model->prevNext($id);
			$data['labels']			= self::splitLabels($row->labels);
			$data['comments']		= $this->model->comments($row->id);
			$data['clouds']			= self::cloudtags();
			$data['latestposts']	= $this->model->latestposts();
					// get Sum ratings
					$ratedCounts =\DB::table('tb_comments')
					->selectRaw('sum(rating) as sum,count(rating) as total_count')
					->where('pageID',$id)
					->where('type','=','blogs')
					->first();
					$sum=$ratedCounts->sum;
					if($sum>0)
						$total=number_format((float)($sum)/($ratedCounts->total_count), 1, '.', '');
					else
						$total='Not yet Rated';

                //Total counts for particular videos 
					$data['avg_ratings']=$total;
			
			$page = 'layouts.'.CNF_THEME.'.index';

			$data['pages'] = 'blogs.blog_view';


			return view($page,$data);
		} else {
			return Redirect::to('blogs/list')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}		


	public static function articles($params , $page , $title = 'all')
	{
		// print_r($params);exit;

		$model  = new Blogs();
		$info   = $model::makeInfo('blogs');

		$data['pageLang'] = 'en';
		if(\Session::get('lang') != '')
		{
			$data['pageLang'] = \Session::get('lang');
		}		

		$result = $model::getRows( $params );

		foreach ($result['rows'] as $iKey => $val) {
	
			$sCateWhere             = '`id` IN ('.$val->categories.')';
			$aCategory 				= \SiteHelpers::getCategories($sCateWhere);
			$sCategory = '';
			if(!empty($aCategory))
				$sCategory = implode(',', array_column($aCategory, 'name'));

			$result['rows'][$iKey]->cate   = $sCategory;
			$result['rows'][$iKey]->author = \SiteHelpers::getName('username',$val->userid);
		}

		$data['rowData'] 	= $result['rows'];	
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
		$pagination->setPath('');
		$data['i']			= ($page * $params['limit'])- $params['limit']; 
		$data['pagination'] = $pagination;

		if($title !='all')
		{
			$data['pageTitle'] 	= 'Label :  '.$title;
				
		} else {
			$data['pageTitle'] 	= 'Blogs';
		}
$option = new Sximo();
		
		$data['pageNote'] 	=  'View All';
		$data['breadcrumb'] 	= 'false';	
		$data['pageMetakey'] 	=  '' ;
		$data['pageMetadesc'] =  '' ;
		$data['tweetimg'] = '';
		$data['gplusimg'] = '';
		$data['v_url'] = ''; 
		$data['urls'] = '';
        $data['fbimg'] = '';
        $data['links']= $option->get_options('links');
		$data['filename'] 	=  '';
		foreach ($data['rowData'] as $key => $value) {
			$u = url('blogs/list/');
			$data['urls'] = (!empty($u))?$u:'';
			//$img = asset('uploads/blogs/'.$value->image);
			$img = asset('uploads/blogs/'.'no-image.png');
			$data['fbimg'] = (!empty($img))?$img:'';
			$data['tweetimg'] = (!empty($img))?$img:'';
		}

		$data['clouds']		= self::cloudtags();
		$data['latestposts']		= $model::latestposts();
		$data['conpost'] = json_decode(file_get_contents(base_path().'/resources/views/core/posts/config.json'),true);
		$page = 'layouts.'.CNF_THEME.'.index';
		$data['pages'] = 'blogs.list';
		


		return view($page,$data);			
	

	}	



		function getUpdate(Request $request, $id = null)
		{
			
			if(\Auth::check() || \Session::has('agent_id') ){				

				$row = $this->model->find($id);
				if($row)
				{
					$this->data['row'] =  $row;
				} else {
					$this->data['row'] = $this->model->getColumnTable('tb_blogs'); 
				}
				$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

				$this->data['id'] = $id;

				// check the user is agent 
				// if(\Session::has('agent_id')){
				// 	$sTemplate = 'fullpage';

				// 	if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$sTemplate.'.blade.php') && $sTemplate !='')
				// 	{
				// 		$page_template = 'layouts.'.CNF_THEME.'.template.'.$sTemplate;
				// 	} else {
				// 		$page_template = 'layouts.'.CNF_THEME.'.template.page';
				// 	}		

				// 	$this->data['pageMetakey'] 	=   '' ;
				// 	$this->data['pageMetadesc'] =  '' ;
				// 	$this->data['pages'] = $page_template;


				// 	$this->data['content'] = view('blogs.public.form',$this->data);
				// 	$page = 'layouts.'.CNF_THEME.'.index';

				// 	// var_dump($page_template); exit;
				// 	return view($page,$this->data);
				// }

				$this->data['categories'] = \SiteHelpers::getCategories();

				return view('blogs.form',$this->data);

			}else{
				// show acess denied page
			}
		}	

		public function getShow( Request $request, $id = null)
		{

			if($this->access['is_detail'] ==0) 
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
			
			$row = $this->model->getRow($id);
			if($row)
			{
				$this->data['row'] =  $row;
				$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
				$this->data['id'] = $id;
				$this->data['access']		= $this->access;
				$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
				$this->data['prevnext'] = $this->model->prevNext($id);
				return view('blogs.view',$this->data);
			} else {
				return Redirect::to('blogs')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
			}
		}	

		function postSave( Request $request)
		{
			$rules = $this->validateForm();
			$rules=array(
			'note'=>'required','title'=>'required','categories'=>'required',
			'labels'=>'required','status'=>'required');
			// echo "<pre>"; print_r($this->info);exit;
			$validator = Validator::make($request->all(), $rules);	
			if ($validator->passes()) {
				$data = $this->validatePost('tb_blogs');

				// check the categories posted or not
				$aCategories = $request->input('categories');
				
				if(!empty($aCategories)){
					$sCategories = implode(',', $aCategories);
					$data['categories'] = $sCategories;
				}
				$data['labels'] = $request->input('labels');
				$data['status'] = $request->input('status');
				$data['created'] = date('Y-m-d H:i:s');

// echo "<pre>";print_r($data);exit;
				// inser ownerid
				$data['userid'] = \Session::get('uid');

				$id = $this->model->insertRow($data , $request->input('id'));
				
				if(!is_null($request->input('apply')))
				{
					$return = 'blogs/update/'.$id.'?return='.self::returnUrl();
				} else {
					$return = 'blogs?return='.self::returnUrl();
				}

					// Insert logs into database
				if($request->input('id') =='')
				{
					\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
				} else {
					\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
				}

				return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
				
			} else {

				return Redirect::to('blogs/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
				->withErrors($validator)->withInput();
			}	
			
		}	

		public function postDelete( Request $request)
		{
			
			if($this->access['is_remove'] ==0) 
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
			if(count($request->input('ids')) >=1)
			{
				$this->model->destroy($request->input('ids'));
				
				\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
				return Redirect::to('blogs?return='.self::returnUrl())
				->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
				
			} else {
				return Redirect::to('blogs?return='.self::returnUrl())
				->with('messagetext','No Item Deleted')->with('msgstatus','error');				
			}

		}	

	public static function splitLabels($value='')
	{
		$value = explode(',',$value);
		$vals = '';
		foreach($value as $val)
		{
			$vals .= '<a href="'.url('post/label/'.trim($val)).'" class="btn btn-xs btn-default"> '.trim($val).' </a> ';
		}
		return $vals;
	}

	public static function cloudtags()
	{
		$tags = array();	
		$keywords = array();
		$word = '';
		$data = \DB::table('tb_blogs')->get();
		foreach($data as $row)
		{
			$clouds = explode(',',$row->labels);
			foreach($clouds as $cld)
			{
				$cld = strtolower($cld);
				if (isset($tags[$cld]))
				{
					$tags[$cld] += 1;
				} else {
					$tags[$cld] = 1;
				}
				//$tags[$cld] = trim($cld);
			}
		}

		ksort($tags);
		foreach($tags as $tag=>$size)
		{
			//$size += 12;
			$word .= "<a href='".url('post/label/'.trim($tag))."'><span class='cloudtags' ><i class='fa fa-tag'></i> ".ucwords($tag)." (".$size.") </span></a> ";
		}

		return $word;
	}

	public static function latestpost()
	{

	}

		public static function display( )
		{
			$mode  = isset($_GET['view']) ? 'view' : 'default' ;
			$model  = new Blogs();
			$info = $model::makeInfo('blogs');

			$data = array(
				'pageTitle'	=> 	$info['title'],
				'pageNote'	=>  $info['note']
				
			);

			if($mode == 'view')
			{
				$id = $_GET['view'];
				$row = $model::getRow($id);
				if($row)
				{
					$data['row'] =  $row;
					$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
					$data['id'] = $id;
					return view('blogs.public.view',$data);
				} 

			} else {

				$page = isset($_GET['page']) ? $_GET['page'] : 1;
				$params = array(
					'page'		=> $page ,
					'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
					'sort'		=> 'id' ,
					'order'		=> 'desc',
					'params'	=> '',
					'global'	=> 1 
				);

				$result = $model::getRows( $params );
				$data['tableGrid'] 	= $info['config']['grid'];
				$data['rowData'] 	= $result['rows'];	

				$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
				$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
				$pagination->setPath('');
				$data['i']			= ($page * $params['limit'])- $params['limit']; 
				$data['pagination'] = $pagination;
				return view('blogs.public.index',$data);			
			}


		}


	public function postComment( Request $request)
	{
		$url_id =urlencode( base64_encode($request->input('pageID')));
		// check there is comments
		if(empty($request->input('comments')))

			
		  return Redirect::to('blogs/article/'.$request->input('pagetitle').'/'.$url_id.'/'.$request->input('alias'))
        		->with('messagetext',\SiteHelpers::alert('error','The following errors occurred'))->with('msgstatus','error');	

		if(\Session::get('gid')){
			$iUserId    = \Session::get('gid');
			$sUserMode  = 'user';
		}
		elseif(\Session::get('model_id')){
		 	$iUserId = \Session::get('model_id');
		 	$sUserMode  = 'model';
		}
		elseif(\Session::get('agent_id')){
		 	$iUserId = \Session::get('agent_id');		 		
		 	$sUserMode  = 'agent';
		}

		$rules = array();
		$validator = Validator::make($request->all(), $rules);	
		// echo "<pre>"; print_r($validator->passes()); exit;
		if ($validator->passes()) {
			$data = array(
					'userID'		=> $iUserId,
					'posted'		=> date('Y-m-d H:i:s') ,
					'type'          => 'blogs',
					'rating'      	=> $request->input('rating'),
					'status'        => 'active',
					'userType'      => $sUserMode,
					'comments'		=> $request->input('comments'),
					'pageID'		=> $request->input('pageID')
				);
			// echo "<pre>"; print_r($data); exit;	
			\DB::table('tb_comments')->insert($data);
			return Redirect::to('blogs/article/'.$request->input('pagetitle').'/'.$url_id.'/'.$request->input('alias'))
        		->with('messagetext', 'Thank You , Your comment has been sent !')->with('msgstatus','success');
		} else {
			return Redirect::to('blogs/article/'.$request->input('pagetitle').'/'.$url_id.'/'.$request->input('alias'))
        		->with('messagetext','The following errors occurred')->with('msgstatus','error');	
		}
	}

	public function getRemove( Request $request, $pageID , $alias , $commentID )
	{
		if($commentID !='')
		{

			\DB::table('tb_comments')->where('commentID',$commentID)->delete();
			return Redirect::to('blogs/view/'.$pageID.'/'.$alias)
        		->with('messagetext', \SiteHelpers::alert('success','Comment has been deleted !'))->with('msgstatus','success');

		} else {

			return Redirect::to('blogs/view/'.$pageID.'/'.$alias)
        		->with('messagetext', \SiteHelpers::alert('error','Failed to remove comment !'))->with('msgstatus','error');
		}
	}


		function postSavepublic( Request $request)
		{
			
			$rules = $this->validateForm();
			$validator = Validator::make($request->all(), $rules);	
			if ($validator->passes()) {
				$data = $this->validatePost('tb_blogs');		
				$this->model->insertRow($data , $request->input('id'));
				return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
			} else {

				return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
				->withErrors($validator)->withInput();

			}	
			
		}	





	}