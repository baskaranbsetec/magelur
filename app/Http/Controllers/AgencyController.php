<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Agency;
use App\Models\Message;
use App\Models\Favorite;
use App\Models\Modele;
use Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Mail;
use App\Models\Sximo;


class AgencyController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'agency';
	static $per_page	= '10';
	protected $iLoggedId            = false;

	public function __construct()
	{

		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Agency();
		$this->message = new Message();
		$this->modele       = new Modele();
		$this->Favorite       = new Favorite();
		$this->option = new Sximo();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'agency',
			'return'	=> self::returnUrl()

		);

		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

			$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
			\App::setLocale($lang);
		}  

		$this->iLoggedId  = \Session::get('logged_id'); 

	}
	function getModelid($id) {
		return $this->modele->where('id',$id)->where('status','1')->where('approved_status','1')->get();
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
		->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');

		if($sort=='status' && $order=='desc')
            $order='asc';
        else if($sort=='status' && $order=='asc')
            $order='desc';
// End Filter sort and order for query 
// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 


		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
// Get Query 
		$results = $this->model->getRows( $params );		

// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('agency');
		$rows = $request->input('rows') ? $request->input('rows') : 10 ;

		$query = \DB::table('tb_agency');

		if($request->get('username'))
		{
			$query->where('username','like','%'.$request->get('username').'%')->orderBy($sort,$order);

		}
		if($request->get('email'))
		{
			$query->where('email','like','%'.$request->get('email').'%')->orderBy($sort,$order);
		}
		if($request->get('agency_name'))
		{
			$query->where('agency_name','like','%'.$request->get('agency_name').'%')->orderBy($sort,$order);
		}
		if($request->get('mobile'))
		{
			$query->where('mobile','like','%'.$request->get('mobile').'%')->orderBy($sort,$order);
		}
		if($request->get('approved_date'))
		{
			$date= date('Y-m-d H',strtotime($request->get('approved_date')));
			$query->where('approved_date','like','%'.$date.'%')->orderBy($sort,$order);
		}

		$query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

		$this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);  		
		$this->data['rowData']		= $results['rows'];
// Build Pagination 
		$this->data['pagination']	= $pagination;
// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];

		$this->data['username'] = 	(!empty($request->get('username')))?$request->get('username'):'';
		$this->data['email'] = (!empty($request->get('email')))?$request->get('email'):'';	
		$this->data['agency_name'] = (!empty($request->get('agency_name')))?$request->get('agency_name'):'';	
		$this->data['mobile'] = (!empty($request->get('mobile')))?$request->get('mobile'):'';	
		$this->data['approved_date'] = (!empty($request->get('approved_date')))?$request->get('approved_date'):'';	
		$this->data['status'] = $request->get('status');	

// Group users permission
		$this->data['access']		= $this->access;
// Detail from master if any

// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
// Render into template
		return view('agency.index',$this->data);

	}	



	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
				return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
				return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_agency'); 
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

		$this->data['id'] = $id;

		return view('agency.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
		->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);

		$row->our_categories =\SiteHelpers::getcolumnvalue('our_categories',$row->our_categories);
		$row->categories_needed =\SiteHelpers::getcolumnvalue('model_categories',$row->categories_needed);
		$row->experience_needed =\SiteHelpers::getcolumnvalue('experience_needed',$row->experience_needed);
		$row->our_industries =\SiteHelpers::getcolumnvalue('our_industries',$row->our_industries);
		if (is_numeric($row->country)) {
			$row->country = \SiteHelpers::getName('country',$row->country);
		}
		else{
			$row->country =$row->country;
		}

// echo "<pre>";print_r($row->our_industries );exit;
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			return view('agency.view',$this->data);
		} else {
			return Redirect::to('agency')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request)
	{

//$rules = $this->validateForm();
// $rules['contact_number'] = 'required|numeric';
// $rules['mobile'] = 'required|numeric';
		$rules=array(
// 'mobile' => 'required|numeric|min:11',
// 'contact_number' => 'required|numeric|min:11',
// 'profile_photo' => 'required',
// 'agency_banner' => 'required',

		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$data = $this->validatePost('tb_agency');
			if(Input::get('status') == '1' || Input::get('verification_status') == '1'){
				$data['approved_date'] = date("Y-m-d H:i:s");
			}
			if(Input::get('created_date') == ''){
				$data['created_date'] = date("Y-m-d H:i:s");
			}
			if($request->hasFile('profile_cover')) {
				$files=$request->file('profile_cover');
				// $name=$file->getClientOriginalExtension();
				$name=date('His').$files->getClientOriginalName();
				$files->move('uploads/agency_profile/cover',$name);
				$data['profile_cover'] =$name;

			}
			else{
				$name = '';
			}

			$data['language'] = $request->input('language');
			$data['years_in_business'] = $request->input('years_in_business');
			$data['experience'] = $request->input('experience');
			$data['gender']=$request->input('gender');
			if($request->get('categories_needed')){
				$categories_needed = implode(",", $request->get('categories_needed'));
				$data['categories_needed'] =$categories_needed;
			}
			if($request->get('our_categories')){
				$our_categories = implode(",", $request->get('our_categories'));
				$data['our_categories'] =$our_categories;
//$data['experience_needed'] = $request->input('experience_needed');
			}
			if($request->get('genders_needed')){
				$genders_needed = implode(",", $request->get('genders_needed'));
				$data['genders_needed'] =$genders_needed;
			}
//$data['our_industries'] =$request->input('our_industries');
			$data['traveling'] =$request->input('traveling');
			if($request->get('experience_needed')){

				$experience_needed=implode(',', $request->input('experience_needed'));
				$data['experience_needed']=$experience_needed;
			}
			if($request->get('our_industries')){

				$our_industries=implode(',', $request->input('our_industries'));
				$data['our_industries']=$our_industries;
			}
			if($request->input('password') == ''){
				$data['password']=str_random(5);}
			else{
			$data['password'] = $request->input('password');
			}
			$data['DOB'] = $request->input('DOB');
			$data['mail']= $request->input('mail');
			$data['username']= $request->input('username');
// print_r($data['password']);exit;
			$data['last_updated'] = date("Y-m-d H:i:s");
			$data['business_description'] = strip_tags($request->input('business_description'));
			$data['agency_description'] = strip_tags($request->input('agency_description'));
			$data['business_biography'] = strip_tags($request->input('business_biography'));
			$data['business_biography'] = strip_tags($request->input('business_biography'));
			$data['looking_for_desc'] = strip_tags($request->input('looking_for_desc'));
			
			$id = $this->model->insertRow($data , $request->input('id'));
			if(CNF_ACTIVATION =='manual' || CNF_ACTIVATION =='auto'){
			if($data['status']==1 && $data['verification_status']==1 && $data['mail']=='not_send' ){
				$data['links'] = $this->option->get_options('links');
				$data['username']= $request->input('username');
				Mail::send('user.emails.registration',$data ,function($message) use ($data){
					$message->from(CNF_EMAIL);
					$message->to($data['email'])->subject('Registration mail');
				});		
				\DB::table('tb_agency')->where('id', $id)->update(['mail' => 'send']);
			
			}
		}

			if(!is_null($request->input('apply')))
			{
				$return = 'agency/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'agency?return='.self::returnUrl();
			}

// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

		} else {

			return Redirect::to('agency/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	

	}	

	public function postDelete( Request $request)
	{


		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
		->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));


			$id 	= $request->input('ids');
			if(count($id)>0)
			{
			foreach ($id as $key => $value)
			{
				$datas['status']   = 'deleted';
				\DB::table('tb_transactions')
				->where('user_id','=',$value )
				->where('user_type','=','agent' )
				->update($datas);
			}
		}
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
// redirect
			return Redirect::to('agency?return='.self::returnUrl())
			->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 

		} else {
			return Redirect::to('agency?return='.self::returnUrl())
			->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Agency();
		$info = $model::makeInfo('agency');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']

		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('agency.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('agency.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_agency');		
			$this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	

	}	




	public function  getAgentRegister()
	{
		if(\Session::has('agent_id'))
			{
				return Redirect::to('agent-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));
			} 
			elseif(\Session::has('model_id'))
				{
					return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));
				} 
				else {
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];

						$this->data['pageTitle'] 	= 'Agent Registration';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            			$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'agent_register';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}
						$this->data['models'] = $this->modele->where('status','1')->where('approved_status','1')->where('publishes','1')->orderBy('id','desc')->get();

						// $this->data['pages'] = $page_template;
//$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();
						$this->data['country'] = \DB::table('tb_country')->get();
						$this->data['pages'] ='agency.agent_register';
						// $this->data['content'] = view('agency.agent_register',$this->data);
						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
			}

			function postAgentSave( Request $request)
			{

				$rules = $this->validateForm();

				$validator = Validator::make($request->all(), $rules);	
				if ($validator->passes()) {
					$data = $this->validatePost('tb_agency');
					$data['created_date'] = date("Y-m-d H:i:s");
					$data['last_updated'] = date("Y-m-d H:i:s");
					$data['DOB'] = $request->input('DOB');
					if(CNF_ACTIVATION == 'confirmation'){
						$data['password']= ' ';
					}
					else{
						$data['password']=str_random(5);
					}
					
					$data['gender']=$request->input('gender');

					$id = $this->model->insertRow($data , $request->input('id'));
					$plan_id = '1';
					$planlimits=\SiteHelpers::getplandetailsbyId($plan_id);

					$userinfo=\SiteHelpers::getUserdetail($id);
					if($planlimits->event_limit!="unlimited")
						$eventlimit=(int)$userinfo->eventplan + (int)$planlimits->event_limit;
					else
						$eventlimit="unlimited";

					if($planlimits->video_limit!="unlimited")
						$videolimit=(int)$userinfo->videoplan + (int)$planlimits->video_limit;
					else          
						$videolimit="unlimited";

					if($planlimits->image_limit!="unlimited")
						$imagelimit=(int)$userinfo->photoplan + (int)$planlimits->image_limit;
					else
						$imagelimit="unlimited";

					\DB::table('tb_agency')->where('id', $id)->update(['plan_id' => $plan_id,'eventplan'=>$eventlimit,'videoplan'=>$videolimit,'photoplan'=>$imagelimit]);

//Update status in Tell a friend 
					$encoded_id= $request->input('decoded_id');
					$decoded_id= base64_decode($encoded_id);

					$updateplan = \SiteHelpers::updateBasicMembership('agent',$id);
// if automatic activation is set true from admin
					if(CNF_ACTIVATION == 'auto')
					{

						$feed = array();
						$feed['username'] = $request->input('username');
						$feed['email'] = $request->input('email');
						// $feed['password'] = '******';
						$feed['password']= $data['password'];
						
						$feed['links']=$this->option->get_options('links');
						// print_r($feed['links']['fb']);exit;
						$status = Mail::send('user.emails.registration_auto_activation',$feed ,function($message) use ($feed){
							$message->from(CNF_EMAIL);
							$message->to($feed['email'])->subject('Registration Confirmation');

						});


						$updatestatus = \DB::table('tb_agency')->where('id','=',$id)->update(['approved_date'=>$data['created_date'],'status'=>'1','verification_status'=>'1']);
					
					}elseif(CNF_ACTIVATION == 'confirmation'){

						$feed = array();
						$feed['username'] = $request->input('username');
						$feed['email'] = $request->input('email');
						// $feed['password'] = '******';
						$feed['password']=str_random(5);
						$feed['activationLink']='/agentConfirmAccount/'.$feed['email'].'/'.$feed['password'];
						$feed['links']=$this->option->get_options('links');
						// print_r($feed['links']['fb']);exit;
						$status = Mail::send('user.emails.registration_activation',$feed ,function($message) use ($feed){
							$message->from(CNF_EMAIL);
							$message->to($feed['email'])->subject('Registration Confirmation');

						});
						$updatestatus = \DB::table('tb_agency')->where('id','=',$id)->update(['password' => $feed['password'],'mail' => 'send','approved_date'=>$data['created_date'],'status'=>'0','verification_status'=>'0']);
						
					
					}
//end


					$user = \DB::table('tb_tell_a_friend')->where('id', '=',$decoded_id )
					->where('status', '=', 'logged')
					->exists();
					$users = \DB::table('tb_tell_a_friend')->where('id', '=',$decoded_id )
					->where('status', '=', 'requested')
					->exists();

					if($user==1){
						return Redirect::to('login')->with('messagetext','You are not allowed to Login with this URL')->with('msgstatus','error');
					}
					else{
						\DB::table('tb_tell_a_friend')
						->where('id', $decoded_id )
						->update(array(
							'status' => 'logged',
							'friend_id' => $id,
							'user_type' => 'agent',
							'friend_name' => $request->input('username'),
							'registered' => date("Y-m-d H:i:s"),
						));	
					}



					if($request->input('decoded_id')!=''){
						$return = "login?tellfriend=".$encoded_id;
					}
					else{
						$return = "login";
					}

					return Redirect::to($return)->with('messageetext','Thanks for registering! . You will receive a Mail after we will active your Account.')->with('msgstatus','success');

				} else {
// return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
// ->withErrors($validator)->withInput();
				}	

			}

			public function agentConfirmAccount($email,$pwd){
				$user = \DB::table('tb_agency')->where('email', $email )->where('password',$pwd)->get();
				if (count($user) >= 1)
				{
					\DB::table('tb_agency')->where('email',  $email )->where('password',$pwd)->update(array('status' => '1','verification_status'=>'1'));
					
					return Redirect::to('login')->with('message_reset', 'Your account is active now!')->with('msgstatus','success');
					
				} else {
					return Redirect::to('agent/create')->with('message_reset', 'Invalid Confirmation!')->with('msgstatus','error');
				}
			}

			public function postCheckuniqueemailagent(Request $request)
			{
				$email=\DB::table('tb_agency')->where('email',$request->input('email'))->get();
				if($email){
					return '"This email id is already taken."';		
				}	
				else{
					return 'true';
				}
			}
			public function postCheckuniquealias(Request $request)
			{
				$result = explode(',',$_POST['alias']);
				$email=\DB::table('tb_pages')->where('alias',$_POST["alias"])->get();
				if(count($email)>0)
				{
					foreach ($email as $key => $value) {
						if(isset($value->pageID) and $value->pageID == $_POST["username"])
						{
							return 'true';
						}	
						else
						{
							return '"This alias is already used."';	
						}
					}
				}
				else
				{
					return 'true';
				}

				
			}
			public function postCheckuniquetitle(Request $request)
			{
				$result = explode(',',$_POST['title']);
				$email=\DB::table('tb_pages')->where('title',$_POST["title"])->get();
				if(count($email)>0)
				{
					foreach ($email as $key => $value) {
						if(isset($value->pageID) and $value->pageID == $_POST["username"])
						{
							return 'true';
						}	
						else
						{
							return '"This title is already used."';	
						}
					}
				}
				else
				{
					return 'true';
				}

				
			}


			public function postCheckuniqueNameagent(Request $request)
			{
				$query=\DB::select("
			SELECT * FROM tb_models,tb_agency WHERE tb_models.firstname = '".$request->input('recipient1')."' OR tb_agency.username = '".$request->input('recipient1')."'");
				if($query){
					return 'true';	
				}	
				else{
					// return 'true';
					return '"Name does not match"';
				}

				// if(\Session::get('user_type')=='agent'){
				// $email=\DB::table('tb_models')->where('username',$request->input('recipient1'))
				// 		->where('status','1')->get();
				// }
				// else{
				// 	$email=\DB::table('tb_agency')->where('username',$request->input('recipient1'))
				// 		->where('status','1')->get();
				// }
				
			}

// check unique mail for Admin
			public function postCheckuniqueemailagentadmin(Request $request)
			{
				$result = explode(',',$_POST['email']);
				$email=\DB::table('tb_agency')->where('email',$result[0])->get();
				if(count($email)>0)
				{
					foreach ($email as $key => $value) {
						if(isset($value->id) and $value->id==$result[1])
						{
							return 'true';
						}	
						else
						{
							return '"This email id is already taken."';	
						}
					}
				}
				else
				{
					return 'true';
				}
			}
//end

			public function getFavouriteVideos($id){
				$id=\SiteHelpers::getSecurityurlID($id);
				// print_r($id);exit;
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$user_id=$id; 
					$this->data['user_id']	= $user_id;
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];
						$this->data['pageTitle'] 	= 'Favorite Videos';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            			$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'favourite-listing';				
//$page_template='';
						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}
						$user_type=\Session::get('user_type');
						$this->data['user_type']=$user_type;

						if($user_type=='agent'){
							$this->data['agents'] = $this->getAgentid($id);
							$this->data['images'] = $this->getAgentid($id);
						}else{			
							$this->data['models'] = $this->getModelid($id);
							$this->data['images'] = $this->getModelid($id);
						}
						// $this->data['pages'] = $page_template;

						$Favorite = $this->Favorite->leftjoin('videos', 'videos.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','video')->where('tb_favorites.user_type',$user_type)->get();
						$this->data['models_fav'] = $this->Favorite->leftjoin('videos', 'videos.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','video')->where('tb_favorites.user_type',$user_type)->get()->toArray();
						$this->data['count'] =count($this->Favorite->leftjoin('videos', 'videos.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','video')->where('tb_favorites.user_type',$user_type)->lists('videos.preview')->toArray());
						$count =count($this->Favorite->leftjoin('videos', 'videos.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','video')->where('tb_favorites.user_type',$user_type)->lists('videos.preview')->toArray());

						$items = $Favorite;
						$page = Input::get('page', 1);
						$perPage = 6;


						$this->data['models_fav']=$Favorite->forPage($page, $perPage);

						$pagination = new Paginator($this->data['models_fav'],$count,$perPage,null, [
							'path' => Paginator::resolveCurrentPath()
						]);

						$this->data['pagination']	= $pagination;
						$this->data['pages'] ='favourite.videos';
						// $this->data['content'] = view('favourite.videos',$data);

						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
				}
			}	



			public function getFavouritePhotos($id){
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 
				elseif($this->iLoggedId==$id)
				{
					$user_id=$id;
					$this->data['user_id']	= $user_id;
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];
						$this->data['pageTitle'] 	= 'Favorite Photos';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'favourite-listing-photos';				
//$page_template='';
						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}
						$this->data['user_type']='agent';
						$this->data['agents'] = $this->getAgentid($id);
						$this->data['images'] = $this->getAgentid($id);
						$this->data['pages'] = $page_template;

						$user_type=\Session::get('user_type');
						$this->data['user_type']=$user_type;

						if($user_type=='agent'){
							$this->data['agents'] = $this->getAgentid($id);
							$this->data['images'] = $this->getAgentid($id);
						}else{			
							$this->data['models'] = $this->getModelid($id);
							$this->data['images'] = $this->getModelid($id);
						}
						// $this->data['pages'] = $page_template;

						$Favorite = $this->Favorite->leftjoin('tb_images', 'tb_images.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','image')->where('tb_favorites.user_type',$user_type)->get();
						$this->data['models_fav'] = $this->Favorite->leftjoin('tb_images', 'tb_images.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','image')->where('tb_favorites.user_type',$user_type)->get()->toArray();
						$this->data['count'] =count($this->Favorite->leftjoin('tb_images', 'tb_images.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','image')->where('tb_favorites.user_type',$user_type)->lists('tb_images.MediaUri')->toArray());
						$count =count($this->Favorite->leftjoin('tb_images', 'tb_images.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','image')->where('tb_favorites.user_type',$user_type)->lists('tb_images.MediaUri')->toArray());

						$items = $Favorite;
						$page = Input::get('page', 1);
						$perPage = 6;


						$this->data['models_fav']=$Favorite->forPage($page, $perPage);

						$pagination = new Paginator($this->data['models_fav'],$count,$perPage,null, [
							'path' => Paginator::resolveCurrentPath()
						]);

						$this->data['pagination']	= $pagination;
$this->data['pages'] ='favourite.photos';
						// $this->data['content'] = view('favourite.photos',$data);

						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
				}
			}	

			public function getFavouriteModels($id){
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$user_id=$id; 
					$this->data['user_id']	= $user_id;	
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];
						$this->data['pageTitle'] 	= 'Favorite Models';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'favourite-listing';				
//$page_template='';
						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}
						$user_type=\Session::get('user_type');
						$this->data['user_type']=$user_type;
						if($user_type=='agent'){
							$this->data['agents'] = $this->getAgentid($id);
							$this->data['images'] = $this->getAgentid($id);
						}else{			
							$this->data['models'] = $this->getModelid($id);
							$this->data['images'] = $this->getModelid($id);
						}
						// $this->data['pages'] = $page_template;

						$Favorite = $this->Favorite->leftjoin('tb_models', 'tb_models.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','model')->where('tb_favorites.user_type',$user_type)->get();
						$this->data['models_fav'] = $this->Favorite->leftjoin('tb_models', 'tb_models.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','model')->where('tb_favorites.user_type',$user_type)->get()->toArray();
						$this->data['count'] =count($this->Favorite->leftjoin('tb_models', 'tb_models.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','model')->where('tb_favorites.user_type',$user_type)->lists('tb_models.profile_photo')->toArray());
						$count =count($this->Favorite->leftjoin('tb_models', 'tb_models.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','model')->where('tb_favorites.user_type',$user_type)->lists('tb_models.profile_photo')->toArray());

						$items = $Favorite;
						$page = Input::get('page', 1);
						$perPage = 6;


						$this->data['models_fav']=$Favorite->forPage($page, $perPage);

						$pagination = new Paginator($this->data['models_fav'],$count,$perPage,null, [
							'path' => Paginator::resolveCurrentPath()
						]);

						$this->data['pagination']	= $pagination;
						$this->data['pages'] ='favourite.models';
						// $this->data['content'] = view('favourite.models',$data);

						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
				}
			}	


			public function getFavouriteAgents($id){
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$user_id=$id; 
					$this->data['user_id']	= $user_id;
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];
						$this->data['pageTitle'] 	= 'Favorite Agents';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'favourite-listing-agents';				
//$page_template='';
						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}
						$user_type=\Session::get('user_type');
						$this->data['user_type']=$user_type;
						if($user_type=='agent'){
							$this->data['agents'] = $this->getAgentid($id);
							$this->data['images'] = $this->getAgentid($id);
						}else{			
							$this->data['models'] = $this->getModelid($id);
							$this->data['images'] = $this->getModelid($id);
						}
						// $this->data['pages'] = $page_template;

						$Favorite = $this->Favorite->leftjoin('tb_agency', 'tb_agency.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','agent')->where('tb_favorites.user_type',$user_type)->get();
						$this->data['models_fav'] = $this->Favorite->leftjoin('tb_agency', 'tb_agency.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','agent')->where('tb_favorites.user_type',$user_type)->get()->toArray();
						$this->data['count'] =count($this->Favorite->leftjoin('tb_agency', 'tb_agency.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','agent')->where('tb_favorites.user_type',$user_type)->lists('tb_agency.profile_photo')->toArray());
						$count =count($this->Favorite->leftjoin('tb_agency', 'tb_agency.id', '=', 'tb_favorites.favourite_id')->where('tb_favorites.user_id',$id)->where('tb_favorites.favourite_type','agent')->where('tb_favorites.user_type',$user_type)->lists('tb_agency.profile_photo')->toArray());

						$items = $Favorite;
						$page = Input::get('page', 1);
						$perPage = 6;


						$this->data['models_fav']=$Favorite->forPage($page, $perPage);

						$pagination = new Paginator($this->data['models_fav'],$count,$perPage,null, [
							'path' => Paginator::resolveCurrentPath()
						]);

						$this->data['pagination']	= $pagination;

						$this->data['pages'] ='favourite.agents';
						// $this->data['content'] = view('favourite.agents',$data);

						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
				}
			}	



			public function  getAgentDash($id)
			{
				$id=\SiteHelpers::getSecurityurlID($id);
				if($this->iLoggedId==$id)
				{
					$agent_id =\Session::get('agent_id');
					$uid =\Session::get('model_id');


					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];
						$type = 0;
						$label = 0;
						$page = 1;
						$params = array(
							'page'		=> $page ,
							'limit'		=>'' ,
							'sort'		=> 'id' ,
							'order'		=> 'desc',
							'params'	=> '',
							'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
						);

						$this->data['pageTitle'] 	= 'Agent Dashboard';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
                	$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'agent_dash';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}


						// $this->data['pages'] = $page_template;
$this->data['agent'] =$this->model->where('id',$id)->where('status','1')->where('verification_status','1')->first();
						$this->data['agents'] = $this->getAgentid($id);
						$this->data['images'] = $this->getAgentid($id);
						$this->data['row'] = $this->model->find($id);
						$this->data['a_mess_name'] = \SiteHelpers::getAgentMessageUsername();
						$this->data['m_mess_name'] = \SiteHelpers::getModelMessageUsername();
						$this->data["member"]="";
						if(\Session::get('agent_id')){

							$agent_id = \Session::get('agent_id');
							$getagentdetail=\SiteHelpers::getIsmember($agent_id);		
							$this->data["member"]=$getagentdetail[0]->membership; 
						}




// echo "<pre>";print_r($data['images']);exit;
						$this->data['results'] =\DB::table('tb_message')
						->where('recipient',$agent_id)
						->where('draft',0)
						->where('trashed_by_recipient',0)
						->where('deleted_by_recipient',0)
						->where('label_by_sender',NULL)
						->where('label_by_recipient',NULL)
// ->where('model_agent',1)
						->limit(3)
						->orderBy('id','desc')
						->get();
						$this->data['videos'] =\DB::table('videos')->where('user_type','agent')
						->where('user_id',$agent_id)->where('status',1)->orderBy('id', 'desc')->first();
// echo "<pre>";print_r($data['videos']);exit;
// Count Total Views
						$this->data['views'] = \DB::table('videos')->where('user_type','agent')->count();
						/*Agent list*/
						$agent_users_data = $this->message->getMessageUsersAgent();
						$usersAgentAvatar = array();
						foreach($agent_users_data as $user_data){
							$usersAgentNameList[$user_data->id] = $user_data->username;
							$usersAgentAvatar[$user_data->id] = $user_data->profile_photo;
						}	
						$this->data['usersAgentAvatar'] = $usersAgentAvatar;
						$this->data['usersAgentNameList'] = $usersAgentNameList;

						$users_data =  $this->message->getMessageUsers( $uid );
						$users = array(''=>'Select an User');
						$username = array();
						$usersNameList = array();
						$usersEmailList = array();
						$usersAvatar = array();
						foreach($users_data as $user_data){
							$users[$user_data->id] = $user_data->username;
							$usersNameList[$user_data->id] = $user_data->username;
							$username[$user_data->id] = $user_data->username;
							$usersEmailList[$user_data->id] = $user_data->email;
							$usersIdList[$user_data->id] = $user_data->id;
							$usersAvatar[$user_data->id] = $user_data->profile_photo;
						}
						$this->data['usersNameList'] = $usersNameList;
						$this->data['usersAvatar'] = $usersAvatar;
						$this->data['gallery'] = \DB::table('tb_images')->where('status','Active')->where('user_type','agent')->where('OwnerId',$agent_id)->orderBy('id', 'desc')->paginate(8);
// echo '<pre>'; print_r(($this->data['gallery']));exit;
						$this->data['agent_id'] =$agent_id;
						$this->data['user_type']='agent';

						$this->data['m_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',2)->where('recipient_type','model')->orderBy('id', 'desc')->paginate(3);
						$this->data['a_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',2)->where('recipient_type','agent')->orderBy('id', 'desc')->paginate(3);
// print_r($this->data['m_messages']);exit;
						/*Membership start*/
						$this->data['membershipplan'] = \DB::table('tb_agency')
						->select('tb_membership_plan.plan_name','tb_membership_plan.plan_amount')
						->join('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_agency.plan_id')
						->where('tb_agency.id',$agent_id)
						->get();
						$umembership = \SiteHelpers::getMembershipofuser();
						if($umembership){
							$odr_details = json_decode($umembership->order_details,true);
							$subscriptionReference = $odr_details['subscriptionReference'];
							$customerReference = $odr_details['customerReference'];
							$gateway = \Omnipay::gateway('stripe');
							$options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
							$stripe = array();
							foreach($options as $results)
							{
								$stripe[$results->option_key]   = $results->options;
							}
							$gateway->setApiKey($stripe['secret_key']);

							$check_subscription_status 	= $gateway->fetchSubscription([
								"subscriptionReference" => $subscriptionReference,
								"customerReference" 	=> $customerReference
							])->send();
							$check_subscription_status_data = $check_subscription_status->getdata();

							if(isset($check_subscription_status_data['current_period_end']))
							{
								$current_period_end = date( 'm-d-Y', $check_subscription_status_data['current_period_end']);
							}else{
								$current_period_end = '-';
							}

							$this->data['membership'] = $umembership;
							$this->data['renewal_date'] = $current_period_end;
							$this->data['purchase_period'] = $umembership->purchase_period;	
						}else{

							$umembership_all_status = \SiteHelpers::getMembershipofuserallstatus();
							if($umembership_all_status){
								$this->data['membership'] = $umembership_all_status;
								$this->data['renewal_date'] = '-';
								$this->data['purchase_period'] = $umembership_all_status->purchase_period;
							}else{
								$this->data['renewal_date'] = '';
								$this->data['purchase_period'] = '';
							}

						}
						/*membership end*/
						$res = $this->getAgentid($id);
						foreach ($res as $key => $value) {
							$u = url('agent-dashboard/'.$value->id);
							$img = \SiteHelpers::user_profiles_share($value->id,'agent',$value->profile_photo,'/uploads/agency_profile/');
						}

						$this->data['urls'] = (!empty($u))?$u:'';
						$this->data['fbimg'] = (!empty($img))?$img:'';
						$this->data['tweetimg'] = (!empty($img))?$img:'';
						$this->data['gplusimg'] = (!empty($img))?$img:'';
// echo "<pre>";print_r($data['a_messages']);exit;
						$this->data['pages'] ='agency.agent_dash';
						// $this->data['content'] = view('agency.agent_dash',$data);
						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');

				}

			}
			public function  getAgentprofile($name,$id)
			{
	$id=\SiteHelpers::getSecurityurlNameID($id);
// $aUser  = \SiteHelpers::user_avatars($id,'agent');
//  echo "<pre>"; print_r($aUser);exit;
				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Profile';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'agent_profile';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					// $this->data['pages'] = $page_template;
					$this->data['pages'] ='agency.agent_profile';
					$this->data['agents'] = $this->model->where('id',$id)->where('status','1')->where('verification_status','1')->get();
					$this->data['images'] = $this->getAgentid($id);
					$row = $this->model->getRow($id);

					if (is_numeric($row->country)) {
						$row->country = \SiteHelpers::getName('country',$row->country);
					}
					else{
						$row->country =$row->country;
					}
					$row->genders_needed  = str_replace(",", "<span class='gend_need'>", $row->genders_needed);


					$row->our_categories =\SiteHelpers::getcolumnvalue('our_categories',$row->our_categories);
					$row->categories_needed =\SiteHelpers::getcolumnvalue('model_categories',$row->categories_needed);
					$row->experience_needed =\SiteHelpers::getcolumnvalue('experience_needed',$row->experience_needed);
					$row->our_industries =\SiteHelpers::getcolumnvalue('our_industries',$row->our_industries);


					if($row)
					{
						$this->data['row'] =  $row;
					}	
					$this->data['user_type']= 'agent';

					if(\SiteHelpers::checkLogged() == 1 && \Session::get('logged_id')){    
						$UserType=\Session::get('user_type');
						if($UserType=='model'){
							$userid = \Session::get('model_id');
						}
						else if($UserType=='agent'){
							$userid = \Session::get('agent_id');
						}
						else if($this->iLoggedId)
						{
						$userid =$this->iLoggedId; 
						}
						$this->data['star_rating']= \SiteHelpers::getStarCount($id,$userid,$UserType);
// get Sum ratings
						$ratedCounts =\SiteHelpers::getRatingCount($id);
						$sum=$ratedCounts->sum;
						if($sum>0)
							$total=($sum)/($ratedCounts->total_count);
						else
							$total='Not yet Rated';	

//Total counts for particular videos 
						$this->data['avg_ratings']=$total;
						$this->data['total_count']=$ratedCounts->total_count;
					}else{


//Total counts for particular videos 
						$this->data['avg_ratings']='';
						$this->data['star_rating']='';
					}

					$this->data['favourite'] = \SiteHelpers::getfavourites($row->id,'agent');
					if(\SiteHelpers::checkLogged()){
					$this->data['models'] = \DB::table('tb_models')->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orderBy('id','desc')->get();
					}
					else
					{ 
					$this->data['models'] = \DB::table('tb_models')->where('status','1')->where('approved_status','1')->where('publishes','1')->orderBy('id','desc')->get();
					}

					// $this->data['content'] = view('agency.agent_profile',$data);
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}

			}
			public function  getAgentProfileEdit(Request $request, $id)
			{
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];

						$this->data['pageTitle'] 	= 'Profile Edit';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            			$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'agent_profile_edit';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}

						// $this->data['pages'] = $page_template;
						$this->data['agents'] = $this->model->where('id',$id)->where('status','1')->where('verification_status','1')->get();
						$this->data['images'] = $this->getAgentid($id);
						$row = $this->model->find($id);
						if($row)
						{
							$this->data['row'] =  $row;
						}
						if (is_numeric($row->country)) {
							$row->country = \SiteHelpers::getName('country',$row->country);
						}
						else{
							$row->country =$row->country;
						}
						$this->data['pages'] ='agency.agent_profile_edit';

						// $this->data['content'] = view('agency.agent_profile_edit',$data);
						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
				}

			}
			public function  postAgentprofileSave(Request $request)
			{

				$rules = $this->validateForm();

				$validator = Validator::make($request->all(), $rules);	
				if ($validator->passes()) {
					$data = $this->validatePost('tb_agency');
					$data['language'] = $request->input('language');
					$data['years_in_business'] = $request->input('years_in_business');
					$data['experience'] = $request->input('experience');
					$data['business_description'] = strip_tags($request->input('business_description'));
					$data['agency_description'] = strip_tags($request->input('agency_description'));
					$data['business_biography'] = strip_tags($request->input('business_biography'));
					$data['business_biography'] = strip_tags($request->input('business_biography'));
					$data['looking_for_desc'] = strip_tags($request->input('looking_for_desc'));

					if($request->get('categories_needed')){
						$categories_needed = implode(",", $request->get('categories_needed'));
						$data['categories_needed'] =$categories_needed;
					}
					if($request->get('our_categories')){
						$our_categories = implode(",", $request->get('our_categories'));
						$data['our_categories'] =$our_categories;
//$data['experience_needed'] = $request->input('experience_needed');
					}
					if($request->get('genders_needed')){
						$genders_needed = implode(",", $request->get('genders_needed'));
						$data['genders_needed'] =$genders_needed;
					}
					$data['gender']=$request->input('gender');
//$data['our_industries'] =$request->input('our_industries');
					$data['traveling'] =$request->input('traveling');
					if($request->get('experience_needed')){

						$experience_needed=implode(',', $request->input('experience_needed'));
						$data['experience_needed']=$experience_needed;
					}
					if($request->get('our_industries')){

						$our_industries=implode(',', $request->input('our_industries'));
						$data['our_industries']=$our_industries;
					}
					$data['id']=$request->input('ids');
					$data['address']=$request->input('formatted_address');
					$data['city']=$request->input('locality');
					$data['zipcode']=$request->input('postal_code');
					$data['state']=$request->input('administrative_area_level_1');
					$data['country']=$request->input('country');
					$data['latitude']=$request->input('lat');
					$data['longitude']=$request->input('lng');
					$data['last_updated'] = date("Y-m-d H:i:s");
					$data['DOB'] = $request->input('DOB');

// echo "<pre>";print_r($_POST);exit;
					$id = $this->model->insertRow($data , $request->input('ids'));
					return Redirect::to('agent-profile-edit/'.\Session::get('url_securityID'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

// return Redirect::to('agent-profile-edit/'.$request->input('ids'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
				}


			}
			public function  getOuragency(Request $request, $id = null)
			{
				$id=\SiteHelpers::getSecurityurlID($id);

				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Our Agencies';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            		$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'ouragencies';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$rowperpage = 6;

					$this->data['agencies'] = $this->model->where('status','1')->where('verification_status','1')->limit($rowperpage)->get();				
					$photos = $this->model->where('status','1')->where('verification_status','1')->lists('profile_photo')->toArray();

					$this->data['count'] = count($photos);

					// $this->data['content'] = view('agency.ouragencies',$data);
					$this->data['pages'] ='agency.ouragencies';

					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}


			}
			public function postAgentLoadDataAjax(Request $request)
			{
				$row = $_POST['row'];
				$rowperpage = 6;

// selecting posts
				$query =  \DB::table('tb_agency')->where('status',1)->where('verification_status',1)
				->offset($row)
				->limit($rowperpage)
				->get();

				$html = '';

				foreach ($query as  $value) {
# code...			
					$username_str=str_replace(' ','',$value->username);
					$url_id =urlencode( base64_encode($username_str.'/'.$value->id));
					$url   = \SiteHelpers::user_agent_profile_sizes($value->id,'_medium','png'); 
					$href  = url('agent-profile/'.$username_str.'/'.$url_id);
					$html .= '<a href='.$href.'>';
					$html .= '<div class="model_item agent_item" id="'.$value->id.'">';
					$html .= '<img src="'.$url.'" >';
					$html .= ' <div class="colorbox">';
					$html .= '   <div class="inner_shades">';
					$html .= '     <h4>'.$value->agency_name.' </h4>';
					$html .= ' <p>'.$value->username.' '.\SiteHelpers::getName('country',$value->country).'</p>';
					$html .= '</div>';
					$html .=  ' </div>';
					$html .= ' </div>';
					$html .= ' </a>';

// Creating HTML structure
// $html .= '<div id="'.$value->id.'" class="post">';
// $html .= '<img src="'.$url.'" >';

// $html .= '</div>';

				}

				echo $html;
			}
			public function  getAgentmembership(Request $request, $id = null)
			{
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];

						$this->data['pageTitle'] 	= 'Membership';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'agent_membership';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}

						// $this->data['pages'] = $page_template;
						$this->data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();

						$this->data['images'] = $this->getAgentid($id);


						$this->data['memberships'] = \DB::table('tb_membership_plan')->get();
						$this->data['user_type'] = 'agent';
						$user_id = \Session::get('agent_id');
						$user_type =	\Session::get('user_type');

						$this->data['membershipplan'] = \DB::table('tb_agency')
						->select('tb_membership_plan.plan_name','tb_membership_plan.plan_amount')
						->join('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_agency.plan_id')
						->where('tb_agency.id',$user_id)
						->get();


						$umembership = \SiteHelpers::getMembershipofuser();

						if($umembership){
							$odr_details = json_decode($umembership->order_details,true);
							$subscriptionReference = $odr_details['subscriptionReference'];
							$customerReference = $odr_details['customerReference'];


							$gateway = \Omnipay::gateway('stripe');
							$options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
							$stripe = array();
							foreach($options as $results)
							{
								$stripe[$results->option_key]   = $results->options;
							}
							$gateway->setApiKey($stripe['secret_key']);


							$check_subscription_status 	= $gateway->fetchSubscription([
								"subscriptionReference" => $subscriptionReference,
								"customerReference" 	=> $customerReference
							])->send();
							$check_subscription_status_data = $check_subscription_status->getdata();

							if(isset($check_subscription_status_data['current_period_end']))
							{
								$current_period_end = date( 'm-d-Y', $check_subscription_status_data['current_period_end']);
							}else{
								$current_period_end = '-';
							}


							$this->data['membership'] = $umembership;
							$this->data['renewal_date'] = $current_period_end;
							$this->data['purchase_period'] = $umembership->purchase_period;	

						}else{

							$umembership_all_status = \SiteHelpers::getMembershipofuserallstatus();
							if($umembership_all_status){
								$this->data['membership'] = $umembership_all_status;
								$this->data['renewal_date'] = '-';
								$this->data['purchase_period'] = $umembership_all_status->purchase_period;
							}else{
								$this->data['renewal_date'] = '';
								$this->data['purchase_period'] = '';
							}

						}
						// $this->data['content'] = view('agency.agent_membership',$data);
						$this->data['pages'] ='agency.agent_membership';
						$page = 'layouts.'.CNF_THEME.'.index';
						return view($page,$this->data);		
					}
				}
				else{
					return Redirect::to('/');
				}
			}
			public function  getFind_best(Request $request)
			{
				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Best Agent & Models';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'best_agent_model';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;

					$this->data['content'] = view('agency.best_agent_model');
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}
			}
			public function  getAgentEvents()
			{

				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Events';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'agent_events';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;
					$data['user_type']='agent';
					$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
					$this->data['content'] = view('agency.agent_events',$data);
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}

			}
			public function  getAgentEventsCreate()
			{

				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Create Events';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'events_create';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;
					$data['user_type']='agent';
					$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
					$this->data['content'] = view('agency.events_create',$data);
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}

			}
			public function  getAgentEventsDetail()
			{

				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Videos';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'agent_video';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;
					$data['user_type']='agent';
					$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
					$this->data['content'] = view('agency.agent_video',$data);
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}

			}
			public function  getAgentUserVideo($id)
			{
				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];

					$this->data['pageTitle'] 	= 'Videos';
// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['tweetimg'] = '';
					$this->data['gplusimg'] = '';
					$this->data['v_url'] = ''; 
					$this->data['urls'] = '';
					$this->data['fbimg'] = '';
           		 $this->data['links']=$this->option->get_options('links');
					$this->data['filename'] 	=  'agent_video';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;
					$this->data['images'] = $this->getAgentid($id);
					$this->data['videos'] =\DB::table('videos')->where('user_type','agent')->orderBy('id', 'desc')->paginate(8);

					$this->data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
					// $this->data['content'] = view('agency.agent_video',$data);
					$this->data['pages'] ='agency.agent_video' ;
						$page = 'layouts.'.CNF_THEME.'.index';
						return view($page,$this->data);				
				}

			}

			function getAgentid($id) {
				return $this->model->where('id',$id)->where('status','1')->where('verification_status','1')->get();
			}
			public function  postRating_ajax(Request $request)
			{
				$UserType=\Session::get('user_type');
				if($UserType=='model'){
					$userid = \Session::get('model_id');
				}
				else if($UserType=='agent'){
					$userid = \Session::get('agent_id');
				}


				$postid = $_POST['postid'];
				$rating = $_POST['rating'];
				$post_type = $_POST['post_type'];

// Check entry within table
				$count =\DB::table('tb_rating')->where('post_id',$postid)->where('user_id',$userid)->count();

				if($count == 0){
					$values = array(
						'user_id' =>$userid,
						'post_id' => $postid,
						'star_count' => $rating,
						'created_date' =>date("Y-m-d H:i:s"),
						'post_type' => $post_type, 
						'user_type' =>$UserType
					);
					\DB::table('tb_rating')->insert($values);
					$response = array(
						'status' => 'success',
						'msg'    => 'Rated Successfully',
					);
					return Response::json($response);

				}else {
					$response = array(
						'status' => 'error',
						'msg'    => 'Already rated',
					);
					return Response::json($response);

				}




			}
			public function  getAgentNetwork($id)
			{
				$id=\SiteHelpers::getSecurityurlID($id);
				if(empty($this->iLoggedId)){

					return Redirect::to('/login');
				} 	
				elseif($this->iLoggedId==$id)
				{
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];

						$this->data['pageTitle'] 	= 'Networks';
// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
						$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
						$this->data['fbimg'] = '';
            			$this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'agent_network';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}

						// $this->data['pages'] = $page_template;
						$this->data['images'] = $this->getAgentid($id);
						$this->data['videos'] =\DB::table('videos')->where('user_type','agent')->orderBy('id', 'desc')->paginate(8);
						$this->data['m_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',2)->where('recipient_type','model')->paginate(9);
						$this->data['a_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',2)->where('recipient_type','agent')->paginate(9);
						$this->data['a_mess_name'] = \SiteHelpers::getAgentMessageUsername();
						$this->data['m_mess_name'] = \SiteHelpers::getModelMessageUsername();
						// print_r($this->data['mess_name']);exit;
						$this->data['user_type']='agent';
						$this->data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
						// $this->data['content'] = view('agency.agent_network',$data);
						$this->data['pages'] ='agency.agent_network' ;
						$page = 'layouts.'.CNF_THEME.'.index';
						return view($page,$this->data);			
					} 	  	


				}
				else{
					return Redirect::to('/');
				}
			}

			/*-------------------to send mail for events/images/videos-----------*/
			public function sendMail($product)
			{
// echo $product;die;
// echo "<pre>";print_r($_REQUEST);die;
				$to = $_REQUEST['email'];
				$subject = $_REQUEST['subject'];
				$message1 = $_REQUEST['message'];
				$from = $_REQUEST['fromemail'] ;

// mail($to,$subject,$txt,$headers);
				$data['bodymessage'] = $_REQUEST['message'];
				$data['links'] = $this->option->get_options('links');

				Mail::send('user.emails.email_user',$data ,function($message) use ($to){
					$message->from($_REQUEST['fromemail']);
					$message->to($to)->subject($_REQUEST['subject']);
				});
				if($product=='events')
				{
					return Redirect::to('events/')->with('messagetext','Email Sent Successfully')->with('msgstatus','success');
				}
				elseif ($product=='images') {
					return Redirect::to('imagesnew/')->with('messagetext','Email Sent Successfully')->with('msgstatus','success');
				}
				elseif ($product=='videos') {
					return Redirect::to('videosnew/')->with('messagetext','Email Sent Successfully')->with('msgstatus','success');
				}
				elseif ($product=='agents') {
					return Redirect::to('agency/')->with('messagetext','Email Sent Successfully')->with('msgstatus','success');
				}
				elseif ($product=='models') {
					return Redirect::to('models/')->with('messagetext','Email Sent Successfully')->with('msgstatus','success');
				}
			}


		}