<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Helper\Helper;
use App\Models\Modele;
use App\Models\Message;
use App\Models\Membership;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Mail;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Sximo;



class ModelController extends Controller {

	protected $layout   = "layouts.main";
	protected $data     = array();	
	public $module      = 'model';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		
		$this->model       = new Modele();
		$this->message     = new Message();
		$this->membership  = new Membership();
		$this->option = new Sximo();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'model',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  

 $this->iLoggedId  = \Session::get('logged_id'); 
		
	}

	public function getIndex( Request $request )
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');

		if($sort=='status' && $order=='desc')
            $order='asc';
        else if($sort=='status' && $order=='asc')
            $order='desc';
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		  // print_r( $request->input('username'));exit;		
		  // Build pagination setting

		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('model');
		
	$rows = $request->input('rows') ? $request->input('rows') : 10 ;

    $query = \DB::table('tb_models');
    
    if($request->get('username'))
    {
      // echo $request->get('name');
        $query->where('username','like','%'.$request->get('username').'%')
        	->orwhere('firstname','like','%'.$request->get('username').'%')
        	->orwhere('lastname','like','%'.$request->get('username').'%')
        	->orderBy($sort,$order);
    }
     if($request->get('email'))
    {
        $query->where('email','like','%'.$request->get('email').'%')
         ->orwhere('contact_number','like','%'.$request->get('email').'%')
        ->orderBy($sort,$order);
    }
        	// print_r($request->get('status'));exit;

    // if($request->get('status'))
    // {
        $query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);
            	// 	print_r($query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order));exit;

    // }
       $this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);   

		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['username'] = 	(!empty($request->get('username')))?$request->get('username'):'';
		$this->data['email'] = (!empty($request->get('email')))?$request->get('email'):'';
		$this->data['status'] = $request->get('status');	
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('model.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_models'); 

		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
	
		
		return view('model.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);

		$row->ethnicity =  \SiteHelpers::getName('ethnicity',$row->ethnicity);
		$row->hair_color =  \SiteHelpers::getName('hair_color',$row->hair_color);
		$row->eye_color =  \SiteHelpers::getName('eye_color',$row->eye_color);
		$row->body_type =  \SiteHelpers::getName('body_type',$row->body_type);
		$row->height =  \SiteHelpers::getName('heights',$row->height);
		$row->marital_status =  \SiteHelpers::getName('marital_status',$row->marital_status);
		$row->smoking =  \SiteHelpers::getName('smoking',$row->smoking);
		//$row->model_categories =  \SiteHelpers::getName('model_categories',$row->model_categories);
		$row->drinking =  \SiteHelpers::getName('drinking',$row->drinking);
		$row->drugs =  \SiteHelpers::getName('drugs',$row->drugs);
		$row->children =  \SiteHelpers::getName('children',$row->children);
		//$row->model_industries = \SiteHelpers::getName('model_industries',$row->model_industries);
		$row->publishes =  \SiteHelpers::getName('publishes',$row->publishes);
		// $row->country = \SiteHelpers::getName('country',$row->country);
		$row->education = \SiteHelpers::getName('education',$row->education);
		$row->model_industries =\SiteHelpers::getcolumnvalue('model_industries',$row->model_industries);
		$row->model_categories =\SiteHelpers::getcolumnvalue('model_categories',$row->model_categories);
		if (is_numeric($row->country)) {
		$row->country = \SiteHelpers::getName('country',$row->country);
		}
		else{
		$row->country =$row->country;
		}
		if ($row->waist) 
			$row->waist=$row->waist;
		if ($row->hip) 
			$row->hip=$row->hip;
		
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			return view('model.view',$this->data);
		} else {
			return Redirect::to('model')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		//$rules['shoe_size'] = 'required|numeric';
		$rules=array(
			// 'marital_status'=>'required','username'=>'required','firstname'=>'required','lastname'=>'required',
			// 'DOB'=>'required','gender'=>'required','education'=>'required','profession'=>'required',
			// 'hobbies'=>'required','biography'=>'required','contact_number'=>'required','email'=>'required',
			// 'zipcode'=>'required','country'=>'required','state'=>'required','city'=>'required',
			// 'ethnicity'=>'required','body_type'=>'required','hair_color'=>'required','eye_color'=>'required',
			// 'shoe_size'=>'required','marital_status'=>'required','model_categories'=>'required',
			// 'drugs'=>'required','publishes'=>'required','children'=>'required','drinking'=>'required',
			// 'smoking'=>'required','hip'=>'required','waist'=>'required','bust_size'=>'required',
			// 'height'=>'required','language'=>'required',
			);
		
		$validator = Validator::make($request->all(), $rules);	
		// print_r($rules);exit;
		if ($validator->passes()) {
			$data = $this->validatePost('tb_model');
			if($request->hasFile('profile_cover')) {
			$files=$request->file('profile_cover');
						// print_r($files);exit;

			// $name=$file->getClientOriginalExtension();

			$name=date('His').$files->getClientOriginalName();
			$files->move('uploads/model_profile/cover',$name);
			$data['profile_cover'] =$name;

		
			}
			else{
				$name=' ';
			}
			$data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');
			$data['approved_date']=date("Y-m-d H:i:s");
			$data['marital_status']=$request->input('marital_status');
			$data['smoking']=$request->input('smoking');
			//$data['model_categories']=$request->input('model_categories');
			$data['drinking']=$request->input('drinking');
			$data['drugs']=$request->input('drugs');
			$data['children']=$request->input('children');
			//$data['model_industries']=$request->input('model_industries');
			$data['publishes']=$request->input('publishes');
			if($request->input('body_piercing')==''){
			$data['body_piercing'] = '';}
			else{
			$data['body_piercing']=$request->input('body_piercing');}
			if($request->input('tattoos')==''){
			$data['tattoos'] = '';}
			else{
			$data['tattoos']=$request->input('tattoos');}
			if($request->input('model_experience')==''){
			$data['model_experience'] = '';}
			else{
			$data['model_experience']=$request->input('model_experience');}
			$data['year']=$request->input('years');
			$data['biography']=strip_tags($request->input('biography'));
			$data['insta_link']=$request->input('insta_link');
			$data['twitter_link']=$request->input('twitter_link');
			$data['fb_link']=$request->input('fb_link');

			if($request->get('model_industries')){
			$model_industries = implode(",", $request->get('model_industries'));
			$data['model_industries'] =$model_industries;
			}
			if($request->get('model_categories')){
			$model_categories = implode(",", $request->get('model_categories'));
			$data['model_categories'] =$model_categories;
			}
			if(Input::get('created_date') == ''){
                $data['created_date'] = date("Y-m-d H:i:s");
            }
            if($request->input('password') == ''){
				$data['password']=str_random(5);}
				else{
				$data['password'] = $request->input('password');
			}

			$data['mail']= $request->input('mail');
			$id = $this->model->insertRow($data , $request->input('id'));
			if(CNF_ACTIVATION =='manual' || CNF_ACTIVATION =='auto'){

			if($data['status']==1 && $data['approved_status']==1 && $data['mail']=='not_send'){
				$data['links'] = $this->option->get_options('links');
			if($request->input('username') == ''){
				$data['username']=$request->input('firstname');}
				else{
				$data['username'] = $request->input('username');
			}
			Mail::send('user.emails.registration',$data ,function($message) use ($data){
			$message->from($data['email']);
			$message->to($data['email'])->subject('Registration mail');
			});		
			\DB::table('tb_models')->where('id', $id)->update(['mail' => 'send']);
			}
		}
			if(!is_null($request->input('apply')))
			{
				$return = 'model/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'model?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			$id 	= $request->input('ids');
			if(count($id)>0)
			{
			foreach ($id as $key => $value)
			{
				$datas['status']   = 'deleted';
				\DB::table('tb_transactions')
				->where('user_id','=',$value )
				->where('user_type','=','model' )
				->update($datas);
			}
}
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('model?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('model?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Model();
		$info = $model::makeInfo('model');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('model.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('model.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
			
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_models');		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}	

	
	

	public function  getOurmodel()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['links'] 	= '';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'ourmodel';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$rowperpage = 6;

				// Registered
				if(\Session::has('model_id') || \Session::has('agent_id'))
				{
				$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->limit($rowperpage)->get();	
				$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->lists('profile_photo')->toArray();
				}
				// Private
				elseif(\Session::has('model_id')){
				$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->limit($rowperpage)->get();	
				$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->lists('profile_photo')->toArray();	
				}
				// Admin
				elseif(\Auth::check()){
				$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit($rowperpage)->get();				
				$photos = $this->model->where('status','1')->where('approved_status','1')->lists('profile_photo')->toArray();
				}
				// Public
				else{
				$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->limit($rowperpage)->get();				
				$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->lists('profile_photo')->toArray();
				}
				

				$this->data['count'] = count($photos);
								$this->data['pages'] = 'model.ourmodel';

				// $this->data['content'] = view('model.ourmodel',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}

    public function postLoadDataAjax(Request $request)
    {
    	$row = $_POST['row'];
    	$rowperpage = 6;

// selecting posts
    	if(\Session::has('model_id') || \Session::has('agent_id'))
    		{
    			$query =  \DB::table('tb_models')->where('status',1)->where('approved_status',1)
    			->where('publishes','1')
    			->orwhere('publishes','4')
    			->offset($row)
    			->limit($rowperpage)
    			->get();
    		}
    		elseif(\Session::has('model_id')){
    			$query =  \DB::table('tb_models')->where('status',1)->where('approved_status',1)
    			->where('publishes','1')
    			->orwhere('publishes','4')
    			->orwhere('publishes','2')
    			->offset($row)
    			->limit($rowperpage)
    			->get();
    		}
    		elseif(\Auth::check()){
    			$query =  \DB::table('tb_models')->where('status',1)
    			->where('approved_status',1)
    			->offset($row)
    			->limit($rowperpage)
    			->get();
    		}
    		else{
    			$query =  \DB::table('tb_models')->where('status',1)->where('approved_status',1)
    			->where('publishes','1')
    			->offset($row)
    			->limit($rowperpage)
    			->get();
    		}

    		$html = '';
    		$html .= '<div class="row-model">';
    		foreach ($query as  $value) {
	# code...
    			$username_str=str_replace(' ','',$value->firstname);
    			$url_id =urlencode( base64_encode($username_str.'/'.$value->id));
				if($value->profile_photo !=''){
				$url = \SiteHelpers::user_model_profiles_home($value->profile_photo); }
				else{
    			$url = \SiteHelpers::user_model_profile_sizes($value->id,'_medium','png'); 
				}
    			$href=url('model-profile/'.$username_str.'/'.$url_id);

    			$html .= ' <a href='.$href.' onmouseover="changespp();"  onmouseout="changeapp2();" id="app_row">';
    			$html .= '<div class="model_item  our_model_item1" id="'.$value->id.'">';
    			$html .= '<img src="'.$url.'" >';
    			$html .= ' <div class="colorbox">';
    			$html .= '   <div class="inner_shades">';
    			$html .= '     <h4>Real Life & People</h4>';
    			$html .= '     <h1>'.$value->firstname.' '.$value->lastname.'</h1>';
    			$html .= ' <p>Professional Model</p>';
    			$html .= ' <p>Book model</p>';
    			$html .= '</div>';
    			$html .=  ' </div>';
    			$html .= ' </div>';
    			$html .= ' </a>';


    // Creating HTML structure
    // $html .= '<div id="'.$value->id.'" class="post">';
    // $html .= '<img src="'.$url.'" >';

    // $html .= '</div>';

    		}
$html .= '</div>';
    		echo $html;
    	}


/*	public function postModelRegister(Request $request){

		if(\Session::has('model_id')){
			return Redirect::to('model-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));
		} 
		elseif(\Session::has('agent_id')){
			return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));
		}else{
			$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$iPlanId   = $request->input('plan_membership');
				$cPlanType = $request->input('membership_type');

				$this->data['pageTitle'] 	= 'Models';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'model_register';


				// echo "<pre>"; print_r($this->data['membership']); exit;
				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();
				$data['country'] = \DB::table('tb_country')->get();

				$data['plan_id']	= $iPlanId;
				$data['plan_type']	= $cPlanType;
				$data['membership']	= $this->membership::getRow($iPlanId);						

				$this->data['content'] = view('model.model_register',$data);
				$page = 'layouts.'.CNF_THEME.'.index';

				return view($page,$this->data);			
			}
		}
	}*/  	

    public function  getModelRegister()
    {

    	if(\Session::has('model_id'))
    		{
    			return Redirect::to('model-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));
    		} 
    		elseif(\Session::has('agent_id'))
		{
			return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));
		}

		else {
			$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
			$row = $sql[0];

			$this->data['pageTitle'] 	= 'Model Registration';
			// $this->data['pageNote'] 	=  $row->note;
			$this->data['breadcrumb'] 	= 'inactive';	
			$this->data['pageMetakey'] 	=   '' ;
			$this->data['pageMetadesc'] = '';
			$this->data['tweetimg'] = '';
			$this->data['gplusimg'] = '';
			$this->data['v_url'] = ''; 
			$this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');
			$this->data['filename'] 	=  'model_register';				

			if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
			{
				$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
			} else {
				$page_template = 'layouts.'.CNF_THEME.'.template.page';
			}

			$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orderBy('id','desc')->get();

			$this->data['country'] = \DB::table('tb_country')->get();
			$this->data['pages'] = 'model.model_register';

			// $this->data['content'] = view('model.model_register',$this->data);
			$page = 'layouts.'.CNF_THEME.'.index';

			return view($page,$this->data);			
    		}
    	}
    }

	function postModelSave( Request $request)
	{
		$rules = $this->validateForm();
		// $rules=array(
		// 	'marital_status'=>'required','username'=>'required','firstname'=>'required','lastname'=>'required',
		// 	'DOB'=>'required','gender'=>'required','education'=>'required','profession'=>'required',
		// 	'hobbies'=>'required','biography'=>'required','contact_number'=>'required','email'=>'required',
		// 	'zipcode'=>'required','country'=>'required','state'=>'required','city'=>'required',
		// 	'ethnicity'=>'required','body_type'=>'required','hair_color'=>'required','eye_color'=>'required',
		// 	'shoe_size'=>'required','marital_status'=>'required','model_categories'=>'required',
		// 	'drugs'=>'required','publishes'=>'required','children'=>'required','drinking'=>'required',
		// 	'smoking'=>'required','hip'=>'required','waist'=>'required','bust_size'=>'required',
		// 	'height'=>'required','language'=>'required',
		// );
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_models');
			$data['created_date'] = date("Y-m-d H:i:s");
			$data['last_updated'] = date("Y-m-d H:i:s");
			$data['password']=str_random(5);

			$data['body_piercing'] = ' ';
			$data['tattoos'] = ' ';
			$data['model_experience'] = ' ';
			$data['DOB'] = $request->input('DOB');
			
			
			$data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');	
			$id = $this->model->insertRow($data , $request->input('id'));
			$plan_id = '5';
			$planlimits=\SiteHelpers::getplandetailsbyId($plan_id);

			 $userinfo=\DB::table('tb_models')->where('id',$id)->first();
 // print_r($userinfo);exit;
            if($planlimits->event_limit!="unlimited")
            $eventlimit=(int)$userinfo->eventplan + (int)$planlimits->event_limit;
            else
            $eventlimit="unlimited";
           
            if($planlimits->video_limit!="unlimited")
            $videolimit=(int)$userinfo->videoplan + (int)$planlimits->video_limit;
            else          
            $videolimit="unlimited";

            if($planlimits->image_limit!="unlimited")
            $imagelimit=(int)$userinfo->photoplan + (int)$planlimits->image_limit;
            else
            $imagelimit="unlimited";

                    \DB::table('tb_models')->where('id', $id)->update(['plan_id' => $plan_id,'eventplan'=>$eventlimit,'videoplan'=>$videolimit,'photoplan'=>$imagelimit]);


			//Update status in Tell a friend 
			$encoded_id= $request->input('decoded_id');
			$decoded_id= base64_decode($encoded_id);

			$updateplan = \SiteHelpers::updateBasicMembership('model',$id);
			// if automatic activation is set true from admin
			if(CNF_ACTIVATION=='auto')
			{

				$feed = array();
				$feed['username'] = $request->input('firstname');
				$feed['email'] = $request->input('email');
				// $feed['password'] = '******';
				$feed['password']= $data['password'];
				
				$feed['links']=$this->option->get_options('links');
				// print_r($feed['links']['fb']);exit;
				$status = Mail::send('user.emails.registration_auto_activation',$feed ,function($message) use ($feed){
					$message->from(CNF_EMAIL);
					$message->to($feed['email'])->subject('Registration Confirmation');

				});
				
				$updatestatus = \DB::table('tb_models')->where('id','=',$id)->update(['approved_date'=>$data['created_date'],'status'=>'1','approved_status'=>'1']);
			}
			elseif(CNF_ACTIVATION == 'confirmation'){
						$feed = array();
						$feed['username'] = $request->input('firstname');
						$feed['email'] = $request->input('email');
						// $feed['password'] = '******';
						$feed['password']=str_random(5);
						$feed['activationLink']='/modelConfirmAccount/'.$feed['email'].'/'.$feed['password'];
						$feed['links'] = $this->option->get_options('links');
						$status = Mail::send('user.emails.registration_activation',$feed ,function($message) use ($feed){
							$message->from('ramyabsetec@gmail.com');
							$message->to($feed['email'])->subject('Registration Confirmation');

						});
				$updatestatus = \DB::table('tb_models')->where('id','=',$id)->update(['password' => $feed['password'],'mail' => 'send','approved_date'=>$data['created_date'],'status'=>'0','approved_status'=>'0']);
						
					}
			//end
		
			$user = \DB::table('tb_tell_a_friend')->where('id', '=',$decoded_id )
			->where('status', '=', 'logged')
			->exists();
			if($user==1){
				 return Redirect::to('login')->with('messagetext','You are not allowed to Login with this URL')->with('msgstatus','error');
				
			}
			else{
					\DB::table('tb_tell_a_friend')
			->where('id', $decoded_id )
			->update(array(
				'status' => 'logged',
				'friend_id' => $id,
				'user_type' => 'model',
				'friend_name' => $request->input('username'),
				'registered' => date("Y-m-d H:i:s"),
				));
			}
			if($request->input('decoded_id')!=''){
			$return = "login?tellfriend=".$encoded_id;
			}
			else{
			$return = "login";
			}
			//$return = '/model-dashboard';

			// return Redirect::to($redirect)
			// 		->with('message', \SiteHelpers::alert('success',''))
			// 		->withInput();
			return Redirect::to($return)->with('messageetext','Thanks for registering! . You will receive a Mail after we will active your Account.')->with('msgstatus','success');

		} else {
			// return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			// ->withErrors($validator)->withInput();
		}	
	
	}

	public function modelConfirmAccount($email,$pwd){

				$user = \DB::table('tb_models')->where('email', $email )->where('password',$pwd)->get();

				if (count($user) >= 1)
				{
					// echo '<pre>';print_r($user);exit;
					\DB::table('tb_models')->where('email',  $email )->where('password',$pwd)->update(array('status' => '1','approved_status'=>'1'));

					return Redirect::to('login')->with('message_reset', 'Your account is active now!')->with('msgstatus','success');
					
				} else {
					return Redirect::to('model/create')->with('message_reset', 'Invalid Confirmation!')->with('msgstatus','error');
					
				}
			}

	public function postCheckuniqueemail(Request $request)
	{
		$email=\DB::table('tb_models')->where('email',$request->input('email'))->get();
		if($email){
		return '"This email id is already taken."';		
		}	
		else{
		return 'true';
		}
	}

	// check unique mail for Admin
	public function postCheckuniqueemailadmin(Request $request)
	{
		$result = explode(',',$_POST['email']);

		$email=\DB::table('tb_models')->where('email',$result[0])->get();
		if(count($email)>0)
		{
			foreach ($email as $key => $value) {
				if(isset($value->id) and $value->id==$result[1])
				{
					return 'true';
				}	
				else
				{
					return '"This email id is already taken."';	
				}
			}
		}
		else
		{
			return 'true';
		}
		
	}
	//end
	public function postCheckactiveaccount(Request $request)
	{
		$agree = $request->get('agree');
		if($agree=='model'){
		$email=\DB::table('tb_models')->where('email',$request->input('email'))->where('status',0)->where('approved_status',0)->get();
		}
		else{
		$email=\DB::table('tb_agency')->where('email',$request->input('email'))->where('status',0)->where('verification_status',0)->get();	
		}
		if($email){
		return 'true';
		}	
		else{
		return 'true';
		}
	}

	public function  getModelDash($id)
	{
		$id=\SiteHelpers::getSecurityurlID($id);

		if($this->iLoggedId==$id)
		{
						$model_id =\Session::get('model_id');
						$uid =\Session::get('agent_id');

		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Model Dashboard';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
								$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}
				$this->data['results'] =\DB::table('tb_message')
					
					->where('draft',0)
					->where('trashed_by_recipient',0)
					->where('deleted_by_recipient',0)
					->where('label_by_sender',NULL)
					->where('label_by_recipient',NULL)
					// ->where('model_agent',2)
					->where('recipient',$model_id)
					->limit(3)
					->orderBy('id','desc')
					->get();
				$this->data['videos'] =\DB::table('videos')->where('user_type','model')->where('user_id',$model_id)->where('status',1)->orderBy('id', 'desc')->first();	
				// Count Total Views
            	$this->data['views'] = \DB::table('videos')->where('user_type','agent')->count();
				// $this->data['results'] = \DB::table('tb_message')->where('model_agent',2)->where('read',0)->where('trashed_by_recipient',0)->limit(3)->get();
				
				/*Model list*/
				$model_users_data =$this->message->getMessageUsersModel();
				$usersModelAvatar = array();
				foreach($model_users_data as $user_data){
					$usersModelNameList[$user_data->id] = $user_data->username;
					$usersModelAvatar[$user_data->id] = $user_data->profile_photo;
				}
				$this->data['usersModelAvatar'] = $usersModelAvatar;
				$this->data['usersModelNameList'] = $usersModelNameList;
				// echo "<pre>";print_r($usersModelNameList);exit;
				$users_data =  $this->message->getMessageUsers( $uid );
				$users = array(''=>'Select an User');
				$username = array();
				$usersNameList = array();
				$usersEmailList = array();
				$usersAvatar = array();
				foreach($users_data as $user_data){
				$users[$user_data->id] = $user_data->username;
				$usersNameList[$user_data->id] = $user_data->username;
				$username[$user_data->id] = $user_data->username;
				$usersEmailList[$user_data->id] = $user_data->email;
				$usersIdList[$user_data->id] = $user_data->id;
				$usersAvatar[$user_data->id] = $user_data->profile_photo;
				}

				$this->data['usersNameList'] = $usersNameList;
				$this->data['usersAvatar'] = $usersAvatar;

				/*Membership start*/
					$this->data['membershipplan'] = \DB::table('tb_models')
					->select('tb_membership_plan.plan_name','tb_membership_plan.plan_amount')
					->join('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_models.plan_id')
					->where('tb_models.id',$model_id)
					->get();
					$umembership = \SiteHelpers::getMembershipofuser();
					if($umembership){
						$odr_details = json_decode($umembership->order_details,true);
						$subscriptionReference = $odr_details['subscriptionReference'];
						$customerReference = $odr_details['customerReference'];
						$gateway = \Omnipay::gateway('stripe');
						$options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
						$stripe = array();
						foreach($options as $results)
						{
							$stripe[$results->option_key]   = $results->options;
						}
						$gateway->setApiKey($stripe['secret_key']);

						$check_subscription_status 	= $gateway->fetchSubscription([
							"subscriptionReference" => $subscriptionReference,
							"customerReference" 	=> $customerReference
							])->send();
						$check_subscription_status_data = $check_subscription_status->getdata();

						if(isset($check_subscription_status_data['current_period_end']))
						{
							$current_period_end = date( 'm-d-Y', $check_subscription_status_data['current_period_end']);
						}else{
							$current_period_end = '-';
						}

						$this->data['membership'] = $umembership;
						$this->data['renewal_date'] = $current_period_end;
						$this->data['purchase_period'] = $umembership->purchase_period;	
					}else{

						$umembership_all_status = \SiteHelpers::getMembershipofuserallstatus();
						if($umembership_all_status){
							$this->data['membership'] = $umembership_all_status;
							$this->data['renewal_date'] = '-';
							$this->data['purchase_period'] = $umembership_all_status->purchase_period;
						}else{
							$this->data['renewal_date'] = '';
							$this->data['purchase_period'] = '';
						}

					}
					/*membership end*/
				$this->data['models'] = $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->get();
				$this->data['model'] = $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->first();
				$this->data['images'] = $this->getModelid($id);
				$this->data['gallery'] = \DB::table('tb_images')->where('status','Active')->where('user_type','model')->where('OwnerId',$model_id)->orderBy('id', 'desc')->paginate(8);
				$this->data['row'] = $this->model->find($id);
				// echo "<pre>";print_r($row);exit;
				$this->data['model_id']=$model_id;
				$this->data['user_type']='model';
				$this->data['messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',1)->where('recipient_type','model')->orderBy('id', 'desc')->paginate(3);
				$this->data['a_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',1)->where('recipient_type','agent')->orderBy('id', 'desc')->paginate(3);
				$this->data['a_mess_name'] = \SiteHelpers::getAgentMessageUsername();
				$this->data['m_mess_name'] = \SiteHelpers::getModelMessageUsername();
				// echo "<pre>";print_r($this->data['messages']);exit;
				$this->data['pages'] = 'model.model_dash';

				// $this->data['content'] = view('model.model_dash',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';

				return view($page,$this->data);			
			}
		}
		else{
			return Redirect::to('/');
		}
		
	}
		public function  getModelavatarprofilecover(Request $request, $id = null)
		{

		}
		public function  getModelprofile(Request $request, $id)
		{
			
			$id=\SiteHelpers::getSecurityurlID($id);
			if($this->iLoggedId==$id)
		{
					$sql = \DB::table('tb_pages')->where('default',1)->get();
					if(count($sql)>=1)
					{
						$row = $sql[0];

						$this->data['pageTitle'] 	= 'Profile';
						// $this->data['pageNote'] 	=  $row->note;
						$this->data['breadcrumb'] 	= 'inactive';	
						$this->data['pageMetakey'] 	=   '' ;
						$this->data['pageMetadesc'] = '';
						$this->data['tweetimg'] = '';
				  		$this->data['gplusimg'] = '';
						$this->data['v_url'] = ''; 
						$this->data['urls'] = '';
           			    $this->data['fbimg'] = '';
           			    $this->data['links']=$this->option->get_options('links');
						$this->data['filename'] 	=  'model_profile';				

						if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
						{
							$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
						} else {
							$page_template = 'layouts.'.CNF_THEME.'.template.page';
						}

						$this->data['models'] = $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->get();
						$this->data['images'] = $this->getModelid($id);
						
						$row = $this->model->find($id);
						if($row)
						{
							$this->data['row'] =  $row;
						}
						if (is_numeric($row->country)) {
				$row->country = \SiteHelpers::getName('country',$row->country);
				}
				else{
				$row->country =$row->country;
				}				
				
				$this->data['pages'] = 'model.model_profile';

						// $this->data['content'] = view('model.model_profile',$this->data);
						$page = 'layouts.'.CNF_THEME.'.index';


						return view($page,$this->data);			
					}
				}
				else{
					return Redirect::to('/');
					
				}

			}
	public function  postModelprofileSave(Request $request)
	{
		$rules = $this->validateForm();
		// if($request->input('hair_color')){
		// 	$rules['source_type'] = 'required';
		// }
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_model');
			$data['DOB'] = $request->input('DOB');
			$data['insta_link']=$request->input('insta_link');
			$data['twitter_link']=$request->input('twitter_link');
			$data['fb_link']=$request->input('fb_link');
			$data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');
			$data['marital_status']=$request->input('marital_status');
			$data['smoking']=$request->input('smoking');
			//$data['model_categories']=$request->input('model_categories');
			$data['drinking']=$request->input('drinking');
			$data['drugs']=$request->input('drugs');
			$data['children']=$request->input('children');
			//$data['model_industries']=$request->input('model_industries');
			$data['publishes']=$request->input('publishes');
			if($request->input('body_piercing')==''){
			$data['body_piercing'] = '';}
			else{
			$data['body_piercing']=$request->input('body_piercing');}
			// print_r($data['body_piercing']);exit;
		if($request->input('tattoos')==''){
			$data['tattoos'] = '';}
			else{
			$data['tattoos']=$request->input('tattoos');}
		if($request->input('model_experience')==''){
			$data['model_experience'] = '';}
			else{
			$data['model_experience']=$request->input('model_experience');}
			$data['year']=$request->input('years');
			$data['biography']=strip_tags($request->input('biography'));

			$data['id']=$request->input('ids');
			$data['address']=$request->input('formatted_address');
			$data['city']=$request->input('locality');
			$data['zipcode']=$request->input('postal_code');
			$data['state']=$request->input('administrative_area_level_1');
			$data['country']=$request->input('country');
			$data['latitude']=$request->input('lat');
			$data['longitude']=$request->input('lng');

			$data['insta_link']=$request->input('insta_link');
			$data['twitter_link']=$request->input('twitter_link');
			$data['fb_link']=$request->input('fb_link');
			
			$data['last_updated'] = date("Y-m-d H:i:s");
			if($request->get('model_industries')){
			$model_industries = implode(",", $request->get('model_industries'));
			$data['model_industries'] =$model_industries;
			}
			if($request->get('model_categories')){
			$model_categories = implode(",", $request->get('model_categories'));
			$data['model_categories'] =$model_categories;
			}
// echo "<pre>";print_r($_POST);exit;
			$id = $this->model->insertRow($data , $request->input('ids'));
		
		return Redirect::to('model-profile/'.\Session::get('url_securityID'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
	}
		
		
	}
	public function  getModelmembership(Request $request, $id = null)
	{
		$id=\SiteHelpers::getSecurityurlID($id);
		if($this->iLoggedId==$id)
		{
		
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Membership';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
				$this->data['links']=$this->option->get_options('links');
           	    $this->data['fbimg'] = '';
				$this->data['filename'] 	=  'model_membership';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				$this->data['images'] = $this->getModelid($id);
				$this->data['membershipplans'] = \DB::table('tb_models')
										    ->select('tb_membership_plan.plan_name')
										    ->join('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_models.plan_id')
										    ->where('tb_models.id',\Session::get('model_id'))
										    ->get();
				//membership plan for model
				$umembership = \SiteHelpers::getMembershipofuser();

				// echo "<pre>";print_r($umembership);die;
				if($umembership){
					$odr_details = json_decode($umembership->order_details,true);
					$subscriptionReference = $odr_details['subscriptionReference'];
					$customerReference = $odr_details['customerReference'];


					$gateway = \Omnipay::gateway('stripe');
			        $options = \DB::table('tb_options')->where('code', '=', 'stripe')->get();
			        $stripe = array();
			        foreach($options as $results)
			        {
			            $stripe[$results->option_key]   = $results->options;
			        }
			        $gateway->setApiKey($stripe['secret_key']);


					$check_subscription_status 	= $gateway->fetchSubscription([
						"subscriptionReference" => $subscriptionReference,
						"customerReference" => $customerReference
						])->send();
					$check_subscription_status_data = $check_subscription_status->getdata();
					if(isset($check_subscription_status_data['current_period_end']))
					{
						$current_period_end = date( 'm-d-Y', $check_subscription_status_data['current_period_end']);
					}else{
						$current_period_end = '-';
					}

					$this->data['membership'] = $umembership;
					$this->data['renewal_date'] = $current_period_end;
					$this->data['purchase_period'] = $umembership->purchase_period;	
				}else{

					$umembership_all_status = \SiteHelpers::getMembershipofuserallstatus();
// echo '<pre>';print_r($umembership_all_status);exit;
					if($umembership_all_status){
						$this->data['membership'] = $umembership_all_status;
						$this->data['renewal_date'] = '-';
						$this->data['purchase_period'] = $umembership_all_status->purchase_period;
					}else{
						$this->data['renewal_date'] = '';
						$this->data['purchase_period'] = '';
					}
					
				}
				$this->data['user_type'] = 'model';
				//$this->data['memberships'] = \DB::table('tb_membership_plan')->get();
				$this->data['pages'] = 'model.model_membership';

				// echo "<pre>";print_r($this->data['membershipplans']);exit;
				// $this->data['content'] = view('model.model_membership',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		}
		else{
			return redirect::to('/');
		}
	}
	
	public function  postModelAvatar(Request $request )
	{
		echo "<pre>";print_r($_FILES);echo "</pre>";
		$avatar 			= $request->file('avatar');
		
		if(!is_null($avatar)) {
		$jpeg_quality 				= 100;
		$targ_w = $targ_h 			= 300;
		//  get extension
		$type 	  = $avatar->getClientOriginalExtension();
		
		$pathName =  \Session::get('model_id').'.'.$type;

		$request->file('avatar')->move('./uploads/model_profile/'.\Session::get("model_id").'/',$pathName);

		$imgUrl = url('/uploads/model_profile/'.\Session::get("model_id").'/'.$pathName);

		$what = getimagesize($imgUrl);
		// echo "<pre>"; print_r($what);exit;
		switch(strtolower($what['mime'])){
		case 'image/png':
		$img_r = imagecreatefrompng($imgUrl);
		break;
		case 'image/jpeg':
		$img_r = imagecreatefromjpeg($imgUrl);
		break;
		case 'image/gif':
		$img_r = imagecreatefromgif($imgUrl);
		break;
		default: die('image type not supported');
		}
		$percent = 0;
		list($width, $height) = getimagesize($imgUrl);
		$new_width  = $width * $percent;
		$new_height = $height * $percent;
		// image process	
		$output_filename = './uploads/model_profile/'.\Session::get("model_id").'/'.\Session::get('model_id').'_crop';

		echo "output name"; var_dump($output_filename); exit;
		$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
		imagecopyresampled($dst_r,$img_r,0,0,$new_width,$new_height,$targ_w,$targ_h,$width,$height);
		imagejpeg($dst_r,$output_filename.'.'.$type,$jpeg_quality);

		// resize image width and height
		$image_size[] = array('imgW'=>'300','imgH'=>'300','value'=>'large');
		$image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
		$image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

		foreach($image_size as $size){
		$save_path = ($size['value'] =='normal') ? \Session::get('model_id').'.'.$type : \Session::get('model_id').'_'.$size['value'].'.'.$type; 
		Image::make($output_filename.'.'.$type)->resize($size['imgW'],$size['imgH'])->save('./uploads/model_profile/'.\Session::get("model_id").'/'.$save_path);
		}

		// $user_update = User::find(Session::get('uid'));
		//          $user_update->avatar = $image_id;
		//          if($user_update->save()){
		//              $response = array(
		//                  "status" => 'success',
		//                  "image_hash" => $image_update->image_hash,
		//              ); 
		//          }

		//          if (file_exists($output_filename.'.'.$type)){
		//              File::Delete($output_filename.'.'.$type);
		//              File::Delete('./uploads/users/'.$pathName);
		//          }
		}

		// return response()->json($response);
	}
	public function  postSaveModelProfile(Request $request )
	{
		if(\Session::get('agent_id')){
			$data = $request->image;
			$image_name= \Session::get('agent_id').'.png';
        $path = public_path() . "/uploads/agency_profile/" . $image_name;
			$image_size[] = array('imgW'=>'800','imgH'=>'800','value'=>'largeX');
 		$image_size[] = array('imgW'=>'330','imgH'=>'603','value'=>'large');
        $image_size[] = array('imgW'=>'314','imgH'=>'440','value'=>'medium');
		$image_size[] = array('imgW'=>'185','imgH'=>'176','value'=>'small');
		foreach($image_size as $size){
			$save_path = ($size['value'] =='normal') ? \Session::get('agent_id').'.'."png" : \Session::get('agent_id').'_'.$size['value'].'.'."png";
			Image::make($data)->resize($size['imgW'],$size['imgH'])->save('./uploads/agency_profile/'.$save_path);
		}

		$data = $request->image;
 		if($data){
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
        $image_name= \Session::get('agent_id').'.png';
        $path = public_path() . "/uploads/agency_profile/" . $image_name;

		\DB::table('tb_agency')->where('id',  \Session::get('agent_id') )->update(array('profile_photo' => $image_name));

        file_put_contents($path, $data);

 		
        return response()->json(['success'=>'done']);

    	}
	}
		elseif (\Session::get('model_id')) {
		$data = $request->image;
		 $image_name= \Session::get('model_id').'.png';
$path = public_path() . "/uploads/model_profile/" . $image_name;
$image_size[] = array('imgW'=>'800','imgH'=>'800','value'=>'largeX');
$image_size[] = array('imgW'=>'330','imgH'=>'603','value'=>'large');
$image_size[] = array('imgW'=>'314','imgH'=>'440','value'=>'medium');
$image_size[] = array('imgW'=>'185','imgH'=>'176','value'=>'small');
foreach($image_size as $size){
$save_path = ($size['value'] =='normal') ? \Session::get('model_id').'.'."png" : \Session::get('model_id').'_'.$size['value'].'.'."png";
Image::make($data)->resize($size['imgW'],$size['imgH'])->save('./uploads/model_profile/'.$save_path);
}
$data = $request->image;
 		if($data){
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
        $image_name= \Session::get('model_id').'.png';

		\DB::table('tb_models')->where('id',  \Session::get('model_id') )->update(array('profile_photo' => $image_name));
        file_put_contents($path, $data);

       
        return response()->json(['success'=>'done']);
    	}
}
 		
		
        
		}
public function  postSaveProfileCover(Request $request )
	{  
	if(\Session::get('agent_id')){
		$data1 = $request->images;
		if($data1){
		list($type, $data1) = explode(';', $data1);
        list(, $data1)      = explode(',', $data1);
		$data1 = base64_decode($data1);
        $image_name= time().'.png';
        $path = public_path() . "/uploads/agency_profile/cover/" . $image_name;

		\DB::table('tb_agency')->where('id',\Session::get('agent_id') )->update(array('profile_cover' => $image_name));
        file_put_contents($path, $data1);

		return response()->json(['success'=>'done']);
		} 
	}
	elseif (\Session::get('model_id')) {
		$data1 = $request->images;
		if($data1){
		list($type, $data1) = explode(';', $data1);
        list(, $data1)      = explode(',', $data1);
		$data1 = base64_decode($data1);
        $image_name= time().'.png';
        $path = public_path() . "/uploads/model_profile/cover/" . $image_name;

		\DB::table('tb_models')->where('id',  \Session::get('model_id') )->update(array('profile_cover' => $image_name));
        file_put_contents($path, $data1);
		return response()->json(['success'=>'done']);
		}
	}
	}

	public function  getModelMessage()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Models messages';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
								$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
				$this->data['fbimg'] = '';
				$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_message';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] ='model.model_message';
				$data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				
				// $this->data['content'] = view('model.model_message',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}
	public function  getModelMyProfile($name,$id)
	{
		
	$id=\SiteHelpers::getSecurityurlNameID($id);
// print_r($id);exit;
		$sUserType = 'model';
		// print_r($sUserType);exit;
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Profile  Edit';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '';
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
          		$this->data['fbimg'] = '';
          		$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_myprofile';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else 
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}


				$this->data['models'] = $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->get();
				$row = $this->model->getRow($id);
				//similar models
				$this->data['similarModels'] = \DB::table('tb_models')->where('id','!=',$id)->where('model_experience',$row->model_experience)->where('model_categories',$row->model_categories)->where('status','1')->where('approved_status','1')->get();	
				// echo "<pre>";print_r($this->data['similarModels']);exit;
				$similarmodelsData = $this->model->where('id','!=',$id)->where('model_experience',$row->model_experience)->where('model_categories',$row->model_categories)->where('status','1')->where('approved_status','1')->lists('profile_photo')->toArray();;
				$this->data['similarModelscount'] = count($similarmodelsData);
				$this->data['tweetimg'] = \SiteHelpers::user_model_profile_sizes($row->id,'_medium','png');
				$this->data['gplusimg'] = \SiteHelpers::user_model_profile_sizes($row->id,'_medium','png');
				$this->data['fbimg'] = \SiteHelpers::user_model_profile_sizes($row->id,'_medium','png');

		$row->ethnicity =  \SiteHelpers::getName('ethnicity',$row->ethnicity);
		$row->hair_color =  \SiteHelpers::getName('hair_color',$row->hair_color);
		$row->eye_color =  \SiteHelpers::getName('eye_color',$row->eye_color);
		$row->body_type =  \SiteHelpers::getName('body_type',$row->body_type);
		$row->bust_size =  \SiteHelpers::getName('bust_size',$row->bust_size);
		$row->height =  \SiteHelpers::getName('heights',$row->height);
		$row->marital_status =  \SiteHelpers::getName('marital_status',$row->marital_status);
		$row->smoking =  \SiteHelpers::getName('smoking',$row->smoking);
		//$row->model_categories =  \SiteHelpers::getName('model_categories',$row->model_categories);
		$row->drinking =  \SiteHelpers::getName('drinking',$row->drinking);
		$row->drugs =  \SiteHelpers::getName('drugs',$row->drugs);
		$row->children =  \SiteHelpers::getName('children',$row->children);
		//$row->model_industries = \SiteHelpers::getName('model_industries',$row->model_industries);
		$row->publishes =  \SiteHelpers::getName('publishes',$row->publishes);
		if (is_numeric($row->country)) {
		$row->country = \SiteHelpers::getName('country',$row->country);
		}
		else{
		$row->country =$row->country;
		}
		$row->education = \SiteHelpers::getName('education',$row->education);

		$row->model_industries =\SiteHelpers::getcolumnvalue('model_industries',$row->model_industries);
		$row->model_categories =\SiteHelpers::getcolumnvalue('model_categories',$row->model_categories);
		// print_r($row->model_industries);exit;

		// if ($row->body_piercing==1) $row->body_piercing='Yes';
		// else $row->body_piercing='No';
	
		// if ($row->tattoos==1) $row->tattoos='Yes';
		// else $row->tattoos='No';

		if ($row->waist) 
			$row->waist=$row->waist;
		if ($row->hip) 
			$row->hip=$row->hip;

		if ($row->model_experience==1) 
			$row->model_experience='New Face';
		elseif($row->model_experience==2) 
			$row->model_experience='Aspiring Model';
		elseif($row->model_experience==3) 
			$row->model_experience='Experienced Model';
		else $row->model_experience='Professional Model';
		
		if($row)
		{
			$this->data['row'] =  $row;
		}
			$this->data['user_type']=$sUserType;
		if(\SiteHelpers::checkLogged() == 1 && \Session::get('logged_id')){    

		$UserType=\Session::get('user_type');
		if($UserType=='model'){
		$userid = \Session::get('model_id');
		}
		else if($UserType=='agent'){
		$userid = \Session::get('agent_id');
		}
		else if($this->iLoggedId)
		{
			$userid =$this->iLoggedId; 
		}
		//Get count for particular ids selected stars
		$this->data['star_rating']= \SiteHelpers::getStarCount($id,$userid,$UserType);
			// get Sum ratings
		$ratedCounts =\SiteHelpers::getRatingCount($id);

		$sum=$ratedCounts->sum;
		if($sum>0)
			$total=($sum)/($ratedCounts->total_count);
		else
			$total='Not yet Rated';	
	       //Total counts for particular videos 
		$this->data['avg_ratings']=$total;
		$this->data['total_count']=$ratedCounts->total_count;
		}
		else{
			$this->data['avg_ratings']='';
			$this->data['star_rating']= '';
		}
		$this->data['favourite'] = \SiteHelpers::getfavourites($row->id,'model');
						$this->data['pages'] = 'model.model_myprofile';
				// $this->data['content'] = view('model.model_myprofile',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}

	public function  getModelsListing()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Models listing';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
           		$this->data['fbimg'] = '';
           		$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'models_listing';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				$this->data['pages'] = 'model.models_listing';

				// $this->data['content'] = view('model.models_listing','model.models_listing');
				$page = 'layouts.'.CNF_THEME.'.index';

				$this->data['max_photo_limit'] 	=  12;

				return view($page,$this->data);			
		}
	}

	public function getModelAlbums(){
		
	}

	public function  getModelsGallery(Request $request)
	{
		$iLoggedId = \session::get('logged_id');
		$sUserType = \session::get('user_type');
// print_r($iLoggedId);exit;
		if(empty($iLoggedId)){
			return Redirect::to('login');
		}

		$iUserId = \Session::get('model_id');
		$sql = \DB::table('tb_pages')->where('default',1)->get();
		if(count($sql)>=1)
		{
			$row = $sql[0];

			$this->data['pageTitle'] 	= 'Models Gallery';
			// $this->data['pageNote'] 	=  $row->note;
			$this->data['breadcrumb'] 	= 'inactive';	
			$this->data['pageMetakey'] 	=   '' ;
			$this->data['pageMetadesc'] = '';
			$this->data['tweetimg'] = '';
			$this->data['gplusimg'] = '';
			$this->data['v_url'] = ''; 
			$this->data['urls'] = '';
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');


			$this->data['filename'] 	=  'model_gallery';				

			if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
			{
				$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
			} else {
				$page_template = 'layouts.'.CNF_THEME.'.template.page';
			}

			$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();


			$page 	= $request->input('page', 1);

			$params = array(
				'page'		=> $page ,
				'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : 15 ) ,
				'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
			);


			$results = $this->model->getModelImages($iUserId,$params);

			$this->data['gallery'] = $results['rows'];

				// Get Query 
			$page 				= $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination         = new Paginator($results['rows'], $results['total'], $params['limit']);	
			$pagination->setPath('model-gallery');	
			$this->data['pagination'] = $pagination;			

			$this->data['logged_user']  = $iLoggedId;
			$this->data['user_type']    = $sUserType;
			$this->data['pages'] = 'model.model_gallery';

			// $this->data['content'] = view('model.model_gallery',$this->data);
			$page = 'layouts.'.CNF_THEME.'.index';


			return view($page,$this->data);			
		}
	}


	public function postAddAlbum(Request $request){
		
		// get the title & description
		$sTitle   = $request->input('title');
		$sDesc    = $request->input('desc');
		$slug     = \SiteHelpers::generate_slug($sTitle);
		$sStatus  = 'active';
		$iLoggedId = \session::get('logged_id');
		$sUserType = \session::get('user_type');

		$aData = array(
			'title' 		=> $sTitle,
			'uri' 			=> $slug,
			'description' 	=> $sDesc,
			'user_type' 	=> $sUserType,
			'ownerid' 		=> $iLoggedId,
			'status' 		=> $sStatus,
			'created' 		=> date('d-m-y h:i s'),
		);

		$iAlbumId = \SiteHelpers::addAlbum($aData);

		if(!empty($iAlbumId)){
		  return json_encode(array(
				'status'   => 1,
				'msg'	   => 'album added',
				'album_id' => $iAlbumId,
			));
		}else{
		  return json_encode(array(
				'status'   => 0,
				'msg'	   => 'unable to add album',	
				'album_id' => $iAlbumId,
			));			
		}
		
	}

	public function uploadGallery($sImage,$sExt,$iFotoId){
		// resize image width and height

		$image_size[] = array('imgW'=>'805','imgH'=>'470','value'=>'large');
		$image_size[] = array('imgW'=>'330','imgH'=>'603','value'=>'largeX');
		$image_size[] = array('imgW'=>'194','imgH'=>'90','value'=>'mediumX');
		$image_size[] = array('imgW'=>'355','imgH'=>'370','value'=>'medium');
		$image_size[] = array('imgW'=>'110','imgH'=>'120','value'=>'small');

		$sDirectory = public_path()."\uploads\model_gallery/";
		foreach($image_size as $size){
			$save_path = ($size['value'] =='normal') ? $iFotoId.'.'.$sExt : $iFotoId.'_'.$size['value'].'.'.$sExt;
			Image::make($sImage)->resize($size['imgW'],$size['imgH'])->save('./uploads/model_gallery/'.$save_path);
		}
	}

	public function postModelGalleryImage(){
		$sImage 	  = '';
		$iRemaingFoto = '';
		$iUserId 	  = \Session::get('logged_id');
		$sUserType 	  = \Session::get('user_type');
 		if($sUserType == 'model'){
        $iUserName= \Session::get('model_name');      
        }else{
         $iUserName= \Session::get('agent_name');   }
		$aUserInfo        = \SiteHelpers::getUserInfo($iUserId,$sUserType);
		// $iTotalUserFoto   = $aUserInfo->photo_count;
		// $iFotoLimit       = $aUserInfo->photo_limit;
		// echo "<pre>"; print_r($_REQUEST); exit;

		//get album details and check its image limit and album limit
		
		$image_status=1;
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
			// Upload directory
			$sDirectory = public_path()."/uploads/model_gallery/";

			// Allow file types
			$aAllowedExts = array("gif", "jpg", "jpeg", "png");

			$iTotalMedia  = 0;

			// create album
			$sAlbumType   = $_POST['album_type']; 
			if($sAlbumType == 'new'){
				$sAlbName = $_POST['album_name'];
				$sAlbDesc = $_POST['album_desc'];
				$sAlbumId = $this->model->insertAlbum($sAlbName,$sAlbDesc,$iUserId,$sUserType);
			}else
			$sAlbumId = $_POST['album_id'];


			foreach($_FILES as $key => $file)
			{
				/* Variables Declaration and Assignments */
				$sFileName  = basename($file['name']);
				$aFileParts = pathinfo($file['name']);
				$sFileTitle = $aFileParts['filename'];
				$sTmpFile   = $file['tmp_name'];
				$sExt       = pathinfo(strtolower($sFileName), PATHINFO_EXTENSION);

				$sPhotoTitle = $_POST['photo_title-'.$key];
				$sPhotoDesc  = $_POST['photo_desc-'.$key];
				$sPhotoTags  = $_POST['photo_tags-'.$key];

				//New file name
				if($sFileName == "") 
				{
					$aResArr[] = array(
						'status'  => 0,
						'key'     => $key,
						'msg'     => 'invalid file name' 
					);
					//Browse for a photo that you wish to use
				}
				else
				{
					if (in_array($sExt, $aAllowedExts)) 
					{
						$sStatus     = 'Inactive';
						$iFotoId     = $this->model->inserImage($iUserId,$sUserType,$sPhotoTitle,$sPhotoDesc,$sPhotoTags,$sExt,'Photo',$sStatus,$sAlbumId,$iUserName);

						$sRandomName = $iFotoId.'.'.$sExt;
						if(move_uploaded_file($sTmpFile, $sDirectory.$sRandomName)) 
						{
							// increase t he media count to update it in profiles
							if($_POST['photo_title-'.$key] == 'undefined')
							{

							}else{
							$iTotalMedia++;
							}
							$sDirImage = $sDirectory.$sRandomName;
							$this->uploadGallery($sDirImage,$sExt,$iFotoId);
							//  set the image status
							$aResArr[] = array(
								'status'  => 0,
								'key'     => $key,
								'msg'     => 'uploaded' 
							);

						}else{
							$aResArr[] = array(
								'status'  => 0,
								'key'     => $key,
								'msg'     => 'unable to upload' 
							);							
						}
					}
					else
					{
						$aResArr[] = array(
							'status'  => 0,
							'key'     => $key,
							'msg'     => 'invalid file extension' 
						);
					}

				}
			}

			if($iTotalMedia > 0){
				// update album count
				\DB::table('tb_albums')
				->where('album_id', $sAlbumId)
				->update([
					'count' => \DB::raw('count + '.$iTotalMedia.' '),
				]); 	
				// $this->model->updateMediaCount($iUserId,$iTotalMedia,'photo_count');
			}

			$oAlbum = Helper::getAlbumInfo($sAlbumId,'album_id');

			echo json_encode(
				array(
					'status'   => 1,
					'limit'    => $iRemaingFoto,
					'album_id' => $sAlbumId,
					'url'      => url('/photo/album/'.$oAlbum->uri),
					'arr'      => json_encode($aResArr)
				)); exit;
		}else{
			echo json_encode(
				array(
					'status' => 0,
					'limit'  => $iRemaingFoto,
					'album_id' => '',
					'msg'    => 'You are not allowed to Upload more Images !'
				)); exit;
		}
	}
	// public function postModelGalleryImage(){
		
	// 	$sImage 	  = '';
	// 	$iRemaingFoto = '';
	// 	$iUserId 	  = \Session::get('logged_id');
	// 	$sUserType 	  = \Session::get('user_type');

	// 	$aUserInfo        = \SiteHelpers::getUserInfo($iUserId,$sUserType);
	// 	// $iTotalUserFoto   = $aUserInfo->photo_count;
	// 	// $iFotoLimit       = $aUserInfo->photo_limit;

	// 	//get album details and check its image limit and album limit
		
	// 	$image_status=1;
	// 	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	// 	{

	// 		$sAlbumType   = $_POST['album_type']; 

	// 		if($sAlbumType == 'new'){
	// 			$membership_details = \DB::table('tb_membership_plan')
	// 								->where('plan_id', '=', $aUserInfo->plan_id)
	// 								->where('status','=','1')->first();
	// 			$album_details = \DB::table('tb_albums')
	// 							->where('ownerid', '=', $iUserId)
	// 							->where('user_type','=',$sUserType)
	// 							->get();

	// 			 // echo "<pre>";print_r($membership_details);
	// 			 // echo "<pre>";print_r($album_details);die;
	// 			if(count($album_details)>=$membership_details->image_limit)
	// 			{
	// 				$image_status=0;
	// 			}

	// 		}
	// 		else
	// 		{
	// 			$sAlbumId = $_POST['album_id'];
	// 			$album_details = \DB::table('tb_albums')
	// 							->where('ownerid', '=', $iUserId)
	// 							->where('user_type','=',$sUserType)
	// 							->where('album_id','=',$sAlbumId)
	// 							->first();
	// 			// echo "<pre>"; print_r($album_details); exit;				
	// 			if($album_details->count>=CNF_ALBUM)
	// 			{
	// 				$image_status=0;
	// 			}
	// 		}
	// 	}
			
	//     //end

	// 	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST" and $image_status==1)
	// 	{
	// 		// Upload directory
	// 		$sDirectory = public_path()."/uploads/model_gallery/";

	// 		// Allow file types
	// 		$aAllowedExts = array("gif", "jpg", "jpeg", "png");

	// 		$iTotalMedia  = 0;

	// 		// create album
	// 		$sAlbumType   = $_POST['album_type']; 
	// 		if($sAlbumType == 'new'){
	// 			$sAlbName = $_POST['album_name'];
	// 			$sAlbDesc = $_POST['album_desc'];
	// 			$sAlbumId = $this->model->insertAlbum($sAlbName,$sAlbDesc,$iUserId,$sUserType);
	// 		}else
	// 		$sAlbumId = $_POST['album_id'];


	// 		foreach($_FILES as $key => $file)
	// 		{
	// 			/* Variables Declaration and Assignments */
	// 			$sFileName  = basename($file['name']);
	// 			$aFileParts = pathinfo($file['name']);
	// 			$sFileTitle = $aFileParts['filename'];
	// 			$sTmpFile   = $file['tmp_name'];
	// 			$sExt       = pathinfo(strtolower($sFileName), PATHINFO_EXTENSION);

	// 			$sPhotoTitle = $_POST['photo_title-'.$key];
	// 			$sPhotoDesc  = $_POST['photo_desc-'.$key];
	// 			$sPhotoTags  = $_POST['photo_tags-'.$key];

	// 			//New file name
	// 			if($sFileName == "") 
	// 			{
	// 				$aResArr[] = array(
	// 					'status'  => 0,
	// 					'key'     => $key,
	// 					'msg'     => 'invalid file name' 
	// 				);
	// 				//Browse for a photo that you wish to use
	// 			}
	// 			else
	// 			{
	// 				if (in_array($sExt, $aAllowedExts)) 
	// 				{
	// 					$sStatus     = 'Inactive';
	// 					$iFotoId     = $this->model->inserImage($iUserId,$sUserType,$sPhotoTitle,$sPhotoDesc,$sPhotoTags,$sExt,'Photo',$sStatus,$sAlbumId);

	// 					$sRandomName = $iFotoId.'.'.$sExt;
	// 					if(move_uploaded_file($sTmpFile, $sDirectory.$sRandomName)) 
	// 					{
	// 						// increase t he media count to update it in profiles
	// 						$iTotalMedia++;

	// 						$sDirImage = $sDirectory.$sRandomName;
	// 						$this->uploadGallery($sDirImage,$sExt,$iFotoId);
	// 						//  set the image status
	// 						$aResArr[] = array(
	// 							'status'  => 0,
	// 							'key'     => $key,
	// 							'msg'     => 'uploaded' 
	// 						);

	// 					}else{
	// 						$aResArr[] = array(
	// 							'status'  => 0,
	// 							'key'     => $key,
	// 							'msg'     => 'unable to upload' 
	// 						);							
	// 					}
	// 				}
	// 				else
	// 				{
	// 					$aResArr[] = array(
	// 						'status'  => 0,
	// 						'key'     => $key,
	// 						'msg'     => 'invalid file extension' 
	// 					);
	// 				}

	// 			}
	// 		}

	// 		if($iTotalMedia > 0){
	// 			// update album count
	// 			\DB::table('tb_albums')
	// 			->where('album_id', $sAlbumId)
	// 			->update([
	// 				'count' => \DB::raw('count + '.$iTotalMedia.' '),
	// 			]); 	
	// 			// $this->model->updateMediaCount($iUserId,$iTotalMedia,'photo_count');
	// 		}

	// 		$oAlbum = Helper::getAlbumInfo($sAlbumId,'album_id');

	// 		echo json_encode(
	// 			array(
	// 				'status'   => 1,
	// 				'limit'    => $iRemaingFoto,
	// 				'album_id' => $sAlbumId,
	// 				'url'      => url('/photo/album/'.$oAlbum->uri),
	// 				'arr'      => json_encode($aResArr)
	// 			)); exit;
	// 	}else{
	// 		echo json_encode(
	// 			array(
	// 				'status' => 0,
	// 				'limit'  => $iRemaingFoto,
	// 				'album_id' => '',
	// 				'msg'    => 'You are not allowed to Upload more Images !'
	// 			)); exit;
	// 	}
	// }
	public function  getModelVideo($id)
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'video';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
           		$this->data['fbimg'] = '';
           		$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_video';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				$this->data['images'] = $this->getModelid($id);
				$this->data['videos'] =\DB::table('videos')->where('user_type','model')->orderBy('id', 'desc')->paginate(8);
								$this->data['pages'] = 'model.model_video';

				// $this->data['content'] = view('model.model_video',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}
	
	function getModelid($id) {
		return $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->get();
	}
		public function  getModelNetwork($id)
	{
		$id=\SiteHelpers::getSecurityurlID($id);
		if($this->iLoggedId==$id)
		{
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Networks';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_network';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['m_messages'] = Message::select('recipient','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',1)->where('recipient_type','model')->paginate(9);
				$this->data['a_messages'] = Message::select('recipient','recipient_type','entry_by')->distinct()->where('entry_by',$id)->where('model_agent',1)->where('recipient_type','agent')->paginate(9);
				$this->data['a_mess_name'] = \SiteHelpers::getAgentMessageUsername();
				$this->data['m_mess_name'] = \SiteHelpers::getModelMessageUsername();
				// echo "<pre>";print_r($this->data['a_messages']);exit;
				$this->data['user_type']='model';
				$this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				$this->data['images'] = $this->getModelid($id);
				$this->data['pages'] = 'model.model_network';

				// $this->data['content'] = view('model.model_network',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			} 	  	

		
	}
	else{
		return redirect::to('/');
	}
	
}


	public function getModelfiltersearch(Request $request ){
		// echo '<pre>';print_r($request->input('genders'));exit;
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['links'] 	= '';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'ourmodel';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$rowperpage = 6;

				$sql = $this->model;
				$query = '';
				$this->data['genders'] = '';
				$this->data['experience_needed'] = '';
				$this->data['categories_needed'] = '';
				
				if($request->input('genders') != 'Genders'){
				 $query = $sql->where('gender',$request->input('genders'));
				 $this->data['genders'] = $request->input('genders');
				
				}
				if($request->input('experience_needed') != ''){
					if($query == ''){
						$query = $sql->where('model_experience',$request->input('experience_needed'));
					}else{
						$query = $query->where('model_experience',$request->input('experience_needed'));
					}
				 	
			 		$this->data['experience_needed'] = $request->input('experience_needed');
				
				}
				if($request->input('categories_needed') != ''){
					if($query == ''){
						$query = $sql->where('model_categories',$request->input('categories_needed'));
					}else{
						$query = $query->where('model_categories',$request->input('categories_needed'));
					}
					$this->data['categories_needed'] = $request->input('categories_needed');
				}

				if($query == ''){
					$query = $sql;
				}
				
				// Registered
				if(\Session::has('model_id') || \Session::has('agent_id'))
				{
					$this->data['models'] = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->limit($rowperpage)->get();			
					$photos = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->lists('profile_photo')->toArray();
				}
				// Private
				elseif(\Session::has('model_id')){
					$this->data['models'] = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->limit($rowperpage)->get();			
					$photos = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->lists('profile_photo')->toArray();
				}
				// Admin
				elseif(\Auth::check()){
					$this->data['models'] = $query->where('status','1')->where('approved_status','1')->limit($rowperpage)->get();			
					$photos = $query->where('status','1')->where('approved_status','1')->lists('profile_photo')->toArray();
				}
				// Public
				else{
					$this->data['models'] = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->limit($rowperpage)->get();			
					$photos = $query->where('status','1')->where('approved_status','1')->where('publishes','1')->lists('profile_photo')->toArray();
				}

				$this->data['count'] = count($photos);
				$this->data['pages'] = 'model.model_filter';

				$page = 'layouts.'.CNF_THEME.'.index';
//echo '<pre>';print_r($this->data['count']);exit;
				return view($page,$this->data);			
		}
	}

	public function getModelfilter(Request $request ){
		//echo '<pre>';print_r('here');exit;
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['links'] 	= '';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['tweetimg'] = '';
				$this->data['gplusimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
            	$this->data['fbimg'] = '';
            	$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'ourmodel';				
				$this->data['genders'] = '';
				$this->data['experience_needed'] = '';
				$this->data['categories_needed'] = '';
				
				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$rowperpage = 6;

				// Registered
				if(\Session::has('model_id') || \Session::has('agent_id'))
				{
					$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->limit($rowperpage)->get();			
					$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->lists('profile_photo')->toArray();
				}
				// Private
				elseif(\Session::has('model_id')){
					$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->limit($rowperpage)->get();			
					$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->lists('profile_photo')->toArray();
				}
				// Admin
				elseif(\Auth::check()){
					$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit($rowperpage)->get();			
					$photos = $this->model->where('status','1')->where('approved_status','1')->lists('profile_photo')->toArray();
				}
				// Public
				else{
					$this->data['models'] = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->limit($rowperpage)->get();			
					$photos = $this->model->where('status','1')->where('approved_status','1')->where('publishes','1')->lists('profile_photo')->toArray();
				}
		
				$this->data['count'] = count($photos);
				$this->data['pages'] = 'model.model_filter';

				// $this->data['content'] = view('model.ourmodel',$this->data);
				$page = 'layouts.'.CNF_THEME.'.index';

 //echo '<pre>';print_r($this->data['count']);exit;
				return view($page,$this->data);			
		}

	}

	public function getModelfilterAjax(Request $request)
    {
    	$row = $_POST['row'];
    	$rowperpage = 6;
    	$sqlquery = '';

		$sql = $this->model;
		
		if($request->input('genders') != 'Genders'){
			$sqlquery = $sql->where('gender',$request->input('genders'));
		}
		if($request->input('experience_needed') != ''){
			if($sqlquery == ''){
				$sqlquery = $sql->where('model_experience',$request->input('experience_needed'));
			}else{
				$sqlquery = $sqlquery->where('model_experience',$request->input('experience_needed'));
			}
		}
		if($request->input('categories_needed') != ''){
			if($sqlquery == ''){
				$sqlquery = $sql->where('model_categories',$request->input('categories_needed'));
			}else{
				$sqlquery = $sqlquery->where('model_categories',$request->input('categories_needed'));
			}
		}

		if($sqlquery == ''){
			$sqlquery = $sql;
		}

 		// Registered
				if(\Session::has('model_id') || \Session::has('agent_id'))
				{
					
				$query =  $sqlquery->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->offset($row)->limit($rowperpage)->get();	
				}
				// Private
				elseif(\Session::has('model_id')){
				$query =  $sqlquery->where('status','1')->where('approved_status','1')->where('publishes','1')->orwhere('publishes','4')->orwhere('publishes','2')->offset($row)->limit($rowperpage)->get();	
				}
				// Admin
				elseif(\Auth::check()){
				$query =  $sqlquery->where('status','1')->where('approved_status','1')->offset($row)->limit($rowperpage)->get();
				}
				// Public
				else{
				$query =  $sqlquery->where('status','1')->where('approved_status','1')->where('publishes','1')->offset($row)->limit($rowperpage)->get();
				}	


    		$html = '';
    		foreach ($query as  $value) {
				
				$username_str=str_replace(' ','',$value->username);
    			$url_id =urlencode( base64_encode($username_str.'/'.$value->id));
    			$url = url('uploads/model_profile/'.$value->profile_photo); 
    			
				
				$href=url('model-profile/'.$username_str.'/'.$url_id);
    			$html .= ' <a href='.$href.' onmouseover="changespp();">';
    			$html .= '<div class="agent_div col-md-3 col-sm-3 col-xs-6">
				<div class="agent_image_blk">
				<img src="'.$url.'" class="agent_gallery_img">
				<div class="agent_hvr_icon_div">
				<div class="agent_hvr_effect">';
				$html .= '     <h1>'.$value->firstname.' '.$value->lastname.'/'.$value->age.'</h1>';
				$html .= '<h1>Experienced Model</h1>';
				$html .= '<h1>'. $value->state.','.$value->country.'</h1>
				</div>
				</div>
				</div>
				</div></a>';

    		}
    		echo $html;
    	}
}