<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect ; 
use Cookie;
class CookieController extends Controller {
     public function setCookie(Request $request){

        $value = $request->cookie('userType');

        if($value != $request->route('usertype') || $request->cookie('email') != $request->route('email') || $request->cookie('password') != $request->route('password')){
            $response = new Response('Magelur');
            $response->withCookie(cookie()->forever('userType', $request->route('usertype')));
            $response->withCookie(cookie()->forever('email', $request->route('email')));
            $response->withCookie(cookie()->forever('password', $request->route('password')));
             return $response;
        }else{
            if($value == 'agent'){
                $row = \DB::table('tb_agency')->select('*')->where('email',$request->cookie('email'))->where('password',$request->cookie('password'))->where('verification_status',1)->where('status',1)->first(); 
                  $encodeurlID = urlencode( base64_encode($row->id));
                  return Redirect::to('agent-dashboard/'.$encodeurlID);
            }else if($value == 'model'){
                $row = \DB::table('tb_models')->select('*')->where('email',$request->cookie('email'))->where('password',$request->cookie('password'))->where('approved_status',1)->where('status',1)->first();  
                  $encodeurlID = urlencode( base64_encode($row->id));
                  return Redirect::to('model-dashboard/'.$encodeurlID);
            }else{
                 $response = new Response('Magelur');
                $response->withCookie(cookie()->forever('userType', $request->route('usertype')));
                $response->withCookie(cookie()->forever('email', $request->route('email')));
                $response->withCookie(cookie()->forever('password', $request->route('password')));
                 return $response;
            }
        }
     }

     public function deleteCookie(Request $request){
        return Redirect::to('login')->withCookie(Cookie::forget('userType'))->withCookie(Cookie::forget('email'))->withCookie(Cookie::forget('password'))->with('message_reset', 'Your are now logged out!')->with('msgstatus','success');
     }

}