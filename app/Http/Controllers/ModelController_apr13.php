<?php namespace App\Http\Controllers;



use App\Http\Controllers\controller;

use App\Models\Modele;

use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Validator, Input, Redirect ; 





class ModelController extends Controller {



	protected $layout = "layouts.main";

	protected $data = array();	

	public $module = 'model';

	static $per_page	= '10';



	public function __construct()

	{

		

		$this->beforeFilter('csrf', array('on'=>'post'));

		$this->model = new Modele();

		

		$this->info = $this->model->makeInfo( $this->module);

		$this->access = $this->model->validAccess($this->info['id']);

	

		$this->data = array(

			'pageTitle'	=> 	$this->info['title'],

			'pageNote'	=>  $this->info['note'],

			'pageModule'=> 'model',

			'return'	=> self::returnUrl()

			

		);

		

		\App::setLocale(CNF_LANG);

		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {



		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);

		\App::setLocale($lang);

		}  





		

	}



	public function getIndex( Request $request )

	{



		if($this->access['is_view'] ==0) 

			return Redirect::to('dashboard')

				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');



		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 

		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');

		// End Filter sort and order for query 

		// Filter Search for query		

		$filter = '';	

		if(!is_null($request->input('search')))

		{

			$search = 	$this->buildSearch('maps');

			$filter = $search['param'];

			$this->data['search_map'] = $search['maps'];

		} 



		

		$page = $request->input('page', 1);

		$params = array(

			'page'		=> $page ,

			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,

			'sort'		=> $sort ,

			'order'		=> $order,

			'params'	=> $filter,

			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )

		);

		// Get Query 

		$results = $this->model->getRows( $params );		

		

		// Build pagination setting

		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	

		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	

		$pagination->setPath('model');

		

		$this->data['rowData']		= $results['rows'];

		// Build Pagination 

		$this->data['pagination']	= $pagination;

		// Build pager number and append current param GET

		$this->data['pager'] 		= $this->injectPaginate();	

		// Row grid Number 

		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 

		// Grid Configuration 

		$this->data['tableGrid'] 	= $this->info['config']['grid'];

		$this->data['tableForm'] 	= $this->info['config']['forms'];	

		// Group users permission

		$this->data['access']		= $this->access;

		// Detail from master if any

		

		// Master detail link if any 

		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

		// Render into template

		return view('model.index',$this->data);

	}	







	function getUpdate(Request $request, $id = null)

	{

	

		if($id =='')

		{

			if($this->access['is_add'] ==0 )

			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		}	

		

		if($id !='')

		{

			if($this->access['is_edit'] ==0 )

			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		}				

				

		$row = $this->model->find($id);

		if($row)

		{

			$this->data['row'] =  $row;

		} else {

			$this->data['row'] = $this->model->getColumnTable('tb_models'); 



		}

		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

		

		$this->data['id'] = $id;

	

		

		return view('model.form',$this->data);

	}	



	public function getShow( Request $request, $id = null)

	{



		if($this->access['is_detail'] ==0) 

		return Redirect::to('dashboard')

			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

					

		$row = $this->model->getRow($id);



		$row->ethnicity =  \SiteHelpers::getName('ethnicity',$row->ethnicity);

		$row->hair_color =  \SiteHelpers::getName('hair_color',$row->hair_color);

		$row->eye_color =  \SiteHelpers::getName('eye_color',$row->eye_color);

		$row->body_type =  \SiteHelpers::getName('body_type',$row->body_type);

		$row->height =  \SiteHelpers::getName('height',$row->height);

		$row->marital_status =  \SiteHelpers::getName('marital_status',$row->marital_status);

		$row->smoking =  \SiteHelpers::getName('smoking',$row->smoking);

		$row->model_categories =  \SiteHelpers::getName('model_categories',$row->model_categories);

		$row->drinking =  \SiteHelpers::getName('drinking',$row->drinking);

		$row->drugs =  \SiteHelpers::getName('drugs',$row->drugs);

		$row->children =  \SiteHelpers::getName('children',$row->children);

		$row->model_industries = \SiteHelpers::getName('model_industries',$row->model_industries);

		$row->publishes =  \SiteHelpers::getName('publishes',$row->publishes);

		$row->country = \SiteHelpers::getName('country',$row->country);

		

		if($row)

		{

			$this->data['row'] =  $row;

			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);

			$this->data['id'] = $id;

			$this->data['access']		= $this->access;

			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

			$this->data['prevnext'] = $this->model->prevNext($id);

			return view('model.view',$this->data);

		} else {

			return Redirect::to('model')->with('messagetext','Record Not Found !')->with('msgstatus','error');					

		}

	}	



	function postSave( Request $request)

	{

		

		$rules = $this->validateForm();

		//$rules['shoe_size'] = 'required|numeric';

		$rules=array(

			'marital_status'=>'required','username'=>'required','firstname'=>'required','lastname'=>'required',

			'DOB'=>'required','gender'=>'required','education'=>'required','profession'=>'required',

			'hobbies'=>'required','biography'=>'required','contact_number'=>'required','email'=>'required',

			'zipcode'=>'required','country'=>'required','state'=>'required','city'=>'required',

			'ethnicity'=>'required','body_type'=>'required','hair_color'=>'required','eye_color'=>'required',

			'shoe_size'=>'required','marital_status'=>'required','model_categories'=>'required',

			'drugs'=>'required','publishes'=>'required','children'=>'required','drinking'=>'required',

			'smoking'=>'required','hip'=>'required','waist'=>'required','bust_size'=>'required',

			'height'=>'required','language'=>'required',

			);

		

		$validator = Validator::make($request->all(), $rules);	

		// print_r($rules);exit;

		if ($validator->passes()) {

			$data = $this->validatePost('tb_model');

			$data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');

			$data['approved_date']=date("Y-m-d H:i:s");

			$data['marital_status']=$request->input('marital_status');

			$data['smoking']=$request->input('smoking');

			$data['model_categories']=$request->input('model_categories');

			$data['drinking']=$request->input('drinking');

			$data['drugs']=$request->input('drugs');

			$data['children']=$request->input('children');

			$data['model_industries']=$request->input('model_industries');

			$data['publishes']=$request->input('publishes');

			$data['body_piercing']=$request->input('body_piercing');

			$data['tattoos']=$request->input('tattoos');

			$data['model_experience']=$request->input('model_experience');

			$id = $this->model->insertRow($data , $request->input('id'));

			

			if(!is_null($request->input('apply')))

			{

				$return = 'model/update/'.$id.'?return='.self::returnUrl();

			} else {

				$return = 'model?return='.self::returnUrl();

			}



			// Insert logs into database

			if($request->input('id') =='')

			{

				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');

			} else {

				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');

			}



			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

			

		} else {



			return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')

			->withErrors($validator)->withInput();

		}	

	

	}	



	public function postDelete( Request $request)

	{

		

		if($this->access['is_remove'] ==0) 

			return Redirect::to('dashboard')

				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		// delete multipe rows 

		if(count($request->input('ids')) >=1)

		{

			$this->model->destroy($request->input('ids'));

			

			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");

			// redirect

			return Redirect::to('model?return='.self::returnUrl())

        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 

	

		} else {

			return Redirect::to('model?return='.self::returnUrl())

        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				

		}



	}	



	public static function display( )

	{

		$mode  = isset($_GET['view']) ? 'view' : 'default' ;

		$model  = new Model();

		$info = $model::makeInfo('model');



		$data = array(

			'pageTitle'	=> 	$info['title'],

			'pageNote'	=>  $info['note']

			

		);



		if($mode == 'view')

		{

			$id = $_GET['view'];

			$row = $model::getRow($id);

			if($row)

			{

				$data['row'] =  $row;

				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);

				$data['id'] = $id;

				return view('model.public.view',$data);

			} 



		} else {



			$page = isset($_GET['page']) ? $_GET['page'] : 1;

			$params = array(

				'page'		=> $page ,

				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,

				'sort'		=> 'id' ,

				'order'		=> 'asc',

				'params'	=> '',

				'global'	=> 1 

			);



			$result = $model::getRows( $params );

			$data['tableGrid'] 	= $info['config']['grid'];

			$data['rowData'] 	= $result['rows'];	



			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	

			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	

			$pagination->setPath('');

			$data['i']			= ($page * $params['limit'])- $params['limit']; 

			$data['pagination'] = $pagination;

			return view('model.public.index',$data);			

		}





	}



	function postSavepublic( Request $request)

	{

		

		$rules = $this->validateForm();

			

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_models');		

			 $this->model->insertRow($data , $request->input('id'));

			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');

		} else {



			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')

			->withErrors($validator)->withInput();



		}	

	

	}	



	

	



	public function  getOurmodel()

	{

		 $sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Models';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ourmodel';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();

				

				$this->data['content'] = view('model.ourmodel',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

		}

	}



    public function postLoadDataAjax(Request $request)

    {

        $output = '';

        $id = $request->id;

       	



        $posts = $this->model->where('status','1')->where('approved_status','1')->where('id','>',$id)->limit(6)->get();

      //echo "<pre>"; print_r($posts);exit;



        if(!$posts->isEmpty())

        {

            foreach($posts as $post)

            {

                $href=url('myprofile/'.$post->id);
                $url = url('uploads/model_profile/'.$post->profile_photo);                 
                $output .= ' <a href='.$href.'>
                			<div class="model_item">
							   <img src='.$url.'>
							     <div class="colorbox">
							      <div class="inner_shades">
							        <h4>Real Life & People</h4>
							        <h1>'.$post->firstname.' '.$post->lastname.'</h1>
							        <p>Professional Model</p>
							        <p>Book model</p>
							      </div>
							     </div>
							</div>
							</a>';

            }

            $output .= '<div class="butsload clearfix"  id="remove-row"><button id="btn-more" data-id='.$post->id.' ><span>Load More</span></button></div>';

            

            echo $output;

        }

    }



    	public function  getModelRegister()

	{

			if(\Session::has('model_id'))

		{

			return Redirect::to('model-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));

		} 

		elseif(\Session::has('agent_id'))

		{

			return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));

		}



		else {

		 $sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Models';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ourmodel';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();

				$data['country'] = \DB::table('tb_country')->get();



				$this->data['content'] = view('model.model_register',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

		}

	}

	}



	function postModelSave( Request $request)

	{

		

		$rules = $this->validateForm();

		$rules=array(

			'marital_status'=>'required','username'=>'required','firstname'=>'required','lastname'=>'required',

			'DOB'=>'required','gender'=>'required','education'=>'required','profession'=>'required',

			'hobbies'=>'required','biography'=>'required','contact_number'=>'required','email'=>'required',

			'zipcode'=>'required','country'=>'required','state'=>'required','city'=>'required',

			'ethnicity'=>'required','body_type'=>'required','hair_color'=>'required','eye_color'=>'required',

			'shoe_size'=>'required','marital_status'=>'required','model_categories'=>'required',

			'drugs'=>'required','publishes'=>'required','children'=>'required','drinking'=>'required',

			'smoking'=>'required','hip'=>'required','waist'=>'required','bust_size'=>'required',

			'height'=>'required','language'=>'required',

		);

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_models');

			$data['password']=md5(str_random(8));	

			$data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');	

			$id = $this->model->insertRow($data , $request->input('id'));

			$return = '/model-dashboard';

			$return = "/login";

			

		return Redirect::to('login');

					// ->with('message', \SiteHelpers::alert('success','Thanks for registering. Please check your inbox and follow activation link.'))->withInput();	

		} else {

			// return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')

			// ->withErrors($validator)->withInput();

		}	

	

	}



	public function postCheckuniqueemail(Request $request)

	{

		$email=\DB::table('tb_models')->where('email',$request->input('email'))->get();

		if($email){

		return '"This email id is already taken."';		

		}	

		else{

		return 'true';

		}

	}

	public function postCheckactiveaccount(Request $request)

	{

		$email=\DB::table('tb_models')->where('email',$request->input('email'))->where('status',0)->where('approved_status',0)->get();

		if($email){

		return '"Your account is not active"';		

		}	

		else{

		return 'true';

		}

	}



	public function  getModelDash($id)

	{

		if(\Session::has('model_id'))

		{

			

		 $sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Models';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ourmodel';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				$data['models'] = $this->model->where('id',$id)->where('status','1')->where('approved_status','1')->get();

				$this->data['content'] = view('model.model_dash',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

			}

		}

	}



		public function  getModelprofile(Request $request, $id = null)

	{

		if(\Session::has('model_id'))

		{

		$sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Models';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ourmodel';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				$data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

				

				$row = $this->model->find($id);

				if($row)

						{

							$data['row'] =  $row;

						}

				$this->data['content'] = view('model.model_profile',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

			}

		}

		else{

			return Redirect::to('login');

					

		}

		

	}

	public function  postModelprofileSave(Request $request)

	{

		$rules = $this->validateForm();

		// if($request->input('hair_color')){

		// 	$rules['source_type'] = 'required';

		// }

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_model');

			$data['DOB'] = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');

			$data['insta_link']=$request->input('insta_link');

			$data['twitter_link']=$request->input('twitter_link');

			$data['fb_link']=$request->input('fb_link');



			$data['marital_status']=$request->input('marital_status');

			$data['smoking']=$request->input('smoking');

			$data['model_categories']=$request->input('model_categories');

			$data['drinking']=$request->input('drinking');

			$data['drugs']=$request->input('drugs');

			$data['children']=$request->input('children');

			$data['model_industries']=$request->input('model_industries');

			$data['publishes']=$request->input('publishes');

			$data['body_piercing']=$request->input('body_piercing');

			$data['tattoos']=$request->input('tattoos');

			$data['model_experience']=$request->input('model_experience');
			$data['year']=$request->input('years');
			

			$id = $this->model->insertRow($data , $request->input('id'));

		

		return Redirect::to('model-profile/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

	}

		

		

	}

	public function  getModelmembership()

	{

		

		 $sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Models';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ourmodel';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				$data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();

				$this->data['content'] = view('model.model_membership',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

			}

		

	}

	public function  postSaveModelProfile(Request $request )

	{



 		$data = $request->image;



        list($type, $data) = explode(';', $data);

        list(, $data)      = explode(',', $data);

		$data = base64_decode($data);

        $image_name= time().'.png';

        $path = public_path() . "/uploads/model_profile/small/" . $image_name;



		\DB::table('tb_models')->where('id',  \Session::get('model_id') )->update(array('profile_small' => $image_name));

        file_put_contents($path, $data);



		//return Redirect::to('model-profile/'.\Session::get('model_id'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');



        return response()->json(['success'=>'done']);

		}
public function  getModelMessage()
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'ourmodel';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				
				$this->data['content'] = view('model.model_message',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}
	public function  getModelMyProfile($id)
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'ourmodel';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else 
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
				$row = $this->model->getRow($id);

		$row->ethnicity =  \SiteHelpers::getName('ethnicity',$row->ethnicity);
		$row->hair_color =  \SiteHelpers::getName('hair_color',$row->hair_color);
		$row->eye_color =  \SiteHelpers::getName('eye_color',$row->eye_color);
		$row->body_type =  \SiteHelpers::getName('body_type',$row->body_type);
		$row->bust_size =  \SiteHelpers::getName('bust_size',$row->bust_size);
		$row->height =  \SiteHelpers::getName('heights',$row->height);
		$row->marital_status =  \SiteHelpers::getName('marital_status',$row->marital_status);
		$row->smoking =  \SiteHelpers::getName('smoking',$row->smoking);
		$row->model_categories =  \SiteHelpers::getName('model_categories',$row->model_categories);
		$row->drinking =  \SiteHelpers::getName('drinking',$row->drinking);
		$row->drugs =  \SiteHelpers::getName('drugs',$row->drugs);
		$row->children =  \SiteHelpers::getName('children',$row->children);
		$row->model_industries = \SiteHelpers::getName('model_industries',$row->model_industries);
		$row->publishes =  \SiteHelpers::getName('publishes',$row->publishes);
		$row->country = \SiteHelpers::getName('country',$row->country);
		$row->education = \SiteHelpers::getName('education',$row->education);

		if ($row->body_piercing==1) $row->body_piercing='Yes';
		else $row->body_piercing='No';
	
		if ($row->tattoos==1) $row->tattoos='Yes';
		else $row->tattoos='No';

		if ($row->waist) 
			$row->waist=(int)($row->waist/2.54);
		if ($row->hip) 
			$row->hip=(int)($row->hip/2.54);

		if ($row->model_experience==1) 
			$row->model_experience='New Face';
		elseif($row->model_experience==2) 
			$row->model_experience='Aspiring Model';
		elseif($row->model_experience==3) 
			$row->model_experience='Experienced Model';
		else $row->model_experience='Professional Model';
		
		if($row)
		{
			$data['row'] =  $row;
		}
				$this->data['content'] = view('model.model_myprofile',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}

}