<?php namespace App\Http\Controllers;



use App\Http\Controllers\controller;

use App\Models\Agency;

use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Validator, Input, Redirect ; 





class AgencyController extends Controller {



	protected $layout = "layouts.main";

	protected $data = array();	

	public $module = 'agency';

	static $per_page	= '10';



	public function __construct()

	{

		

		$this->beforeFilter('csrf', array('on'=>'post'));

		$this->model = new Agency();

		

		$this->info = $this->model->makeInfo( $this->module);

		$this->access = $this->model->validAccess($this->info['id']);

	

		$this->data = array(

			'pageTitle'	=> 	$this->info['title'],

			'pageNote'	=>  $this->info['note'],

			'pageModule'=> 'agency',

			'return'	=> self::returnUrl()

			

		);

		

		\App::setLocale(CNF_LANG);

		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {



		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);

		\App::setLocale($lang);

		}  





		

	}



	public function getIndex( Request $request )

	{



		if($this->access['is_view'] ==0) 

			return Redirect::to('dashboard')

				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');



		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 

		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');

		// End Filter sort and order for query 

		// Filter Search for query		

		$filter = '';	

		if(!is_null($request->input('search')))

		{

			$search = 	$this->buildSearch('maps');

			$filter = $search['param'];

			$this->data['search_map'] = $search['maps'];

		} 



		

		$page = $request->input('page', 1);

		$params = array(

			'page'		=> $page ,

			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,

			'sort'		=> $sort ,

			'order'		=> $order,

			'params'	=> $filter,

			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )

		);

		// Get Query 

		$results = $this->model->getRows( $params );		

		

		// Build pagination setting

		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	

		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	

		$pagination->setPath('agency');

		

		$this->data['rowData']		= $results['rows'];

		// Build Pagination 

		$this->data['pagination']	= $pagination;

		// Build pager number and append current param GET

		$this->data['pager'] 		= $this->injectPaginate();	

		// Row grid Number 

		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 

		// Grid Configuration 

		$this->data['tableGrid'] 	= $this->info['config']['grid'];

		$this->data['tableForm'] 	= $this->info['config']['forms'];	

		// Group users permission

		$this->data['access']		= $this->access;

		// Detail from master if any

		

		// Master detail link if any 

		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

		// Render into template

		return view('agency.index',$this->data);

	}	







	function getUpdate(Request $request, $id = null)

	{

	

		if($id =='')

		{

			if($this->access['is_add'] ==0 )

			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		}	

		

		if($id !='')

		{

			if($this->access['is_edit'] ==0 )

			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		}				

				

		$row = $this->model->find($id);

		if($row)

		{

			$this->data['row'] =  $row;

		} else {

			$this->data['row'] = $this->model->getColumnTable('tb_agency'); 

		}

		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);

		

		$this->data['id'] = $id;

		$this->data['country'] = \DB::table('tb_country')->get();

		

		return view('agency.form',$this->data);

	}	



	public function getShow( Request $request, $id = null)

	{



		if($this->access['is_detail'] ==0) 

		return Redirect::to('dashboard')

			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

					

		$row = $this->model->getRow($id);

		if($row->country)

				$row->country = \SiteHelpers::getCountry($row->country);

		if($row)

		{

			$this->data['row'] =  $row;

			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);

			$this->data['id'] = $id;

			$this->data['access']		= $this->access;

			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

			$this->data['prevnext'] = $this->model->prevNext($id);

			return view('agency.view',$this->data);

		} else {

			return Redirect::to('agency')->with('messagetext','Record Not Found !')->with('msgstatus','error');					

		}

	}	



	function postSave( Request $request)

	{

		

		//$rules = $this->validateForm();

		// $rules['contact_number'] = 'required|numeric';

		// $rules['mobile'] = 'required|numeric';

	$rules=array(

 // 'mobile' => 'required|numeric|min:11',

 // 'contact_number' => 'required|numeric|min:11',

 // 'profile_photo' => 'required',

 // 'agency_banner' => 'required',



		);

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_agency');

				

			$id = $this->model->insertRow($data , $request->input('id'));

			

			if(!is_null($request->input('apply')))

			{

				$return = 'agency/update/'.$id.'?return='.self::returnUrl();

			} else {

				$return = 'agency?return='.self::returnUrl();

			}



			// Insert logs into database

			if($request->input('id') =='')

			{

				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');

			} else {

				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');

			}



			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

			

		} else {



			return Redirect::to('agency/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')

			->withErrors($validator)->withInput();

		}	

	

	}	



	public function postDelete( Request $request)

	{

		

		if($this->access['is_remove'] ==0) 

			return Redirect::to('dashboard')

				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		// delete multipe rows 

		if(count($request->input('ids')) >=1)

		{

			$this->model->destroy($request->input('ids'));

			

			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");

			// redirect

			return Redirect::to('agency?return='.self::returnUrl())

        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 

	

		} else {

			return Redirect::to('agency?return='.self::returnUrl())

        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				

		}



	}	



	public static function display( )

	{

		$mode  = isset($_GET['view']) ? 'view' : 'default' ;

		$model  = new Agency();

		$info = $model::makeInfo('agency');



		$data = array(

			'pageTitle'	=> 	$info['title'],

			'pageNote'	=>  $info['note']

			

		);



		if($mode == 'view')

		{

			$id = $_GET['view'];

			$row = $model::getRow($id);

			if($row)

			{

				$data['row'] =  $row;

				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);

				$data['id'] = $id;

				return view('agency.public.view',$data);

			} 



		} else {



			$page = isset($_GET['page']) ? $_GET['page'] : 1;

			$params = array(

				'page'		=> $page ,

				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,

				'sort'		=> 'id' ,

				'order'		=> 'asc',

				'params'	=> '',

				'global'	=> 1 

			);



			$result = $model::getRows( $params );

			$data['tableGrid'] 	= $info['config']['grid'];

			$data['rowData'] 	= $result['rows'];	



			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	

			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	

			$pagination->setPath('');

			$data['i']			= ($page * $params['limit'])- $params['limit']; 

			$data['pagination'] = $pagination;

			return view('agency.public.index',$data);			

		}





	}



	function postSavepublic( Request $request)

	{

		

		$rules = $this->validateForm();

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_agency');		

			 $this->model->insertRow($data , $request->input('id'));

			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');

		} else {



			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')

			->withErrors($validator)->withInput();



		}	

	

	}	





	function getCountry($id) {

		return \DB::table('tb_country')->select('country_name')->where('id', '=', $id)->value('country_name');

	}



	public function  getAgentRegister()

	{

			if(\Session::has('agent_id'))

		{

			return Redirect::to('agent-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));

		} 

		elseif(\Session::has('model_id'))

		{

			return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));

		} 

		else {

		 $sql = \DB::table('tb_pages')->where('default',1)->get();

			if(count($sql)>=1)

			{

				$row = $sql[0];



				$this->data['pageTitle'] 	= 'Our Agents';

				// $this->data['pageNote'] 	=  $row->note;

				$this->data['breadcrumb'] 	= 'inactive';	

				$this->data['pageMetakey'] 	=   '' ;

				$this->data['pageMetadesc'] = '';

				$this->data['filename'] 	=  'ouragent';				



				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')

				{

					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;

				} else {

					$page_template = 'layouts.'.CNF_THEME.'.template.page';

				}



				$this->data['pages'] = $page_template;

				//$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();

				$data['country'] = \DB::table('tb_country')->get();



				$this->data['content'] = view('agency.agent_register',$data);

				$page = 'layouts.'.CNF_THEME.'.index';





				return view($page,$this->data);			

		}

	}

	}



	function postAgentSave( Request $request)

	{

		

		$rules = $this->validateForm();



		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$data = $this->validatePost('tb_agency');

			$data['terms_agree']= $request->input('terms_agree');	

			// $data['age'] =	\Carbon\Carbon::parse($request->input('DOB'))->diff(\Carbon\Carbon::now())->format('%y');	

			$id = $this->model->insertRow($data , $request->input('id'));

			$return = '/agent-dashboard';

			$return = '/login';

			

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

			

		} else {

			// return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')

			// ->withErrors($validator)->withInput();

		}	

	

	}

	public function postCheckuniqueemailagent(Request $request)

	{

		$email=\DB::table('tb_agency')->where('email',$request->input('email'))->get();

		if($email){

		return '"This email id is already taken."';		

		}	

		else{

		return 'true';

		}

	}



}