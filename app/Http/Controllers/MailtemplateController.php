<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Mailtemplate;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class MailtemplateController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'mailtemplate';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Mailtemplate();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'mailtemplate',
			'return'	=> self::returnUrl()
			
		);
	}

	public function getIndex( Request $request )
	{
		if($this->access['is_view'] == 0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');

		  if($sort=='status' && $order=='desc')
            $order='asc';
        else if($sort=='status' && $order=='asc')
            $order='desc';
        
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('mailtemplate');
		$rows = $request->input('rows') ? $request->input('rows') : 10 ;
		$query = \DB::table('tb_mail_template');
		if($request->get('title'))
		{
			$query->where('subject','like','%'.$request->get('title').'%')->orderBy($sort,$order);

		}
		if($request->get('titles'))
		{
			$query->where('message','like','%'.$request->get('titles').'%')->orderBy($sort,$order);

		}
		if($request->get('startdate'))
		{
			$date= date('Y-m-d H:i',strtotime($request->get('startdate')));
			$query->where('created_at','like','%'.$date.'%')->orderBy($sort,$order);

		}
		// echo "<pre>";print_r($request->get('title'));exit;

					$query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

// print_r($request->get('email'));exit;

		$this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['title'] = (!empty($request->get('title')))?$request->get('title'):'';
		$this->data['titles'] = (!empty($request->get('titles')))?$request->get('titles'):'';	
		$this->data['startdate'] = 	(!empty($request->get('startdate')))?$request->get('startdate'):'';

		$this->data['status'] = $request->get('status');
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('mailtemplate.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_mail_template'); 
		}

		$this->data['id'] = $id;
		return view('mailtemplate.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_mail_template'); 
		}
		$this->data['id'] = $id;
		$this->data['prevnext'] = $this->model->prevNext($id);
		$this->data['access']		= $this->access;
		return view('mailtemplate.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		$rules = array(
			'subject' => 'required',
			'message' => 'required');
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('mailtemplate');
			$data['status'] = (Input::get('status') ==1) ? 1 :0;
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'mailtemplate/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'mailtemplate?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('mailtemplate/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('mailtemplate')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('mailtemplate')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}