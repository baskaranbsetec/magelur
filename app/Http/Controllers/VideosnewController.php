<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Videosnew;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class VideosnewController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'videosnew';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Videosnew();
		
		$this->info = $this->model->makeInfo($this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'videosnew',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  


		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('videosnew');
		
		$rows = $request->input('rows') ? $request->input('rows') : 10 ;
		//For Search by Title and Tags
	    $query = \DB::table('videos');
	    			//->join('categories', 'videos.category', '=', 'categories.id');
	    // echo "<pre>";print_r($request);die;
	    if($request->get('Title'))	
	    {
	      // echo $request->get('Title');die;
	        $query->where('title','like','%'.$request->get('Title').'%')
	        		->orderBy($sort,$order);
	        
	    }
	     /*if($request->get('Tags'))
	    {
	        $query->where('tags','like','%'.$request->get('Tags').'%');
	       
	    }*/
	    if($request->get('Tags'))
	    {

	    	// for multiple tags
	    	$alltags = explode(",",$request->get('Tags') );
	    	 // echo "<pre>";print_r($alltags);die;
	    	foreach ($alltags as $value) {
	    		if($value!="")
	    		{
	    			$query->orWhere('tags','like','%'.$value.'%')
	    				->orderBy($sort,$order);
	    		}
	    	}
	        // $query->where('Tags','like','%'.$request->get('Tags').'%');
	    }
	    //to filter by User type
	    if($request->get('Usertype'))
	    {
	      // echo $request->get('Title');die;
	        $query->where('user_type','=',$request->get('Usertype'))
	        		->orderBy($sort,$order);
	    }
	     if($request->get('video_id'))
	    {
	      // echo $request->get('Title');die;
	        $query->where('video_id','=',$request->get('video_id'))
	        		->orderBy($sort,$order);
	    }
	    if($request->get('categories'))
	    {
	      // echo $request->get('Title');die;
	        $query->where('category','=',$request->get('categories'))
	        		->orderBy($sort,$order);
	    }
	    if($request->get('ownername'))
	    {
	      // echo $request->get('Title');die;
	        $query->where('user_name','=',$request->get('ownername'))
	        		->orderBy($sort,$order);
	    }
	    $query->where('status','like','%'.$request->get('status').'%')->orderBy($sort,$order);

	    // echo "<pre>";print_r($rows);die;
		$this->data['model_filter'] = $query->orderBy($sort,$order)->paginate($rows);   

		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['ownername'] = (!empty($request->get('ownername')))?$request->get('ownername'):'';		
		$this->data['Usertype'] = (!empty($request->get('Usertype')))?$request->get('Usertype'):'';		
		$this->data['Title'] = (!empty($request->get('Title')))?$request->get('Title'):'';	;	
		$this->data['video_id'] = (!empty($request->get('video_id')))?$request->get('video_id'):'';	

		$this->data['Tags'] = (!empty($request->get('Tags')))?$request->get('Tags'):'';	

		$this->data['categories'] = $request->get('categories');	
		$this->data['status'] = $request->get('status');
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		// echo "<pre>";print_r($this->data);die;
		return view('videosnew.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('videos'); 
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		return view('videosnew.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			return view('videosnew.view',$this->data);
		} else {
			return Redirect::to('videosnew')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_videosnew');
				
			$id = $this->model->insertRow($data , $request->input('id'));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'videosnew/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'videosnew?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('videosnew/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		$types=$_POST['types'];
		if($types == 0){
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('videosnew?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('videosnew?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}
	}elseif ($types =='1') {
		$id 	= $request->input('ids');
// echo $id;
		if(count($id)>0)
		{
			foreach ($id as $key => $value)
			{
				$datas['status']   = 1;
				$sql=\DB::table('videos')->where('id','=',$value )->where('status','=',0 )->update($datas);
			}
			if($sql){
				return Redirect::to('videosnew?return='.self::returnUrl())
				->with('messagetext', 'videos has been approved successfully.')->with('msgstatus','success');                      
			}
			else{
				return Redirect::to('videosnew?return='.self::returnUrl())
				->with('messagetext', 'Not Applicable.')->with('msgstatus','error');                    	
			}
			                

		}
	}
	elseif ($types =='2') {
		$id 	= $request->input('ids');
// echo $id;
		if(count($id)>0)
		{
			foreach ($id as $key => $value)
			{
				$datas['status']   = 0;
				$sql=\DB::table('videos')->where('id','=',$value )->where('status','=',1 )->update($datas);
			}
			if($sql){
				return Redirect::to('videosnew?return='.self::returnUrl())
				->with('messagetext', 'videos has been unapproved successfully.')->with('msgstatus','success');                    
			}
			else{
				return Redirect::to('videosnew?return='.self::returnUrl())
				->with('messagetext', 'Not Applicable.')->with('msgstatus','error');                    	
			}
			                 

		}
	}


	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Videosnew();
		$info = $model::makeInfo('videosnew');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('videosnew.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('videosnew.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('videos');		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}	




}