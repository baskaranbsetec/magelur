<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Helper\Helper as Helper;
use Carbon\Carbon;
use Auth;
use Response;
use Validator;
use Khill\Lavacharts\Lavacharts as Lava;
use Input;
use SEOMeta;
use OpenGraph;
use Purifier;

use App\Models\Agency;
use App\Models\Modele;
use App\Models\Message;
use App\Models\Imagesnew;
use App\Models\Sximo;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Redirect ; 
use Intervention\Image\ImageManagerStatic as Image;

class VideoController extends Controller
{

    protected $layout     = "layouts.main";
    protected $data       = array();  
    public $module        = 'model';
    static $per_page      = '10';
    protected $iLoggedId            = false;
    protected $sUserType            = false;
    protected $bLogged              = false;
    protected $aUserInfo   = array();  

    public function __construct()
    {
        
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->model = new Modele();
        $this->agents = new Agency();
        $this->message = new Message();
        $this->imagesnew = new Imagesnew();
        $this->option = new Sximo();
        $this->iLoggedId  = \Session::get('logged_id');
        $this->sUserType  = \Session::get('user_type');
        $this->bLogged    = !empty($this->iLoggedId)?true:false;
        $this->aUserInfo  = \SiteHelpers::getUserInfo($this->iLoggedId,$this->sUserType);


        $this->info = $this->model->makeInfo( $this->module);
        $this->access = $this->model->validAccess($this->info['id']);
    
        $this->data = array(
            'pageTitle' =>  $this->info['title'],
            'pageNote'  =>  $this->info['note'],
            'pageModule'=> 'model',
            'return'    => self::returnUrl()
            
        );
        
        \App::setLocale(CNF_LANG);
        if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

        $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
        \App::setLocale($lang);
        }  

    }

    /**************** Search by tags ****************/
    public function tags(Request $request, $tag)
    {
        // Get Results
        $results = DB::table('videos')
                   ->where('status', 1)
                   ->where('title', 'LIKE', '%'.$tag.'%')
                   ->orWhere('tags', 'LIKE', '%'.$tag.'%')
                   ->orWhere('description', 'LIKE', '%'.$tag.'%')
                   ->paginate(20);

        // Get Ads
        $ads = DB::table('advertisments')->where('id', 1)->first();

        // SEO Meta
        $site_title        = Helper::site_title();
        $seo_description   = Helper::seo_description();

        // SEOMeta::setTitle($tag.' - '.$site_title);
        // SEOMeta::setDescription($seo_description);

        // Send Array
        $data = array(
            'results' => $results, 
            'ads'     => $ads, 
        );

        $this->data['content'] = view('videos.tag',$data);
        $this->data['pageTitle']    = 'Video Tag Selection';
                // $this->data['pageNote']  =  $row->note;
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['urls'] = '';
        $this->data['v_url'] = ''; 
        $this->data['fbimg'] = '';    
        $this->data['links']=$this->option->get_options('links');  
        $this->data['pages'] = 'layouts.'.CNF_THEME.'.template.page';;

        $page = 'layouts.'.CNF_THEME.'.index';

        return view($page,$this->data);         

    }    

    /* View Home Page */
    public function index()
    {
        // Check if User a VIP
        if (Helper::is_vip()) {
            // Get User
            $vip_user = DB::table('users')->where('id', $this->iLoggedId)->first();

            if ($vip_user && $vip_user->is_vip == 1) {
                // Get Payment
                $payment = DB::table('payments')->where('user_id', $this->iLoggedId)->first();
                if ($payment) {
                    // Check of expired
                    $created = new DateTime($payment->created_at);
                    $expired = new DateTime($payment->expired_at);

                    $diff = $expired->diff($created)->format("%a");

                    if ($diff >= 30 && $this->iLoggedId !== 1) {
                        // Change user from vip to normal
                        DB::table('users')->where('id', $this->iLoggedId)->update([
                            'is_vip'   => 0,
                            'verified' => 0,
                        ]);
                    }
                }
            }
        }

       
        $site_title        = Helper::site_title();
        $title_description = Helper::title_description();
        $seo_description   = Helper::seo_description();

        // SEO Meta
        // SEOMeta::setTitle($site_title.' - '.$title_description);
        // SEOMeta::setDescription($seo_description);

        $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];

                $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
            $this->data['breadcrumb']   = 'inactive';   
            $this->data['pageMetakey']  =   '' ;
            $this->data['pageMetadesc'] = '';
            $this->data['gplusimg'] = '';
            $this->data['tweetimg'] = '';
            $this->data['urls'] = '';
            $this->data['v_url'] = ''; 
            $this->data['fbimg'] = '';
            $this->data['links']=$this->option->get_options('links');           

            $this->data['filename']     =  'videos';             

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                 $this->data['agents'] = $this->agents->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();

                 // Get Videos
                 $this->data['videos'] = DB::table('videos')->where('status', 1)
                                    // ->where('user_id',)
                                     ->orderBy('id', 'desc')
                                     ->paginate(12);
                $this->data['pages'] = 'videos.videos';

                // $this->data['content'] = view('videos.videos', $this->data);
                $page = 'layouts.'.CNF_THEME.'.index';

                return view($page,$this->data);   

            }else{
            // video does not exists redirect to home page
            return redirect('/'); 
        }

        //return view('index')->with('videos', $videos);
    }

    public function list_user_videos($iUserId,$sUserType){
$iUserId=\SiteHelpers::getSecurityurlID($iUserId);
        // goto to login if user not logged
        if(empty($this->iLoggedId)){
           return redirect('/login'); 
       }


       if($sUserType == 'agent')
          $aUser  = \SiteHelpers::getUserInfo($iUserId,'agent');
       else
          $aUser  = \SiteHelpers::getUserInfo($iUserId,'model');


        // Check if User a VIP
      // if (Helper::is_vip()) {}


    $site_title        = Helper::site_title();
    $title_description = Helper::title_description();
    $seo_description   = Helper::seo_description();

    $sql = \DB::table('tb_pages')->where('default',1)->get();
    if(count($sql)>=1)
    {
        $row = $sql[0];

        $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['v_url'] = ''; 
        $this->data['urls'] = '';
        $this->data['fbimg'] = '';
        $this->data['links']=$this->option->get_options('links');           

        $this->data['filename']     =  'videos';             

        if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
        {
            $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
        } else {
            $page_template = 'layouts.'.CNF_THEME.'.template.page';
        }


        if($iUserId == $this->iLoggedId){
            $this->data['owner'] = 1;
            $iLimit        = 11;
        }
        else{
            $this->data['owner'] = 0;
            $iLimit        = 12;
        }
        // Get Videos
        $this->data['videos'] = DB::table('videos')
        ->where('user_id',$iUserId)
        ->where('user_type',$sUserType)
        ->where('status', 1)
        ->orderBy('id', 'desc')
        ->paginate($iLimit);

         // Get total count of videos and send it to view page
        $this->data['total_videos'] = DB::table('videos')
        ->where('user_id',$iUserId)
        ->where('user_type',$sUserType)
        ->count();

        $this->data['user']      = $aUser;
        $this->data['user_type'] = $sUserType;
        $this->data['userId'] = $this->iLoggedId; // send user Id
         $this->data['user_vid_id'] =$iUserId;
            // echo "<pre>"; print_r($this->data['user']); exit;
       $this->data['pages'] = 'videos.agent_videos';

        // $this->data['content'] = view('videos.agent_videos',$this->data);
        $page = 'layouts.'.CNF_THEME.'.index';

        return view($page,$this->data);   

    }else{
            // video does not exists redirect to home page
        return redirect('/'); 
    }

        //return view('index')->with('videos', $videos);

}

    public function getModelVideo(Request $request, $iUserId)
    { 
        
        return $this->list_user_videos($iUserId,'model');   
      
        
    }

    /* View Home Page */
    public function getAgentVideo(Request $request, $iUserId)
    {
        
        return $this->list_user_videos($iUserId,'agent');
       
    } 

    public function list_user_album($iUserId,$sUserType){

        $iUserId=\SiteHelpers::getSecurityurlID($iUserId);
        // goto to login if user not logged
        if(empty($this->iLoggedId)){
         return redirect('/login'); 
        }


     if($sUserType == 'agent'){
      $aUser  = \SiteHelpers::getUserInfo($iUserId,'agent');
      $username =  \Session::get('agent_name');}
      else{
      $aUser  = \SiteHelpers::getUserInfo($iUserId,'model');
      }
        // Check if User a VIP
        // if (Helper::is_vip()) {}


        // $site_title        = Helper::site_title();
        // $title_description = Helper::title_description();
        // $seo_description   = Helper::seo_description();

      $sql = \DB::table('tb_pages')->where('default',1)->get();
      if(count($sql)>=1)
      {
        $row = $sql[0];

        $this->data['pageTitle']    = 'Models Gallery';
                // $this->data['pageNote']  =  $row->note;
        $this->data['breadcrumb']   = 'inactive';   
        $this->data['pageMetakey']  =   '' ;
        $this->data['pageMetadesc'] = '';
        $this->data['gplusimg'] = '';
        $this->data['tweetimg'] = '';
        $this->data['v_url'] = ''; 
        $this->data['urls'] = '';
        $this->data['fbimg'] = '';
        $this->data['links']=$this->option->get_options('links');           
        $this->data['filename']     =  'videos';             

        if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
        {
            $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
        } else {
            $page_template = 'layouts.'.CNF_THEME.'.template.page';
        }

        // $this->data['pages'] = $page_template;

        if($iUserId == $this->iLoggedId){
            $this->data['owner'] = 1;
            $iLimit        = 11;
        }
        else{
            $this->data['owner'] = 0;
            $iLimit        = 12;
        }
        // Get Videos
    $this->data['albums'] = DB::table('tb_albums')
    ->where('ownerid',$iUserId)
    ->where('user_type',$sUserType)
    ->where('status','active')
    ->paginate(20);
// echo "<pre>";print_r(\DB::table('tb_images')->select('Ext')->where('ID',417)->value('Ext'));exit;
        $this->data['user']      = $aUser;

        $this->data['user_type'] = $sUserType;
         $this->data['userId'] = $this->iLoggedId; //sending userid
         $this->data['user_vid_id'] =$iUserId;
            // echo "<pre>"; print_r($sUserType); exit;
         $this->data['pages'] ='videos.albums';
        // $this->data['content'] = view('videos.albums', $this->data);
        $page = 'layouts.'.CNF_THEME.'.index';
        
        return view($page,$this->data);   

    }else{
            // video does not exists redirect to home page
        return redirect('/'); 
    }

        //return view('index')->with('videos', $videos);

}

     public function getAgentAlbum(Request $request, $iUserId)
    {
        return $this->list_user_album($iUserId,'agent');
    }  
    public function getModelAlbum(Request $request, $iUserId)
    {
        return $this->list_user_album($iUserId,'model');
    }    



//        public function postVideoLoadData(Request $request)
//     {
//         $row = $_POST['row'];
//         $rowperpage = 12;

// // selecting posts
//         if(\Session::has('model_id') || \Session::has('agent_id'))
//             {
//                 $query =  DB::table('videos')->where('status', 1)
//                                      ->orderBy('id', 'desc')
                                    
//                                      ->offset($row)
//                                      ->limit($rowperpage)
//                                      ->get();
//             }
           

//             $html = '';

//             foreach ($query as  $video) {
//     # code...
//                 $url = $video->preview; 
//                 $href=url('video/'.$video->video_id);

//                 $html .= '<div class=" gallery_video">';
//                 $html .= '<div class="magelur_gallery_div magelur_gallery_video_div col-md-6 col-sm-6 col-xs-12">';
//                 $html .= '<div class="magelur_image_blk magelur_video_blk">';
//                 $html .= ' <img src="'.$url.'" class="gallery_images1 gallery_video_images1">';
//                 $html .= ' <div class="img_hover_blk">';
//                 $html .= ' <div class="gallery_video_hvr">';
//                 $html .= ' <span class="video_bg_clr">';
//                 $html .= ' <a href='.$href.' class="hover-posts" target="_blank">';
//                 $html .= ' <span></span>';
//                 $html .= '<i class="fa fa-play"></i>';
//                 $html .=  ' </a>';
//                 $html .= ' </span>';
//                 $html .= ' </div>';
//                 $html .= ' </div>';
//                 $html .= ' </div>';
//                 $html .= ' </div>';


//     // Creating HTML structure
//     // $html .= '<div id="'.$value->id.'" class="post">';
//     // $html .= '<img src="'.$url.'" >';

//     // $html .= '</div>';

//             }

//             echo $html;
//         }




    /* View Watch video page */
    public function watch(Request $request, $vid)
    {

        if(empty($this->iLoggedId)){
            return Redirect::to('login');
        }

        // Check Video Exists or Not
        $video      = DB::table('videos')->where('video_id', $vid)->first();
        $aOwnerInfo = \SiteHelpers::getUserInfo($video->user_id,$video->user_type);

        if (($video) AND ($video->status == 1)) {
            // Check if a video for vip only
            if ($video->vip == 1) {
                
                // Check if user is vip
                if (!Helper::is_vip()) {
                    // Not VIP
                    return redirect('/upgrade')->with('error', 'Oops! Video only for VIP users.');
                }
            }

            // Get Visitor IP
            $ip          = Helper::get_visitorIP();

            // Get Visitor Data 
            $data        = Helper::get_country($ip);

            // country Code
            $countryCode = $data[0];

            // Country Name
            $country     = $data[1];

            // Visitor City
            $city        = $data[2];

            // Visitor Browser 
            $browser     = Helper::get_browser();

            // Get Visitor Plat Form
            $platform    = Helper::get_platform();

            // Check if Mobile or Computer
            $device      = Helper::is_mobile();

            
             // Check if Already view this video or it's first time
            $check_view  = DB::table('video_views')
                           ->where('video_id', $vid)
                           ->where('ip', $ip)
                           ->first();

            if (!$check_view) {
                // It's First Time Insert Data
                DB::table('video_views')->insert([
                    'video_id'     => $vid,
                    'owner'        => $video->user_id,
                    'ip'           => $ip,
                    'country'      => $country,
                    'country_code' => $countryCode,
                    'city'         => $city,
                    'browser'      => $browser,
                    'platform'     => $platform,
                    'device'       => $device,
                    'created_at'   => Carbon::now(),
                    'updated_at'   => Carbon::now(),
                ]);
            }

            // Count Total Views
            $views = DB::table('video_views')->where('video_id', $vid)->count();

            // Count Likes 
            $likes = DB::table('video_votes')->where('video_id', $vid)
                                             ->where('video_like', 1)
                                             ->count();

            // Count Dislikes 
            $dislikes = DB::table('video_votes')->where('video_id', $vid)
                                             ->where('video_dislike', 1)
                                             ->count();

            // Get All video Comments
            $comments = DB::table('video_comments')->where('video_id', $vid)
                                             ->where('status', 1)
                                             ->orderBy('id', 'desc')
                                             ->paginate(5);

            // Count Comments
            $total_comments = DB::table('video_comments')->where('video_id', $vid)
                                                   ->where('status', 1)
                                                   ->count();

            
            // Get Related Videos
            if(($video->user_id == $this->iLoggedId) && ($video->user_type == $this->sUserType)){
            $related_videos = DB::table('videos')->where('status', 1)
                                                 ->where('video_id', '!=', $vid)
                                                 ->where('user_id', '=', $video->user_id)
                                                 ->orderByRaw('RAND()')
                                                 ->take(15)->get();
            }else{
                $related_videos = DB::table('videos')->where('status', 1)
                                                    ->where('video_id', '!=', $vid)
                                                    ->Where('tags','like',$video->tags)
                                                    ->orderByRaw('RAND()')
                                                    ->take(15)->get();
            }
            // Get Ads
            $ads = DB::table('advertisments')->where('id', 1)->first();

            // if (Helper::is_adsense() == TRUE) {
            //     // Get User Adsense Code
            //     $adsense_revenue_fetch = DB::table('adsense_revenue')->where('user_id', $data['video']->user_id)->first();

            //     if (!$adsense_revenue_fetch) {
            //         DB::table('adsense_revenue')->insert([
            //             'user_id'         => $data['video']->user_id,
            //             'adsense_pub'     => null,
            //             'adsense_channel' => null,
            //         ]);
            //     }

            //     $adsense_revenue = DB::table('adsense_revenue')->where('user_id', $data['video']->user_id)->first();
            // }else{
            //     $adsense_revenue = NULL;
            // }
            

            // Get Player Settings
            $settings_player = DB::table('settings_player')->where('id', 1)->first();

            // Send As Array
            $this->data = array(
                'video'           => $video,
                'views'           => $views,
                'likes'           => $likes,
                'dislikes'        => $dislikes,
                'comments'        => $comments,
                'total_comments'  => $total_comments,
                'related_videos'  => $related_videos,
                'ads'             => $ads,
                'settings_player' => $settings_player,
                //'adsense_revenue' => $adsense_revenue,
            );

            // SEO Meta
            $site_title        = Helper::site_title();

            // SEOMeta::setTitle($video->title.' - '.$site_title);
            // SEOMeta::setDescription($video->description);
            // SEOMeta::addMeta('article:published_time', $video->created_at, 'property');
            // SEOMeta::addMeta('article:section', Helper::category_by_id($video->category), 'property');
            // SEOMeta::addKeyword([$video->tags]);
            // OpenGraph::addVideo($video->video_path, [
            //     'secure_url' => $video->video_path,
            //     'type'       => Helper::get_mime($video->type),
            //     'width'      => $video->width,
            //     'height'     => $video->height
            // ]);

            // OpenGraph::setDescription($video->description);
            // OpenGraph::setTitle($video->title);
            // OpenGraph::setUrl(url('/watch').'/'.$video->video_id);
            // OpenGraph::addProperty('type', 'article');
            // OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);
            // OpenGraph::addImage($video->preview);

            $sql = \DB::table('tb_pages')->where('default',1)->get();
            if(count($sql)>=1)
            {
                $row = $sql[0];
            if($video->user_type == 'model'){

                $img = \SiteHelpers::user_profiles_share($aOwnerInfo->id,'model',$aOwnerInfo->profile_photo,'/uploads/model_profile/');

                $this->data['pageTitle']    = 'Models Gallery';
                $this->data['model_firstname']    = $aOwnerInfo->firstname;
            }
            if($video->user_type == 'agent'){

                $img = \SiteHelpers::user_profiles_share($aOwnerInfo->id,'agent',$aOwnerInfo->profile_photo,'/uploads/agency_profile/');
                $this->data['pageTitle']    = 'Agents Gallery';
                $this->data['model_firstname']    = $aOwnerInfo->username;
            }
                // $img = asset('magelur/images/frontend-logo.png');
                // $this->data['pageNote']  =  $row->note;
                $this->data['breadcrumb']   = 'inactive';   
                $this->data['pageMetakey']  =   '' ;
                $this->data['pageMetadesc'] = '';
                $this->data['gplusimg'] = '';
                $this->data['tweetimg'] = $img;
                $this->data['v_url'] = ''; 
                $this->data['urls'] = '';
                $this->data['fbimg'] = $img;
                $this->data['links']=$this->option->get_options('links');           
                $this->data['filename']     =  'watch';             

                
                

                if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
                {
                    $page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
                } else {
                    $page_template = 'layouts.'.CNF_THEME.'.template.page';
                }

                // $this->data['models'] = $this->model->where('id',\Session::get('model_id'))->where('status','1')->where('approved_status','1')->get();
            
               // get Sum ratings
                $ratedCounts =\DB::table('video_comments')
                ->selectRaw('sum(rating) as sum,count(rating) as total_count')
                ->where('video_id',$vid)
                ->first();
                // echo "<pre>";print_r($ratedCounts);exit;
                $sum=$ratedCounts->sum;
                if($sum>0)
                    $total=number_format((float)($sum)/($ratedCounts->total_count), 1, '.', '');
                else
                     $total='Not yet Rated';  
                

                //Total counts for particular videos 
                $this->data['avg_ratings']=$total;

                 $u =  url('/video') .'/'.$video->video_id;

                $this->data["v_url"] = (!empty($u))?$u:'';

              // $this->data['average']= \DB::table('video_comments')->select('rate_avg')->where('user_id',$this->iLoggedId)->value('rate_avg');  

                 // Get Videos
                // $this->data['videos'] = DB::table('videos')->where('status', 1)
                //                      ->orderBy('id', 'desc')
                //                      ->paginate(12);
                // echo "<pre>"; print_r($this->data['comments']); exit;
                $this->data['favourite'] = \SiteHelpers::getfavourites($video->id,'video');
                $this->data['userId'] =$this->iLoggedId;// sending userid
                $this->data['pages'] = 'videos.watch';

                // $this->data['content'] = view('videos.watch',$this->data);
                $page = 'layouts.'.CNF_THEME.'.index';

                return view($page,$this->data);   
                //return view('videos.watch')->with($video_data);

            }else{
            // video does not exists redirect to home page
            return redirect('/'); 
        }

        //     // video Exists Show Watch Page
        //     return view('watch')->with($video_data);
         }else{
             // video does not exists redirect to home page
             //return redirect('/'); 
            $url_securityID = \Session::get('url_securityID');
            if(\Session::has('model_id')) 
        return redirect('/model/video/'.$url_securityID);  
        else if(\Session::has('agent_id'))
        return redirect('/agent/video/'.$url_securityID);
         //return redirect('/'); 
     }
     
 }


    /************* Like a Video **************/
    public function like(Request $request)
    {
        if ($request->ajax()) {
            // Check if User Login or Not
            if ($this->bLogged) {

                // Get User ID
                $user_id = $this->iLoggedId;

                // Get username
                $username = $this->aUserInfo->username;
                
                // Get Video ID
                $videoID = $request->get('videoID');

                // Check if video exists
                $checkVid = DB::table('videos')->where('video_id', $videoID)->where('status', 1)->first();

                if (!$checkVid) {
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Video not exists.',
                    );
                    return Response::json($response);
                }else{
                    if (!Helper::is_vip()) {
                        if ($checkVid->vip == 1) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! Please upgrade your account to like this video.',
                            );
                            return Response::json($response);
                        }
                    }
                }

                // Check if Already liked or disliked
                $check_vote = DB::table('video_votes')
                              ->where('user_id', $user_id)
                              ->where('user_type', $this->sUserType)
                              ->where('video_id', $videoID)
                              ->first();

                if ($check_vote) {

                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! You already Liked this video.',
                    );
                    return Response::json($response);
                }else{
                    // Insert Video in liked list
                    DB::table('video_votes')->insert([
                        'video_id'      => $videoID,
                        'user_id'       => $user_id,
                        'user_type'     => $this->sUserType,
                        'video_like'    => 1,
                        'video_dislike' => 0,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);

                    // Count Likes
                    $likes = DB::table('video_votes')
                             ->where('video_id', $videoID)
                             ->where('video_like', 1)
                             ->count();


                    // Send Notification
                    if ((Helper::video_owner($videoID)) != ($username)) {

                        DB::table('notifications')->insert([
                            'notice_to'   => Helper::video_owner($videoID),
                            'notice_from' => $username,
                            'type'        => 'like',
                            'details'     => $videoID,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);

                    }

                    // Return Success
                    $response = array(
                        'status' => 'success',
                        'likes'  => $likes,
                        'msg'    => 'Success! video added to liked list.',
                    );
                    return Response::json($response);
                }
                

            }else{
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Please login to rate this video.',
                );
                return Response::json($response);
            }
        }
    }


    /************* Dislike a Video **************/
    public function dislike(Request $request)
    {
        if ($request->ajax()) {
            // Check if User Login or Not
            if ($this->bLogged) {

                // Get User ID
                $user_id = $this->iLoggedId;

                // Username
                $username = $this->aUserInfo->username;
                
                // Get Video ID
                $videoID = $request->get('videoID');

                // Check if video exists
                $checkVid = DB::table('videos')->where('video_id', $videoID)->where('status', 1)->first();

                if (!$checkVid) {
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Video not exists.',
                    );
                    return Response::json($response);
                }else{
                    if (!Helper::is_vip()) {
                        if ($checkVid->vip == 1) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! Please upgrade your account to dislike this video.',
                            );
                            return Response::json($response);
                        }
                    }
                }

                // Check if Already liked or disliked
                $check_vote = DB::table('video_votes')
                              ->where('user_id', $user_id)
                              ->where('user_type', $this->sUserType)
                              ->where('video_id', $videoID)
                              ->first();

                if ($check_vote) {

                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! You already liked this video.',
                    );
                    return Response::json($response);
                }else{
                    // Insert Video in disliked list
                    DB::table('video_votes')->insert([
                        'video_id'      => $videoID,
                        'user_id'       => $user_id,
                        'video_like'    => 0,
                        'video_dislike' => 1,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);

                    // Send Notification
                    if ((Helper::video_owner($videoID)) != ($username)) {

                        DB::table('notifications')->insert([
                            'notice_to'   => Helper::video_owner($videoID),
                            'notice_from' => $username,
                            'type'        => 'dislike',
                            'details'     => $videoID,
                            'is_read'     => 0,
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now(),
                        ]);
                        
                    }

                    // Count Dislikes
                    $dislikes = DB::table('video_votes')
                                ->where('video_id', $videoID)
                                ->where('video_dislike', 1)
                                ->count();

                    // Return Success
                    $response = array(
                        'status'    => 'success',
                        'dislikes'  => $dislikes,
                        'msg'       => 'Success! video added to disliked list.',
                    );
                    return Response::json($response);
                }
                

            }else{
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Please login to rate this video.',
                );
                return Response::json($response);
            }
        }
    }



    /***** Embed Video *****/
    public function embed(Request $request, $vid)
    {
        // Check if Video Exists
        $video = DB::table('videos')->where('video_id', $vid)->first();

        if ($video AND($video->status == 1)) {

            if ($video->vip == 1) {

                if (!Helper::is_vip()) {
                    
                    return redirect('/upgrade')->with('error', 'Oops! Video id only for VIP users.');
                }
            }

            // Get Ads
            $ads = DB::table('advertisments')->where('id', 1)->first();

            // Get Player Settings
            $settings_player = DB::table('settings_player')->where('id', 1)->first();

            // Send array
            $data = array(
                'video'           => $video, 
                'ads'             => $ads, 
                'settings_player' => $settings_player, 
            );

            return view('videos.embed')->with($data);
        }else{
            return redirect('/');
        }
    }

    /***** Submit New Comment *****/
    public function comment(Request $request, $vid)
    {
        // Check if AJAX Request
        if ($request->ajax()) {

            // Check if Video Exists
            $video = DB::table('videos')->where('video_id', $vid)->first();

            if ($video) {

                if (!Helper::is_vip()) {
                    if ($video->vip == 1) {
                        $response = array(
                            'status' => 'error',
                            'msg'    => 'Oops! Please upgrade your account to like this video.',
                        );
                        return Response::json($response);
                    }
                }

                // Get Comment Value
                $comment = Purifier::clean($request->get('comment'));
                // get rating value
                // $rating  = Purifier::clean($request->get('rating'));
                 $rating  = $request->get('rating');
                // Check if User Login or Not
                if (!$this->bLogged) {
                    // Not Login
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Please login to add a comment.',
                    );
                    return Response::json($response);
                }else{
                    // Username
                    $username = $this->aUserInfo->username;

                    // if(empty($rating)){
                    //     $response = array(
                    //         'status' => 'error',
                    //         'msg'    => 'Oops! Please Choose Your Rating.',
                    //     );
                    //     return Response::json($response);
                    // }
                    
                    // Make a Validation
                    $validator = Validator::make($request->all(), [
                        'comment' => 'required|max:500',
                        // 'rating'  => 'required', 
                    ]);

                    if ($validator->fails()) {
                        $response = array(
                            'status' => 'error',
                            'msg'    => 'Oops! Comment required and max characters is 500.',
                        );
                        return Response::json($response);
                    }else{
                        // check if already post the same comment
                        $double_cm = DB::table('video_comments')
                                     ->where('video_id', $vid)
                                     ->where('user_id', $this->iLoggedId)
                                     ->where('comment', $comment)
                                     ->first();

                        if ($double_cm) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! You already added the same comment.',
                            );
                            return Response::json($response);
                        }

                        // Check Comment status
                        if ($this->aUserInfo->status == 1 && $this->sUserType != 'admin') {
                            $status = 1;
                        }

                        $auto_approve_comments = DB::table('settings_upload')
                                                 ->where('id', 1)
                                                 ->first();

                        if ($auto_approve_comments->auto_approve_comments == 0) {
                            $status = 1;
                        }else{
                            $status = 0;
                        }

                       // Insert Comment
                       $iCmtId = DB::table('video_comments')->insertGetId([
                            'video_id'   => $vid,
                            'user_id'    => $this->iLoggedId,
                            'user_type'  => $this->sUserType,
                            'status'     => $status,
                            'rating'     => $rating,
                            'comment'    => $comment,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);


                        // Send Notification
                        if ((Helper::video_owner($vid)) != ($username)) {

                            DB::table('notifications')->insert([
                                'notice_to'   => Helper::video_owner($vid),
                                'notice_from' => $username,
                                'type'        => 'comment',
                                'details'     => $vid,
                                'is_read'     => 0,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now(),
                            ]);

                        }

                        $response = array(
                            'status' => 'success',
                            'msg'    => 'Congartulations! Comment has been successfully added.',
                            'cmt_id' =>  $iCmtId,
                            'html'   => '<li class="commentScroll"><div class="comment-main-level">
                             <div class="comment-avatar">
                                    <img src="'.\App\Helper\Helper::user_avatar(\Session::get('logged_id'),\Session::get('user_type')).'" alt=""></div>
                                    <!-- Comment Details -->
                                    <div class="comment-box">
                                        <div class="comment-head">
                                            <!-- Uploader -->
                                            <h6 class="comment-name by-author"><a href="http://192.168.1.65:81/magelur/public/user/Baskaran" target="_blank">'.\App\Helper\Helper::channel_by_id($this->iLoggedId,$this->sUserType).'</a>
                                             <!-- Comment Date -->
                                            <span class="cm_date">'.\App\Helper\Helper::change_date_format(Carbon::now()).'</span>
                                            </h6>
                                           
                                        </div>

                                        <select data-rate="'.$rating.'" id="comment_id_'.$iCmtId.'" style="display: none;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        </select>

                                        <div class="comment-content">
                                           '.nl2br( \App\Helper\Helper::emoticons(\App\Helper\Helper::active_links($comment, $vid))) .'
                                        </div>
                                    </div>
                             </div>
                         </li>'
                        );
                        return Response::json($response);
                    }

                }

            }else{
                // Video Doesn't exists
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Video Doesn\'t Exists.',
                );
                return Response::json($response);
            }
        }   
    }


    /********* Report Video **********/
    public function report(Request $request, $vid)
    {

        // Check if AJAX Request
        if ($request->ajax()) {

            // Check if Video Exists
            $video = DB::table('videos')->where('video_id', $vid)->first();

            if ($video) {
                // Check if User Login or Not
                if ($this->bLogged) {

                    if (!Helper::is_vip()) {
                        if ($video->vip == 1) {
                            $response = array(
                                'status' => 'error',
                                'msg'    => 'Oops! Please upgrade your account to like this video.',
                            );
                            return Response::json($response);
                        }
                    }

                    $u_id = $this->iLoggedId;

                    // User Login, Check if Already Report this video
                    $report = DB::table('videos_flag')->where('video_id', $vid)
                                                      ->where('user_id', $u_id)
                                                      ->first();

                    if ($report) {
                        // Already flagged
                        $response = array(
                            'status' => 'error',
                            'msg'    => 'Oops! Video Already Flagged.',
                        );
                        return Response::json($response);
                    }else{
                        DB::table('videos_flag')->insert([
                            'video_id'   => $vid,
                            'user_id'    => $u_id,
                            'status'     => 0,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);

                        // Send Notification
                        if ((Helper::username_by_id(1,'admin')) != ($this->aUserInfo->name)) {

                            DB::table('notifications')->insert([
                                'notice_to'   => Helper::username_by_id(1,'admin'),
                                'notice_from' => $this->aUserInfo->name,
                                'type'        => 'report',
                                'details'     => $vid,
                                'is_read'     => 0,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now(),
                            ]);

                        }

                        // Success Flag
                        $response = array(
                            'status' => 'success',
                            'msg'    => 'Thank you! Video has been successfully flagged.',
                        );
                        return Response::json($response);
                    }

                }else{
                    // User Not Login
                    $response = array(
                        'status' => 'error',
                        'msg'    => 'Oops! Please login to flag this video.',
                    );
                    return Response::json($response);
                }
            }else{
                // Video not exists
                $response = array(
                    'status' => 'error',
                    'msg'    => 'Oops! Video Doesn\'t Exists.',
                );
                return Response::json($response);
            }
        }
    }

    /*********** User Videos Management ************/
    public function my_videos()
    {
        // Check if User Login or Not
        if ($this->bLogged) {

            // SEO Meta
            $site_title        = Helper::site_title();
            $seo_description   = Helper::seo_description();

            SEOMeta::setTitle('Video Manager - '.$site_title);
            SEOMeta::setDescription($seo_description);

            // User ID
            $user_id = $this->iLoggedId;

            // Get User Videos
            $videos = DB::table('videos')->where('user_id', $user_id)
                                         ->where('status', 1)
                                         ->orderBy('id', 'desc')   
                                         ->paginate(30);

            return view('my_videos')->with('videos', $videos);

        }else{

            // Not Login
            return redirect('/login');
        }
        
    }

    /*********** User Video statistics *********/
    public function userVideoStats(Request $request, $vid)
    {
        // check if user online or not
        if ($this->bLogged) {

            // User ID
            $user_id = $this->iLoggedId;

            // Check if Videos Exists and have permissions to see stats
            $video = DB::table('videos')
                     ->where('video_id', $vid)
                     ->where('status', 1)
                     ->where('user_id', $user_id)
                     ->first();

            if ($video) {
                // check if have a stats or not
                $video_stats = DB::table('video_views')
                               ->where('video_id', $vid)->get();

                if ($video_stats) {

                    // Get Video Stats
                    $views = DB::table('video_views')->where('video_id', $vid)->get();

                    // Get Browser
                    $get_browsers = Helper::get_browsers($vid);

                    $browsers = \Lava::DataTable();
                    $browsers->addStringColumn('Browsers')
                             ->addNumberColumn('Percent');
                    $browsers->addRow(['Unkonwn Browsers', $get_browsers[0]])
                             ->addRow(['Internet Explorer', $get_browsers[1]])
                             ->addRow(['Firefox', $get_browsers[2]])
                             ->addRow(['Safari', $get_browsers[3]])
                             ->addRow(['Chrome', $get_browsers[4]])
                             ->addRow(['Opera', $get_browsers[5]])
                             ->addRow(['Netscape', $get_browsers[6]])
                             ->addRow(['Maxthon', $get_browsers[7]])
                             ->addRow(['Konqueror', $get_browsers[8]])
                             ->addRow(['Handheld Browser', $get_browsers[9]]);

                    \Lava::DonutChart('Browsers', $browsers, [
                        'title' => 'Browsers'
                    ]);


                    // Get Platforms
                    $get_platforms = Helper::get_platforms($vid);

                    $platforms = \Lava::DataTable();
                    $platforms->addStringColumn('Platforms')
                              ->addNumberColumn('Percent');
                    $platforms->addRow(['Unkonwn OS Platforms', $get_platforms[0]])
                            ->addRow(['Windows 10', $get_platforms[1]])
                            ->addRow(['Windows 8', $get_platforms[2]])
                            ->addRow(['Windows 7', $get_platforms[3]])
                            ->addRow(['Windows XP', $get_platforms[4]])
                            ->addRow(['Mac OS', $get_platforms[5]])
                            ->addRow(['Linux', $get_platforms[6]])
                            ->addRow(['Ununtu', $get_platforms[7]])
                            ->addRow(['iPhone', $get_platforms[8]])
                            ->addRow(['Android', $get_platforms[9]])
                            ->addRow(['Mobile', $get_platforms[10]]);

                    \Lava::DonutChart('Platforms', $platforms, [
                        'title' => 'Platforms'
                    ]);

                    // Get Devices
                    $get_devices = Helper::get_devices($vid);

                    $devices = \Lava::DataTable();
                    $devices->addStringColumn('Devices')
                            ->addNumberColumn('Percent');
                    $devices->addRow(['Mobiles', $get_devices[0]])
                            ->addRow(['Computers', $get_devices[1]]);

                    \Lava::DonutChart('Devices', $devices, [
                        'title' => 'Devices'
                    ]);

                    // Send array
                    $data = array('views' => $views, 'video_stats' => $video_stats);

                    // SEO Meta
                    $site_title        = Helper::site_title();
                    $seo_description   = Helper::seo_description();

                    SEOMeta::setTitle('Video Stats - '.$site_title);
                    SEOMeta::setDescription($seo_description);
                    return view('video_stats')->with($data);
                }else{
                    return redirect('/videos')->with('error', 'Oops! Videos doesn\'t have any stats right now.');
                }

            }else{
                return redirect('/videos')->with('error', 'Oops! Videos doesn\'t exists or you don\'t have permissions to see stats.');
            }

        }else{
            // Not Login
            return redirect('/login');
        }
    }


    /**************** Edit User Video ******************/
    public function userVideoEdit(Request $request, $vid)
    {
        // check if user login or not
        if ($this->bLogged) {

            // Get User ID
            $user_id = $this->iLoggedId;

            // Check if video exists and have permission to edit
            $video = DB::table('videos')
                     ->where('video_id', $vid)
                     ->where('user_id', $user_id)
                     ->where('status', 1)
                     ->first();

            if ($video) {
                // SEO Meta
                $site_title        = Helper::site_title();
                $seo_description   = Helper::seo_description();

                SEOMeta::setTitle('Edit Video - '.$site_title);
                SEOMeta::setDescription($seo_description);

                return view('video_edit')->with('video', $video);
            }else{
                // Doesn't exists or don't have permission to edit it
                return redirect('/videos')->with('error', 'Oops! video doesn\'t exists or you don\'t have permissions to access this page');
            }

        }else{
            // Not login
            return redirect('/login');
        }
    }

    /**************** Update Video Details ********************/
    public function UpdateVideo(Request $request, $vid)
    {
        // Check if User Online
        if ($this->bLogged) {
            
            // Get user id
            $user_id = $this->iLoggedId;

            // check if have permissions to edit this video
            $video = DB::table('videos')->where('video_id', $vid)->where('user_id', $user_id)->first();

            if ($video) {

                // Get inputs values
                $title       = $request->get('title');
                $tags        = $request->get('tags');
                $description = strip_tags($request->get('description'));
                $category    = $request->get('category');
                $preview     = Input::file('preview');

                // Make Validation
                $validator = Validator::make($request->all(), [
                    'title'       => 'required|max:130| min:5',
                    'tags'        => 'required',
                    'category'    => 'required',
                    'description' => 'required|max:1000',
                    'preview'     => 'image|max:3000',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator);
                }else{

                    // Update video 
                    DB::table('videos')->where('video_id', $vid)->update([
                        'title'       => $title,
                        'tags'        => $tags,
                        'category'    => $category,
                        'description' => $description,
                        'updated_at'  => Carbon::now(),
                    ]);

                    // Check if wants to change video cover
                    if ($preview) {
                        // get Image Extension
                        $preview_ext  = $preview->getClientOriginalExtension();

                        // change Preview
                        $change_preview = Helper::change_video_preview($vid, $preview, $preview_ext);

                        // Get Preview URL to save in DB
                        $preview_url = url('/').'/content/uploads/video/images/preview/'.$vid. '/preview.'.$preview_ext;

                        // Get Thumb URL to save in DB
                        $thumb_url   = url('/').'/content/uploads/video/images/thumbnail/'.$vid. '/thumbnail.'.$preview_ext;

                        DB::table('videos')->where('video_id', $vid)->update([
                            'preview' => $preview_url,
                            'thumb'   => $thumb_url,
                        ]);
                    }

                    return back()->with('success', 'Congartulations! Video has been successfully updated.');

                }

            }else{
                // don't have permissions
                return redirect('/videos')->with('error', 'Oops! video doesn\'t exists or you don\'t have permissions to access this page');
            }

        }else{
            // Not login
            return redirect('/login');
        }
    }


    /**************** User wants to delete a video ***************/
    public function DeleteVideo(Request $request, $vid)
    {
        // check if user login
        if ($this->bLogged) {
            // echo $vid;die;
            // Get user id
            $user_id = $this->iLoggedId;
            $user_type = $this->sUserType;
            // Check if he is the owner of the video
            $video = DB::table('videos')->where('video_id', $vid)
                                        ->where('status', 1)
                                        ->where('user_id', $user_id)
                                        ->first();

            if ($video) {
                
                // Delete Video from uploads folder
            // $delete_video = Helper::delete_video($vid, $video->type);

                // Delete Video stats
                DB::table('video_views')->where('video_id', $vid)->delete();

                // Delete Video Flags
                DB::table('videos_flag')->where('video_id', $vid)->delete();

                // Delete Video Votes
                DB::table('video_votes')->where('video_id', $vid)->delete();

                // Delete Video Comments
                DB::table('video_comments')->where('video_id', $vid)->delete();

                // Now you can delete video from DB
            // DB::table('videos')->where('video_id', $vid)->delete();
  \DB::table('videos')
       ->where('video_id','=',$vid)
       ->update(['status'=>'deleted']);
                //Delete from favorites

                $fav_video = DB::table('tb_favorites')->where('favourite_id', $video->id)
                                        ->get();

                if($fav_video)
                {
                foreach ($fav_video as $key => $value) {
                     DB::table('tb_favorites')->where('favourite_id', $video->id)->delete();
                }
                }
                
                $user_id =urlencode( base64_encode($user_id));
                // print_r($user_id);exit;
                // Success Delete
                if($user_type=='agent')
                {
                    return redirect('/agent/video/'.$user_id)->with('messagedelete', 'Congartulations! Video has been successfully deleted.');        
                    
                }
                elseif($user_type=='model')
                {
                    return redirect('/model/video/'.$user_id)->with('messagedelete', 'Congartulations! Video has been successfully deleted.');
                }
                

            }else{
                // don't have permissions
                return redirect('/videos')->with('error', 'Oops! video doesn\'t exists or you don\'t have permissions to access this page');
            }

        }else{
            // Not login
            return redirect('/login');
        }
    }
    
}
