<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Modele;
use App\Models\Agency;
use Response;
use Socialize;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Sximo;

use Mail;
class UserController extends Controller {

	
	protected $layout = "layouts.main";

	public function __construct() {
		parent::__construct();
		$this->model = new Modele();
		$this->agent = new Agency();
		$this->option = new Sximo();
	} 

	public function getRegister() {
        
		if(CNF_REGIST =='false') :    
			if(\Auth::check()):
				 return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));
			else:
				 return Redirect::to('administration');
			  endif;
			  
		else :
				
				return view('user.register');  
		 endif ; 
           
	

	}

	public function postCreate( Request $request) {
	
		$rules = array(
			'firstname'=>'required|alpha_num|min:2',
			'lastname'=>'required|alpha_num|min:2',
			'email'=>'required|email|unique:tb_users',
			'password'=>'required|between:6,12|confirmed',
			'password_confirmation'=>'required|between:6,12'
			);	
		if(CNF_RECAPTCHA =='true') $rules['recaptcha_response_field'] = 'required|recaptcha';
				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			$code = rand(10000,10000000);
			
			$authen = new User;
			$authen->first_name = $request->input('firstname');
			$authen->last_name = $request->input('lastname');
			$authen->email = trim($request->input('email'));
			$authen->activation = $code;
			$authen->group_id = 3;
			$authen->password = \Hash::make($request->input('password'));
			if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
			$authen->save();
			
			$data = array(
				'firstname'	=> $request->input('firstname') ,
				'lastname'	=> $request->input('lastname') ,
				'email'		=> $request->input('email') ,
				'password'	=> $request->input('password') ,
				'code'		=> $code,
				'links'		=> $this->option->get_options('links'),
				
			);
			if(CNF_ACTIVATION == 'confirmation')
			{ 

				$to = $request->input('email');
				$subject = "[ " .CNF_APPNAME." ] REGISTRATION "; 

			
				if(defined('CNF_MAIL') && CNF_MAIL =='swift')
				{ 
					Mail::send('user.emails.registration', $data, function ($message) {
			    		$message->to($to)->subject($subject);
			    	});	

				}  else {
		
					$message = view('user.emails.registration', $data);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);	
				}

				$message = "Thanks for registering! . Please check your inbox and follow activation link";
								
			} elseif(CNF_ACTIVATION=='manual') {
				$message = "Thanks for registering! . We will validate you account before your account active";
			} else {
   			 	$message = "Thanks for registering! . Your account is active now ";         
			
			}	


			return Redirect::to('administration')->with('message',\SiteHelpers::alert('success',$message));
		} else {
			return Redirect::to('user/register')->with('message',\SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}
	}
	
	public function getActivation( Request $request  )
	{
		$num = $request->input('code');
		if($num =='')
			return Redirect::to('administration')->with('message',\SiteHelpers::alert('error','Invalid Code Activation!'));
		
		$user =  User::where('activation','=',$num)->get();
		if (count($user) >=1)
		{
			\DB::table('tb_users')->where('activation', $num )->update(array('active' => 1,'activation'=>''));
			return Redirect::to('administration')->with('message',\SiteHelpers::alert('success','Your account is active now!'));
			
		} else {
			return Redirect::to('administration')->with('message',\SiteHelpers::alert('error','Invalid Code Activation!'));
		}
		
		
	
	}
	public function getLogin() {
	
		if(\Auth::check())
		{
			return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));

		} else {
			$this->data['socialize'] =  config('services');
			return View('user.login',$this->data);
			
		}	
	}
	public function getModelAgentLogin() {
	
		if(\Session::has('model_id'))
		{
			return Redirect::to('model-dashboard/'.\Session::get('url_securityID'))->with('message',\SiteHelpers::alert('success',"You're already login"));
		} 
		elseif(\Session::has('agent_id'))
		{
			return Redirect::to('agent-dashboard/'.\Session::get('url_securityID'))->with('message',\SiteHelpers::alert('success',"You're already login"));
		} 

		else {
			 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Login';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] =  '' ;
				$this->data['gplusimg'] = '';
				$this->data['tweetimg'] = '';
				$this->data['v_url'] = ''; 
				$this->data['urls'] = '';
				$this->data['fbimg'] = '';
				$this->data['links']=$this->option->get_options('links');
				$this->data['filename'] 	=  'model_agent_login';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = 'user.model_agent_login';
				// $this->data['content'] = view('user.model_agent_login');
				$this->data['socialize'] =  config('services');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);		
			}	
		}
	}

	public function postModelAgentsignin( Request $request) {
		$rules = array();	
		$messages = array();
		// if(CNF_RECAPTCHA =='true') $rules['recaptcha_response_field'] = 'required|recaptcha';

		$validator = Validator::make(Input::all(),$rules,$messages);
		if ($validator->passes()) {

			$agree = $request->get('agree');
			if($agree=='model'){
				$checkIfActive=\DB::table('tb_models')->where('email',$request->input('email'))->where('status',0)->where('approved_status',0)->get();
			}
			else{
				$checkIfActive=\DB::table('tb_agency')->where('email',$request->input('email'))->where('status',0)->where('verification_status',0)->get();	
			}

			if($checkIfActive){
				return Redirect::to('/login')->with('message_reset', 'Your account is not active')->with('msgstatus','error');
			}

			
			$email=$request->input('email');
			$password=$request->input('password');
				if($request->get('agree')=='model'){
					
					$row = \DB::table('tb_models')->select('*')->where('email',$email)->where('password',$password)->where('approved_status',1)->where('status',1)->first(); 
					if($row){
					\Session::put('model_id',$row->id);
					\Session::put('logged_id', $row->id);
					\Session::put('user_type','model');
					\Session::put('model_email',$row->email);
					\Session::put('model_name',$row->firstname);
					$encodeurlID=urlencode( base64_encode($row->id));
					$encodeurl=urlencode( base64_encode( $row->firstname.'/'.$row->id) );
					// print_r($encodeurlID);exit;
					\Session::put('url_security',$encodeurl);
					\Session::put('url_securityID',$encodeurlID);

					if($request->input('terms_agree') == 1){
						return Redirect::to('cookie/set/model/'.$email.'/'.$password);
					}
					// print_r( \Session::get('model_name'));exit;
					//return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
	
					return Redirect::to('model-dashboard/'.$encodeurlID);
					}
					
					else{


					}
					

				} 
				else{
					$row = \DB::table('tb_agency')->select('*')->where('email',$email)->where('password',$password)->where('verification_status',1)->where('status',1)->first(); 
					if($row){
					\Session::put('agent_id'  , $row->id);
					\Session::put('logged_id' , $row->id);
					\Session::put('user_type' , 'agent');
					\Session::put('agent_email', $row->email);
					\Session::put('agent_name', $row->username);
					$encodeurlID=urlencode( base64_encode($row->id));
					$encodeurl=urlencode( base64_encode( $row->username.'/'.$row->id) );
					\Session::put('url_security',$encodeurl);
					\Session::put('url_securityID',$encodeurlID);	
					
					if($request->input('terms_agree') == 1){
						return Redirect::to('cookie/set/agent/'.$email.'/'.$password);
					}

					return Redirect::to('agent-dashboard/'.$encodeurlID);
					}else{
					
					}
				}
	}
		else {
				return Redirect::to('login')
					->with('message', \SiteHelpers::alert('error','The following  errors occurred'))
					->withErrors($validator)->withInput();
		}	
	}

	public function postSignin( Request $request) {
		$rules = array(
			'email'=>'required',
			'password'=>'required',
		);		
		//if(CNF_RECAPTCHA =='true') $rules['recaptcha_response_field'] = 'required|recaptcha';
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
			$remember = (!is_null($request->get('remember_me')) ? 'true' : 'false' );

			if (\Auth::attempt(array('username'=>$request->input('email'), 'password'=> $request->input('password') ), $remember ) || \Auth::attempt(array('email'=>$request->input('email'), 'password'=> $request->input('password') ), $remember )) {
				if(\Auth::check())
				{
					$row = User::find(\Auth::user()->id); 
	
					if($row->active =='0')
					{
						// inactive 
						if($request->ajax() == true )
						{
							
							return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
						} else {
							\Auth::logout();
							return Redirect::to('administration')->with('message', \SiteHelpers::alert('error','Your Account is not active'));
						}
						
					} else if($row->active=='2')
					{

						if($request->ajax() == true )
						{
							return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
						} else {
							// BLocked users
							\Auth::logout();
							return Redirect::to('administration')->with('message', \SiteHelpers::alert('error','Your Account is BLocked'));
						}
					} else if($row->active=='1') {
						\DB::table('tb_users')->where('id', '=',$row->id )->update(array('last_login' => date("Y-m-d H:i:s")));
						\Session::put('uid', $row->id);
						\Session::put('logged_id', $row->id);
						\Session::put('user_type','admin');
						\Session::put('username', $row->first_name.' '. $row->last_name);
						\Session::put('gid', $row->group_id);
						\Session::put('eid', $row->email);
						\Session::put('ll', $row->last_login);
						\Session::put('fid', $row->first_name.' '. $row->last_name);	
						if(\Session::get('lang') =='')
						{
							\Session::put('lang', CNF_LANG);	
						}

						// if($request->ajax() == true )
						// {
						// 	if(CNF_FRONT =='false') :
						// 		return response()->json(['status' => 'success', 'url' => url('dashboard')]);					
						// 	else :
						// 		return response()->json(['status' => 'success', 'url' => url('dashboard')]);
						// 	endif;	

						// } else {
						// 	if(CNF_FRONT =='false') :
						// 		return Redirect::to('dashboard');						
						// 	else :
						// 		return Redirect::to('dashboard');
						// 	endif;	

						// }
						return Redirect::to('dashboard');
						
											
					}
					else{

					}			
					
				}			
				
			} else {
return 'false';
				// if($request->ajax() == true )
				// {
				// 	return response()->json(['status' => 'error', 'message' => 'Your username/password combination was incorrect']);
				// } else {

				// 	return Redirect::to('administration')
				// 		->with('message', \SiteHelpers::alert('error','Your username/password combination was incorrect'))
				// 		->withInput();					
				// }


			}
		} else {

				if($request->ajax() == true)
				{
					return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
				} else {

					return Redirect::to('administration')
						->with('message', \SiteHelpers::alert('error','The following  errors occurred'))
						->withErrors($validator)->withInput();
				}	
		

		}	
	}

	public function getProfile() {
		
		if(!\Auth::check()) return redirect('administration');
		
		
		$info =	User::find(\Auth::user()->id);
		$this->data = array(
			'pageTitle'	=> 'My Profile',
			'pageNote'	=> 'View Detail My Info',
			'info'		=> $info,
		);
		return view('user.profile',$this->data);
	}
	
	public function postSaveprofile( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('administration');
		$rules = array(
			'first_name'=>'required|alpha_num|min:2',
			'last_name'=>'required|alpha_num|min:2',
			);	
			
		if($request->input('email') != \Session::get('eid'))
		{
			$rules['email'] = 'required|email|unique:tb_users';
		}	

		if(!is_null(Input::file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';

				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			
			if(!is_null(Input::file('avatar')))
			{
				$file = $request->file('avatar'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				 $newfilename = \Session::get('uid').'.'.$extension;
				$uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
				    $data['avatar'] = $newfilename; 
				} 
				
			}		
			
			$user = User::find(\Session::get('uid'));
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->email 		= $request->input('email');
			if(isset( $data['avatar']))  $user->avatar  = $newfilename; 			
			$user->save();

			$newUser = User::find(\Session::get('uid'));

			\Session::put('fid',$newUser->first_name.' '.$newUser->last_name);

			return Redirect::to('user/profile')->with('messagetext','Profile has been saved!')->with('msgstatus','success');
		} else {
			return Redirect::to('user/profile')->with('messagetext','The following errors occurred')->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}
	
	public function postSavepassword( Request $request)
	{
		$rules = array(
			'password'=>'required|between:6,12',
			'password_confirmation'=>'required|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$user = User::find(\Session::get('uid'));
			$user->password = \Hash::make($request->input('password'));
			$user->save();

			return Redirect::to('user/profile')->with('message_reset','Password has been saved!')->with('msgstatus','success');
		} else {
			return Redirect::to('user/profile')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}	
	
	public function getReminder()
	{
	
		return view('user.remind');
	}	

	public function postRequest( Request $request)
	{

		$rules = array(
			'credit_email'=>'required|email'
		);	
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
	
			$user =  User::where('email','=',$request->input('credit_email'));
			if($user->count() >=1)
			{
				$user = $user->get();
				$user = $user[0];
				$data = array('token'=>$request->input('_token'));	
				$to = $request->input('credit_email');
				$subject = "[ " .CNF_APPNAME." ] REQUEST PASSWORD RESET "; 	

				if(defined('CNF_MAIL') && CNF_MAIL =='swift')
				{ 
					Mail::send('user.emails.auth.reminder', $data, function ($message) {
			    		$message->to($to)->subject($subject);
			    	});	

				}  else {

							
					$message = view('user.emails.auth.reminder', $data);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);	
				}					
			
				
				$affectedRows = User::where('email', '=',$user->email)
								->update(array('reminder' => $request->input('_token')));
								
				return Redirect::to('administration')->with('message_reset', 'Please check your email')->with('msgstatus','success');	
				
			} 
			else {
				return Redirect::to('administration?reset')->with('message_reset', 'Cant find email address')->with('msgstatus','error');
			}

		}  else {
			return Redirect::to('administration?reset')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	 
	}	
	public function postRequested( Request $request)
	{

		$rules = array(
			'credit_email'=>'required|email'
		);	
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
			$invite = array();
		if($request->input('agree')=='model'){
			$user =  \DB::table('tb_models')->where('email','=',$request->input('credit_email'));
			$invite['url'] = 'user/reseted';
			$invite['type'] = 'XYX';

		}
		elseif($request->input('agree')=='agent'){
			$user =  \DB::table('tb_agency')->where('email','=',$request->input('credit_email'));
			$invite['url'] = 'user/reseted';
			$invite['type'] = 'YXY';

		}
			if($user->count() >=1)
			{
				$user = $user->get();

				$user = $user[0];


				if($request->input('agree')=='agent')
				{
					$invite['username'] = $user->username;
				}
				else{
					$invite['username'] = $user->firstname;

				}
			$invite['friend_email'] = $request->input('credit_email');
			$invite['password'] = $user->password;
			$invite['token'] = $request->input('_token');
			

			Mail::send('user.emails.auth.reminder',$invite ,function($message) use ($invite){
					$message->from('ramyabsetec@gmail.com');
					$message->to($invite['friend_email'])->subject('Password Reset');
					});

				$to = $request->input('credit_email');
				// print_r($to);exit;
				// $subject = "[ " .CNF_APPNAME." ] REQUEST PASSWORD RESET ";

				// 	Mail::send('user.emails.auth.reminder', $data, function ($message) {
			 //    		$message->to('ramyabsetec@gmail.com')->subject('dsf');
			 //    	});	
				if($request->input('agree')=='model'){
					$affectedRows = Modele::where('email', '=',$user->email)
					->update(array('reminder' => $request->input('_token')));

				}
				else{
					$affectedRows = Agency::where('email', '=',$user->email)
					->update(array('reminder' => $request->input('_token')));

				}
				
				return Redirect::to('/login')->with('message_reset', 'Please check your email')->with('msgstatus','success');	
				
			} 
			else{
			return Redirect::to('/login')->with('message_reset', 'Cant find email address')->with('msgstatus','error');	

			}
}
}
	public function getReseted( $token = '', $type='')
	{
		$sql = \DB::table('tb_pages')->where('default',1)->get();
		if(count($sql)>=1)
		{
			$row = $sql[0];

			$this->data['pageTitle'] 	= 'Reset Password';
					// $this->data['pageNote'] 	=  $row->note;
			$this->data['breadcrumb'] 	= 'inactive';	
			$this->data['pageMetakey'] 	=   '' ;
			$this->data['pageMetadesc'] =  '' ;
			$this->data['gplusimg'] = '';
			$this->data['tweetimg'] = '';
			$this->data['v_url'] = ''; 
			$this->data['urls'] = '';
			$this->data['fbimg'] = '';
			$this->data['links']=$this->option->get_options('links');
			$this->data['filename'] 	=  'user_remind';				

			if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
			{
				$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
			} else {
				$page_template = 'layouts.'.CNF_THEME.'.template.page';
			}

			$this->data['pages'] = 'user.user_remind';
					// $this->data['content'] = view('pages.feedbacksuggestion');
			$page = 'layouts.'.CNF_THEME.'.index';
			$users = Agency::where('reminder','=',$token);

			$user = Modele::where('reminder','=',$token);

			if($user->count() >=1)
			{
				$this->data['verCode']= $token;
				// $this->data['type']= 'model';
							// echo "<pre>";print_r($this->data['type']);

				return view($page,$this->data);			

				// return view('user.user_remind',$this->data);

			} 
			elseif($users->count() >=1)
			{
				$this->data['verCode']= $token;
				// $this->data['type']= 'agent';
							// echo "<pre>";print_r($this->data['type']);exit;

				return view($page,$this->data);			

				// return view('user.user_remind',$this->data);

			} 
			else {
				return Redirect::to('login')->with('message_reset', 'Cant find your reset code')->with('msgstatus','error');
			}

		}


	}
	
	public function postDoreseted( Request $request , $token = '')
	{
		$rules = array(
			// 'password'=>'required|alpha_num|between:6,12|confirmed',
			// 'password_confirmation'=>'required|alpha_num|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			if($request->input('type')=='XYX'){
			$user =  Modele::where('reminder','=',$token);
			if($user->count() >=1)
			{
				$data = $user->get();
				$user = Modele::find($data[0]->id);
				$user->reminder = '';
				$user->password =($request->input('password'));
				$user->save();	

			}
		}
			elseif($request->input('type')=='YXY'){
				$users =  Agency::where('reminder','=',$token);
			if($users->count() >=1)
			{
				$data = $users->get();
				$users = Agency::find($data[0]->id);
				$users->reminder = '';
				$users->password =($request->input('password'));
				$users->save();	
											// print_r($request->input('password'));exit;
		
			}

			}

			return Redirect::to('login')->with('message_reset','Password has been saved!')->with('msgstatus','success');
		} else {
			return Redirect::to('user/reseted/'.$token)->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}	
		

	
	public function getReset( $token = '')
	{
		if(\Auth::check()) return Redirect::to('dashboard');

		$user = User::where('reminder','=',$token);
		if($user->count() >=1)
		{
			$this->data['verCode']= $token;
			return view('user.remind',$this->data);

		} else {
			return Redirect::to('administration')->with('message_reset', 'Cant find your reset code')->with('msgstatus','error');
		}
		
	}	
	
	public function postDoreset( Request $request , $token = '')
	{
		$rules = array(
			'password'=>'required|alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			
			$user =  User::where('reminder','=',$token);
			if($user->count() >=1)
			{
				$data = $user->get();
				$user = User::find($data[0]->id);
				$user->reminder = '';
				$user->password = \Hash::make($request->input('password'));
				$user->save();			
			}

			return Redirect::to('administration')->with('message_reset','Password has been saved!')->with('msgstatus','success');
		} else {
			return Redirect::to('user/reset/'.$token)->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}	

	public function getLogout() {
		$currentLang = \Session::get('lang');
		\Auth::logout();
		\Session::flush();
		\Session::put('lang', $currentLang);
		return Redirect::to('/login')->with('message_reset', 'Your are now logged out!')->with('msgstatus','success');
	}

	function getSocialize( $social )
	{
		return Socialize::with($social)->redirect();
	}

	function getAutosocial( $social )
	{
		$user = Socialize::with($social)->user();
		$user =  User::where('email',$user->email)->first();
		return self::autoSignin($user);		
	}


	function autoSignin($user)
	{

		if(is_null($user)){
		  return Redirect::to('administration')
				->with('message', \SiteHelpers::alert('error','You have not registered yet '))
				->withInput();
		} else{

		    Auth::login($user);
			if(Auth::check())
			{
				$row = User::find(\Auth::user()->id); 

				if($row->active =='0')
				{
					// inactive 
					Auth::logout();
					return Redirect::to('administration')->with('message', \SiteHelpers::alert('error','Your Account is not active'));

				} else if($row->active=='2')
				{
					// BLocked users
					Auth::logout();
					return Redirect::to('administration')->with('message', \SiteHelpers::alert('error','Your Account is BLocked'));
				} else {
					Session::put('uid', $row->id);
					Session::put('gid', $row->group_id);
					Session::put('eid', $row->group_email);
					Session::put('fid', $row->first_name.' '. $row->last_name);	
					if(CNF_FRONT =='false') :
						return Redirect::to('dashboard');						
					else :
						return Redirect::to('');
					endif;					
					
										
				}
				
				
			}
		}

	}
	public function getAdminLogin()
	{
		return view('user.admin_login');
	}

	public function addToFavorites(Request $request)
	{
		$favourite_id = $request->favourite_id;
		$favourite_type = $request->favourite_type;
		$user_id    = \Session::get('logged_id');
        $user_type   = \Session::get('user_type');
        $response = array();
		$favorites_exist = \DB::table('tb_favorites')->where('favourite_id',$favourite_id )->where('favourite_type',$favourite_type )->where('user_id',$user_id )->where('user_type',$user_type )->first();

		if(!$favorites_exist){
			$data['favourite_id'] = $favourite_id;
            $data['favourite_type'] = $favourite_type;
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_type;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $favorites = \DB::table('tb_favorites')->insertGetId($data);

            $response = array(
				'status' => 'added',
				'msg'    => 'Added to your favourites',
			);
		
		}elseif($favorites_exist){
			\DB::table('tb_favorites')->where('id',$favorites_exist->id)->delete();
			$response = array(
				'status' => 'removed',
				'msg'    => 'Removed from your favourites',
			);
		}else{
			$response = array(
				'status' => 'error',
				'msg'    => 'Some error occurred. Please try again after some time.',
			);
		}

		return Response::json($response);
	}
	
}