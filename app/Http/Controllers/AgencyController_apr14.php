<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Agency;
use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class AgencyController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'agency';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Agency();
		$this->message = new Message();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'agency',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  


		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('agency');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];	
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('agency.index',$this->data);
		
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);


		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_agency'); 
		}
		$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		
		return view('agency.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		$row->country = \SiteHelpers::getName('country',$row->country);
		
		$row->our_categories =\SiteHelpers::getcolumnvalue('our_categories',$row->our_categories);
		$row->categories_needed =\SiteHelpers::getcolumnvalue('model_categories',$row->categories_needed);
		$row->experience_needed =\SiteHelpers::getcolumnvalue('experience_needed',$row->experience_needed);
		$row->our_industries =\SiteHelpers::getcolumnvalue('our_industries',$row->our_industries);


		// echo "<pre>";print_r($row->our_industries );exit;
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['prevnext'] = $this->model->prevNext($id);
			return view('agency.view',$this->data);
		} else {
			return Redirect::to('agency')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}	

	function postSave( Request $request)
	{
		
		//$rules = $this->validateForm();
		// $rules['contact_number'] = 'required|numeric';
		// $rules['mobile'] = 'required|numeric';
	$rules=array(
 // 'mobile' => 'required|numeric|min:11',
 // 'contact_number' => 'required|numeric|min:11',
 // 'profile_photo' => 'required',
 // 'agency_banner' => 'required',

		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			
			$data = $this->validatePost('tb_agency');
			$data['language'] = $request->input('language');
			$data['years_in_business'] = $request->input('years_in_business');
			$data['experience'] = $request->input('experience');

			if($request->get('categories_needed')){
			$categories_needed = implode(",", $request->get('categories_needed'));
			$data['categories_needed'] =$categories_needed;
			}
			if($request->get('our_categories')){
			$our_categories = implode(",", $request->get('our_categories'));
			$data['our_categories'] =$our_categories;
			//$data['experience_needed'] = $request->input('experience_needed');
			}
			if($request->get('genders_needed')){
			$genders_needed = implode(",", $request->get('genders_needed'));
			$data['genders_needed'] =$genders_needed;
			}
						//$data['our_industries'] =$request->input('our_industries');
			$data['traveling'] =$request->input('traveling');
			if($request->get('experience_needed')){

			$experience_needed=implode(',', $request->input('experience_needed'));
			$data['experience_needed']=$experience_needed;
			}
			if($request->get('our_industries')){

			$our_industries=implode(',', $request->input('our_industries'));
			$data['our_industries']=$our_industries;
			}

			$id = $this->model->insertRow($data , $request->input('id'));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'agency/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'agency?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('agency/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('agency?return='.self::returnUrl())
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('agency?return='.self::returnUrl())
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Agency();
		$info = $model::makeInfo('agency');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('agency.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('agency.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_agency');		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}	


	

	public function  getAgentRegister()
	{
			if(\Session::has('agent_id'))
		{
			return Redirect::to('agent-dashboard')->with('message',\SiteHelpers::alert('success',"You're already login"));
		} 
		elseif(\Session::has('model_id'))
		{
			return Redirect::to('')->with('message',\SiteHelpers::alert('success',"You're already login"));
		} 
		else {
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agents';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'ouragent';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				//$data['models'] = $this->model->where('status','1')->where('approved_status','1')->limit(6)->get();
				$data['country'] = \DB::table('tb_country')->get();

				$this->data['content'] = view('agency.agent_register',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
		}
	}
	}

	function postAgentSave( Request $request)
	{
		
		$rules = $this->validateForm();

		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_agency');
			
			$id = $this->model->insertRow($data , $request->input('id'));
			
			//Update status in Tell a friend 
			$encoded_id= $request->input('decoded_id');
			$decoded_id= base64_decode($encoded_id);
			$user = \DB::table('tb_tell_a_friend')->where('id', '=',$decoded_id )
			->where('status', '=', 'logged')
			->exists();
			if($user===1){
				\DB::table('tb_tell_a_friend')
			->where('id', $decoded_id )
			->update(array(
				'status' => 'logged',
				'friend_id' => $id,
				'user_type' => 'agent',
				'friend_name' => $request->input('username'),
				'registered' => date("Y-m-d H:i:s"),
				));
				
			}
			else{
				return Redirect::to('login');
			}
			
			if($request->input('decoded_id')!=''){
			$return = "/login?tellfriend=".$encoded_id;
			}
			else{
			$return = "/login";
			}
			
			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {
			// return Redirect::to('model/update/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			// ->withErrors($validator)->withInput();
		}	
	
	}

	public function postCheckuniqueemailagent(Request $request)
	{
		$email=\DB::table('tb_agency')->where('email',$request->input('email'))->get();
		if($email){
		return '"This email id is already taken."';		
		}	
		else{
		return 'true';
		}
	}
	public function  getAgentDash($id)
	{
		if(\Session::has('agent_id') )
			{	$agent_id =\Session::get('agent_id');
				$uid =\Session::get('model_id');

				$sql = \DB::table('tb_pages')->where('default',1)->get();
				if(count($sql)>=1)
				{
					$row = $sql[0];
					$type = 0;
					$label = 0;
					$page = 1;
					$params = array(
						'page'		=> $page ,
						'limit'		=>'' ,
						'sort'		=> 'id' ,
						'order'		=> 'desc',
						'params'	=> '',
						'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
					);

					$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
					$this->data['breadcrumb'] 	= 'inactive';	
					$this->data['pageMetakey'] 	=   '' ;
					$this->data['pageMetadesc'] = '';
					$this->data['filename'] 	=  'agent_dash';				

					if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
					{
						$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
					} else {
						$page_template = 'layouts.'.CNF_THEME.'.template.page';
					}

					$this->data['pages'] = $page_template;
					$data['agents'] = $this->getAgentid($id);
					$data['images'] = $this->getAgentid($id);

					// echo "<pre>";print_r($data['images']);exit;
					$data['results'] =\DB::table('tb_message')
										->where('recipient',$agent_id)
										->where('draft',0)
										->where('trashed_by_recipient',0)
										->where('deleted_by_recipient',0)
										->where('label_by_sender',NULL)
										->where('label_by_recipient',NULL)
										->where('model/agent',1)
										->limit(3)
										->orderBy('id','desc')
										->get();
					$data['videos'] =\DB::table('videos')->where('user_type','agent')->orderBy('id', 'desc')->first();
					
					// Count Total Views
            		$data['views'] = \DB::table('videos')->where('user_type','agent')->count();

					$users_data =  $this->message->getMessageUsers( $uid );
					$users = array(''=>'Select an User');
					$username = array();
					$usersNameList = array();
					$usersEmailList = array();
					$usersAvatar = array();
					foreach($users_data as $user_data){
						$users[$user_data->id] = $user_data->username;
						$usersNameList[$user_data->id] = $user_data->username;
						$username[$user_data->id] = $user_data->username;
						$usersEmailList[$user_data->id] = $user_data->email;
						$usersIdList[$user_data->id] = $user_data->id;
						$usersAvatar[$user_data->id] = $user_data->profile_photo;
					}
					$data['usersNameList'] = $usersNameList;
					$data['usersAvatar'] = $usersAvatar;

					$this->data['content'] = view('agency.agent_dash',$data);
					$page = 'layouts.'.CNF_THEME.'.index';


					return view($page,$this->data);			
				}
			}
			else{
				return redirect::to('/login');
			}
		}
	public function  getAgentprofile($id)
	{
			
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['agents'] = $this->model->where('id',1)->where('status','1')->where('verification_status','1')->get();
				$data['images'] = $this->getAgentid($id);
				$row = $this->model->getRow($id);
				$row->country = \SiteHelpers::getName('country',$row->country);
				$row->genders_needed  = str_replace(",", "<br>", $row->genders_needed);

		$row->our_categories =\SiteHelpers::getcolumnvalue('our_categories',$row->our_categories);
		$row->categories_needed =\SiteHelpers::getcolumnvalue('model_categories',$row->categories_needed);
		$row->experience_needed =\SiteHelpers::getcolumnvalue('experience_needed',$row->experience_needed);
		$row->our_industries =\SiteHelpers::getcolumnvalue('our_industries',$row->our_industries);


		if($row)
		{
			$data['row'] =  $row;
		}	
				$this->data['content'] = view('agency.agent_profile',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
			
	}
	public function  getAgentProfileEdit(Request $request, $id = null)
	{
		if(\Session::has('agent_id'))
		{
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_profile_edit';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['agents'] = $this->model->where('id',1)->where('status','1')->where('verification_status','1')->get();
				$data['images'] = $this->getAgentid($id);
				$row = $this->model->find($id);
				if($row)
						{
							$data['row'] =  $row;
						}

				$this->data['content'] = view('agency.agent_profile_edit',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		}
		else{
			return Redirect::to('login');
					
		}
		
	}
	public function  postAgentprofileSave(Request $request)
	{

		$rules = $this->validateForm();
		
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_agency');
			$data['language'] = $request->input('language');
			$data['years_in_business'] = $request->input('years_in_business');
			$data['experience'] = $request->input('experience');

			if($request->get('categories_needed')){
			$categories_needed = implode(",", $request->get('categories_needed'));
			$data['categories_needed'] =$categories_needed;
			}
			if($request->get('our_categories')){
			$our_categories = implode(",", $request->get('our_categories'));
			$data['our_categories'] =$our_categories;
			//$data['experience_needed'] = $request->input('experience_needed');
			}
			if($request->get('genders_needed')){
			$genders_needed = implode(",", $request->get('genders_needed'));
			$data['genders_needed'] =$genders_needed;
			}
						//$data['our_industries'] =$request->input('our_industries');
			$data['traveling'] =$request->input('traveling');
			if($request->get('experience_needed')){

			$experience_needed=implode(',', $request->input('experience_needed'));
			$data['experience_needed']=$experience_needed;
			}
			if($request->get('our_industries')){

			$our_industries=implode(',', $request->input('our_industries'));
			$data['our_industries']=$our_industries;
			}
			$data['id']=$request->input('ids');
			$data['address']=$request->input('formatted_address');
			$data['city']=$request->input('locality');
			$data['zipcode']=$request->input('postal_code');
			$data['state']=$request->input('administrative_area_level_1');
			$data['country']=$request->input('country');
			$data['latitude']=$request->input('lat');
			$data['longitude']=$request->input('lng');

			// echo "<pre>";print_r($_POST);exit;
			$id = $this->model->insertRow($data , $request->input('ids'));
			
		return Redirect::to('agent-profile-edit/'.$request->input('ids'))->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
	}
		
		
	}
	public function  getOuragency(Request $request, $id = null)
	{
		
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_profile_edit';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$rowperpage = 6;

				$data['agencies'] = $this->model->where('status','1')->where('verification_status','1')->limit($rowperpage)->get();				
				$photos = $this->model->where('status','1')->where('verification_status','1')->lists('profile_photo')->toArray();

				$data['count'] = count($photos);
				
				$this->data['content'] = view('agency.ouragencies',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		
		
	}
	 public function postAgentLoadDataAjax(Request $request)
    {
			$row = $_POST['row'];
			$rowperpage = 6;

			// selecting posts
			$query =  \DB::table('tb_agency')->where('status',1)->where('verification_status',1)
			                ->offset($row)
			                ->limit($rowperpage)
			                ->get();

			$html = '';

			foreach ($query as  $value) {
				# code...
				$url = url('uploads/agency_profile/'.$value->profile_photo); 
			$href=url('agent-profile/'.$value->id);
			$html .= '<a href='.$href.'>';
			$html .= '<div class="model_item agent_item" id="'.$value->id.'">';
			$html .= '<img src="'.$url.'" >';
			$html .= ' <div class="colorbox">';
			$html .= '   <div class="inner_shades">';
			$html .= '     <h4>'.$value->agency_name.' </h4>';
			$html .= ' <p>'.$value->username.' '.\SiteHelpers::getName('country',$value->country).'</p>';
			$html .= '</div>';
			$html .=  ' </div>';
			$html .= ' </div>';
			$html .= ' </a>';

			    // Creating HTML structure
			    // $html .= '<div id="'.$value->id.'" class="post">';
			    // $html .= '<img src="'.$url.'" >';
			   
			    // $html .= '</div>';

			}

			echo $html;
    }
    public function  getAgentmembership(Request $request, $id = null)
	{
		if(\Session::has('agent_id'))
		{
		
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Models';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'ourmodel';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
				$data['images'] = $this->getAgentid($id);
				$data['membershipplan'] = \DB::table('tb_agency')
										    ->select('tb_membership_plan.plan_name','tb_membership_plan.plan_amount')
										    ->join('tb_membership_plan', 'tb_membership_plan.plan_id', '=', 'tb_agency.plan_id')
										    ->get();
				$data['memberships'] = \DB::table('tb_membership_plan')->get();

				//echo "<pre>";print_r($leagues);exit;
				$this->data['content'] = view('agency.agent_membership',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		}
	}
	 public function  getFind_best(Request $request)
	{
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Best Agent & Models';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'best_agent_model';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
			
				$this->data['content'] = view('agency.best_agent_model');
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
	}
	public function  getAgentEvents()
	{
		
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
				$this->data['content'] = view('agency.agent_events',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		
	}
	public function  getAgentEventsCreate()
	{
		
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
				$this->data['content'] = view('agency.events_create',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		
	}
	public function  getAgentEventsDetail()
	{
		
		 $sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;

				$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
				$this->data['content'] = view('agency.agent_video',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		
	}
	public function  getAgentUserVideo($id)
	{
		$sql = \DB::table('tb_pages')->where('default',1)->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];

				$this->data['pageTitle'] 	= 'Our Agent';
				// $this->data['pageNote'] 	=  $row->note;
				$this->data['breadcrumb'] 	= 'inactive';	
				$this->data['pageMetakey'] 	=   '' ;
				$this->data['pageMetadesc'] = '';
				$this->data['filename'] 	=  'agent_dash';				

				if(file_exists(base_path().'/resources/views/layouts/'.CNF_THEME.'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.CNF_THEME.'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.CNF_THEME.'.template.page';
				}

				$this->data['pages'] = $page_template;
				$data['images'] = $this->getAgentid($id);
				$data['videos'] =\DB::table('videos')->where('user_type','agent')->orderBy('id', 'desc')->paginate(8);

				$data['agents'] = $this->model->where('id',\Session::get('agent_id'))->where('status','1')->where('verification_status','1')->get();
				$this->data['content'] = view('agency.agent_video',$data);
				$page = 'layouts.'.CNF_THEME.'.index';


				return view($page,$this->data);			
			}
		
	}

	function getAgentid($id) {
		return $this->model->where('id',$id)->where('status','1')->where('verification_status','1')->get();
	}

}