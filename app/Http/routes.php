<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$active_multilang = defined('CNF_MULTILANG') ? CNF_LANG : 'en'; 
 \App::setLocale($active_multilang);
 if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

    $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
    \App::setLocale($lang);
}   

//Route::get('/', 'HomeController@index');
Route::get('/', function () {
    return redirect('index');
});

Route::get('/index', 'HomeController@landing');
Route::get('/home', 'HomeController@index');
Route::controller('home', 'HomeController');
Route::get('/aboutus', 'HomeController@getAbout');


Route::get('/contactus', 'HomeController@getContactus');
Route::post('/save/contactus', 'HomeController@postSaveContactus');

//Tell a Friend
Route::get('/tellfriend', 'HomeController@getTellfriend');
Route::post('/invite/friend', 'HomeController@postInvitefriend');
Route::get('/tell_a_friend', 'HomeController@getInvitedfriends');

Route::get('/terms', 'HomeController@getTerms');
Route::get('/privacy-policy', 'HomeController@getPrivacyPolicy');
// Route::get('/feedback', 'HomeController@getFeedback');
Route::get('/advertise', 'HomeController@getAdvertisement');
// events
Route::get('/events/list', 'EventsController@getEventsList');
Route::get('/my/events'  , 'EventsController@getMyEventsList');

// Route::get('/event/fields' ,'EventsController@getEventfieldUpdate');
// Route::post('/event/fields/save' ,'EventsController@postEventfieldSave');
// Route::get('/event/form', 'EventsController@getFields');
Route::get('event/fields', 'EventsController@getFields');

Route::get('event/fields/update/{id?}', 'EventsController@getEventUpdate');
Route::get('event/fields/show/{id?}', 'EventsController@getEventShow');
Route::post('event/fields/save', 'EventsController@postEventSave');
Route::post('event/fields/delete','EventsController@postEventDelete');
Route::post('/event/fields/filter','EventsController@postEventFilter');
Route::post('/event/fields/multisearch','EventsController@postEventMultisearch');
Route::post('/event/fields/delete/{id?}','EventsController@postDeleteEvent');

Route::get('/blogs/list', 'BlogsController@getBlogList');
Route::get('/blogs/article/{title}/{id}', 'BlogsController@getView');

//Blogs
Route::controller('blogs','BlogsController');

//Events
Route::controller('events','EventsController');

Route::get('/bloges', 'BlogController@getBlog');


Route::get('/blog-details/{id}', 'BlogController@getBlogDetails');

//FaqController
Route::get('/faqs', 'FaqController@getFaqs');
/*Feedback*/
Route::get('/feedback', 'HomeController@getFeedback');
Route::get('/feedback/view/{id}', 'HomeController@showFeedback');
Route::post('/feedback-form', 'HomeController@postFeedbackform');
Route::get('/feedback-list', 'HomeController@getSuggestedlist');


Route::get('/contactus-list', 'HomeController@getContactuslist');
Route::get('/contactus/view/{id}', 'HomeController@showContactus');

Route::get('/cookie/set/{usertype}/{email}/{password}','CookieController@setCookie');
Route::get('/cookie/delete','CookieController@deleteCookie');
//Membership
Route::get('/membership-plan', 'MembershipController@getMembershipPlan');
Route::get('/payment', 'MembershipController@getPayment');
Route::get('/membership/payment/{id}', 'MembershipController@getPaymentform');
Route::get('/membership/membershipCron', 'MembershipController@membershipCron');

// ModelController
Route::get('/models', 'ModelController@getOurmodel');
Route::post('/demos/loaddata', 'ModelController@postLoadDataAjax');

Route::post('/model/create', 'ModelController@postModelRegister');
Route::get('/model/create', 'ModelController@getModelRegister');
Route::post('/model/signup', 'ModelController@postModelSave');
Route::post('/model/checkuniqueemail', 'ModelController@postCheckuniqueemail');
Route::post('/model/checkaccount', 'ModelController@postCheckactiveaccount');
Route::post('/model/checkuniqueemailadmin', 'ModelController@postCheckuniqueemailadmin');//check email for admin



Route::get('/model/filter', 'ModelController@getModelfilter');
Route::post('/model/filterSearch', 'ModelController@getModelfiltersearch');
Route::post('/model/filterajax', 'ModelController@getModelfilterAjax');

Route::get('/model-dashboard/{id}', 'ModelController@getModelDash');
Route::get('/model-profile/{id}', 'ModelController@getModelprofile');
Route::post('/model-profile/save', 'ModelController@postModelprofileSave');
Route::get('/model-membership/{id}', 'ModelController@getModelmembership');

Route::post('/model/save/profile', 'ModelController@postSaveModelProfile');
Route::post('/model/save/Cover', 'ModelController@postSaveProfileCover');

//Route::get('/model-message', 'ModelController@getModelMessage');
Route::get('/model-profile/{name}/{id}', 'ModelController@getModelMyProfile');

Route::get('/models-listing', 'ModelController@getModelsListing');
Route::get('/model-gallery', 'ModelController@getModelsGallery');
Route::get('/model_video/{id}', 'ModelController@getModelVideo');

Route::post('/add/album', 'ModelController@postAddAlbum');

Route::get('/buy-membership-plan', 'MembershipController@getBuyMembershipPlan');
/*Forget password*/
Route::post('/user/email/request', 'UserController@postRequested');
Route::get('user/reseted', 'UserController@getReseted');
Route::post('/user/doreseted/', 'UserController@postDoreseted');

//newsletter
Route::post('/newsletter/subscribe', 'HomeController@postNewsletter');
Route::post('newsletter/checkuniqueEmail', 'HomeController@postCheckSubscribeEmail');


//transactions
Route::get('transactions', 'TransactionsController@getIndex');
Route::get('transactions/agent', 'TransactionsController@getAgentTrans');// for agent transactions
Route::get('transactions/model', 'TransactionsController@getModelTrans');// for model transactions
// sample
Route::post('/user/save/profile/avatar', 'ModelController@postModelAvatar');
Route::post('/user/save/model/image', 'ModelController@postModelGalleryImage');

// AgentController

Route::post('agent/checkuniqueName', 'AgencyController@postCheckuniqueNameagent');
Route::get('/agencies', 'AgencyController@getOuragency');
Route::post('/agency/loaddata', 'AgencyController@postAgentLoadDataAjax');

Route::get('/agent/create', 'AgencyController@getAgentRegister');
Route::post('/agent/signup', 'AgencyController@postAgentSave');
Route::post('/agent/checkuniqueemail', 'AgencyController@postCheckuniqueemailagent');
Route::post('/agent/checkuniqueemailadmin', 'AgencyController@postCheckuniqueemailagentadmin');//check email for admin

Route::post('/unique_alias', 'AgencyController@postCheckuniquealias');
Route::post('/unique_title', 'AgencyController@postCheckuniquetitle');

Route::get('/agent-dashboard/{id}', 'AgencyController@getAgentDash');
Route::get('/agent-profile/{name}/{id}', 'AgencyController@getAgentprofile');
Route::get('/agent-profile-edit/{id}', 'AgencyController@getAgentProfileEdit');
Route::post('/agent-profile/save', 'AgencyController@postAgentprofileSave');
Route::get('/agent-membership/{id}', 'AgencyController@getAgentmembership');

Route::get('/favourite-videos/{id}', 'AgencyController@getFavouriteVideos');
Route::get('/favourite-photos/{id}', 'AgencyController@getFavouritePhotos');
Route::get('/favourite-models/{id}', 'AgencyController@getFavouriteModels');
Route::get('/favourite-agents/{id}', 'AgencyController@getFavouriteAgents');

Route::get('/find_best_agent_models', 'AgencyController@getFind_best');

Route::get('/agent_events', 'AgencyController@getAgentEvents');
Route::get('/agent_events_detail', 'AgencyController@getAgentEventsDetail');

Route::get('/create_event', 'AgencyController@getAgentEventsCreate');
Route::get('/agent_video/{id}', 'AgencyController@getAgentUserVideo');

Route::post('/rating_ajax', 'AgencyController@postRating_ajax');
Route::get('/agent-network/{id}', 'AgencyController@getAgentNetwork');
Route::get('/model-network/{id}', 'ModelController@getModelNetwork');

//send mail for events/images/videos
Route::get('/agentmail/{product}', 'AgencyController@sendMail');

//Blogs controller
Route::get('/blog_articles', 'BlogsController@getBlog_articles');

// Video module
Route::get('/upload', 'UploaderController@index');
Route::post('/upload', 'UploaderController@StartUpload');

Route::get('/agent/video/{vid}', 'VideoController@getAgentVideo');
Route::get('/model/video/{vid}', 'VideoController@getModelVideo');


Route::get('/agentConfirmAccount/{email}/{pwd}', 'AgencyController@agentConfirmAccount');
Route::get('/modelConfirmAccount/{email}/{pwd}', 'ModelController@modelConfirmAccount');


Route::get('/video/{vid}', 'VideoController@watch');
Route::get('/videos', 'VideoController@index');
Route::post('/video/loaddata', 'VideoController@postVideoLoadData');
Route::post('/comment/{vid}', 'VideoController@comment');
Route::get('/like', 'VideoController@like');
Route::get('/dislike', 'VideoController@dislike');
Route::get('/video/tag/{tag}', 'VideoController@tags');
Route::get('/embed/{vid}', 'VideoController@embed');
Route::get('/video/delete/{vid}', 'VideoController@DeleteVideo'); // added to delete video

// Photos Module
Route::get('/photo/upload', 'UploaderController@getPhotoUpload');
Route::get('/photo/album/{iAlbumId}', 'UploaderController@getPhotosAlbum');
Route::get('/photo/{uri}', 'UploaderController@photoView');
Route::post('/photo/comment/{id}', 'UploaderController@PhotoComment');
Route::get('/like/photo', 'UploaderController@PhotoLike');
Route::get('/dislike/photo', 'UploaderController@PhotoDislike');
Route::get('/tag/photo/{Tags}', 'UploaderController@getPhotoTags');
Route::get('/album/model', 'UploaderController@getModelPhotoAlbum');
Route::get('/album/agent', 'UploaderController@getAgentPhotoAlbum');
Route::get('/agent/album/{id}', 'VideoController@getAgentAlbum');
Route::get('/model/album/{id}', 'VideoController@getModelAlbum');
Route::get('/photo/albumdelete/{id}', 'UploaderController@getDeleteAlbum'); // added to delete album
Route::get('/photo/photodelete/{id}', 'UploaderController@getDeletePhoto'); // added to delete Image
Route::post('/album/checkuniquename', 'UploaderController@postCheckuniqueAlbumname');

//Message

	Route::get('message', 'MessageController@getIndex');
	Route::post('messages/mfilter', 'MessageController@postMessageFilter');
	Route::post('messageupdate', 'MessageController@postMessageUpdate');
	Route::post('messagegroupupdate', 'MessageController@postGroupMessageUpdate');
	Route::post('messagesend', 'MessageController@postSave');
	Route::post('messagelabelsave', 'MessageController@postLabelSave');
	Route::post('messagelabeldelete', 'MessageController@postLabelDelete');
	Route::get('auto/model', 'MessageController@getAjaxData');
	Route::get('auto/agent', 'MessageController@getAutoData');
	Route::post('delete/draft', 'MessageController@postDraftdelete');
	Route::get('autocomplete', 'MessageController@getAutoComplete');

Route::post('/user/resume/save/{uid}', 'MessageController@postResumeSave');
Route::post('/user/ppt/save/{uid}', 'MessageController@postPPTSave');

// UserController
Route::get('/admin_login', 'UserController@getAdminLogin');

Route::post('/addToFavorites', 'UserController@addToFavorites');

Route::get('/administration', 'UserController@getLogin');
Route::get('/login', 'UserController@getModelAgentLogin');
Route::post('/model-agent/signin', 'UserController@postModelAgentsignin');
Route::controller('/user', 'UserController');


include('pageroutes.php');
include('moduleroutes.php');

Route::get('/restric',function(){

	return view('errors.blocked');

});

Route::resource('sximoapi', 'SximoapiController'); 
Route::group(['middleware' => 'auth'], function()
{

	Route::get('core/elfinder', 'Core\ElfinderController@getIndex');
	Route::post('core/elfinder', 'Core\ElfinderController@getIndex'); 
	Route::controller('/dashboard', 'DashboardController');
	Route::controllers([
		'core/users'		=> 'Core\UsersController',
		'notification'		=> 'NotificationController',
		'post'				=> 'PostController',
		'core/logs'			=> 'Core\LogsController',
		'core/pages' 		=> 'Core\PagesController',
		'core/groups' 		=> 'Core\GroupsController',
		'core/template' 	=> 'Core\TemplateController',
		'core/posts'		=> 'Core\PostsController',
		'core/forms'		=> 'Core\FormsController'
	]);

});	

Route::group(['middleware' => 'auth' , 'middleware'=>'sximoauth'], function()
{
	Route::get('magelur/config/footer-options', 'HomeController@getLinks');
	Route::get('magelur/config/payment-settings', 'HomeController@getPaymentsettings');
	Route::post('updatelinks', 'HomeController@postUpdatelinks');
	Route::post('updatePaymentsettings', 'HomeController@postUpdatePaymentsettings');

	Route::controllers([
		'magelur/menu'		    => 'magelur\MenuController',
		'magelur/config' 		=> 'magelur\ConfigController',
		'magelur/module' 		=> 'magelur\ModuleController',
		'magelur/tables'		=> 'magelur\TablesController',
		'magelur/code'		    => 'magelur\CodeController'
	]);			



});





