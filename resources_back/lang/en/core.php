<?php

return array(
// General 
	"norecord" => "No Record Found",
	"create" => "Create New",

	// General , Login Info & Signup
	"home" => "Home",
	"group" => "Group",
	"username" => "Username",
	"email" => "Email Address",
	"password" => "Password",
	"repassword" => "Confirm Password",
	"forgotpassword" => "Forgot Password",
	"newpassword" => "New Password",
	"conewpassword" => "Confirm Password",
	"notepassword" => "Leave blank if you don't want to change current password", // updated apidevlab	
	"submit" => "Submit",
	"signin" => "Sign In",
	"signup" => "Sign Up",
	"subscribe" => "Subscribe",
	"language" => "Language",
	"subscription" => "Subscription",
	"firstname" => "First Name",
	"lastname" => "Last Name ",
	"lastlogin"	=> "Last Login",
	"personalinfo"	=> "Personal Info",
	"changepassword"	=> "Change Password",
	"registernew" => "Register New Account ",
	"backtosite" => " Back To Site ",
	"planexpiration" => "Plan Expiration",
	'invitationmail' => 'Invitation Mail',

	// Events
	"event" => "Event",
	"add_events" => "ADD EVENTS",
	"events" => "Events",
	"read_more" => "Read more",
	"check_link" => "Check This Link",
	"share" => "Share",
	"start" => "Start",
	"end" => "End",
	"location" => "Location",
	"organizer" => "Organizer",
	"submit_comment" => "Submit Comment",
	"up_events" => "Upcoming Event",
	"share_exp" => "Share experiences",
	"with_frnd" => "with your friends",
	"learn_more" => "Learn More",
	"am_model" => "I am a Model",
	"am_agent" => "I am an Agent",
	"create_new_event" => "Create a new event",
	"details" => "Details",
	"contact_num" => "Contact Number",
	"cover_banner" => "Cover Banner",
	"event_link" => "Event Link",
	"address_place" => "Enter Your Address",
	"address" => "Address",
	"city" => "City",
	"zip" => "Zip Code",
	"country" => "Country",
	"state" => "State",
	"tags" => "Tags",
	"video_embed" => "Video Embed",
	"event_news" => "event news",
	"carve_events" => "Crave - Event",
	"edit_event" => "Edit Event",
	
	// Blogs
	"blog" => "Blog",
	"category" => "Category",
	"author" => "Author",
	"comment(s)" => "comment(s)",
	"no_tags" => "No More Tags",
	"share_fb" => "Share",
	"tweet_fb" => "Tweet",
	"read_article" => "Read article",
	"articles_in" => "Articles In",
	"widget_categories" => "Categories",
	"latest_post" => "Latest Post",
	"tag_cloud" => "Tag Cloud",
	"comment_place" => "Enter Your Comments",
	"live_in" => "Live in",

	// Tell a friend
	"your_name" => "Your Name",
	"your_email" => "Your Email",
	"friend_email" => "Friend Email",
	"friend_name" => "Friend Name",
	"description" => "Description",
	

	
	
/* grid , pagination */
	"grid_displaying" 	=> "Displaying",
	"grid_to" 			=> "To",
	"grid_of" 			=> "Of",
	"grid_show" 			=> "Show",
	"grid_sort" 			=> "Sort",
	"grid_order" 			=> "Order",	
	"grid_page" 			=> "Page",	
		

/* Menu navigation here */
	"m_controlpanel"	=> "Control Panel",
	"m_dashboard" 		=> "Dashboard",
	"m_setting" 		=> "Settings", // updated apidevlab
	"m_usersgroups" 	=> "Users & Groups",
	"m_users" 			=> "Users",
	"m_groups" 			=> "Groups",
	"m_pagecms" 		=> "Page CMS",
	"m_menu" 			=> "Menu Management",
	"m_logs" 			=> "Activity Logs",
	"m_codebuilder" 	=> "Code Generator",
	"m_blastemail" 		=> "Blast Email",
	"m_myaccount" 		=> "My Account",
	"m_logout" 			=> "Logout",
	"m_profile" 		=> "Profile",
	"m_manual" 		=> "Manual Guide ",

/* Setting page translation */	

	"t_generalsetting"		=> "General Settings", //updated apidevlab
	"t_generalsettingsmall"	=> "Manage Settings Configuration", // updated apidevlab
	"t_blastemail"			=> "Blast Email", // updated apidevlab
	"t_blastemailsmall"		=> "Send Bulk Email",
	"t_emailtemplate"		=> "Email Templates",
	"t_emailtemplatesmall"	=> "Manage Email Templates", // updated apidevlab
	"t_loginsecurity"		=> "Login & Security", // updated apidevlab
	"t_loginsecuritysmall"	=> "Manage Logins & Security",	// updated apidevlab	
	"t_socialmedia"			=> "Social Media Login", // updated apidevlab
	"t_lfb"					=> "Login via Facebook", // updated apidevlab
	"t_lgoogle"				=> "Login via Google", // updated apidevlab
	"t_ltwit"				=> "Login via Twitter", // updated apidevlab
	"tab_siteinfo"			=> "General Settings", // Site Info updated apidevlab
	"tab_loginsecurity"		=> "Login & Security",
	"tab_email"				=> "Email Templates", // updated apidevlab
	"tab_translation"			=> "Translation", 
	"fr_appname"			=> "Application Name ",
	"fr_appdesc"			=> "Application Desc",
	"fr_comname"			=> "Company Name ",
	"fr_emailsys"			=> "Email System ",
	"fr_emailmessage"		=> "Email Message ",
	"fr_enable"				=> "Enable",
	"fr_multilanguage"			=> "Muliti language",
	"fr_mainlanguage"			=> "Main Language",
	"fr_fronttemplate"			=> "Frontend Template",
	"fr_appmode"			=> "Application Mode",
	"fr_appid"				=> "APP ID",
	"fr_secret"				=> "SECRET NUMBER",
	"fr_registrationdefault"		=> "Default Group Registration ",
	"fr_registrationsetting"	=> "Registration Setting",
	"fr_registration"		=> "Registration",
	"fr_allowregistration"		=> "Allow Registration",
	"fr_allowfrontend"			=> "Allow Frontend",
	"fr_registrationauto"		=> "Automatic activation ",
	"fr_registrationmanual"		=> "Manual activation ",
	"fr_registrationemail"		=> "Email with activation link ",	
	"fr_emailsubject"				=> "Subject",
	"fr_emailsendto"				=> "Send To",
	"fr_emailmessage"				=> "Email Message",
	"fr_emailtag"					=> "You Can Use",
	"fr_album"					=> "Album Image Limit",
	
	
/* submit */
	"sb_savechanges"			=> "Save Changes",
	"sb_send"					=> "Send",
	"sb_save"					=> "Save",
	"sb_apply"					=> "Apply Change(s)",
	"sb_submit"					=> "Submit",
	"sb_cancel"					=> "Cancel",	
	
/* button */
	"btn_back"						=> "Back",	
	"btn_action"					=> "Action",	
	"btn_search"					=> "Search",	
	"btn_clearsearch"				=> "Clear Search",	
	"btn_download"					=> "Download",	
	"btn_config"					=> "Configuration",	
	"btn_copy"						=> "Copy", // Ajax 
	"btn_print"						=> "Print",	
	"btn_create"					=> "Create",	
	"btn_install"					=> "Install",
	"btn_backup"					=> "Backup",
	"btn_remove"					=> "Remove",
	"btn_edit"						=> "Edit",	
	"btn_view"						=> "View",
	"btn_typesearch"				=> "Type & Enter",	// updated apidevlab	
	
/* Core Module */
	"t_menu"						=> "Menu Management",
	"t_menusmall"					=> "List Of All Menu",
	"t_tipsdrag"					=> "Drag and Drop to reorder menu list", // updated apidevlab
	"t_tipsnote"					=> "Note!, Menus only support 3 levels ", // updated apidevlab
	"tab_topmenu"					=> "Top Menu",
	"tab_sidemenu"					=> "Side Menu ",
	"sb_reorder"					=> "Reorder Menu",	
	"fr_mtitle"						=> "Name / Title ",	
	"fr_mtype"						=> "Menu Type",	
	"fr_mposition"					=> "Position",	
	"fr_mactive"					=> "Active",	
	"fr_minactive"					=> "Inactive",
	"fr_maccess"					=> "Access", // updated apidevlab	
	"fr_miconclass"					=> "Icon Class",
	"fr_mpublic"					=> "Public",
	"fr_mexample"					=> "Example",
	"fr_musage"						=> "Useage", // updated apidevlab	
	
/* Code BuilderModule */
	"t_module"						=> "Module",
	"t_modulesmall"					=> "List Of All Modules",// updated apidevlab
	"tab_installed"					=> "Installed Modules", // updated apidevlab
	"tab_core"						=> "Core Modules", // updated apidevlab
	"fr_modtitle"					=> "Module Name / Title",
	"fr_modnote"					=> "Module Note",
	"fr_modtable"					=> "Module Table",
	"fr_modautosql"					=> "Auto Mysql Statment",
	"fr_modmanualsql"				=> "Manual Mysql Statment ",
	"fr_createmodule"				=> "Create New Module",
	"fr_installmodule"				=> "Install Module",
	"fr_backupmodule"				=> "Backup and make installer for modules ",

/* dashboard Interface */

	"dash_i_module"					=> "Module",
	"dash_i_setting"				=> "Setting",
	"dash_i_sitemenu"				=> "Site Menu",
	"dash_i_usergroup"				=> "User & Group",
	"dash_module"					=> "Manage Existing Modules or Create new one",
	"dash_setting"					=> "Setting Up your application login option , sitename , email etc. ",
	"dash_sitemenu"					=> "Manage Menu for your application frontend or backend",
	"dash_usergroup"				=> "Manage groups and users and grant what module and menu are accesible",

/*updates	on may ,5 2014 */
	
	"loginsocial"				=> "Login via Social Networking", // updated apidevlab
	"enteremailforgot"			=> "Enter Your Email Address",
	"detail" 					=> "View Detail",
	"addedit" 					=> "Add - Edit",
	
/* Notification */
	"note_noexists"				=> "Sorry, The Page Does Not Exist!", // updated apidevlab
	"note_restric"				=> "Sorry, You are not allowed to access this page!", // updated apidevlab
	"note_success"				=> "Saved successfully!", // updated apidevlab
	"note_error"				=> "The following errors occurred !",
	"note_success_delete"		=> "Removed successfully!",	// updated apidevlab
	"connect_stripe_note"		=> "You need to connect to stripe first.",
	"accept_payment"			=>"Member is not able to accept payment right now.",

	/*Validation*/

	//Tell a friend
	"invalid_email"			    =>"Invalid Email address",
	"fill_friend_email"			=>"Please fill your Friend email",
	"req_email"			        =>"Email field is required",
	"req_description"		    =>"Description  is required",
	"req_name"		   		    =>"Name is required",
	"req_fried_name"		   	=>"Friend Name is required",
	
	
	// Membership
	"plan_type"  				=> "Plan Type",

    // Upload Video Page
    'unlimited'           => 'Unlimited',
    'max_file_size'       => 'THE MAXIMUM FILE SIZE IS',
    'upload_from_youtube' => 'Upload from YouTube (Just ID of Video)',
    'fetch_video'         => 'Fetch Video',
    'video_title'         => 'Video Title',
    'video_tags'          => 'Video Tags',
    'select_category'     => 'Select Category',
    'video_description'   => 'Video Description',
    'upload_cover'        => 'Upload cover',
    'start_uploading'     => 'Start Uploading',

    // Watch Video Page
    'uploaded_on'         => 'Uploaded on',
    'share_video'         => 'Share Video',
    'share_your_video'    => 'Share Your Video',
    'flag_video'          => 'Flag Video',
    'show_more'           => 'Show more',
    'comments'            => 'Comments',
    'add_comment'         => 'Add Comment',
    'post_comment'        => 'Post Comment',
    'no_comment'          => 'Oops! No comments right now.',
    'no_related'          => 'Oops! No related videos right now.',
    'video_gallery'       => 'Video Gallery',
    'agent_video'         => 'AGENCY VIDEO',
    'model_video'         => 'MODEL VIDEO',
    'add_videos'          => 'ADD VIDEOS',
    'vip_users'           => 'VIP Users',
    'verified'       => 'Verified',

    
    	
    	

     // Photo Page
 	'share_your_photo'    => 'Share Your Photo',
	

);