@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
		
	<h1>Testimonial</h1>
		<div class="sbox-tools" >
			<!-- @if($access['is_excel'] ==1)
			<a href="{{ URL::to('testimonial/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i></a>
			@endif

			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif  -->
		</div>
	</div>

	<div class="sbox-content"> 	
	<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('testimonial/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'testimonial/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
		<!-- 	@if($access['is_excel'] ==1)
			<a href="{{ URL::to('testimonial/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif
 -->
		</div>

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'testimonial/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Image' || $t['label']=='Sort Order' ||$t['label']=='Id' || $t['label']=='Created Date' || $t['label']=='Last Updated')
							{{--*/ $t['label']=" " /*--}}
							@elseif($t['label']=='Title')
								{{--*/ $t['label']="Title(Posted_By) " /*--}}
							@endif
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody>  
<tr id="orderDetails">
        		<td class="number"></td>
        		<td></td>
        		<td></td>

        		<td id="title" class="fieldsearch"> <div class="select-style">{!! Form::text('title',"{$title}",array('class'=>'form-control', 'placeholder'=>'Title', )) !!} </div></td>
        		<td id="description" class="fieldsearch"> <div class="select-style">{!! Form::text('description',"{$description}",array('class'=>'form-control', 'placeholder'=>'Description', )) !!} </div></td>

        		<td id="status" class="fieldsearch"> 
        			<div class="select-style">
        				<select name='status' class = 'select2 '>
        					<option value="">Select option</option>
        					<option value="1" {{ $status == '1' ? 'selected' : ''}}>Active</option>
        					<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>

        				</select>

        			</div>
        		</td>
        		<td></td>
        		<td class="icons">
        			<input type="hidden"  value="Search">
        			<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
        			<a href="{{ url('testimonial') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
        		</td>
        	</tr>       						
              						
             {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('testimonial/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('testimonial/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->
{{--*/ $i++ /*--}}
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>	
 @if($field['field']=='image' || $field['field']=='slug' || $field['field']=='id' || $field['field']=='parent_id' || $field['field']=='created_date' || $field['field']=='last_updated' )
								{{--*/$field['field']=='';/*--}}
								@elseif($field['field']=='status' )
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
								@elseif($field['field']=='description')
								{!! ($row->description) !!}
						 	@else			 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
						 @endif
						 </td>
						@endif	
					 @endif					 
				 @endforeach
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('testimonial/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('testimonial/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>			 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>

		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			@if( $field['field'] == 'id' || $field['field'] == 'image' || $field['field'] == 'sort_order' || $field['field'] == 'created_date'|| $field['field'] == 'last_updated') {{ $field['field'] == '' }} @else
			  <option value="{{ $field['field'] }}" 
			  @endif
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				@endif	
			  >{{ $field['label']}}  </option>
			@endif	  
		  @endforeach
		 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
		</p>		
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
@endif
	  </div>
	</div>	
	<!-- end footer -->	</div>
</div>	
	</div>	  
</div>	
	
@stop