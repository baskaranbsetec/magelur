@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Testimonial</h1> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'testimonial/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset>
									
									  {!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title <span class="asterix"> * </span> </label>
										<div class="col-md-7">
										  <input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
						     class='form-control ' required/> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Description" class=" control-label col-md-4 text-left"> Description <span class="asterix"> * </span> </label>
										<div class="col-md-7">
										  <textarea name='description' maxlength='400' rows='5' id='description' class='form-control '  
				           required>{{ $row['description'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <!-- <div class="form-group  " >
										<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
										<div class="col-md-7">
										  <input  type='file' name='image' id='image' @if($row['image'] =='') class='required' @endif style='width:150px !important;'  /><br>
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['image'],'/uploads/testimonial/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 	 -->				
									 <!--  <div class="form-group  " >
										<label for="Sort Order" class=" control-label col-md-4 text-left"> Sort Order </label>
										<div class="col-md-7">
										  <input  type='text' name='sort_order' id='sort_order' value='{{ $row['sort_order'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 	 -->				
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span> </label>
										<div class="col-md-7">
										   <label class='radio radio-inline'>
					<input type='radio' name='status' value ='0' required @if($row['status'] == '0') checked="checked" @endif > Inactive </label>
					<label class='radio radio-inline'>
					<input type='radio' name='status' value ='1' required @if($row['status'] == '1') checked="checked" @endif > Active </label> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									 </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('testimonial?return='.$return) }}' " class="btn btn-warning btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("testimonial/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	var minLength = 3;
var maxLength =400	;

$("#description").on("keypress", function(){
    var value = $(this).val();
    // alert(value.length);
    if (value.length == maxLength)
        swal("Description should only 400 Characters");
    
});
	</script>		 
@stop