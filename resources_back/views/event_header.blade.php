<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
	
<script src="{{ asset('magelur/js/tagsly.js')}}"></script>
<link href="{{ asset('magelur/css/tagsly.css')}}" rel="stylesheet" type="text/css">
	<div class="magelur_about model_abouts">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner magelur_event_details_banner">
					
				</div>
			</div>
		</section>

<div class="sm_bars">
	@foreach($agents as $agent)
	<div class="user_img">
		<a href="javascript:void(0)"><img src="{{ asset('images/model_event_profile.jpg') }}"></a>
	</div>	
 <div class="yell_bar clearfix">
  <div class="barbs_text">
  	<h1>{{ $agent->agency_name }} | <span> {{ \SiteHelpers::getName('country',$agent->country) }} </span></h1>
  </div>
  <div class="socail_torend">
   <ul class="icones">
        <li class="fbook"><a href=""></a></li>
        <li class="gpluser"><a href=""></a></li>
        <li class="twittera"><a href=""></a></li>
        <li class="instagra"><a href=""></a></li>
   </ul>
  </div> 	
 </div>	

	<div class="after_login_menu clearfix">
		<ul class="loged_in">
			<li class="{{ Request::path() == 'agent-dashboard/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-dashboard/'.$agent->id)}}">Dashboard</a></li>
			<li class="{{ Request::path() == 'agent-profile-edit/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-profile-edit/'.$agent->id)}}">Profile</a></li>
			<li class="{{ Request::path() == 'message' ? 'active' : '' }}"><a href="{{ URL::to('message')}}">Message</a></li>
			<li class="{{ Request::path() == 'agent-photos' ? 'active' : '' }}"><a href="javascript:void(0)">Event</a></li>
			<li class="{{ Request::path() == 'agent-photos' ? 'active' : '' }}"><a href="javascript:void(0)">Photos</a></li>
			<li class="{{ Request::path() == 'agent-videos' ? 'active' : '' }}"><a href="javascript:void(0)">Video</a></li>
			<li class="{{ Request::path() == 'agent-network' ? 'active' : '' }}"><a href="javascript:void(0)">Network</a></li>
			<li class="{{ Request::path() == 'agent-membership/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-membership/'.$agent->id)}}">membership</a></li>
		</ul>
		<div class="logout"><a href="{{ URL::to('user/logout')}}">Logout</a></div>	
	</div>
	@endforeach
</div>	
		<!-- Header End -->
</div>