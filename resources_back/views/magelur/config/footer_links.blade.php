
 @extends('layouts.app')

@section('content')
  <div class="page-content row">


 <div class="page-content-wrapper">  
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('magelur.config.tab')	
<div class="tab-content m-t">
	  <div class="tab-pane active use-padding" id="info">	
{!! Form::open(array('url'=>'updatelinks', 'class'=>'form-vertical row')) !!}
<div class="col-sm-6 animated fadeInRight ">
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Facebook Links</label>
			<div class="col-md-8">
			<input name="fb" type="text" id="fb" class="form-control input-sm" required  value="{{ $fb }}" />  
			 </div> 
		  </div>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Twitter Links</label>
			<div class="col-md-8">
			<input name="twitter" type="text" id="twitter" class="form-control input-sm" required  value="{{ $twitter }}" />  
			 </div> 
		  </div>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Youtube Links</label>
			<div class="col-md-8">
			<input name="ytube" type="text" id="ytube" class="form-control input-sm" required  value="{{ $ytube }}" />  
			 </div> 
		  </div>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Instagram Links</label>
			<div class="col-md-8">
			<input name="insta" type="text" id="insta" class="form-control input-sm" required  value="{{ $insta }}" />  
			 </div> 
		  </div>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Address</label>
			<div class="col-md-8">
			<textarea name="address" type="text" id="address" class="form-control input-sm" required >{{ $address }}</textarea>  
			 </div> 
		  </div>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div>  
		</div>

 {!! Form::close() !!}
</div>
</div>
</div>
</div>

@stop





