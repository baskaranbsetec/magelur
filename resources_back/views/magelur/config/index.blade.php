@extends('layouts.app')


@section('content')
  <div class="page-content row">

 	<div class="page-content-wrapper">  
 
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="block-content">
	@include('magelur.config.tab')	
<div class="tab-content m-t">
  <div class="tab-pane active use-padding" id="info">	
  <div class="sbox  "> 
  <div class="sbox-content"> 
		 {!! Form::open(array('url'=>'magelur/config/save/', 'class'=>'form-horizontal', 'files' => true)) !!}

		<div class="col-sm-6 animated fadeInRight ">
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_appname') }} </label>
			<div class="col-md-8">
			<input name="cnf_appname" type="text" id="cnf_appname" class="form-control input-sm" required  value="{{ CNF_APPNAME }}" />  
			 </div> 
		  </div>  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Become a Model </label>
			<div class="col-md-8">
			<textarea name="cnf_appdesc" type="text" id="cnf_appdesc" class="form-control input-sm" value="">{{ CNF_APPDESC }}</textarea>
			 </div> 
		  </div>  
		  
		  <!-- <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_comname') }} </label>
			<div class="col-md-8">
			<input name="cnf_comname" type="text" id="cnf_comname" class="form-control input-sm" value="{{ CNF_COMNAME }}" />  
			 </div> 
		  </div>  -->     

		 <!--  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_emailsys') }} </label>
			<div class="col-md-8">
			<input name="cnf_email" type="text" id="cnf_email" class="form-control input-sm" value="{{ CNF_EMAIL }}" /> 
			 </div> 
		  </div>  -->  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> Muliti language <br /> <small> Only Layout Interface </small> </label>
			<div class="col-md-8">
				<div class="checkbox">
					<input name="cnf_multilang" type="checkbox" id="cnf_multilang" value="1"
					@if(CNF_MULTILANG ==1) checked @endif
					  />  {{ Lang::get('core.fr_enable') }} 
				</div>	
			 </div> 
		  </div> 
		     
		   <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_mainlanguage') }} </label>
			<div class="col-md-8">

					<select class="form-control" name="cnf_lang">

					@foreach(SiteHelpers::langOption() as $lang)
						<option value="{{  $lang['folder'] }}"
						@if(CNF_LANG ==$lang['folder']) selected @endif
						>{{  $lang['name'] }}</option>
					@endforeach
				</select>
			 </div> 
		  </div>   
		      
 <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_album') }} </label>
			<div class="col-md-8">
			<input name="cnf_album" type="text" id="cnf_album" class="form-control input-sm" value=@if (defined('CNF_ALBUM'))"{{ CNF_ALBUM }}" @endif >  
			 </div> 
		  </div> 
		   <!-- <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> Frontend Template </label>
			<div class="col-md-8">

					<select class="form-control" name="cnf_theme">
					@foreach(SiteHelpers::themeOption() as $t)
						<option value="{{  $t['folder'] }}"
						@if(CNF_THEME ==$t['folder']) selected @endif
						>{{  $t['name'] }}</option>
					@endforeach
				</select>
			 </div> 
		  </div>  -->

		  <div class="form-group hide">
		    <label for="ipt" class=" control-label col-md-4"> Development Mode ?   </label>
			<div class="col-md-8">
				<div class="checkbox">
					<input name="cnf_mode" type="checkbox" id="cnf_mode" value="1"
					@if (defined('CNF_MODE') &&  CNF_MODE =='production') checked @endif
					  />  Production
				</div>
				<small> If you need to debug mode , please unchecked this option </small>	
			 </div> 
		  </div> 		  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 
		</div>

		<div class="col-sm-6 animated fadeInRight ">

		  
		 		

		 <!--  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Metakey </label>
			<div class="col-md-8">
				<textarea class="form-control input-sm" name="cnf_metakey">{{ CNF_METAKEY }}</textarea>
			 </div> 
		  </div> 

		   <div class="form-group">
		    <label  class=" control-label col-md-4">Meta Description</label>
			<div class="col-md-8">
				<textarea class="form-control input-sm"  name="cnf_metadesc">{{ CNF_METADESC }}</textarea>
			 </div> 
		  </div>  --> 
		  <div class="form-group">
		  	<label  class=" control-label col-md-4">Backend Logo</label>
		  	<div class="col-md-8">
		  		<div id="logo">
		  			<input type="file" accept=".jpg, .jpeg, .png" data-width="190" data-height="45" data-id="blogo_src" name="logo" />
		  			<!-- <p> <i>Please use image dimension 190px * 45px </i> </p> -->
		  		</div>	
		  		<div id='content-area'></div>
		  		<p> <i>Please use image dimension 190px * 45px </i> </p>
		  		<div class='prev_logo' style="padding:5px; border:solid 1px #e98c23;  width:auto;">
		  			@if(file_exists(asset('/magelur/images/'.CNF_LOGO)) && CNF_LOGO !='')
		  			<img id="blogo_src" src="{{ asset('magelur/images/'.CNF_LOGO)}}" style="max-width:190px;max-height:45px;" alt="{{ CNF_APPNAME }}" />
		  			@else
		  			<img id="blogo_src" src="{{ asset('magelur/images/'.CNF_LOGO)}}" style="max-width:190px;max-height:45px;" alt="{{ CNF_APPNAME }}" />
		  			@endif	
		  		</div>	
		  	</div> 
		  </div> 
		  <div class="form-group">
		    <label  class=" control-label col-md-4">Frontend Logo</label>
			<div class="col-md-8">
			<div id="flogo">
				<input type="file" name="flogo" data-width="398" data-height="122" data-id="flogo_src"  />
			</div>	
			<p> <i>Please use image dimension 398px * 122px </i> </p>
			<div style="padding:5px; border:solid 1px #1ab394;  width:auto;">
				 	@if(file_exists(asset('/magelur/images/'.CNF_FLOGO)) && CNF_FLOGO !='')
				 	<img  id="flogo_src" src="{{ asset('magelur/images/'.CNF_FLOGO)}}" style="max-width:195px;max-height:50px;" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img id="flogo_src" src="{{ asset('magelur/images/'.CNF_FLOGO)}}" style="max-width:195px;max-height:50px;" alt="{{ CNF_APPNAME }}" />
					@endif	
				</div>				
			 </div> 
		  </div> 
		<!--    <div class="form-group">
		    <label  class=" control-label col-md-4">Backend Logo</label>
			<div class="col-md-8">
				<input type="file" name="logo">
				<p> <i>Please use image dimension 155px * 30px </i> </p>
				<div style="padding:5px; border:solid 1px #ddd; background:#f5f5f5; width:auto;">
				 	@if(file_exists(public_path().'/magelur/images/'.CNF_LOGO) && CNF_LOGO !='')
				 	<img src="{{ asset('magelur/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img src="{{ asset('magelur/images/logo.png')}}" alt="{{ CNF_APPNAME }}" />
					@endif	
				</div>				
			 </div> 
		  </div>  	 -->

		 	  

		</div>  
		 {!! Form::close() !!}
	</div>
	</div>	 
</div>
</div>
</div>
</div>
<script type="text/javascript">
$('body').on('change','input:file',function(){
	var _URL = window.URL || window.webkitURL,
	file, img,height,file_html='',
	id = $(this).data('id'),
	height = $(this).data('height'),
	width = $(this).data('width'),
	attrName = $(this).attr('name');
	selector = $('#'+id);
	selector.addClass('hide');
	if (this.files && this.files[0]) {
		img = new Image();
		img.src = _URL.createObjectURL(this.files[0]);
		if(id=='fav') image_type = ['gif','png','jpg','jpeg','ico']; else image_type = ['gif','png','jpg','jpeg']; 
		ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, image_type) == -1) {
			alert("invalid image");
			file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
			$('input[data-id="'+id+'"]').remove();
			$('#'+attrName).html(file_html);
			selector.attr('src','');
			return false;
		}else{
			selector.attr('src',img.src);
			img.onload = function(){
				if(this.width > width || this.height > height){
					alert('Your Selected Image size '+this.width+'px * '+this.height+'px.Please use image dimension '+width+'px * '+height+'px');
					file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
					$('input[data-id="'+id+'"]').remove();
					$('#'+attrName).html(file_html);
					selector.attr('src','{{ asset("magelur/images/file-broken.png")}}');
					return false;
				}
			}
		}

	}
	setTimeout(function(){
		selector.removeClass('hide');
	},1000);
});
</script>

@stop