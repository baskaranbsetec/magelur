

<div class="ourmodel_banner advertisement">
  <div class="ourmod_cnt">
    <h1>tell a friend</h1>
    <div class="two_laid"><a href="{{asset('login')}}">Home</a><p>tell a friend</p></div>
  </div>
</div>

<div class="payment_order_form_page col-md-12 col-sm-12 col-xs-12">
<div class="container">
  <div class="payment_order_blk col-md-12 col-sm-12 col-xs-12">

      <div class="payment_order_leftside col-md-6 col-sm-12 col-xs-12">

      <h4 class="payment_title">YOUR ORDER DETAILS</h4>

<form method="POST" action="#" class="theme_input payment_post_form1">  
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width "><h1 class="bold-text">Buyer Name:</h1><h2 class="normal-text">{{$transaction->username}}</h2></label> 
    </div>
    <div class="form-group form-group_full">
    <label for="Vendor Name:" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h1 class="bold-text">Vendor Name:</h1><h2 class="normal-text">Magelur</h2></label>
    </div>
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h1 class="bold-text">Order Number:</h1><h2 class="normal-text">{{$transaction->order_num}}</h2></label>
    </div>
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12  col-sm-12 col-xs-12 text-left fields_width"><h1 class="bold-text">Order Date:</h1><h2 class="normal-text">{{$transaction->created_at}}</h2></label>
    </div>
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h1 class="bold-text">Payment Method:</h1><h2 class="normal-text">{{$transaction->payment_method}}</h2></label>
    </div>
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h1 class="bold-text">Plan Name:</h1><h2 class="normal-text">{{$transaction->plan_name}}</h2></label>
    </div>
    <div class="form-group form-group_full">
    <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h1 class="bold-text">Plan Amount:</h1><h2 class="normal-text">${{$transaction->amount}}</h2></label>
    </div>
    <div class="text-center col-md-12 col-sm-12 col-xs-12">
      <button onclick="window.print();return false;"><span>PRINT</span></button>
    </div>
</form>
</div>
<div class="payment_order_rightside col-md-6 col-sm-12 col-xs-12">
      <div class="payment_order_image_blk col-md-12 col-sm-12 col-xs-12">        
      <div class="payment_order_div">  
      @if($transaction->transaction_status == 'completed')
        <img src="{{asset('images/Success_2.png')}}" alt="file_image">
      @else
        <img src="{{asset('images/Failure_2.png')}}" alt="file_image">
      @endif
      </div>
<!--       <div class="payment_order_div">  
      <img src="{{asset('images/close.png')}}" alt="file_image">
      </div> -->
      </div>
      <div class="form-group form-group_full">
        <label for="Buyer Name" class=" control-label col-md-12 col-sm-12 col-xs-12  text-left fields_width"><h3 class="bold-text text-center">{{$transaction->plan_name}} plan details</h2></label>
      </div>
      <div class="payment_order_content_blk col-md-12 col-sm-12 col-xs-12">        
      <p>{{$transaction->statusMessage}}</p>
      </div>

      <div class="payment_order_list col-md-12 col-sm-12 col-xs-12">        
      <ul class="payment_ul_div">
      <li>Use {{$transaction->video_limit}} Videos</li>
      <li>Advertise {{$transaction->event_limit}} events</li>
      <li>Can upload {{$transaction->image_limit}} images</li>
     
      </ul>
      </div>

</div>

      </div>
  
</div>
</div> 
<style media="print">
 @page {
  size: auto;
  margin: 0;
       }
</style>
<style type="text/css">
@media print{
  body{ background-color:#FFFFFF; background-image:none; color:#000000 }
  header{ display:none;}
  footer{ display:none;}
  
}
</style>
<style>
.map {
    display: none;
}
.ourmodel_banner.advertisement {
    display: none;
}
</style>


