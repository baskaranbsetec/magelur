@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
		<h1>Membership plan</h1>
		

		<div class="sbox-tools" >
			
<!-- 
			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif  -->
		</div>
	</div>

	<div class="sbox-content"> 	
	<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('membership/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'membership/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
	<!-- 		@if($access['is_excel'] ==1)
			<a href="{{ URL::to('membership/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif -->

		</div>

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'membership/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
				
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Plan Interval' || $t['label']=='Plan Periods' || $t['label']=='Level' || $t['label']=='Stripe Plan' || $t['label']=='Stripe Plan Y' )
							{{--*/ $t['label']=" " /*--}}
						@elseif($t['label']=='Plan Amount')	
						{{--*/ $t['label']="Monthly Plan Amount" /*--}}
						@elseif($t['label']=='Plan Amount Y')	
						{{--*/ $t['label']="Yearly Plan Amount" /*--}}	
						@elseif($t['label']=='Status')	
						{{--*/ $t['label']="Monthly Plan Status" /*--}}	
						@elseif($t['label']=='Status Y')	
						{{--*/ $t['label']="Yearly Plan Status" /*--}}		
						@elseif($t['label']=='Level')	
						{{--*/ $t['label']="User Type" /*--}}		
						@endif						
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.plan_type') }}</span></th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody>        
        <tr id="orderDetails">
		<td class="number"></td>
		<td></td>
		<td id="plan_name" class="fieldsearch"> <div class="select-style"  style="width:80px">
			{!! Form::text('plan_name',"{$plan_name}",array('class'=>'form-control', 'placeholder'=>'Plan Name', )) !!} </div></td>

		<td id="plan_statement" class="fieldsearch"> <div class="select-style"  style="width:80px">
		    {!! Form::text('plan_statement',"{$plan_statement}",array('class'=>'form-control', 'placeholder'=>'Plan Statement', )) !!} </div></td>
		</div></td>
		<td id="plan_amount" class="fieldsearch"> <div class="select-style"  style="width:80px">
		    {!! Form::number('plan_amount',"{$plan_amount}",array('class'=>'form-control', 'placeholder'=>'Monthly Plan Amount', )) !!} </div></td>
		</div></td>
		<td id="plan_amount_y" class="fieldsearch"> <div class="select-style"  style="width:80px">
		    {!! Form::number('plan_amount_y',"{$plan_amount_y}",array('class'=>'form-control', 'placeholder'=>'Yearly Plan Amount', )) !!} </div></td>
		</div></td>
		<td></td>
		<td></td>
		<td id="status" class="fieldsearch"> <div class="select-style"  style="width:80px">
			<select name='status' class = 'select2 '>
				<option value="">Select option</option>
				<option value="1"{{ $status == '1' ? 'selected' : ''}} >Active</option>
				<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>
			</select>		
		</div></td>
			<td id="status_y" class="fieldsearch"> <div class="select-style"  style="width:80px">
				<select name='status_y' class = 'select2 '>
					<option value="">Select option</option>
					<option value="1" {{ $status_y == '1' ? 'selected' : ''}}>Active</option>
					<option value="0" {{ $status_y == '0' ? 'selected' : ''}}>Inactive</option>
				</select>		
		</div></td>
		<td></td>
		<td></td>
		<td></td>
		<td id="type" class="fieldsearch"> <div class="select-style"  style="width:80px">
		<select name='type' id='type' class = 'select2 '>
		<option value="">Select option</option>
		<option value="agent"  id='type-name' {{ $type == 'agent' ? 'selected' : ''}}>Agent</option>
		<option value="model"  id='type-name' {{ $type == 'model' ? 'selected' : ''}}>Model</option>
		</select>		</div></td>
		


		<td class="icons">
		    <input type="hidden"  value="Search">
		   <!--  <input type="hidden"  name="Tags" id="Alltags" value=""> -->

		    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
		    <a href="{{ url('membership') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
		</td>
		</tr> 						
        	{{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
        	@foreach($model_filter as $row)
        	<tr>
        		<td width="30"> {{ $i }} </td>
					@if($row->plan_id !='1' && $row->plan_id !='5')
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->plan_id }}" />  </td>	
					@else
					<td></td>
					@endif
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('membership/show/'.$row->plan_id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('membership/update/'.$row->plan_id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->
{{--*/ $i++ /*--}}
				 @foreach ($tableGrid as $field)
				 
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>		
						  @if($field['field']=='plan_interval' || $field['field']=='plan_periods' || $field['field']=='level' || $field['field']=='stripe_plan' || $field['field']=='stripe_plan_y')
							{{--*/$field['field']=='';/*--}}

							@elseif($field['field']=='level')
							<span><pre>{{ print_r($row-type) }} </pre> </span> <br>
							@elseif($field['field']=='status')
								
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif	
								
							@elseif($field['field']=='status_y')
								
								@if( $row->status_y == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif	

							@else			 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}	
						 	@endif					 
						 </td>
						@endif	
					 @endif					 
				 @endforeach	
				  <td>
				  	{{ ucfirst($row->type) }}
				  </td>
				  <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('membership/show/'.$row->plan_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
							<a  href="{{ URL::to('membership/update/'.$row->plan_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
				</td>	 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
<div class="table-footer">
	<div class="row">
	 <div class="col-sm-4">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			@if( $field['field'] == 'plan_interval' || $field['field'] == 'plan_periods' || $field['field'] == 'level' || $field['field'] == 'stripe_plan'|| $field['field'] == 'stripe_plan_y') {{ $field['field'] == '' }} @else
			  <option value="{{ $field['field'] }}" 
			  @endif
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				@endif	
			  >@if( $field['label'] == 'Plan Amount'){{ 'Monthly Plan Amount' }} @elseif( $field['label'] == 'Plan Amount Y'){{ 'Yearly Plan Amount' }} @elseif( $field['label'] == 'Status'){{ 'Monthly Plan Status' }} @elseif( $field['label'] == 'Status Y'){{ 'Yearly Plan Status' }} @else {{ $field['label']}} @endif 

			</option>
			@endif	  
		  @endforeach
		 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-2">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
				
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
</p>
@endif
	  </div>
	</div>

	<!-- @include('footer') -->	</div>
</div>	
	</div>	  
</div>	
	
@stop