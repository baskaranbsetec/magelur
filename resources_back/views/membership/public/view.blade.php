<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Plan Id</td>
						<td>{{ $row->plan_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Name</td>
						<td>{{ $row->plan_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Statement</td>
						<td>{{ $row->plan_statement}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Amount</td>
						<td>{{ $row->plan_amount}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Amount Y</td>
						<td>{{ $row->plan_amount_y}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Interval</td>
						<td>{{ $row->plan_interval}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Periods</td>
						<td>{{ $row->plan_periods}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status Y</td>
						<td>{{ $row->status_y}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Level</td>
						<td>{{ $row->level}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Stripe Plan</td>
						<td>{{ $row->stripe_plan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Stripe Plan Y</td>
						<td>{{ $row->stripe_plan_y}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created At</td>
						<td>{{ $row->created_at}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Modified At</td>
						<td>{{ $row->modified_at}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	