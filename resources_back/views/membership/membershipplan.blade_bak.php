
<div class="ourmodel_banner membt">
  <div class="ourmod_cnt">
    <h1>membership Plan</h1>
    <div class="two_laid"><a href="index.html">Home</a> <p> membership Plan</p></div>
  </div>
</div>
<div class="col-md-offset-2 col-md-8 col-sm-12">
        <div class="terms_contant">
<div class="terms_details">
            <h3><span></span>Our membership Plan</h3>
            <p>We are seeing an increasing number of people wanting to break away from more traditional models and rather than just call this a hybrid model as we have previously, we decided that what was actually needed was a new approach to membership models.</p>

          </div>
          </div>
          </div>
<div class="adevrtise_with memvr_adver">
 <div class="container">
  <div class="three_plans clearfix">
     @foreach($memberships as $membership)
     {!! Form::open(array('url' => url('membership/buy-membership-plan'),'id'=>'payment-stripe' )) !!}
      <input type="hidden" name="plan_membership" id="plan_membership" value="">
 
     

       <div class="first_plan">
        <div class="start_bg">
          <h1>{{$membership->plan_name}}</h1>
           <div class="sbase"> 
            <div class="traingle">
             <div class="mini_traingle"></div>  
            </div>
            @if($membership->status == 1)
             <div class="pre_price"><h1>${{$membership->plan_id}}</h1>
            
             <p>MONTHLY</p>
           </div>
           
           <input type="hidden" name="membership_type" value="m">
           @elseif($membership->status_y == 1)
             <div class="pre_price"><h1>${{$membership->plan_amount_y}}</h1>
             <p>YEARLY</p>
           </div>
            
            <input type="hidden" name="membership_type" value="y">
           @endif
           </div>
          <div class="content-withs">  
            <ul>
              <li>Use 5 Videos</li>
              <li>Models profession</li>
              <li>Can upload 10 images</li>
            </ul>
          </div> 
                 <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}'>BUY NOW</a></div> 

         </div>
        </div> 

    {!! Form::close() !!}
    @endforeach

   
  </div>    
 </div>  
</div>  



<script type="text/javascript">
 $(document).ready(function() {

    $('.add').click(function () {
         var vl = $(this).attr('data-value');
         var pl = $(this).attr('data-plan');

         $('#plan_membership').val(vl);
         $('#payment-stripe').submit();

         
    });
  });
</script>
