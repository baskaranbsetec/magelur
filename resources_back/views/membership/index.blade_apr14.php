@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
		<h1>Testimonial</h1>
		

		<div class="sbox-tools" >
			
<!-- 
			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif  -->
		</div>
	</div>

	<div class="sbox-content"> 	
	<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('membership/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'membership/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
	<!-- 		@if($access['is_excel'] ==1)
			<a href="{{ URL::to('membership/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif -->

		</div>

	 {!! (isset($search_map) ? $search_map : '') !!}
	
	 {!! Form::open(array('url'=>'membership/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
				
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Plan Interval' || $t['label']=='Plan Periods' || $t['label']=='Level' || $t['label']=='Stripe Plan' || $t['label']=='Stripe Plan Y' )
							{{--*/ $t['label']=" " /*--}}
						@elseif($t['label']=='Plan Amount')	
						{{--*/ $t['label']="Monthly Plan Amount" /*--}}
						@elseif($t['label']=='Plan Amount Y')	
						{{--*/ $t['label']="Yearly Plan Amount" /*--}}	
						@elseif($t['label']=='Status')	
						{{--*/ $t['label']="Monthly Plan Status" /*--}}	
						@elseif($t['label']=='Status Y')	
						{{--*/ $t['label']="Yearly Plan Status" /*--}}		
						@elseif($t['label']=='Level')	
						{{--*/ $t['label']="Plan Type" /*--}}		
						@endif						
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.plan_type') }}</span></th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody>        						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->plan_id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('membership/show/'.$row->plan_id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('membership/update/'.$row->plan_id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->

				 @foreach ($tableGrid as $field)
				 
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>		
						  @if($field['field']=='plan_interval' || $field['field']=='plan_periods' || $field['field']=='level' || $field['field']=='stripe_plan' || $field['field']=='stripe_plan_y')
							{{--*/$field['field']=='';/*--}}

							@elseif($field['field']=='level')
							<span><pre>{{ print_r($row-type) }} </pre> </span> <br>
							@elseif($field['field']=='status')
								
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif	
								
							@elseif($field['field']=='status_y')
								
								@if( $row->status_y == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif	

							@else			 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}	
						 	@endif					 
						 </td>
						@endif	
					 @endif					 
				 @endforeach	
				  <td>
				  	{{ ucfirst($row->type) }}
				  </td>
				  <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('membership/show/'.$row->plan_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('membership/update/'.$row->plan_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
				</td>	 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
	
@stop