@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('membership?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('membership/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			{{$row->plan_name}}
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('membership/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('membership/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Plan Id</td>
						<td>{{ $row->plan_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Name</td>
						<td>{{ $row->plan_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Statement</td>
						<td>{{ $row->plan_statement}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Monthly Plan Amount</td>
						<td>{{ $row->plan_amount}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Yearly Plan Amount</td>
						<td>{{ $row->plan_amount_y}} </td>
						
					</tr>
				
					<!-- <tr>
						<td width='30%' class='label-view text-right'>Plan Interval</td>
						<td>{{ $row->plan_interval}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Plan Periods</td>
						<td>{{ $row->plan_periods}} </td>
						
					</tr> -->
				
					<tr>
						<td width='30%' class='label-view text-right'>Monthly Status</td>
						<td>@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Yearly Status</td>
						<td>@if( $row->status_y == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
							 </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Album Limit</td>
						<td>{{ $row->image_limit}} </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Video Limit</td>
						<td>{{ $row->video_limit}} </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Video Duration Limit</td>
						<td>{{ $row->video_duration_limit}} </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Event Limit</td>
						<td>{{ $row->event_limit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Stripe Plan</td>
						<td>{{ $row->stripe_plan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Yearly Stripe Plan</td>
						<td>{{ $row->stripe_plan_y}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created At</td>
						<td>{{ $row->created_at}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Modified At</td>
						<td>{{ $row->modified_at}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop