<div class="member_payment_page">
<!-- <div class="ourmodel_banner membt">
  <div class="ourmod_cnt">
    <h1>Billing Information</h1>
    <div class="two_laid"><a href="index.html">Home</a> <p> membership Plan</p></div>
  </div>
</div> -->

<div class="adevrtise_with memvr_adver">
 <div class="container">
  <div class="three_plans clearfix">
  <div class="member_payment_boxshadow">
  <h1 class="heading">Payment Form</h1>

          <div class="card-wrapper"></div>
            <div class="form-container">
                <form id="payment-stripe" action="{{ url('membership/stripe') }}" method="POST" >

                    <input type="hidden" name="membership_type" value="{{$membership_type}}">
                    <input type="hidden" name="plan_membership" value="{{$plan_membership}}">
                    <input name='stripeToken' id="stripeToken" type="hidden" value=""/>
                    <div class="row ">
                        <div class="small-12 columns">
                            <input placeholder="Card number" id='card-number' type="text" name="card-number"  >
                        </div>
                        <div class="small-12 columns">
                            <input placeholder="Full name" id='nameOnCard' type="text" name="nameOnCard"  >
                        </div>
                    </div>
                    <div class="row ">
                        <div class="small-6 columns">
                            <input placeholder="MM/YY" type="text" name="card-expiry-year"  >
                        </div>

                        <div class="small-6 columns">
                            <input placeholder="CVC" type="text" id='card-cvc' name="card-cvc" >
                        </div>

                        <div class="small-12 columns payment_submit_btn">
                            <div class="payment_submit_div">
                        <span style='display:none' class='loader'>
                        <img  src="{{ asset('magelur/images/orange_circles.gif') }}" style='height:40px;width:40px;'/>
                        Please wait your Payment is processing..</span>
                                <input type="submit" id="submit_button" value="Submit" class="button postfix">
                            </div>
                        </div>

                        
                    </div>
                    <div class="text-center" id="error-payment">
                      <span class="payment-errors text-danger"></span>
                    </div>
                </form>
            </div>    


   
  </div>  
 </div>  
 </div>
 </div>
 </div>

<script src="{{ asset('magelur/js/jquery_1.11.1.validate.min.js')}}"></script>
 <!-- card animation files -->   
<!-- <link rel="stylesheet" type="text/css" href="https://jessepollak.github.io/card/build/css/demo.css"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/default/css/demo.css')}}">


<script src="https://rawgit.com/jessepollak/card/master/dist/card.js"></script>


<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>


<script type="text/javascript">
// payment options script start
Stripe.setPublishableKey("{{ $publishable_key }}");
var payment_error = $('.payment-errors');

$(document).ready(function() {
    function submit(form) {
        // $('#submit_button').prop('disabled', true);


        var expirydate = $('input[name="card-expiry-year"]').val();
        expirydate =  expirydate.split('/');
        
        // alert($('input[name="card-expiry-year"]').val())
        // return;
        // given a valid form, submit the payment details to stripe
        $(form['submit_button']).attr("disabled", "disabled")
        Stripe.createToken({
            number    : $('#card-number').val(),
            cvc       : $('#card-cvc').val(),
            exp_month : $.trim(expirydate[0]), 
            exp_year  : $.trim(expirydate[1])
        },function(status, response) {

            if (response.error) {
                // re-enable the submit button
                $(form['submit_button']).removeAttr("disabled")
                // $('.loader').hide();
                // $('#submit_button').show();
                // show the error
               payment_error.html("<i class='fa fa-warning'></i>"+response.error.message);
               if(response.error.code == 'invalid_expiry_year' || response.error.code == 'invalid_expiry_month'){
                 $('.payment-errors').show();
               }
               else{
                $('.payment-errors').hide();
               }
              
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
                $('.loader').show();
                $('#submit_button').hide();
                $('.payment-errors').hide();
                $('#submit_button').prop('disabled', true);
                $('#submit_button').addClass('disabled');
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "<i class='fa fa-warning'></i> Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "<i class='fa fa-warning'></i> Please enter a valid security code");

    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
             "card-number" : {
                cardNumber: true,
            },
            "card-cvc" : {
                cardCVC: true,
            },
           
            "card-expiry-year" : {
                required: true,
            },
        },
        errorPlacement: function(error, element){
            console.log(error)
            console.log(element)
            // payment_error.html(" ");
            payment_error.html(error);
        },
        message: {
           
            "card-expiry-year" : {
                required: "<span><i class='fa fa-warning'></i></span> Please enter a Expiry Date",
            },
        },
    });
});


</script>

<script>
    var card = new Card({ 
        form: 'form#payment-stripe', 
        container: '.card-wrapper',
        debug: true,
        formSelectors: {
                numberInput  : 'input[name="card-number"]', // optional — default input[name="number"]
                expiryInput  : 'input[name="card-expiry-year"]', // optional — default input[name="expiry"]
                cvcInput     : 'input[name="card-cvc"]', // optional — default input[name="cvc"]
                nameInput    : 'input[name="nameOnCard"]' // optional - defaults input[name="name"]
            },
        });
    </script>



    <style>
    .map{
        display:none;
    }
    .ourmodel_banner.membt {

    display: none;

}
    </style>