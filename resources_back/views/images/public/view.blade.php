<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
						<tr>
							<td width='30%' class='label-view text-right'>ID</td>
							<td>{{ $row->ID}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>OwnerId</td>
							<td>{{ $row->OwnerId}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>AlbumId</td>
							<td>{{ $row->AlbumId}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>{{ $row->user_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>MediaUri</td>
							<td>{{ $row->MediaUri}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Title</td>
							<td>{{ $row->Title}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Description</td>
							<td>{{ $row->Description}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Tags</td>
							<td>{{ $row->Tags}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Ext</td>
							<td>{{ $row->Ext}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Type</td>
							<td>{{ $row->Type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Status</td>
							<td>{{ $row->Status}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>LastEdit</td>
							<td>{{ $row->LastEdit}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created</td>
							<td>{{ $row->Created}} </td>
							
						</tr>
							
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	