<!-- <script type="text/javascript" src="{{ asset('magelur/js/lazyload.js')}}"></script>
 -->
@if($user_type=='model'){
		@include('model_dash_header')
    {{--*/ $url = 'model-dashboard' /*--}}
		@endif
		@if($user_type=='agent'){
		@include('agent_dash_header')
    {{--*/ $url = 'agent-dashboard' /*--}}
		@endif
<div class="col-md-12 col-xs-12 col-sm-12 fav_video_page">
<div class="container">
<div class="agency_video_lisiting">
<div class="col-md-12 col-sm-12 col-xs-12 fav_first_part">
<span class="favourite_title">Favourites</span>
<button onClick="location.href='{{url('favourite-photos/'.$user_id)}}'"><span>PHOTOS</span></button>
<button class="active"><span>VIDEOS</span></button>
<button onClick="location.href='{{url('favourite-models/'.$user_id)}}'"><span>MODELS</span></button>
<button  onClick="location.href='{{url('favourite-agents/'.$user_id)}}'"><span>AGENTS</span></button>
</div>
@if(count($models_fav)>0)

 @foreach($models_fav as $iKey => $model)
  <div class=" gallery_video">            
    <div class="magelur_gallery_div magelur_gallery_video_div col-md-4 col-sm-4 col-xs-12">
      <div class="magelur_image_blk magelur_video_blk">
        <!-- Video Thumb -->
        <img src="{{ $model['preview'] }}" class="gallery_images1 gallery_video_images1">
        <!-- <img src="http://img.youtube.com/vi/qTeS4XOFXgc/0.jpg" title="New Insta Upload"> -->
        <div class="img_hover_blk">
         <a href="{{ url('video/'.$model['video_id']) }}" class="hover-posts" target='_blank'>
          <div class="gallery_video_hvr">                                
            <span class="video_bg_clr">                                        
              <i class="fa fa-play"></i>                                        
            </span>                                  
          </div>
        </a>
      </div>
      </div>
    </div>                  
  </div>
 @endforeach
     @else
   <!-- show upload only for model & agent -->
          <div class="magelur_gallery empty_vertical wow bounce">
          <div class="empty_msg_details clearfix">
            <div class="empty_msg_blk clearfix">
              <div class="heading_blk">
                <div class="heading_details">
                  <h1>Access Denied!</h1>
                  <h2>
                    No more Favorites found
                  </h2>
                </div>
                <div class="file_img_blk">
                  <div class="file_img_div">
                    <!-- <img src="{{asset('images/file_image1.png')}}" alt="file_image" /> -->
                    <i class="fa fa-warning"></i>
                  </div>
                </div>
              </div>
              <!-- <p>You can search on the bar below or return to home page</p> -->
              <div class="pages_blk">
                <div class="msg_previous_page">
                  <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
                </div>
                <div class="msg_next_page">

                  <a href="{{url($url)}}/{{request()->route('id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
                </div>
              </div>
            </div>
          </div>
          </div>
    @endif
  <!--
  <div class=" gallery_video">            
    <div class="magelur_gallery_div magelur_gallery_video_div col-md-4 col-sm-4 col-xs-12">
      <div class="magelur_image_blk magelur_video_blk">
      
        <img src="http://img.youtube.com/vi/qTeS4XOFXgc/0.jpg" class="gallery_images1 gallery_video_images1">
        
        <div class="img_hover_blk">
        <a href="javascript:void(0);" class="hover-posts">
          <div class="gallery_video_hvr">                                
            <span class="video_bg_clr">                                        
              <i class="fa fa-play"></i>                                        
            </span>                                  
          </div>
        </a>
      </div>
      </div>
    </div>                  
  </div>
  <div class=" gallery_video">            
    <div class="magelur_gallery_div magelur_gallery_video_div col-md-4 col-sm-4 col-xs-12">
      <div class="magelur_image_blk magelur_video_blk">
        
        <img src="http://img.youtube.com/vi/qTeS4XOFXgc/0.jpg" class="gallery_images1 gallery_video_images1">
       
        <div class="img_hover_blk">
        <a href="javascript:void(0);" class="hover-posts">
          <div class="gallery_video_hvr">                                
            <span class="video_bg_clr">                                        
              <i class="fa fa-play"></i>                                        
            </span>                                  
          </div>
        </a>
      </div>
      </div>
    </div>                  
  </div>
   -->
   </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12 fav_pagination fav_agent_pagination">
<div class="pagination_content text-center">
  <ul class="pagination">
    {!! $pagination->render() !!}
  </ul>
</div>
</div>
</div>
</div>
@include('footer_map')
