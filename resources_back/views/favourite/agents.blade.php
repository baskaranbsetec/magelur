		@if($user_type=='model'){
		@include('model_dash_header')
    {{--*/ $url = 'model-dashboard' /*--}}
		@endif
		@if($user_type=='agent'){
		@include('agent_dash_header')
    {{--*/ $url = 'agent-dashboard' /*--}}
		@endif


<div class="model_photos mainder_pent our_agencies_blk fav_agencies_page">
<div class="container">
<div class="modelspro clearfix">
 
<div class="col-md-12 col-sm-12 col-xs-12 fav_first_part">
<span class="favourite_title">Favourites</span>
<button onClick="location.href='{{url('favourite-photos/'.$user_id)}}'"><span>PHOTOS</span></button>
<button onClick="location.href='{{url('favourite-videos/'.$user_id)}}'"><span>VIDEOS</span></button>
<button onClick="location.href='{{url('favourite-models/'.$user_id)}}'"><span>MODELS</span></button>
<button class="active"><span>AGENTS</span></button>
</div>
@if(count($models_fav)>0)
 @foreach($models_fav as $iKey => $model)
     <a href="{{ URL::to('agent-profile/'.$model['favourite_id'])}}">
    <div class="model_item agent_item" id="{{$model['favourite_id']}}">
     <img src="{{ asset('uploads/agency_profile/'.$model['profile_photo']) }}" alt="Favourite agent" class="gallery_images1">
       <div class="colorbox">
        <div class="inner_shades">
          <h4>{{$model['agency_name']}}</h4>
          <p>{{$model['username']}},</p>
        </div>
       </div>
    </div>
  </a>
 @endforeach
     @else
    <div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
      <div class="gallery_blk_container">
        <div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">

          <!-- show upload only for model & agent -->
          <div class="empty_vertical">
          <div class="empty_msg_details clearfix">
            <div class="empty_msg_blk clearfix">
              <div class="heading_blk">
                <div class="heading_details">
                  <h1>Access Denied!</h1>
                  <h2>
                   No more Favorites found
                  </h2>
                </div>
                <div class="file_img_blk">
                  <div class="file_img_div">
                    <!-- <img src="{{asset('images/file_image1.png')}}" alt="file_image" /> -->
                    <i class="fa fa-warning"></i>
                  </div>
                </div>
              </div>
              <!-- <p>You can search on the bar below or return to home page</p> -->
              <div class="pages_blk">
                <div class="msg_previous_page">
                  <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
                </div>
                <div class="msg_next_page">

                  <a href="{{url($url)}}/{{request()->route('id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    @endif
  <!--
        <a href="http://192.168.1.65:81/magelur/public/agent-profile/8">
    <div class="model_item agent_item" id="8">
     <img src="{{ asset('uploads/agency_profile/1526546262.png')}}" alt="Favourite agent" class="gallery_images1">
       <div class="colorbox">
        <div class="inner_shades">
          <h4>Alex</h4>
          <p>Kate,Anguilla</p>
        </div>
       </div>
    </div>
  </a>
        <a href="http://192.168.1.65:81/magelur/public/agent-profile/10">
    <div class="model_item agent_item" id="10">
     <img src="{{ asset('uploads/agency_profile/1521099138-34650312.jpg')}}" alt="Favourite agent" class="gallery_images1">
       <div class="colorbox">
        <div class="inner_shades">
          <h4>jery</h4>
          <p>jack,India</p>
        </div>
       </div>
    </div>
  </a>
  
    <a href="http://192.168.1.65:81/magelur/public/agent-profile/10">
    <div class="model_item agent_item" id="10">
     <img src="{{ asset('uploads/agency_profile/1521099138-34650312.jpg')}}" alt="Favourite agent" class="gallery_images1">
       <div class="colorbox">
        <div class="inner_shades">
          <h4>jery</h4>
          <p>jack,India</p>
        </div>
       </div>
    </div>
  </a>
  -->
  
 </div>
<div class="col-md-12 col-sm-12 col-xs-12 fav_pagination fav_agent_pagination">
<div class="pagination_content text-center">
	<ul class="pagination">
		{!! $pagination->render() !!}
	</ul>
</div>
</div>
</div>
</div>

@include('footer_map')
