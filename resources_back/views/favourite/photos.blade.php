<!-- <script type="text/javascript" src="{{ asset('magelur/js/lazyload.js')}}"></script>
 -->

		@if($user_type=='model'){
		@include('model_dash_header')
    {{--*/ $url = 'model-dashboard' /*--}}
		@endif
		@if($user_type=='agent'){
		@include('agent_dash_header')
    {{--*/ $url = 'agent-dashboard' /*--}}
		@endif

<div class="magelur_gallery wow bounce fav_images_page col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">
<div class="col-md-12 col-sm-12 col-xs-12 fav_first_part">
<span class="favourite_title">Favourites</span>
<button class="active"><span>PHOTOS</span></button>
<button onClick="location.href='{{url('favourite-videos/'.$user_id)}}'"><span>VIDEOS</span></button>
<button onClick="location.href='{{url('favourite-models/'.$user_id)}}'"><span>MODELS</span></button>
<button  onClick="location.href='{{url('favourite-agents/'.$user_id)}}'"><span>AGENTS</span></button>
</div>
@if(count($models_fav)>0)
<div class="gallery_images  col-md-12 col-sm-12 col-xs-12">
@foreach($models_fav as $iKey => $model)

<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
    <img src="{!! \SiteHelpers::user_albums($model['ID'],'_medium',$model['Ext']) !!}" alt="Testing" class="gallery_images1">
   
    <div class="img_hover_blk fav_img_hover_blk">
      <div class="hvr_icon_div">
       <a class="fancybox" rel="" href="{!! \SiteHelpers::user_albums($model['ID'],'_medium',$model['Ext']) !!}" title=""> 
        <span class="circle_white"><img src="{{ asset('images/expanded_img.png')}}" alt="expanded image" /></span>
       </a>
       <a href="{{ asset('photo/'.$model['ID']) }}"><span class="circle_white"><img src="{{ asset('images/crop_img.png')}}" alt="crop_image" /></span></a>
      </div>
    </div>
  </div>
</div>
 @endforeach

</div>
    @else
    <div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
      <div class="container gallery_blk_container">
        <div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">

          <!-- show upload only for model & agent -->
          <div class="empty_vertical">
          <div class="empty_msg_details clearfix">
            <div class="empty_msg_blk clearfix">
              <div class="heading_blk">
                <div class="heading_details">
                  <h1>Access Denied!</h1>
                  <h2>
                    No more Favorites found
                  </h2>
                </div>
                <div class="file_img_blk">
                  <div class="file_img_div">
                    <!-- <img src="{{asset('images/file_image1.png')}}" alt="file_image" /> -->
                    <i class="fa fa-warning"></i>
                  </div>
                </div>
              </div>
              <!-- <p>You can search on the bar below or return to home page</p> -->
              <div class="pages_blk">
                <div class="msg_previous_page">
                  <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
                </div>
                <div class="msg_next_page">

                  <a href="{{url($url)}}/{{request()->route('id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    @endif
<div class="col-md-12 col-sm-12 col-xs-12 fav_pagination">
<div class="pagination_content text-center">
	<ul class="pagination">
		{!! $pagination->render() !!}
	</ul>
</div>
</div>

</div>
</div>
@include('footer_map')

<script type="text/javascript">
    $(".fancybox").fancybox({});

</script>