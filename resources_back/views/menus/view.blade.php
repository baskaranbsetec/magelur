@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('menus?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('menus/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('menus/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('menus/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Menu Id</td>
						<td>{{ $row->menu_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Parent Id</td>
						<td>{{ $row->parent_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module</td>
						<td>{{ $row->module}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url</td>
						<td>{{ $row->url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Name</td>
						<td>{{ $row->menu_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Type</td>
						<td>{{ $row->menu_type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Role Id</td>
						<td>{{ $row->role_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deep</td>
						<td>{{ $row->deep}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ordering</td>
						<td>{{ $row->ordering}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Position</td>
						<td>{{ $row->position}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Icons</td>
						<td>{{ $row->menu_icons}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Active</td>
						<td>{{ $row->active}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Access Data</td>
						<td>{{ $row->access_data}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Allow Guest</td>
						<td>{{ $row->allow_guest}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Lang</td>
						<td>{{ $row->menu_lang}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop