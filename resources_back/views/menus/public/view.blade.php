<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Menu Id</td>
						<td>{{ $row->menu_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Parent Id</td>
						<td>{{ $row->parent_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Module</td>
						<td>{{ $row->module}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Url</td>
						<td>{{ $row->url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Name</td>
						<td>{{ $row->menu_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Type</td>
						<td>{{ $row->menu_type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Role Id</td>
						<td>{{ $row->role_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deep</td>
						<td>{{ $row->deep}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ordering</td>
						<td>{{ $row->ordering}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Position</td>
						<td>{{ $row->position}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Icons</td>
						<td>{{ $row->menu_icons}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Active</td>
						<td>{{ $row->active}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Access Data</td>
						<td>{{ $row->access_data}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Allow Guest</td>
						<td>{{ $row->allow_guest}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Menu Lang</td>
						<td>{{ $row->menu_lang}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	