@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'menus/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> menus</legend>
									
									  <div class="form-group  " >
										<label for="Menu Id" class=" control-label col-md-4 text-left"> Menu Id </label>
										<div class="col-md-7">
										  <input  type='text' name='menu_id' id='menu_id' value='{{ $row['menu_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Parent Id" class=" control-label col-md-4 text-left"> Parent Id </label>
										<div class="col-md-7">
										  <input  type='text' name='parent_id' id='parent_id' value='{{ $row['parent_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Module" class=" control-label col-md-4 text-left"> Module </label>
										<div class="col-md-7">
										  <input  type='text' name='module' id='module' value='{{ $row['module'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Url" class=" control-label col-md-4 text-left"> Url </label>
										<div class="col-md-7">
										  <input  type='text' name='url' id='url' value='{{ $row['url'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Menu Name" class=" control-label col-md-4 text-left"> Menu Name </label>
										<div class="col-md-7">
										  <input  type='text' name='menu_name' id='menu_name' value='{{ $row['menu_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Menu Type" class=" control-label col-md-4 text-left"> Menu Type </label>
										<div class="col-md-7">
										  <input  type='text' name='menu_type' id='menu_type' value='{{ $row['menu_type'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Role Id" class=" control-label col-md-4 text-left"> Role Id </label>
										<div class="col-md-7">
										  <input  type='text' name='role_id' id='role_id' value='{{ $row['role_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Deep" class=" control-label col-md-4 text-left"> Deep </label>
										<div class="col-md-7">
										  <input  type='text' name='deep' id='deep' value='{{ $row['deep'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ordering" class=" control-label col-md-4 text-left"> Ordering </label>
										<div class="col-md-7">
										  <input  type='text' name='ordering' id='ordering' value='{{ $row['ordering'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Position" class=" control-label col-md-4 text-left"> Position </label>
										<div class="col-md-7">
										  <input  type='text' name='position' id='position' value='{{ $row['position'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Menu Icons" class=" control-label col-md-4 text-left"> Menu Icons </label>
										<div class="col-md-7">
										  <input  type='text' name='menu_icons' id='menu_icons' value='{{ $row['menu_icons'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Active" class=" control-label col-md-4 text-left"> Active </label>
										<div class="col-md-7">
										  <input  type='text' name='active' id='active' value='{{ $row['active'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Access Data" class=" control-label col-md-4 text-left"> Access Data </label>
										<div class="col-md-7">
										  <textarea name='access_data' rows='5' id='access_data' class='form-control '  
				           >{{ $row['access_data'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Allow Guest" class=" control-label col-md-4 text-left"> Allow Guest </label>
										<div class="col-md-7">
										  <input  type='text' name='allow_guest' id='allow_guest' value='{{ $row['allow_guest'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Menu Lang" class=" control-label col-md-4 text-left"> Menu Lang </label>
										<div class="col-md-7">
										  <textarea name='menu_lang' rows='5' id='menu_lang' class='form-control '  
				           >{{ $row['menu_lang'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('menus?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("menus/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop