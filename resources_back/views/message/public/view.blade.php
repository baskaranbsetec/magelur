<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Entry By</td>
						<td>{{ $row->entry_by}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Recipient</td>
						<td>{{ $row->recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Subject</td>
						<td>{{ $row->subject}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Message</td>
						<td>{{ $row->message}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Starred By Sender</td>
						<td>{{ $row->starred_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Starred By Recipient</td>
						<td>{{ $row->starred_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Draft</td>
						<td>{{ $row->draft}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Trashed By Sender</td>
						<td>{{ $row->trashed_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Trashed By Recipient</td>
						<td>{{ $row->trashed_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Read</td>
						<td>{{ $row->read}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deleted By Sender</td>
						<td>{{ $row->deleted_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deleted By Recipient</td>
						<td>{{ $row->deleted_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Attachments</td>
						<td>{{ $row->attachments}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Label By Sender</td>
						<td>{{ $row->label_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Label By Recipient</td>
						<td>{{ $row->label_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>CreatedOn</td>
						<td>{{ $row->createdOn}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>UpdatedOn</td>
						<td>{{ $row->updatedOn}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	