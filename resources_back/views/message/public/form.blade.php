

		 {!! Form::open(array('url'=>'message/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend>  message</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Recipient" class=" control-label col-md-4 text-left"> Recipient </label>
										<div class="col-md-7">
										  <input  type='text' name='recipient' id='recipient' value='{{ $row['recipient'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Subject" class=" control-label col-md-4 text-left"> Subject </label>
										<div class="col-md-7">
										  <input  type='text' name='subject' id='subject' value='{{ $row['subject'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Message" class=" control-label col-md-4 text-left"> Message </label>
										<div class="col-md-7">
										  <textarea name='message' rows='5' id='message' class='form-control '  
				           >{{ $row['message'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Starred By Sender" class=" control-label col-md-4 text-left"> Starred By Sender </label>
										<div class="col-md-7">
										  <input  type='text' name='starred_by_sender' id='starred_by_sender' value='{{ $row['starred_by_sender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Starred By Recipient" class=" control-label col-md-4 text-left"> Starred By Recipient </label>
										<div class="col-md-7">
										  <input  type='text' name='starred_by_recipient' id='starred_by_recipient' value='{{ $row['starred_by_recipient'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Draft" class=" control-label col-md-4 text-left"> Draft </label>
										<div class="col-md-7">
										  <input  type='text' name='draft' id='draft' value='{{ $row['draft'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Trashed By Sender" class=" control-label col-md-4 text-left"> Trashed By Sender </label>
										<div class="col-md-7">
										  <input  type='text' name='trashed_by_sender' id='trashed_by_sender' value='{{ $row['trashed_by_sender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Trashed By Recipient" class=" control-label col-md-4 text-left"> Trashed By Recipient </label>
										<div class="col-md-7">
										  <input  type='text' name='trashed_by_recipient' id='trashed_by_recipient' value='{{ $row['trashed_by_recipient'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Read" class=" control-label col-md-4 text-left"> Read </label>
										<div class="col-md-7">
										  <input  type='text' name='read' id='read' value='{{ $row['read'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Deleted By Sender" class=" control-label col-md-4 text-left"> Deleted By Sender </label>
										<div class="col-md-7">
										  <input  type='text' name='deleted_by_sender' id='deleted_by_sender' value='{{ $row['deleted_by_sender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Deleted By Recipient" class=" control-label col-md-4 text-left"> Deleted By Recipient </label>
										<div class="col-md-7">
										  <input  type='text' name='deleted_by_recipient' id='deleted_by_recipient' value='{{ $row['deleted_by_recipient'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Attachments" class=" control-label col-md-4 text-left"> Attachments </label>
										<div class="col-md-7">
										  <textarea name='attachments' rows='5' id='attachments' class='form-control '  
				           >{{ $row['attachments'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Label By Sender" class=" control-label col-md-4 text-left"> Label By Sender </label>
										<div class="col-md-7">
										  <input  type='text' name='label_by_sender' id='label_by_sender' value='{{ $row['label_by_sender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Label By Recipient" class=" control-label col-md-4 text-left"> Label By Recipient </label>
										<div class="col-md-7">
										  <input  type='text' name='label_by_recipient' id='label_by_recipient' value='{{ $row['label_by_recipient'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('createdOn', $row['createdOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UpdatedOn" class=" control-label col-md-4 text-left"> UpdatedOn </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updatedOn', $row['updatedOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
