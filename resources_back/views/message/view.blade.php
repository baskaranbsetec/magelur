@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('message?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('message/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('message/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('message/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Entry By</td>
						<td>{{ $row->entry_by}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Recipient</td>
						<td>{{ $row->recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Subject</td>
						<td>{{ $row->subject}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Message</td>
						<td>{{ $row->message}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Starred By Sender</td>
						<td>{{ $row->starred_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Starred By Recipient</td>
						<td>{{ $row->starred_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Draft</td>
						<td>{{ $row->draft}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Trashed By Sender</td>
						<td>{{ $row->trashed_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Trashed By Recipient</td>
						<td>{{ $row->trashed_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Read</td>
						<td>{{ $row->read}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deleted By Sender</td>
						<td>{{ $row->deleted_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Deleted By Recipient</td>
						<td>{{ $row->deleted_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Attachments</td>
						<td>{{ $row->attachments}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Label By Sender</td>
						<td>{{ $row->label_by_sender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Label By Recipient</td>
						<td>{{ $row->label_by_recipient}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>CreatedOn</td>
						<td>{{ $row->createdOn}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>UpdatedOn</td>
						<td>{{ $row->updatedOn}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop