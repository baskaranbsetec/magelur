
 <script src="{{ asset('magelur/js/uploader.js')}}" type="text/javascript"></script>

<!-- Top DIV -->
<div class="topUpload">

	<!-- Session Messages -->
	@if (Session::has('error'))
		<div class="error">
			<p>{{ Session::get('error') }}</p>
		</div>
	@endif

	@if (Session::has('success'))
		<div class="success">
			<p>{!! Session::get('success') !!}</p>
		</div>
	@endif

	@if (count($errors) > 0)
	    <div class="error">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	<!-- Start Upload Form -->
	<form method="POST" action="{{ url('/upload') }}" enctype="multipart/form-data" id="uploaderForm">
		<meta name="csrf-token" content="{{ csrf_token() }}">


	@if ( ($settings_upload->upload_type == 0) OR ($settings_upload->upload_type == 2) )
	<!-- Drag Drop Zone -->
	<div class="dragzone">

		<div class="box">
			<input type="file" name="video_file" id="video_file" class="inputfile inputfile-5">
			<!-- <input type="file"  id="video_file" class="inputfile inputfile-5" name="files[]"  multiple="multiple"> -->
			<label for="video_file"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></path></svg></figure> <span></span></label>
		</div>

		<div class="drag-footer">
			<div class="para">

				@if (App\Helper\Helper::is_vip())
				<!-- Storage Counting -->
				<div class="progress">
					<div class="progress-bar progress-bar-success" style="width: {{ App\Helper\Helper::storage_percent(App\Helper\Helper::settings_vip()->storage_space, $total_videos_size) }}%">
				  	</div>
				</div>
				@else
				<!-- Storage Counting -->
				<div class="progress">
					<div class="progress-bar progress-bar-success" style="width: {{ App\Helper\Helper::storage_percent($settings_upload->storage_space, $total_videos_size) }}%">
				  	</div>
				</div>
				@endif
				<div class="storage">
					
				</div>

			</div>

			@if (App\Helper\Helper::is_vip())
			<p class="max-size">THE MAXIMUM FILE SIZE IS <b>{{ App\Helper\Helper::settings_vip()->video_size }} MB</b></p>
			@else
			<p class="max-size">{{ Lang::get('main.max_file_size') }} <b>{{ $settings_upload->video_size }} MB</b></p>
			@endif
		</div>

	</div>

	@endif

	<!-- Bottom Upload Div -->
	<div class="vid_details">

		<!-- Image, Title, Description, Tags, Keyworkds... -->
		<div class="video_info">

			@if ( ($settings_upload->upload_type == 1) OR ($settings_upload->upload_type == 2))

			<input type="text" placeholder="upload from: dailymotion, vimeo or youtube " name="video_sites" id="video_sites" onkeyup="hideCover();">

			@else

			<input type="hidden" id="video_sites" >

			@endif

			<!-- Video Title -->
			<input type="text" placeholder="{{ Lang::get('main.video_title') }}" name="title" id="title">

			@if (App\Helper\Helper::is_admin())
		
			<!-- Only VIPs -->
            <div class="select_group">
                <select class="form-control" name="vip" id="vip">
                    <option selected="" disabled="">Who can watch this video?</option>

                    <option value="1">Only VIP</option>
                    <option value="0">All Users</option>

                </select>
            </div>

			@endif

			<!-- Video Tags -->
			<input type="text" placeholder="{{ Lang::get('main.video_tags') }}" name="tags" id="tags">

			<!-- List Of Categories -->
            <div class="select_group">
                <select class="form-control" name="category" id="category">
                    <option selected="" disabled="">{{ Lang::get('main.select_category') }}</option>

                    @if (count($categories))
                    @foreach ($categories as $category)
                    	<option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                    @endif

                </select>
            </div>

			<!-- Video Description -->
			<textarea placeholder="{{ Lang::get('main.video_description') }}" name="description" id="description"></textarea>

			<!-- Upload Cover For Video -->
			<div id="coverIMG">
				<input style="display: none;" name="cover" type="file" id="fileUP">
				<div data-name="cover" class="fileUpload">
					<div class="fileBtn">{{ Lang::get('main.upload_cover') }}</div>
					<div class="fileName"></div>
				</div>

				<!-- Allowed Extensions and Size -->
				<span class="helper">JPG, JPEG, PNG MAX SIZE IS 3MB</span>
			</div>

			<!-- Start Upload -->
			<button type="submit" >{{ Lang::get('main.start_uploading') }}</button>
		</div>
		
	</div>
	</form>
</div>

<!-- Validate Video Size with JS -->
<script type="text/javascript">

	@if (App\Helper\Helper::is_vip())
	var MaxFilesize = {{ App\Helper\Helper::covert_to_bytes(App\Helper\Helper::settings_vip()->video_size) }};
	@else 
	var MaxFilesize = {{ App\Helper\Helper::covert_to_bytes($settings_upload->video_size) }};
	@endif
	

	function hideCover() {
		$('#coverIMG').hide();
	}
</script>

