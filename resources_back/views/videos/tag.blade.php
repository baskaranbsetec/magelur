
<!-- SEARCH PAGE -->
<div class="search_page tags_search_page">
<div class="tags_page_boxshadow">
	
	<!-- List of Results -->
	<div class="search_results">
		
		<!-- Search Header -->
		<div class="search_header">
			<p class="tags_heading">Tag Videos ({{ number_format(count($results)) }})</p>
		</div>

		<!-- Search Content -->
		<div class="search_body">
			
			@if (count($results) > 0)
			@foreach ($results as $video)
			<!-- Video Box -->
			<div class="search_video col-md-3 col-sm-4 col-xs-6">
				<div class="thumb_img_blk">
				
				<!-- Video Thumb & Duration -->
				<div class="video_thumb">
					<a href="{{ url('/video') }}/{{ $video->video_id }}">
					<div class="tags_video_thumbimg">
						<img src="{{ $video->thumb }}" alt="" class="tags_video_img">
						<div class="thumb_play_icon">
						<i class="fa fa-play"></i></div>
						<span class="video_time">{{ $video->duration }}</span>
					</div>
						
					</a>
				</div>

				<!-- Video Details -->
				<div class="video_details">
					
					<!-- Video Title -->
					<div class="vid_title">
						<a href="{{ url('/video') }}/{{ $video->video_id }}"><span class="video_title_name">{{ $video->title }}</span></a>
						<!-- Video Date & Views -->
					<div class="vid_date_views">
						<!-- <p>{{ App\Helper\Helper::change_date_format($video->created_at) }} - {{ App\Helper\Helper::count_views($video->video_id) }} views</p> -->
						<p><span><i class="fa fa-eye"></i></span>{{ App\Helper\Helper::count_views($video->video_id) }}</p>
					</div>
						
					</div>

					<!-- Video Uploader -->
					<div class="vid_uploader">
						<a href="{{ url('/user') }}/{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}"><span class="video_author_name">
						<i class="fa fa-user"></i>{{ App\Helper\Helper::channel_by_id($video->user_id,$video->user_type) }}</span></a>
						@if (App\Helper\Helper::is_verified($video->user_id,$video->user_type) == TRUE)
						<!-- <span class="verified" data-balloon="Verified" data-balloon-pos="top">
							<img src="{{ url('/') }}/content/assets/img/verified-opacity.png" alt="" >
						</span> -->
						@endif
					</div>
					<div class="video_upload_date">
						<p><span><i class="fa fa-calender"></i></span>{{ App\Helper\Helper::change_date_format($video->created_at) }}</p>
					</div>

					<!-- Video Description -->
					<!-- <div class="vid_description">
						<p>{{ $video->description }}</p>
					</div> -->

					<!-- Is New OR HD -->
<!-- 					<div class="vid_new_hd">

						@if (App\Helper\Helper::is_new($video->video_id) == TRUE)
						<button>New</button>
						@endif

						@if (App\Helper\Helper::video_quality($video->video_id) == TRUE)
						<button>HD</button>
						@endif

					</div> -->

				</div>
				</div>

			</div>
			@endforeach
			@endif

		</div>

		@if (count($results) > 0)
		
			<div style="text-align:center;">
                
            </div>

		@endif

	</div>

	<!-- AD Code Here -->
	<div class="ad_code">
		<!-- Sidebar Ad Code -->
		@if ($ads->sidebar)
		<div class="search_ad">
			{!! $ads->sidebar !!}
		</div>
		@endif
	</div>

</div>
</div>


<style>
.page-header{
	display:none;
}
.map{
	display:none;
}
</style>