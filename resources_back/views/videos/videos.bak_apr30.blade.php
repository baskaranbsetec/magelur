    <script src="{{ asset('magelur/js/uploader.js')}}" type="text/javascript"></script>
<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>CRAVE VIDEO</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<!-- Home Page -->
<div id="home">
<div class="video_listing_page">
<div class="agency_video_blk">
<div class="container">
<div class="agent_video_content col-md-12 col-sm-12 col-xs-12">
<div class="crave_image_div">
<img src="http://192.168.1.65:81/magelur/public/uploads/agency_profile/1523098079-57358215.jpg" alt="crave_image"/>
</div>
<div class="agency_video_title">
<h1>AGENCY VIDEO - CRAVE</h1>
</div>
</div>
</div>
</div>
<div class="agency_video_lisiting">
<div class="container">
<div id="videosScroll">

        @if (count($videos) > 0)
        @foreach ($videos as $video)

        <!-- Random Video Box -->
        <div class=" gallery_video">
                    <div class="magelur_gallery_div magelur_gallery_video_div col-md-6 col-sm-6 col-xs-12">
                        <div class="magelur_image_blk magelur_video_blk">
                            <!-- Video Thumb -->
                            <img src="images/gallery_video3.png" class="gallery_images1 gallery_video_images1">
                            <!-- <img src="{{ $video->thumb }}" title="{{ $video->title }}"> -->
                            <div class="img_hover_blk">
                                <div class="gallery_video_hvr">
                                    <span class="video_bg_clr"><i class="fa fa-play"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                   

                    @if ($video->vip == 1)
                    <!-- Only VIP user -->
                    <div class="vip_only">
                        <h6>VIP Users</h6>
                    </div>
                    @endif

                    <a href="{{ url('/video') }}/{{ $video->video_id }}" class="hover-posts">
                        <span></span>
                    </a>
                    <div class="video-stats clearfix" style="display:none">

                        <!-- Video quality -->
                        @if (App\Helper\Helper::video_quality($video->video_id) == TRUE)
                        <div class="thumb-stats pull-left">
                            <h6>HD</h6>
                        </div>
                        @endif

                        <div class="thumb-stats pull-right">
                            <span>{{ $video->duration }}</span>
                </div>
                <div class="post-des" style="display:none">
                    <h6><a href="{{ url('/video') }}/{{ $video->video_id }}">{{ $video->title }}</a></h6>
                    <div class="post-stats clearfix">
                        <p class="pull-left">
                            <span><a class="uploader" href="{{ url('/user') }}/{{ App\Helper\Helper::username_by_id($video->user_id) }}">{{ App\Helper\Helper::channel_by_id($video->user_id) }}</a></span>
                        </p>
                        <p class="pull-right">
                            <span class="views">{{ App\Helper\Helper::count_views($video->video_id) }}</span>
                        </p>
                    </div>
                    <div class="post-summary">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
        

        @else
        <div class="no_data_msg">
            <p>Oops! No Videos Right Now!</p>
        </div>
        @endif


<div class="load_more_btn text-center col-md-12 col-sm-12 col-xs-12">
<button><span>LOAD MORE</span></button>
</div>
</div>
</div>
</div>
    
    </div>

</div>

    

</div>