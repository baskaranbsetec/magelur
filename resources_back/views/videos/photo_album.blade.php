<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- SEARCH PAGE -->
<div class="search_page tags_search_page">
	<div class="tags_page_boxshadow">
		
		<!-- List of Results -->
		<div class="search_results">
			@if (count($results) > 0)
			<!-- Search Header -->
			<div class="search_header">
				<p class="tags_heading">{{$album->title}} - <span>Albums ({{ number_format(count($results)) }})</span></p>
			</div>

			<!-- Search Content -->
			<div class="search_body">
				<input type="hidden" name="tot_img" id="tot_img" value="{{ $total_images }}">
				<input type="hidden" name="disp_img" id="disp_img" value="{{ count($results) }}">
				<input type="hidden" name="userid" id="userid" value="{{ $userId }}">
				<input type="hidden" name="albumuserid" id="albumuserid" value="{{ $album->ownerid }}">
				@foreach ($results as $image)
				<!-- Video Box -->
				<div class="search_video col-md-3 col-sm-4 col-xs-6">
					<div class="thumb_img_blk">
						
						<!-- Video Thumb & Duration -->
						<div class="video_thumb">
							<a href="javascript:void(0)">
								<div class="tags_video_thumbimg">
									<img src="{!! \SiteHelpers::user_albums($image->ID,'_medium',$image->Ext) !!}" alt="" class="tags_video_img">
									<div class="thumb_play_icon">
									<a class="fancybox" rel="gallery1" href="{!! \SiteHelpers::user_albums($image->ID,'_medium',$image->Ext) !!}" title="">
									<span class="circle_white"><img src="{{ asset('images/expanded_img.png')}}" alt="expand_image" /></span></a>
        							<!-- <a href="{{ asset('photo/'.$image->MediaUri) }}"><span class="circle_white"><img src="{{ asset('images/crop_img.png')}}" alt="crop_image" /></span></a> -->
        							<a href="{{ asset('photo/'.$image->ID) }}"><span class="circle_white"><img src="{{ asset('images/crop_img.png')}}" alt="crop_image" /></span></a>
									</div>
										<span class="video_time"></span>
									</div>
								</a>
							</div>

							<!-- Video Details -->
							<div class="video_details">
								
								<!-- Video Title -->
								<div class="vid_title">
									<a href="{{ url('/photo') }}/{{ $image->MediaUri }}"><span class="video_title_name">{{ $image->Title }}</span></a>
									<!-- Video Date & Views -->
									<div class="vid_date_views">
										
										<p><span><i class="fa fa-eye"></i></span>{{ App\Helper\Helper::count_photo_views($image->ID) }}</p>
									</div>
									
								</div>

								<!-- Video Uploader -->
								<div class="vid_uploader">
									<a href="{{ App\Helper\Helper::get_user_profile_link($image->OwnerId,$image->user_type) }}"><span class="video_author_name">
										<i class="fa fa-user"></i>
										@if(App\Helper\Helper::channel_by_id($image->OwnerId,$image->user_type))
										{{ App\Helper\Helper::channel_by_id($image->OwnerId,$image->user_type) }}
										@else
										Unnamed
										@endif
									</span>
								</a>
							</div>
							<div class="video_upload_date">
								<p><span><i class="fa fa-calender"></i></span>{{ App\Helper\Helper::change_date_format($image->Created) }}</p>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@else
			<div class="magelur_gallery wow bounce col-md-12 col-sm-12 col-xs-12">
			  <div class="empty_vertical">
			  <div class="empty_msg_details clearfix">
			  <div class="empty_msg_blk clearfix">
			    <div class="heading_blk">
			      <div class="heading_details">
			        <h1>No Images Found!</h1>
			        <h2>
			          You Have Not Uploaded Any Images Under Your Gallery Or Admin Needs to Approve your Images.Go to Album page and Click on the image upload button to add images on gallery
			        </h2>
			      </div>
			      <div class="file_img_blk">
			        <div class="file_img_div">
			          <i class="fa fa-warning"></i>
			        </div>
			      </div>
			    </div>
			    <div class="pages_blk">
			      <div class="msg_previous_page">
			        <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
			      </div>
			      <div class="msg_next_page">

			      		@if(\Session::get('logged_id') == \Session::get('agent_id') && \Session::get('user_type') == 'agent'  )

			      		<a href="{{ url('agent-dashboard') }}/{{\Session::get('agent_id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
			      		@elseif(\Session::get('logged_id') == \Session::get('model_id') && \Session::get('user_type') == 'model'  )
			      		<a href="{{ url('model-dashboard') }}/{{\Session::get('model_id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
			      		@else
			      		<a href="{{ url('dashboard') }}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>

			      		@endif
			       </div>
			    </div>
			  </div>
			</div>
			</div>
			</div>
			
		</div>
			@endif
			<!-- @if (count($results) > 0)
			
			<div style="text-align:center;">
				
			</div>

			@endif -->

		</div>

	</div>
</div>
<script>
$(".fancybox").fancybox({});
if($('#tot_img').val()!=$('#disp_img').val() && $('#userid').val()==$('#albumuserid').val())
{
	swal("All Your Album Images will be Visible only after Admin approves your Images");
}
</script>

<style>
/*.page-header{
	display:none;
}*/
.map{
	display:none;
}
</style>