  <script src="{{ asset('magelur/js/fullscreen.js')}}" type="text/javascript"></script>
    <script src="{{ asset('magelur/js/videoPlayer.js')}}" type="text/javascript"></script>
    <script src="{{ asset('magelur/js/playlist.js')}}" type="text/javascript"></script>

<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>CRAVE VIDEO</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<div class="watch_page">

<div class="col-md-12 col-sm-12 col-xs-12 video_upload_page">
<div class="container">
	<!--####################### Left Side DIV #######################-->

	<div class="col-md-8 col-sm-12 col-xs-12 video_upload_left ">
	<div class="w_leftSide">
		
		@if (!App\Helper\Helper::is_vip())


		<div class="video_upload_blk1 col-md-12 col-sm-12 col-xs-12 nopadding_div">
		<!-- Video -->
		  	<div id="parent" style="width:100%;height:auto;box-shadow: 0 1px 2px rgba(0,0,0,.1);top:0px;left:0px;position: relative;">

		  		@if (!is_null($video->vimeo_id))
				
				<!-- Vimeo Video -->
				<iframe src="https://player.vimeo.com/video/{{ $video->vimeo_id }}" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		  		@elseif (!is_null($video->dailymotion_id))

		  		<!-- Dailymotion Video -->
		  		<iframe frameborder="0" width="100%" height="360" src="//www.dailymotion.com/embed/video/{{ $video->dailymotion_id }}" allowfullscreen></iframe>
				
				@else
				<div id="watch_video_now" style="position: relative;"></div> 

				@endif
			</div>

			<!-- Skip Ads Button -->
			<div class="skip_ads_msg">
				You can skip advertisements in <span id="countdown"></span> seconds
			</div>

			<!-- Skip Button -->
			<div class="skip_now_ads">
				<button id="SkipAdsNow">Skip Ad</button>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk nopadding_div" style="display:none">
				<!-- Video header -->
		<div class="video_header">
			
			<!-- Video Title -->
			<div class="video_title">
				<h2>{{ $video->title }}</h2>
			</div>

			<!-- User panel -->
			<div class="user_panel">

				<!-- User channel avatar -->
				<div class="channel_avatar">
					<img src="{{ App\Helper\Helper::user_avatar($video->user_id) }}">
				</div>

				<!-- Channel name and subscribers counter -->
				<div class="channel_counter">
					
					<!-- Channel name -->
					<div class="channel_name">
						<a href="{{ url('/user') }}/{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}">{{ App\Helper\Helper::channel_by_id($video->user_id) }}</a>

						<!-- User Verification -->
						@if (App\Helper\Helper::is_verified($video->user_id) == TRUE)
						<span class="verified" data-balloon="{{ Lang::get('main.verified') }}" data-balloon-pos="right">
							<img id="verified" src="{{ url('/') }}/content/assets/img/verified-opacity.png" alt="" >
						</span>
						@endif
					</div>

					<!-- Subscribers counter -->
					<div class="subscribers_counter" style="float: left;">

						<!-- Subscribe Button -->
						@if (Auth::check())

						@if (App\Helper\Helper::is_subscribe($video->user_id) == TRUE)
						<button id="unsubscribeBTN" class="button_original">style="background-color: #C0C0C0;background-image: url({{ url('/')  }}/content/assets/img/unsubscribe.png);" data-user="{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}">{{ Lang::get('main.unsubscribe') }}</button>
						@else
						<button class="button_original" style="background-color: rgb(27, 216, 183);background-image: url({{ url('/')  }}/content/assets/img/subscribe.png);" id="subscribeBTN" data-user="{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}">{{ Lang::get('main.subscribe') }}</button>
						@endif

						@else
						<button class="button_original" style="background-color: rgb(27, 216, 183);background-image: url({{ url('/')  }}/content/assets/img/subscribe.png);" id="subscribeBTN" data-user="{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}">{{ Lang::get('main.subscribe') }}</button>
						@endif

						<span id="totalSubscribers">{{ App\Helper\Helper::count_subscribers($video->user_id) }}</span>
					</div>
				</div>
				
			</div>

			<!-- Quick Links -->
			<div class="video_btns">

				<!-- Views and votting counter -->
				<div class="viewVoteCounter">
					<!-- Views Counter -->
					<div class="count_views">
						<h2>{{ number_format($views) }}</h2>
					</div>
					<!-- Votting  Line -->
					<div class="sparkbars">
						<div class="sparkbars-likes" style="width: {{ App\Helper\Helper::likes_percent($likes, $dislikes) }}%;"></div>
						<div class="sparkbars-dislikes" style="width: {{ App\Helper\Helper::dislikes_percent($likes, $dislikes) }}%;"></div>
					</div>
					<!-- Votting System -->
					<div class="votting_system">

						<!-- Like Button -->
						@if (Auth::check())
						@if (App\Helper\Helper::already_liked($video->video_id) == TRUE)
						<button style="opacity:1;" class="vote_like" data-video="{{ $video->video_id }}" id="like">
							{{ $likes }}
						</button>
						@else
						<button class="vote_like" data-video="{{ $video->video_id }}" id="like">
							{{ $likes }}
						</button>
						@endif
						@else
						<button class="vote_like" data-video="{{ $video->video_id }}" id="like">
							{{ $likes }}
						</button>
						@endif

						<!-- Dislike Button -->
						@if (Auth::check())
						@if (App\Helper\Helper::already_disliked($video->video_id) == TRUE)
						<button style="opacity:1;" data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
							{{ $dislikes }}
						</button>
						@else
						<button data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
							{{ $dislikes }}
						</button>
						@endif
						@else
						<button data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
							{{ $dislikes }}
						</button>
						@endif
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="adblock-detected" id="AdblockDetected">
			<img src="{{ url('/') }}/content/assets/img/sad.png" alt="">
			<p>
				We've detected that you are using ad block or some other ad blocking software. In order to keep our site free, this site is sponsored in party by advertisements.Please consider disabling ad blocker on this website if you enjoyed the content. Thank you!
			</p>
		</div>
		
		<!-- HTML Ads -->
		<div class="col-md-12 col-sm-12 col-xs-12 nopadding_div">
		<div class="html-ads" id="HtmlAds" style="background-image: url({{ $video->preview }})">
			
			<!-- Progress Bar Timer -->
			<div class="progress_timer"></div>

			<!-- Shadow -->
			<div class="html_ads_shadow"></div>

			<div class="h_ads_boxes">
				
				<!-- Left Ad -->
				<div class="h_aLeft">
					{!! $ads->html_left !!}
				</div>

				<!-- Right Ad -->
				<div class="h_aRight">
					{!! $ads->html_right !!}
				</div>

			</div>

			
		</div>
		</div>

		@endif


		<div class="col-md-12 col-sm-12 col-xs-12 nopadding_div">
		<!-- Video Div -->
		<div class="w_video" id="w_video">
			<script type="text/javascript" charset="utf-8">

			$(document).ready(function($){

				videoPlayer = $("#watch_video_now").Video({

					instanceName:"player1",
					autohideControls:2, 
					hideControlsOnMouseOut:"No",
					videoPlayerWidth:700,
					videoPlayerHeight:360,
					responsive:true,
					playlist:"Off",
					@if ($settings_player->autoplay == 0)
					autoplay:true,
					@else 
					autoplay:false,
					@endif
					videoPlayerShadow:"off",
					loadRandomVideoOnStart:"No",
					shuffle:"No",
					posterImg:"{{ $video->preview }}",
					onFinish:"Stop video",
					nowPlayingText:"No",
					fullscreen:"Fullscreen native",
					rightClickMenu:false,
					hideVideoSource:true,
					showAllControls:true,
					@if ($settings_player->skip_ad == 0)
					allowSkipAd:true,
					@else
					allowSkipAd:false,
					@endif
					infoShow:"No",
					shareShow:"No",
					logoShow:"No",
					logoClickable:"Yes",
					logoPath:"",
					logoGoToLink:"",
					logoPosition:"bottom-right",
					embedShow:"No",

					videos:[
						{
							@if ($video->youtube_id != NULL)
							videoType:"youtube",
							youtubeID:"{{ $video->youtube_id }}",
							@else 
							videoType:"HTML5",
							@endif

							title:"AD 5 sec + Pieces After Effects project",

							mp4:"{{ $video->video_path }}",

							imageUrl:"",
							imageTimer:2,
							@if (App\Helper\Helper::is_vip())
							prerollAD:"no",
							@else
								@if ($ads->preroll_ad)
								prerollAD:"yes",
								@else
								prerollAD:"no",
								@endif
							@endif
							prerollGotoLink:"{{ $ads->preroll_go }}",
							preroll_mp4:"{{ $ads->preroll_ad }}",
							prerollSkipTimer:5,
							midrollAD:"no",
							midrollAD_displayTime:"00:10",
							midrollGotoLink:"",
							midroll_mp4:"",
							midrollSkipTimer:5,	
							@if (App\Helper\Helper::is_vip())
							postrollAD:"no",
							@else
								@if ($ads->end_ad)
								postrollAD:"yes",
								@else
								postrollAD:"no",
								@endif
							@endif
							postrollGotoLink:"{{ $ads->end_go }}",
							postroll_mp4:"{{ $ads->end_ad }}",
							postrollSkipTimer:5,
							popupImg:"{{ $ads->popup_ad }}",
							@if (App\Helper\Helper::is_vip())
							popupAdShow:"no",
							@else
								@if ($ads->popup_ad)
								popupAdShow:"yes",
								@else
								popupAdShow:"no",
								@endif
							@endif
							popupAdStartTime:"00:10",
							popupAdEndTime:"00:30",
							popupAdGoToLink:"{{ $ads->popup_go }}",
							thumbImg:"{{ $video->thumb }}"
						}

					]
				});	
					
				});

		  </script>

		
		</div>
		</div>


		
		<!-- Share Video -->
		<div class="video_share" id="video_share" style="display:none">
			<!-- Share on Social Media -->
			<div class="socialShare">
				
			</div>
		</div>


		@if (!App\Helper\Helper::is_vip())
		@if ($ads->ad728)
		<!-- Adsense Code -->
		<div class="adsense_box" id="ad728Code">
			{!! $ads->ad728 !!}
		</div>
		@endif
		@endif

		<!-- Video Comments -->
		<div class="comments_box">
			<div class="comments_counter">
				<p>{{ Lang::get('main.comments') }} <span>•</span> {{ $total_comments }}</p>
			</div>

			<div class="cm_box">
				<div class="cm_avatar">
					@if (Auth::check())
					<img src="{{ App\Helper\Helper::user_avatar(Auth::user()->id) }}" alt="">
					@else
					<img src="{{ url('/') }}/content/uploads/avatars/avatar.png" alt="">
					@endif
				</div>

				

				<div class="clear"></div>
			</div>

			
		</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_list_blk nopadding_div">
		<!-- List of Comments -->
		    <div class="comments-container" id="commentScroll">

		        <ul class="comments-list">
		        	@if (count($comments) > 0)
		        	@foreach ($comments as $comment)
		        	
		            <li class="commentScroll upload_comment_blk upload_comment_list">
		                <div class="comment-main-level">
		                    <!-- Avatar -->
		                    <div class="comment-avatar" style="display:none">
		                    	<img src="{{ App\Helper\Helper::user_avatar($comment->user_id) }}" alt=""></div>
			                    <!-- Comment Details -->
			                    <div class="comment-box">
			                        <div class="comment-head">

			                        	<!-- Uploader -->
			                            <h6 class="comment-name by-author"><a href="{{ url('/user') }}/{{ App\Helper\Helper::username_by_id($comment->user_id,$comment->user_type) }}" target="_blank">{{ App\Helper\Helper::channel_by_id($comment->user_id) }}</a></h6>

			                            <!-- Comment Date -->
			                            <span class="cm_date">{{ App\Helper\Helper::change_date_format($comment->created_at) }}</span>
			                        </div>
			                        <div class="comment-content">
			                            {!!
			                             nl2br( App\Helper\Helper::emoticons(App\Helper\Helper::active_links($comment->comment, $video->video_id))) 
			                            !!}
			                        </div>
			                    </div>
		                </div>
		            </li>

		            @endforeach

		            <div style="display:none;">
		            	
		            </div>
		            
		            @else
		            <div class="no_data_msg">
		            	<p>{{ Lang::get('main.no_comment') }}</p>
		            </div>
		            @endif

		        </ul>
		    </div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk">
		<div class="cm_textarea">

				<form action="#" method="POST" id="SubmitComment">
				<meta name="csrf-token" content="{{ csrf_token() }}">
				<div class="text_area_video">
					<textarea name="comment" rows="5" placeholder="Enter Your Comments" id="commentsBox"></textarea>
				</div>
				<div class="text-center">
					<button id="postComment" data-video="{{ $video->video_id }}" type="submit">{{ Lang::get('main.post_comment') }}</button>
				</div>
				</form>

				</div>
		</div>
				<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk video_upload_desc_blk" style="display:none">
					<!-- Video Description -->
		<div class="video_description">
			
			<!-- Publish Date -->
			<div class="publish_date">
				<strong>{{ Lang::get('main.uploaded_on') }} {{ App\Helper\Helper::description_date($video->created_at) }}</strong>
			</div>

			<!-- Embed & Flag Video -->
			<div class="embed_flag">
				<a class="embed_vid" id="shareVid">{{ Lang::get('main.share_video') }}</a>
				<a href="#" class="flag_vid" id="reportVid" data-video="{{ $video->video_id }}">{{ Lang::get('main.flag_video') }}</a>
			</div>

			<div class="v_description" id="videoDescription">
				
				<!-- Embed Video -->
				<div class="embedVideo">

					<ul>

						<!-- Share on Facebook -->
						<li><a href="http://www.facebook.com/sharer.php?u={{ url('/watch') }}/{{ $video->video_id }}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/facebook.png">
						</a></li>

						<!-- Share on Twitter -->
						<li><a href="https://twitter.com/share?url={{ url('/watch') }}/{{ $video->video_id }}&amp;text={{$video->title}}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/twitter.png">
						</a></li>

						<!-- share on google plus -->
						<li><a href="https://plus.google.com/share?url={{ url('/watch') }}/{{ $video->video_id }}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/google.png">
						</a></li>

						<!-- Share on Reddit -->
						<li><a href="http://reddit.com/submit?url={{ url('/watch') }}/{{ $video->video_id }}&amp;title={{$video->title}}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/reddit.png">
						</a></li>

						<!-- share on Tumblr -->
						<li><a href="http://www.tumblr.com/share/link?url={{ url('/watch') }}/{{ $video->video_id }}&amp;title={{$video->title}}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/tumblr.png">
						</a></li>

						<!-- share on Linkedin -->
						<li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ url('/watch') }}/{{ $video->video_id }}" target="_blank">
							<img src="{{ url('/') }}/content/assets/img/social/linkedin.png">
						</a></li>

					</ul>
					<textarea><embed src="{{ url('/') }}/embed/{{ $video->video_id }}" height="400" width="600"></textarea>
				</div>

				<p>{!! nl2br(App\Helper\Helper::active_links($video->description, $video->video_id)) !!}</p>

				<div style="position: relative;margin-top: 25px;padding-bottom: 30px;">
					<ul class="tags">

						<!-- Video Tags -->
						{{ App\Helper\Helper::video_tags($video->video_id) }}

					</ul>
				</div>

			</div>

			

			<div class="show_more">
				<a id="showMore">{{ Lang::get('main.show_more') }}</a>
			</div>

		</div>
		</div>

	</div>
	</div>
	
	<!--####################### Right Side DIV #######################-->
	<div class="col-md-4 col-sm-12 col-xs-12 video_upload_right">
		<div class="related_videos">
			
			<!-- Related Videos -->
			
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
				<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ asset('content/uploads/avatars/event_image1.jpg')}}" alt="video_images" />
					<span>2:06</span>
				</div>

				<div class="r_video_info">
					<a href="javascript:void(0);" class="r_title">test</a>
					<a href="javascript:void(0);" class="r_channel">Channel Name</a>
					<a class="r_views">1 views</a>
				</div>
				</div>
			</div>
			<div class="text-center pagination_content"> <ul class="pagination"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="javascript:void(0)">2</a></li><li><a href="model-gallery?page=3">3</a></li><li><a href="javascript:void(0)">4</a></li><li><a href="javascript:void(0)">5</a></li><li><a href="javascript:void(0)" rel="next">»</a></li></ul></div>
			
			
		</div>
	</div>
	</div>
	</div>

</div>



@if (!App\Helper\Helper::is_vip())
</div>
<script>
	
	window.onload = function() {

		$('#w_video').css('display', 'none');

		var videoPlayerTag = document.getElementsByTagName( 'video' )[0];

		videoPlayerTag.pause();

	    var countdownElement = document.getElementById('countdown'),
	        seconds          = 10,
	        second           = 0,
	        interval;

	    interval = setInterval(function() {

	        countdownElement.innerHTML = seconds - second;

	        if (second >= seconds) {
	            clearInterval(interval);
	            $('.skip_ads_msg').hide('fast');
	            $('.skip_now_ads').fadeIn('slow');
	        }

	        second++;
	    }, 1000);
	}

	/************ Click skip Ad **************/
	$('#SkipAdsNow').click(function(){
		// Hide Ads 
		$('#HtmlAds').fadeOut('fast');

		// Show Video 
		$('#w_video').fadeIn('slow');

		// Play Video
		videoPlayerTag.play();
	});

</script>
@endif


