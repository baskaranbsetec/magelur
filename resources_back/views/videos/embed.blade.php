<!-- HTML5 Player Style -->
<link rel="stylesheet" href="{{ url('/') }}/content/assets/css/html5player.css" type="text/css" media="screen"/>

<!-- HTML5 Player Font Awesome -->
<link rel="stylesheet" href="{{ url('/') }}/content/assets/css/player-font-awesome.css" type="text/css" media="screen"/>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="{{ url('/') }}/content/assets/js/fullscreen.js"></script>

<script src="{{ url('/') }}/content/assets/js/videoPlayer.js" type="text/javascript"></script>

<script src="{{ url('/') }}/content/assets/js/playlist.js" type="text/javascript"></script>



<!-- Video Div -->
<div class="w_video">
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function($){

		videoPlayer = $("#embed_video_player").Video({

			instanceName:"player1",
			autohideControls:2, 
			hideControlsOnMouseOut:"No",
			videoPlayerWidth:700,
			videoPlayerHeight:360,
			responsive:true,
			playlist:"Off",
			autoplay:false,
			videoPlayerShadow:"off",
			loadRandomVideoOnStart:"No",
			shuffle:"No",
			posterImg:"{{ $video->preview }}",
			onFinish:"Stop video",
			nowPlayingText:"No",
			fullscreen:"Fullscreen native",
			rightClickMenu:false,
			hideVideoSource:true,
			showAllControls:true,
			allowSkipAd:true,
			infoShow:"No",
			shareShow:"No",
			logoShow:"Yes",
			logoClickable:"Yes",
			logoPath:"{{ $settings_player->logo_path }}",
			logoGoToLink:"{{ url('/watch') }}/{{ $video->video_id }}",
			logoPosition:"bottom-right",
			embedShow:"No",

			videos:[
				{
					videoType:"HTML5",

					title:"AD 5 sec + Pieces After Effects project",

					mp4:"{{ $video->video_path }}",

					imageUrl:"",
					imageTimer:2,
					@if ((Auth::check()) AND (Auth::user()->id == 1))
					prerollAD:"no",
					@elseif ($ads->preroll_ad)
					prerollAD:"yes",
					@else
					prerollAD:"no",
					@endif
					prerollGotoLink:"{{ $ads->preroll_go }}",
					preroll_mp4:"{{ $ads->preroll_ad }}",
					prerollSkipTimer:5,
					midrollAD:"no",
					midrollAD_displayTime:"00:10",
					midrollGotoLink:"",
					midroll_mp4:"",
					midrollSkipTimer:5,	
					@if ((Auth::check()) AND (Auth::user()->id == 1))
					postrollAD:"no",
					@elseif ($ads->end_ad)
					postrollAD:"yes",
					@else
					postrollAD:"no",
					@endif
					postrollGotoLink:"{{ $ads->end_go }}",
					postroll_mp4:"{{ $ads->end_ad }}",
					postrollSkipTimer:5,
					popupImg:"{{ $ads->popup_ad }}",
					@if ((Auth::check()) AND (Auth::user()->id == 1))
					popupAdShow:"no",
					@elseif ($ads->popup_ad)
					popupAdShow:"yes",
					@else
					popupAdShow:"no",
					@endif
					popupAdStartTime:"00:10",
					popupAdEndTime:"00:30",
					popupAdGoToLink:"{{ $ads->popup_go }}",
					thumbImg:"{{ $video->thumb }}"
				}

			]
		});	
			
		});
  </script>

<!-- Video -->
  	<div id="parent" style="width:100%;height:auto;box-shadow: 0 1px 2px rgba(0,0,0,.1);top:0px;left:0px;position: relative;">
		<div id="embed_video_player"></div> 
	</div>
</div>