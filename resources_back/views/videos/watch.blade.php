
<script src="{{ asset('magelur/js/videos/fullscreen.js')}}"></script>
<script src="{{ asset('magelur/js/videos/videoPlayer.js')}}" type="text/javascript"></script>
<script src="{{ asset('magelur/js/videos/playlist.js')}}" type="text/javascript"></script>
<script src="{{ asset('magelur/js/videos/jquery.menu-aim.js')}}"></script>
<script src="{{ asset('magelur/js/videos/main.js')}}"></script>
<script src="{{ asset('magelur/js/videos/ajax.votes.js')}}"></script>
<script src="{{ asset('magelur/js/videos/ajax.comments.js')}}"></script>
<script src="{{ asset('magelur/js/videos/jquery.jscroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/videos/toastr.js')}}"></script>
<script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('magelur/js/star_rating.js')}}"></script>
<link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>
<div class="watch_page video_view_innerdiv video_upload_page col-md-12 col-sm-12 col-xs-12">
	<div class="container video_view_container">
		<div class="col-md-8 col-sm-12 col-xs-12 video_upload_left ">
			<!--####################### Left Side DIV #######################-->
			<div class="w_leftSide">
				<!-- Video Div -->
				<div class="w_video" id="w_video">
					<script type="text/javascript" charset="utf-8">

						$(document).ready(function($){

							videoPlayer = $("#watch_video_now").Video({
								instanceName:"player1",
								autohideControls:2, 
								hideControlsOnMouseOut:"No",
								videoPlayerWidth:700,
								videoPlayerHeight:360,
								responsive:true,
								playlist:"Off",
								@if ($settings_player->autoplay == 0)
								autoplay:true,
								@else 
								autoplay:false,
								@endif
								videoPlayerShadow:"off",
								loadRandomVideoOnStart:"No",
								shuffle:"No",
								posterImg:"{{ $video->preview }}",
								onFinish:"Stop video",
								nowPlayingText:"No",
								fullscreen:"Fullscreen native",
								rightClickMenu:false,
								hideVideoSource:true,
								showAllControls:true,
								@if ($settings_player->skip_ad == 0)
								allowSkipAd:true,
								@else
								allowSkipAd:false,
								@endif
								infoShow:"No",
								shareShow:"No",
								logoShow:"No",
								logoClickable:"Yes",
								logoPath:"",
								logoGoToLink:"",
								logoPosition:"bottom-right",
								embedShow:"No",

								videos:[
								{
									@if ($video->youtube_id != NULL)
									videoType:"youtube",
									youtubeID:"{{ $video->youtube_id }}",
									@else 
									videoType:"HTML5",
									@endif

									title:"AD 5 sec + Pieces After Effects project",

									mp4:"{{ $video->video_path }}",

									imageUrl:"",
									imageTimer:2,
									@if (App\Helper\Helper::is_vip())
									prerollAD:"no",
									@else
									@if ($ads->preroll_ad)
									prerollAD:"yes",
									@else
									prerollAD:"no",
									@endif
									@endif
									prerollGotoLink:"{{ $ads->preroll_go }}",
									preroll_mp4:"{{ $ads->preroll_ad }}",
									prerollSkipTimer:5,
									midrollAD:"no",
									midrollAD_displayTime:"00:10",
									midrollGotoLink:"",
									midroll_mp4:"",
									midrollSkipTimer:5,	
									@if (App\Helper\Helper::is_vip())
									postrollAD:"no",
									@else
									@if ($ads->end_ad)
									postrollAD:"yes",
									@else
									postrollAD:"no",
									@endif
									@endif
									postrollGotoLink:"{{ $ads->end_go }}",
									postroll_mp4:"{{ $ads->end_ad }}",
									postrollSkipTimer:5,
									popupImg:"{{ $ads->popup_ad }}",
									@if (App\Helper\Helper::is_vip())
									popupAdShow:"no",
									@else
									@if ($ads->popup_ad)
									popupAdShow:"yes",
									@else
									popupAdShow:"no",
									@endif
									@endif
									popupAdStartTime:"00:10",
									popupAdEndTime:"00:30",
									popupAdGoToLink:"{{ $ads->popup_go }}",
									thumbImg:"{{ $video->thumb }}"
								}

								]
							});	

						});

					</script>

					<!-- Video -->
					<div id="parent1" style="width:100%;height:auto;box-shadow: 0 1px 2px rgba(0,0,0,.1);top:0px;left:0px;position: relative;">

						@if (!is_null($video->vimeo_id))

						<!-- Vimeo Video -->
						<iframe src="https://player.vimeo.com/video/{{ $video->vimeo_id }}" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						@elseif (!is_null($video->dailymotion_id))

						<!-- Dailymotion Video -->
						<iframe frameborder="0" width="100%" height="360" src="//www.dailymotion.com/embed/video/{{ $video->dailymotion_id }}" allowfullscreen></iframe>

						@else
						<div id="watch_video_now" style="position: relative;"></div> 

						@endif
					</div>
				</div>


				<!-- Video header -->
				<div class="video_header">
					<!-- Video Title -->
					<div class="video_title">
						<h2 title='{{ $video->title }}'>{{ $video->title }}</h2>
@if($video->user_id == \Session::get('logged_id') && $video->user_type == \Session::get('user_type'))
						<!-- favourite -->
						@else
						<div class="favourites">
						  <a onclick="addToFavourite()" title="Add to favourite"><i class="fa fa-heart"></i></a>
						</div>
						@endif
						<!-- @if($video->user_id != \Session::get('logged_id') && $video->user_type != \Session::get('user_type')) -->
						<!-- favourite -->
						<!-- <div class="favourites">
						  <a onclick="addToFavourite()" title="Add to favourite"><i class="fa fa-heart"></i></a>
						</div>
						@endif -->

						@if($video->user_id == $userId && $video->user_type == \Session::get('user_type'))
						<div class="del_image" style="float:right">
							<!-- {{ url('video/delete/'.$video->video_id.'/' )}} -->
						<h1><a href="javascript:void(0)" title="Delete" id='linkkk'>
                  		<i class="fa fa-trash"></i></a></h1>
              			</div>
              		@endif
              		<h3><i class="fa fa-eye"></i>{{ number_format($views) }}</h3>
							<!-- Votting System -->
						<div class="votting_system">
							<input type="hidden" name="vote_type" id="vote_type" value="video" />
							<!-- Like Button -->
							@if (\Session::get('logged_id'))
							@if (App\Helper\Helper::already_liked($video->video_id) == TRUE)
							<button style="opacity:1;" class="vote_like" data-video="{{ $video->video_id }}" id="like">
								<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span>	{{ $likes }}
							</button>
							@else
							<button class="vote_like" data-video="{{ $video->video_id }}" id="like">
								<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span> {{ $likes }}
							</button>
							@endif
							@else
							<button class="vote_like" data-video="{{ $video->video_id }}" id="like">
								<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span>	{{ $likes }}
							</button>
							@endif
							<!-- Dislike Button -->
							<!-- @if (\Session::get('logged_id'))
							@if (App\Helper\Helper::already_disliked($video->video_id) == TRUE)
							<button style="opacity:1;" data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@else
							<button data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@endif
							@else
							<button data-video="{{ $video->video_id }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@endif -->						

						</div>

						
					</div>

					

					<!-- User panel -->
					<div class="user_panel">
						<div class="post-action">
							<!-- Rating -->
							<!-- <h2>{{$avg_ratings}}</h2> -->
							<!-- <select class='rating' id='rating_'  data-id='rating_'>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							<option value="5" >5</option>
							</select> -->

							</div>
							<script type="text/javascript">
                            // set rating for each star
							// setRating($('.rating'),'{{$avg_ratings}}');
							</script>
						<!-- User channel avatar -->
						<div class="channel_avatar">
							<img src="{{ App\Helper\Helper::user_avatar($video->user_id,$video->user_type) }}">
						</div>

						<!-- Channel name and subscribers counter -->
						<div class="channel_counter">
							
							<!-- Channel name -->
							<div class="channel_name">
								<a href="{{ App\Helper\Helper::get_user_profile_link($video->user_id,$video->user_type) }}">{{ App\Helper\Helper::channel_by_id($video->user_id,$video->user_type) }}</a>

								<!-- User Verification -->
								@if (App\Helper\Helper::is_verified($video->user_id,$video->user_type) == TRUE)
								<span class="verified" data-balloon="{{ Lang::get('core.verified') }}" data-balloon-pos="right">
									<!-- <img id="verified" src="{{ url('/') }}/content/assets/img/verified-opacity.png" alt="" > -->
									<span class="success_icon"><i class="fa fa-check-circle"></i></span>
								</span>
								@endif
							</div>

							<!-- Subscribers counter -->
							<div class="subscribers_counter" style="float: left;">
								<span id="totalSubscribers">
									{{ strip_tags(substr($video->description , 0, 500)) }}
								</span>
								<!-- <a href="javascript:void(0);" class="read_more_tag">Read more</a> -->
							</div>
							</div>
							<span class="video_update_date">{{$avg_ratings}}</span>

						<!-- <span class="video_update_date">{{ Lang::get('core.uploaded_on') }} {{ App\Helper\Helper::description_date($video->created_at) }}</span> -->

					</div>
					<!-- Views and votting counter -->
					<div class="viewVoteCounter">
						<!-- Votting  Line -->
						<div class="sparkbars">
							<div class="sparkbars-likes" style="width: 0%;"></div>
							<div class="sparkbars-dislikes" style="width: 0%;"></div>
						</div>
					
					</div>



				</div>	

				<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk video_upload_desc_blk" >
					<!-- Video Description -->
					<div class="video_description">
						
						<!-- Publish Date -->
						<div class="publish_date">
							<h1>{{ Lang::get('core.share_your_video') }}</h1>
						</div>

						<!-- Embed & Flag Video -->
						<div class="embed_flag" style="display:none">
							<a class="embed_vid" id="shareVid">{{ Lang::get('core.share_video') }}</a>
							<a href="#" class="flag_vid" id="reportVid" data-video="{{ $video->video_id }}">{{ Lang::get('core.flag_video') }}</a>
						</div>

						<div class="v_description" id="videoDescription">
							
							<!-- Embed Video -->
							<div class="embedVideo">

								<ul>

									<!-- Share on Facebook -->
									<li><a href="http://www.facebook.com/sharer.php?u={{ url('/video') }}/{{ $video->video_id }}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/facebook.png">
									</a></li>

									<!-- Share on Twitter -->
									<li><a href="https://twitter.com/share?url={{ url('/video') }}/{{ $video->video_id }}&amp;text={{$video->title}}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/twitter.png">
									</a></li>

									<!-- share on google plus -->
									<li><a href="https://plus.google.com/share?url={{ url('/video') }}/{{ $video->video_id }}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/google.png">
									</a></li>

									<!-- Share on Reddit 
									<li><a href="http://reddit.com/submit?url={{ url('/video') }}/{{ $video->video_id }}&amp;title={{$video->title}}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/reddit.png">
									</a></li> -->

									<!-- share on Tumblr 
									<li><a href="http://www.tumblr.com/share/link?url={{ url('/video') }}/{{ $video->video_id }}&amp;title={{$video->title}}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/tumblr.png">
									</a></li> -->

									<!-- share on Linkedin 
									<li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ url('/video') }}/{{ $video->video_id }}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/linkedin.png">
									</a></li> -->

								</ul>
								<textarea><embed src="{{ url('/') }}/embed/{{ $video->video_id }}" height="400" width="600"></textarea>
								</div>

								<!-- <p>{!! nl2br(App\Helper\Helper::active_links($video->description, $video->video_id)) !!}</p> -->

								<div style="position: relative;margin-top: 25px;padding-bottom: 30px;">
									<ul class="tags">

										<!-- Video Tags -->
										{{ App\Helper\Helper::video_tags($video->video_id) }}

									</ul>
								</div>

							</div>

							

			<!-- <div class="show_more">
				<a id="showMore">{{ Lang::get('core.show_more') }}</a>
			</div> -->

		</div>
	</div>

	<!-- Video Comments -->
	<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk nopadding_div">
		<div class="comments_box video_comments_box">
			<div class="comments_counter">
				<p>{{ Lang::get('core.comments') }} <span>•</span> {{ $total_comments }}</p>
			</div>

			<div class="cm_box">
				<div class="cm_avatar">
					@if (\Session::get('logged_id'))
					<img src="{{ App\Helper\Helper::user_avatar(\Session::get('logged_id'),\Session::get('user_type')) }}" alt="">
					@else
					<img src="{{ url('/') }}/content/uploads/avatars/avatar.png" alt="">
					@endif
				</div>

				<div class="cm_textarea">

					<form action="#" method="POST" id="SubmitComment">
						<meta name="csrf-token" content="{{ csrf_token() }}">
						<input type="hidden" id="comment_type" name="type" value="video" />
						<textarea name="comment" placeholder="{{ Lang::get('core.add_comment') }}" id="commentsBox"></textarea>
						<select id="comment_rating" style="display: none;">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
						<span style='display:none' class='loader'>
						<img  src="{{ asset('magelur/message/fancybox_loading.gif') }}" />
						</span>
						<button id="postComment" data-video="{{ $video->video_id }}" type="submit"><span>{{ Lang::get('core.post_comment') }}</span></button>
					</form>

				</div>

				<div class="clear"></div>
			</div>

			<!-- List of Comments -->
			<div class="comments-container" id="commentScroll">

				<ul class="comments-list">
					@if (count($comments) > 0)
					@foreach ($comments as $comment)
					
					<li class="commentScroll">
						<div class="comment-main-level">
							<!-- Avatar -->
							<div class="comment-avatar">
								<img src="{{ App\Helper\Helper::user_avatar($comment->user_id,$comment->user_type) }}" alt=""></div>
								<!-- Comment Details -->
								<div class="comment-box">
									<div class="comment-head">

										<!-- Uploader -->
										<h6 class="comment-name by-author">
											<a href="{{ App\Helper\Helper::get_user_profile_link($comment->user_id,$comment->user_type) }}" target="_blank">{{ App\Helper\Helper::channel_by_id($comment->user_id,$comment->user_type) }}</a>
											<span class="cm_date">
												{{ App\Helper\Helper::change_date_format($comment->created_at) }}
											</span>
										</h6>

										<!-- Comment Date -->
										<!-- <span class="cm_date">{{ App\Helper\Helper::change_date_format($comment->created_at) }}</span> -->
									</div>

									
									<script type="text/javascript">
										$(document).ready(function(){
											$('#comment_id_{{ $comment->id  }}').barrating({
												theme: 'fontawesome-stars'
											});
										});
									</script>	

									<select class="not_rated" data-rate="{{$comment->rating}}" id="comment_id_{{ $comment->id }}" style="display: none;">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
									
									<script type="text/javascript">
										// set rating for each star
										setRating($('#comment_id_{{ $comment->id  }}'),'{{$comment->rating}}');
									</script>	

									<div class="comment-content">
										{!!
											nl2br( App\Helper\Helper::emoticons(App\Helper\Helper::active_links($comment->comment, $video->video_id))) 
											!!}
									</div>


									
									</div>
								</div>
							</li>



							@endforeach

		    


							<div >
							</div>
							
							
							@else
							<div class="no_data_msg no_data_msg_div">
								<p><span><i class="fa fa-warning"></i></span>{{ Lang::get('core.no_comment') }}</p>
							</div>
							@endif

							<div style="display:none;">
								{!! str_replace('/?', '?', $comments->appends(Input::except('page'))->render()) !!}
							</div>

						</ul>
					</div>
				</div>
			</div>		

<!-- 
		<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk">
		<div class="cm_textarea">

				<form action="#" method="POST" id="SubmitComment">
				<meta name="csrf-token" content="{{ csrf_token() }}">
				<div class="text_area_video">
					<textarea name="comment" rows="5" placeholder="Enter Your Comments" id="commentsBox"></textarea>
				</div>
				<div class="text-center">
					<button id="postComment" data-video="{{ $video->video_id }}" type="submit"><span>{{ Lang::get('core.post_comment') }}</span></button>
				</div>
				</form>

				</div>
			</div>	 -->


		</div>
	</div>


	<!-- Recommended Videos,  Similar Videos -->
	<div class="col-md-4 col-sm-12 col-xs-12 video_upload_right">
		<div class="related_videos">
			<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk video_upload_desc_blk tags_blk1">
	<!-- Video Description -->
	<div class="video_description">
	<div class="video_title">
		<h2>{{ Lang::get('core.tags') }}</h2>
	</div>
	<div>
									<ul class="tags">

										<!-- Video Tags -->
										{{ App\Helper\Helper::video_tags($video->video_id) }}

									</ul>
								</div>
								</div>
								</div>
			
			<!-- Related Videos -->
					<div class="video_title">
	<h2 style="padding-top:10px;">Related Videos</h2>
	</div>
			@if (count($related_videos) > 0)

			@foreach ($related_videos as $related_video)
			<div class="r_video_box">
				<div class="r_video_bg">
					<img src="{{ $related_video->thumb }}" alt="">
					<span>{{ $related_video->duration }}</span>
				</div>

				<div class="r_video_info">
					<a href="{{ url('/video/') }}/{{ $related_video->video_id }}" class="r_title">{{ $related_video->title }}</a>

					<a href="{{ App\Helper\Helper::get_user_profile_link($related_video->user_id,$related_video->user_type) }}" class="r_channel">{{ App\Helper\Helper::channel_by_id($related_video->user_id,$related_video->user_type) }}</a>

					<a class="r_views"><i class='fa fa-eye'></i> {{ App\Helper\Helper::count_views($related_video->video_id) }} views</a>
				</div>
			</div>
			@endforeach

			@else
			<div class="no_data_msg no_data_msg_div" style="width: 90%;margin: 10px auto;margin-bottom: 5px;">
				<p><span><i class="fa fa-warning"></i></span>{{ Lang::get('core.no_related') }}</p>
			</div>
			@endif

		</div>
	<!-- 	<div class="pagination_content text-center"> <ul class="pagination"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="javascript:void(0);">2</a></li> <li><a href="javascript:void(0);" rel="next">»</a></li></ul></div> -->
	</div>

</div>
</div>


<style type="text/css">
.map{display: none;}
</style>

<script type="text/javascript">

	$('#postComment').click(function(e){
   	 location.reload();
   });
	
	$(function() {
		$('#commentScroll').jscroll({
			autoTrigger: true,
			nextSelector: '.pagination li.active + li a', 
			contentSelector: '#commentScroll',
			callback: function() {
				$('ul.pagination:visible:first').hide();
				setTimeout(function(){
					setLoadMoreRating();
				},1000)
			}
		});


 $("#linkkk").click(function()
 {	
	swal({
   		title: "Are you sure?",
   		text: "Once deleted, you will not be able to recover this Video!",
   		icon: "warning",
   		buttons: true,
   		dangerMode: true,
   	}).then(function(willDelete)  {
   		if (willDelete) {
   			$(location).attr('href', "{{ url('video/delete/'.$video->video_id.'/' )}}");
   		} else {
   			swal("Your Video is safe!");
   		}
   	}); 

 	});
});

</script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <script type="text/javascript">


   function addToDelete() {

   	swal({
   		title: "Are you sure?",
   		text: "Once deleted, you will not be able to recover this Video!",
   		icon: "warning",
   		buttons: true,
   		dangerMode: true,
   	})
   	.then((willDelete) => {
   		if (willDelete) {
   			$(location).attr('href', "{{ url('video/delete/'.$video->video_id.'/' )}}");
   		} else {
   			swal("Your Video is safe!");
   		}
   	}); 	
   }
    	
   var fav='{{$favourite}}' ; 
   if(fav== ''){
      $('.favourites a').removeClass('favouriteActive');
      $('.favourites a').attr('title','Add to favourites');
   }else{
      $('.favourites a').addClass('favouriteActive');
      $('.favourites a').attr('title','Remove from favourites');
   }
   function addToFavourite() {

      var favourite_id = '{{$video->id}}';
      var favourite_type = 'video';
      
      $.ajax({
        url: '{{url("addToFavorites")}}',
        type: 'post',
        data: {favourite_id:favourite_id,favourite_type:favourite_type},
        dataType: 'json',
        success: function(data){
          if (data.status == 'added') {
              swal({
                title: "",
                text: "Video added to your Favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').addClass('favouriteActive');
              $('.favourites a').attr('title','Remove from favourites');
          }else if(data.status == 'removed'){
              swal({
                title: "",
                text: "Video removed from your favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').removeClass('favouriteActive');
              $('.favourites a').attr('title','Add to favourites');
          }else{
            swal({
              title: "",
              text: "Some error occured. Please try again after some time.",
              icon: "danger",
              dangerMode: true
            });
          }
        }
          
      });
    }
    
        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                });
            });
</script>