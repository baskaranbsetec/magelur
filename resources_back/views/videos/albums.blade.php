
<link href="{{ asset('magelur/css/uploader.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<!-- Display Error message for Video limit -->
 @if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{{\Session::get('messagetext')}}">
  @elseif (\Session::has('messagedelete'))
      <input type="hidden" id="msg" value="{{\Session::get('messagedelete')}}">   
  @else
      <input type="hidden" id="msg" value="">  
  @endif
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- end -->
<script src="{{ asset('magelur/js/uploader.js')}}"></script>
<style type="text/css">
  .circle_white {
    width: 80px;
    height: 80px;
  }
</style>
<div class="magelur_about faq_sec">
  <!-- show upload only for model & agent -->
<div class="container-fluid magelur_header">


<section>
    <div id="upload-demo-i-cover">
    @if($user_type == 'agent')
    {!! SiteHelpers::user_avatars($user->id,'agent',$user->profile_cover,'/uploads/agency_profile/cover/') !!}
    @elseif($user_type == 'model')
    {!! SiteHelpers::user_avatars($user->id,'model',$user->profile_cover,'/uploads/agency_profile/cover/') !!}
    @endif
    </div>
    <div class="banner_title wow bounce">
      <h1>{{$user->username }} - Gallery</h1>      
     </div>
</section>
</div>
<div class="agency_video_blk">
<div class="container">
<div class="agent_video_content col-md-12 col-sm-12 col-xs-12">
<div class="crave_image_div">

@if($user_type == 'agent')
<img src="{{ asset('uploads/agency_profile/'.$user->profile_photo) }}" alt="{{$user->username}}"/>
@elseif($user_type == 'model')
<img src="{{ asset('uploads/model_profile/'.$user->profile_photo) }}" alt="{{$user->username}}"/>
@else
<img src="" alt="No Preview"/>
@endif
</div>

@if($user_type == 'agent')
<div class="agency_video_title">
<h1>{{$user->username}} AGENT ALBUM </h1>
</div>
@elseif($user_type == 'model')
<div class="agency_video_title">
<h1>{{$user->username}} MODEL ALBUM</h1>
</div>
@endif
</div>

</div>
</div>
@if(count($albums)>0)
<div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">
<div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">
@if($albums[0]->ownerid==$userId)
<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
    <a href="{{url('photo/upload')}}"><span class="span_upload_photo"><i class="fa fa-plus-circle"></i></span></a>
    <h1 class="span_heading">Add Photos</h1>
  </div>
</div>
@endif
@foreach($albums as $album)
<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
   {{--*/ $Ext=App\Helper\Helper::getLastedExt($album->album_id) /*--}} 
    @if(empty(App\Helper\Helper::getLastedPhoto($album->album_id)))
    <img src="{!! asset('/magelur/images/Coming-Soon.png') !!}" alt="{{ $album->title }}" class="gallery_images1">
  @else
    <img src="{!! \SiteHelpers::user_albums(App\Helper\Helper::getLastedPhoto($album->album_id),'_medium',$Ext) !!}" alt="{{ $album->title }}" class="gallery_images1">
   @endif
    <div class="img_hover_blk">
      <div class="hvr_icon_div">
       <a class="fancybox" rel="gallery1" href="{{ url('uploads/model_gallery/'.App\Helper\Helper::getLastedPhoto($album->album_id).'_medium.'.$Ext) }}" title=""> 
        <span class="circle_white"><img src="{{ asset('images/expanded_img.png')}}" title='Preview Image'></span>
       </a>
        <a href="{{ url('photo/album/'.$album->uri) }}"><span class="circle_white"><img src="{{ asset('images/crop_img.png')}}" title='View Album'></span></a>
        @if($album->ownerid==$userId)
        <!-- {{ url('photo/albumdelete/'.$album->album_id) }} -->
         <a href="javascript:void(0)" onclick="javascript:test(this.id)" id="{{ url('photo/albumdelete/'.$album->album_id) }}"><span class="circle_white"><img src="{{ asset('images/trash.png')}}" title="Delete Album"></span></a>
         @endif
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function test(clickedID) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Album!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(function(willDelete) {
     // body...    
      if (willDelete) {
        
        $(location).attr('href',clickedID);
      } else {
        swal("Your Album is safe!");
      }
    });   
   }

</script>
@endforeach
</div>
</div>
</div>

@else
<div class="magelur_gallery wow bounce col-md-12 col-sm-12 col-xs-12">
<div class="empty_vertical">
  <div class="empty_msg_details clearfix">
  <div class="empty_msg_blk clearfix">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>No More Images!</h1>
        <h2>
          @if(\Auth::check())
          This Model has no images to show
          @else
          You Have Not Uploaded Any Images Under Your Gallery. Click Add New to upload images on gallery
          @endif
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="{{asset('images/file_image1.png')}}" alt="file_image" />
        </div>
      </div>
    </div>
    <div class="pages_blk">
      <div class="msg_previous_page">
        @if(\Auth::check())
        @else
        <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
        @endif
      </div>
       @if($user_type == 'agent'  && $user_vid_id == \Session::get('logged_id'))
      <div class="msg_next_page">

         <a href="{{ url('photo/upload') }}"><span>Add New</span><img src="{{asset('images/next_image1.png')}}"></a>
      </div>
      @endif
      @if($user_type == 'model'  && $user_vid_id == \Session::get('logged_id'))
      <div class="msg_next_page">

         <a href="{{ url('photo/upload') }}"><span>Add New</span><img src="{{asset('images/next_image1.png')}}"></a>
      </div>
      @endif
    </div>
  </div>
</div>
</div>
</div>

@endif


<style type="text/css">
  .map{display: none;}
</style>

<script type="text/javascript">
$(document).ready(function(){

    $('.preview_imges_scroll').slimScroll({
        height          : '310px',
        width           : '100%',
        allowPageScroll :  true
        
    });     

    $(document).on('click','input[name="album_type"]',function(){
      // get the choosed value
      var curVal = $(this).val();

      if(curVal == 'new'){
        // show album create form
        $('div.new-albums-form').show();
        // hide album pick list
        $('div.old-albums-form').hide();
      }else{
        // allow user to pick the album list
        $('div.old-albums-form').show();
        // hide album create form
        $('div.new-albums-form').hide();

      }
    })

    $(".fancybox").fancybox({});

});

function addAlbum(){
  var title    = $('input[name="album_name"]').val();
  var desc     = $('textarea[name="album_desc"]').val();
  $.post('{!! url("add/album")!!}',{title:title,desc:desc},function(data){

  });
}
if($('#msg').val()!="")
{
  swal($('#msg').val());
}

</script>
