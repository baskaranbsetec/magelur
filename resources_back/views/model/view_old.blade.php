@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('model?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('model/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('model/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('model/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			<!-- @if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif --> 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>{{ $row->username}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Firstname</td>
						<td>{{ $row->firstname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Lastname</td>
						<td>{{ $row->lastname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>DOB</td>
						<td>{{ $row->DOB}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Gender</td>
						<td>{{ $row->gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>{{ $row->state}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>{{ $row->country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>{{ $row->zipcode}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Biography</td>
						<td>{{ $row->biography}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>
						<a href="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" target="_blank" class="previewImage">

						<img src="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" border="0" width="50" class="img-circle">
						</a> 
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ethnicity</td>
						<td>{{ $row->ethnicity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hair Color</td>
						<td>{{ $row->hair_color}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Eye Color</td>
						<td>{{ $row->eye_color}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Shoe Size</td>
						<td>{{ $row->shoe_size}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hip</td>
						<td>{{ $row->hip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Waist</td>
						<td>{{ $row->waist}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Bust Size</td>
						<td>{{ $row->bust_size}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Body Type</td>
						<td>{{ $row->body_type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Height</td>
						<td>{{ $row->height}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Marital status</td>
						<td>{{ $row->marital_status}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Smoking</td>
						<td>{{ $row->smoking}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Categories</td>
						<td>{{ $row->model_categories}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Drinking</td>
						<td>{{ $row->drinking}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Drugs</td>
						<td>{{ $row->drugs}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Children</td>
						<td>{{ $row->children}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Industries</td>
						<td>{{ $row->model_industries}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Publishes</td>
						<td>{{ $row->publishes}}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Body Piercing</td>
						<td>@if( $row->body_piercing == 1)
							<span >Yes</span>
							@else
							<span >No</span>
							@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Tattoos</td>
						<td>@if( $row->tattoos == 1)
							<span >Yes</span>
							@else
							<span >No</span>
							@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Experience</td>
						<td>@if( $row->model_experience == 1)
							<span > New Face </span>
							@elseif( $row->model_experience == 2)
							<span >Aspiring Model</span>
							@elseif( $row->model_experience == 3)
							<span > Experienced Model </span>
							@else
							<span >Professional Model</span>
							@endif
						</td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>Language</td>
						<td>{{ $row->language}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Education</td>
						<td>{{ $row->education}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profession</td>
						<td>{{ $row->profession}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hobbies</td>
						<td>{{ $row->hobbies}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>{{ $row->contact_number}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Terms Agree</td>
						<td>
							@if( $row->terms_agree == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>
							@if( $row->status == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif 
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Status</td>
						<td>
							@if( $row->approved_status == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>{{ $row->created_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>{{ $row->last_updated}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>@if($row->approved_status == 1){{ $row->approved_date}} @endif </td>
						
					</tr>
				
					<!-- <tr>
						<td width='30%' class='label-view text-right'>Password</td>
						<td>{{ $row->password}} </td>
						
					</tr> -->
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop