@include('model_dash_header')

<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<div class="aboutmodel network_page">
 <div class="container">	
   <div class="agent_network_container">	
 	<div class="cutfix clearfix">
 	 <div class="col-sm-3">
 	 	<div class="agent_net">
 	 	 <ul class="monicel">
 	 	  <li class="active model_active"><a href="javascript:void(0)">Model</a></li>
 	 	  <li class=" agent_active"><a href="javascript:void(0)">Agency</a></li>
 	 	 </ul>	
 	 	</div>	
 	 </div>	

 	 <div class="col-sm-9">
 	 <div class="smackel">
 	 	 <ul class="thme_three">
 	 	 	<input type="hidden" name="path" value="{{ app('request')->input('type') }}">
 	 	 	<input type="hidden" name="path_id" value="{{request()->route('id')}}">

 	 	 <div class='agent-ntw' style='display:none'>
 	 	   @if (count($a_messages) > 0)

 	 	 	@foreach($a_messages as $message)
 	 	 	<li class="network_parent_list">
 	 	  	<div class="network_img_div">
 	 	 		@if(\SiteHelpers::getagentnetworks($message->recipient))
 	 	 		<img src="{{ asset('uploads/agency_profile/'.\SiteHelpers::getagentnetworks($message->recipient) ) }}" class="img-responsive">
 	 	 		@endif
 	 	 		<div class="an_hover">
 	 	 			<div class="an_hover_icons">
 	 	 				<ul class="an_icons_list">
 	 	 					<li><a class="fancybox" rel="" href="{{ asset('uploads/agency_profile/'.\SiteHelpers::getagentnetworks($message->recipient) ) }}" title=""> 
 	 	 						<i class="fa fa-arrows-alt" aria-hidden="true" title="Preview Image"></i>
 	 	 					</a>
 	 	 					</li>
 	 	 					<li>
 	 	 					<a href=" {{ url(App\Helper\Helper::get_user_profile_link($message->recipient,'agent')) }}
 	 	 						"><i class="fa fa-external-link" aria-hidden="true" title="View Profile"></i></a>
 	 	 					</li>
 	 	 				</ul>
 	 	  			<!-- <ul class="an_icons_list">
 	 	  				<li><a class="fancybox" rel="gallery1" href="{{ asset('uploads/agency_profile/'.\SiteHelpers::getagentnetworks($message->recipient) ) }}" title=""> <i class="fa fa-arrows-alt" aria-hidden="true"></i></a></li>
 	 	  				<li><a href=""><i class="fa fa-external-link" aria-hidden="true"></i></a></li>
 	 	  			</ul> -->
 	 	  		</div>
 	 	  	</div>
 	 	  	</div>
 	 	  </li>
 	 	  @endforeach
 	 	  
 	 	  <div class="text-center pagination_padding col-md-12 col-sm-12 col-xs-12">
 	 	  	<div class="pagination_content"> 
 	 	  		{!! str_replace('/?', '?', $a_messages->appends(Input::except('page'))->render()) !!}
 	 	  	</div>
 	 	  </div>
 	 	  @else
 	 	  You have not yet connect any of the Agents. Go to Agent Listing Page and get connected with more contacts 
 	 	  @endif
 	 	 
 	 	  </div>
 	 	  <div class='model-ntw' >
 	 	  	@if (count($m_messages) > 0)
 	 	  @foreach($m_messages as $message)
 	 	  <li class="network_parent_list">
 	 	  <div class="network_img_div">
 	 	  @if(\SiteHelpers::getmodelnetworks($message->recipient))
 	 	 
        <img src="{{ asset('uploads/model_profile/'.\SiteHelpers::getmodelnetworks($message->recipient) ) }}" class="img-responsive network_image">
 	  		
 	  		@endif
 	 	  	<div class="an_hover">
 	 	  		<div class="an_hover_icons">
 	 	  			<ul class="an_icons_list">
 	 	  				<li><a class="fancybox" rel="" href="{{ asset('uploads/model_profile/'.\SiteHelpers::getmodelnetworks($message->recipient) ) }}" title=""> 
 	 	  					<i class="fa fa-arrows-alt" aria-hidden="true" title="Preview Image"></i>
 	 	  				</a>
 	 	  				</li>
 	 	  				<li>
 	 	  				<a href=" {{ url(App\Helper\Helper::get_user_profile_link($message->recipient,'model')) }}
 	 	  					"><i class="fa fa-external-link" aria-hidden="true" title="View Profile"></i></a>
 	 	  				</li>
 	 	  			</ul>
 	 	  		</div>
 	 	  	</div>
 	 	  	</div>
 	 	  </li>

 	 	  @endforeach
 	 	  
 	 	  <div class="text-center pagination_padding col-md-12 col-sm-12 col-xs-12">
 	 	  	<div class="pagination_content"> 
 	 	  		{!! str_replace('/?', '?', $m_messages->appends(Input::except('page'))->render()) !!}
 	 	  	</div>
 	 	  </div>
 	 	  @else
 	 	  You have not yet connect any of the models. Go to Model Listing Page and get connected with more contacts  
 	 	  @endif
 	 	</div>
 	 	 </ul>	
 	 	</div>	
 	 </div>	
 	</div>	
 </div>	
</div>
</div>	
<script type="text/javascript">

var ParamsId = $('input[name=path_id]').val()
var queryParams = $('input[name=path]').val()
if(queryParams == 'agent'){
$('.agent-ntw').show();
$('.model-ntw').hide();
$('li.model_active').removeClass('active');
$('li.agent_active').addClass('active');
$('li.agent_active').click(function () {return false;});

}
if(queryParams == 'model'){
$('.model-ntw').show();
$('.agent-ntw').hide();
$('li.agent_active').removeClass('active');
$('li.model_active').addClass('active');
$('li.model_active').click(function () {return false;});
}

$(document).on('click', 'li.model_active', function () {
var temp="?type=model";
$(location).attr('href','{{url("model-network")}}/'+ParamsId+temp);
});

$(document).on('click', 'li.agent_active', function () {
var temp="?type=agent";
$(location).attr('href','{{url("model-network")}}/'+ParamsId+temp);

});
$(".fancybox").fancybox({});
</script>