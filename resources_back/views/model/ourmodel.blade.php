<!-- <script type="text/javascript" src="{{ asset('magelur/js/lazyload.js')}}"></script>
 -->
  <style type="text/css">
.model_photos img{
  max-width: 370px;
  max-height: 450px;
}

 </style>
<div class="ourmodel_banner">
  <div class="ourmod_cnt">
    <h1>Our Models</h1>
    <div class="two_laid"><a href="{!! url('') !!}">Home</a><p>our models</p></div>
  </div>
</div>

<div class="model_photos">
<div class="container">
 <div class="modelspro clearfix">
 
 <div id="post">
     @foreach($models as $iKey => $model)

     @if($iKey == 0 || (($iKey + 1 ) % 4 ) == 0)
      <div class="row-model">
      @endif  
    <a href="{{ URL::to('myprofile/'.$model->id)}}" >
    <div class="model_item our_model_item1" id="{{$model->id}}">
     <img src="{{ asset('uploads/model_profile/'.$model->profile_photo) }}" >
       <div class="colorbox">
        <div class="inner_shades">
          <h4>Real Life & People</h4>
          <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
          <p>Professional Model</p>
          <p>Book model</p>
        </div>
       </div>
    </div>
  </a>

    @if((($iKey + 1 ) % 3 ) == 0)
    </div>
    @endif 

    @endforeach
</div>
<input type="hidden" id="row" value="0">
<input type="hidden" id="all" value="{{$count}}">


  <div class="butsload clearfix "  id="remove-row">
@if($count > 6)
      <button class="load-more"> <span>Load More</span></button>
@endif
    </div>
 
 </div> 
</div>  
</div>



<script type="text/javascript">

$(document).ready(function(){
$('.map').hide();

$('.row-model a').on({
    mouseenter: function() {
        // get parent element
        var parent = $(this).parent();
        // check element exists or not
        if(parent.length > 0){
          parent.addClass('hovered')
        }
        console.log($(this).parent());
        console.log('enter');
    },
    mouseleave: function() {
        // get parent element
        var parent = $(this).parent();
        // check element exists or not
        if(parent.length > 0){
          parent.removeClass('hovered')
        }      
        console.log($(this).parent());
        console.log('leave');        
    }
})  

// Load more data
$('.load-more').click(function(){
var row = Number($('#row').val());
var allcount = Number($('#all').val());
row = row + 6;

if(row <= allcount){
$("#row").val(row);

$.ajax({
url: '{{ url("demos/loaddata") }}',
type: 'post',
data: {row:row},
beforeSend:function(){
$(".load-more").text("Loading...");
},
success: function(response){

// Setting little delay while displaying new content
setTimeout(function() {
// appending posts after last post with class="post"
//$("#post").after(response).show().fadeIn("slow");
$("#post").append(response).show().fadeIn("slow");

var rowno = row + 6;

// checking row value is greater than allcount or not
if(rowno > allcount){

// Change the text and background
$('.load-more').hide();
}else{
$(".load-more").text("Load more");
}
}, 2000);


}
});
}

});

});
function changespp()
{
  var parent = $("#app_row").parent();
        // check element exists or not
        if(parent.length > 0){
          parent.addClass('hovered')
        }
}
function changeapp2()
{
  var parent = $("#app_row").parent();
        // check element exists or not
        if(parent.length > 0){
          parent.removeClass('hovered')
        }
}
</script>








