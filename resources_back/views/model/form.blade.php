@extends('layouts.app')

@section('content')
<style type="text/css">
label.error,.asterix{
padding: 0;
    list-style: none;
    color: #bd8404;
}
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<link href="{{ asset('magelur/css/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/css/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>


  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		
		</div>
		<h1> @if($row['username']!='') {{ $row['username'] }}'s Profile @else Model Management @endif</h1>
		<div class="col-md-5">
	</div>
	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
 {!! Form::open(array('url'=>'model/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=>'register')) !!}

<!-- <form method="POST" action="{{ url('model/save?return='.$return) }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" "> -->
<div class="col-md-12">
<fieldset><legend> Basic Information</legend>

	<div class="col-md-6">
		{!! Form::hidden('id', $row['id']) !!}	
		<input type='hidden' name='created_date' value='{{$row["created_date"]}}'>				
		<input type='hidden' name="password" value='{{ $row["password"]}}'>
		<div class="form-group " >
		<label for="Username" class=" control-label col-md-4 text-left"> Username <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('username', $row['username'],array('id'=>"username",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Firstname" class=" control-label col-md-4 text-left"> Firstname <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('firstname', $row['firstname'],array('id'=>"firstname",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Lastname" class=" control-label col-md-4 text-left"> Lastname <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('lastname', $row['lastname'],array('id'=>"lastname",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="DOB" class=" control-label col-md-4 text-left"> DOB <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<input type="text" name="DOB" id="datepicker" value='{{$row["DOB"]}}' class="form-control"  autocomplete='off' placeholder="Date of Birth"  >
		<span class="input-group-addon" style='display:none'></span>							      
		</div> 
		</div>  

		<div class="form-group  " >
		<label for="Gender" class=" control-label col-md-4 text-left"> Gender <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"male",($row['gender']=="male") ? true:false,array('')) !!} Male </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"female",($row['gender']=="female") ? true:false,array()) !!} Female </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"other",($row['gender']=="other") ? true:false,array()) !!} Other </label> 
		</div> 
		</div> 						

		<div class="form-group  " >
		<label for="Education" class=" control-label col-md-4 text-left"> Education <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('education',array('' => 'Select Education') +\SiteHelpers::getTableValues('education') ,$row['education'],array('class' => ' form-control','id' => 'education')) !!}   
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Profession" class=" control-label col-md-4 text-left"> Profession <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('profession', $row['profession'],array('id'=>"profession",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Hobbies" class=" control-label col-md-4 text-left"> Hobbies <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('hobbies', $row['hobbies'],array('id'=>"hobbies",'class'=>"form-control")) !!}                                	 	
		<label class="error" for="hobbies" generated="true"></label>
		</div> 
		</div> 
		<br>	

	</div>

	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<input  type='file' name='profile_photo' id='profile_photo'  value="{{$row['profile_photo']}}" style='width:200px !important;' /><br>
		<div class="preview-events ">
		{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/model_profile/') !!}
		</div>
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Banner <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<input  type='file' name='profile_cover' id='profile_cover'  value="{{$row['profile_cover']}}" style='width:200px !important;' /><br>
		<div class="preview-cover ">
		{!! SiteHelpers::showUploadedFile($row['profile_cover'],'/uploads/model_profile/cover/') !!}
		</div>
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="City" class=" control-label col-md-4 text-left"> City <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('city', $row['city'],array('id'=>"city",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="State" class=" control-label col-md-4 text-left"> State <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('state', $row['state'],array('id'=>"state",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Country" class=" control-label col-md-4 text-left"> Country <span class="asterix"> * </span></label>
		<div class="col-md-7">
			@if (is_numeric($row['country']))
			{{--*/ $row['country']= \SiteHelpers::getName('country',$row['country']); /*--}}
			@else
			{{--*/	$row['country'] =$row['country']; /*--}}
			@endif
			<input name="country" type="text" class="form-control" placeholder="Country" value="{{ $row["country"] }}">	

<!-- 		{!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),$row['country'],array('class' => 'form-control','id' => 'country')) !!}
 -->		</div> 
		</div>

		<div class="form-group  " >
		<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('zipcode', $row['zipcode'],array('id'=>"zipcode",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('email', $row['email'],array('id'=>"email",'class'=>"form-control")) !!}                                	 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('contact_number', $row['contact_number'],array('id'=>"contact_number",'class'=>"form-control")) !!}                                	
		</div> 
		</div>
		

	</div>
		<div class="col-md-12">	<div class="form-group  " >
		<label for="Biography" class=" control-label col-md-12 text-left"> Biography <span class="asterix"> * </span></label>
		<div class="col-md-12">
		{!! Form::textarea('biography', $row['biography'],array('id'=>"biography",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div>  </div>
</fieldset>
</div>
			
			
	<div class="col-md-12">	<fieldset><legend>Other Information </legend> </fieldset> 	</div>

	<div class="col-md-6">		
		<div class="form-group  " >
		<label for="Ethnicity" class=" control-label col-md-4 text-left"> Ethnicity <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('ethnicity',array('' => 'Select Ethnicity') +\SiteHelpers::getTableValues('ethnicity') ,$row['ethnicity'],array('class' => 'form-control','id' => 'ethnicity')) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Body Type" class=" control-label col-md-4 text-left"> Body Type <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('body_type',array('' => 'Select Body Type') +\SiteHelpers::getTableValues('body_type') ,$row['body_type'],array('class' => 'form-control','id' => 'body_type')) !!} 
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Hair Color" class=" control-label col-md-4 text-left"> Hair Color <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('hair_color',array('' => 'Select Hair Color') +\SiteHelpers::getTableValues('hair_color') ,$row['hair_color'],array('class' => 'form-control','id' => 'hair_color')) !!}
		</div> 
		</div> 	


		<div class="form-group  " >
		<label for="Eye Color" class=" control-label col-md-4 text-left"> Eye Color <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('eye_color',array('' => 'Select Eye Color') +\SiteHelpers::getTableValues('eye_color') ,$row['eye_color'],array('class' => 'form-control','id' => 'eye_color')) !!}
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Shoe-size" class=" control-label col-md-4 text-left"> Shoe-size <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('shoe_size', $row['shoe_size'],array('id'=>"shoe_size",'class'=>"form-control")) !!}                                	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Marital Status" class=" control-label col-md-4 text-left"> Marital Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('marital_status',array('' => 'Select Marital status') +\SiteHelpers::getTableValues('marital_status') ,$row['marital_status'],array('class' => 'form-control','id' => 'marital_status')) !!} 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Model Categories" class=" control-label col-md-4 text-left"> Model Categories <span class="asterix"> * </span></label>
		<div class="col-md-7">

		{!! Form::select('model_categories[]',\SiteHelpers::getTableValues('model_categories'),explode(',',$row['model_categories']),array('class' => 'select2 model_categories' ,'id' => 'model_categories','multiple'=>'multiple')) !!} 

		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Drugs" class=" control-label col-md-4 text-left">Drugs <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('drugs',array('' => 'Select Drugs') +\SiteHelpers::getTableValues('drugs') ,$row['drugs'],array('class' => 'form-control','id' => 'drugs')) !!}
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Model Industries" class=" control-label col-md-4 text-left">Model Industries <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('model_industries[]',\SiteHelpers::getTableValues('model_industries') ,explode(',',$row['model_industries']),array('class' => 'select2 model_industries' ,'id' => 'model_industries','multiple'=>'multiple')) !!} 
		</div> 
		</div>

 		<div class="form-group  " >
		<label for="Body Piercing" class=" control-label col-md-4 text-left"> Body Piercing <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('body_piercing',"1",($row['body_piercing']=="1") ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('body_piercing',"2",($row['body_piercing']=="2") ? true:false,array()) !!} No </label> 
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Tattoos" class=" control-label col-md-4 text-left"> Tattoos <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('tattoos',"1",($row['tattoos']=="1") ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('tattoos',"2",($row['tattoos']=="2") ? true:false,array()) !!} No </label> 
		</div> 
		</div>		
		<div class="form-group  " >
		<label for="Year of experience" class=" control-label col-md-4 text-left">Year of experience <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('years',array('' => 'Year of experience') +[0,1, 2, 3,'More than 3 years'] ,$row['year'],array('class' => 'form-control','id' => 'years')) !!}
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Instagram" class=" control-label col-md-4 text-left"> Instagram Link <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('insta_link', $row['insta_link'],array('id'=>"insta_link",'class'=>"form-control")) !!}                                	
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Facebook" class=" control-label col-md-4 text-left"> Facebook Link<span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('fb_link', $row['fb_link'],array('id'=>"fb_link",'class'=>"form-control")) !!}                                	
		</div> 
		</div>
	</div>


	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Publishes" class=" control-label col-md-4 text-left">Publishes <span class="asterix"> * </span></label>
		<div class="col-md-7">

		{!! Form::select('publishes',array('' => 'Select Publishes') +\SiteHelpers::getTableValues('publishes') ,$row['publishes'],array('class' => 'form-control','id' => 'publishes')) !!}
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Children" class=" control-label col-md-4 text-left"> Children <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('children',array('' => 'Select Children') +\SiteHelpers::getTableValues('children') ,$row['children'],array('class' => 'form-control','id' => 'children')) !!}
 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Drinking" class=" control-label col-md-4 text-left"> Drinking <span class="asterix"> * </span></label>
		<div class="col-md-7">
			{!! Form::select('drinking',array('' => 'Select Drinking') +\SiteHelpers::getTableValues('drinking') ,$row['drinking'],array('class' => 'form-control','id' => 'drinking')) !!}

		</div> 
		</div>

		<div class="form-group  " >
		<label for="Smoking" class=" control-label col-md-4 text-left"> Smoking <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('smoking',array('' => 'Select Smoking') +\SiteHelpers::getTableValues('smoking') ,$row['smoking'],array('class' => 'form-control','id' => 'smoking')) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Hip" class=" control-label col-md-4 text-left"> Hip <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="input-group hip">
		{!! Form::text('hip', $row['hip'],array('id'=>"hip",'class'=>"form-control")) !!}                                	
		<span class="input-group-addon">inches</span>
		</div>
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Waist" class=" control-label col-md-4 text-left"> Waist <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="input-group waist">
		{!! Form::text('waist', $row['waist'],array('id'=>"waist",'class'=>"form-control")) !!}                                	
		
		<span class="input-group-addon">inches</span>
		</div>
		</div> 
		</div> 			

		<div class="form-group  " >
		<label for="Bust Size" class=" control-label col-md-4 text-left"> Bust Size <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('bust_size',array('' => 'Select Bust Size') +\SiteHelpers::getTableValues('bust_size') ,$row['bust_size'],array('class' => 'form-control','id' => 'bust_size')) !!} 
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Height" class=" control-label col-md-4 text-left"> Height <span class="asterix"> * </span></label>
		<div class="col-md-7">
		
		{!! Form::select('height',array('' => 'Select Height') +\SiteHelpers::getTableValues('heights') ,$row['height'],array('class' => 'form-control','id' => 'height')) !!}
		</div> 
		</div> 						
													
										 					
		<div class="form-group  " >
		<label for="Language" class=" control-label col-md-4 text-left"> Language <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('language', $row['language'],array('id'=>"language",'class'=>"form-control")) !!}                                	
<label class="error" for="language" generated="true"></label>
		</div> 
		</div> 	
		<div class="form-group  " >
		<label for="Model Experience" class=" control-label col-md-4 text-left"> Model Experience <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',1,($row['model_experience']==1) ? true:false,array()) !!} New Face </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',2,($row['model_experience']==2) ? true:false,array()) !!} Aspiring Model </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',3,($row['model_experience']==3) ? true:false,array()) !!} Experienced Model </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',4,($row['model_experience']==4) ? true:false,array()) !!} Professional Model </label> 
		<label class="error" for="model_experience" generated="true"></label>
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Twitter" class=" control-label col-md-4 text-left"> Twitter Link<span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('twitter_link', $row['twitter_link'],array('id'=>"twitter_link",'class'=>"form-control")) !!}                                	
		</div> 
		</div>
	


	</div>

	<div class="col-md-12">	<fieldset><legend></legend> </fieldset> </div>

	<div class="col-md-6">

		<!-- <div class="form-group  " >
		<label for="Terms Agree" class=" control-label col-md-4 text-left"> Terms Agree <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('terms_agree',1,($row['terms_agree']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('terms_agree',0,($row['terms_agree']==0) ? true:false,array()) !!} No </label> 
		</div> 
		</div> --> 

		<div class="form-group  " >
		<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active </label> 					  
		<label class='radio radio-inline'>
		{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive </label>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Approved Status" class=" control-label col-md-4 text-left"> Verification Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('approved_status',1,($row['approved_status']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('approved_status',0,($row['approved_status']==0) ? true:false,array()) !!} No </label> 		</div> 
		</div> 	
		

	 </div>

		
		<div style="clear:both"></div>	

		<div class="form-group">
		<label class="col-sm-4 text-right">&nbsp;</label>
		<div class="col-sm-8">	
		<button type="submit" name="apply" class="btn btn-info btn-sm btnsubmit" > {{ Lang::get('core.sb_apply') }}</button>
		<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
		<button type="button" onclick="location.href='{{ URL::to('model?return='.$return) }}' " class="btn btn-warning btn-sm ">{{ Lang::get('core.sb_cancel') }} </button>
		</div>	  
		</div> 
		 
		{!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>

<script type="text/javascript">
	$(document).ready(function() { 

		var categories     = '{{ $row["model_categories"] }}';
		if(categories){
		var selectedValues = categories.split(',');
		$("select#model_categories").select2('val',selectedValues);	
		}
		var categories     = '{{ $row["model_industries"] }}';
		if(categories){
		var selectedValues = categories.split(',');
		$("select#model_industries").select2('val',selectedValues);	
		}
		
		$('.removeMultiFiles').on('click',function(){
		var removeUrl = '{{ url("model/removefiles?file=")}}'+$(this).attr('url');
		$(this).parent().remove();
		$.get(removeUrl,function(response){});
		$(this).parent('div').empty();	
		return false;
		});		

		});

		$(function(){
			// $('#DOB').datepicker().on('changeDate', function(ev)
			// {                 
			// 	$('.datepicker').hide();
			// 	var mydate = new Date(ev.date);
			// 	var mydate1 = new Date();
			// 	var sdate = mydate.getDate()+'-'+mydate.getMonth()+'-'+mydate.getFullYear();
			// 	var tdate = mydate1.getDate()+'-'+mydate1.getMonth()+'-'+mydate1.getFullYear();
			// 	// alert(mydate.getDate()+'-'+mydate.getMonth()+'-'+mydate.getFullYear());
			// 	if(sdate==tdate)
			// 	{
			// 		swal("Please select Valid Date");
			// 		$('#DOB').val("").datepicker("update");
			// 	}
			// 	else if(sdate>tdate)
			// 	{
			// 		swal("Please select Valid Date");
			// 		$('#DOB').val("").datepicker("update");
			// 	}
			// });
		// tinyMCE.init({
		// theme : "advanced",
		// mode: "exact",
		// elements : "biography",
		// theme_advanced_toolbar_location : "top",
		// theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		// + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		// + "bullist,numlist,outdent,indent",
		// theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		// +"undo,redo,cleanup,code,separator,sub,sup,charmap",
		// theme_advanced_buttons3 : "",
		// height:"250px",
		// width:"520px"
		// });

		$('#hobbies').tagit({
		singleField: true,
		});
		$('#language').tagit({
		singleField: true,
		});
				 var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);

    $('#datepicker').datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });

	})


		$("#register").validate({
     ignore: "not:hidden",
      rules: {
       //  contact_number:{
       //   required:true,
       //   number:true,
       //   minlength:10,
         
       // },
      insta_link: { url:true },
		twitter_link: {url:true },
		fb_link: {url:true },
       
        email:{
          // required:true,
          email:true,
          remote:{
          url:"{!! url('model/checkuniqueemailadmin')!!}",
          type:'post',
          data:{
            email:function(){
            	var email = $('#email').val();
            	var model_id = "{{ $row['id'] }}";
              return [email,model_id];
            },
            
          }
        }
        },
        profile_photo:{
          // required:true,
          accept:"jpg,png,jpeg,gif",
      
        },
       //  ethnicity:{
       //   required:true
       //  },
       //   bust_size:{
       //   required:true
       //  },
       //  years:{
       //   required:true
       //  },
       //  body_type:{
       //   required:true
       //  },
       //  hair_color:{
       //   required:true
       //  },
       //  eye_color:{
       //   required:true
       //  },

       //  city:{
       //   required:true
       //  },
       //  zipcode:{
       //   required:true,
       //   number:true,
       //  },
       //  state:{
       //   required:true
       //  },
       //  country:{
       //   required:true
       //  },
       //  gender:
       //  {
       //  required:true
       //  },
       
       //  shoe_size:{
       //  	required:true,
       //  	number:true,
       //  },
       //   hip:{
       //  	required:true,
       //  	number:true,
       //  },
       //   waist:{
       //  	required:true,
       //  	number:true,
       //  },
       //   height:{
       //  	required:true,
       //  	number:true,
       //  },
       //   marital_status:{
       //  	required:true,
       //  },
       //  smoking:{
       //  	required:true,
       //  },
       //  language:{
       //  	required:true,
       //  },
       //  hobbies:{
       //  	required:true,
       //  },
       //  drinking:{
       //  	required:true,
       //  },
       //  drugs:{
       //  	required:true,
       //  },
       //  children:{
       //  	required:true,
       //  },
       //  // 'model_industries[]':{
       //  // 	required:true,
       //  // },
       //  publishes:{
       //  	required:true,
       //  },
       //  body_piercing:{
       //  	required:true,
       //  },
       //  tattoos:{
       //  	required:true,
       //  },
       //  model_experience:{
       //  	required:true,
       //  },


      },

       messages: {
      contact_number:{
         
         number:"Please enter valid phone number",
         minlength:"Phone number should be 10 number"
       },
       
       language:{
       	required:"This value is required",
       },
       hobbies:{
       	required:"This value is required",
       },
        email:{
          required:"Please enter email id",
          email:"Please enter valid email id"
        },
        profile_photo:{
          // required:"Please choose image",
         accept:"Please Select Only Images",
        },
        city:{
         required:"Please select city"
        },
        zipcode:{
         required:"Please select zipcode",
          number:"Numbers only allowed",
        },
         state:{
         required:"Please select state"
        },
        country:{
         required:"Please select country"
        },
        // DOB:
        // {
        // required:"Date of Birth required"
        // },
       
        shoe_size:{
         
          number:"Size should be a number",
        },
        hip:{
         
          number:"Hip size should be a number",
        },
        waist:{
         
          number:"Waist size should be a number",
        },
        height:{
         
          number:"Height should be a number",
        },

       },
        submitHandler:function(form){
        $('#reg-button').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"> </i> {{ Lang::get("core.loading") }}</div>');
        $('#reg-button').prop("disabled",true);
        $('#date').removeAttr("disabled");
        form.submit();
        }
    });

		// show preview image
		$(document).on('change','input[name="profile_photo"]',function(){
		  readURL(this);
		})
		// render image
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-events img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		} 
		// show preview image
		$(document).on('change','input[name="profile_cover"]',function(){
		  readURLs(this);
		})
		// render image
		function readURLs(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-cover img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}   
</script>		 
@stop