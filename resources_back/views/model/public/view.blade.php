<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>{{ $row->username}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Firstname</td>
						<td>{{ $row->firstname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Lastname</td>
						<td>{{ $row->lastname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>DOB</td>
						<td>{{ $row->DOB}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Gender</td>
						<td>{{ $row->gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>{{ $row->state}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>{{ $row->country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>{{ $row->zipcode}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Biography</td>
						<td>{{ $row->biography}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>{{ $row->profile_photo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ethnicity</td>
						<td>{{ $row->ethnicity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hair Color</td>
						<td>{{ $row->hair_color}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Eye Color</td>
						<td>{{ $row->eye_color}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Shoe Size</td>
						<td>{{ $row->shoe_size}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hip</td>
						<td>{{ $row->hip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Waist</td>
						<td>{{ $row->waist}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Bust Size</td>
						<td>{{ $row->bust_size}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Body Type</td>
						<td>{{ $row->body_type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Height</td>
						<td>{{ $row->height}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Language</td>
						<td>{{ $row->language}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Education</td>
						<td>{{ $row->education}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profession</td>
						<td>{{ $row->profession}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hobbies</td>
						<td>{{ $row->hobbies}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>{{ $row->contact_number}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Terms Agree</td>
						<td>{{ $row->terms_agree}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Status</td>
						<td>{{ $row->approved_status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>{{ $row->created_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>{{ $row->last_updated}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>{{ $row->approved_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Password</td>
						<td>{{ $row->password}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	