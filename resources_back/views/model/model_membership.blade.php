	@include('model_dash_header')


<div class="subscription">
 <div class="container">
  <div class="subpanter clearfix">
    @foreach($models as $model)
  
     @if($model->plan_id !='')
       @foreach($membershipplans as $plan)
   <div class="subscript_left">	
  	<a class="subcript" href="javascript:void(0)">Subscription</a>
    @if($model->pause_subscription =='paused')
      <p><span class="sub_stateone">Subscription Plan</span>    
      <span class="sub_static">:</span>  {{$plan->plan_name}} Membership </p>  
<!-- 
      
      <p><span class="sub_stateone">Plan Duration</span> 
      <span class="sub_static">:</span> Not Available </p>
 -->
      <p>[ No Plans right now. Please upgrade your membership ] </p>
   
    @else
    	<p><span class="sub_stateone">Subscription Plan</span>    
      <span class="sub_static">:</span> {{$plan->plan_name}} Membership</p> 
	    <p><span class="sub_stateone">Plan Duration</span> 
      <span class="sub_static">:</span>  One  @if($purchase_period !='') {{$purchase_period}} @else month @endif</p>
	    <p><span class="sub_stateone">Renewal date</span>      
      <span class="sub_static">:</span> @if($renewal_date !='') {{$renewal_date}} @else - @endif</p>
    @endif
	    <div class="upmap">
       <!--  <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a> 
      <a href="javascript:void(0)" class="white_bg">Pause my subscription</a> -->
      @if(isset($membership->status) && $membership->status == 'cancelled')
          <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a>
      @elseif(isset($membership->status) && $membership->status == 'active' && $model->pause_subscription =='not_paused')
          <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a>
          {!! Form::open(array('url'=>'membership/cancelmembership','id' =>'member-form')) !!}
              <button class="btn btn-color btn-color-warning m-b-sm confirmcancel" type='submit' >Pause my subscription</button>
          {!! Form::close() !!}
      @else
          <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a>
      @endif

    </div>
     
     
   </div>
    @endforeach
     @elseif($model->pause_subscription == 'paused')
   <div class="subscript_left">  
    
      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
      </div>
    
   </div>
   @else
   <div class="subscript_left">  
    
      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
      </div>
    
   </div>
   @endif
   @endforeach
   <div class="subscript_right">	
  	<img src="{{ asset('images/membership_brand.png') }}">
   </div>
  </div>	
 </div>	
</div>	

  @include('footer_map')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

// <script type="text/javascript">
//         $(document).ready(function() {
//         $('#vertical').lightSlider({
//           item:1,
//           vertical:true,
//           verticalHeight:250
//         });  
//       });
 


   $(document).on('click', '#member-form', function(e) {
    e.preventDefault();
    swal({
        title: "Are you sure?",
      text: "Once Subscription paused,you will not get benefits of basic plan again",
      buttons: true,
      dangerMode: true,
       
    }).then(function (result) {
      if(result==true){
        // alert('true')
        $('#member-form').submit();
      }else{
swal({ icon: "success", title:"Your plan is safe!"});
      }
    });

});
 </script>


// <script>
// function openNav() {
//     document.getElementById("mySidenav").style.width = "250px";
//     document.getElementById("main").style.marginLeft = "250px";
// }

// function closeNav() {
//     document.getElementById("mySidenav").style.width = "";
//     document.getElementById("main").style.marginLeft= "";
// }
// </script>