<!-- uploader script -->
<script src="{{ asset('magelur/js/uploader.js')}}" type="text/javascript"></script>
<!-- Tags script -->
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>

<div class="video_upload_form_blk  col-md-12 col-sm-12 col-xs-12">
<div class="container">
<!-- Top DIV -->
<div class="topUpload">
<h1 class="video_title">Video Upload</h1>

	<!-- Session Messages -->
	@if (\Session::has('error'))
		<div class="error">
			<p>dsfsdg{{ Session::get('error') }}</p>
		</div>
	@endif

	@if (\Session::has('success'))
		<div class="success">
			<p>{!! Session::get('success') !!}</p>
		</div>
	@endif

	@if (count($errors) > 0)
	    <div class="error">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<!-- Display Error message for Video Duration -->
	@if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{!! Session::get('messagetext') !!}">
     @else
     <input type="hidden" id="msg" value="">
	@endif
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<!-- end -->

	<!-- Start Upload Form -->
	<form method="POST" action="{{ url('/upload') }}" enctype="multipart/form-data" id="uploaderForm">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<div class="video-init-desc">
		<p>	If you use social media, you already know how important video is.Clearly, video is the dominant form of content grabbing your audience's attention.Clearly, video is the dominant form of content grabbing your audience's attention.Clearly, video is the dominant form of content grabbing your audience's attention.
		</p> 	
		</div>

		<div class="upload_radio_btn">
			<label class="agree radio_button_div">Upload Video
    			<input name="upload_type" value="upload" type="radio">  
    			<span class="checkmark"></span>
			</label>
			<label class="agree radio_button_div">Embed Links
    			<input name="upload_type" value="embed"  type="radio">  
    			<span class="checkmark"></span>
			</label>
		</div>

	@if ( ($settings_upload->upload_type == 0) OR ($settings_upload->upload_type == 2) )
	<!-- Drag Drop Zone -->
	<div class="dragzone" style="display: none;">

		<div class="box no_space_box">
			<input type="file" name="video_file" id='video_file' class='file_video inputfile inputfile-5 valid video_upload_svg' accept="video/mp4,video/x-m4v">

			<!-- <input type="file" name="video_file" id="video_file" class="inputfile inputfile-5"> -->
			<!-- <input type="file"  id="video_file" class="inputfile inputfile-5" name="files[]"  multiple="multiple"> -->
			<label for="video_file" class="video_svg_label"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></path></svg></figure> <span></span>	
			<p class="image-titl">Only MP4, x-m4v Videos are allowed..</p>

			@if(empty($row["video"]))
            <video class="video-preview event_preview_vd" controls style="display:none;margin-top:0px !important;height:300px !important">
              <source src="mov_bbb.mp4" id="video_here">
                Your browser does not support HTML5 video.
            </video>
            @else
              <input type="hidden" name="video_id" value="{{ $row['video'] }}" />
              <input type="hidden" name="video_ext" value="{{ $row['video_ext'] }}" />
            <video class="video-preview event_preview_vd" controls>
                <source src="{{ asset('uploads/events/videos/'.$row['video'].'/video.'.$row['video_ext'])}}" id="video_here" type="http/flv">
                  Your browser does not support HTML5 video.
            </video>  
            @endif 

            <div class="choose_box_vd  @if(!empty($row['video'] )) image_selected @endif ">
          @if(!empty($row['video'] ))
          <a href="javascript:void(0)" class="hide_vd_preview" onclick="$('p.image-titl').show(); $('figure').show(); $('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected');$('.file_video').css('display','block');$('#video_file').val(null); "><i class="fa fa-trash"></i></a> 
          @else
          <a href="javascript:void(0)" class="hide_vd_preview" style="display: none;" onclick="$('p.image-titl').show(); $('figure').show(); $('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected'); $('.file_video').css('display','block');$('#video_file').val(null);"><i class="fa fa-trash"></i></a>
          @endif
          </div> 
		</div>
		
			<div id="errorToShow"></div>

		<div class="drag-footer" style="display:none">
			<div class="para">

				@if (App\Helper\Helper::is_vip())
				<!-- Storage Counting -->
				<div class="progress">
					<div class="progress-bar progress-bar-success" style="width: {{ App\Helper\Helper::storage_percent(App\Helper\Helper::settings_vip()->storage_space, $total_videos_size) }}%">
				  	</div>
				</div>
				@else
				<!-- Storage Counting -->
<!-- 				<div class="progress">
					<div class="progress-bar progress-bar-success" style="width: {{ App\Helper\Helper::storage_percent($settings_upload->storage_space, $total_videos_size) }}%">
				  	</div>
				</div>
				@endif
				<div class="storage">0.00 GB / 100</div> -->

			</div>

			
		</div>
		

	</div>

	@endif

	<!-- Bottom Upload Div -->
	<div class="vid_details">

		<!-- Image, Title, Description, Tags, Keyworkds... -->
		<div class="video_info">

			@if ( ($settings_upload->upload_type == 1) OR ($settings_upload->upload_type == 2))

			<input style="display: none;" type="text" placeholder="Video Link" name="video_sites" id="video_sites" onkeyup="hideCover(this);">

			<span class="note_text" style="display: none;"><span class="bold-text">*Note: </span>upload links from dailymotion, vimeo or youtube </span>

			@else

			<input type="hidden" id="video_sites" >

			@endif

			<!-- Video Title -->

			<input type="text" placeholder="{{ Lang::get('core.video_title') }}" name="title" id="title">


			@if (App\Helper\Helper::is_admin())
		
			<!-- Only VIPs -->
            <div class="select_group">
                <select class="form-control" name="vip" id="vip">
                    <option selected="" disabled="">Who can watch this video?</option>

                    <option value="1">Only VIP</option>
                    <option value="0">All Users</option>

                </select>
            </div>


			@endif

			<!-- Video Tags -->

			<input type="text" placeholder="{{ Lang::get('core.video_tags') }}" name="tags" id="tags">

			<!-- List Of Categories -->
            <div class="select_group">
                <select class="form-control" name="category" id="category">
                    <option selected="" disabled="">{{ Lang::get('core.select_category') }}</option>

                    @if (count($categories))
                    @foreach ($categories as $category)
                    	<option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                    @endif

                </select>
            </div>


			<!-- Video Description -->
			<textarea placeholder="{{ Lang::get('core.video_description') }}"  name="description" id="description"></textarea>


			<!-- Upload Cover For Video -->
			<div id="coverIMG" style="display: none;">
				

				<div data-name="cover" class="fileUpload">
					<input name="cover" type="file" id="fileUP" accept='.jpg,.jpeg,.png'>
					<div class="fileBtn">{{ Lang::get('core.upload_cover') }}</div>
					<div class="fileName"></div>
					
				</div>


				<!-- Allowed Extensions and Size -->
				<span class="helper">JPG, JPEG, PNG MAX SIZE IS 3MB</span>
			</div>

			<span id="error1" style="display:none;top: -8px;
			font-size: 14px;
			color: #bd8414;"><i class='fa fa-warning error_icon_div'></i>Invalid Image Format!</span>

			<!-- Start Upload -->
			<div class="col-md-12 col-xs-12 col-sm-12 text-center">
			<button type="submit" class="start_vid"><span>{{ Lang::get('core.start_uploading') }}</span></button>
			<button type="submit" class="load_start_vid" style="display: none;">
				<span>Uploading<img  src="{{ asset('magelur/images/sending2.gif') }}" style="width:40px;height: 40px" />
						</span>
					</button>
			</div>
		</div>
		
	</div>
	</form>
</div>
</div>
</div>

<!-- hide map -->

<style type="text/css">
	.map{display: none;}
</style>

<!-- Validate Video Size with JS -->
<script type="text/javascript">
$('#fileUP').change(function() {
  //$('#title').val(this.value ? this.value.match(/([\w-_]+)(?=\.)/)[0] : '');
  $('div.fileName').text(this.files && this.files.length ? this.files[0].name.split('.')[0] : '');
  var ext = $('#fileUP').val().split('.').pop().toLowerCase();
if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
	// $('#error1').slideDown("slow");
	// $('.start_vid').attr('disabled',true);
	 swal("Invalid Image Format!");
	$("input[name='cover']").val(null);
	$('.fileName').hide();
	
	}
	else{
	$('.fileName').show();
		$('#error1').hide();
		$('.start_vid').attr('disabled',false);

	}

})
$(document).on('click','#video_file',function(){
$("input[name='video_file']").addClass('valid');
	 
});
	//  tag plugin
	$('input[name="tags"]').tagsInput({
		width:'auto'
	});		
	
	// Validation form
	$("#uploaderForm").validate({
		rules: {
			upload_type:{
				required:true,
			},
			title:{
				required:true,
			},
			video_sites:{
				required:true,
				url: true,
			},
			video_file:{
				required:true,
			},
			category:{
				required:true,
			},tags:{
				required:true,
			},description:{
				required:true,
				minlength:300
			},cover:{
				required:true,
			}				

		},
		messages: {
			upload_type:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Pick the Upload Type",
			},
			title:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide Video Title"
			},
			video_sites:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Enter The Embed Link",
				url:"Please Enter the valid URL"
			},
			video_file:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide Upload The Video"
			},
			category:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Select the category",
			},tags:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Enter Tags",
			},description:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Enter Video Description",
				minlength:"<span><i class='fa fa-warning error_icon_div'></i></span>Please enter at least 300 characters",
			},cover:{
				required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide Cover Image"
			}
		},
		errorPlacement: function(error, element) {
        if (element.attr("name") == "video_file") {
            error.appendTo("#errorToShow");
            $("input[name='video_file']").addClass('valid');

        } else {
        	$("input[name='video_file']").addClass('valid');
            error.insertAfter(element);
        }
        },
		  submitHandler:function(form){
		$('.load_start_vid').show();
        $('.start_vid').hide();
        form.submit();
        }
	});	

	// show upload form based on the selected checkbox
	$(document).on('click','input[name="upload_type"]',function(){
		var curval = $(this).val();

		if(curval == 'upload'){
			// show upload btn & hide embed links
			$('.dragzone').show('slow');
			$('input[name="video_sites"]').hide('slow');
			// show upload preview button
			$('#coverIMG').show();
			// hide note text
			$('.note_text').hide();
		}else{
			// empty the embed links
			$('input[name="video_sites"]').val('');
			// hide embed upload btn & show embed links
			$('.dragzone').hide('slow');
			$('input[name="video_sites"]').show('slow');
			// hide upload preview button
			$('#coverIMG').hide();			
			// show note text
			$('.note_text').show();			


		}

	});

	function hideCover(ele) {
		var curval = ele.value;
		// if(curval.length > 0)
		  $('#coverIMG').hide();

	}

	// show video preview
    $(document).on("change", "input[name='video_file']", function(evt) {
      // add class to parent div
      // var $source = $('#video_here');
      // $('.video-preview').show(); 
      // $source[0].src = URL.createObjectURL(this.files[0]);
      // $source.parent()[0].load();
   var ext = $('#video_file').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['mp4','x-m4v']) == -1) {
    swal("Invalid extension!");

$('#video_file').val(null); 
}	
    var val= $("input[name='video_file']").val();
	if(val=='')
	{
		  $('figure').show(); 
	      $('p.image-titl').show();
	}
	else{
	      $('figure').hide(); 
	      $('p.image-titl').hide(); 
	}
    
        readvideoURL(this);

    });  
    // render video
    function readvideoURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.event_preview_vd').attr('src', e.target.result).show();
          $("input[name='video_file']").css('display','none');
        }
        $('.choose_box_vd').addClass('image_selected');
        $('.hide_vd_preview').show();

        reader.readAsDataURL(input.files[0]);
      }
    }  
var val= $("input[name='video_id']").val();
if(typeof(val)  === "undefined") {

}
else{
$("input[name='video_file']").css('display','none');
}
	@if (App\Helper\Helper::is_vip())
	var MaxFilesize = {{ App\Helper\Helper::covert_to_bytes(App\Helper\Helper::settings_vip()->video_size) }};
	@else 
	var MaxFilesize = {{ App\Helper\Helper::covert_to_bytes($settings_upload->video_size) }};
	@endif
	


</script>
<script type="text/javascript">
if($('#msg').val()!="")
{
	swal($('#msg').val());
}
</script>

