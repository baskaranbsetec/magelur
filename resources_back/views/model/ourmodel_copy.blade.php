<!-- <script type="text/javascript" src="{{ asset('magelur/js/lazyload.js')}}"></script>
 -->
<div class="ourmodel_banner">
  <div class="ourmod_cnt">
    <h1>Our Models</h1>
    <div class="two_laid"><a href="{!! url('') !!}">Home</a><p>our models</p></div>
  </div>
</div>

<div class="model_photos">
<div class="container">
 <div class="modelspro clearfix">
 
    <div id="load-data">
     @foreach($models as $model)
     <a href="{{ URL::to('myprofile/'.$model->id)}}" >
    <div class="model_item" >
     <img src="{{ asset('uploads/model_profile/'.$model->profile_photo) }}" >
       <div class="colorbox">
        <div class="inner_shades">
          <h4>Real Life & People</h4>
          <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
          <p>Professional Model</p>
          <p>Book model</p>
        </div>
       </div>
    </div>
    </a>
    @endforeach

   
    <div class="butsload clearfix "  id="remove-row">

      <button id="btn-more"  data-id="{{ $model->id }}" >
        <span>Load More</span></button>

    </div>


  </div>
 </div> 
</div>  
</div>

<script>
$(document).ready(function(){
   $(document).on('click','#btn-more',function(){
        
       var id = $(this).data('id');
  
       $("#btn-more").html("Loading...");

       $.ajax({
           url : '{{ url("demos/loaddata") }}',
           method : "POST",
           data : {id:id, _token:"{{csrf_token()}}"},
           dataType : "text",
         
           success : function (data)
           {
            // console.log(data);

              if(data != '') 
              {
                  $('#remove-row').remove();
                  $('#load-data').append(data);
    
              }

              
              else
              { 
               
                  $('#btn-more').hide();
               
              }

           
           

           }
       });
   });  
}); 
</script>








