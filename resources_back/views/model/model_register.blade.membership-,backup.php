 
<!-- <link href="{{ asset('magelur/css/datepicker.min.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/bootstrap-datepicker.js')}}" type="text/javascript" charset="utf-8"></script> -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>

<div class="model_registration">
<div class="section_one">
 <div class="left_half">
  <img src="{{ asset('images/register_bg.png') }}">
 </div>
  <div class="container">
   <div class="cnt_txt wow Bounce">
    <h2>Talent Models Registration</h2>
    <h3><span></span>Promote Yourself</h3>
   </div>
  </div>
</div><!--end section_one-->

<div class="register_wrap">
  <div class="container">
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="left-content">
        <h3><span>Become</span><br/> A Model today</h3>
        <div class="left-registerbg">
          <h4>Your road to stardom starts here... <span>Sign up now!</span></h4>
          <span class="asterix">
          @if(Session::has('message'))
          {!! Session::get('message') !!}
          @endif

          </span> 

          <form method="POST" action="{{ url('/model/signup') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">

            <input type="hidden" name="_token"  value="{{ csrf_token() }}">
            <input type="hidden" name="plan_id" value="{{ $plan_id }}">
            <input type="hidden" name="decoded_id" value="{{ app('request')->input('tellfriend') }}">

            <label class="label-head">your name</label>
            <div class="form-group">
              <input type="text" name='firstname' id='firstname' class="form-control" placeholder="Enter your first name" >
            </div><!--end form-group-->

            <div class="form-group">
              <input type="text" name='lastname' id='lastname' class="form-control" placeholder="Enter your last name" >
            </div><!--end form-group-->

            <div class="about-form">
              <label class="label-head">about you</label>
              <div class="clearfix"></div>
              <div class="form-group first date" >

                <input type="text" name="DOB" id="datepicker" class="form-control" placeholder="Date of Birth"  >
                <span class="input-group-addon" style='display:none'></span>


              </div><!--end form-group-->


              <div class="form-group">
                <div class="select-style">
                  <select name='gender' id='gender' class="form-control">
                    <option value=''>Select Gender</option>
                    <option value='male'>Male</option>
                    <option value='female'>Female</option>
                    <option value='other'>Other</option>
                  </select>
                </div><!--end select-style-->
              </div>
            </div><!--end about-form-->

            <div class="common-form">
              <label class="label-head">select country</label>
              <div class="form-group">
                <div class="select-style">

                 {!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),'',array('class' => 'form-control','id' => 'country')) !!}

               </div><!--end select-style-->
             </div>
           </div><!--end common-form-->

           <div class="common-form">
            <label class="label-head">email address</label>
            <div class="form-group">
              <input type="text" name="email" id='email' class="form-control" placeholder="Enter your email address here">
            </div><!--end form-group-->
          </div><!--end common-form-->

          <div class="common-form">
            <label class="label-head">biography</label>
            <div class="form-group">

            </div><!--end form-group-->
          </div><!--end common-form-->

          <p class="file-typetxt">What do you look like?: <span>[formats: jpg/jpeg, gif, png] [max size: 2mb]</span></p>
          <div class="form-group">
            <div id="clickHere">
             <input type="file"  name='profile_photo' id="profile_photo"  />
           </div>
         </div>


          <div class="common-form">
            <label class="label-head">Membership</label>
            <div class="form-group">
              <input type="text" disabled="disabled" name="plan_name" id='plan_name' value="{{ $membership->plan_name }}" class="form-control">
              @if($plan_type == 'm')
              <input type="text" disabled="disabled" name="plan_amount" id='plan_amount' value="${{ $membership->plan_amount }}" class="form-control">
              @else
              <input type="text" disabled="disabled" name="plan_amount" id='plan_amount' value="${{ $membership->plan_amount_y }}" class="form-control">
              @endif
            </div><!--end form-group-->
          </div><!--end common-form-->  

          <div class="common-form">
            <label class="label-head">Card Details</label>
            <div class="form-group">

            </div><!--end form-group-->
          </div><!--end common-form-->                  

         <p class="terms-text">By registering with us, you agree that you are 18 years of age or older. You also agree to our <span>privacy policy</span> and terms of service. all information (including images) are true about you, and all images attached were taken at the age of 18 years old or older.</p>

         <label class="agree">I agree
          <input type='radio' name='terms_agree' value ='1'>  
          <span class="checkmark"></span>
        </label>

        <div class="submit-btn">
          <button type="submit" >submit</button>
        </div><!--end submit-btn-->
      </form>

        </div><!--end left-registerbg-->
      </div><!--end left-content-->
    </div><!--end col-->
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="right-content">
          <h3>Let us discover you!</h3>
          <p>Not yet a member? Here is what you are missing out on!</p>

          <ul class="list-txt">
              <li><span></span>Promoting yourself on our vibrant and growing network</li>
              <li><span></span>Being the next top Model</li>
              <li><span></span>Share your talents for all to see</li>
              <li><span></span>Share your photos and videos</li>
              <li><span></span>Share your social pages to be discovered in more ways</li>
              <li><span></span>Commenting and liking other talents and productions</li>
          </ul>

          <p class="acc-txt">Already have an account?</p>
        {{--*/ $url=app('request')->input('tellfriend') /*--}}
          <div class="login-btn">
            @if( app('request')->input('tellfriend'))
            <a href="{{ url('/login?tellfriend=') }}{{$url}}">Login Now!</a>
            @else
            <a href="{{ url('/login?tellfriend=') }}">Login Now!</a>
            @endif
          </div>

          <p class="add-optiontxt">Check out these additional options...</p>

          <div class="add-details">
            <div class="left-text">
                <p>Already Have an Account?</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
                @if( app('request')->input('tellfriend'))
                <a href="{{ url('/login?tellfriend=') }}{{$url}}">Login in now</a>
                @else
                <a href="{{ url('/login?tellfriend=') }}">Login in now</a>
                @endif
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>About Us</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="#">learn more</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view agents in your industry</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="#">view agents</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view events & castings</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="#">view events</a>
            </div>
          </div><!--end add-details-->

      </div><!--end right-content-->
    </div><!--end col-->
  </div><!--end container-->
</div><!--end register_wrap-->

<div class="safety-wrap">
  <div class="container">
      <h3>Safety, our top priority</h3>
      <p>Pure & Innocent - Talent Models Network mandates the safety and well-being of all users of this application as our top priority. We pride ourselves in our professionalism and transparency, and believe it is important to advise everyone about unsavoury people on the periphery of this industry, who prey on young people's ambitions. Please be aware that we do all communications through our portal application and we protect your information from all prying eyes.</p>
  </div>
</div><!--end safety-wrap-->

<div class="section_seven">
 <h1>Most Recent Registered Models</h1>
 <p class="sub-text">The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p>
 <ul class="instagramer">
  <li class="wow SlideInLeft">
  <img src="{{ asset('images/model_img.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img2.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img3.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img4.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img5.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
 </ul>
</div>



<script type="text/javascript">
 // $('#selectdate').datepicker({
 //        format:'yyyy-mm-dd',
 //        autoclose: true,
             
 //      });
$( "#datepicker" ).datepicker({
      showOtherMonths:  true,
            selectOtherMonths:  true,
            changeMonth:      true,
            changeYear:         true,
            dateFormat:         "yy-mm-dd",
            showAnim:         "slideDown",
            defaultDate:        "-18y",
            yearRange:        "-100: -16",
            //minDate:          '-100y', 
            //maxDate:          "-17y",

    });
$("#register").validate({
     ignore: "not:hidden",
      rules: {
        firstname:{
          required:true,
                 
         
        },
         lastname:{
          required:true,
                  
          
        },
         DOB:{
          required:true,
          
        },
        gender:{
          required:true,
          
        },
        country:{
          required:true,
          
        },
         biography:{
          required:true,
          
        },
         profile_photo:{
          required:true,
          
        },
        terms_agree:{
          required:true,
          
        },
         email:{
          required:true,
          email:true,
          remote:{
          url:"{!! url('model/checkuniqueemail')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#email').val();
            },
            
          }
        }
        },
         password:{
         required:true,
         rangelength:[6,12]
        },
   

      },
      messages: {
          firstname:{
          required:"This field is required", 

         
        },
        lastname:{
          required:"This field is required",       
          
        },
        DOB:{
          required:"This field is required",       
         
        },
        gender:{
          required:"This field is required",       
         
        },
        country:{
          required:"This field is required",       
         
        },
        biography:{
          required:"This field is required",       
         
        },
         profile_photo:{
          required:"This field is required",       
         
        },
         terms_agree:{
          required:"This field is required",       
         
        },
        email:{
          required:"This field is required",
          email:"Invalid Email address",
        },
        password:{
         required:"This field is required",
         rangelength:"Password must between 6 to 12 characters"
        },
      },
        submitHandler:function(form){

        toastr.success("Registered Successfully");
        form.submit();
        }
    });


 </script>
