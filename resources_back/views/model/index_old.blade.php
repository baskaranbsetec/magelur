@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Model Management</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>

	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('model/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'model/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('model/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif

		</div>

	 {!! (isset($search_map) ? $search_map : '') !!}
	
	 {!! Form::open(array('url'=>'model/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='DOB' )
							{{--*/ $t['label']=" " /*--}}
								
							@endif
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th>View Gallery</th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody>        						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
				

				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>	
						 	 @if($field['field']=='DOB')
								{{--*/$field['field']=='';/*--}}
						 @elseif($field['field']=='profile_photo')
							<a href="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" target="_blank" class="previewImage">

							<img src="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" border="0" width="50" class="img-circle">
							</a>					
						@elseif($field['field']=='status')
								
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
							@else				 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
						@endif
						 </td>
						@endif	
					 @endif					 
				 @endforeach
				 <td>

							{{--*/ $images = explode(',', $row->view_gallery); 
							$size = 0; /*--}}
						@foreach($images as $info) 

							@if($size == '0')
								<a rel="gallery2" href="{{ URL::to('uploads/model_gallery/') }}/{{ $info }}" target="_blank" class="previewImage">
								<img src="{{ URL::to('uploads/images/no.png') }}" border="0" width="30" class="img-circle" style="margin-left: 7px;"></a>
							@else
								<span style="display: none">
								<a rel="gallery2" href="{{ URL::to('uploads/model_gallery/') }}/{{ $info }}" target="_blank" class="previewImage">
								<img src="{{ URL::to('uploads/model_gallery/') }}/{{ $info }}" border="0" width="30" class="img-circle" style="margin-right:2px;"></a> 
								</span>
							@endif
						{{--*/ $size++ /*--}}
						@endforeach		 	

				 </td>	
				  <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('model/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('model/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>		 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
	
@stop