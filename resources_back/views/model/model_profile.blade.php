	
@include('model_dash_header')

<style type="text/css">
.applynow_form .selectdiv select,input{
	color:#777777;
}
</style>

<div class="dashboard-ders">
 <div class="container">
 <div class="subpanter clearfix">	
  <div class="left_part_ders">
  	 	<!--  -->
  	 	<section class="model_profile_page">
			<div class="container">
				<div class="row">
					<div class="applynow_form">
						<form method="POST" action="{{ url('/model-profile/save') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">
							{!! Form::hidden('id', $row['id']) !!}
							<div class="col-md-12 col-sm-12">
								<div class="select_gender wow bounce">
								<p>PLEASE SELECT YOUR GENDER</p>
								<ul class="gender_list">
									<li>    
							          <label for = "male">MALE</label>
							      	<!-- <input type="radio" name="radSize" id="male" value="small" checked="checked" /> -->
									<input type='radio' name='gender' value ='male' required @if($row['gender'] == 'male') checked="checked" @endif >
									</li>

									<li>
										<label for="female">FEMALE</label>
										<input type='radio' name='gender' value ='female' required @if($row['gender'] == 'female') checked="checked" @endif >
						    		</li>
						    			
						    		<li>
										<label for="female">OTHER</label>
										<input type='radio' name='gender' value ='other' required @if($row['gender'] == 'other') checked="checked" @endif >
						    		</li>
						    		
						    	</ul>
								</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-12 col-sm-12"> 
								    <div class="group">      
								      {!! Form::text('username', $row['username'],array( 'placeholder'=>'User Name','required'=>"required")) !!} 
									  <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>User Name</label>
								    </div>
								</div> 
								<div class="col-md-6 col-sm-12"> 
								   
								</div> 
							</div>
							<div class="col-md-12 col-m-12">
								
								<div class="col-md-6 col-sm-12"> 
								    <div class="group"> 
								      {!! Form::text('firstname', $row['firstname'],array( 'placeholder'=>'First Name','required'=>"required")) !!}      
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>First Name</label>
								    </div>
								</div>

								<div class="col-md-6 col-sm-12"> 
									<div class="group"> 
									  {!! Form::text('lastname', $row['lastname'],array( 'placeholder'=>'Last Name','required'=>"required")) !!}           
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Last Name</label>
							    </div>
									
								</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								
								 <div class="group">  
								 	  {!! Form::text('profession', $row['profession'],array( 'placeholder'=>'Profession','required'=>"required")) !!}               
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Profession</label>
								    </div>
								</div> 
								<div class="col-md-6 col-sm-12">
								 <div class="group">   
								     <div class="selectdiv ">
										 {!! Form::select('education',array('' => 'Select Education') +\SiteHelpers::getTableValues('education') ,$row['education'],array('class' => 'select2','id' => 'education')) !!}   

									</div>
								    </div>
								</div> 
							</div>
					
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								     <p style='color:#777777;'>Language</p>
								<div class="group"> 
								  {!! Form::text('language', $row['language'],array( 'placeholder'=>'Language','required'=>"required",'id'=>"language")) !!}                    
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Language</label>
							    </div>
								 
								</div> 
								<div class="col-md-6 col-sm-12">
								 <p style='color:#777777;'>Hobbies</p> 
								   <div class="group">
								   	  {!! Form::text('hobbies', $row['hobbies'],array( 'placeholder'=>'Hobbies','required'=>"required",'id'=>"hobbies")) !!}                          
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Hobbies</label>
			
								    </div>
								</div> 
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								<div class="group">
								   {!! Form::text('email', $row['email'],array( 'placeholder'=>'Email Address','required'=>"required",'id'=>"email")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Email Address</label>
							    </div>

								</div>
								<div class="col-md-6 col-sm-12">
								<div class="group"> 
								  {!! Form::text('contact_number', $row['contact_number'],array( 'placeholder'=>'Contact Number','required'=>"required",'id'=>"contact_number")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Contact Number</label>
							    </div> 
							</div>
								
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('insta_link', $row['insta_link'],array( 'placeholder'=>'Instagram Link','required'=>"required",'id'=>"insta_link")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Instagram Link</label>
							    </div>
								<!-- <div class="group">    
								  {!! Form::text('city', $row['city'],array( 'placeholder'=>'City','required'=>"required",'id'=>"city")) !!}
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>City</label>
							    </div> -->
							</div>

							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('twitter_link', $row['twitter_link'],array( 'placeholder'=>'Twitter Link','required'=>"required",'id'=>"twitter_link")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Twitter Link</label>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
								  {!! Form::text('fb_link', $row['fb_link'],array( 'placeholder'=>'Facebook Link','required'=>"required",'id'=>"fb_link")) !!}                          
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Facebook Link</label>
							    </div>
								<!-- <div class="group">
								   {!! Form::text('state', $row['state'],array( 'placeholder'=>'State','required'=>"required",'id'=>"state")) !!}
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>State</label>
							    </div> -->
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
 									<div class="selectdiv ">
										{!! Form::select('hair_color',array('' => 'Select Hair Color') +\SiteHelpers::getTableValues('hair_color') ,$row['hair_color'],array('class' => 'select2','id' => 'hair_color')) !!}
									</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <!-- <label>Hair Color</label> -->
								    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
								     <div class="selectdiv ">
								     	{!! Form::select('eye_color',array('' => 'Select Eye Color') +\SiteHelpers::getTableValues('eye_color') ,$row['eye_color'],array('class' => 'select2','id' => 'eye_color')) !!}

									</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								    </div>
								<!-- <div class="group">
								   {!! Form::text('zipcode', $row['zipcode'],array( 'placeholder'=>'Zipcode','required'=>"required",'id'=>"zipcode")) !!}
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Zipcode</label>
							    </div> -->
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('height',array('' => 'Select Height') +\SiteHelpers::getTableValues('heights') ,$row['height'],array('class' => 'select2','id' => 'height')) !!}
									 
									</div>
							      <span class="highlight"></span>
							      <span class="bar"></span>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('body_type',array('' => 'Select Body Type') +\SiteHelpers::getTableValues('body_type') ,$row['body_type'],array('class' => 'select2','id' => 'body_type')) !!}
										
									</div>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">
									{!! Form::text('waist', $row['waist'],array( 'placeholder'=>'Waist','required'=>"required",'id'=>"waist")) !!}                                
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Waist in inches</label>
							  		</div>

							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								 <div class="group">      
								      <!-- <input type="text" placeholder="Suit" required> -->
								      <div class="selectdiv ">
								      	{!! Form::select('ethnicity',array('' => 'Select Ethnicity') +\SiteHelpers::getTableValues('ethnicity') ,$row['ethnicity'],array('class' => 'select2','id' => 'ethnicity')) !!}
										
										</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								    </div>
								    <div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('bust_size',array('' => 'Select Bust Size') +\SiteHelpers::getTableValues('bust_size') ,$row['bust_size'],array('class' => 'select2','id' => 'bust_size')) !!}
									
									</div>
							      <span class="highlight"></span>
							      <span class="bar"></span>
							    </div>
							</div>

							<div class="col-md-6 col-sm-12"> 
									<p class="dob_title">DOB</p>
  
									<div class="col-md-4 col-sm-4 dob_day">
										<div class="group">    
										 <div class="selectdiv">

									      <select name="day" required>

									        <option value='' selected> Day</option>
									        <?php
											$day=substr($row['DOB'], 8);
											for($x = 1; $x <= 31; $x++) {
 											$value = str_pad($x,2,"0",STR_PAD_LEFT);
 											?>
											<option value='{{ $value }}' @if($day == $value)  selected='selected' @endif>{{ $value }}</option>
											<?php
											}
											?>
									      </select>
										</div>
										</div>
									</div>

										<div class="col-md-4 col-sm-4 dob_month">
										<div class="group">    
										 <div class="selectdiv ">

									      <select name='month'>
									          <option value='' selected>Month</option>
												<?php
												$month=substr($row['DOB'], 5, -3);
												$months = array( '01' => 'January' ,  '02' => 'February' ,  '03' => 'March' ,  '04' => 'April',  '05' => 'May',  '06' => 'June',  '07' => 'July',  '08' => 'August',  
												'09' => 'September',  '10' => 'October',  '11' => 'November',  '12' => 'December' );
												foreach ($months as $key => $value)
												{
												?>
												<option value='{{$key}}' @if($month == $key)  selected='selected' @endif>{{ $value }}</option>
												<?php
												}
												?>
									      </select>
											</div>
										</div>
									</div>

										<div class="col-md-4 col-sm-4 dob_year">
										<div class="group">    
										 <div class="selectdiv ">
											<select name='year'>
											<option value='' selected> Year</option>
											<?php
											$year=substr($row['DOB'],0,-4);
											$firstYear = (int)date('Y') - 70;
											$lastYear = $firstYear + 52;
											for($i=$firstYear;$i<=$lastYear;$i++)
											{
											?>
											<option value='{{ $i }}' @if($year == $i)  selected='selected' @endif>{{ $i }}</option>
											<?php
											}
											?>
											</select>
										</div>
										</div>
									</div>
								
								<span class="highlight"></span>
							    <span class="bar"></span>
							</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">
							      {!! Form::text('shoe_size', $row['shoe_size'],array( 'placeholder'=>'Shoe Size','required'=>"required",'id'=>"shoe_size")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Shoe Size</label>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv">
							      	{!! Form::select('marital_status',array('' => 'Select Marital status') +\SiteHelpers::getTableValues('marital_status') ,$row['marital_status'],array('class' => 'select2','id' => 'marital_status')) !!}
									
									</div>
							    </div>
							</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12">
									<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('smoking',array('' => 'Select Smoking') +\SiteHelpers::getTableValues('smoking') ,$row['smoking'],array('class' => 'select2','id' => 'smoking')) !!}
									
									</div>
							    </div>
							     {{--*/ $json_cat   = '['.$row->model_categories.']' /*--}}
							    <div class="group">      
									<div class="selectdiv ">
									{!! Form::select('model_categories[]',array('' => 'Select Categories') +\SiteHelpers::getTableValues('model_categories') ,$row->model_categories,array('class' => ' model_categories','id' => 'model_categories','multiple'=>'multiple')) !!}
									</div>
							    </div>
							     <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('drinking',array('' => 'Select Drinking') +\SiteHelpers::getTableValues('drinking') ,$row['drinking'],array('class' => 'select2','id' => 'drinking')) !!}
									
									</div>
							    </div>
							    <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('drugs',array('' => 'Select Drugs') +\SiteHelpers::getTableValues('drugs') ,$row['drugs'],array('class' => 'select2','id' => 'drugs')) !!}
									
									</div>
							    </div>
							    <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('children',array('' => 'Select Children') +\SiteHelpers::getTableValues('children') ,$row['children'],array('class' => 'select2','id' => 'children')) !!}
									
									</div>
							    </div>
								</div>

								<div class="col-md-6 col-sm-12">
     							  <p style='color:#777777;'>Biography</p>
							    <div class="group">
							      {!! Form::textarea('biography', $row['biography'],array('class'=>'theme_textarea', 'placeholder'=>'Biography','required'=>"required",'id'=>"biography")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Biography</label>
							    </div>
							</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
							    
							</div>
							
							<div class="col-md-6 col-sm-12">
								
							</div>
							</div>

						

							<div class="col-md-12 col-m-12">
								{{--*/ $json_cat   = '['.$row->model_industries.']' /*--}}
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
									{!! Form::select('model_industries[]',\SiteHelpers::getTableValues('model_industries') ,$row->model_industries,array('class' => ' model_industries','id' => 'model_industries','multiple'=>'multiple')) !!}
									</div>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('publishes',array('' => 'Select Publishes') +\SiteHelpers::getTableValues('publishes') ,$row['publishes'],array('class' => 'select2','id' => 'publishes')) !!}
							
									</div>
							    </div>
							</div>
							</div>

							

							
									<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
								{!! Form::text('hip', $row['hip'],array( 'placeholder'=>'Hip','required'=>"required",'id'=>"hip")) !!}                                
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Hip in inches</label>
								</div>
							</div>

							
							<div class="col-md-6 col-sm-12">
								<div class="group">
								<div class="selectdiv ">
										{!! Form::select('years',array('' => 'Year of experience') +[0,1, 2, 3,'More than 3 years'] ,$row['year'],array('class' => 'select2','id' => 'years')) !!}
							
									</div>      

							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								
								<p class="text_design">Body Piercing</p>
								
								<ul class="gender_list">
									

									<li>
										<label for="yes">Yes</label>
										{!! Form::radio('body_piercing',1,($row['body_piercing']==1) ? true:false,array()) !!}  
						    		</li>
						    			
						    		<li>
						    			<label for = "no">No</label>
						    			{!! Form::radio('body_piercing',2,($row['body_piercing']==2) ? true:false,array()) !!}  
						    		</li>
						    	</ul>

							      
							</div>
							
							<div class="col-md-6 col-sm-12">
								<p class="text_design">Tattoos</p>
								
								<ul class="gender_list">
									

									<li>
										<label for="yes">Yes</label>
										{!! Form::radio('tattoos',1,($row['tattoos']==1) ? true:false,array()) !!}
						    		</li>
						    			
						    		<li>
						    			<label for = "no">No</label>
						    			{!! Form::radio('tattoos',2,($row['tattoos']==2) ? true:false,array()) !!}
						    		</li>
						    	</ul>
							    
							</div>
							</div>
							
							<div class="col-md-12 col-m-12">
						
								<div class="col-md-12 col-sm-12">
								<p class="text_design">Model Experience</p>
								
								<ul class="gender_list">
									<li>    
							          <label for = "New Face">New Face</label>
										{!! Form::radio('model_experience',1,($row['model_experience']==1) ? true:false,array()) !!}
									</li>

									<li>
										<label for="Aspiring Model">Aspiring Model</label>
										{!! Form::radio('model_experience',2,($row['model_experience']==2) ? true:false,array()) !!}
						    		</li>
						    			
						    		<li>
						    			<label for = "Experienced Model">Experienced Model</label>
						    			{!! Form::radio('model_experience',3,($row['model_experience']==3) ? true:false,array()) !!}
						    		</li>
						    		<li>
						    			<label for = "Professional Model">Professional Model</label>
						    			{!! Form::radio('model_experience',4,($row['model_experience']==4) ? true:false,array()) !!}
						    		</li>
						    	</ul>
							
							    
							      
							</div>
							
							</div>

<div class="events_map_form events_map_form_model col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 event_form_fields">
							<div class="events_label_blk col-md-12">
<!-- 							<div class="col-md-6 form-group  ">
								<label for="Enddate" class=" control-label text-left"> Select Your Location </label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group  group event_input_field_blk">
								<input name='geocomplete' id="geocomplete" type="text" placeholder="Select Your Location" class="form-control" value="{{ $row["address"] }}" />
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Select Your Location</label>
							</div>
<!-- 							<div class="col-md-6 form-group  ">
								<label class=" control-label text-left">Address</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group group event_input_field_blk">
								<input name="formatted_address" type="text" class="form-control" placeholder="Address" value="{{ $row["address"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Address</label>

							</div>
<!-- 							<div class="col-md-6 form-group  ">
								<label class=" control-label text-left">City</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group group  event_input_field_blk">
								<input name="locality" type="text" class="form-control" placeholder="City" value="{{ $row["city"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>City</label>
							</div>	
<!-- 							<div class="col-md-6 form-group  ">
								<label class=" control-label text-left">Zip Code</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group  group event_input_field_blk">
								<input name="postal_code" type="text" class="form-control" placeholder="Zip Code" value="{{ $row['zipcode'] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Zip Code</label>
							</div>
<!-- 							<div class="col-md-6 form-group  ">
								<label class=" control-label text-left">State</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group  group event_input_field_blk">
								<input name="administrative_area_level_1" class="form-control" placeholder="Select Your Location" type="text" value="{{ $row["state"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>State</label>
							</div>
<!-- 							<div class="col-md-6 form-group  ">
								<label class=" control-label text-left">Country</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12 form-group  group event_input_field_blk">
								<input name="country" type="text" class="form-control" placeholder="Country" value="{{ $row["country"] }}">	
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Country</label>
							</div>
						<input name="ids" type="hidden" class="form-control" value="{{ $row["id"] }}">
						<input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
						<input type="hidden" name="lng" value="{{ $row['longitude'] }}" />	
						</div>
						</div>
						<div class="col-md-6 map_blk">
							<div class="input-group m-b" style="width:100% !important;">
							<div id="map_canvas" class="map_canvas" style="width: 100%;height: 290px;display:none;"></div>
						</div>
						</div>
						</div>
							<div class="col-md-12 col-sm-12">
								<div class="apply_almost wow SlideInLeft">
									

									<div class="load_button">
										<button type="submit"><span>Submit</span></button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!--  -->
</div>
</div>
</div>
</div>

		

		

	</div>

<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">

      <script>
       $('#geocomplete').on('input', function() {
      	$('.map_canvas').css('display','block');

      });
      if($('#geocomplete').val()!='')
      {
      	$('.map_canvas').css('display','block');

      }
      var formid = $('form#register').find('input[name="ids"]').val();			
		if(formid ){
			var mapStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];
				var options = {
					map: ".map_canvas",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					details: "form",					
					types: ["geocode", "establishment"],
					mapOptions: {
			            styles: mapStyles
			          },
					markerOptions: {
					draggable: true,
					icon: '{{asset("images/spotlight-poi2.png")}}',
					}
				};
				$("#geocomplete").geocomplete(options);
			}
				$("#geocomplete").geocomplete({
					map: ".map_canvas",
					details: "form",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					types: ["geocode", "establishment"],

				});	
		$("#geocomplete").bind("geocode:dragged", function (event, latLng)
		{
          $("input[name=lat]").val(latLng.lat());
          $("input[name=lng]").val(latLng.lng());
       
            var latlng = new google.maps.LatLng(latLng.lat(), latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {

                    if (results[0])
                    {

                        $("#geocomplete").val(results[0].formatted_address)
                        console.log(results[0])
                        $.each(results[0].address_components, function (index, data)
                        {
                           
                            if (data.types[0] == "administrative_area_level_1")
                            {
                                $("input[name=administrative_area_level_1]").val(data.long_name)
                            }
                            
                            if (data.types[0] == "administrative_area_level_2")
                            {
                                $("input[name=locality]").val(data.long_name)
                            }
                            
                            
                            if (data.types[0] == "country")
                            {
                            	 $("input[name=country]").val(data.long_name)
                            }
                            
                            if (data.types[0] == "postal_code")
                            {
                                $("input[name=postal_code]").val(data.long_name)
                            }
                            else{
                            	 $("input[name=postal_code]").val('')
                            }
                            
                                $("input[name=formatted_address]").val(results[0].formatted_address)
                                // alert("Location: " + results[1].formatted_address);
                        });
                    }
                }
            });		
            });		
		$(function(){
			    	$('#model_categories').val({{ $json_cat }}).trigger('change');
			    	$('#model_industries').val({{ $json_cat }}).trigger('change');

		    $('.map').hide();


		// tinyMCE.init({
		// theme : "advanced",
		// mode: "exact",
		// elements : "biography",
		// theme_advanced_toolbar_location : "top",
		// theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		// + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		// + "bullist,numlist,outdent,indent",
		// theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		// +"undo,redo,cleanup,code,separator,sub,sup,charmap",
		// theme_advanced_buttons3 : "",
		// height:"200px",
		// width:"505px"
		// });
		// $('#hobbies').tagsly({});
		// $('#language').tagsly({});
 //  tag plugin
      $('input[name="hobbies"]').tagsInput({
        width:'auto',
      });   
      $('input[name="language"]').tagsInput({
        width:'auto'
      });

$( ".model_industries" ).select2( {
			placeholder : ' '
			} );
$( ".model_categories" ).select2( {
			placeholder : ' '
			} );
			$( " .select2-multiple" ).select2( {
			placeholder : ' Model Categories'
			} );
	
	})

	  $('#register').on('click',function(){
         if($('#language_tagsinput span.tag').text() != '')
         {
        $(this).valid();
         }
          if($('#hobbies_tagsinput span.tag').text() != '')
         {
        $(this).valid();

         }

      }); 
	$("#register").validate({
     ignore: "not:hidden",
      rules: {
  		contact_number: { required:true, number:true,minlength:10,},
		username: { required:true },
		firstname: { required:true },
		lastname: { required:true },
		insta_link: { required:true,url:true },
		twitter_link: { required:true,url:true },
		fb_link: { required:true,url:true },
		profession: {required:true },
		body_type: { required:true },
		eye_color: { required:true },
		education: { required:true },
		language: { required:true },
		hobbies: { required:true },
		day: { required:true },
		month: { required:true },
		year: { required:true },
		email: { required:true,email:true },
		biography: { required:true },
		geocomplete: { required:true },
		formatted_address: { required:true },
		locality: { required:true },
		postal_code: { required:true, number:true,},
		administrative_area_level_1: { required:true },
		country: { required:true },
		hair_color: { required:true },
		bust_size: { required:true },
		ethnicity: { required:true },
		shoe_size: { required:true,number:true,},
		hip: { required:true,number:true, },
		waist: { required:true,number:true,},
		height: { required:true,number:true, },
		marital_status: { required:true, },
		smoking: { required:true,},
		'model_categories[]': { required:true,},
		drinking: { required:true,},
		drugs: { required:true,},
		children: { required:true,},
		'model_industries[]': { required:true,},
		publishes: { required:true,},
		body_piercing: { required:true,},
		tattoos: { required:true,},
		model_experience:{ required:true,},
		years:{ required:true,},

        


      },
       messages: {
        contact_number:{ number:"Please enter valid phone number",
        minlength:"Phone number should be 10 number" },
        email:{ required:"Please enter email id", email:"Please enter valid email id" },
        city:{ required:"Please select city" },
        zipcode:{ required:"Please select zipcode", number:"Numbers only allowed",  },
        state:{ required:"Please select state" },
        country: { required:"Please select country" },
        DOB: { required:"Date of Birth required"  },
        shoe_size:{ number:"Size should be a number", },
        hip:{ number:"Hip size should be a number", },
        waist:{ number:"Waist size should be a number", },
        height:{ number:"Height should be a number", },

       },
        submitHandler:function(form){
        toastr.success("Saved Successfully");
        setTimeout(function(){
        form.submit();
      },2000);
        }
    });
$('select').on('change', function() {
        $(this).valid();
    });
$("input").prop('required',true);

    </script>

