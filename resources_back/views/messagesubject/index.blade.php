@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Message Subject Management</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>


	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('messagesubject/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'agency/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('messagesubject/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif

		</div>
  

<!-- 	 {!! (isset($search_map) ? $search_map : '') !!}
 -->	
	 {!! Form::open(array('url'=>'messagesubject/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Id' )
							{{--*/ $t['label']=" " /*--}}
								
							@endif
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody>  
        <tr id="orderDetails">
<td class="number"></td>
<td></td>
<td></td>
<td id="name" class="fieldsearch"> <div class="select-style">{!! Form::text('name',"{$name}",array('class'=>'form-control', 'placeholder'=>'Name', )) !!} </div></td>
<td id="status" class="fieldsearch"> <div class="select-style">
<select name='status' class = 'select2 '>
<option value="">Select option</option>
<option value="1" {{ $status == '1' ? 'selected' : ''}}>Active</option>
<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>
</select>
</div>
</td>
<td class="icons">
    <input type="hidden"  value="Search">
    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
    <a href="{{ url('/messagesubject') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
</td>
</tr>          						
            {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
				@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('messagesubject/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('messagesubject/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td>
 -->
 {{--*/ $i++ /*--}}
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>	 
						 	@if($field['field']=='id')
								{{--*/$field['field']=='';/*--}}
						 	@elseif($field['field']=='status')

						 		@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif	
							@else			 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
							@endif
						 </td>
						@endif	
					 @endif					 
				 @endforeach
				  <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('messagesubject/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('messagesubject/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>		 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	<!-- @include('footer') -->
	@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
@endif
	</div>
</div>	
	</div>	  
</div>	
	
@stop