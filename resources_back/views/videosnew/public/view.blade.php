<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
						<tr>
							<td width='30%' class='label-view text-right'>Id</td>
							<td>{{ $row->id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Id</td>
							<td>{{ $row->user_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>{{ $row->user_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Video Id</td>
							<td>{{ $row->video_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Title</td>
							<td>{{ $row->title}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Description</td>
							<td>{{ $row->description}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Tags</td>
							<td>{{ $row->tags}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Category</td>
							<td>{{ $row->category}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Status</td>
							<td>{{ $row->status}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Vip</td>
							<td>{{ $row->vip}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Preview</td>
							<td>{{ $row->preview}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Thumb</td>
							<td>{{ $row->thumb}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Video Path</td>
							<td>{{ $row->video_path}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Duration</td>
							<td>{{ $row->duration}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Width</td>
							<td>{{ $row->width}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Height</td>
							<td>{{ $row->height}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Size</td>
							<td>{{ $row->size}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Type</td>
							<td>{{ $row->type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Youtube Id</td>
							<td>{{ $row->youtube_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Vimeo Id</td>
							<td>{{ $row->vimeo_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Dailymotion Id</td>
							<td>{{ $row->dailymotion_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created At</td>
							<td>{{ $row->created_at}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Updated At</td>
							<td>{{ $row->updated_at}} </td>
							
						</tr>
							
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	