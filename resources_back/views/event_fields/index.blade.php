@extends('layouts.app')

@section('content')
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Event Field Management</h1>
		

	</div>


	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >
			
	   		<a href="{{ URL::to('event/fields/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			

		</div>

{!! Form::open(array('url'=>'/event/fields/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}	
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
					<th> No </th> 
					<th> <input type="checkbox" class="checkall" /></th> 
					<th> Fields </th>
					<th> Values </th>
					<th> Status </th>
					<th> Action </th>   
					
			  </tr>
        </thead>

        <tbody>        						
           {{--*/ $i = ($results->currentPage() - 1) * $results->perPage() + 1;/*--}}
				@if(count($results) >0)
				@foreach($results as $result)
				<tr>
				    <td> {{ $i }} </td> 
				    <td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $result->id }}" />  </td>	 
				    <td> 
				    	{{$result->field_categories}}
				    </td>  
				    <td> 
				    	{{$result->field_values}}
				    </td> 
				    <td> 
				    	@if( $result->status == 'active')
						<span class="label label-success" >Active</span>
						@else
						<span class="label label-danger" >Inactive</span>
						@endif
				    </td>  
				     <td>
					 	
						
						<a  href="{{ URL::to('event/fields/update/'.$result->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						
												
					
				</td>
				     
				    {{--*/ $i++ /*--}}
				</tr>
				@endforeach                            
				@endif
              
        </tbody>
      
    </table>
   
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions form_footer">
	 
	   {!! Form::open(array('url'=>'event/fields','method'=>'GET')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="Sort" class="select-alt">
		  <option value=""> Sort </option>	    
		  <option value="field_categories">Fields</option>
		  <option value="field_values">Values</option>
		  <option value="status">Status</option>
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $results->total() }}</b>
		</p>		
	   </div>
		<div class="col-sm-4">	
		<div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  
 			<div class=""> 
	  			{!! str_replace('/?', '?', $results->appends(Input::except('page'))->render()) !!}
	  		</div>
	  </div>
	  </div>
	</div>	
	</div>	
</div>	
	</div>	  
</div>	
	
@stop