<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>ID</td>
						<td>{{ $row->ID}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>OwnerId</td>
						<td>{{ $row->OwnerId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Title</td>
						<td>{{ $row->Title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Uri</td>
						<td>{{ $row->Uri}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->Status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Description</td>
						<td>{{ $row->Description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Tags</td>
						<td>{{ $row->Tags}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Location</td>
						<td>{{ $row->Location}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>StartDate</td>
						<td>{{ $row->StartDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>EndDate</td>
						<td>{{ $row->EndDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Phone</td>
						<td>{{ $row->Phone}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Website</td>
						<td>{{ $row->Website}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ProfileImage</td>
						<td>{{ $row->ProfileImage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>BannerImage</td>
						<td>{{ $row->BannerImage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>LastEdit</td>
						<td>{{ $row->LastEdit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created</td>
						<td>{{ $row->Created}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	