@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('event?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('event/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('event/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('event/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>ID</td>
						<td>{{ $row->ID}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>OwnerId</td>
						<td>{{ $row->OwnerId}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Title</td>
						<td>{{ $row->Title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Uri</td>
						<td>{{ $row->Uri}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->Status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Description</td>
						<td>{{ $row->Description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Tags</td>
						<td>{{ $row->Tags}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Location</td>
						<td>{{ $row->Location}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>StartDate</td>
						<td>{{ $row->StartDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>EndDate</td>
						<td>{{ $row->EndDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Phone</td>
						<td>{{ $row->Phone}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Website</td>
						<td>{{ $row->Website}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ProfileImage</td>
						<td>{{ $row->ProfileImage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>BannerImage</td>
						<td>{{ $row->BannerImage}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>LastEdit</td>
						<td>{{ $row->LastEdit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created</td>
						<td>{{ $row->Created}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop