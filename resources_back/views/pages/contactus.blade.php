<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>

    <script src="{{ asset('magelur/js/valid.js')}}"></script>

    <script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  


	<div class="magelur_about faq_sec">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner faq_banner">
					

					<div class="banner_title wow bounce">
						<h1>CONTACT US</h1>
							<ul class="link_list">
								<li class="active"><a href="{!! url('') !!}">HOME</a></li>
								<li>//</li>
								<li><a href="javascript:void(0)">CONTACT US</a></li>
							</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- Header End -->

<div class="bootfaq cnt_boot clearfix contact_full_page">
 <div class="col-sm-6">
  <div class="we_weleder">
   <h1>We welcome and value your feedback</h1>
  </div>
  <div class="contactus_form">

    {!! Form::open(array('url'=>'save/contactus', 'class'=>'form-signup','method'=>'post','parsley-validate'=>'','novalidate'=>'','id'=>'login')) !!}
  	<div class="main_partiels">
  	<div class="sendform">
  	 <a href="javascript:void(0)">Send us a message</a>
  	</div>	
        <b> <div id='message' class='w3-animate-bottom' style='top: -8px;font-size: 15px; color: #bd8414;margin-bottom: 17px;'></div></b>

  	<div class="form-group form-group_full">
  	 <div class="inputslogin">
  	  <input type="text" name="name" placeholder="Enter your name here">	
  	 </div>
  	</div>
  	<div class="form-group form-group_full">
  	 <div class="inputslogin">	
  	  <input type="text" name="city" placeholder="Enter your city here">
  	 </div> 	
  	</div>	
  	<div class="form-group form-group_full">
  	  {!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),'',array('class' => '','id' => 'country')) !!}
  	</div>
  	<div class="inputslogin contactus_email_input">
  	 <input type="text" name="email" placeholder="Enter your email here">
  	</div>
  	<div class="text_area">

  	 <textarea name='hai' placeholder="Write your message here"></textarea>
  	</div>	
  	</div>
 <div >
    
    <button type='submit' class="body button cont_btn" value=''>Submit</button>
 </div> 
    {!! Form::close() !!}	
  </div>	
 </div> 
 <div class="col-sm-6">
  <div class="we_weleder last_wes">
   <h1>Get in Touch</h1>
  </div>
  <div class="share_idea">
  	<p>Share your great idea or question with us.</p>
  	<p>Got something to say? Have a great idea? Want a new feature on here? If you have any feedback or question for us, then drop us a line.</p>
  	<p>We welcome and value your feedback. Drop us a line and look out for our speedy response. Every idea and question will be looked at and evaluated.</p>
  </div>
   <div class="we_weleder last_wes">
   <h1>We Are Sociable</h1>
  </div>
  <div class="share_idea">
  	<p>You can reach us by submitting the form on the left or email us directly or meet us on line. Join us on our social pages.</p>
  	<ul>
  	 <li class="lik_fb"><a href="javascript:void(0)">Like us on Facebook</a></li>
  	 <li class="frnd_instagra"><a href="javascript:void(0)">Friend us on Instagram</a></li>
  	 <li class="flow_twitter"><a href="javascript:void(0)">Follow us on Twitter</a></li>
  	</ul>	
  </div>	
 </div>	
</div>

<script type="text/javascript">
    

$("#login").validate({
     ignore: "not:hidden",
      rules: {
       
       email:{
          required:true,
          email:true,
           
        },
         name:{
         required:true,
         
        },
        hai:{
         required:true,
         
        },
        city:{
         required:true,
         
        },
        country:{
         required:true,
         
        },
      },
      messages: {
       
        email:{
          required:"This field is required",
          email:"Invalid Email address",
        },
        
       

      },
      
        submitHandler: function(form) {
       
          $('.cont_btn').click(function() {
          $(this).prop('disabled', true);
          $(this).css('cursor', 'not-allowed');

          })
          $("#message").html('Your details send sucessfully ! !');   

          form.submit();
          // form.submit();
 
        },
        });

</script>
		


