@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<style type="text/css">
.tagsinput {
min-height: 36px !important;
height: 0px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
  <div class="page-content row">
	<div class="page-content-wrapper m-t">	 	
		<div class="sbox">
			<div class="sbox-title">
				<h1>Images Management</h1>

<!-- 		<div class="sbox-tools" >
@if($access['is_excel'] ==1)
<a href="{{ URL::to('events/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
<i class="fa fa-cloud-download"></i></a>
@endif

<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
@if(Session::get('gid') ==1)
<a href="{{ URL::to('module/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
@endif 
</div> -->
</div>
<div class="sbox-content"> 
	<div class="sbox-tools pull-left" >
				<!-- Approve/unapprove -->
<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Approve" data-type="1">
<i class="fa fa-check"></i>&nbsp;Approve</a>
<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Unapprove" data-type="2">
<i class="fa fa-times"></i>&nbsp;Unapprove</a>
		@if($access['is_add'] ==1)
		<a href="{{ URL::to('imagesnew/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create </a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default updatestatus" data-type="0" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
				<a href="{{ URL::to( 'imagesnew/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}" style="display:none"><i class="fa  fa-search"></i> </a>				
			<!-- @if($access['is_excel'] ==1)
			<a href="{{ URL::to('imagesnew/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif -->
			</div>
 

 
	

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'imagesnew/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
		     {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}

	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
							@if($t['label']!='ID'   && $t['label']!='Ext' && $t['label']!='Type' && $t['label']!='OwnerId' && $t['label']!='Description' && $t['label']!='MediaUri' && $t['label']!='LastEdit')
								<th><span>{{ $t['label'] }}</span></th>

							@endif	
						@endif 
					@endif
				@endforeach
				<th><span>Owner Name</span></th>		
				<th><span>Image</span></th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
				<th ><span>Message</span></th>
			  </tr>
        </thead>

        <tbody>    
        <tr id="orderDetails">
		<td class="number"></td>
		<td></td>
		<td id="AlbumId" class="fieldsearch"> <div class="select-style"  style="width:80px">{!! Form::text('AlbumId',"{$AlbumId}",array('class'=>'form-control', 'placeholder'=>'AlbumId', )) !!} </div></td>

		<td id="Usertype" class="fieldsearch"> <div class="select-style"  style="width:80px">
			<!-- {!! Form::text('Usertype',"{$Usertype}",array('class'=>'form-control', 'placeholder'=>'User Type', )) !!}  -->
		<select name='Usertype' id='Usertype' class = 'select2 '>
		<option value="">Select option</option>
		<option value="agent" {{ $Usertype == 'agent' ? 'selected' : ''}}  id='type-name'>Agent</option>
		<option value="model" {{ $Usertype == 'model' ? 'selected' : ''}}  id='type-name'>Model</option>
		</select>
		</div></td>
		<td id="Title" class="fieldsearch"> <div class="select-style">{!! Form::text('Title',"{$Title}",array('class'=>'form-control', 'placeholder'=>'Title', )) !!} </div></td>
		<!-- <td id="Tags" class="fieldsearch"> <div class="select-style"><input type="text" name='tags' id='tags' class='form-control' value="" /> </div></td> -->
		<td id="Tags" class="fieldsearch" style="width:100px"> <div class="select-style">{!! Form::text('tags','',array('id'=>'tags','class'=>'form-control', 'placeholder'=>'Tags', )) !!} {!! Form::hidden('Tags','',array('id'=>'Alltags','class'=>'form-control', 'placeholder'=>'Tags', )) !!}</div></td>
		<td id="status" class="fieldsearch"> 
		<div class="select-style">
		<select name='status' class = 'select2 '>
		<option value="">Select option</option>
		<option value="Active" {{ $status == 'Active' ? 'selected' : ''}}>Active</option>
		<option value="Inactive" {{ $status == 'Inactive' ? 'selected' : ''}}>Inactive</option>
		<option value="Deleted" {{ $status == 'Deleted' ? 'selected' : ''}}>Deleted</option>
		</select>
		</div>
		</td>
		<td id="Created" class="fieldsearch"> <div class="select-style">{!! Form::text('Created',"{$Created}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Created Date', )) !!} </div></td>
		<td id="ownername" class="fieldsearch"> 
			<div class="select-style">
				@if($Usertype=='agent')
				{!! Form::select('ownername',array('' => 'Select username') +\SiteHelpers::getTableValues('agents'),'{$ownername}',array('class' => 'select2','id' => 'username agent_name')) !!}
				@elseif($Usertype=='model')
				{!! Form::select('ownername',array('' => 'Select username') +\SiteHelpers::getTableValues('models'),'{$ownername}',array('class' => 'select2','id' => 'username model_name')) !!}
				@else
				{!! Form::text('ownername',"",array('class'=>'form-control', 'placeholder'=>'ownername','','id'=>'ownername' )) !!}
				@endif
			</div>
		</td>




		<td class="icons">
		    <input type="hidden"  value="Search">
		   <!--  <input type="hidden"  name="Tags" id="Alltags" value=""> -->

		    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
		    <a href="{{ url('imagesnew') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
		</td>
		</tr>    						
            {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->ID }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu" style="background-color:#ea962e">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('imagesnew/show/'.$row->ID.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('imagesnew/update/'.$row->ID.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->

				 @foreach ($tableGrid as $field)
				 	@if($field['label']!='ID' && $field['label']!='Ext' && $field['label']!='Type' && $field['label']!='OwnerId' && $field['label']!='Description' && $field['label']!='MediaUri' && $field['label']!='LastEdit')
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>
						 @if($field['field']=='Status')
								
								@if( $row->Status == 'Active')
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@elseif( $row->Status == 'Inactive')
								<span class="label label-warning">{{ Lang::get('core.fr_minactive') }}</span>
								@else
								<span class="label label-danger">Deleted</span>
								@endif	
								@else				 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
						@endif
						 </td>
						 
						@endif	
					 @endif
					@endif					 
				 @endforeach	
<td>{{ App\Helper\Helper::channel_by_id($row->OwnerId,$row->user_type) }}</td>	
				 <td>					 
				 	<img src="{{ url('uploads/model_gallery/'.$row->ID.'_small.'.$row->Ext) }}" alt="{{$row->Title}}">						 
				 </td>
				 <td>
				 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('imagesnew/show/'.$row->ID.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
					@endif
					@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('imagesnew/update/'.$row->ID.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
					@endif
				 </td>
				 <td><a  class="tips btn btn-xs btn-white" data-toggle="modal" data-target="#myModal" title="Send Mail"  onclick="getmailid('{!! SiteHelpers::getUserEmailbyId($row->OwnerId,$row->user_type) !!}');"><i class="fa fa-envelope "></i></a></td>	 
              {{--*/ $i++ /*--}}
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />
	</div>
	{!! Form::close() !!}
<!-- The Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:black">Send Mail</h4>
      </div>
      <div class="modal-body">
       <form role="form" method="get" id="reused_form" action="agentmail/images">

            <div class="row">
            	<div class="col-sm-12 form-group">
                	<label for="subject" style="color:black">
                        Subject:</label>
                     <textarea class="form-control" type="textarea" name="subject" id="subject" maxlength="200" rows="1" style="background-color:white;color:black"></textarea>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="message" style="color:black">
                        Message:</label>
                    <textarea class="form-control" type="textarea" name="message" id="message" maxlength="6000" rows="7" style="background-color:white;color:black"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                         From:</label>
                    <input type="email" class="form-control" id="fromemail" name="fromemail" style="background-color:white;color:black" value = "{{ CNF_EMAIL }}" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                        To:</label>
                    <input type="email" class="form-control" id="email" name="email" style="background-color:white;color:black" required>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-white pull-right" >Send →</button>
                </div>
            </div>

        </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-4">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			@if( $field['field'] == 'ID' || $field['field'] == 'OwnerId' || $field['field'] == 'MediaUri' || $field['field'] == 'Description'|| $field['field'] == 'Ext'|| $field['field'] == 'Type'|| $field['field'] == 'LastEdit') {{ $field['field'] == '' }} @else
			  <option value="{{ $field['field'] }}" 
			  @endif
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				@endif	
			  >@if( $field['label'] == 'ID'  || $field['label'] == 'OwnerId'|| $field['label'] == 'MediaUri'|| $field['label'] == 'Description'|| $field['label'] == 'Ext' || $field['label'] == 'Type'|| $field['label'] == 'LastEdit') {{ $field['label'] == '' }} @else {{ $field['label']}} @endif </option>
			@endif	  
		  @endforeach
		 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-2">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
				
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
</p>
@endif
	  </div>
	</div>

	<!-- @include('footer') -->
	</div>
</div>	
	</div>	  
</div>	
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
//  tag plugin
var alltags='';
$('#tags').tagsInput({
	width:'auto',
	onChange: function(elem, elem_tags)
	{
		// alert(elem_tags);
		alltags =elem_tags+','+alltags;
		document.getElementById("Alltags").value=alltags;
		// alert(alltags);
		
	}
});
$("#reused_form").validate({
			ignore: "not:hidden",
			rules: {
				message:{
					required:true,
				},
				subject:{
					required:true,
				},
		}
	});

function getmailid(mailid)
 {
 	// alert(mailid);
 	$('#email').val(mailid);
 }
$('.close').click(function(){
$('textarea[name=subject]').val('');
$('textarea[name=message]').val('');	
$('label.error').hide();	

});
$("#ownername").click(function(){
    swal("Please search Usertype to choose Owner Name");
});

$( document ).ready(function() {
    
$("#Created").change(function() {
   
	$(".datetimepicker").hide();
});

});
 $(document).on('click','.updatestatus',function(){
        var status = $(this).data('type');
        $('#actiontype').val(status);
        if(status=='0' && $("input[name='ids[]']:checked").length==0){
            alert('Please select atleast one checkbox!');
        }else if($("input[name='ids[]']:checked").length>0){
            $('#SximoTable').submit();
        }else{
            alert('select checkbox');
        }
        
    });
</script>
	
@stop
