@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('imagesnew?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('imagesnew/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('imagesnew/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('imagesnew/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
						<tr>
							<td width='30%' class='label-view text-right'>ID</td>
							<td>{{ $row->ID}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>OwnerId</td>
							<td>{{ $row->OwnerId}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>AlbumId</td>
							<td>{{ $row->AlbumId}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>{{ $row->user_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>MediaUri</td>
							<td>{{ $row->MediaUri}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Title</td>
							<td>{{ $row->Title}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Description</td>
							<td>{{ $row->Description}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Tags</td>
							<td>{{ $row->Tags}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Ext</td>
							<td>{{ $row->Ext}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Type</td>
							<td>{{ $row->Type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Status</td>
							<td>{{ $row->Status}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>LastEdit</td>
							<td>{{ $row->LastEdit}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created</td>
							<td>{{ $row->Created}} </td>
							
						</tr>
					
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop