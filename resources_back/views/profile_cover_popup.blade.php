@foreach($images as $image)
<div id="myAvatar" class="modal fade  my_profile_page" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
<h4 class="modal-title">Profile Banner upload</h4>
</div>
<div class="modal-body">
<div class="alert-container"></div>

<span class="input_covers">
<div class="profile_img_upload_blk text-center">
<input type="file" name="avatar" id="avatar" accept=".jpg, .jpeg, .png">
<span class="profile_img_upload_icon">
<i class="fa fa-upload"></i>
</span>
<h1>Browse Files</h1>
</div>
</span>

<div class="form-group" >
<div class="image-wrapper">
</div>  
<img src="" class="text-center hide" id="cropavatar" />
</div> 

<div class="form-group" >
<div class="col-md-12 col-sm-12 col-xs-12">

<div class="col-md-4 text-center">
<span class='covers'>
<div id="upload-demo" style="width:300px">
<div class="cr-viewport cr-vp-square" id='old' tabindex="0" style="width: 490px;
height: 329px;">
@if(\Session::get('model_id'))  
{!! SiteHelpers::user_profiles($image->id,'model',$image->profile_photo,'/uploads/model_profile/') !!}
@elseif(\Session::get('agent_id'))
{!! SiteHelpers::user_profiles($image->id,'agent',$image->profile_photo,'/uploads/agency_profile/') !!}
@endif
</div>
</div>
</span>
<div class="cover_image_close_btn close_btn"><span>&times;</span></div>
</div>
<div id="avatarFile"></div>                           
</div>
</div>
<div>
<div class="profile_pic_update text-center">
<button  class="upload-result" disabled='disabled'>Update</button>
</div> 
</div>
</div>    
</div>
</div>
</div>

<!-- Cover -->
<div id="myCover" class="modal fade  banner_popup_img my_profile_page-cover" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
<h4 class="modal-title">Profile Banner upload</h4>
</div>
<div class="modal-body">
<div class="alert-container"></div>
<div class="form-group" >
<div class="image-wrapper">
</div>  
<img src="" class="text-center hide" id="cropavatar" />
</div> 

<div class="form-group form-group_full" >
<div class="col-md-12 col-sm-12 col-xs-12 nopadding_div">

<div class="col-md-12 col-sm-12 col-xs-12 cover_upload_blk text-center nopadding_div">

<span class='input_covers'>
<div class="cover_upload_blk">
<input type="file" name="avatar_cover" accept=".jpg, .jpeg, .png" id="avatar-cover">
<span class="cover_upload_icon">
<i class="fa fa-upload"></i>
</span>
<h1>Browse Files</h1>
</div>
</span>

<span class='covers'>
<div id="upload-demo-cover">
<div class="cr-viewport cr-vp-square" id='old-cover' tabindex="0"  data-src="{{$image->profile_cover}}" style="width: 550px;height: 350px;">
@if(\Session::get('model_id'))
{!! SiteHelpers::user_avatars($image->id,'model',$image->profile_cover,'/uploads/model_profile/cover/') !!} 
<!-- <div class="cover_image_close_btn"><span>&times;</span></div> -->
@elseif(\Session::get('agent_id'))
{!! SiteHelpers::user_avatars($image->id,'agent',$image->profile_cover,'/uploads/agency_profile/cover/') !!}
<!-- <button type="button" class="close close_btn">&times;</button> -->
@endif
</div>
</div>
</span>

<div class="cover_image_close_btn close_btn"><span>&times;</span></div>

</div>
<div id="avatarFileCover">                            
</div>                           
</div>
</div>
<div class="cover_browse_btn">
<!-- <input type="file" name="avatar_cover" id="avatar-cover" > -->
<button  class="upload-result-cover" disabled='disabled' ><span>Update</span></button> 
<button  class="uploading" disabled='disabled' ><span>Uploading<img src='{{asset("magelur/images/loading.gif")}}' style='height:40px;width:40px;'></span></button> 
<button  class="uploaded" disabled='disabled' ><span>Uploaded Successfully</span></button> 
<!-- <span class='uploading'></span>
<span class='uploaded'></span> -->

</div>
</div>    
</div>
</div>
</div>

@endforeach

<script type="text/javascript">
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 340,
        height: 325,
        type: 'square'
    },
    boundary: {
        width: 400,
        height: 400
    }
});


$('#avatar').on('change', function () { 
  $('#old').hide();

var reader = new FileReader();
    reader.onload = function (e) {
    $uploadCrop.croppie('bind', {
    url: e.target.result
    }).then(function(){
    console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
$('.my_profile_page').modal('hide');
$uploadCrop.croppie('result', {
type: 'canvas',
size: 'viewport'
}).then(function (resp) {
$.ajax({
url: "{{ url('/model/save/profile') }}",
type: "POST",
data: {"image":resp},
success: function (data) {
html = '<img src="' + resp + '" />';
$("#upload-demo-i").html(html);
}
});
});
});

$('.editbtn_div').on('click', function () {
$('.cr-slider-wrap').hide();
$('.input_covers').show();
$('.covers').hide();
$('.close_btn').hide();
$('.upload-result').hide();
});

$('input:file').on("change", function() {
$('.cr-slider-wrap').show();
$('.upload-result').removeAttr('disabled');
$('.upload-result').show();
$('.close_btn').show();
});


</script>

<script type="text/javascript">
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$uploadCropCover = $('#upload-demo-cover').croppie({
    enableExif: true,
    viewport: {
        width: 550,
        height: 300,
    },
    boundary: {
        width: 550,
        height: 350
    }
});

$('#avatar-cover').on('change', function () { 
    $('#old-cover').hide();
    var reader = new FileReader();
    reader.onload = function (e) {
    $uploadCropCover.croppie('bind', {
    url: e.target.result
    }).then(function(){
    console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result-cover').on('click', function (ev) {
// $('.my_profile_page-cover').modal('hide');

$uploadCropCover.croppie('result', {
type: 'canvas',
size: { width: 767},
quality: 0.8,

}).then(function (resp) {
$.ajax({
url: "{{ url('/model/save/Cover') }}",
type: "POST",
data: {"images":resp},
beforeSend: function() {
$('.upload-result-cover').hide();
$('.uploading').show();
},
success: function (data) {
html = '<img src="' + resp + '" />';
$("#upload-demo-i-cover").html(html);
$('.upload-result-cover').hide();
$('.uploading').hide();
$('.uploaded').show();
$('.cr-slider-wrap').hide();
 // window.location.reload();return(true)
$('#myCover').modal('hide');

}

});

});
});

$('.editbtn_div_cover').on('click', function () {
$('.uploading').hide();
$('.uploaded').hide();
var image = $('#old-cover').attr("data-src");

        $('.input_covers').show();
        $('.covers').hide();
        $('.close_btn').hide();
        $('.upload-result-cover').hide();

$('.cr-slider-wrap').hide();
});

$('input:file').on("change", function() {
$('.input_covers').hide();
$('.covers').show();
$('.close_btn').show();
$('.cr-slider-wrap').show();
$('.upload-result-cover').removeAttr('disabled');
$('.upload-result-cover').show();
});
$(document).on('click', '.close_btn', function () {
$('input[name=avatar_cover]').val(null);
$('input[name=avatar]').val(null);

$('.covers').hide();
$('.close_btn').hide();
$('.input_covers').show(); 
$('.upload-result-cover').hide();
$('.upload-result').hide();

$('.uploaded').hide();

});
$(document).on('click', '.close', function () {
$('input[name=avatar_cover]').val(null);
$('input[name=avatar]').val(null);


});

</script>