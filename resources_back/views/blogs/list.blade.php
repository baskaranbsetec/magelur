<?php
use App\Library\Markdown;
?>
<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->

<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     
<div class="banner_title wow bounce">
            <h1>{{ Lang::get('core.blog') }}</h1>
              <ul class="link_list">
                <li class="active"><a href="{{asset('/')}}">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">Blog</a></li>
              </ul>
          </div>
 
    </div>
   </div>
  </section>
<div class="blog_post_page">
<div class="container m-t">	

	<div class="row" style="padding-top:25px;">
		<div class="col-md-9 m-t left_abt clearfix blog_left_side">
		<div class="posts_blog">

			@if(Session::has('messagetext'))	  
			{!! Session::get('messagetext') !!}
			@endif	

			@foreach ($rowData as $row)

			<div class="posts">
				<div class="heading_date">
					<h1> {{ date(" j M. " , strtotime($row->created)) }} </h1> 
      			</div>
      			<div class="blog_right_side">
				<div class="title">
					<a href="{{ url('blogs/view/'.$row->id.'/'.$row->alias)}}" ><h4> {{ $row->title }} </h4></a>
					<h5>{{ Lang::get('core.category') }}: 
						{{--*/ $idz=\SiteHelpers::getcolumnvalue('category',$row->categories) /*--}}
						@foreach($idz as $id)
						@if($row->categories) {{$id->name}}, @endif
						@endforeach
					<?php 
				foreach(explode(',',$row->labels) as $val)
				{
					echo '<span href="'.url('blogs/label/'.trim($val)).'"> '.trim($val).' </span> ';
				}
				?> |  {{ Lang::get('core.author') }}: {{ $row->author }}</h5>
				</div>
				<div class="info" style="display:none;">
					<i class="fa fa-user"></i>  <span> {{ ucwords($row->username) }}  </span>   
					<i class="fa fa-calendar"></i>  <span> {{ date("M j, Y " , strtotime($row->created)) }} </span> 
					<i class="fa fa-comment-o "></i>   <span> <a href="{{ url('blogs/view/'.$row->id.'/'.$row->alias)}}#comments" > {{ $row->comments }}  {{ Lang::get('core.comment(s)') }} </a> </span> 
				</div>
				<div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
    				<a href="{{ url('blogs/view/'.$row->id.'/'.$row->alias)}}" class="">   
      				<!-- <img src="../images/blog_img1.png" class="img-responsive" /> -->
      				<img src="{{ asset('uploads/blogs/'.$row->image) }}" class="">
    				</a>
      			</div>
				<div class="note">
				<p>
				<?php

				$content = explode('<hr>', $row->note);
				if(count($content>=1))
				{
					echo PostHelpers::formatContent($content[0]);
				} else {
					//echo PostHelpers::formatContent($row->note);
				}
				?>
				</p>

					 
				</div>
				<div class="blog_button_blk">

				@if( !empty($row->labels))
				{{--*/ $aLabel = explode(',', $row->labels); /*--}}
				
				<div class="blog_button_div1">   
				@foreach($aLabel as $sLabel)      
					<button onclick="window.location='{{ url('blogs/tags/'.trim($sLabel)) }}'">{{ $sLabel }}</button>
				@endforeach					
				</div>
				
				@else
				<div class="no-tags blog_button_div1">         
					<button>{{ Lang::get('core.no_tags') }}</button>
				</div>				
				@endif

      			<div class="blog_button_div2">
				<div class="blog_twitter">
      			<div class="blog_twitter_left">
      				<!--<a href="https://www.facebook.com/sharer/sharer.php?u={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}&t={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}"
      					onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
      					target="_blank" title="Share on Facebook"><img src="{{ asset('images/blog_fb_img.png') }}">{{ Lang::get('core.share_fb') }}
      				</a>
      			-->
      				<a href="https://www.facebook.com/sharer/sharer.php?u={{ url('blogs/list')}}"
      					onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
      					target="_blank" title="Share on Facebook"><img src="{{ asset('images/blog_fb_img.png') }}">{{ Lang::get('core.share_fb') }}
      				</a>
      			</div>
      			<!-- <div class="blog_twitter_right">1</div> -->
      			</div> 
      			<div class="blog_twitter blog_twitter_blk">
      			<div class="blog_twitter_left">
      			<!-- <img src="../images/blog_twitter_img.png">Tweet -->
      			<a href="https://twitter.com/share?url={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}"
      				onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
      				target="_blank" title="Share on Twitter"><img src="{{ asset('images/blog_twitter_img.png') }}"> {{ Lang::get('core.tweet_fb') }}
      			</a>
      			</div>
      			<!-- <div class="blog_twitter_right">1</div> -->
      			</div>
      			</div>
      			<div class="blog_button_div3">
      			<div class="view_all_btn">
     			 <a href="{{ url('blogs/view/'.$row->id.'/'.$row->alias)}}"><button>{{ Lang::get('core.read_article') }}...</button></a>
      			</div>
      			</div>
      			</div>

				<div class="labels" style="display:none"><br />
				<b>{{ Lang::get('core.articles_in') }} :  </b><br />
				<?php 
				foreach(explode(',',$row->labels) as $val)
				{
					echo '<a href="'.url('blogs/label/'.trim($val)).'" class="btn btn-xs btn-default"> '.trim($val).' </a> ';
				}
				?>
				</div>	

				<!-- <a href="{{ url('post/view/'.$row->id.'/'.$row->alias)}}" class="btn btn-default btn-sm pull-right">Read More <i class="fa fa-arrow-right"></i>  </a>  -->				
			 <hr />	
			 </div>
			 </div>         			 
			
            @endforeach
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  

<!--  <div class="pagination_content"> 
 <h1>page 1 of  3
</h1>
 <ul class="pagination"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="http://192.168.1.65:81/magelur/public/bloges?page=2">2</a></li> <li><a href="http://192.168.1.65:81/magelur/public/bloges?page=2" rel="next">»</a></li></ul>

   <ul class="pagination last_page_blk">
   <li class="disabled"><span>«</span></li> 
   <li class="active"><span>1</span></li>
   <li><a href="model?page=2">2</a></li>
   <li><a href="model?page=3">3</a></li>
   <li><a href="model?page=4">4</a></li>
   <li><a href="model?page=5">5</a></li> 
   <li><a href="model?page=2" rel="next">»</a></li>
   <li class="yellow_bg_clr"><a href="http://192.168.1.65:81/magelur/public/blogs?page=2
" rel="next">last</a></li>
   </ul>

 </div>
  -->
 </div>





		</div>

		<div class="col-md-3">
			@include('blogs.widget')
		</div>

	</div>
</div>

<div class="pagination_content text-center"> {!! $pagination->render() !!}</div>



 <!--   <div class="load_more_btn_div col-md-12 col-sm-12 col-xs-12">
 <button><span>Load More</span></button>
 </div> -->
</div>
</div>


<style>
.map {
    display: none;
}

.footer_absolute .magelur_about.faq_sec .magelur_header {
    padding-bottom: 0px;
    height: auto;
    width: 100%;
    display: inline-block;
}

</style>