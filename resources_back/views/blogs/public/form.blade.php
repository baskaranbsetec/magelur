

{!! Form::open(array('url'=>'blogs/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

@if(Session::has('messagetext'))

{!! Session::get('messagetext') !!}

@endif
<ul class="parsley-error-list">
	@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</ul>		


<div class="col-md-12">
	<fieldset><legend> Blog management</legend>


		<div class="form-group  " >
			<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
			<div class="col-md-7">
				<input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 		
				
		<div class="form-group  " >
			<label for="Note" class=" control-label col-md-4 text-left"> Note </label>
			<div class="col-md-7">
				<textarea name='note' rows='5' id='note' class='form-control '  
				>{{ $row['note'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Created" class=" control-label col-md-4 text-left"> Created </label>
			<div class="col-md-7">

				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created', $row['created'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>

			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Updated" class=" control-label col-md-4 text-left"> Updated </label>
			<div class="col-md-7">

				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updated', $row['updated'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>

			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Filename" class=" control-label col-md-4 text-left"> Filename </label>
			<div class="col-md-7">
				<input  type='text' name='filename' id='filename' value='{{ $row['filename'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
			<div class="col-md-7">
				<input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Access" class=" control-label col-md-4 text-left"> Access </label>
			<div class="col-md-7">
				<textarea name='access' rows='5' id='access' class='form-control '  
				>{{ $row['access'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Allow Guest" class=" control-label col-md-4 text-left"> Allow Guest </label>
			<div class="col-md-7">
				<input  type='text' name='allow_guest' id='allow_guest' value='{{ $row['allow_guest'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Template" class=" control-label col-md-4 text-left"> Template </label>
			<div class="col-md-7">
				<input  type='text' name='template' id='template' value='{{ $row['template'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Metakey" class=" control-label col-md-4 text-left"> Metakey </label>
			<div class="col-md-7">
				<input  type='text' name='metakey' id='metakey' value='{{ $row['metakey'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Metadesc" class=" control-label col-md-4 text-left"> Metadesc </label>
			<div class="col-md-7">
				<textarea name='metadesc' rows='5' id='metadesc' class='form-control '  
				>{{ $row['metadesc'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Default" class=" control-label col-md-4 text-left"> Default </label>
			<div class="col-md-7">
				<input  type='text' name='default' id='default' value='{{ $row['default'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Pagetype" class=" control-label col-md-4 text-left"> Pagetype </label>
			<div class="col-md-7">
				<input  type='text' name='pagetype' id='pagetype' value='{{ $row['pagetype'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Labels" class=" control-label col-md-4 text-left"> Labels </label>
			<div class="col-md-7">
				<textarea name='labels' rows='5' id='labels' class='form-control '  
				>{{ $row['labels'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Views" class=" control-label col-md-4 text-left"> Views </label>
			<div class="col-md-7">
				<input  type='text' name='views' id='views' value='{{ $row['views'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Userid" class=" control-label col-md-4 text-left"> Userid </label>
			<div class="col-md-7">
				<input  type='text' name='userid' id='userid' value='{{ $row['userid'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 					
		<div class="form-group  " >
			<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
			<div class="col-md-7">
				<input  type='file' name='image' id='image' @if($row['image'] =='') class='required' @endif style='width:150px !important;'  />
				<div >
					{!! SiteHelpers::showUploadedFile($row['image'],'/uploads/blogs/') !!}

				</div>					

			</div> 
			<div class="col-md-1">

			</div>
		</div> </fieldset>
	</div>



	<div style="clear:both"></div>	


	<div class="form-group">
		<label class="col-sm-4 text-right">&nbsp;</label>
		<div class="col-sm-8">	
			<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
			<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
		</div>	  

	</div> 

	{!! Form::close() !!}

	<script type="text/javascript">
		$(document).ready(function() { 



			$('.removeCurrentFiles').on('click',function(){
				var removeUrl = $(this).attr('href');
				$.get(removeUrl,function(response){});
				$(this).parent('div').empty();	
				return false;
			});		

		});
	</script>		 
