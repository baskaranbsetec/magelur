<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="{{ url('post')}}"> Posts </a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->
 <script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
  <link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>
  <script src="{{ asset('magelur/js/star_rating.js')}}"></script>
<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     
<div class="banner_title wow bounce">
            <h1>{{ Lang::get('core.blog') }}</h1>
              <ul class="link_list">
                <li class="active"><a href="{{asset('/')}}">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">Blog</a></li>
              </ul>
          </div>
    </div>
   </div>
  </section>
<div class="blog_post_page">
<div class="container m-t">
	<div class="row"  style="padding:25px 0;">
		<div class="col-md-9 left_abt clearfix blog_left_side">
		<div class="posts">
		<div class="heading_date">
			<h1>  {{ date("M j " , strtotime($row->created)) }}  </h1> 
      	</div>
      	<div class="blog_right_side blog_details_right_side">
      	<h4>{{ $row->title }}</h4>
      	<h5>{{ Lang::get('core.category') }}: {{--*/ $idz=\SiteHelpers::getcolumnvalue('category',$row->categories) /*--}}
@foreach($idz as $id)
@if($row->categories) {{$id->name}}, @endif
@endforeach  |  {{ Lang::get('core.author') }}: {{ $author }}</h5>
<div class="rating_star_div">
<div class="post-action">
      <!-- Rating -->
      
      <select class='ratings' id='rating_'  data-id='rating_'>
        <option value="1" >1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>
        <option value="4" >4</option>
        <option value="5" >5</option>
      </select>
      <h2>( {{$avg_ratings}} )</h2>

    </div>
    </div>
    <script type="text/javascript">
    // set rating for each star
    setRating($('.ratings'),'{{$avg_ratings}}');
    </script>
      	<div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
      	<a href="javascript:void(0);">
      		<img src="{{ asset('uploads/blogs/'.$row->image) }}" class="img-responsive">
      	</a>
      	</div>

      	<div class="desc">
      		<p>{{ $row->note }}</p>
      	</div>

      	<hr class="yellow_line">
      	<div class="blog_button_blk">
      		<!-- tags section -->
      		@if( !empty($row->labels))
      		{{--*/ $aLabel = explode(',', $row->labels); /*--}}
      		
      		<div class="blog_button_div1">  
      		@foreach($aLabel as $sLabel)       
      			<button onclick="window.location='{{ url('blogs/tags/'.trim($sLabel)) }}'">{{ $sLabel }}</button>
      		@endforeach
      		</div>
      		
      		@else
      		<div class="no-tags blog_button_div1">         
      			<button>{{ Lang::get('core.no_tags') }}</button>
      		</div>				
      		@endif

      <div class="blog_button_div2 blog_details_button_div2">
      <div class="blog_twitter">
      <div class="blog_twitter_left">
      	<a href="https://www.facebook.com/sharer/sharer.php?u={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}&t={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}"
      		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
      		target="_blank" title="Share on Facebook">
     	<img src="{{ asset('images/blog_fb_img.png') }}">{{ Lang::get('core.share_fb') }}
        </a>

  	  </div>
      <!-- <div class="blog_twitter_right">1</div> -->
      </div>
      <div class="blog_twitter blog_twitter_blk">
      <div class="blog_twitter_left">
      	<a href="https://twitter.com/share?url={{ url('blogs/view/'.$row->id.'/'.$row->alias)}}"
      		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
      		target="_blank" title="Share on Twitter">
      		<img src="{{ asset('images/blog_twitter_img.png') }}">{{ Lang::get('core.tweet_fb') }}
      	</a>	
  	  </div>
      <!-- <div class="blog_twitter_right">1</div> -->
      </div>
      </div>
      </div>      
      </div>
      <div class="blog_comments_blk">


		@if($conpost['commsys'] ==1)
		<div class="comments">
<!-- 		<h3> Comment(s) </h3> -->

		@foreach($comments as $comm)
		@if(!empty($comm->username))
		<div class="comment_blk1">
			<h1>{{ ucwords($comm->username) }}  
 		
			@if(!empty($comm->city) && !empty($comm->country))		
			|  {{ $comm->city }}  ,  {!! SiteHelpers::getCountry($comm->country) !!} 
			@endif

		</h1>
    <div class="rating_star_div">
    <div class="post-action">
		<select class="not_rated" data-rate="{{$comm->rating}}" id="comment_id_{{ $comm->commentID }}">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
      </div>

        <script type="text/javascript">
        setRating($('#comment_id_{{ $comm->commentID }}'),'{{$comm->rating}}');
        $(function() {
          $('.not_rated').barrating({
            theme: 'fontawesome-stars',
          });
        });
        </script>
<!--       <div class="rating_blk">
      <ul class="starer">
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
        </ul>
    </div> -->
    <div class="content" style="display:block">
    	<p>{!! PostHelpers::formatContent($comm->comments) !!}</p>
    	<div class="tools" style="display:none">
    		@if(Session::get('gid') == '1' OR $comm->userID == Session::get('uid')) 
    		<a href="{{ url('blogs/remove/'.$row->id.'/'. $row->alias.'/'.$comm->commentID) }}"><i class="fa fa-minus-circle"></i>{{ Lang::get('core.btn_remove') }}   </a>
    		@endif
    	</div>
    </div>
</div>
@endif


		<div class="info" style="display:none;">
		@if(!empty($comm->avatar))	
		<div class="avatar" >
		<?php if( file_exists( './uploads/users/'.$comm->avatar) && $comm->avatar !='') { ?>
			<img src="{{ URL::to('uploads/users').'/'.$comm->avatar }} " border="0" width="40" class="img-circle" />
		<?php  } else { ?> 
			<img alt="" src="http://www.gravatar.com/avatar/{{ md5($comm->email) }}" width="40" class="img-circle" />
		<?php } ?> 
		</div>
		@endif
			 
			<!--  {{ date("M j, Y " , strtotime($comm->posted)) }} -->
			
		</div>
		

		@endforeach
		<div class="comment_text_area">
		{{--*/ $bLogged=\SiteHelpers::checkLogged() /*--}}	

		@if(!empty($bLogged))
		<form method="post"  id='SubmitComments' action="{{ url('blogs/comment') }}" parsley-validate  class="form">
		<div class="note">
			@if(Session::has('messagetext'))	  
				{!! Session::get('messagetext') !!}
			@endif	
		<!-- {!! PostHelpers::formatContent($row->note) !!} -->
		</div>
    <div class="rating_star_div">
    <div class="post-action">
		<select id="comment_rating" >
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
      </div>
      </div>
      <input type="hidden" name="rating" value="1" /> 
		<textarea minlength="5" id='commentBox' rows="5" placeholder="{{ Lang::get('core.comment_place') }}" required name="comments"></textarea>
		<div class="comment_submit_btn">		
		<button type="submit"><span>{{ Lang::get('core.submit_comment') }} </span></button>	
		</div>
		<input type="hidden" name="pageID" value="{{ $row->id }}" />	
		<input type="hidden" name="alias" value="{{ $row->alias }}" />						
		</form>
		@else
		<!-- show login button -->
		<div class="header_menu_list">
		<h3>You need to be member to post comments</h3>
			<p>To add a comments for above post you need to make a login.If you are a existing user login into the site and add your comments.If you didn't had an account please click on the below signup button to create your account   </p>
			<div class="signupsagent signupsagent_blog">
				<a href="{{ url('model/create') }}">{{ Lang::get('core.am_model') }}</a>
				<a href="{{ url('agent/create') }}">{{ Lang::get('core.am_agent') }}</a>
			</div>
		</div>

		@endif

		</div>

		</div>
		@endif
      </div>

				
				<div class="info" style="display:none">
					<i class="fa fa-eye "></i>  <span>  Views (<b> {{ $row->views }} </b>)  </span>   
					<i class="fa fa-user "	></i>  <span>  {{ ucwords($row->username) }}  </span>   
					<i class="fa fa-calendar "></i>  <span> {{ date("M j, Y " , strtotime($row->created)) }} </span> 
					<i class="fa fa-comment-o "></i>   <span>  {{ $row->comments }}  {{ Lang::get('core.comment(s)') }}  </span> 
				</div>
				@if($conpost['commshare'] ==1 AND $conpost['commshareapi'] !='')
				<span class='st_sharethis_large' displayText='ShareThis'></span>
				<span class='st_facebook_large' displayText='Facebook'></span>
				<span class='st_twitter_large' displayText='Tweet'></span>
				<span class='st_googleplus_large' displayText='Google +'></span>
				<span class='st_linkedin_large' displayText='LinkedIn'></span>
				<span class='st_email_large' displayText='Email'></span>
				@endif				


				@if($conpost['commimage'] ==1 )
				<div class="image">
				<img src="{{ asset('uploads/images/'.$row->image)}}" class="img-responsive" />
				</div>	
				@endif

				<div class="labels" style="display:none"><br />
				<b>{{ Lang::get('core.articles_in') }}  :  </b><br /><br />
				{!! $labels !!}
				
				<a href="{{ url('post')}}" class="btn btn-default btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back </a>

				</div>	
			</div>   
		</div>

		<div class="col-md-3">
			@include('blogs.widget',array("conpost"=>$conpost))
		</div>

	</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.map').hide();

});
$(function() {
  $('#comment_rating ').barrating({
    theme: 'fontawesome-stars',
    onSelect: function(value, text, event) {
      var rate = $( "#comment_rating option:selected" ).val();
      $('input[name="rating"]').val(rate);

    }
  });
  $('.ratings').barrating({
    theme: 'fontawesome-stars',
  });
  $('form#SubmitComments div.br-widget').hide();
  $("#commentBox").click(function() {
  	$('form#SubmitComments div.br-widget').show();

  });
});
 $("#SubmitComments").validate({
      ignore: "not:hidden",
      rules: {
        comments:{
          required:true,
          minlength:5
        },
       
      },
      messages: {
        comments:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Your Comments",
          minlength:"<span><i class='fa fa-warning'></i></span>Please enter at least 5 characters",
        },
        
       
      },
       
      submitHandler:function(form){

        form.submit();
      }
    }); 
</script>

<style type="text/css">
.footer_absolute .magelur_about.faq_sec .magelur_header {
    padding-bottom: 0px;
    height: auto;
    width: 100%;
    display: inline-block;
}
</style>