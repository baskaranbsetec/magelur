
@include('agent_dash_header')

{!! Form::open(array('url'=>'messagelabeldelete', 'class'=>'form-horizontal ajaxLabelDelete','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
	{!! Form::hidden('label_id', '' )  !!}
{!! Form::close() !!}
<input type="hidden" name="urlmodule" id="urlmodule" value="http://192.168.1.65:81/magelur/public/message">
	

<div class="aboutmodel">
 <div class="container">
  <div class="message_two_sec clearfix">
   
   <div class="col-sm-3">
   	<div class="message_left">
		<div class="">
		<div class="sbox-title sendReplyForward" type="button" data-btn="forward" style="height:65px" ><a href="javascript:void(0);" id="composeMessage" class="btn btn-block btn-lg btn-primary">Compose message</a></div>

		</div>
		<ul class=" msaged">
		<li class="@if($type == 0) active @endif"><a href="{{ URL::to('message?ty=0') }}">
		Inbox
		<span  data-inbox="{!! $totalType['0'] !!}" id="cinbox">{!! $totalType['0'] !!}</span>
		</a></li>
		<li class="@if($type == 1) active @endif"><a href="{{ URL::to('message?ty=1') }}">
		Important
		<span  data-star="{!! $totalType['1'] !!}" id="cstar">{!! $totalType['1'] !!}</span>
		</a></li>
		<li class=" @if($type == 2) active @endif"><a href="{{ URL::to('message?ty=2') }}">
		Send item
		<span  data-sent="{!! $totalType['2'] !!}" id="csent">{!! $totalType['2'] !!}</span>
		</a></li>
		<li class=" @if($type == 3) active @endif"><a href="{{ URL::to('message?ty=3') }}">
		Draft
		<span  data-draft="{!! $totalType['3'] !!}" id="cdraft">{!! $totalType['3'] !!}</span>
		</a></li>
		<li class=" @if($type == 4) active @endif"><a href="{{ URL::to('message?ty=4') }}">
		Trash
		<span  data-trash="{!! $totalType['4'] !!}" id="ctrash">{!! $totalType['4'] !!}</span>
		</a></li>
		</ul>
				
   	</div>	
   </div>

   <div class="col-sm-9">
   	<div class="message_box clearfix">
   	 <div class="left_sht_msg">
   	 	<div class="letmsg"><input type="text" id='search' name="search" placeholder="Search"></div>
   	 	<div class="fullmsg_blg message_block_leftside">
					
			<table class="table table-hover" id='userTbl'>
				<tbody class="dataMessage">
				@foreach($rowData as $row)

					<input type="hidden" name="entryby" class="entryby" value="{{ $row->entry_by }}" />
					{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
					@if($row->read == 0 && ($type == 0 || $type == 1)) <tr class="mail-unread" id="msg_{!! $row->id !!}"> @else <tr class="mail" id="msg_{!! $row->id !!}"> @endif
					<td data-type="{!! $type !!}" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}" width="10" style="padding-bottom:10px; padding-right:0px;"></td>
					{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
					{{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}

					<td class="td-clickable text-success">

					<div class="usertext">
					<img src="{{ asset('uploads/model_profile/'.$rowUserAvatar) }}" border="0" width="25" height="25" class="img-circle">
					<a >{!! $rowUser !!}</a>	
					<p>{!! substr(\SiteHelpers::getName('message_subject',$row->subject) , 0, 30) !!}...</p>
					</div>

					<div class="more_dots">
					<a href="javascript:void(0)"></a>
					<p>{{--*/$zone=date("Y-m-d h:i:s a", strtotime($row->createdOn));/*--}}
					{!! \SiteHelpers::time_ago($zone) !!}</p>
					</div>
					</td>
					{{--*/ if($isSender == 0 && isset($labels[$row->label_by_recipient])) { $rowLabel = $labels[$row->label_by_recipient]; $rowLabelId = $row->label_by_recipient; } else if($isSender == 1 && isset($labels[$row->label_by_sender])) { $rowLabel = $labels[$row->label_by_sender]; $rowLabelId = $row->label_by_sender; } else { $rowLabel = ''; $rowLabelId = ''; } /*--}}
					<!-- <td class="td-clickable text-"><label class="badge badge-inverse labelDisp" data-id="{!! $rowLabelId !!}" id="label_{!! $row->id !!}">{!! $rowLabel !!}</label> {!! substr($row->subject, 0, 30) !!}</td> -->
					<!-- <td class="td-clickable text-light">{!! substr(strip_tags($row->message), 0, 60) !!}</td> -->
					<!-- <td class="td-clickable text-light">{!! date("m/d/y h:i A", strtotime($row->createdOn)) !!}</td> -->
					<td>
					{!! Form::open(array('url'=>'messageupdate', 'class'=>'form-horizontal ajaxMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
					{{--*/ if($type == 0) $rowStarred = $row->starred_by_recipient; else if($type == 2) $rowStarred = $row->starred_by_sender; else $rowStarred = 0; /*--}}
					{!! Form::hidden('starred', $rowStarred )  !!}
					{!! Form::hidden('update_function', '' )  !!}
					{!! Form::hidden('msg_type', $type )  !!}
					{!! Form::hidden('msg_id', $row->id )  !!}
					@if($type == 0 || $type == 2)
					<i class="fa fa-star msgstar @if($rowStarred == 1) active @endif"></i>
					@endif
					{!! Form::close() !!}
					</td>
					</tr>

				@endforeach
				</tbody>
			</table>
					{!! Form::open(array('url'=>'messagegroupupdate', 'class'=>'form-horizontal ajaxGroupMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('msg_ids', '' )  !!}
						{!! Form::hidden('is_sender', '' )  !!}
						{!! Form::hidden('label_id', '' )  !!}
						{!! Form::hidden('label_name', '' )  !!}
						{!! Form::hidden('group_update_function', '' )  !!}
						{!! Form::hidden('type', $type )!!}
					{!! Form::close() !!}
   	 	
		</div> 
   	 </div>
@foreach($rowData as $row)
		{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
		<div class="hidden viewMessageDiv" id="viewMessageDiv_{!! $row->id !!}">

		<div class="msg_right_box ">
		<div class="close_btn_div">
		<button class="btn  gotodataMessageDiv" type="">
		<i class="fa fa-close"></i>
		</button>
		</div>

		<div class="total_newer clearfix">

		<div class="renew ">
		
<input type="hidden" class="senderid" value="{!! $row->entry_by !!}">
				<input type="hidden" class="recipientid" value="{!! $row->recipient !!}">
				<input type="hidden" class="msg_id" value="{!! $row->id !!}">
				<input type="hidden" class="subj_id" value="{!! $row->subject !!}">

				<h2 class="viewMessageTitle pull-left" hidden>{!! $row->subject !!}</h2>
<div id='start'>
<input type="hidden" name="viewMessageimage_{!! $row->id !!}" class="viewMessageimage viewMessageimage_{!! $row->id !!} pull-left" value='{!! $row->attachments !!}'></div>

		<h3  >Subject:{!! \SiteHelpers::getName('message_subject',$row->subject) !!}</h3>
		{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
		{{--*/ if($isSender == 1 && isset($usersEmailList[$row->recipient])) { $rowUserEmail = $usersEmailList[$row->recipient]; } else if($isSender == 0 && isset($usersEmailList[$row->entry_by])) { $rowUserEmail = $usersEmailList[$row->entry_by]; } else { $rowUserEmail = ''; } /*--}}
		{{--*/ if($isSender == 1 && isset($usersIdList[$row->recipient])) { $rowUserId = $usersIdList[$row->recipient]; } else if($isSender == 0 && isset($usersIdList[$row->entry_by])) { $rowUserId = $usersIdList[$row->entry_by]; } else { $rowUserId = ''; } /*--}}
		{{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}
		<p><span class="for_span">from</span> <span class="ed_span">{!! $rowUser !!}</span></p>
<!-- 		<p><span class="for_span">to</span> <span class="ed_span">You</span></p>
 -->	
 <h2 class="viewMessageTitles pull-left" hidden>{!! $row->entry_by !!}</h2>

		</div>
		<div class="minew">

		{{--*/$monthNum=date("m")/*--}}
		<p>
		@if(date('d')==date("d", strtotime($row->createdOn)))
		Today 
		@elseif((date('d')-1)==date("d", strtotime($row->createdOn)))
		Yesterday 
		@else
		{!! date('F', mktime(0, 0, 0, $monthNum, 10))!!}
		@endif
		{!! date(" h:i A", strtotime($row->createdOn)) !!}	  </p>

		<div class="model_icoded">
		<ul class="stuningeder">
		<!--  <li class="shield"><a href="javascript:void(0)"></a></li>
		<li class="warn"><a href="javascript:void(0)"></a></li> -->
		
		@if($type != 4 && $type != 3)	
		<li class="right_multi_left sendReplyForward" type="button" title='Reply' data-btn="reply" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
		</a></li>
		<li class="delets singleMessageDelete" type="button" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}"><a href="javascript:void(0)">

		</a></li>
		<!-- <li class="right_multi_middle"><a href="javascript:void(0)"></a></li> -->
		<li class="right_multi_right sendReplyForward" type="button" title='Forward' data-btn="forward" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
		</a></li>
		</ul>	
		</div>
		@endif
		</div>
		</div>
		<div class="newtent_content viewMessageContent">
		<p>{!! $row->message !!}</p>

		</div>
		<div class="rader clearfix rader_div rader_msg_div">
		<div class="minyues">
		{{--*/ $images = explode(',', $row->attachments); /*--}}
		@foreach($images as $info) 
		@if($row->attachments !='')
		<span>
		<a  rel='gallery2' href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
		<img src="{{ URL::to('uploads/message/') }}/{{ $info }}" border="0" width="30" class="img-circle" style="margin-right:2px;"></a> 
		</span>
		@else
		<span style='display:none'>
		<a  href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
		<img src="{{ URL::to('uploads/message/') }}/{{ $info }}" border="0" width="30" height="30" class="img-circle" style="margin-right:2px;"></a> 
		</span>
		@endif
		@endforeach
		</div>

		</div>
		
		</div>
		</div>
	
   	
	@endforeach
	</div>  	 	
   	</div>	
   </div>

  </div>	
 </div>	
</div>	
</div>
</div>
</div>
</div>
</div>
</div>


<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="max-width:800px;width:90%;">
	  <div class="modal-content">
		<div class="modal-header bg-default">
			
			<button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true" onclick="window.location.reload();return(true)">&times;</button>
			<h4 class="modal-titles">Compose message</h4>
		</div>
		<div class="load_notify"></div>
		<div class="modal-body" id="compose-modal-content">
			 {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<input type="hidden" name="typee" class="type" />
				<div class="form-group">
					<label for="mail_to" class="col-xs-2">Send To</label>
					<div class="col-xs-10">
						<div>{!! Form::select('recipient', $users, null, array('class'=>'form-control select2 width100p recipientselect2', 'style'=>'padding:0px;', 'required'=>'' ) ) !!}</div>
										<span class="err_recipient" style="color:red"></span>

					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">Subject</label>
					<div class="col-xs-10">
				{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control subject','id' => 'subject')) !!}
					<span class="err_subject" style="color:red"></span>

					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">Message</label>
					<div class="col-xs-10">
						<div><textarea name='message' rows='3' style="width:100%;" class='form-control mceEditor' placeholder="{{ Lang::get('core.message')}}"></textarea></div>
						<span class="error" style="color:red"></span>
					</div>
				</div>
				<div class="form-group">
				<label for="mail_subject" class="col-xs-2">Attach files</label>
				<div class="col-xs-10">
				<input  type="file" class="form-control" name="images[]" id='images' accept=".png,.jpg,.jpeg,.gif" placeholder="" multiple>
				<div id='theDiv'></div>
				<input type="hidden" name="duplicate" value='' class='duplicate'>
				<input type="hidden" name="remaining" value='' class='remaining'>
			    <input type="hidden" name="deleted" value='' class='deleted'>

				[Allow multiple image] 
				<span class="err_image" style="color:red"></span>

				</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2"></label>
					<div class="col-xs-10">
						{!! Form::hidden('draft', '0' )  !!}
						{!! Form::hidden('messageid', '' )  !!}
						<button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i> Compose Message</button>
						<button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
					</div>
				</div>
								<div class="sending_loadergs"></div>

			 {!! Form::close() !!}
		</div>

	  </div>
	</div>
</div>



<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header bg-default">
		
		<button type="button " class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">Modal title</h4>
	</div>
	<div class="modal-body" id="bsetec-modal-content">

	</div>

  </div>
</div>
</div>

		<!--  -->


</html>
<script>

$(document).ready(function(){
var count = $('.table-hover tbody tr.mail-unread').length;
       if(count>0){
         $("#count").html(count);
       }
	
	
	$(function(){
		tinymce.init({	
			mode : "specific_textareas",
			editor_selector : "mceEditor",
			theme : "advanced",
			theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
			// theme_advanced_toolbar_location : "top",
			// theme_advanced_toolbar_align : "left",
			// theme_advanced_statusbar_location : "bottom",
		 });	
	});
	
	$('.sendbtn').click(function(){
		$('.type').val(0);
	});
	$('.savedraft').click(function(){
		$('.type').val(3);
	});
	function tinyClean(value) {
	value = value.replace(/&nbsp;/ig, ' ');
	value = value.replace(/\s\s+/g, ' ');
	if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
		value = '';
	}
	return value;
}

   $(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       
      
        
      },
      messages: {
       
       
      },
      
        submitHandler: function(form) {

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
     var recipients = $('[name="recipient"]').val();
    var subject = $('[name="subject"]').val();
    var image = $('input[type="file"]').val();
    // alert(image);
    var img = $("#start").find('.viewMessageTitles').val();
// alert(img);
    // if(image==''){
    //   $('.err_image').html("Image Field is required");
    // }
    // else{
    //   $('.err_image').hide();

    // }
    if(recipients==''){
      $('.err_recipient').html("Recipient Field is required");
    }
    else{
      $('.err_recipient').hide();

    }
    if(subject==''){
      $('.err_subject').html("Subject Field is required");
    }
    else{
      $('.err_subject').hide();

    }
    if(value==''){
      $('.error').html("Message Field is required");
    }else{
      $('.error').hide();
    }
    
    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();

        toastr.success("Message sent Successfully");
         $('#compose-modal').modal("toggle"); 
         form.submit();
}
           
        },

        });
	// $('.sendMessage').submit(function(){

	// 	var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
	// 	var typee = $('[name="typee"]').val();
		
	// 	if(value==''){
	// 		$('.error').html("Message Field is required");
	// 	}else{
	// 		$('.error').html('');
	// 	}
		
	// 	if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !='' && value !=''){
	// 		$('.sendbtn').attr('disabled','disabled');
	// 		$('.savedraft').attr('disabled','disabled');
	// 		var recipient = $('[name="recipient"]').val();
	// 		var url = $(this).attr('action');
	// 		tinyMCE.triggerSave();
	// 		$.ajax({
	// 			type: 'POST',
	// 			url: url,
	// 			data: $(".sendMessage").serialize(),
	// 			beforeSend:function(){
	// 				$('.sendbtn').hide();
	// 				$('.savedraft').hide();
	// 				$('.sending_loadergs').append('<h2><i class="fa fa-refresh fa-spin"></i> Loading...</h2>');					
	// 			},
	// 			success : function(response) {
	// 				var data = jQuery.parseJSON( response );
	// 				if(data.status == '1') {
	// 					toastr.success("Message sent Successfully");
	// 				} else {
	// 					toastr.error("Label name already exist");
	// 				}
	// 				$('.sendbtn').removeAttr('disabled');
	// 				$('.savedraft').removeAttr('disabled');
	// 				$('#compose-modal').modal("toggle");					
	// 				$('.sendbtn').show();
	// 				$('.savedraft').show();
	// 				if(typee==0){
	// 					var sent =$('#csent').attr('data-sent');
	// 					var sentt =parseInt(sent)+parseInt(1);
	// 					$('#csent').html(sentt);
	// 				}else if(typee==3){
	// 					var draft =$('#cdraft').attr('data-draft');
	// 					var draftt =parseInt(draft)+parseInt(1);
	// 					$('#cdraft').html(draftt);
	// 				}
	// 			},complete: function() {
	// 			$('.sending_loadergs').html('');
	// 		}
	// 		});
	// 	}
	// 	return false;
	// });
	$('.ajaxMessageUpdate').submit(function(){
		var starred = $(this).children('[name="starred"]').val();
		if(starred == '1'){
			$(this).children('[name="starred"]').val('0');
		} else {
			$(this).children('[name="starred"]').val('1');
		}
		var url = $(this).attr('action');
			$(this).children('.msgstar').toggleClass('active');
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				var star =$('#cstar').attr('data-star');
				var starr;
				if(response==1){
					var s = parseInt(star) + parseInt(response);
					// alert(s)
					$('#cstar').attr('data-star', s);
					starr =parseInt(star)+parseInt(response);
										
				}else if(response==0){
					var s = parseInt(star) - parseInt(1);	
					 $('#cstar').attr('data-star', s);		 
					 starr =parseInt(star)-parseInt(1);	
				}
				 $('#cstar').html(starr);
				
			}
		});
		return false;
	});
	$('.msgstar').click(function(){
		$(this).parent('form').children('[name="update_function"]').val('starred');
		$(this).parent('form').submit();
	});
	
	$('.sendbtn').click(function(){
		$('[name="draft"]').val('0');
	});
	$('.savedraft').click(function(){
		$('[name="draft"]').val('1');
	});
	
	$('#composeMessage').click(function(){
		
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="messageid"]').val('');
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	$('.gotodataMessageDiv').click(function(){
		$('.viewMessageDiv').addClass('hidden');
		$('.dataMessageDiv').slideDown();
	});
	$('.sendReplyForward').click(function(){
		var msgid = $(this).attr('data-id');
		var msgtype = $(this).attr('data-type');
		var issender = $(this).attr('data-sender');
		var option = $(this).attr('data-btn');
		
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="messageid"]').val('');
			if(option == 'reply'){
			if(msgtype == 0) {
				
				$('#compose-modal').find('[name="recipient"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitles').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				
				} else if(msgtype == 2) {
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				} else if(msgtype == 1 || msgtype == 5) {
				if(issender == 1){
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
				} else {
				// $('#compose-modal').find('[name="recipient"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitles').text());
				$('#compose-modal').find('[name="recipient"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitles').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
				}
			}
			} else if(option == 'forward'){
				$('.drop').show();
				$('.drop1').hide();
				$('#compose-modal').find('[name="recipient"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitles').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
				var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
				var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
				var img = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
				var array =$('#viewMessageDiv_'+msgid).find('.viewMessageimage').val().split(",");
				$.each(array,function(i){
				var targetPath = "uploads/message/"+array[i]+"";
				if(img){
				$('#theDiv').prepend('<div class="image-delete" data-id='+recipientid+' data-msgid='+msg_id+' data-src='+array[i]+' data-path='+targetPath+' onClick=deleteImage(this,"' +targetPath+ '")><img border="0" width="50" class="fancyboxa img-circle" style="margin-right:2px;" src="{{ asset("uploads/message/") }}/'+array[i]+'"/>X</div>');
				$('input.duplicate').val($('#viewMessageDiv_'+msgid).find('.viewMessageimage').val());
				}
				else{
				$('#theDiv').hide();

				}
				});
			}
		
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});

$('tr').click(function(e){
e.preventDefault();
$('tr').removeClass('mail-click'); // removes all highlights from tr's
$(this).addClass('mail-click'); // adds the highlight to this row
});
	$('.dataMessage tr .td-clickable').click(function(){
		// $('table tr').addClass('mail-click');

		var msgid = $(this).parent('tr').find('td:nth-child(1)').attr('data-id');
		var msgtype = $(this).parent('tr').find('td:nth-child(1)').attr('data-type');

		if(msgtype == '3'){

			$('.sendMessage')[0].reset();
			// $('#compose-modal').find('[name="recipient"]').select2('val','');
			$('#compose-modal').find('[name="messageid"]').val('');

			var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
			var subjectid = $('#viewMessageDiv_'+msgid).find('.subj_id').val();
			var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
			// $('#compose-modal').find('[name="recipient"]').select2('val',recipientid);
			$('#compose-modal').find('[name="messageid"]').val(msg_id);
			tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
			$('.select2').val(recipientid)
			$('.subject').val(subjectid); 

			var img = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
			// alert(img[i]);
			var array =$('#viewMessageDiv_'+msgid).find('.viewMessageimage').val().split(",");
			$.each(array,function(i){
			// alert(array[i]);
			var targetPath = "uploads/message/"+array[i]+"";
			// alert(targetPath);
			if(img){
			$('#theDiv').prepend('<div class="image-delete" data-id='+recipientid+' data-msgid='+msg_id+' data-src='+array[i]+' data-path='+targetPath+' onClick=deleteImage(this,"' +targetPath+ '")><img border="0" width="50" class="fancyboxa img-circle" style="margin-right:2px;" src="{{ asset("uploads/message/") }}/'+array[i]+'"/>X</div>');
			$('input.duplicate').val($('#viewMessageDiv_'+msgid).find('.viewMessageimage').val());
			}
			else{
			$('#theDiv').hide();

			}
			});


			$('.sendbtn').removeAttr('disabled');
			$('.savedraft').removeAttr('disabled');
			$('#compose-modal').modal({
				backdrop: 'static',
				keyboard: false
			});


		} else {
			$('div.viewMessageDiv').addClass('hidden'); 
// $(this).addClass('hidden');
	/*		if($(this).parent('tr').hasClass('mail-unread')){
				$(this).parent('tr').find('form').children('[name="update_function"]').val('read');
				$(this).parent('tr').find('form').submit();
				$(this).parent('tr').attr('class','mail');
			}*/
			$('.dataMessageDiv').hide();
			$('#viewMessageDiv_'+msgid).removeClass('hidden');
		}
	
	});
	$('.ajaxGroupMessageUpdate').submit(function(){
		var url = $(this).attr('action');
		var ids = $(this).children('[name="msg_ids"]').val();
		var message_ids = ids.split(',');
		var typee=$('[name="type"]').val();

		if($(this).children('[name="group_update_function"]').val() == 'delete' || $(this).children('[name="group_update_function"]').val() == 'deleteTrash'){
			jQuery.each(message_ids, function(index, item) {
				$('#msg_'+item).remove();
			});
			$('.gotodataMessageDiv').trigger('click');
		} else if($(this).children('[name="group_update_function"]').val() == 'label'){
			var label_name = $(this).children('[name="label_name"]').val();
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text(label_name);
			});
		} else if($(this).children('[name="group_update_function"]').val() == 'removelabel'){
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text('');
			});
		}
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				$('.ajaxGroupMessageUpdate')[0].reset();
				if(response!='label')
				{
					var trash = $('#ctrash').attr('data-trash');
					var total =$('#page_total').attr('data-total');
					var res = parseInt(trash)+parseInt(response);
					//$('#page_total').html(res);
					$('#ctrash').html(res);
					if(typee==0){
						var inbox =$('#cinbox').attr('data-inbox');
						var inboxx = parseInt(inbox)-parseInt(response);					
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cinbox').attr('data-inbox',inboxx).text(inboxx);
					}else if(typee==1){
						var star =$('#cstar').attr('data-star');
						var starr =parseInt(star)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cstar').attr('data-star',starr).text(starr);
						
					}else if(typee==2){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#csent').attr('data-sent',sentt).text(sentt);
						
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cdraft').attr('data-draft',draftt).text(draftt);
					}else if(typee==4){
						trash = $('#ctrash').attr('data-trash');
						res = parseInt(trash)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#ctrash').attr('data-trash',res).text(res);
					}
				}
				
			}
		});
		return false;
	});
	$('#deleteMessage').click(function(){
		var msg_ids = '';
		var is_sender = '';

		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
					
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('#deleteFromTrash').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to remove selected messages permanently?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('deleteTrash');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('.singleMessageDelete').click(function(){

		var msg_ids = $(this).attr('data-id');
		var is_sender = $(this).attr('data-sender');
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
		


});	

// $('#search').on('keyup',function(){
// var value=$(this).val();
// console.log(value);
// $.ajax({
// type:'get',
// url: "{{URL::to('show')}}",
// data:{'search':value},
// success:function(data){
// $('tbody').html(data);
// }
// });
// })

$(document).ready(function(){
    $('#search').on('keyup',function(){
        var searchTerm = $(this).val().toLowerCase();
              // alert(searchTerm);

        $('#userTbl tbody tr').each(function(){
            var lineStr = $(this).text().toLowerCase();
            if(lineStr.indexOf(searchTerm) === -1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    });
});
    // cv upload process
  $(document).ready(function(){
      $('#cvupload').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(pdf)$/i,
        dataType:'json',
        maxFileSize: 1024000000, // 1 GB
        progress: function (e, data) {
                    $('.upload-delete').addClass('hide');
                    $('.job_fileUpload').addClass('hide');
                    $('.upload').removeClass('hide');
          $('#cv_name').text("");
                    $('#cv_date').html("");
                    $('.progress').removeClass('hide');
          var percentage = parseInt(data.loaded / data.total * 100);
          $('#probar').css('width',percentage+'%');
          if(percentage == '100') {
            $('#cv_name').text('{!! Lang::get("core.lecture_file_process")!!}');
            // $('.aboutpdf').text('uploaded');
          }
        },
        processfail: function (e, data) {
          file_name = data.files[data.index].name;
          alert("File not allowed");   
        },
        done: function(e, data){
          var return_data = data.result;
          if(return_data.status){
                        $('.progress').addClass('hide');
            $('#probar').css('width','0%');
            $(".uploadedfile").removeClass('hide');
            $('#cv_id').val(return_data.file_id);
            $('.aboutpdfs').text(return_data.file_title + '(' + return_data.file_size + ')');
            $('#cvdownload').attr('href',return_data.download_url);
              $('#cv_date').html('<span>On '+return_data.cv_date+'</span>');
            if(return_data.file_duration!='') 
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size +' - ' + return_data.file_duration + ' ) ');
            else
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size + ' ) ');

            $('.upload').addClass('hide');
            $('.upload-delete').removeClass('hide');
          }else{
            alert("invalid file.");
          }
        }
      });

   
  });

$(document).ready(function(){
      $('#pptupload').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(ppt)$/i,
        dataType:'json',
        maxFileSize: 1024000000, // 1 GB
        progress: function (e, data) {
                    $('.upload-delete').addClass('hide');
                    $('.job_fileUploads').addClass('hide');
                    $('.uploads').removeClass('hide');
          $('#cv_name').text("");
                    $('#cv_date').html("");
                    $('.pro').removeClass('hide');
          var percentage = parseInt(data.loaded / data.total * 100);
          $('#probars').css('width',percentage+'%');
          if(percentage == '100') {
            $('#cv_name').text('{!! Lang::get("core.lecture_file_process")!!}');
            // $('.aboutpdf').text('uploaded');
          }
        },
        processfail: function (e, data) {
          file_name = data.files[data.index].name;
          alert("File not allowed");   
        },
        done: function(e, data){
          var return_data = data.result;
          if(return_data.status){
                        $('.pro').addClass('hide');
            $('#probars').css('width','0%');
            $(".uploadedfile").removeClass('hide');
            $('#cv_id').val(return_data.file_id);
            $('.homeppt').text(return_data.file_title + '(' + return_data.file_size + ')');
            $('#cvdownload').attr('href',return_data.download_url);
              $('#cv_date').html('<span>On '+return_data.cv_date+'</span>');
            if(return_data.file_duration!='') 
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size +' - ' + return_data.file_duration + ' ) ');
            else
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size + ' ) ');

            $('.uploads').addClass('hide');
            $('.upload-delete').removeClass('hide');
          }else{
            alert("invalid file.");
          }
        }
      });

   
  });


		$(".fancybox").fancybox({
    afterLoad: function() {
        this.title = '<i class="fa fa-download" aria-hidden="true"><a href="' + this.href + '" download>Download</a></i>' + this.title;
    },
    helpers : {
        title: {
            type: 'inside'
        }
    }
});

	$('#model_select').autocomplete({		
		source: function(request, response) {
			$.ajax({
				method : 'GET',
				url: '{{ url('auto/model') }}',
				data:{name:request},					
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['username'],
						}
					}));
				}
			});
		},
		select: function(item) {	
			// alert(item);
			$('input[name="recipient"]').val(item['label']);	    
		}
	}); 



						
    $('.rader_div').slimScroll({
        height: '70px',
        width: '100%',
        allowPageScroll :true
        
    });      
   // $(document).ready(function () {
   //      $('.close').click(function () {
   //          window.opener.location.reload(); 
   //      });
   //  });

var removeCommaSeperatedValue = function(list, value, separator) {
  separator = separator || ",";
  var values = list.split(separator);
  for(var i = 0 ; i < values.length ; i++) {
    if(values[i] == value) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }
  return list;
}

function deleteImage(ele,path) {
	//  get imagename
	var imageName = $(ele).data("src");
	var id        = $(ele).data("id");
	var msgid     = $(ele).data("msgid");

	// console.log(ele)
	// get all messages
	var allImages = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
	// Removed images

	var updateImages = removeCommaSeperatedValue(allImages,imageName);
	 // console.log(imageName+',')
	$('input.viewMessageimage_'+msgid).val(updateImages);
	//console.log($('input.viewMessageimage_'+msgid))

	// Save remaining values
	$('input.remaining').val(updateImages);
	// console.log($('input.remaining').val(updateImages))
	// Save deleted values
	$('input.deleted').val(imageName);

// console.log($('input.deleted').val(imageName))
	// return;

	$(ele).addClass('click');
	$('.click').remove();
	
}

</script>	



 
<span role="status" aria-live="polite" class="select2-hidden-accessible"></span>
