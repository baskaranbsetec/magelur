<link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
<script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script src="{{ asset('magelur/js/star_rating.js')}}"></script>

<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<div class="ourmodel_banner agency_profilepage">
    <section>
      <div class="container-fluid magelur_header">
        <div class=" ">
        <div id="upload-demo-i-cover">
          
          {!! SiteHelpers::user_avatars($row->id,'agent',$row->profile_cover,'/uploads/agency_profile/cover/') !!}
        </div>
        </div>
      </div>
    </section>

  <div class="ourmod_cnt pents">
    
    <h1>{{ $row->agency_name }}</h1>
  </div>
</div>
<div class="mainder_pent">
<div class="sender_parter">
 <div class="penty_houser">
  <h3>{{ $row->agency_name }}</h3>
  <p>{{ $row->username }}</p>
 </div>
 <div class="agents">
  <ul class="minagents">
   <li><span class="left_cols">Agency</span> <span class="righ_cols">:</span>@if($row->agency_name) {{ $row->agency_name }} @else Not Specified @endif</li>
   <li><span class="left_cols">Location</span> <span class="righ_cols">:</span>@if($row->state && $row->country) {{ $row->state }}, {{ $row->country }} @else Not Specified @endif</li>
   <li><span class="left_cols">Languages</span> <span class="righ_cols">:</span> @if($row->language){{ $row->language }} @else Not Specified @endif </li>
  </ul>
 </div>
  <div class="sub_agents">
   <ul class="minsubagents">
   <li><span class="left_cols">Years in Business</span> <span class="righ_cols">:</span> @if($row->years_in_business) {{ $row->years_in_business }} ({{ date("Y")-$row->years_in_business }}yrs) @else Not Specified @endif </li>
   <li><span class="left_cols">Experiences</span> <span class="righ_cols">:</span>  @if($row->experience) {{ $row->experience }} @else Not Specified @endif</li>
   </ul>
  </div>
   <div class="sub_agents agent_rating_blk">
     <div class="image_video_link">
     <div class="agent_profile_rating">
@if(SiteHelpers::checkLogged() )    
<div class="agent_gray_btn rating_star_space">
<div class="post-action">
<!-- Rating -->
<select class='rating' id='rating_{{$row->id}}'  data-id='rating_{{$row->id}}'>
<option value="1" >1</option>
<option value="2" >2</option>
<option value="3" >3</option>
<option value="4" >4</option>
<option value="5" >5</option>
</select>
<input type='hidden' name='post_type' value='agent'>

@if($avg_ratings>0)
<p>{{number_format($avg_ratings,1, '.', '')}}<p>
@endif
@if($row->id == \Session::get('logged_id'))
<script type="text/javascript">
setRating($('#rating_{{$row->id}}'),Math.round({{$avg_ratings}}));
</script>
@endif
</div>
</div>
@endif
</div>
<div class="agent_profile_rating">
<div class="agent_gray_btn">
  <h1><a href="{{ url('agent/album/'.$row->id) }}"><span><i class="fa fa-image"></i></span>Gallery</a></h1>
  </div>
  </div>
  <div class="agent_profile_rating">
  <div class="agent_gray_btn">
  <h1><a href="{{ url('agent/video/'.$row->id) }}"><span><i class="fa fa-video-camera"></i></span>VIDEOS</a></h1>
  </div>
  </div>

    <!-- favourite -->

@if(SiteHelpers::checkLogged() )    
<div class="favourites">
  <a onclick="addToFavourite()" title="Add to favourite"><i class="fa fa-heart"></i></a>
</div>
@endif





  </div>
  </div>
  <div class="setcher agency_setcher">
    <!-- <img src="{{ asset('uploads/agency_profile/'.$row->profile_photo) }}"> -->
 {!! SiteHelpers::user_profiles($row->id,'agent',$row->profile_photo,'/uploads/agency_profile/') !!}
  </div>

 </div>
 <div class="categoriesed">
  <h4>Our Categories</h4>
 
   <ul class="listboxnes">
    @if($row->our_categories) 
    @foreach($row->our_categories as $id)
    <li><a href="javascript:void(0)">{!! $id->name !!}</a></li>
    @endforeach
    @else Not Specified @endif
   </ul>

 </div>
 <div class="bggraphy">
  <h4>About Us (Biography)</h4>
  <p>@if($row->business_biography)  {!! $row->business_biography!!} @else Not Specified @endif</p>
 </div> 
 <div class="bggraphy">
   <h4>Models Intrested In</h4>
  <p>@if($row->looking_for_desc)  {!! $row->looking_for_desc!!} @else Not Specified @endif</p>
 </div>
 <div class="twopackages clearfix">
 
  <div class="start_pack_one">
   <h4>Benefits Offer</h4>
  </div>
  <div class="start_pack_one">
   <h4>Packages Offer</h4>
   <p><a href="javascript:void(0)">Basic Pay</a></p>
   <p><a href="javascript:void(0)">Accommodation</a></p>
   <p><a href="javascript:void(0)">Traveling</a></p>
  </div>
 </div>
 <div class="three_packages clearfix">
  <div class="start_pack_one">
   <h4>Genders Needed</h4>
      
    <ul class="our_categories_list">
  
    <li><a href="javascript:void(0)">@if($row->categories_needed)  {!! $row->genders_needed !!} @else Not Specified @endif</a></li>
      
   </ul>
  </div>
  <div class="start_pack_one">
   <h4>Experiences Needed</h4>
   <ul class="our_categories_list">
    @if($row->experience_needed) 
    @foreach($row->experience_needed as $ids)
    <li><a href="javascript:void(0)">{!! $ids->name !!}</a></li>
    @endforeach      
    @else Not Specified @endif
   </ul>
   
  </div>
  <div class="start_pack_one">
   <h4>Categories Needed</h4>
   <ul class="our_categories_list">
    @if($row->categories_needed) 
     @foreach($row->categories_needed as $id)
      <li><a href="javascript:void(0)">  {{$id->name}}</a></li>
     @endforeach
     @else Not Specified @endif
    
   </ul>
  </div>
 </div>
 @if(\SiteHelpers::checkLogged() )
 <div class="contact_this_agent clearfix">
  @if( \Session::get('logged_id') == $row->id ) @else

  <a href="javascript:void(0);" id="composeMessage"><span>  Contact This Agent </span></a> 
  @endif
</div>
@endif
 <div class="section_seven">
 <h1>Most Recent Registered Models</h1>
 <p>The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p>
 <ul class="instagramer">
  <li class="wow SlideInLeft">
  <img src="{{ asset('images/insta_one.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/insta_two.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_three.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/insta_four.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_five.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
 </ul>
</div>
</div>

<div class="modal fade msg_popup_model" id="compose-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="max-width:800px;width:90%;">
    <div class="modal-content">
    <div class="modal-header bg-default">
      
      <button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true"><span>&times;</span></button>
      <h4 class="modal-titles">Compose Message </h4>
    </div>
    <div class="load_notify"></div>
    <div class="modal-body" id="compose-modal-content">
       {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
        <input type="hidden" name="typee" class="type" />
        <div class="form-group form-group_full">
          <label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
          <div class="col-xs-10">
            <div>
              <input type='hidden' name='recipient_type' value='agent' class='form-control' >

              <input type='hidden' name='recipient' value='{!! $row->id !!}' class='form-control' >{!! $row->username !!}
            </div>
          </div>
        </div>
        <div class="form-group form-group_full">
          <label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
          <div class="col-xs-10">
            <div>{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control','id' => 'subject')) !!}
</div>
          <span class="err_subject" style="color:red"></span>
          </div>
        </div>
        <div class="form-group form-group_full">
          <label for="mail_subject" class="col-xs-2">Message</label>
          <div class="col-xs-10">
            <div><textarea name='message' rows='3' style="width:100%;" class='form-control theme_textarea' placeholder="Enter your message"></textarea></div>
            <span class="error" style="color:red"></span>
          </div>
        </div>
        <div class="form-group form-group_full">
        <label for="mail_subject" class="col-xs-2">Attach files</label>
        <div class="col-xs-10">
        <input  type="file" class="form-control msgpopup_browse_btn" name="images[]" id='images' placeholder="" multiple>
       
        <span class="attach_files_blk">[Allow multiple image]</span>
        </div>
        </div>
        <div class="sending_loadergs"></div>
        <div class="form-group form-group_full">
          <!-- <label class="col-xs-2"></label> -->
          <div class="col-xs-12 msg_button_blk">
            {!! Form::hidden('draft', '0' )  !!}
            {!! Form::hidden('messageid', '' )  !!}
            <button type="submit" name="submit" data-type="inboxx" class="btn-sm sendbtn" ><span><i class="fa  fa-envelope "></i>Send message</span></button>
<!--             <button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
 -->          </div>
        </div>
       {!! Form::close() !!}
    </div>

    </div>
  </div>
</div>
@include('footer_map')
<script>

 $(document).ready(function(){

  
  
  $(function(){
    tinymce.init({  
      mode : "specific_textareas",
      editor_selector : "mceEditor",
      theme : "advanced",
      theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
     });  
  });
  

  function tinyClean(value) {
  value = value.replace(/&nbsp;/ig, ' ');
  value = value.replace(/\s\s+/g, ' ');
  if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
    value = '';
  }
  return value;
}

   $(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       
       recipient1:{
      required:true
      },
      subject:{
      required:true
      },
      message:{
      required:true
      },
        
      },
      messages: {
       recipient1:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Recipient Field is required</span>',
      },
      subject:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Subject Field is required</span>',
      },
      message:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Message Field is required</span>',
      }
       
      },
      
        submitHandler: function(form) {
          toastr.success("Message sent Successfully");     

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
    var subject = $('[name="subject"]').val();
     if(subject==''){
      $('.err_subject').html("Subject Field is required");
    }
    else{
      $('.err_subject').hide();

    }
    if(value==''){
      $('.error').html("Message Field is required");
    }else{
      $('.error').hide();
    }

    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();
        
         $('#compose-modal').modal("toggle"); 
         form.submit();
}
        },

        });
  

  $('#composeMessage').click(function(){
    
    $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
 var msg="<?php if(isset($_REQUEST['msg'])) echo $_REQUEST['msg']; else echo ''; ?>";
 
if(msg)
{
   $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  }

}); 



  </script>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <script type="text/javascript">
       if('{{$favourite}}' == ''){
          $('.favourites a').removeClass('favouriteActive');
          $('.favourites a').attr('title','Add to favourites');
       }else{
          $('.favourites a').addClass('favouriteActive');
          $('.favourites a').attr('title','Remove from favourites');
       }
       function addToFavourite() {

          var favourite_id = '{{$row->id}}';
          var favourite_type = 'agent';
          
          $.ajax({
            url: '{{url("addToFavorites")}}',
            type: 'post',
            data: {favourite_id:favourite_id,favourite_type:favourite_type},
            dataType: 'json',
            success: function(data){
              if (data.status == 'added') {
                  swal({
                    title: "",
                    text: "Agent added to your Favourites",
                    icon: "success",
                    dangerMode: true
                  });
                  $('.favourites a').addClass('favouriteActive');
                  $('.favourites a').attr('title','Remove from favourites');
              }else if(data.status == 'removed'){
                  swal({
                    title: "",
                    text: "Agent removed from your favourites",
                    icon: "success",
                    dangerMode: true
                  });
                  $('.favourites a').removeClass('favouriteActive');
                  $('.favourites a').attr('title','Add to favourites');
              }else{
                swal({
                  title: "",
                  text: "Some error occured. Please try again after some time.",
                  icon: "danger",
                  dangerMode: true
                });
              }
            }
              
          });
        }

        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function(value, text, event) {

                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');

                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {
                        
                        var split_id = el_id.split("_");

                        var postid = split_id[1];  // postid
                        var post_type = $('input[name=post_type]').val();// posttype
                        // AJAX Request
                        $.ajax({
                            url: '{{url("rating_ajax")}}',
                            type: 'post',
                            data: {post_type:post_type,postid:postid,rating:value},
                            dataType: 'json',
                            success: function(data){
                              // alert(data)
                             if (data.status == 'error') {

                                          // Show Error Notification
                                          toastr["error"](data.msg)
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": true,
                                            "progressBar": false,
                                            "positionClass": "toast-top-full-width",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "3000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "linear",
                                            "hideEasing": "swing",
                                            "showMethod": "slideDown"
                                          }
                                        }else{
                                          
                                          // Show success Notification
                                          toastr["success"](data.msg)
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": true,
                                            "progressBar": false,
                                            "positionClass": "toast-top-full-width",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "3000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "linear",
                                            "hideEasing": "swing",
                                            "showMethod": "slideDown"
                                          }
                                          
                                        }
            
          
                                
                            }
                        });
                    }
                }
            });
        });
      
 </script>
<!-- Set rating -->
<script type='text/javascript'>
$(document).ready(function(){
$('#rating_{{$row->id}}').barrating('set',Math.round({{$avg_ratings}}));
});

</script>