
@include('agent_dash_header')

{!! Form::open(array('url'=>'messagelabeldelete', 'class'=>'form-horizontal ajaxLabelDelete','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
	{!! Form::hidden('label_id', '' )  !!}
{!! Form::close() !!}
<input type="hidden" name="urlmodule" id="urlmodule" value="http://192.168.1.65:81/magelur/public/message">


<div class="aboutmodel">
 <div class="container">
  <div class="message_two_sec clearfix">
   
   <div class="col-sm-3">
   	<div class="message_left">
		<div class="">
		<div class="sbox-title sendReplyForward" type="button" data-btn="forward" style="height:65px" ><a href="javascript:void(0);" id="composeMessage" class="btn btn-block btn-lg btn-primary">Compose message</a></div>

		</div>
		<ul class=" msaged">
		<li class="@if($type == 0) active @endif"><a href="{{ URL::to('message?ty=0') }}">
		Inbox
		<span  data-inbox="{!! $totalType['0'] !!}" id="cinbox">{!! $totalType['0'] !!}</span>
		</a></li>
		<li class="@if($type == 1) active @endif"><a href="{{ URL::to('message?ty=1') }}">
		Important
		<span  data-star="{!! $totalType['1'] !!}" id="cstar">{!! $totalType['1'] !!}</span>
		</a></li>
		<li class=" @if($type == 2) active @endif"><a href="{{ URL::to('message?ty=2') }}">
		Send item
		<span  data-sent="{!! $totalType['2'] !!}" id="csent">{!! $totalType['2'] !!}</span>
		</a></li>
		<li class=" @if($type == 3) active @endif"><a href="{{ URL::to('message?ty=3') }}">
		Draft
		<span  data-draft="{!! $totalType['3'] !!}" id="cdraft">{!! $totalType['3'] !!}</span>
		</a></li>
		<li class=" @if($type == 4) active @endif"><a href="{{ URL::to('message?ty=4') }}">
		Trash
		<span  data-trash="{!! $totalType['4'] !!}" id="ctrash">{!! $totalType['4'] !!}</span>
		</a></li>
		</ul>
				
   	</div>	
   </div>

   <div class="col-sm-9">
   	<div class="message_box clearfix">
   	 <div class="left_sht_msg">
   	 	<div class="letmsg"><input type="text" id='search' name="search" placeholder="Search"></div>
   	 	<div class="fullmsg_blg message_block_leftside">
   	 	<div class="msg_blk_slimscroll">
			<table class="table table-hover" id='userTbl'>
				<tbody class="dataMessage">
				@foreach($rowData as $row)
					<input type="hidden" name="entryby" class="entryby" value="{{ $row->entry_by }}" />
					{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
					@if($row->read == 0 && ($type == 0 || $type == 1)) <tr class="mail-unread msg_blk" id="msg_{!! $row->id !!}"> @else <tr class="mail msg_blk" id="msg_{!! $row->id !!}"> @endif
					<td data-type="{!! $type !!}" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}" width="0" style="padding-bottom:10px; padding-right:0px;"></td>
					{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
					{{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}
					{{--*/ if($isSender == 1 && isset($usersAgentAvatar[$row->recipient])) { $rowUserAvatarAgent = $usersAgentAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAgentAvatar[$row->entry_by])) { $rowUserAvatarAgent = $usersAgentAvatar[$row->entry_by]; } else { $rowUserAvatarAgent = ''; } /*--}}
					{{--*/ if($isSender == 1 && isset($usersAgentNameList[$row->recipient])) { $rowAgentUser = $usersAgentNameList[$row->recipient]; } else if($isSender == 0 && isset($usersAgentNameList[$row->entry_by])) { $rowAgentUser = $usersAgentNameList[$row->entry_by]; } else { $rowAgentUser = ''; } /*--}}

					<td class="td-clickable text-success">
					<div class="msg_profile_image">
					<!-- @if($rowUserAvatar)
					<img src="{{ asset('uploads/model_profile/'.$rowUserAvatar) }}" border="0" width="25" height="25" class="img-circle">
					@else
					<img src="{{ asset('uploads/images/no-image-rounded.png') }}" border="0" width="25" height="25" class="img-circle">
					@endif -->
					@if($type == 2)
					@if($row->recipient_type == 'agent' )
					{!! SiteHelpers::user_profiles($row->id,'agent',$rowUserAvatarAgent,'/uploads/agency_profile/') !!}
					@elseif($row->recipient_type == 'model')
					{!! SiteHelpers::user_profiles($row->id,'model',$rowUserAvatar,'/uploads/model_profile/') !!}
					@endif
					@else
					@if($row->model_agent == '2' )
					{!! SiteHelpers::user_profiles($row->id,'agent',$rowUserAvatarAgent,'/uploads/agency_profile/') !!}
					@elseif($row->model_agent == '1')
					{!! SiteHelpers::user_profiles($row->id,'model',$rowUserAvatar,'/uploads/model_profile/') !!}
					@elseif($row->model_agent == '3')
					{!! SiteHelpers::user_avatar('1','admin') !!}
					@endif
					@endif
					</div>
					<div class="usertext">
					<a class="">
						@if($type == 2)
						@if($row->recipient_type == 'agent') {!! $rowAgentUser !!} @elseif($row->recipient_type == 'model') {!! $rowUser !!} @endif
					@else
					@if($row->model_agent == '2') 
					{!! $rowAgentUser !!} 
					@elseif($row->model_agent == '1') 
					{!! $rowUser !!}
					@elseif($row->model_agent == '3') 
					Admin
					@endif
					@endif
					</a>	
					@if($row->recipient_type == 'agent' )
					<input type="hidden" name="viewrowUser" class="viewrowUser" value="{!! $rowAgentUser !!}" />
					@elseif($row->recipient_type == 'model') 
					<input type="hidden" name="viewrowUser" class="viewrowUser" value="{!! $rowUser !!}" />
					@endif

					<p>{!! substr(\SiteHelpers::getName('message_subject',$row->subject) , 0, 30) !!}...</p>
					</div>

					<div class="more_dots">
					<a href="javascript:void(0)"></a>
					<p>{{--*/$zone=date("Y-m-d g:i a", strtotime($row->createdOn));/*--}}
					{!! \SiteHelpers::time_ago($zone) !!}</p>
					</div>
					</td>
					{{--*/ if($isSender == 0 && isset($labels[$row->label_by_recipient])) { $rowLabel = $labels[$row->label_by_recipient]; $rowLabelId = $row->label_by_recipient; } else if($isSender == 1 && isset($labels[$row->label_by_sender])) { $rowLabel = $labels[$row->label_by_sender]; $rowLabelId = $row->label_by_sender; } else { $rowLabel = ''; $rowLabelId = ''; } /*--}}
					<!-- <td class="td-clickable text-"><label class="badge badge-inverse labelDisp" data-id="{!! $rowLabelId !!}" id="label_{!! $row->id !!}">{!! $rowLabel !!}</label> {!! substr($row->subject, 0, 30) !!}</td> -->
					<!-- <td class="td-clickable text-light">{!! substr(strip_tags($row->message), 0, 60) !!}</td> -->
					<!-- <td class="td-clickable text-light">{!! date("m/d/y h:i A", strtotime($row->createdOn)) !!}</td> -->
					<td>
					{!! Form::open(array('url'=>'messageupdate', 'class'=>'form-horizontal ajaxMessageUpdate msg_star_block','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
					{{--*/ if($type == 0) $rowStarred = $row->starred_by_recipient; else if($type == 2) $rowStarred = $row->starred_by_sender; else $rowStarred = 0; /*--}}
					{!! Form::hidden('starred', $rowStarred )  !!}
					{!! Form::hidden('update_function', '' )  !!}
					{!! Form::hidden('msg_type', $type )  !!}
					{!! Form::hidden('msg_id', $row->id )  !!}
					@if($type == 0 || $type == 2)
					<i class="fa fa-star msgstar @if($rowStarred == 1) active @endif"></i>
					@endif
					{!! Form::close() !!}
					</td>
					</tr>
					
					@endforeach

					@if ( empty ( $rowData ) ) 
					No Messages here !
					@endif
				</tbody>
			</table>
					{!! Form::open(array('url'=>'messagegroupupdate', 'class'=>'form-horizontal ajaxGroupMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('msg_ids', '' )  !!}
						{!! Form::hidden('is_sender', '' )  !!}
						{!! Form::hidden('label_id', '' )  !!}
						{!! Form::hidden('label_name', '' )  !!}
						{!! Form::hidden('group_update_function', '' )  !!}
						{!! Form::hidden('type', $type )!!}
					{!! Form::close() !!}
   	
		</div> 
		</div>
   	 </div>

@foreach($rowData as $row)
		{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
		<div class="hidden viewMessageDiv" id="viewMessageDiv_{!! $row->id !!}">

		<div class="msg_right_box " id='{!! $row->id !!}'>
		<div class="close_btn_div">
		<button class="btn  gotodataMessageDiv" type="">
		<!-- <i class="fa fa-close"></i> -->x
		</button>
		</div>
		<div class="chat_content_div1">

		<div class="total_newer clearfix">

		<div class="renew ">
		
<input type="hidden" class="senderid" value="{!! $row->entry_by !!}">
				<input type="hidden" class="recipientid" value="{!! $row->recipient !!}">
				<input type="hidden" class="msg_id" value="{!! $row->id !!}">
				<input type="hidden" class="subj_id" value="{!! $row->subject !!}">
					<input type="hidden" class="parti_mes" value="{!! $row->message !!}">

				<h2 class="viewMessageTitle pull-left" hidden>{!! $row->subject !!}</h2>
<div id='start'>
<input type="hidden" name="viewMessageimage_{!! $row->id !!}" class="viewMessageimage viewMessageimage_{!! $row->id !!} pull-left" value='{!! $row->attachments !!}'></div>

						<h3  >Sub:{!! \SiteHelpers::getName('message_subject',$row->subject) !!}</h3>
						{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
						{{--*/ if($isSender == 1 && isset($usersEmailList[$row->recipient])) { $rowUserEmail = $usersEmailList[$row->recipient]; } else if($isSender == 0 && isset($usersEmailList[$row->entry_by])) { $rowUserEmail = $usersEmailList[$row->entry_by]; } else { $rowUserEmail = ''; } /*--}}
						{{--*/ if($isSender == 1 && isset($usersIdList[$row->recipient])) { $rowUserId = $usersIdList[$row->recipient]; } else if($isSender == 0 && isset($usersIdList[$row->entry_by])) { $rowUserId = $usersIdList[$row->entry_by]; } else { $rowUserId = ''; } /*--}}
						{{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}
						{{--*/ if($isSender == 1 && isset($usersAgentAvatar[$row->recipient])) { $rowUserAvatarAgent = $usersAgentAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAgentAvatar[$row->entry_by])) { $rowUserAvatarAgent = $usersAgentAvatar[$row->entry_by]; } else { $rowUserAvatarAgent = ''; } /*--}}
					    {{--*/ if($isSender == 1 && isset($usersAgentNameList[$row->recipient])) { $rowAgentUser = $usersAgentNameList[$row->recipient]; } else if($isSender == 0 && isset($usersAgentNameList[$row->entry_by])) { $rowAgentUser = $usersAgentNameList[$row->entry_by]; } else { $rowAgentUser = ''; } /*--}}	

					<p><span class="for_span">@if($type == 2 || $row->starred_by_sender == 1 )to @else from @endif</span> 
					<span class="ed_span">
						@if($type == 2) 
						@if($row->recipient_type == 'agent') {!! $rowAgentUser !!} @elseif($row->recipient_type == 'model') {!! $rowUser !!} @endif
						@else

						@if($row->model_agent == '2') 
						{!! $rowAgentUser !!} 
						@elseif($row->model_agent == '1') 
						{!! $rowUser !!}
						@elseif($row->model_agent == '3') 
						Admin
						@endif
						@endif
					</span></p>
<!-- 		<p><span class="for_span">to</span> <span class="ed_span">You</span></p>
 -->
 @if($type == 2) 
 @if($row->recipient_type == 'agent') 
 <input type="hidden" class="parti_recip" value="{!! $rowAgentUser !!}">
 <h2 class="viewMessagename pull-left" hidden>{!! $rowAgentUser !!}</h2>

 @elseif($row->recipient_type == 'model') 
<input type="hidden" class="parti_recip" value="{!! $rowUser !!}">
<h2 class="viewMessagename pull-left" hidden>{!! $rowUser !!}</h2>

 @endif	
@else
 @if($row->model_agent == '2') 
 <input type="hidden" class="parti_recip" value="{!! $rowAgentUser !!}">
 <h2 class="viewMessagename pull-left" hidden>{!! $rowAgentUser !!}</h2>

 @elseif($row->model_agent == '1') 
<input type="hidden" class="parti_recip" value="{!! $rowUser !!}">
<h2 class="viewMessagename pull-left" hidden>{!! $rowUser !!}</h2>

 @endif	
@endif
 <h2 class="viewMessageTitles pull-left" hidden>{!! $row->entry_by !!}</h2>

		</div>
		<div class="minew">

		{{--*/$monthNum=date("m")/*--}}
		<p>
		@if(date('d')==date("d", strtotime($row->createdOn)))
		Today 
		@elseif((date('d')-1)==date("d", strtotime($row->createdOn)))
		Yesterday 
		@else
		{!! date('F', mktime(0, 0, 0, $monthNum, 10))!!}
		@endif
		{!! date(" h:i A", strtotime($row->createdOn)) !!}	  </p>

		<div class="model_icoded">
		<ul class="stuningeder">
		<!--  <li class="shield"><a href="javascript:void(0)"></a></li>
		<li class="warn"><a href="javascript:void(0)"></a></li> -->
		
		@if($type != 4 && $type != 3)	
		<li class="right_multi_left sendReplyForward" type="button" title='Reply' data-btn="reply" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
		</a></li>
		<li class="delets singleMessageDelete" type="button" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}"><a href="javascript:void(0)">

		</a></li>
		<!-- <li class="right_multi_middle"><a href="javascript:void(0)"></a></li> -->
		<li class="right_multi_right sendReplyForward" type="button" title='Forward' data-btn="forward" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
		</a></li>
		</ul>	
		</div>
		@endif
		</div>
		</div>
		<div class="newtent_content viewMessageContent">
		<p>{!! $row->message !!}</p>

		</div>
		<div class="rader clearfix rader_div rader_msg_div">
		<div class="minyues">
		{{--*/ $images = explode(',', $row->attachments); /*--}}
		@foreach($images as $info) 
		@if($row->attachments !='')
		<span>
		<a  rel='gallery2' href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
		<img src="{{ URL::to('uploads/message/') }}/{{ $info }}" class="img-circle msg_attachment_images"></a> 
		</span>
		@else
		<span style='display:none'>
		<a  href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
		<img src="{{ URL::to('uploads/message/') }}/{{ $info }}" class="img-circle msg_attachment_images"></a> 
		</span>
		@endif
		@endforeach
		</div>

		</div>
		</div>
		
		</div>
		</div>
	
   	
	@endforeach
	</div>  	 	
   	</div>	
   </div>

  </div>	
 </div>	
</div>	
</div>
</div>
</div>
</div>
</div>
</div>

@include('modal_popup')
@include('footer_map')



<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header bg-default">
		
		<button type="button " class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">Modal title</h4>
	</div>
	<div class="modal-body" id="bsetec-modal-content">

	</div>

  </div>
</div>
</div>


<script>


$('#recipient1').autocomplete({
		source: function(request, response) {
		$.ajax({
		    url: "{{ url('auto/model') }}",
		    dataType: "json",
		    data:{name:request},
		    success: function (data) {
		    	if(data =='')
		    	{
		    		$('#auto-content').show();

		    	}
		    	else{
		    		$('#auto-content').hide();
		    	}
		        response($.map(data, function (item) {
		            return {
		                label: item['username'],
		                value: item['id']
		            };
		        }));
		    }
		});
		},select: function(item) {
			$('#recipient1 #recipient').val("");			
			$('#recipient1').val(item['label']);
			$('#recipient').val(item['value']);
		}

	});	
</script>