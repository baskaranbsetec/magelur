	@include('agent_dash_header')


<div class="subscription">
 <div class="container">
  <div class="subpanter clearfix">
    @foreach($agents as $agent)
  
     @if($agent->plan_id !='')
       @foreach($membershipplan as $plan)
   <div class="subscript_left">	
  	<a class="subcript" href="javascript:void(0)">Subscription</a>
    	<p><span class="sub_stateone">Current Status</span>    
      <span class="sub_static">:</span> {{$plan->plan_name}} Membership</p> 
	    <p><span class="sub_stateone">Subscription Plan</span> 
      <span class="sub_static">:</span> 24 month</p>
	    <p><span class="sub_stateone">Renewal date</span>      
      <span class="sub_static">:</span> 15/04/2019</p>
     
	    <div class="upmap"><a href="{{ url('/membership-plan') }}">Upgrade My subscription</a> 
      <a href="javascript:void(0)">Pause my subscription</a></div>
     
     
   </div>
    @endforeach
   @else
   <div class="subscript_left">  
    
      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
      </div>
    
   </div>
   @endif
   @endforeach
   <div class="subscript_right">	
  	<img src="{{ asset('images/membership_brand.png') }}">
   </div>
  </div>	
 </div>	
</div>	


    @include('footer_map')


<script type="text/javascript">
        $(document).ready(function() {
        $('#vertical').lightSlider({
          item:1,
          vertical:true,
          verticalHeight:250
        });  
      });
</script>


<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
    document.getElementById("main").style.marginLeft= "";
}
</script>