

		 {!! Form::open(array('url'=>'agency/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Agency Management</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Username" class=" control-label col-md-4 text-left"> Username </label>
										<div class="col-md-7">
										  <input  type='text' name='username' id='username' value='{{ $row['username'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Email" class=" control-label col-md-4 text-left"> Email </label>
										<div class="col-md-7">
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Agency Name" class=" control-label col-md-4 text-left"> Agency Name </label>
										<div class="col-md-7">
										  <input  type='text' name='agency_name' id='agency_name' value='{{ $row['agency_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Agency Description" class=" control-label col-md-4 text-left"> Agency Description </label>
										<div class="col-md-7">
										  <input  type='text' name='agency_description' id='agency_description' value='{{ $row['agency_description'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Mobile" class=" control-label col-md-4 text-left"> Mobile </label>
										<div class="col-md-7">
										  <input  type='text' name='mobile' id='mobile' value='{{ $row['mobile'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Business Name" class=" control-label col-md-4 text-left"> Business Name </label>
										<div class="col-md-7">
										  <input  type='text' name='business_name' id='business_name' value='{{ $row['business_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Business Description" class=" control-label col-md-4 text-left"> Business Description </label>
										<div class="col-md-7">
										  <input  type='text' name='business_description' id='business_description' value='{{ $row['business_description'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo </label>
										<div class="col-md-7">
										  <input  type='file' name='profile_photo' id='profile_photo' @if($row['profile_photo'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/agency_profile/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Agency Banner" class=" control-label col-md-4 text-left"> Agency Banner </label>
										<div class="col-md-7">
										  <input  type='file' name='agency_banner' id='agency_banner' @if($row['agency_banner'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['agency_banner'],'/uploads/agency_banner/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Verification Status" class=" control-label col-md-4 text-left"> Verification Status </label>
										<div class="col-md-7">
										  <input  type='text' name='verification_status' id='verification_status' value='{{ $row['verification_status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-7">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Website" class=" control-label col-md-4 text-left"> Website </label>
										<div class="col-md-7">
										  <input  type='text' name='website' id='website' value='{{ $row['website'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Business Biography" class=" control-label col-md-4 text-left"> Business Biography </label>
										<div class="col-md-7">
										  <textarea name='business_biography' rows='5' id='business_biography' class='form-control '  
				           >{{ $row['business_biography'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Looking For Desc" class=" control-label col-md-4 text-left"> Looking For Desc </label>
										<div class="col-md-7">
										  <textarea name='looking_for_desc' rows='5' id='looking_for_desc' class='form-control '  
				           >{{ $row['looking_for_desc'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number </label>
										<div class="col-md-7">
										  <input  type='text' name='contact_number' id='contact_number' value='{{ $row['contact_number'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Membership" class=" control-label col-md-4 text-left"> Membership </label>
										<div class="col-md-7">
										  <input  type='text' name='membership' id='membership' value='{{ $row['membership'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Membership Expiration" class=" control-label col-md-4 text-left"> Membership Expiration </label>
										<div class="col-md-7">
										  <input  type='text' name='membership_expiration' id='membership_expiration' value='{{ $row['membership_expiration'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Membership Id" class=" control-label col-md-4 text-left"> Membership Id </label>
										<div class="col-md-7">
										  <input  type='text' name='membership_id' id='membership_id' value='{{ $row['membership_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rate" class=" control-label col-md-4 text-left"> Rate </label>
										<div class="col-md-7">
										  <input  type='text' name='rate' id='rate' value='{{ $row['rate'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Views" class=" control-label col-md-4 text-left"> Views </label>
										<div class="col-md-7">
										  <input  type='text' name='views' id='views' value='{{ $row['views'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address </label>
										<div class="col-md-7">
										  <textarea name='address' rows='5' id='address' class='form-control '  
				           >{{ $row['address'] }}</textarea> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="City" class=" control-label col-md-4 text-left"> City </label>
										<div class="col-md-7">
										  <input  type='text' name='city' id='city' value='{{ $row['city'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="State" class=" control-label col-md-4 text-left"> State </label>
										<div class="col-md-7">
										  <input  type='text' name='state' id='state' value='{{ $row['state'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Country" class=" control-label col-md-4 text-left"> Country </label>
										<div class="col-md-7">
										  <input  type='text' name='country' id='country' value='{{ $row['country'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode </label>
										<div class="col-md-7">
										  <input  type='text' name='zipcode' id='zipcode' value='{{ $row['zipcode'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created Date" class=" control-label col-md-4 text-left"> Created Date </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_date', $row['created_date'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Last Updated" class=" control-label col-md-4 text-left"> Last Updated </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('last_updated', $row['last_updated'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Approved Date" class=" control-label col-md-4 text-left"> Approved Date </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('approved_date', $row['approved_date'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
