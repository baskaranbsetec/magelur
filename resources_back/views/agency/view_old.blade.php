@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('agency?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('agency/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('agency/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('agency/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>{{ $row->username}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Name</td>
						<td>{{ $row->agency_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Description</td>
						<td>{{ $row->agency_description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>{!! SiteHelpers::showUploadedFile($row->profile_photo,'/uploads/agency_profile/') !!}</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Banner</td>
						<td>{!! SiteHelpers::showUploadedFile($row->agency_banner,'/uploads/agency_banner/') !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Mobile</td>
						<td>{{ $row->mobile}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Name</td>
						<td>{{ $row->business_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Description</td>
						<td>{{ $row->business_description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Biography</td>
						<td>{{ $row->business_biography}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Looking For Desc</td>
						<td>{{ $row->looking_for_desc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership</td>
						<td>{{ $row->membership}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>{{ $row->approved_date}} </td>
						
					</tr>
				
					
				
					<tr>
						<td width='30%' class='label-view text-right'>Website</td>
						<td>{{ $row->website}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>{{ $row->contact_number}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Expiration</td>
						<td>{{ $row->membership_expiration}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Id</td>
						<td>{{ $row->membership_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Rate</td>
						<td>{{ $row->rate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Views</td>
						<td>{{ $row->views}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Address</td>
						<td>{{ $row->address}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>{{ $row->state}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>{{ $row->country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>{{ $row->zipcode}} </td>
						
					</tr>
				<tr>
						<td width='30%' class='label-view text-right'>Verification Status</td>
						<td>@if( $row->verification_status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>{{ $row->created_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>{{ $row->last_updated}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop