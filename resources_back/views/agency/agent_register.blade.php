
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>

<div class="model_registration">
<div class="section_one">
 <div class="left_half">
  <img src="{{ asset('images/register_bg.png') }}">
 </div>
  <div class="container">
   <div class="cnt_txt wow Bounce">
    <h2>Talent Agents Registration</h2>
    <h3><span></span>Promote Yourself</h3>
   </div>
  </div>
</div><!--end section_one-->

<div class="register_wrap">
  <div class="container">
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="left-content">
        <h3><span>Become</span><br/> An Agent today</h3>
        <div class="left-registerbg">
          <h4>Your road to stardom starts here... <span>Sign up now!</span></h4>
          <form method="POST" action="{{ url('/agent/signup') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">
       <div id='content-area'></div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="decoded_id" value="{{ app('request')->input('tellfriend') }}">
            <label class="label-head">your name</label>
            <b> <div id='message' class='w3-animate-bottom' style='top: -8px;font-size: 15px; color: #bd8414;margin-bottom: 17px;'></div></b>

           <!--  <div class="form-group">
            <input type="text" name='firstname' id='firstname' class="form-control" placeholder="Enter your first name" >
            </div>-->
            <!--end form-group-->

            <div class="form-group">
            <input type="text" name='username' id='username' class="form-control" placeholder="Enter your name" >
            </div><!--end form-group-->

            <div class="about-form">
            <label class="label-head">about you</label>
            <div class="clearfix"></div>
             <div class="form-group first date" >
  
              <input type="text" name="DOB" id="datepicker" class="form-control"  autocomplete='off' placeholder="Date of Birth"  >
              <span class="input-group-addon" style='display:none'></span>
    
           
            </div><!--end form-group-->
  
    
            <div class="form-group">
            <div class="select-style">
            <select name='gender' id='gender' class="form-control">
              <option value=''>Select Gender</option>
              <option value='male'>Male</option>
              <option value='female'>Female</option>
              <option value='other'>Other</option>
            </select>
            </div><!--end select-style-->
            </div>
            </div><!--end about-form-->

            <label class="label-head">Your Business</label>[optional]
             <div class="form-group">
              <input type="text" name="business_name" id="business_name" class="form-control" placeholder="Enter your business name"  >            
            </div><!--end form-group-->

            <div class="form-group">
              <input type="text" name="website" id="website" class="form-control" placeholder="Enter your business URL">            
            </div><!--end form-group-->

            <label class="label-head">Your (Business) Location</label>
             <div class="form-group">
              <input type="text" name="city" id="city" class="form-control" placeholder="Enter your city here"  >            
            </div><!--end form-group-->

            <div class="form-group">
              <input type="text" name="state" id="state" class="form-control" placeholder="Enter your state here"  >            
            </div><!--end form-group-->
            

            <div class="common-form">
            <label class="label-head">select country</label>
              <div class="form-group">
            <div class="select-style">
              {!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),'',array('class' => 'form-control','id' => 'country')) !!}

            </div><!--end select-style-->
            </div>
            </div><!--end common-form-->

            <div class="common-form">
            <label class="label-head">email address</label>
             <div class="form-group">
            <input type="text" name="email" id='email' class="form-control" placeholder="Enter your email address here">
            </div><!--end form-group-->
            </div><!--end common-form-->

            <div class="common-form">
            <label class="label-head">business biography</label>
             <div class="form-group">
            <textarea class="form-control" name='business_biography' placeholder="Write your biography here"></textarea>
        
            </div><!--end form-group-->
            </div><!--end common-form-->

            <div class="common-form">
            <label class="label-head">What models are you looking for?</label>
             <div class="form-group">
            <textarea class="form-control" name='looking_for_desc' placeholder=""></textarea>
        
            </div><!--end form-group-->
            </div><!--end common-form-->

            <p class="file-typetxt">What do you look like?: <span>[formats: jpg/jpeg, gif, png] [max size: 2mb]</span></p>
            <div class="form-group">
              <div id="clickHere">
                 <input type="file"  name='profile_photo' id="profile_photo"  />
               </div>
            </div>
            @if(CNF_RECAPTCHA =='true') 
            <div class="inputslogin second_inputs_lg has-feedback  animated fadeInLeft delayp1">
              <br />
              {!! captcha_img() !!} <br /><br />
              <input type="text" name="captcha" placeholder="Type Security Code"  required/>

              <div class="clr"></div>
            </div>  
            @endif
      <br />
      <br />
            <p class="terms-text">By registering with us, you agree that you are 18 years of age or older. You also agree to our <span>privacy policy</span> and terms of service. all information (including images) are true about you, and all images attached were taken at the age of 18 years old or older.</p>

              <label class="agree">I agree
                <input type='radio' name='terms_agree' value ='1'>  
                <span class="checkmark"></span>
              </label>

            <div class="submit-btn">
                 <button type="submit" class='bef_subm'>submit</button>
                <button type="submit" class='aft_subm' hidden>Submitting..</button>
            </div><!--end submit-btn-->
          </form>
        </div><!--end left-registerbg-->
      </div><!--end left-content-->
    </div><!--end col-->
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="right-content">
          <h3>Let us discover you!</h3>
          <p>Not yet a member? Here is what you are missing out on!</p>

          <ul class="list-txt">
              <li><span></span>Promoting yourself on our vibrant and growing network</li>
              <li><span></span>Being the next top Model</li>
              <li><span></span>Share your talents for all to see</li>
              <li><span></span>Share your photos and videos</li>
              <li><span></span>Share your social pages to be discovered in more ways</li>
              <li><span></span>Commenting and liking other talents and productions</li>
          </ul>

          <p class="acc-txt">Already have an account?</p>
        {{--*/ $url=app('request')->input('tellfriend') /*--}}
          <div class="login-btn">
            @if( app('request')->input('tellfriend'))
            <a href="{{ url('/login?tellfriend=') }}{{$url}}">Login Now!</a>
            @else
            <a href="{{ url('/login') }}">Login Now!</a>
            @endif
          </div>

          <p class="add-optiontxt">Check out these additional options...</p>

          <div class="add-details">
            <div class="left-text">
                <p>Already Have an Account?</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
                @if( app('request')->input('tellfriend'))
                <a href="{{ url('/login?tellfriend=') }}{{$url}}">Login in now</a>
                @else
                <a href="{{ url('/login') }}">Login in now</a>
                @endif
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>About Us</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/aboutus') }}">learn more</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view models in your industry</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/ourmodel') }}">view models</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view events & castings</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/events/list') }}">view events</a>
            </div>
          </div><!--end add-details-->

      </div><!--end right-content-->
    </div><!--end col-->
  </div><!--end container-->
</div><!--end register_wrap-->

<div class="safety-wrap">
  <div class="container">
      <h3>Safety, our top priority</h3>
      <p>Pure & Innocent - Talent Models Network mandates the safety and well-being of all users of this application as our top priority. We pride ourselves in our professionalism and transparency, and believe it is important to advise everyone about unsavoury people on the periphery of this industry, who prey on young people's ambitions. Please be aware that we do all communications through our portal application and we protect your information from all prying eyes.</p>
  </div>
</div><!--end safety-wrap-->

<div class="section_seven">
 <h1>Most Recent Registered Models</h1>
 <p class="sub-text">The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p>
 <ul class="instagramer">
  <li class="wow SlideInLeft">
  <img src="{{ asset('images/model_img.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img2.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img3.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img4.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img5.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
 </ul>
</div>



<script type="text/javascript">
 
// $( "#datepicker" ).datepicker({
//       showOtherMonths:  true,
//             selectOtherMonths:  true,
//             changeMonth:      true,
//             changeYear:         true,
//             dateFormat:         "yy-mm-dd",
//             showAnim:         "slideDown",
//             defaultDate:        "-18y",
//             yearRange:        "-68: -18",
//             //minDate:          '-100y', 
//             //maxDate:          "-17y",

//     });
var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);
$('#datepicker').datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
$("#register").validate({
     ignore: "not:hidden",
      rules: {
         captcha:{
         required:true,
         
        },
        username:{
          required:true,
        },
       
         DOB:{
          required:true,
          
        },
        gender:{
          required:true,
          
        },
        city:{
          required:true,
          
        },
        state:{
          required:true,
          
        },
        country:{
          required:true,
          
        },
         business_biography:{
          required:true,
          
        },
        website:{
           url: true
        },
        looking_for_desc:{
          required:true,
          
        },
        profile_photo:{
          required:true,
           accept:"jpg,png,jpeg,gif"
          
        },
        terms_agree:{
          required:true,
          
        },
         email:{
          required:true,
          email:true,
          remote:{
          url:"{!! url('agent/checkuniqueemail')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#email').val();
            },
            
          }
        }
        },
         password:{
         required:true,
         rangelength:[6,12]
        },
   

      },
      messages: {
          username:{
          required:"This field is required", 

         
        },
       
        DOB:{
          required:"This field is required",       
         
        },
        gender:{
          required:"This field is required",       
         
        },
        city:{
          required:"This field is required",       
         
        },
        state:{
          required:"This field is required",       
         
        },
        country:{
          required:"This field is required",       
         
        },
        business_biography:{
          required:"This field is required",       
         
        },
        website:{
           url: 'Please enter a valid url'
        },
        looking_for_desc:{
          required:"This field is required",       
         
        },
         profile_photo:{
          required:"This field is required",  
          accept: "Please provide valid extension",      
         
        },
         terms_agree:{
          required:"This field is required",       
         
        },
        email:{
          required:"This field is required",
          email:"Invalid Email address",
        },
        
        password:{
         required:"This field is required",
         rangelength:"Password must between 6 to 12 characters"
        },
      },
      
        submitHandler:function(form){
toastr.success("Registered Successfully");
        setTimeout(function(){
        form.submit();
      },2000);
        }
           /*$.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),
             beforeSend: function() {
        $('.bef_subm').hide();
        $('.aft_subm').show();
        },
        success: function(data) {
        // reset form values
            $('input[name="username"]').val('');
            $('input[name="business_name"]').val('');
            $('input[name="email"]').val('');
            $('input[name="DOB"]').val('');
            $('input[name="city"]').val('');
            $('input[name="state"]').val('');
            $('input[name="website"]').val('');
            $('textarea[name="looking_for_desc"]').val('');
            $('textarea[name="business_biography"]').val('');
            $('input[name="profile_photo"]').val('');
            $('input[name="terms_agree"]').attr('checked', false);
            $("#country").val('');
            $("#gender").val('');
            $('.bef_subm').show();
            $('.aft_subm').hide();
            $('#content-area').prepend('<i class="fa fa-check" style="margin-right: 10px;"></i> Thanks for registering! . You will get mail soon after we active your account ');
            $('html, body').animate({
                    scrollTop: $("div.left-registerbg").offset().top
                  }, 1000);
            // toastr.success("Registered Successfully");

        },
          });
}*/
    });



 </script>
