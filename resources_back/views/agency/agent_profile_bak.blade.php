<link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
<script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<div class="ourmodel_banner agency_profilepage">
    <section>
      <div class="container-fluid magelur_header">
        <div class=" ">
        <div id="upload-demo-i-cover">
          
          {!! SiteHelpers::user_avatars($row->id,'agent',$row->profile_cover,'/uploads/agency_profile/cover/') !!}
        </div>
        </div>
      </div>
    </section>

  <div class="ourmod_cnt pents">
    
    <h1>{{ $row->agency_name }}</h1>
  </div>
</div>
<div class="mainder_pent">
<div class="sender_parter">
 <div class="penty_houser">
  <h3>{{ $row->agency_name }}</h3>
  <p>{{ $row->username }}, {{ $row->country }}</p>
 </div>
 <div class="agents">
  <ul class="minagents">
   <li><span class="left_cols">Agency</span> <span class="righ_cols">:</span> {{ $row->agency_name }}</li>
   <li><span class="left_cols">Location</span> <span class="righ_cols">:</span> {{ $row->username }}, {{ $row->country }}</li>
   <li><span class="left_cols">Languages</span> <span class="righ_cols">:</span> {{ $row->language }} </li>
  </ul>
 </div>
  <div class="sub_agents">
   <ul class="minsubagents">
   <li><span class="left_cols">Years in Business</span> <span class="righ_cols">:</span> {{ $row->years_in_business }} ({{ date("Y")-$row->years_in_business }}yrs)</li>
   <li><span class="left_cols">Experiences</span> <span class="righ_cols">:</span> {{ $row->experience }}</li>
   </ul>
  </div>
  <div class="setcher agency_setcher"><img src="{{ asset('uploads/agency_profile/'.$row->profile_photo) }}"></div>
  <div class="image_video_link">
@if(SiteHelpers::checkLogged() )    
<div class="post-action">
<!-- Rating -->
<select class='rating' id='rating_{{$row->id}}'  data-id='rating_{{$row->id}}'>
<option value="1" >1</option>
<option value="2" >2</option>
<option value="3" >3</option>
<option value="4" >4</option>
<option value="5" >5</option>
</select>
<input type='hidden' name='post_type' value='agent'>

</div>
@endif

  <h1><a href="{{ url('agent/album/'.$row->id) }}"><span><i class="fa fa-image"></i></span>PHOTOS</a></h1>
  <h1><a href="{{ url('agent/video/'.$row->id) }}"><span><i class="fa fa-video-camera"></i></span>VIDEOS</a></h1>
  </div>
 </div>
 <div class="categoriesed">
  <h4>Our Categories</h4>
 
   <ul class="listboxnes">
     @foreach($row->our_categories as $id)
    <li><a href="javascript:void(0)">{!! $id->name !!}</a></li>
       @endforeach
   </ul>

 </div>
 <div class="bggraphy">
  <h4>About Us (Biography)</h4>
  {!! $row->business_biography!!}
 </div> 
 <div class="bggraphy">
   <h4>Models Intrested In</h4>
  {!! $row->looking_for_desc!!}
 </div>
 <div class="twopackages clearfix">
 
  <div class="start_pack_one">
   <h4>Benefits Offer</h4>
  </div>
  <div class="start_pack_one">
   <h4>Packages Offer</h4>
   <p><a href="javascript:void(0)">Basic Pay</a></p>
   <p><a href="javascript:void(0)">Accommodation</a></p>
   <p><a href="javascript:void(0)">Traveling</a></p>
  </div>
 </div>
 <div class="three_packages clearfix">
  <div class="start_pack_one">
   <h4>Genders Needed</h4>
      
    <ul class="">
  
    <li><a href="javascript:void(0)">{!! $row->genders_needed !!}</a></li>
      
   </ul>
  </div>
  <div class="start_pack_one">
   <h4>Experiences Needed</h4>
   <ul class="">
  
      @foreach($row->experience_needed as $ids)
    <li><a href="javascript:void(0)">{!! $ids->name !!}</a></li>
       @endforeach      
   </ul>
   
  </div>
  <div class="start_pack_one">
   <h4>Categories Needed</h4>
   <ul class="">
     @foreach($row->categories_needed as $id)
      <li><a href="javascript:void(0)">@if($row->categories_needed)  {{$id->name}} @else - @endif</a></li>
     @endforeach
    
   </ul>
  </div>
 </div>
 @if(\SiteHelpers::checkLogged() )

 <div class="contact_this_agent clearfix">
  <a href="javascript:void(0);" id="composeMessage"><span>Contact This Agent</span></a> 

 </div>
 @endif
 <div class="section_seven">
 <h1>Most Recent Registered Models</h1>
 <p>The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p>
 <ul class="instagramer">
  <li class="wow SlideInLeft">
  <img src="{{ asset('images/insta_one.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/insta_two.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_three.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/insta_four.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_five.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href="javascript:void(0)"><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
 </ul>
</div>
</div>

<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="max-width:800px;width:90%;">
    <div class="modal-content">
    <div class="modal-header bg-default">
      
      <button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-titles">Compose Message </h4>
    </div>
    <div class="load_notify"></div>
    <div class="modal-body" id="compose-modal-content">
       {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
        <input type="hidden" name="typee" class="type" />
        <div class="form-group">
          <label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
          <div class="col-xs-10">
            <div>
              <input type='hidden' name='recipient' value='{!! $row->id !!}' class='form-control' >{!! $row->username !!}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
          <div class="col-xs-10">
            <div>{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control','id' => 'subject')) !!}
</div>
          <span class="err_subject" style="color:red"></span>
          </div>
        </div>
        <div class="form-group">
          <label for="mail_subject" class="col-xs-2">Message</label>
          <div class="col-xs-10">
            <div><textarea name='message' rows='3' style="width:100%;" class='form-control' placeholder="Enter your message"></textarea></div>
            <span class="error" style="color:red"></span>
          </div>
        </div>
        <div class="form-group">
        <label for="mail_subject" class="col-xs-2">Attach files</label>
        <div class="col-xs-10">
        <input  type="file" class="form-control" name="images[]" id='images' placeholder="" multiple>
       
        [Allow multiple image] 
        </div>
        </div>
        <div class="sending_loadergs"></div>
        <div class="form-group">
          <label class="col-xs-2"></label>
          <div class="col-xs-10">
            {!! Form::hidden('draft', '0' )  !!}
            {!! Form::hidden('messageid', '' )  !!}
            <button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i>Send message</button>
<!--             <button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
 -->          </div>
        </div>
       {!! Form::close() !!}
    </div>

    </div>
  </div>
</div>
@include('footer_map')
<script>

 $(document).ready(function(){

  
  
  $(function(){
    tinymce.init({  
      mode : "specific_textareas",
      editor_selector : "mceEditor",
      theme : "advanced",
      theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
     });  
  });
  

  function tinyClean(value) {
  value = value.replace(/&nbsp;/ig, ' ');
  value = value.replace(/\s\s+/g, ' ');
  if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
    value = '';
  }
  return value;
}

   $(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       
       recipient1:{
      required:true
      },
      subject:{
      required:true
      },
      message:{
      required:true
      },
        
      },
      messages: {
       recipient1:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Recipient Field is required</span>',
      },
      subject:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Subject Field is required</span>',
      },
      message:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Message Field is required</span>',
      }
       
      },
      
        submitHandler: function(form) {

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
    var subject = $('[name="subject"]').val();
     if(subject==''){
      $('.err_subject').html("Subject Field is required");
    }
    else{
      $('.err_subject').hide();

    }
    if(value==''){
      $('.error').html("Message Field is required");
    }else{
      $('.error').hide();
    }
    
    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();
        toastr.success("Message sent Successfully");
         $('#compose-modal').modal("toggle"); 
         form.submit();
}
           
        },

        });
  

  $('#composeMessage').click(function(){
    
    $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
 


  

}); 



  </script>

  <script type="text/javascript">
        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function(value, text, event) {

                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');

                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {
                        
                        var split_id = el_id.split("_");

                        var postid = split_id[1];  // postid
                        var post_type = $('input[name=post_type]').val();// posttype
                        // AJAX Request
                        $.ajax({
                            url: '{{url("rating_ajax")}}',
                            type: 'post',
                            data: {post_type:post_type,postid:postid,rating:value},
                            dataType: 'json',
                            success: function(data){
                              // alert(data)
                             if (data.status == 'error') {

                                          // Show Error Notification
                                          toastr["error"](data.msg)
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": true,
                                            "progressBar": false,
                                            "positionClass": "toast-top-full-width",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "3000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "linear",
                                            "hideEasing": "swing",
                                            "showMethod": "slideDown"
                                          }
                                        }else{
                                          
                                          // Show success Notification
                                          toastr["success"](data.msg)
                                          toastr.options = {
                                            "closeButton": false,
                                            "debug": false,
                                            "newestOnTop": true,
                                            "progressBar": false,
                                            "positionClass": "toast-top-full-width",
                                            "preventDuplicates": false,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "3000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "linear",
                                            "hideEasing": "swing",
                                            "showMethod": "slideDown"
                                          }
                                          
                                        }
            
          
                                
                            }
                        });
                    }
                }
            });
        });
      
 </script>
<!-- Set rating -->
<script type='text/javascript'>
$(document).ready(function(){
$('#rating_{{$row->id}}').barrating('set','{{$star_rating}}');
});

</script>