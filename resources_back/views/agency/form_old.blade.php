@extends('layouts.app')



@section('content')

      <script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>



  <div class="page-content row">

    <!-- Page header -->



 

 	<div class="page-content-wrapper m-t">





<div class="sbox">

	<div class="sbox-title"> 

		<div class="sbox-tools pull-left" >

			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 

		</div>
		<h1>Agency Management</h1>
		<div class="sbox-tools " >

			@if(Session::get('gid') ==1)

				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->

			@endif 			

		</div> 



	</div>

	<div class="sbox-content"> 	



		<ul class="parsley-error-list">

			@foreach($errors->all() as $error)

				<li>{{ $error }}</li>

			@endforeach

		</ul>	



		 {!! Form::open(array('url'=>'agency/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

<div class="col-md-12">

<fieldset>

									

	{!! Form::hidden('id', $row['id']) !!}					



	<div class="form-group  " >

	<label for="Username" class=" control-label col-md-4 text-left"> Username </label>

	<div class="col-md-7">

	<input  type='text' name='username' id='username' value='{{ $row['username'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Email" class=" control-label col-md-4 text-left"> Email </label>

	<div class="col-md-7">

	<input  type='email' name='email' id='email' value='{{ $row['email'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Agency Name" class=" control-label col-md-4 text-left"> Agency Name </label>

	<div class="col-md-7">

	<input  type='text' name='agency_name' id='agency_name' value='{{ $row['agency_name'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Agency Description" class=" control-label col-md-4 text-left"> Agency Description </label>

	<div class="col-md-7">

	<textarea name='agency_description' rows='5' id='agency_description' class='form-control' required>{{ $row['agency_description'] }}</textarea> 

	</div> 

	</div>



	<div class="form-group  " >

	<label for="Mobile" class=" control-label col-md-4 text-left"> Mobile </label>

	<div class="col-md-7">

	<input  type='text' name='mobile' id='mobile' value='{{ $row['mobile'] }}' class='form-control ' required/> 

	</div> 

	</div> 					



	<div class="form-group  " >

	<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo </label>

	<div class="col-md-7">

	<input  type='file' name='profile_photo' id='profile_photo' @if($row['profile_photo'] =='') class='required' @endif style='width:200px !important;'  /><br>					 	<div >

	{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/agency_profile/') !!}

	</div>					

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Agency Banner" class=" control-label col-md-4 text-left"> Agency Banner </label>

	<div class="col-md-7">

	<input  type='file' name='agency_banner' id='agency_banner' @if($row['agency_banner'] =='') class='required' @endif style='width:200px !important;'  />

	<br>

	<div >

	{!! SiteHelpers::showUploadedFile($row['agency_banner'],'/uploads/agency_banner/') !!}

	</div>					

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Business Name" class=" control-label col-md-4 text-left"> Business Name </label>

	<div class="col-md-7">

	<input  type='text' name='business_name' id='business_name' value='{{ $row['business_name'] }}' class='form-control ' required/> 

	</div> 

	</div>



	<div class="form-group  " >

	<label for="Business Description" class=" control-label col-md-4 text-left"> Business Description </label>

	<div class="col-md-7">

	<input  type='text' name='business_description' id='business_description' value='{{ $row['business_description'] }}' class='form-control ' required/> 

	</div> 

	</div> 	



	<div class="form-group  " >

	<label for="Website" class=" control-label col-md-4 text-left"> Website </label>

	<div class="col-md-7">

	<input  type='url' name='website' id='website' value='{{ $row['website'] }}' class='form-control ' required/> 

	</div> 

	</div> 		



	<div class="form-group  " >

	<label for="Business Biography" class=" control-label col-md-4 text-left"> Business Biography </label>

	<div class="col-md-7">

	<textarea name='business_biography' rows='5' id='business_biography' class='form-control '  required>{{ $row['business_biography'] }}</textarea> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Looking For Desc" class=" control-label col-md-4 text-left"> Looking For Desc </label>

	<div class="col-md-7">

	<textarea name='looking_for_desc' rows='5' id='looking_for_desc' class='form-control '  required>{{ $row['looking_for_desc'] }}</textarea> 

	</div> 

	</div> 	



	<div class="form-group  " >

	<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number </label>

	<div class="col-md-7">

	<input  type='text' name='contact_number' id='contact_number' value='{{ $row['contact_number'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Membership" class=" control-label col-md-4 text-left"> Membership </label>

	<div class="col-md-7">

	<input  type='text' name='membership' id='membership' value='{{ $row['membership'] }}' class='form-control ' required/> 

	</div> 

	</div>



	<div class="form-group  " >

	<label for="Membership Expiration" class=" control-label col-md-4 text-left"> Membership Expiration </label>

	<div class="col-md-7">

	<div class="input-group m-b" >

	{!! Form::text('membership_expiration', $row['membership_expiration'],array('class'=>'form-control date','id'=>'membership_expiration','required'=>'')) !!}

	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

	</div>

	</div> 

	</div>



	<div class="form-group  " >

	<label for="Membership Id" class=" control-label col-md-4 text-left"> Membership Id </label>

	<div class="col-md-7">

	<select name='membership_id' rows='5' id='membership_id'  class='select2' >

	<option value="">-- Select ID --</option>

	</select> 

	</div> 

	</div> 	



	<div class="form-group  " >

	<label for="Rate" class=" control-label col-md-4 text-left"> Rate </label>

	<div class="col-md-7">

	<input  type='text' name='rate' id='rate' value='{{ $row['rate'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Views" class=" control-label col-md-4 text-left"> Views </label>

	<div class="col-md-7">

	<input  type='text' name='views' id='views' value='{{ $row['views'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Address" class=" control-label col-md-4 text-left"> Address </label>

	<div class="col-md-7">

	<textarea name='address' rows='5' id='address' class='form-control '  required>{{ $row['address'] }}</textarea> 

	</div> 

	</div> 	



	<div class="form-group  " >

	<label for="City" class=" control-label col-md-4 text-left"> City </label>

	<div class="col-md-7">

	<input  type='text' name='city' id='city' value='{{ $row['city'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="State" class=" control-label col-md-4 text-left"> State </label>

	<div class="col-md-7">

	<input  type='text' name='state' id='state' value='{{ $row['state'] }}' class='form-control ' required/> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Country" class=" control-label col-md-4 text-left"> Country </label>

	<div class="col-md-7">

	<select name='country' rows='5' id='country'  class='select2' required>

	<option value="">-- Select Country --</option>

	@foreach($country as $val)

	<option  value ="{{ $val->id }}" @if($row['country'] == $val->id)  selected='selected' @endif> {{ $val->country_name }}</option> 						

	@endforeach

	</select> 

	</div> 

	</div> 	



	<div class="form-group  " >

	<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode </label>

	<div class="col-md-7">

	<input  type='text' name='zipcode' id='zipcode' value='{{ $row['zipcode'] }}' class='form-control ' required/> 

	</div> 

	</div>



	<div class="form-group  " >

	<label for="Verification Status" class=" control-label col-md-4 text-left"> Verification Status </label>

	<div class="col-md-7">

	<label class='radio radio-inline'>

	<input type='radio' name='verification_status' value ='0' required @if($row['verification_status'] == '0') checked="checked" @endif > Inactive </label>

	<label class='radio radio-inline'>

	<input type='radio' name='verification_status' value ='1' required @if($row['verification_status'] == '1') checked="checked" @endif > Active </label> 

	</div> 

	</div> 



	<div class="form-group  " >

	<label for="Status" class=" control-label col-md-4 text-left"> Status </label>

	<div class="col-md-7">

	<label class='radio radio-inline'>

	<input type='radio' name='status' value ='0' required @if($row['status'] == '0') checked="checked" @endif > Inactive </label>

	<label class='radio radio-inline'>

	<input type='radio' name='status' value ='1' required @if($row['status'] == '1') checked="checked" @endif > Active </label> 

	</div> 

	</div> 		 					



</fieldset>

</div>

			

			



		

	<div style="clear:both"></div>	

		

		<div class="form-group">

			<label class="col-sm-4 text-right">&nbsp;</label>

			<div class="col-sm-8">	

			<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>

			<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>

			<button type="button" onclick="location.href='{{ URL::to('agency?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>

			</div>	  

	

		  </div> 

		 

		 {!! Form::close() !!}

	</div>

</div>		 

</div>	

</div>		



   <script type="text/javascript">

   

	$(document).ready(function() { 

		$('.removeMultiFiles').on('click',function(){

			var removeUrl = '{{ url("agency/removefiles?file=")}}'+$(this).attr('url');

			$(this).parent().remove();

			$.get(removeUrl,function(response){});

			$(this).parent('div').empty();	

			return false;

		});		

	});



	$(function(){

	

	tinyMCE.init({

    theme : "advanced",

    mode: "exact",

    elements : "business_biography,looking_for_desc",

    theme_advanced_toolbar_location : "top",

    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"

    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"

    + "bullist,numlist,outdent,indent",

    theme_advanced_buttons2 : "link,unlink,anchor,separator,"

    +"undo,redo,cleanup,code,separator,sub,sup,charmap",

    theme_advanced_buttons3 : "",

    height:"200px",

    width:"485px"

	});

	})



	</script>		 

@stop