@include('agent_dash_header')
@foreach($videos as $video)
<div class="search_video col-md-3 col-sm-4 col-xs-6">
				<div class="thumb_img_blk">
<div class="video_thumb">
					<a href="{{ url('/video') }}/{{ $video->video_id }}">
					<div class="tags_video_thumbimg">
						<img src="{{ $video->thumb }}" alt="" class="tags_video_img">
						<div class="thumb_play_icon">
						<i class="fa fa-play"></i></div>
						<span class="video_time">{{ $video->duration }}</span>
					</div>
						
					</a>
				</div>
</div>
</div>

@endforeach
<div class="load_more_btn text-center col-md-12 col-sm-12 col-xs-12">
<div class="pagination_content"> 
{!! str_replace('/?', '?', $videos->appends(Input::except('page'))->render()) !!}
</div>
</div>
@include('footer_map')