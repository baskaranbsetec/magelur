<?php
use App\Library\Markdown;
?>
<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->

<div class="magelur_about faq_sec">
<section>
   	<div class="container-fluid magelur_header">
   		<div class="magelur_agent_event_banner">
			<div class="video-container">
				<div id="either-gif-or-video">
				            <!-- <video class="embed-responsive-item fillWidth" poster="images/landing_page.jpg" autobuffer loop autoplay playsinline controls >
				            	<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" >
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" >	
				            </video> -->
				</div>
				<div class="event_banner_title">
				    <h1>Crave - Event</h1>				        	
				</div>
			/div>
		</div>
   </div>
</section>
<section class="model_agent_event">
			<div class="container">
				<div class="row">
					<div class="model_agent_container" style="">


						<div class="col-md-4 col-sm-12">
							<div class="agent_event_list_image">
								<img src="../images/agent_event1.jpg" class="img-responsive">
							</div>
							<div class="agent_event_list">
								<h3><a href="javascript:void(0)">Chic Women- Summer Collection</a></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu  ullamcorper ligula finibus ac.</p>
								<p>Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
								<span>6:30  PM |  New  York</span>
							</div>
						</div>



						<div class="col-md-4 col-sm-12">
							<div class="agent_event_list_image">
								<img src="../images/agent_event2.jpg" class="img-responsive">
							</div>
							<div class="agent_event_list">
								<h3><a href="javascript:void(0)">Chic Women- Summer Collection</a></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu  ullamcorper ligula finibus ac.</p>
								<p>Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
								<span>6:30  PM |  New  York</span>
							</div>
						</div>

						<div class="col-md-4 col-sm-12">
							<div class="agent_event_list_image">
								<img src="../images/agent_event3.jpg" class="img-responsive">
							</div>
							<div class="agent_event_list">
								<h3><a href="javascript:void(0)">Chic Women- Summer Collection</a></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu  ullamcorper ligula finibus ac.</p>
								<p>Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
								<span>6:30  PM |  New  York</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
				<section class="agent_news_event">

			

			<div class="container event_news_container">

			<div class="news_event">
				<p>event news</p>
			</div>
				<div class="col-md-12 col-sm-12 event_news_blk">

					@foreach ($rowData as $row)
					<div class="event-unit">
					<div class="col-md-7 col-sm-12 event_news">
						<div class="event_news_image">
							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
							<img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
							</a>
						</div>
					</div>

					<div class="col-md-5 col-sm-12 event_news">
						<div class="event_news_content">

						<a class="event-link" href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
							<h1>{{ $row->title }}</h1>
						</a>	

							<?php

							$content = explode('<hr>', $row->note);
							if(count($content>=1))
							{
								echo '<p>'. PostHelpers::formatContent($content[0]). '</p>'; 
							} else {
								//echo PostHelpers::formatContent($row->note);
							}
							?>
							
							<span>{{ date(" dS M y h:i  A" , strtotime($row->created)) }}</span>
							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">Read more</a>
						</div>
					</div>
					</div>
					@endforeach

				</div>
			</div>

			<div class="pagination_content text-center"> {!! $pagination->render() !!}</div>

		</section>
</div>