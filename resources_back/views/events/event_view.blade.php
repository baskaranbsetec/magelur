<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="{{ url('post')}}"> Posts </a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->
<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
  <script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script> -->
  <!-- start rating plugin -->
  <script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
  <link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>
  <script src="{{ asset('magelur/js/star_rating.js')}}"></script>

<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
          <div class="banner_title wow bounce">
            <h1>{{ Lang::get('core.event') }}</h1>
              <ul class="link_list">
                <li class="active"><a href="{{asset('/')}}">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">EVENT</a></li>
              </ul>
          </div>
    </div>
   </div>
  </section>
  <div class="event_book">
 <div class="container">
  <div class="clearfix">
   <div class="col-sm-3">
    <div class="maistartes"> 
    <div class="mnth_date">
     <h1> {{ date("j " , strtotime($row->startdate)) }}  </h1> 
     <h3> {{ date("M" , strtotime($row->startdate)) }}  </h3>
     <p> {{ date("Y " , strtotime($row->startdate)) }}  </p>
    </div>
    <div class="start_dates">
     <h5>{{ Lang::get('core.start') }}</h5>
     <p>{{ date("F d , Y " , strtotime($row->startdate)) }} <span>{{ date("h:i A" , strtotime($row->startdate)) }}</span></p>
    </div>
    <div class="end_dates">
     <h5>{{ Lang::get('core.end') }}</h5>
     <p>{{ date("F d , Y " , strtotime($row->enddate)) }} <span>{{ date("h:i A" , strtotime($row->enddate)) }}</span></p>
    </div>
    <div class="price_form">
     <!--  <h5>Price From</h5>
     <p>$83</p> -->
     <div class="inbook"><a href="{{ $row->event_link }}">{{ Lang::get('core.check_link') }}</a></div>
     @if(\Session::get('agent_id') == $row->ownerid || \Session::get('user_type') == 'admin'  )
     <div class="inbook agent_edit_btn"><a href="{{ url('events/update/'.$row->id.'/')}}"><i class="fa fa-pencil">
     </i>{{ Lang::get('core.edit_event') }}</a></div>
     @endif

   </div>
   <!-- <div class="">
    <div class="post-action">
    
      <h2>{{$avg_ratings}}</h2>
      <select class='ratings' id='rating_'  data-id='rating_'>
        <option value="1" >1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>
        <option value="4" >4</option>
        <option value="5" >5</option>
      </select>

    </div>
    <script type="text/javascript">
    setRating($('.ratings'),'{{$avg_ratings}}');
    </script>
      </div> -->
    </div>
  </div>
  <div class="col-sm-9">
    <div class="matenfine clearfix event_video_view">
      <div class="full_mates">
       <h1>{{ $row->title }}</h1>
       <p>{{ $row->city }} | {{ $row->country }}</p>
     </div>
     <div class="share">
      <h4>{{ Lang::get('core.share') }}</h4>
      <ul class="icones">
        <!-- fb share -->
        <li class="fbook">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('events/view/'.$row->id.'/'.$row->uri)}}&t={{ url('events/view/'.$row->id.'/'.$row->uri)}}"
            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
            target="_blank" title="Share on Facebook">
          </a>
        </li>


        <!-- google plus share -->
        <li class="gpluser">
          <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://plus.google.com/share?url={{ url('events/view/'.$row->id.'/'.$row->uri)}}"></a>
        </li>

        <li class="twittera">
          <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/share?url={{ url('events/view/'.$row->id.'/'.$row->uri)}}"></a>
        </li>
        <!-- <li class="instagra"><a href="http://pinterest.com/pin/create/button/?url={{ url('events/view/'.$row->id.'/'.$row->uri)}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" ></a></li> -->
      </ul> 

    </div>
    <div class="event_rating_link">
    <div class="post-action">
      <!-- Rating -->
      
      <select class='ratings' id='rating_'  data-id='rating_'>
        <option value="1" >1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>
        <option value="4" >4</option>
        <option value="5" >5</option>
      </select>
      <h2>{{$avg_ratings}}</h2>
    </div>
    <script type="text/javascript">
    // set rating for each star
    setRating($('.ratings'),'{{$avg_ratings}}');
    </script>
      </div>
  </div>
  <div class="banter">
    <a href="javascript:void(0);">
      @if(is_file('uploads/events/'.$row->image))
      <img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
      @else
      <img src="{{ asset('/images/thumb.png') }}" class="img-responsive">
      @endif
    </a>
  </div> 
  <div class="smcnt">
    <div class="desc">
      <p>{{ $row->note }}</p>
    </div>
  </div> 
  <div class="udfix clearfix">
    <div class="col-sm-3">
     <h3>{{ Lang::get('core.location') }}</h3>
     <p>{{$row->address }}</p>
      <p>{{$row->city }}</p>
      <p>{{$row->zipcode }}</p>
      <p>{{$row->state }}</p>
      <p>{{$row->country }}</p>

   </div> 
   <div class="col-sm-9">
     <h3>{{ Lang::get('core.organizer') }}</h3>
     <p>{{ $row->mobile }}  | {{ $row->email }} | <a href="{{ $row->event_link }}"> {{ $row->event_link }} </a> </p>
   </div> 
 </div>
</div>
</div>

<div class="event_map">
<!--     <a href="javascript:void(0)">
      <img src="../../../images/events_map.jpg">
    </a> -->
    <input type="hidden" name="geocomplete" id="geocomplete" />
    <div class="event_map_canvas"></div>
  </div>

  <!-- comments -->
  <div class="event_comments_blk">

    @if($conpost['commsys'] ==1)
    <div class="comments">
      <!--    <h3> Comment(s) </h3> -->
      <div class="note">
        @if(Session::has('messagetext'))    
        {!! Session::get('messagetext') !!}
        @endif  
        <!-- {!! PostHelpers::formatContent($row->note) !!} -->
      </div>
      @foreach($comments as $comm)
      @if(!empty($comm->username))
      <div class="comment_blk1">
      <div class="event_comment_star">
        <select class="not_rated" data-rate="{{$comm->rating}}" id="comment_id_{{ $comm->commentID }}">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        </div>

        <script type="text/javascript">
        setRating($('#comment_id_{{ $comm->commentID }}'),'{{$comm->rating}}');
        $(function() {
          $('.not_rated').barrating({
            theme: 'fontawesome-stars',
          });
        });
        </script>
        <h1>{{ ucwords($comm->username) }}  

          @if(!empty($comm->city) && !empty($comm->country))    
          |  {{ $comm->city }}  ,  {!! SiteHelpers::getCountry($comm->country) !!} 
          @endif

        </h1>
<!--       <div class="rating_blk">
      <ul class="starer">
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
        </ul>
      </div> -->
      <div class="content" style="display:block">
        <p>{!! PostHelpers::formatContent($comm->comments) !!}</p>
        <div class="tools" style="display:none">
          @if(Session::get('gid') == '1' OR $comm->userID == Session::get('uid')) 
          <a href="{{ url('events/remove/'.$row->id.'/'. $row->uri.'/'.$comm->commentID) }}"><i class="fa fa-minus-circle"></i> {{ Lang::get('core.btn_remove') }}  </a>
          @endif
        </div>
      </div>
    </div>
    @endif


    <div class="info" style="display:none;">
      @if(!empty($comm->avatar))  
      <div class="avatar" >
        <?php if( file_exists( './uploads/users/'.$comm->avatar) && $comm->avatar !='') { ?>
        <img src="{{ URL::to('uploads/users').'/'.$comm->avatar }} " border="0" width="40" class="img-circle" />
        <?php  } else { ?> 
        <img alt="" src="http://www.gravatar.com/avatar/{{ md5($comm->email) }}" width="40" class="img-circle" />
        <?php } ?> 
      </div>
      @endif

      <!--  {{ date("M j, Y " , strtotime($comm->posted)) }} -->
      
    </div>
    

    @endforeach
    <div class="comment_text_area">
      {{--*/ $bLogged=\SiteHelpers::checkLogged() /*--}}  

      @if(!empty($bLogged))
      <form method="post" id='SubmitComments' action="{{ url('events/comment') }}" parsley-validate  class="form">
      <div class="event_right_rating">
       <select id="comment_rating" style='display:none'>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
      </div>

      <input type="hidden" name="rating" value="1" />  

      <textarea minlength="5" rows="5" placeholder="{{ Lang::get('core.comment_place') }}" required name="comments" id='commentBox'></textarea>

      <div class="comment_submit_btn">    
        <button type="submit" id='postComment'><span>{{ Lang::get('core.submit_comment') }}</span></button>  
      </div>
      <input type="hidden" name="pageID" value="{{ $row->id }}" />  
      <input type="hidden" name="alias" value="{{ $row->uri}}" />            
    </form>
    @else
    <!-- show login button -->
    <div class="header_menu_list">
      <h3>You need to be member to post comments</h3>
      <p>To add a comments for above post you need to make a login.If you are a existing user login into the site and add your comments.If you didn't had an account please click on the below signup button to create your account   </p>
      <div class="signupsagent signupsagent_blog">
       <a href="{{ url('model/create') }}">{{ Lang::get('core.am_model') }}</a>
       <a href="{{ url('agent/create') }}">{{ Lang::get('core.am_agent') }}</a>
     </div>
   </div>

   @endif

 </div>

</div>
@endif
</div>

<!-- upcoming events -->
<div class="upcomingevents">
  <div class="upcmng">
    <h3>{{ Lang::get('core.up_events') }}</h3>
  </div>

  @if(!empty($recentposts))

  <div class="upmainevent clearfix">
 
  <!-- show remaining events -->

  <div class="srez_twos">
    <!-- <ul class="serias">
      @foreach($recentposts as $iKey => $post )
      @if($iKey > 0)
      <li><div class="upcomingevent_list"><a href="{{ url('events/view/'.$post->id.'/'.$post->uri)}}"><img src="{{ asset('uploads/events/'.$post->image) }}"></a>
        <div class="upcomingevent_hover">
          <div class="upcominevent_hvr_blk">
           <div class="readmore_button_div">    
            <a href="{{ url('events/view/'.$post->id.'/'.$post->uri)}}">{{ Lang::get('core.read_more') }}</a>
          </div>
        </div>
      </div>


    </a></li>
    @endif
    @endforeach
  </ul> -->
</div>

</div> 
@endif
</div>  



</div>
</div>
<!-- <div class="with_frnds">
 <img src="../../../images/event_detail_mini_banner_one.jpg">
  <div class="your_frnds">
    <h3>{{ Lang::get('core.share_exp') }}</h3>
    <p>{{ Lang::get('core.with_frnd') }}</p>
  </div>  
  <div class="learned event_details_learned"><a href="javascript:void(0)"><span>{{ Lang::get('core.learn_more') }}</span></a></div>
</div>  -->

</div>


<script type="text/javascript">
var options = {
  map: ".event_map_canvas",
  location: '{{ $row->zipcode }}',
  // details: "form",          
  // types: ["geocode", "establishment"],
};
$("#geocomplete").geocomplete(options);
$(document).ready(function(){
  $('.map').hide();

});
</script>

<script type="text/javascript">
$(function() {

  $('#comment_rating ').barrating({
    theme: 'fontawesome-stars',
    onSelect: function(value, text, event) {
      var rate = $( "#comment_rating option:selected" ).val();
      $('input[name="rating"]').val(rate);
    }
  });
  $('.ratings').barrating({
    theme: 'fontawesome-stars',
  });

  $('form#SubmitComments div.br-widget').hide();
  $("#commentBox").click(function() {
   // $('form#SubmitComments div.br-widget a').removeClass('br-selected br-current');
   $('form#SubmitComments div.br-widget').show();

  });
});
</script>