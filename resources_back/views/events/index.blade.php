@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
	<h1>Events Management</h1>




<!-- 		<div class="sbox-tools" >
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('events/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i></a>
			@endif

			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('module/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 
		</div> -->
	</div>

	<div class="sbox-content"> 
		<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('events/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create </a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete </a>
			@endif 
			<a href="{{ URL::to( 'events/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}" style="display:none"><i class="fa  fa-search"></i> </a>				


		</div>
	

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'events/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped eventstable-view">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
							@if($t['label']=='Tags')
							<th><span >{{ $t['label'] ==''}}</span></th>
							@elseif($t['label']=='Ownerid')
							<th><span >Owner</span></th>
							@elseif($t['label']=='Note')
							<th><span >Description</span></th>	
							@else
							<th><span>{{ $t['label'] }}</span></th>	
							@endif 		
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
				<th ><span>Message</span></th>
			  </tr>
        </thead>

        <tbody>  
        	<tr id="orderDetails">
        		<td class="number"></td>
        		<td></td>
        		<td id="title" class="fieldsearch"> <div class="select-style">{!! Form::text('title',"{$title}",array('class'=>'form-control', 'placeholder'=>'Name', )) !!} </div></td>
        		<td id="note" class="fieldsearch"> <div class="select-style">{!! Form::text('note',"{$note}",array('class'=>'form-control', 'placeholder'=>'Description', )) !!} </div></td>
        		<td id="status" class="fieldsearch"> <div class="select-style">
        			<select name='status' class = 'select2 '>
        				<option value="">Select option</option>
        				<option value="Active" {{ $status == 'Active' ? 'selected' : ''}} >Active</option>
        				<option value="Inactive" {{ $status == 'Inactive' ? 'selected' : ''}} >Inactive</option>
        				<option value="deleted" {{ $status == 'Inactive' ? 'deleted' : ''}} >Deleted</option>

        			</select>

        			 </div>
        		</td>
        			<td id="startdate" class="fieldsearch"> <div class="select-style">{!! Form::text('startdate',"{$startdate}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Startdate', )) !!} </div></td>
        			<td id="enddate" class="fieldsearch"> <div class="select-style">{!! Form::text('enddate',"{$enddate}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Enddate', )) !!} </div></td>
        			<td></td>
        			<td id="categories" class="fieldsearch"> <div class="select-style">	
        				{!! Form::select('categories',array(''=>'Select Category')+\SiteHelpers::getTableValues('category') ,"{$categories}",array('class' => 'select2 ','id' => 'categories',)) !!}	
        			</div></td>
        			<td id="owner" class="fieldsearch"> <div class="select-style">{!! Form::text('owner',"{$owner}",array('class'=>'form-control', 'placeholder'=>'Owner', )) !!} </div></td>
        			<td></td>
        			<td class="icons">
        				<input type="hidden"  value="Search">
        				<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
        				<a href="{{ url('events') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
        			</td>
        		</tr>    						
                      {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
				@foreach($model_filter as $row)
                <tr>
     				<td class="new_entry" hidden>{{ $row->new }}</td>
					<td width="30"> {{$i }}  </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('events/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('events/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->
				{{--*/ $i++ /*--}}
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>
							@if($field['field']=='tags')	
							{{--*/ $field['field']==''/*--}}
							@elseif($field['field']=='note')
							{{---*/ $content = substr( $row->note,0,100 ) /*---}}
							{!! $content !!}
							@elseif($field['field']=='status')
							@if( $row->status == 'Active')
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@elseif( $row->status == 'deleted')
							<span class="label label-danger">Deleted</span>
							@else
							<span class="label label-warning">{{ Lang::get('core.fr_minactive') }}</span>

							@endif

							@elseif($field['field']=='image')
							@if($row->image !='')
							<a href="{{ asset('uploads/events/'.$row->image) }}" target="_blank" class="previewImage">
							<img src="{{ asset('uploads/events/'.$row->image) }}" border="0" width="50" class="img-circle">
							</a>
							@else
							<img src="{{ asset('/uploads/images/no-image.png') }}" border="0" width="35" class="img-circle">
							@endif
							@elseif($field['field']=='categories')
							{{--*/ $idz=\SiteHelpers::getcolumnvalue('category',$row->categories) /*--}}
							@foreach($idz as $id)
							<ul>
							<li>@if($row->categories) {{$id->name}} @endif</li>
							</ul>
							@endforeach
							@elseif($field['field']=='ownerid')
							{{--*/ $idz=\SiteHelpers::getName($row->usertype,$row->ownerid) /*--}}

							@if(!empty($idz)) 
							 {{ ucfirst($idz) }} 
							 @endif

							@else	
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}	
						 	@endif	
						 </td>
						@endif	
					 @endif					 
				 @endforeach
				  <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('events/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('events/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif

					</td>
					<td><a  class="tips btn btn-xs btn-white" data-toggle="modal" data-target="#myModal" title="Send Mail"  onclick="getmailid('{{ $row->email }}');"><i class="fa fa-envelope "></i></a></td>	 		 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	<!-- The Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:black">Send Mail</h4>
      </div>
      <div class="modal-body">
       <form role="form" method="get" id="reused_form" action="agentmail/events">

            <div class="row">
                <div class="col-sm-12 form-group">
                	<label for="subject" style="color:black">
                        Subject:</label>
                     <textarea class="form-control" type="textarea" name="subject" id="subject" maxlength="200" rows="1" style="background-color:white;color:black"></textarea>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="message" style="color:black">
                        Message:</label>
                    <textarea class="form-control" type="textarea" name="message" id="message" maxlength="6000" rows="7" style="background-color:white;color:black"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                         From:</label>
                    <input type="email" class="form-control" id="fromemail" name="fromemail" style="background-color:white;color:black" value = "{{ CNF_EMAIL }}" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                        To:</label>
                    <input type="email" class="form-control" id="email" name="email" style="background-color:white;color:black" required>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-white pull-right" >Send →</button>
                </div>
            </div>

        </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
			  <option value="title"> Title </option>
			  <option value="note"> Description </option>
			  <option value="status"> Status </option>
			  <option value="startdate"> Startdate </option>
			  <option value="enddate"> Enddate </option>
			  <option value="categories"> Categories </option>
			  <option value="ownerid"> Ownerid </option>
 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
		</p>		
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
@endif
	  </div>
	</div>
	</div>
</div>	
	</div>	  
</div>	
<script type="text/javascript">
var texts= $("tbody tr td.new_entry").map(function() {
	return $(this).val();         
}).get();
if(jQuery.inArray("1", texts) == -1) {
	jQuery.each($('tbody tr td.new_entry'), function () {
		if (this.textContent == "1") {
			$(this).parent().addClass("red");
		}
	});

} else {
	console.log("is NOT in array");
} 

$("#reused_form").validate({
			ignore: "not:hidden",
			rules: {
				message:{
					required:true,
				},
				subject:{
					required:true,
				},
		}
	});

	function getmailid(mailid)
 {
 	// alert(mailid);
 	$('#email').val(mailid);
 }
 $('.pagination_blk').hide();

 $( document ).ready(function() {
    
$("#startdate").change(function() {
   
	$(".datetimepicker").hide();
});

$("#enddate").change(function() {
   
	$(".datetimepicker").hide();
});

});


</script>	

@stop