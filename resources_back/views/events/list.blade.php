<?php
use App\Library\Markdown;
?>


<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<div class="magelur_about faq_sec">
<section>
   	<div class="container-fluid magelur_header">
   		<div class="">
			<div class="video-container">
				<div id="either-gif-or-video">
					<video  id="eventVideo" class="embed-responsive-item fillWidth" autoplay="autoplay"  autobuffer loop  playsinline controls >
						<source class="video-src" src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" media="all and (max-width:480px)">
						<source class="video-src" src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" media="all and (max-width:480px)">
						<source class="video-src" src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" >
					</video>
				</div>
				<div class="event_banner_title">
				    <h1>{{ Lang::get('core.carve_events') }}</h1>				        	
				</div>
			</div>
		</div>
   </div>
</section>
<section class="model_agent_event">
			<div class="container">
				<div class="row">
					<div class="model_agent_container col-md-12" style="">

					<div class="event_list_carousel owl-carousel">
					@foreach($recentposts as $iKey => $sPost)	
					<div class="item" data-id="{{ $sPost->id }}" data-video="{{ asset('uploads/events/videos/'.$sPost->video.'/video.'.$sPost->video_ext)}}">
						<div class="col-md-6 col-sm-12 col-xs-12 ">
							<div class="agent_event_list_image">
								<a href="{{ url('events/view/'.$sPost->id.'/'.$sPost->uri)}}">
								@if(is_file('uploads/events/'.$sPost->image))
								<img src="{{ asset('uploads/events/'.$sPost->image) }}" class="img-responsive">
								@else
								<img src="{{ asset('/images/thumb.png') }}" class="img-responsive">
								@endif
								<!-- <img src="{{ asset('uploads/events/'.$sPost->image) }}" class="img-responsive"> -->
								</a>
							</div>							
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="agent_event_list">
								<h3><a href="{{ url('events/view/'.$sPost->id.'/'.$sPost->uri)}}">{{ $sPost->title }}</a></h3>

								@if(strlen($sPost->note) > 600)
								<p>{{ mb_substr($sPost->note,0,600) }}....</p>
								@else
								<p>{{ $sPost->note }}</p>
								@endif

								<div id="getting-started-{{ $sPost->id }}"></div>
								<script type="text/javascript">
									$('#getting-started-{{ $sPost->id }}').countdown('{!! date("Y/m/d", strtotime($sPost->startdate)) !!}', function(event) {
										$(this).html(event.strftime('<div class="timercount_blk"><div class="timer_blk"><div class="timer_inner_div"><h2>%d</h2><h5>Days</h5></div> <div class="timer_inner_div"><h2>%H</h2><h5>Hours</h5></div> <div class="timer_inner_div"><h2>%M</h2><h5>Minutes</h5></div> <div class="timer_inner_div"><h2>%S</h2><h5>Seconds</h5></div></div></div'));
									});
								</script>
								
								<span>New  York</span>
								
							</div>
						</div>
					</div>
					@endforeach
					</div>
					</div>
				</div>
			</div>
		</section>
				<section class="agent_news_event">

			

			<div class="container event_news_container">

			<div class="news_event">
				<p>{{ Lang::get('core.event_news') }}</p>
			</div>
				<div class="col-md-12 col-sm-12 col-xs-12 event_news_blk">
					@foreach ($rowData as $row)
					<div class="event-unit">
					<div class="col-md-7 col-sm-12 col-xs-12 event_news">
						<div class="event_news_image">
							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
								@if(is_file('uploads/events/'.$row->image))
								<img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
								@else
								<img src="{{ asset('/images/thumb.png') }}" class="img-responsive">
								@endif
<!-- 							<img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
 -->							</a>
						</div>
					</div>

					<div class="col-md-5 col-sm-12 col-xs-12 event_news">
						<div class="event_news_content">

						<a class="event-link" href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
							<h1>{{ $row->title }}</h1>
						</a>	
						@if(strlen($row->note) > 780)
						<p>{{ mb_substr($row->note,0,600) }}....</p>
						@else
						<p>{{ $row->note }}</p>
						@endif
							
							<span>{{ date("F d , Y " , strtotime($row->created)) }}{{ date("h:i A" , strtotime($row->created)) }}<!-- {{ date(" dS M y h:i  A" , strtotime($row->created)) }} --></span>
							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">{{ Lang::get('core.read_more') }}</a>
						</div>
					</div>
					</div>
					@endforeach

				</div>
			</div>

			<div class="pagination_content text-center"> {!! $pagination->render() !!}</div>

		</section>
</div>



    <script>
      var owl = $('.event_list_carousel');
      owl.owlCarousel({
      	margin: 0,
      	loop: true,
      	nav: true,
      	dots:false,
      	autoplay: true,
      	navigation : true,
      	navText: ["<p><i class='fa fa-angle-left'></p>", "<p><i class='fa fa-angle-right'></p>"],
      	autoplayTimeout:1000, 
      	autoplayHoverPause:true,
      	autoplaySpeed:1000,
      	responsive: { 
      		0: {
      			items: 1
      		},
      		480: {
      			items: 1
      		},       
      		1200: {
      			items: 1
      		}
      	}
      });

      // onchange event will trigger , once slider moved
      owl.on('changed.owl.carousel', function(property) {
      	//  get the video src element
      	var vid = document.getElementById("eventVideo");
      	// pause the video
      	vid.autoplay = false;


      	// remove attr
      	$('.video-src').removeAttr('src');

      	var current = property.item.index;
      	var src     = $(property.target).find(".owl-item").eq(current).find('.item').attr('data-id');
      	var video   = $(property.target).find(".owl-item").eq(current).find('.item').attr('data-video');

      	// trigger next video
      	changeSource(video);

      });

      function changeSource(url) {
      	var video = document.getElementById('eventVideo');
      	video.src = url;
      	video.play();
      }
      // hide video controllers
        var vidctrl = document.getElementById("eventVideo");
      	vidctrl.controls = false;
      	// swal("All Your Events will be Visible only after Admin approves your Event");
    </script>

    <style>
    .map{
    	display:none;
    }
    </style>