@include('agent_dash_header')
<!-- selectbox CSS -->
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">
<!-- Tags CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<!-- datepicker CSS  -->
<link href="{{ asset('magelur/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/css/datetimepicker.css')}}" rel="stylesheet">


<ul class="parsley-error-list">
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>


@if(\Session::get('model_id'))
<div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">
<div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">
<div class="empty_vertical">
<div class="empty_msg_details clearfix">
  <div class="empty_msg_blk clearfix">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>Access Denied!</h1>
        <h2>
          Model Can't able to access this page
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="{{asset('images/file_image1.png')}}" alt="file_image" />
        </div>
      </div>
    </div>
    <!-- <p>You can search on the bar below or return to home page</p> -->
    <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
      </div>
      <div class="msg_next_page">

        <a href="{{ url('model-dashboard/'.\Session::get('model_id')) }}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@else


{!! Form::open(array('url'=>'events/save?return='.$return, 'class'=>'form-horizontal','id'=>'events','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}


<section class="create_event_form">
  <div class="container">
   <div class="mainlog" style='display:none;cursor: auto;'>
     <a href="javascript:void(0)" style='cursor: auto;'>{{ Lang::get('core.create_new_event') }}</a>
   </div>

   <!-- hidden fields -->
   <input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' class='form-control ' /> 
   <input  type='hidden' name='post_id' id='post_id' value='{{ $row['id'] }}' class='form-control ' />   
   <!-- get lat & longitute -->
   <input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
   <input type="hidden" name="lng" value="{{ $row['longitude'] }}" />            
      <input name="status" placeholder="Event name" type="hidden" value="{{ $row['status'] }}">

   <div class="col-md-12 col-sm-12">
    <div class="inputslogin">
      <input name="title" placeholder="Event name" type="text" value="{{ $row['title'] }}">
    </div>

    <div class="inputslogin">
      <p>{{ Lang::get('core.description') }}</p>
      <textarea name="note" cols=""  minlength="20" rows="10" class="form-control">{{ $row['note'] }}</textarea>
    </div>

    <div class="inputslogin">
      <input name="email" placeholder="{{ Lang::get('core.email') }}" type="text" value='{{ $row["email"] }}'>
    </div>
  </div>

  <div class="">
   <div class="col-md-6 col-sm-12">
     <div class="inputslogin">
       <input name="mobile" placeholder="{{ Lang::get('core.contact_num') }}" type="text" value='{{ $row["mobile"] }}'>
     </div>
   </div>

   <div class="col-md-6 col-sm-12">
     <div class="inputslogin">
       @if(empty($row['startdate']))
      {{--*/ $startdate = $row['startdate'] /*--}}
      @else
      {{--*/ $startdate = date("F d ,Y h:i", strtotime($row['startdate'])) /*--}}
      @endif
      {!! Form::text('startdate',$startdate,array('id'=>'startdate','class'=>'form-control ','placeholder' => 'Start Date','autocomplete'=>"off")) !!}
       <!-- <input name="enteryourusername" placeholder="Start Date" type="text"> -->
     </div>
   </div>

   

 </div>

<div class=" col-md-12 no_padding_div ">
 <div class="col-md-6 col-sm-12">
  <div class="choose_box  @if(!empty($row['image'] )) image_selected @endif ">
     
      @if(!empty($row['image'] ))
      <a href="javascript:void(0)" class="hide_img_preview" onclick="$('#uploadBtn').val(null);$('.event_preview_img').hide();$(this).hide();$('.choose_box').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>
      <img  src="{{ asset('uploads/events/'.$row['image']) }}" class="event_preview_img">
      @else
      <a href="javascript:void(0)" class="hide_img_preview" style="display: none;" onclick="$('#uploadBtn').val(null);$('.event_preview_img').hide();$(this).hide();$('.choose_box').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>
      <img style="display: none;" src="{{ asset('uploads/images/no-image.png')}}" class="event_preview_img">
      @endif
    
    <div class="group">      
     <div class="fileUpload">

      <label for="uploadBtn" class="label_file" style="cursor: pointer;">
       <h2>{{ Lang::get('core.cover_banner') }}</h2>
       <p>1280x750 Png,Jpeg,Jpg</p>
       <span class="file_browse"><i class="fa fa-upload"></i>
          
      </label>
              <input id="uploadBtn" name="image" accept=".jpg, .jpeg, .png" class="upload" value="" type="file"></span>

    </div>
  </div>  
	
</div>
</div>

<div class="col-md-6 col-sm-12">
     <div class="inputslogin">
      @if(empty($row['enddate']))
      {{--*/ $enddate = $row['enddate'] /*--}}
      @else
      {{--*/ $enddate = date("F d ,Y h:i", strtotime($row['enddate'])) /*--}}
      @endif
      {!! Form::text('enddate',$enddate,array('id'=>'enddate','class'=>'','placeholder' => 'End Date','autocomplete'=>"off")) !!}
       <!-- <input name="enteryourusername" placeholder="Start Date" type="text"> -->
     </div>
   </div>
<div class="col-md-6 col-sm-12">

 <div class="inputslogin">
   <input name="link" placeholder="{{ Lang::get('core.event_link') }}" type="text" value="{{ $row['event_link'] }}">
 </div>
</div>

<div class="events_map_form col-md-12 col-md-12 col-xs-12">
  <div class="col-md-6 event_form_fields">
    <div class="events_label_blk col-md-12 col-sm-12">
 <!--      <div class="col-md-6 col-sm-12 col-xs-12 form-group  ">
        <label for="Enddate" class=" control-label text-left"> Enddate </label>
      </div> -->
      <!-- <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input id="geocomplete" type="text" placeholder="{{ Lang::get('core.address_place') }}" class="form-control" value="{{ $row['address'] }}" />
      </div> -->

   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Address</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="formatted_address" type="text" placeholder="{{ Lang::get('core.address') }}" class="form-control" value="{{ $row['address'] }}">
      </div>
  <!--     <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">City</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="locality" type="text" placeholder="{{ Lang::get('core.city') }}" class="form-control" value="{{ $row['city'] }}">
      </div>  
  <!--     <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Zip Code</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="postal_code" type="text" placeholder="{{ Lang::get('core.zip') }}" class="form-control" value="{{ $row['zipcode'] }}">
      </div>
   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">State</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="administrative_area_level_1" placeholder="{{ Lang::get('core.state') }}" class="form-control" type="text" value="{{ $row['state'] }}">
      </div>
   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Country</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="country" type="text" placeholder="{{ Lang::get('core.country') }}" class="form-control" value="{{ $row['country'] }}">  
      </div>
    </div>
  </div>
  <div class="col-md-6 col-xs-12 col-sm-12 map_blk">
    
<!--     <div class="input-group m-b" style="width:100% !important;">
      <div class="map_canvas" style="width: 100%;height: 280px;"></div>
    </div> -->
    <div class="playvideo_div  col-md-12 col-xs-12 col-sm-12">

            <div class="videoContainer ">

            @if(empty($row["video"]))
            <video class="video-preview event_preview_vd" controls style="display:none;">
              <source src="mov_bbb.mp4" id="video_here">
                Your browser does not support HTML5 video.
            </video>
            @else
              <input type="hidden" name="video_id" value="{{ $row['video'] }}" />
              <input type="hidden" name="video_ext" value="{{ $row['video_ext'] }}" />
            <video class="video-preview event_preview_vd" controls>
                <source src="{{ asset('uploads/events/videos/'.$row['video'].'/video.'.$row['video_ext'])}}" id="video_here" >
                  Your browser does not support HTML5 video.
            </video>  
            @endif 


            <span class="video_upload_icon" ><i class="fa fa-video-camera"></i>
            </span>  
            <div class="video_formats_blk">  
            <h2>Video Upload</h2> 
            <h4>1300*300 MP4, x-m4v</h4> 
            <input type="file" name="video" id='video' class='file_video' accept="video/mp4,video/x-m4v,video/*">

            </div>

            </div>

         
          </div>
         
          
          <div class="choose_box_vd  @if(!empty($row['video'] )) image_selected @endif ">
          @if(!empty($row['video'] ))
          <a href="javascript:void(0)" class="hide_vd_preview" onclick="$('#video').val(null); 
$('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected');$('.file_video').css('display','block'); "><i class="fa fa-trash"></i></a> 
          @else
          <a href="javascript:void(0)" class="hide_vd_preview" style="display: none;" onclick="$('#video').val(null);$('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected'); $('.file_video').css('display','block');"><i class="fa fa-trash"></i></a>
          @endif
          </div> 

          
  </div>
</div>
<!-- event Tags -->       
<div class="event_tags_blk col-md-6 col-sm-12 col-xs-12">
  <div class="form-group  " >
    <!-- <label for="Tags" class=" control-label col-md-12 text-left"> Tags </label> -->
    <div class="col-md-12 event_tags_input">
      <input name='tags' id='tags'  class='form-control tags' placeholder="Add a tags" value="{{ $row['tags'] }}" />
    </div> 
  </div>
  <div class="form-group  " >
    <div class="inputslogin">
      <div class="col-md-12 col-sm-12 col-xs-12 theme_nopadding_div">
        <input type='text' name='entrycost' id='entrycost' class='form-control' placeholder="Entry Cost" value="{{ $row['entrycost'] }}" />
      </div> 
    </div>
  </div>

</div>
<div class="col-md-6 col-sm-12 col-xs-12 agent_categories_blk ">
  <!-- event Categories -->           
  <div class="form-group  " >
    <!-- <label for="Categories" class=" control-label col-md-6 text-left"> Categories </label> -->
    <div class="col-md-12 categories_select_div">
    {!! Form::select('categories[]',\SiteHelpers::getTableValues('category') ,explode(',',$row['categories']),array('class' => 'select2 h' ,'id' => 'categories','multiple'=>'multiple')) !!} 
    </div> 
  </div>  
  <div class="form-group  " >
    <div class="inputslogin">
      <div class="col-md-12 col-sm-12 col-xs-12 theme_nopadding_div">
        <input type='url' name='get_ticket' id='get_ticket' class='form-control' placeholder="Get Ticket" value="{{ $row['get_tickets'] }}" />
      </div> 
    </div>
  </div>
  <!-- image file -->   
<!--             <div class="form-group  " >
              <label for="Image" class=" control-label col-md-6 text-left"> Image </label>
              
              <div class="col-md-6">
                <input type="file"  name="image" />
              </div> 
              <div class="col-md-6">
                <div class="preview-events event_form_img">
                  <img src="{{ asset('uploads/images/no-image.png')}}" border="0" width="50" height="50" class="img-circle">
                </div>
              </div>
            </div>  --> 
            <!-- video embed link -->
            <div class="form-group  " style="margin:0;">
              <!-- <label for="Image" class=" control-label col-md-6 text-left"> Video Embed </label> -->
              <!-- <div class="col-md-12 categories_select_div">
                <input type="input" name="video" value="{{ $row['video'] }}" placeholder="{{ Lang::get('core.video_embed') }} " class="form-control" />
              </div>  -->
              <div class="col-md-1">
              </div>
            </div>               
          </div>
           
            <!-- Need models with following fields -->
            <div class="col-md-6 col-sm-12 col-xs-12 agent_need_models_radio">
            <div class="form-group form-group_full " >
              <label for="Need Models" class=" control-label col-md-4 col-sm-4 col-xs-12 text-left">  Need Models </label>
              <div class="col-md-8 col-sm-9 col-xs-12  event_upload_radio">
              <label class=' agree radio_button_div'>
              {!! Form::radio('need_models','yes',($row['need_model']=="yes") ? true:false,array()) !!} Yes 
              <span class="checkmark"></span></label> 
              <label class='agree radio_button_div'>
              {!! Form::radio('need_models','no',($row['need_model']=="no") ? true:false,array()) !!} No 
              <span class="checkmark"></span></label>
              </label>     
              </div> 
            </div> 

            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 agent_need_models_radio">
            <div class="form-group  form-group_full" >
              <label for="publishID" class=" control-label col-md-4 text-left">publishID </label>
              <div class="col-md-7 theme_select nopadding_right">
              {!! Form::select('publishID',array(''=>'Select publishID')+\SiteHelpers::getEventFieldCategories('publishID'), $row['publishID'], array('class'=>'form-control publishID' ) ) !!}             
              </div> 
              <div class="col-md-1">
              </div>
            </div>
          </div>
            <div class="col-md-12 col-sm-12 col-xs-12 agent_need_models">
          <div class='models_types' style='display:none'>   
            <div class="form-group form-group_full" >
              <label for="Talent Needed" class="control-label col-md-6 text-left"> Talent Needed </label>
              <div class="col-md-6 theme_select nopadding_right">
              {!! Form::select('talent_needed',array(''=>'Select Talent Needed')+\SiteHelpers::getEventCategories('talent_needed'),$row['talent_needed'], array('class'=>'form-control talent_needed') ) !!}
              </div> 
            </div>
            <div class="form-group form-group_full" >
              <label for="Travel Package" class="control-label col-md-6 text-left"> Travel Package </label>
              <div class="col-md-6 theme_select nopadding_right">
              {!! Form::select('travel_package',array(''=>'Select Travel Package')+\SiteHelpers::getEventCategories('travel_package'), $row['travel_package'], array('class'=>'form-control travel_package' ) ) !!}
              </div> 
            </div>
            <div class="form-group form-group_full" >
              <label for="Accommodation Package" class="control-label col-md-6 text-left"> Accommodation Package </label>
              <div class="col-md-6 theme_select nopadding_right">
              {!! Form::select('accomm_package',array(''=>'Select Accommodation Package')+\SiteHelpers::getEventCategories('accomm_package'), $row['accomm_package'], array('class'=>'form-control accomm_package') ) !!}
              </div> 
            </div>  
            <div class="form-group form-group_full" >
              <label for="Benefit Package" class="control-label col-md-6 text-left"> Benefit Package </label>
              <div class="col-md-6 theme_select nopadding_right">
              {!! Form::select('benefit_package',array(''=>'Select Benefit Package')+\SiteHelpers::getEventCategories('benefit_package'), $row['benefit_package'], array('class'=>'form-control benefit_package' ) ) !!}
              </div> 
            </div>
            <div class="form-group  form-group_full" >
              <label for="Dress code" class=" control-label col-md-6 text-left"> Dress code </label>
             
              <div class="col-md-6 nopadding_right">
              <input type='text' name='dress_code' id='dress_code' placeholder='Add dress code' class='form-control dress_code' value="{{$row['dresscode']}}" />
              <div id="errorToShow"></div>
              </div> 

              <div class="col-md-1">
              </div>
            </div>

            
            </div>
            

          </div>
          <!-- End Need model -->    


          <div class="playvideo_blk col-md-12 col-sm-12 col-xs-12"> 
          <div class="no_left_space col-md-6 col-sm-6 col-xs-12">                                
            
          </div>

          <div class="play_video_blk nopadding_div col-md-12 col-sm-12 col-xs-12">
          <div class="terms_cond_blk">
          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' </p>
            <label class="agree">Terms and conditions
                <!-- <input type="checkbox" name='agree'> -->
<input type='checkbox' name='agree' value ='1' required @if($row['agree'] == '1') checked="checked" @endif > 

                <span class="checkmark"></span>
              </label>
          </div>
          <div class="comment_submit_btn">
            <button type="submit" class='sub_eve'><span>{{ Lang::get('core.sb_submit') }}</span></button>  
          </div>                  
          </div> 
          </div>
         

        </div>


      </div>
    </section>
    {!! Form::close() !!}      
@endif
<!-- Show start date alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- datepicker JS  -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- selectbox JS -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js') }}"></script>
<!-- Tags JS -->
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<!-- Google Map Js -->
<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script> -->
 
     
  <script type="text/javascript">
$(document).ready(function(){

    $("#startdate").datetimepicker({
        autoclose: true,
        startDate: '+1d'
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#enddate').datetimepicker('setStartDate', minDate);
        $(this).valid();
    });

    $("#enddate").datetimepicker({
        autoclose: true,
    }).on('changeDate', function (selected) {
          var minDate =$("#startdate").val();
          if(minDate=='')
          {
            swal("Please choose startdate");
            $("#enddate").val('').datetimepicker('setDate', null);
          }            
          else{
            var maxDate = new Date(selected.date.valueOf());
            $('#startdate').datetimepicker('setEndDate', maxDate);
            $(this).valid();
          }
        });

});
    $(document).ready(function() {
  $('div.categories_select_div').css('width', '');

    // $('.datetime').datetimepicker({
    //   format: 'yyyy-mm-dd hh:ii:ss',
    //   useCurrent: false,
    //   autoclose: true,
    //   keepOpen: false,
    //   }); 


        
     $(".select2").select2({
      width:"100%",
      placeholder: "Select Categories:",
    });  
     var id     = '{{ $row["id"] }}';
     if(id == ''){
            $('.mainlog').show();

     }
     else {
      $('.mainlog').hide();

     }

          // set selected values 
          var categories     = '{{ $row["categories"] }}';
          if(categories){
            var selectedValues = categories.split(',');
            $("select#categories").select2('val',selectedValues); 
          }
 

      $('.removeMultiFiles').on('click',function(){
        var removeUrl = '{{ url("events/removefiles?file=")}}'+$(this).attr('url');
        $(this).parent().remove();
        $.get(removeUrl,function(response){});
        $(this).parent('div').empty();  
        return false;
      }); 

      //  tag plugin
      $('input[name="tags"]').tagsInput({
        width:'auto',
      });   
      $('input[name="dress_code"]').tagsInput({
        width:'auto'
      }); 


      var formid = $('form#events').find('input[name="id"]').val();     
      // if(!formid){
      //   // render map application
      //   $("#geocomplete").geocomplete({
      //     map: ".map_canvas",
      //     details: "form",
      //     types: ["geocode", "establishment"],
      //   }); 
      // }


      if(formid > 0){
        var options = {
          map: ".map_canvas",
          location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
          details: "form",          
          types: ["geocode", "establishment"],
        };
        $("#geocomplete").geocomplete(options);
      }

      var country = '{{ $row["country"] }}';  
      var city    = '{{ $row["city"] }}'; 
      var zipcode = '{{ $row["zipcode"] }}';  
      var state   = '{{ $row["state"] }}';  
      var address = '{{ $row["address"] }}';  

      // set the country fields
      if(!country){
        $('form#events').find('input[name="country"]').val(country);
      } 
      // set the city fields
      if(!city){
        $('form#events').find('input[name="city"]').val(city);
      } 
      // set the zipcode fields
      if(!zipcode){
        $('form#events').find('input[name="zipcode"]').val(zipcode);
      }
      // set the state fields
      if(!state){
        $('form#events').find('input[name="state"]').val(state);
      } 
      // set the address fields
      if(!address){
        $('form#events').find('input[name="formatted_address"]').val(address);
      }                           

    });

    // show preview image
    $(document).on('change','input[name="image"]',function(){
      var ext = $("input[name='image']").val().split('.').pop().toLowerCase();

        if($.inArray(ext, ['png','jpeg','jpg']) == -1) {
        swal("Invalid extension!");

        $("input[name='image']").val(null); 
        } 
      readURL(this);
    })
    // render image
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.event_preview_img').attr('src', e.target.result).show();
        }
        $('.choose_box').addClass('image_selected');
        $('.hide_img_preview').show();
        reader.readAsDataURL(input.files[0]);
      }
    }

    // show video preview
    $(document).on("change", "input[name='video']", function(evt) {
      // add class to parent div
      // $('.playvideo_div').addClass('video_choosed');
      // var $source = $('#video_here');
      // $('.video-preview').show(); 
      // $source[0].src = URL.createObjectURL(this.files[0]);
      // $source.parent()[0].load();
        var ext = $("input[name='video']").val().split('.').pop().toLowerCase();

        if($.inArray(ext, ['mp4','x-m4v']) == -1) {
        swal("Invalid extension!");

        $("input[name='video']").val(null); 
        } 
        readvideoURL(this);

    });  
    // render video
    function readvideoURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.event_preview_vd').attr('src', e.target.result).show();
          $("input[name='video']").css('display','none');
        }
        $('.choose_box_vd').addClass('image_selected');
        $('.hide_vd_preview').show();

        reader.readAsDataURL(input.files[0]);
      }
    }  
var val= $("input[name='video_id']").val();
if(typeof(val)  === "undefined") {
}
else{

$("input[name='video']").css('display','none');
$("input[name='video']").removeClass('error');
}
   $('#events').on('click',function(){
         if($('#dress_code_tagsinput span.tag').text() != '')
         {
          $('#errorToShow').hide()
         }
         else{
          $('#errorToShow').show()

         }
          if($('#tags_tagsinput span.tag').text() != '')
         {
        $(this).valid();

         }
         
 
      }); 

    $("#events").validate({
      ignore: "not:hidden",
      rules: {
        agree:{
          required:true,
        },
        title:{
          required:true,
        },
        note:{
          required:true,
        },
        labels:{
          required:true,
        },
        startdate:{
          required:true,
        },
        enddate:{
          required:true,
        },
        link:{
          required:true,
          url: true
        },
        /*image:{
          required:true,
        },*/
        email:{
          required:true,
          email: true
        },
        geocomplete:{
          required:true,
        },
        // video:{
        //   required:true,
        // },
        formatted_address:{
          required:true,
        },
        locality:{
          required:true,
        },
        postal_code:{
          required:true,
        },
        administrative_area_level_1:{
          required:true,
        },
        country:{
          required:true,
        },
        mobile:{
          required:true,
          number:true,
          minlength:10,
        },
        entrycost:{
          required:true,
          number:true,
        },
        need_models:{
          required:true,
        },
        get_ticket:{
          required:true,
          url:true
        },
        'categories[]':{
          required:true,
        },
        tags:{
          required:true,
        },

        talent_needed: {
              required: {
                  depends: function (element) {
                      return $("input[name='need_models'][value='yes']").is(":checked");
                  }
              }
            },
            accomm_package: {
              required: {
                  depends: function (element) {
                      return $("input[name='need_models'][value='yes']").is(":checked");
                  }
              }
            },
            travel_package: {
              required: {
                  depends: function (element) {
                      return $("input[name='need_models'][value='yes']").is(":checked");
                  }
              }
            },
            benefit_package: {
              required: {
                  depends: function (element) {
                      return $("input[name='need_models'][value='yes']").is(":checked");
                  }
              }
            },
            dress_code: {
              required: {
                  depends: function (element) {
                      return $("input[name='need_models'][value='yes']").is(":checked");
                  }
              }
            },
            publishID: {
              required:true,
              }
            
        

      },
       
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            if(elem.hasClass('h')) {
                elem.siblings('.h').find('.select2-choice').removeClass(errorClass);
            } else {
                elem.removeClass(errorClass);
            }

        },
   
    
   
      messages: {
        'categories[]':{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Categories"
        },
        tags:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Tags"
        },
         agree:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Terms and Conditions"
        },
        title:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Title"
        },
        note:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Description"
        },
        labels:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Tags"
        },
        startdate:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event StartDate"
        },
        enddate:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event EventDate"
        },
        link:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Website",
          url:"<span><i class='fa fa-warning'></i></span>Please Provide valid URL"  
        },
        image:{
         required:"Please Event Banner Image"
        },
        email:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Contact Email",
          email:"<span><i class='fa fa-warning'></i></span>Please enter a valid email address"
        },
        geocomplete:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose an Event Location"
        },
        video:{
          required:"Please Choose an Event Video"
        },
        formatted_address:{
          required:"<span><i class='fa fa-warning'></i></span>Event Address required"
        },  
        locality:{
          required:"<span><i class='fa fa-warning'></i></span>Event Locality required"
        },  
        postal_code:{
          required:"<span><i class='fa fa-warning'></i></span>Event Postal Code is required"
        },  
        administrative_area_level_1:{
          required:"<span><i class='fa fa-warning'></i></span>Event State is Required"
        },  
        country:{
          required:"<span><i class='fa fa-warning'></i></span>Event Country is required"
        },  
        mobile:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Contact Number",
          number:"<span><i class='fa fa-warning'></i></span>Please Provide Valid Contact Number"
        },
         need_models:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Option",
        },
        get_ticket:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Ticket URL",
          url:"<span><i class='fa fa-warning'></i></span>Please Provide valid URL" 
        },
        talent_needed:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Talent Needed",
        },
        travel_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Travel Package",
        },
        benefit_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Benefit Package",
        },
        dress_code:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Dress Code",
        },
         publishID:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Publishes",
        },
        accomm_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Accommodation Package",
        }, 
        entrycost:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Entry cost",
          number:"<span><i class='fa fa-warning'></i></span>Please Provide Cost in Number"
        },
      },
       errorPlacement: function(error, element) {
        if (element.attr("name") == "dress_code") {
            error.appendTo("#errorToShow");
        }
       
         else {
            error.insertAfter(element);
        }
        },
        
        
      submitHandler:function(form){
        

        form.submit();
      }
    }); 

 $('select').on('change', function() {
        $(this).valid();
    });
  $(function(){
    var max = parseInt($("#note").attr("maxlength"));
    $("#count").text("Characters left: " + max);
    $("#note").keyup(function(e){
      $("#count").text("Characters left: " + (max - $(this).val().length));
      if($(this).val().length==max)
        $("#msg").text("500 words allowed");
      else
        $("#msg").text("");
    });
  }); 

var val = $("input[name='need_models']:checked").val();
if($("input:radio[name='need_models']").is(":checked") && val=='yes') {
$('.models_types').show();
}

  $('input[name="need_models"]').on('click', function(event){
      // get the selected value
      var Val = $.trim($(this).val());
      if(Val == 'yes'){
        $('.models_types').show();
      }else{
        $(".talent_needed").val('');
        $(".travel_package").val('');
        $(".benefit_package").val('');
        $(".dress_code").val('');
        $(".accomm_package").val('');
        $(".publishID").val('');
        $('.models_types').hide();
      }
    });



  </script>    


  <style>
  .map{
    display:none;
  }
  </style>

