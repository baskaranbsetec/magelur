@include('agent_dash_header')
<!-- selectbox CSS -->
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">
<!-- Tags CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<!-- datepicker CSS  -->
<link href="{{ asset('magelur/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

<ul class="parsley-error-list">
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>


@if(\Session::get('model_id'))
<div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">
<div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">

<div class="empty_msg_details">
  <div class="empty_msg_blk">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>Access Denied!</h1>
        <h2>
          Model Can't able to access this page
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="{{asset('images/file_image1.png')}}" alt="file_image" />
        </div>
      </div>
    </div>
    <!-- <p>You can search on the bar below or return to home page</p> -->
    <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
      </div>
      <div class="msg_next_page">

        <a href="{{ url('model-dashboard/'.\Session::get('model_id')) }}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
@else


{!! Form::open(array('url'=>'events/save?return='.$return, 'class'=>'form-horizontal','id'=>'events','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}


<section class="create_event_form">
  <div class="container">
   <div class="mainlog">
     <a href="javascript:void(0)">{{ Lang::get('core.create_new_event') }}</a>
   </div>

   <!-- hidden fields -->
   <input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' class='form-control ' /> 
   <input  type='hidden' name='post_id' id='post_id' value='{{ $row['id'] }}' class='form-control ' />   
   <!-- get lat & longitute -->
   <input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
   <input type="hidden" name="lng" value="{{ $row['longitude'] }}" />            

   <div class="col-md-12 col-sm-12">
    <div class="inputslogin">
      <input name="title" placeholder="Event name" type="text" value="{{ $row['title'] }}">
    </div>

    <div class="inputslogin">
      <p>{{ Lang::get('core.details') }}</p>
      <textarea name="note" cols="" rows="10" class="form-control">{{ $row['note'] }}</textarea>
    </div>

    <div class="inputslogin">
      <input name="email" placeholder="{{ Lang::get('core.email') }}" type="text" value='{{ $row["email"] }}'>
    </div>
  </div>

  <div class="">
   <div class="col-md-6 col-sm-12">
     <div class="inputslogin">
       <input name="mobile" placeholder="{{ Lang::get('core.contact_num') }}" type="text" value='{{ $row["mobile"] }}'>
     </div>
   </div>

   <div class="col-md-6 col-sm-12">
     <div class="inputslogin">
      {!! Form::text('startdate', $row['startdate'],array('class'=>'datetime','class'=>'form-control datetime','placeholder' => 'Start Date')) !!}
       <!-- <input name="enteryourusername" placeholder="Start Date" type="text"> -->
     </div>
   </div>

   

 </div>

<div class=" col-md-12 no_padding_div ">
 <div class="col-md-6 col-sm-12">
  <div class="choose_box  @if(!empty($row['image'] )) image_selected @endif ">
     
      @if(!empty($row['image'] ))
      <a href="javascript:void(0)" class="hide_img_preview" onclick="$('.event_preview_img').hide();$(this).hide();$('.choose_box').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>
      <img  src="{{ asset('uploads/events/'.$row['image']) }}" class="event_preview_img">
      @else
      <a href="javascript:void(0)" class="hide_img_preview" style="display: none;" onclick="$('.event_preview_img').hide();$(this).hide();$('.choose_box').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>
      <img style="display: none;" src="{{ asset('uploads/images/no-image.png')}}" class="event_preview_img">
      @endif
    
    <div class="group">      
     <div class="fileUpload">

      <label for="uploadBtn" class="label_file" style="cursor: pointer;">
       <h2>{{ Lang::get('core.cover_banner') }}</h2>
       <p>1280x750 Png,Jpeg</p>
       <span class="file_browse"><i class="fa fa-upload"></i>
        <input id="uploadBtn" name="image" accept=".jpg, .jpeg, .png" class="upload" value="" type="file"></span>
          
      </label>
    </div>
  </div>  	
</div>
</div>

<div class="col-md-6 col-sm-12">
     <div class="inputslogin">
      {!! Form::text('enddate', $row['enddate'],array('class'=>'datetime','placeholder' => 'End Date')) !!}
       <!-- <input name="enteryourusername" placeholder="Start Date" type="text"> -->
     </div>
   </div>
<div class="col-md-6 col-sm-12">

 <div class="inputslogin">
   <input name="link" placeholder="{{ Lang::get('core.event_link') }}" type="text" value="{{ $row['event_link'] }}">
 </div>
</div>

<div class="events_map_form col-md-12">
  <div class="col-md-6 event_form_fields">
    <div class="events_label_blk col-md-12">
 <!--      <div class="col-md-6 col-sm-12 col-xs-12 form-group  ">
        <label for="Enddate" class=" control-label text-left"> Enddate </label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input id="geocomplete" type="text" placeholder="{{ Lang::get('core.address_place') }}" class="form-control" value="{{ $row['address'] }}" />
      </div>

   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Address</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="formatted_address" type="text" placeholder="{{ Lang::get('core.address') }}" class="form-control" value="{{ $row['address'] }}">
      </div>
  <!--     <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">City</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="locality" type="text" placeholder="{{ Lang::get('core.city') }}" class="form-control" value="{{ $row['city'] }}">
      </div>  
  <!--     <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Zip Code</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="postal_code" type="text" placeholder="{{ Lang::get('core.zip') }}" class="form-control" value="{{ $row['zipcode'] }}">
      </div>
   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">State</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="administrative_area_level_1" placeholder="{{ Lang::get('core.state') }}" class="form-control" type="text" value="{{ $row['state'] }}">
      </div>
   <!--    <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
        <label class=" control-label text-left">Country</label>
      </div> -->
      <div class="col-md-12 col-sm-12 col-xs-12 form-group  event_input_field_blk">
        <input name="country" type="text" placeholder="{{ Lang::get('core.country') }}" class="form-control" value="{{ $row['country'] }}">  
      </div>
    </div>
  </div>
  <div class="col-md-6 map_blk">
    <div class="input-group m-b" style="width:100% !important;">
      <div class="map_canvas" style="width: 100%;height: 280px;"></div>
    </div>
  </div>
</div>
<!-- event Tags -->       
<div class="event_tags_blk col-md-6">
  <div class="form-group  " >
    <!-- <label for="Tags" class=" control-label col-md-12 text-left"> Tags </label> -->
    <div class="col-md-12 event_tags_input">
      <input name='tags' id='tags' class='form-control' palceholder="{{ Lang::get('core.tags') }}" value="{{ $row['tags'] }}" />
    </div> 
  </div>

</div>
<div class="col-md-6 agent_categories_blk ">
  <!-- event Categories -->           
  <div class="form-group  " >
    <!-- <label for="Categories" class=" control-label col-md-6 text-left"> Categories </label> -->
    <div class="col-md-12 categories_select_div">
    {!! Form::select('categories[]',\SiteHelpers::getTableValues('category') ,explode(',',$row['categories']),array('class' => 'select2 ' ,'id' => 'categories','multiple'=>'multiple')) !!} 
    </div> 
  </div>  
  <!-- image file -->   
<!--             <div class="form-group  " >
              <label for="Image" class=" control-label col-md-6 text-left"> Image </label>
              
              <div class="col-md-6">
                <input type="file"  name="image" />
              </div> 
              <div class="col-md-6">
                <div class="preview-events event_form_img">
                  <img src="{{ asset('uploads/images/no-image.png')}}" border="0" width="50" height="50" class="img-circle">
                </div>
              </div>
            </div>  --> 
            <!-- video embed link -->
            <div class="form-group  " >
              <!-- <label for="Image" class=" control-label col-md-6 text-left"> Video Embed </label> -->
              <!-- <div class="col-md-12 categories_select_div">
                <input type="input" name="video" value="{{ $row['video'] }}" placeholder="{{ Lang::get('core.video_embed') }} " class="form-control" />
              </div>  -->
              <div class="col-md-1">
              </div>
            </div>               
          </div>


          <div class="playvideo_blk col-md-12 col-sm-12 col-xs-12"> 
          <div class="no_left_space col-md-6 col-sm-6 col-xs-12">                                   
          <div class="playvideo_div  col-md-12 col-xs-12 col-sm-12">
            <input type="file" name="video" accept="video/mp4,video/x-m4v,video/*">

            <div class="videoContainer ">

            @if(empty($row["video"]))
            <video class="video-preview event_preview_vd" controls style="display:none;">
              <source src="mov_bbb.mp4" id="video_here">
                Your browser does not support HTML5 video.
            </video>
            @else
              <input type="hidden" name="video_id" value="{{ $row['video'] }}" />
              <input type="hidden" name="video_ext" value="{{ $row['video_ext'] }}" />
            <video class="video-preview event_preview_vd" controls>
                <source src="{{ asset('uploads/events/videos/'.$row['video'].'/video.'.$row['video_ext'])}}" id="video_here">
                  Your browser does not support HTML5 video.
            </video>  
            @endif 


            <span class="video_upload_icon" ><i class="fa fa-video-camera"></i>
            </span>  
            <div class="video_formats_blk">  
            <h2>Video Upload</h2> 
            <h4>1300*300 MP4,WMV,FLV</h4> 
            </div>

            </div>
         
          </div>
          <div class="choose_box_vd  @if(!empty($row['video'] )) image_selected @endif ">
          @if(!empty($row['video'] ))
          <div class="video_close_btn">
          <a href="javascript:void(0)" class="hide_vd_preview" onclick="$('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>  </div>

          @else
          <a href="javascript:void(0)" class="hide_vd_preview" style="display: none;" onclick="$('.event_preview_vd').hide();$(this).hide();$('.choose_box_vd').removeClass('image_selected'); "><i class="fa fa-trash"></i></a>

          @endif
          </div>          
          </div>

          <div class="play_video_blk col-md-6 col-sm-6 col-xs-12">
          <div class="terms_cond_blk">
          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' </p>
            <label class="agree">Terms and conditions
                <input type="checkbox">
                <span class="checkmark"></span>
              </label>
          </div>
          <div class="comment_submit_btn">
            <button type="submit"><span>{{ Lang::get('core.sb_submit') }}</span></button>  
          </div>                  
          </div> 
          </div>


          

        </div>


      </div>
    </section>
    {!! Form::close() !!}      
@endif
<!-- datepicker JS  -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- selectbox JS -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js') }}"></script>
<!-- Tags JS -->
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<!-- Google Map Js -->
<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script> -->



  <script type="text/javascript">
    $(document).ready(function() {

    $('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
     $(".select2").select2({
      width:"100%",
      placeholder: "Select Categories:",
    });  

          // set selected values 
          var categories     = '{{ $row["categories"] }}';
          if(categories){
            var selectedValues = categories.split(',');
            $("select#categories").select2('val',selectedValues); 
          }
 

      $('.removeMultiFiles').on('click',function(){
        var removeUrl = '{{ url("events/removefiles?file=")}}'+$(this).attr('url');
        $(this).parent().remove();
        $.get(removeUrl,function(response){});
        $(this).parent('div').empty();  
        return false;
      }); 

      //  tag plugin
      $('input[name="tags"]').tagsInput({
        width:'auto'
      });   


      var formid = $('form#events').find('input[name="id"]').val();     
      if(!formid){
        // render map application
        $("#geocomplete").geocomplete({
          map: ".map_canvas",
          details: "form",
          types: ["geocode", "establishment"],
        }); 
      }


      if(formid > 0){
        var options = {
          map: ".map_canvas",
          location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
          details: "form",          
          types: ["geocode", "establishment"],
        };
        $("#geocomplete").geocomplete(options);
      }

      var country = '{{ $row["country"] }}';  
      var city    = '{{ $row["city"] }}'; 
      var zipcode = '{{ $row["zipcode"] }}';  
      var state   = '{{ $row["state"] }}';  
      var address = '{{ $row["address"] }}';  

      // set the country fields
      if(!country){
        $('form#events').find('input[name="country"]').val(country);
      } 
      // set the city fields
      if(!city){
        $('form#events').find('input[name="city"]').val(city);
      } 
      // set the zipcode fields
      if(!zipcode){
        $('form#events').find('input[name="zipcode"]').val(zipcode);
      }
      // set the state fields
      if(!state){
        $('form#events').find('input[name="state"]').val(state);
      } 
      // set the address fields
      if(!address){
        $('form#events').find('input[name="formatted_address"]').val(address);
      }                           

    });

    // show preview image
    $(document).on('change','input[name="image"]',function(){
      readURL(this);
    })
    // render image
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.event_preview_img').attr('src', e.target.result).show();
        }
        $('.choose_box').addClass('image_selected');
        $('.hide_img_preview').show();
        reader.readAsDataURL(input.files[0]);
      }
    }

    // show video preview
    $(document).on("change", "input[name='video']", function(evt) {
      // add class to parent div
      // $('.playvideo_div').addClass('video_choosed');
      // var $source = $('#video_here');
      // $('.video-preview').show(); 
      // $source[0].src = URL.createObjectURL(this.files[0]);
      // $source.parent()[0].load();
        readvideoURL(this);

    });  
    // render video
    function readvideoURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.event_preview_vd').attr('src', e.target.result).show();
        }
        $('.choose_box_vd').addClass('image_selected');
        $('.hide_vd_preview').show();

        reader.readAsDataURL(input.files[0]);
      }
    }  

    $("#events").validate({
      ignore: "not:hidden",
      rules: {
        title:{
          required:true,
        },
        note:{
          required:true,
        },
        labels:{
          required:true,
        },
        startdate:{
          required:true,
        },
        enddate:{
          required:true,
        },
        link:{
          required:true,
          url: true
        },
        /*image:{
          required:true,
        },*/
        email:{
          required:true,
          email: true
        },
        geocomplete:{
          required:true,
        },
        video:{
          required:true,
        },
        formatted_address:{
          required:true,
        },
        locality:{
          required:true,
        },
        postal_code:{
          required:true,
        },
        administrative_area_level_1:{
          required:true,
        },
        country:{
          required:true,
        },
        mobile:{
          number:true,
          minlength:10,
        },

      },
      messages: {
        title:{
          required:"Please Provide Event Title"
        },
        note:{
          required:"Please Provide Event Description"
        },
        labels:{
          required:"Please Provide Event Tags"
        },
        startdate:{
          required:"Please Provide Event StartDate"
        },
        enddate:{
          required:"Please Provide Event EventDate"
        },
        link:{
          required:"Please Provide Event Website",
          url:"Please Provide valid URL"  
        },
        // image:{
        //  required:"Please Event Banner Image"
        // },
        email:{
          required:"Please Provide Event Contact Email",
          email:"Please enter a valid email address"
        },
        geocomplete:{
          required:"Please Choose an Event Location"
        },
        video:{
          required:"Please Choose an Event Video"
        },
        formatted_address:{
          required:"Event Address required"
        },  
        locality:{
          required:"Event Locality required"
        },  
        postal_code:{
          required:"Event Postal Code is required"
        },  
        administrative_area_level_1:{
          required:"Event City is Required"
        },  
        country:{
          required:"Event Country is required"
        },  
        mobile:{
          required:"Please Provide Contact Number",
          number:"Please Provide Valid Contact Number"
        },
      },
      submitHandler:function(form){

        form.submit();
      }
    }); 
  $(function(){
    var max = parseInt($("#note").attr("maxlength"));
    $("#count").text("Characters left: " + max);
    $("#note").keyup(function(e){
      $("#count").text("Characters left: " + (max - $(this).val().length));
      if($(this).val().length==max)
        $("#msg").text("500 words allowed");
      else
        $("#msg").text("");
    });
  }); 
  </script>    


  <style>
  .map{
    display:none;
  }
  </style>