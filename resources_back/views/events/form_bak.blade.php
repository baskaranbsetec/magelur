@extends('layouts.app')

@section('content')
<style type="text/css">
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}
.map {
    display: none;
}
</style>


<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script>
<div class="page-content row">
	<!-- Page header -->


	<div class="page-content-wrapper m-t">


		<div class="sbox">
			<div class="sbox-title"> 
				<div class="sbox-tools pull-left" >
					<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
				</div>
				<h1> Events</h1>
				<div class="sbox-tools " >
					@if(Session::get('gid') ==1)
					<a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
					@endif 			
				</div> 

			</div>
			<div class="sbox-content"> 	

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>	

				{!! Form::open(array('url'=>'events/save?return='.$return, 'class'=>'form-horizontal','id'=>'events','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="col-md-12">

					<fieldset><!-- <legend> Events</legend> -->
						<!-- hidden fields -->
						
						<input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' class='form-control ' /> 
						<input  type='hidden' name='post_id' id='post_id' value='{{ $row['id'] }}' class='form-control ' /> 	
						<!-- get lat & longitute -->
						<input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
						<input type="hidden" name="lng" value="{{ $row['longitude'] }}" />
						<!-- Title -->
						<div class="form-group  " >
							<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
							<div class="col-md-7">
								<input  type='text' name='title' id='title' value='{{ $row["title"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 		

						<!-- Description -->
						<div class="form-group  " >
							<label for="Note" class=" control-label col-md-4 text-left"> Note </label>
							<div class="col-md-7">
								<textarea name='note' minlength="20" rows='5' id='note' class='form-control input-sm  markItUp markItUpEditor '  
								>{{ $row['note'] }}</textarea> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 	


						<!-- Email -->
						<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Email </label>
							<div class="col-md-7">
								<input  type='text' name='email' id='email' value='{{ $row["email"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 

						<!-- Mobile -->
						<div class="form-group  " >
							<label for="Mobile" class=" control-label col-md-4 text-left"> Mobile </label>
							<div class="col-md-7">
								<input  type='text' name='mobile' id='mobile' value='{{ $row["mobile"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 						

						<!-- event status -->
						<div class="form-group  " >
							<label for=" Status" class=" control-label col-md-4 text-left">  Status </label>
							<div class="col-md-7">
							<label class='radio radio-inline'>
							{!! Form::radio('status','Active',($row['status']=='Active') ? true:false,array()) !!} Active </label> 
							<label class='radio radio-inline'>
							{!! Form::radio('status','Inactive',($row['status']=='Inactive') ? true:false,array()) !!} Inactive </label> 		
							</div> 
						</div> 	 

						<!-- event start date -->
						<div class="form-group  " >
							<label for="Startdate" class=" control-label col-md-4 text-left"> Startdate </label>
							<div class="col-md-7">

								<div class="input-group m-b">
									{!! Form::text('startdate', $row['startdate'],array('class'=>'form-control datetime')) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>

							</div> 
							<div class="col-md-1">

							</div>
						</div> 	

						<!-- event end date -->
						<div class="form-group  " >
							<label for="Enddate" class=" control-label col-md-4 text-left"> Enddate </label>
							<div class="col-md-7">

								<div class="input-group m-b">
									{!! Form::text('enddate', $row['enddate'],array('class'=>'form-control datetime')) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>

							</div> 
							<div class="col-md-1">

							</div>
						</div> 	



						<div class="events_map_form col-md-12">
						<div class="col-md-7 event_form_fields">
							<div class="events_label_blk col-md-12">
							<div class="col-md-7 form-group  ">
								<label for="Enddate" class=" control-label text-left"> Select Your Location </label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name='geocomplete' id="geocomplete" type="text" placeholder="" class="form-control" value="{{ $row["address"] }}" />
							</div>
							<div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Address</label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name="formatted_address" type="text" class="form-control" value="{{ $row["address"] }}">
							</div>
							<div class="col-md-7 form-group  ">
								<label class=" control-label text-left">City</label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name="locality" type="text" class="form-control" value="{{ $row["city"] }}">
							</div>	
							<div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Zip Code</label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name="postal_code" type="text" class="form-control" value="{{ $row['zipcode'] }}">
							</div>
							<div class="col-md-7 form-group  ">
								<label class=" control-label text-left">State</label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name="administrative_area_level_1" class="form-control" type="text" value="{{ $row["state"] }}">
							</div>
							<div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Country</label>
							</div>
							<div class="col-md-5 form-group  event_input_field_blk">
								<input name="country" type="text" class="form-control" value="{{ $row["country"] }}">	
							</div>
						</div>
						</div>
						<div class="col-md-4 map_blk">
							<div class="input-group m-b" style="width:100% !important;">
							<div id="map_canvas" class="map_canvas" style="width: 100%;height: 290px;"></div>
						</div>
						</div>
						</div>

						<!-- event Tags -->				
						<div class="form-group  " >
							<label for="Tags" class=" control-label col-md-4 text-left"> Event Link </label>
							<div class="col-md-7">
								<input name='link' id='link' class='form-control' value="{{ $row['event_link'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>


						<!-- event Tags -->				
						<div class="form-group  " >
							<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
							<div class="col-md-7">
								<input name='tags' id='tags' class='form-control' value="{{ $row['tags'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>

						<!-- event Categories --> 					
						<div class="form-group  " >
							<label for="Categories" class=" control-label col-md-4 text-left"> Categories </label>
							<div class="col-md-7" data-val="{{ $row['categories'] }}">
									{!! Form::select('categories[]',\SiteHelpers::getTableValues('category') ,explode(',',$row['categories']),array('class' => 'select2 ','id' => 'categories','multiple'=>'multiple')) !!}	
							</div> 
							<div class="col-md-1">

							</div>
						</div> 	
						<!-- image file -->		
						<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
							
							<div class="col-md-7">
								<input type="file"  name="image" class=" form-control" accept=".jpg, .jpeg, .png" />
								<div class="col-md-7">
								<div class="preview-events event_form_img">
									@if(empty($row["image"]))
									<img src="{{ asset('uploads/images/no-image.png')}}" border="0" width="50" height="50" class="img-circle">
									@else
									<img src="{{ asset('uploads/events/'.$row['image'])}}" border="0" width="50" height="50" class="img-circle">
									@endif
								</div>
							</div>
							</div> 
							
						</div> 	
						<!-- video embed link -->
						<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> Video Embed </label>
							<div class="col-md-7">
								<input type="file" name="video" accept="video/mp4,video/x-m4v,video/*" value="{{ $row['video'] }}"  class=" form-control"  />
								
								@if(empty($row["video"]))
								<video class="video-preview" width="400" controls style="display:none;">
									<source src="mov_bbb.mp4" id="video_here">
										Your browser does not support HTML5 video.
								</video>
								@else
								<input type="hidden" name="video_id" value="{{ $row['video'] }}" />
								<input type="hidden" name="video_ext" value="{{ $row['video_ext'] }}" />
								<video class="video-preview" width="400" controls>
									<source src="{{ asset('uploads/events/videos/'.$row['video'].'/video.'.$row['video_ext'])}}" id="video_here">
										Your browser does not support HTML5 video.
								</video>	
								@endif							

							</div> 
							<div class="col-md-1">
							</div>

						</div> 							

					</fieldset>
					</div>


					<div style="clear:both"></div>	

					
					<div class="form-group">
						<label class="col-sm-4 text-right">&nbsp;</label>
						<div class="col-sm-8">	
							<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
							<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
							<button type="button" onclick="location.href='{{ URL::to('events?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
						</div>	  
					</div> 

					{!! Form::close() !!}
				</div>
			</div>		 
		</div>	
	</div>		
	 
	<script type="text/javascript">
		$(document).ready(function() { 
			$('.removeMultiFiles').on('click',function(){
				var removeUrl = '{{ url("events/removefiles?file=")}}'+$(this).attr('url');
				$(this).parent().remove();
				$.get(removeUrl,function(response){});
				$(this).parent('div').empty();	
				return false;
			});

			// set selected values 
			var categories     = '{{ $row["categories"] }}';
			if(categories){
			var selectedValues = categories.split(',');
			$("select#categories").select2('val',selectedValues);	
			}
			
			//  tag plugin
			$('input[name="tags"]').tagsInput({
				width:'auto'
			});		


			var formid = $('form#events').find('input[name="id"]').val();			
			if(!formid){
				// render map application
				$("#geocomplete").geocomplete({
					map: ".map_canvas",
					details: "form",
					types: ["geocode", "establishment"],
				});	
			}


			if(formid > 0){
				var options = {
					map: ".map_canvas",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					details: "form",					
					types: ["geocode", "establishment"],
				};
				$("#geocomplete").geocomplete(options);
			}

			var country = '{{ $row["country"] }}';	
			var city    = '{{ $row["city"] }}';	
			var zipcode = '{{ $row["zipcode"] }}';	
			var state   = '{{ $row["state"] }}';	
			var address = '{{ $row["address"] }}';	

			// set the country fields
			if(!country){
				$('form#events').find('input[name="country"]').val(country);
			}	
			// set the city fields
			if(!city){
				$('form#events').find('input[name="city"]').val(city);
			}	
			// set the zipcode fields
			if(!zipcode){
				$('form#events').find('input[name="zipcode"]').val(zipcode);
			}
			// set the state fields
			if(!state){
				$('form#events').find('input[name="state"]').val(state);
			}	
			// set the address fields
			if(!address){
				$('form#events').find('input[name="formatted_address"]').val(address);
			}														

		});

		// show preview image
		$(document).on('change','input[name="image"]',function(){
		  readURL(this);
		})
		// render image
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-events img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}
		// show video preview
		$(document).on("change", "input[name='video']", function(evt) {
		  var $source = $('#video_here');
		  $('.video-preview').show();	
		  $source[0].src = URL.createObjectURL(this.files[0]);
		  $source.parent()[0].load();
		});		
		$(document).on("change", ".file_multi_video", function(evt) {
		  var $source = $('#video_here');
		  $source[0].src = URL.createObjectURL(this.files[0]);
		  $source.parent()[0].load();
		});

		// event form validation
		$("#events").validate({
			ignore: "not:hidden",
			rules: {
				title:{
					required:true,
				},
				note:{
					required:true,
				},
				labels:{
					required:true,
				},
				startdate:{
					required:true,
				},
				enddate:{
					required:true,
				},
				link:{
					required:true,
					url: true
				},
				/*image:{
					required:true,
				},*/
				/*video:{
					required:true,
					video:true
				},*/
				email:{
					required:true,
					email: true
				},
				geocomplete:{
					required:true,
				},

				formatted_address:{
					required:true,
				},
				locality:{
					required:true,
				},
				postal_code:{
					required:true,
				},
				administrative_area_level_1:{
					required:true,
				},
				country:{
					required:true,
				},
				mobile:{
					number:true,
					minlength:10,
				},

			},
			messages: {
				title:{
					required:"Please Provide Event Title"
				},
				note:{
					required:"Please Provide Event Description"
				},
				labels:{
					required:"Please Provide Event Tags"
				},
				startdate:{
					required:"Please Provide Event StartDate"
				},
				enddate:{
					required:"Please Provide Event EventDate"
				},
				link:{
					required:"Please Provide Event Website",
					url:"Please Provide valid URL"	
				},
				// image:{
				// 	required:"Please Event Banner Image"
				// },
				email:{
					required:"Please Provide Event Contact Email",
					email:"Please enter a valid email address"
				},
				geocomplete:{
					required:"Please Choose an Event Location"
				},

				formatted_address:{
					required:"Event Address required"
				},	
				locality:{
					required:"Event Locality required"
				},	
				postal_code:{
					required:"Event Postal Code is required"
				},	
				administrative_area_level_1:{
					required:"Event City is Required"
				},	
				country:{
					required:"Event Country is required"
				},	
				mobile:{
					required:"Please Provide Contact Number",
					number:"Please Provide Valid Contact Number"
				},
			},
			submitHandler:function(form){

				form.submit();
			}
		});	
	$(function(){
		var max = parseInt($("#note").attr("maxlength"));
	  $("#count").text("Characters left: " + max);
		$("#note").keyup(function(e){
			$("#count").text("Characters left: " + (max - $(this).val().length));
	    if($(this).val().length==max)
	    	$("#msg").text("500 words allowed");
			else
	    	$("#msg").text("");
		});
	});	
	</script>		 
	@stop