<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
						<tr>
							<td width='30%' class='label-view text-right'>Id</td>
							<td>{{ $row->id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Id</td>
							<td>{{ $row->user_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>{{ $row->user_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Membership Id</td>
							<td>{{ $row->membership_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Amount</td>
							<td>{{ $row->amount}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created At</td>
							<td>{{ $row->created_at}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Updated At</td>
							<td>{{ $row->updated_at}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Order Details</td>
							<td>{{ $row->order_details}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Status</td>
							<td>{{ $row->status}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Payment Method</td>
							<td>{{ $row->payment_method}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Purchase Type</td>
							<td>{{ $row->purchase_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Purchase Period</td>
							<td>{{ $row->purchase_period}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Transaction Status</td>
							<td>{{ $row->transaction_status}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Webhook Check Status</td>
							<td>{{ $row->webhook_check_status}} </td>
							
						</tr>
							
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	