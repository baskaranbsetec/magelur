@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Transaction</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'transactions/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
							<fieldset><legend> transactions</legend>
										
										  <div class="form-group  " >
											<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
											<div class="col-md-7">
											  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="User Id" class=" control-label col-md-4 text-left"> User Id </label>
											<div class="col-md-7">
											  <input  type='text' name='user_id' id='user_id' value='{{ $row['user_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="User Type" class=" control-label col-md-4 text-left"> User Type </label>
											<div class="col-md-7">
											  <input  type='text' name='user_type' id='user_type' value='{{ $row['user_type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Membership Id" class=" control-label col-md-4 text-left"> Membership Id </label>
											<div class="col-md-7">
											  <input  type='text' name='membership_id' id='membership_id' value='{{ $row['membership_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Amount" class=" control-label col-md-4 text-left"> Amount </label>
											<div class="col-md-7">
											  <input  type='text' name='amount' id='amount' value='{{ $row['amount'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Created At" class=" control-label col-md-4 text-left"> Created At </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Order Details" class=" control-label col-md-4 text-left"> Order Details </label>
											<div class="col-md-7">
											  <textarea name='order_details' rows='5' id='order_details' class='form-control '  
					           >{{ $row['order_details'] }}</textarea> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
											<div class="col-md-7">
											  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Payment Method" class=" control-label col-md-4 text-left"> Payment Method </label>
											<div class="col-md-7">
											  <input  type='text' name='payment_method' id='payment_method' value='{{ $row['payment_method'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Purchase Type" class=" control-label col-md-4 text-left"> Purchase Type </label>
											<div class="col-md-7">
											  <input  type='text' name='purchase_type' id='purchase_type' value='{{ $row['purchase_type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Purchase Period" class=" control-label col-md-4 text-left"> Purchase Period </label>
											<div class="col-md-7">
											  <input  type='text' name='purchase_period' id='purchase_period' value='{{ $row['purchase_period'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Transaction Status" class=" control-label col-md-4 text-left"> Transaction Status </label>
											<div class="col-md-7">
											  <input  type='text' name='transaction_status' id='transaction_status' value='{{ $row['transaction_status'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Webhook Check Status" class=" control-label col-md-4 text-left"> Webhook Check Status </label>
											<div class="col-md-7">
											  <input  type='text' name='webhook_check_status' id='webhook_check_status' value='{{ $row['webhook_check_status'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> </fieldset>
				</div>
				
				

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('transactions?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("transactions/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop