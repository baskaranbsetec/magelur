<!DOCTYPE html>


<html>
<head>
</head>
<body>
<table style="width: 600px;background-color:#000;display: block;">
<tbody style="width: 100%;display: block;">
<tr style="width: 100%;display: block;float: left;width: 100%;">
<td style="padding-left: 0px;padding-right: 0px;padding-top:20px;padding-bottom:20px;width: 94%;display:block;padding-left:3%;padding-right:3%;">	
<p style="display: block; height: 65px; max-width: 100%; text-align: center;">
<a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/email_logo.png" style="max-width:100%;height:155px;">	</a>
</p>
<div style="display: block; width: 100%; background-color: transparent;border-radius:6px">
<div style="display:inline-block;width:46%;float:left;background-color:#fff;padding: 35px 2% 30px 2%;height: 340px;">
<p style="color:#000;font-size:16px;font-weight:400 ;text-align:left; font-family: arial;">
<b><span style="color:#000;font-size: 16px;">YOUR ORDER DETAILS</span></b></p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Buyer Name:</b>{{$transaction->username}}</p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Vendor Name:</b>Magelur</p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Order Number:</b>{{$transaction->order_num}}</p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Order Date:</b>{{$transaction->created_at}}</p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Payment Method:</b>{{$transaction->payment_method}}</p>
<p style="color:#000;font-size:14px ;text-align:left;width: 100%;display: block;margin: 0 auto;line-height: 1.5;font-family: arial;padding-bottom:20px"> 
<b>Plan Name:</b>{{$transaction->plan_name}}</p>
</div>
<div style="display:inline-block;width:46%;float:left;background-color:#bd8404;padding: 70px 2% 35px 2%;height: 300px;">
<p style="margin-bottom: 19px; margin-top: 20px;float: left;width: 100%;text-align:center">
<a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/Success_2.png" style="max-width:100%;height:64px;"></a></p>
<p style="color:#000;font-size:16px;font-weight:400 ;text-align:left; font-family: arial;">
<b><span style="color:#fff;font-size: 16px;">Premium plan details</span></b></p>
<p style="color:#000;font-size:16px;font-weight:400 ;text-align:left; font-family: arial;">
<span style="color:#fff;font-size: 12px;">Subscription to basic plan is successful.</span></p>
<h1 style="color:#fff;font-size:15px;font-weight:400 ;text-align:left; font-family: arial;"><span><img src="http://demo.bsetec.com/demo/magelur/public/images/tick_image.png" style="max-width:100%;height:15px;"></span>Use {{$transaction->video_limit}} Videos</h1>
<h1 style="color:#fff;font-size:15px;font-weight:400 ;text-align:left; font-family: arial;"><span><img src="http://demo.bsetec.com/demo/magelur/public/images/tick_image.png" style="max-width:100%;height:15px;"></span>Advertise {{$transaction->event_limit}} events</h1>
<h1 style="color:#fff;font-size:15px;font-weight:400 ;text-align:left; font-family: arial;"><span><img src="http://demo.bsetec.com/demo/magelur/public/images/tick_image.png" style="max-width:100%;height:15px;"></span>Can upload {{$transaction->image_limit}} images</h1>
</div>
</div>
</td>
</tr>
<tr style="width: 100%;display: block;">
<td style="width: 100%;display: block;">
<p style="margin-bottom: 19px; margin-top: 20px;float: left;width: 100%;">
<a href="javascript:void(0);" style="color: #fff;text-align: center;display: block;margin: 0 auto;text-decoration: none;width: 200px;height: 40px;background: #bd8404;
border-radius: 4px;line-height: 2.5;font-size: 17px;font-family: arial;">SUBMIT</a></p>
</td>
</tr>
<tr style="width: 100%;display: block;">
<td style="width: 100%;display: block;padding:0px 0px 20px 10px">
<p href="#" style="display: block;text-decoration: none;text-align: center;">
    <a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/fb_2.png"></a>
    <a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/Twet_2.png"></a>
    <a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/mail_2.png"></a>
</p>
</td>
</tr>
<tr style="width:96%;display:block;background-color:#151515;padding:0px 10px;padding-left:2%;padding-right:2%;">
<td style="text-align:left;width: 29%;display: inline-block;">
<div style="display: inline-block; height: 30px; max-width: 100px; margin-top: 15px; margin-bottom: 15px;">
<a href="javascript:void(0);"><img src="http://demo.bsetec.com/demo/magelur/public/images/Asset 2.png" style="max-width:100%;"></a>	
</div>
</td><td style="text-align:right;width: 70%;display: inline-block;float:right;margin-top:15px">
<p style="display: block; font-size: 11px; font-family: 'arial'; margin-bottom: 1px; line-height: 11px;color:#fff;width:100%">IMAGE ID:121233</p>
<p style="display: block; font-size: 11px; font-family: 'arial'; margin-bottom: 1px;margin-top:5px; line-height: 11px;color:#fff;width:100%">demo.bsetec.com/demo/magelur</p>
</td>
</tr>
</tbody>
</table>
</body>
</html>
