<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Magelur</title>
<link rel="shortcut icon" href="http://192.168.1.65:81/magelur/public/favicon.ico" type="image/x-icon">

		<link href="http://192.168.1.65:81/magelur/public/magelur/js/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"> 
		<link href="http://192.168.1.65:81/magelur/public/magelur/css/magelur.css" rel="stylesheet">
		<link href="http://192.168.1.65:81/magelur/public/magelur/css/animate.css" rel="stylesheet">
		<link href="http://192.168.1.65:81/magelur/public/magelur/css/icons.min.css" rel="stylesheet">
		<link href="http://192.168.1.65:81/magelur/public/magelur/fonts/awesome/css/font-awesome.min.css" rel="stylesheet">
		 <script src="//use.edgefonts.net/source-sans-pro.js"></script>

		<script type="text/javascript" src="http://192.168.1.65:81/magelur/public/magelur/js/plugins/jquery-1.12.3.min.js"></script>
		<script type="text/javascript" src="http://192.168.1.65:81/magelur/public/magelur/js/plugins/parsley.js"></script>			
		<script type="text/javascript" src="http://192.168.1.65:81/magelur/public/magelur/js/plugins/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="http://192.168.1.65:81/magelur/public/magelur/js/plugins/jquery.form.js"></script>	

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->	
		</head>
<body class="admin_login">
<div class="magelur_admin_login col-md-12 col-sm-12 col-xs-12">
<div class="admin_inner_block">
<div class="container">
<div class="admin_login_blk">
<div class="admin_login_div col-md-12 col-sm-12 col-xs-12">
	<div class="login_leftside col-xs-12 col-sm-5 col-md-5">
		<img src="images/magelur_logo.png" class="login_logo">
	</div>
	<div class="login_rightside col-xs-12 col-sm-7 col-md-7">
		<h1>Admin Login</h1>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
		<div class="admin_input_field">
    		<span class="admin_icon"><img src="images/login_user_icon.png"></span>
    		<input type="text" class="form-control" placeholder="User name/ Email">
		</div>
		<div class="admin_input_field admin_pswd_field">
		<span><img src="images/login_pswd_icon.png"></span>
    		<input type="text" class="form-control" placeholder="Password">
		</div>
		<div class="admin_submit_btn">
		<input type="button" name="submit" value="LOGIN">
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</body> 
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#or').click(function(){
		$('#fr').toggle();
		});

		var form = $('#LoginAjax'); 
		form.parsley();
		form.submit(function(){
			
			if(form.parsley('isValid') == true){			
				var options = { 
					dataType:      'json', 
					beforeSubmit :  showRequest,
					success:       showResponse  
				}  
				$(this).ajaxSubmit(options); 
				return false;
							
			} else {
				return false;
			}		
		
		});

	});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		window.location.href = data.url;	
		$('.ajaxLoading').hide();
	} else {
		$('.message').html(data.message)	
		$('.ajaxLoading').hide();
		$('.message').show(data.message)	
		return false;
	}	
}	
</script>
