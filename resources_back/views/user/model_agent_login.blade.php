
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style type="text/css">

.alert {
    padding: 15px;
    margin-bottom: 15px;
    margin-top: -60px;
    border: 1px solid transparent;
    border-radius: 4px;
}
.alert_danger {
    /* background-color: #151515; */
    /* border-color: #151515; */
    color: #ffb524;
    top: -8px;
    font-size: 20px;
    font-family: "Lato-Regular";
    color: #bd8414;
}
</style>
	<div class="magelur_about">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_login_banner">
				
					<div class="banner_logintxt">
					  <div class="signins wow FadeInLeft">
					   <h1>Sign In </h1>
					   <h3>And start networking</h3>
					  </div>
					 </div>

					 <div class="enter_log wow FadeInUp">
					  <p>Enter your</p>
					  <h1>Login credentials</h1>
					 </div>

					</div>
				</div>
			</section>
		</div>
		<!-- Header End -->



<div class="login_base">
 <div class="first_claerfix clearfix">
  <div class="col-sm-6 wow FadeInLeft">
  <div class="montertes">
<div class="ajaxLoading"></div>
    <p class="message alert alert-danger " style="display:none;"></p> 
  @if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{{\Session::get('messagetext')}}">
    @endif
        
        @if(Session::has('message'))
        {!! Session::get('message') !!}
      @endif
    <ul class="parsley-error-list">
      @foreach($errors->all() as $error)
        <li style='color: #ffb524;'>{{ $error }}</li>
      @endforeach
    </ul>
<form method="post" name="myForm" action="{{ url('model-agent/signin')}}" class="form-vertical" id="LoginAjax" accept-charset="UTF-8" class="form-signup" parsley-validate="" novalidate=" " >
  <div class="strendng magelur_login_page">
 <b> <div id='message' style='top: -8px;font-size: 15px; color: #bd8414;margin-bottom: 17px;'></div></b>

   <div class="mainlog">
    <a href="javascript:void(0)">Login As</a>
    <br><br>

    <label class="agree">Agent
    <input type='radio' name='agree' value ='agent' checked>  
    <span class="checkmark"></span>
    </label>  <label class="agree">Model
    <input type='radio' name='agree' value ='model' >  
    <span class="checkmark"></span>
    </label>

   </div>


   <div class="user_login">
   	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="inputslogin">
      {!! Form::text('email', '',array( 'placeholder'=>'Enter your Username','id'=>"email")) !!} 
    </div>
    <div class="inputslogin second_inputs_lg">
       <input type="password" id='password' name="password" placeholder="Enter your Password"   />
    </div>
    @if(CNF_RECAPTCHA =='true')
    <div class="inputslogin second_inputs_lg has-feedback  animated fadeInLeft delayp1">
        <br />
        {!! captcha_img() !!} <br /><br />
        <input type="text" name="captcha" placeholder="Type Security Code"  required/>
        
        <div class="clr"></div>
      </div>
      @endif  
    <div class="rember_me">
     <div class="checkbox checkbox-anr case_mix_check" >
       <input type="checkbox" name="terms_agree" value="1" />
       <label><span>Remember me</span></label>
      </div>
      <p>* By clicking Sign In you agree to our Terms of Use and Privacy Statement.</p>
    </div>
    <!--  <div class="forget_password">
      <a href="javascript:void(0)">Forget your password?</a>
     </div> -->

    </div>
   </div>
   <div class="login_signin_btn">
     <!-- <a href="javascript:void(0)">Sign In</a> -->
   
    <button type="submit" class="full_btn"> Sign In</button>
    </div>

</form>


   </div>
  </div>
  <div class="col-sm-6 wow FadeInRight">
   <div class="dnthave">
    <h1>Don't have an account</h1>
    <p>Not yet a member? Here is what you are missing out on!</p>
    <ul class="mantres">
     <li>Promoting yourself on our vibrant and growing network</li>
     <li>Being the next top Model</li>
     <li>Share your talents for all to see</li>
     <li>Share your photos and videos</li>
     <li>Share your social pages to be discovered in more ways</li>
     <li>Commenting and liking other talents and productions</li>
    </ul>
   </div>
   <div class="signupnow">
    <h1>Sign Up Now!</h1>
   </div>
    <div class="signupsagent">
      @if( app('request')->input('tellfriend'))
      {{--*/ $url=app('request')->input('tellfriend') /*--}}
       <a href="{{ url('/model/create?tellfriend=') }}{{$url}}">I am a Model</a>
       <a href="{{ url('/agent/create?tellfriend=') }}{{$url}}">I am an Agent</a>
      @else
     <a href="{{ url('/model/create') }}">I am a Model</a>
     <a href="{{ url('/agent/create') }}">I am an Agent</a>
     @endif

     
    </div>
  </div>
  </div>
 </div>

<div class="our_terms_use">
 <div class="maintinused wow FadeInUp">
 <h2>Our Terms of use</h2>
 <h4>If you need immediate assistance login in, you may contact our IT Services help desk on our contact page: by clicking here.</h4>
 <p>You are attempting to enter Pure & Innocent - Talent Models Network portal and to access it's information (collectively "Pure And Innocent.net Systems and Information"), which are private. Access to our Systems and Information is restricted to authorized users only and your actions will be monitored.</p>
 <p>Your username and password are for your use only. Do not share your login credentials with any other party. If you believe that another party has knowledge of your login credentials, contact our help desk immediately at models@PureAndInnocent.net</p>
 </div>
</div>


<script type="text/javascript">
    

$("#LoginAjax").validate({
     ignore: "not:hidden",
      rules: {
       agree:{
          required:true,
          
        },
       email:{
          required:true,
          email:true,
            remote:{
          url:"{!! url('model/checkaccount')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#email').val();
            },
            agree:function(){
              return $('input[name=agree]:checked').val();
            },
            
          }
        }
        
        },
         password:{
         required:true,
         
        },
         captcha:{
         required:true,
         
        },
      },
      messages: {
         agree:{
          required:"This field is required",       
         
        },
        
        email:{
          required:"This field is required",
          email:"Invalid Email address",
        },
        
        password:{
         required:"This field is required",
        },
      },
      
        submitHandler: function(form) {
          $.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),

          success: function(data) {
            if(data==''){
            $("#message").html('Incorrect Email ID / Password');
            }
            else{
            form.submit();
            }

          },
          });
        },
        });
if($('#msg').val()!="")
{
  swal($('#msg').val());
}
</script>
