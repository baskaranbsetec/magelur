


	<div class="magelur_about faq_sec">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner faq_banner">
					

					<div class="banner_title wow bounce">
						<h1>FAQ</h1>
							<ul class="link_list">
								<li class="active"><a href="{{asset('/')}}">HOME</a></li>
								<li>//</li>
								<li><a href="javascript:void(0)">FAQ</a></li>
							</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- Header End -->

<div class="bootfaq">
 <div class="container">
  @foreach($faqs as $faq)
  <div class="faq_one">
  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo_{{ $faq->id }}">{{ $faq->question }}</button>
  <div id="demo_{{ $faq->id }}" class="collapse down_modelent">{{ $faq->answer }}</div>
  </div>
  @endforeach

 </div>
</div>


		
