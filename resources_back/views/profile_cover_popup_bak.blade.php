@foreach($images as $image)
<div id="myAvatar" class="modal fade  my_profile_page" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" onclick="window.location.reload();return(true)" >&times;</button>
<h4 class="modal-title">Profile Banner upload</h4>
</div>
<div class="modal-body">
<div class="alert-container"></div>
<div class="form-group" >
<div class="image-wrapper">
</div>  
<img src="" class="text-center hide" id="cropavatar" />
</div> 

<div class="form-group" >
<div class="col-md-12 col-sm-12 col-xs-12">

<div class="col-md-4 text-center">

 @if($image->profile_small)  
<div id="upload-demo" style="width:300px">
<div class="cr-viewport cr-vp-square" id='old' tabindex="0" style="width: 490px;
height: 329px;">
<img src="{{ asset('/uploads/model_profile/small/'.$image->profile_small) }}">
</div>
</div>
@elseif($image->profile_photo)
<div id="upload-demo" style="width:300px">
<div class="cr-viewport cr-vp-square" id='old' tabindex="0" style="width: 490px;
height: 329px;">
<img src="{{ asset('/uploads/agency_profile/'.$image->profile_photo) }}">
</div>
</div>
@endif


</div>
<div id="avatarFile">                            
</div>                           
</div>
</div>
<div>
<input type="file" name="avatar" id="avatar" >

<button  class="upload-result" disabled='disabled'>Update</button> 
</div>
</div>    
</div>
</div>
</div>

<!-- Cover -->
<div id="myCover" class="modal fade  banner_popup_img my_profile_page-cover" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" onclick="window.location.reload();return(true)"><span>&times;</span></button>
<h4 class="modal-title">Profile Banner upload</h4>
</div>
<div class="modal-body">
<div class="alert-container"></div>
<div class="form-group" >
<div class="image-wrapper">
</div>  
<img src="" class="text-center hide" id="cropavatar" />
</div> 

<div class="form-group form-group_full" >
<div class="col-md-12 col-sm-12 col-xs-12 nopadding_div">

<div class="col-md-12 col-sm-12 col-xs-12 cover_upload_blk text-center nopadding_div">

<span class='input_covers'>
<div class="cover_upload_blk">
<input type="file" name="avatar_cover" id="avatar-cover">
<span class="cover_upload_icon">
<i class="fa fa-upload"></i>
</span>
<h1>Browse Files</h1>
</div>
</span>
<span class='covers'>
<div id="upload-demo-cover">
@if(\Session::get('model_id'))
<div class="cr-viewport cr-vp-square" id='old-cover' tabindex="0"  data-src="{{$image->profile_cover}}" style="width: 550px;height: 350px;">
{!! SiteHelpers::user_avatar($image->profile_cover,'/uploads/model_profile/cover/') !!}
<!-- <div class="cover_image_close_btn"><span>&times;</span></div> -->
</div>
@elseif(\Session::get('agent_id'))
<div class="cr-viewport cr-vp-square" id='old-cover' tabindex="0" data-src="{{$image->profile_cover}}" style="width: 550px;height: 350px;">
{!! SiteHelpers::user_avatar($image->profile_cover,'/uploads/agency_profile/cover/') !!}
<!-- <button type="button" class="close close_btn">&times;</button> -->
</div>
@endif
</div>
</span>
<div class="cover_image_close_btn close_btn"><span>&times;</span></div>

</div>
<div id="avatarFileCover">                            
</div>                           
</div>
</div>
<div class="cover_browse_btn">
<!-- <input type="file" name="avatar_cover" id="avatar-cover" > -->
<button  class="upload-result-cover" disabled='disabled' ><span >Update</span></button> 
</div>
</div>    
</div>
</div>
</div>

@endforeach

<script type="text/javascript">


$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 340,
        height: 325,
        type: 'square'
    },
    boundary: {
        width: 400,
        height: 400
    }
});


$('#avatar').on('change', function () { 
  $('#old').hide();

var reader = new FileReader();
    reader.onload = function (e) {
    $uploadCrop.croppie('bind', {
    url: e.target.result
    }).then(function(){
    console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
$('.my_profile_page').modal('hide');
$uploadCrop.croppie('result', {
type: 'canvas',
size: 'viewport'
}).then(function (resp) {
$.ajax({
url: "{{ url('/model/save/profile') }}",
type: "POST",
data: {"image":resp},
success: function (data) {
html = '<img src="' + resp + '" />';
$("#upload-demo-i").html(html);
}
});
});
});

$('.editbtn_div').on('click', function () {
$('.cr-slider-wrap').hide();
});

$('input:file').on("change", function() {
$('.cr-slider-wrap').show();
$('.upload-result').removeAttr('disabled');
});


</script>

<script type="text/javascript">
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$uploadCropCover = $('#upload-demo-cover').croppie({
    enableExif: true,
    viewport: {
        width: 550,
        height: 300,
    },
    boundary: {
        width: 550,
        height: 350
    }
});

$('#avatar-cover').on('change', function () { 
    $('#old-cover').hide();
    var reader = new FileReader();
    reader.onload = function (e) {
    $uploadCropCover.croppie('bind', {
    url: e.target.result
    }).then(function(){
    console.log('jQuery bind complete');
    });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result-cover').on('click', function (ev) {
$('.my_profile_page-cover').modal('hide');

$uploadCropCover.croppie('result', {
type: 'canvas',
size: { width: 767},
quality: 0.8,

}).then(function (resp) {
$.ajax({
url: "{{ url('/model/save/Cover') }}",
type: "POST",
data: {"images":resp},
beforeSend: function() {
},
success: function (data) {
html = '<img src="' + resp + '" />';
$("#upload-demo-i-cover").html(html);
window.location.reload();return(true)
}
});
});
});

$('.editbtn_div_cover').on('click', function () {

var image = $('#old-cover').attr("data-src");
if(image !=""){
    $('.input_covers').hide();
    $('.upload-result-cover').hide();
}
else{
    $('.input_covers').show();  
    $('.covers').hide();
    $('.upload-result-cover').hide();
    
}
$('.cr-slider-wrap').hide();
});

$('input:file').on("change", function() {
$('.input_covers').hide();
$('.covers').show();
$('.cr-slider-wrap').show();
$('.upload-result-cover').removeAttr('disabled');
 $('.upload-result-cover').show();
});
$(document).on('click', '.close_btn', function () {
$('.covers').hide();
$('.close_btn').hide();
$('.input_covers').show(); 
});

</script>