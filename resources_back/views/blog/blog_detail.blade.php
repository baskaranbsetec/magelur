 <div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>Blog</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<div class="dashboard-ders blog_block">
 <div class="container">	
  <div class="left_part_ders">
   <ul class="clearfix">
  	<li class="abt_mtb">
  	 <div class="full_dash_bg">
  	 	<div class="left_abt clearfix blog_left_side">
      <div class="heading_date">
      <h1>{!! date("d" , strtotime($row->created_date)) !!}</h1>
      <h2>{!! date("M" , strtotime($row->created_date)) !!}.</h2>
      </div>
      <div class="blog_right_side blog_details_right_side">
      <h4>{{ $row->title }}</h4>
      <h5>Category: {{\SiteHelpers::getName('category',$row->category)}}  |  Author: JakubSpitzer</h5>
      <div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
      <img src="{{ asset('uploads/blog/'.$row->blog_image) }}" class="img-responsive">

      </div>
      {!! $row->description !!}
      <hr class="yellow_line">
      <div class="blog_button_blk">
      <div class="blog_button_div1">
      <?php
$str= $row['tags'] ;
$arr=explode(",",$str);
 
?>
@foreach($arr as $tag) 
@if($tag)
<?php
$tagg=str_replace(' ', '-', trim(strtolower($tag)));
?>
<button>{!! $tag !!}</button>

@endif
@endforeach  
      
      </div>
      <div class="blog_button_div2 blog_details_button_div2">
      <div class="blog_twitter">
      <div class="blog_twitter_left">
      <img src="{{ asset('images/blog_fb_img.png') }}">Like</div>
      <div class="blog_twitter_right">1</div>
      </div>
      <div class="blog_twitter blog_twitter_blk">
      <div class="blog_twitter_left">
      <img src="{{ asset('images/blog_twitter_img.png') }}">Tweet</div>
      <div class="blog_twitter_right">1</div>
      </div>
      </div>
      </div>      
      </div>
      <div class="blog_comments_blk">
      <div class="comment_blk1">
      <h1>RajeshVNS | Varanasi, India</h1>
      <div class="rating_blk">
      <ul class="starer">
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
        </ul>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac.</p>
      </div>
      <div class="comment_text_area">
      <textarea placeholder="Enter Your Comments"  rows="4"></textarea>
      </div>
      </div>
      </div> 
  	 </div>	
  	</li>
  	<li class="right_mtb">
  	 <div class="full_dash_bg">	
  	  <div class="left_abt clearfix"><h4>Categories</h4></div> 
  	  @foreach($blogcategories as $cat)      
         
      <div class="blog_category">
      <p>{{ $cat->name }}<span>({{ $cat->count }})</span></p>
     
      </div>
      @endforeach  
	 </div>	
	 <div class="full_dash_bg">	
  	  <div class="left_abt clearfix"><h4>Tag Cloud</h4></div> 
  	  <div class="blog_category tag_cloud_blk">
@foreach($blogs as $tag)
{{--*/ $str= $tag['tags']; /*--}}
{{--*/ $arr=explode(",",$str); /*--}}

@if($tag->id % 2 == 0)

@foreach($arr as $tag) 
{!!$tag!!}
@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
<span>{!! $tag !!}</span>@endif
@endforeach

@elseif($tag->id / 5 == 1)

@foreach($arr as $tag) 
@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
<sub>{!! $tag !!}</sub>@endif
@endforeach

@else

@foreach($arr as $tag) 

@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
{!! $tag !!}@endif
@endforeach



@endif

@endforeach
	 </div>	
	 
  	<!--   <div class="left_abt clearfix"><h4>Categories</h4></div> 
  	  <div class="blog_category photography_blk">
  	  <div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="{{ asset('images/blog_photography1.png') }}">
  	 	</div>
  	 	<div class="view_all_btn">
  	 	<button>View all...</button>
  	 	</div>

  	  </div> -->

  	</li>
  </ul>	
 </div>		
 </div>
</div>	
</div>


