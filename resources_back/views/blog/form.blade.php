@extends('layouts.app')

@section('content')
<link href="{{ asset('magelur/css/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/css/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Blogs</h1>

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'blog/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
	<fieldset>
		
			{!! Form::hidden('id', $row['id']) !!}

			<div class="form-group  " >
			<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
			<div class="col-md-7">
			{!! Form::text('title', $row['title'],array('id'=>"title",'class'=>"form-control",'required'=>'required')) !!} 
			</div> 
			</div> 

			<div class="form-group  " >
			<label for="categories" class=" control-label col-md-4 text-left"> categories </label>
			<div class="col-md-7">
			{!! Form::select('categories',array('' => 'Select category')+$categorys,$row['categories'],array('class'=>'form-control select2','id'=>'category')) !!}
			</div> 
			</div> 

			<div class="form-group  " >
			<label for="Description" class=" control-label col-md-4 text-left"> Description </label>
			<div class="col-md-7">
			{!! Form::textarea('description', $row['description'],array('id'=>"description",'class'=>"form-control",'required'=>'required')) !!} 	 	 
			</div> 
			</div> 

			 	

			<div class="form-group  " >
			<label for="Blog Image" class=" control-label col-md-4 text-left"> Blog Image </label>
			<div class="col-md-7">
			<input  type='file' name='blog_image' id='blog_image' @if($row['blog_image'] =='') class='required' @endif style='width:150px !important;'  />
			<div >
			{!! SiteHelpers::showUploadedFile($row['blog_image'],'/uploads/blog/') !!}
			</div>					
			</div> 
			</div> 					

			<div class="form-group  " >
			<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
			<div class="col-md-7">
			{!! Form::text('tags', $row['tags'],array('id'=>"tags",'class'=>"form-control",'required'=>'required')) !!} 	 	 
			</div> 
			</div>

			

			<div class="form-group  " >
			<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
			<div class="col-md-7">
			<label class='radio radio-inline'>
			{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active </label> 					  
			<label class='radio radio-inline'>
			{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive </label>
			</div> 
			</div> 
	</fieldset>
</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" >{{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('blog?return='.$return) }}' " class="btn btn-warning btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("blog/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
		$(function(){

		tinyMCE.init({
		theme : "advanced",
		mode: "exact",
		elements : "description",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		+ "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		+ "bullist,numlist,outdent,indent",
		theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		+"undo,redo,cleanup,code,separator,sub,sup,charmap",
		theme_advanced_buttons3 : "",
		height:"250px",
		width:"520px"
		});

		$('#tags').tagit({
		singleField: true,
		});
		

	})
	</script>		 
@stop