<!DOCTYPE>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
   
    <title>{{ CNF_APPNAME }}  {{ $pageTitle}}</title>
    <meta name="keywords" content="{{ $pageMetakey }}" />
    <meta name="description" content="{{ $pageMetadesc }}" />
    <meta name="Author" content="Mangopik [www.mangopik.com]" />

<link href="{{ asset('frontend') }}/default/css/owl.theme.default.min.css" rel="stylesheet">
      <link href="{{ asset('frontend') }}/default/css/style.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/animate.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/lightslider.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/agency.css" rel="stylesheet">

<script type="text/javascript" src="{{ asset('magelur/message/all_common.js')}}"></script>
<script src="{{ asset('magelur/message/common.js')}}"></script>
<script src="{{ asset('magelur/message/select2.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/message/select2.css')}}" />
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<link rel="stylesheet" href="{{ asset('magelur/message/ui.css')}}">
<!-- <script type="text/javascript" src="http://192.168.1.65:81/jobplus/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
 --><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/message/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/message/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/message/all_common.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/message/jquery.fancybox-thumbs.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/message/theme1.css')}}" rel="stylesheet">
<link href="{{ asset('magelur/message/bootstrap-formhelpers.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/message/settings.css')}}" media="screen">
<link href="{{ asset('magelur/message/common.css')}}" rel="stylesheet">
<!-- <link href="http://192.168.1.65:81/expert/bsetec/css/bsetec.css" rel="stylesheet">	-->
<!-- <link href="{{ asset('magelur/message/styles.css')}}" rel="stylesheet"> -->
<script type="text/javascript" src="{{ asset('magelur/message/jquery.mixitup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.themepunch.plugins.min.js')}}"></script>
<noscript>&lt;meta http-equiv="refresh" content="0;url=http://192.168.1.35/expert/nojs.php"&gt;</noscript>
<script src="{{ asset('magelur/message/jquery.raty.min.js')}}"></script>        
<link href="{{ asset('magelur/message/style.css')}}" rel="stylesheet">
<!-- <script src="http://192.168.1.35/expert/bsetec/js/owl.carousel.js"></script>-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->		
<script type="text/javascript" src="{{ asset('magelur/message/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-process.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-validate.js')}}"></script>

</head>




	<div class="magelur_about model_abouts">
		<!-- Header Start -->

		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner">
					<header>
<div class="logo">
            <h1><a href="javascript:void(0)"><img src="{{ asset('images/Asset 2.png') }}"></a></h1>
          </div>  
<div class="menu clearfix wow SlideInDown">
  <ul class="nav navbar-nav magelur_menulist">
            <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ouragencies') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">EVENTS</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
           <li class="header_menu_list">
            <a href="{!! url('aboutus') !!}">ABOUT</a>
             <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('contactus') }}">Contactus</a></li>
                <li><a tabindex="-1" href="{{ url('tellfriend') }}">Tell a friend</a></li>
                <li><a tabindex="-1" href="{{ url('toc') }}">Terms & conditions</a></li>
                <li><a tabindex="-1" href="{{ url('privacy') }}">Privay policy</a></li>
                <li><a tabindex="-1" href="{{ url('faqs') }}">FAQs</a></li>
                <li><a tabindex="-1" href="{{ url('feedback') }}">Feedbacks/Suggestions</a></li>
                <li><a tabindex="-1" href="{{ url('advertise') }}">Advertise with us</a></li>
                <li><a tabindex="-1" href="{{ url('blogs/list') }}">Blogs</a></li>
                
              </ul>
            </li>
            
            @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))

              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('model-dashboard/'.Session::get('model_id'))}}" >My Dashboard</a></li>
              <li><a href="{{ URL::to('myprofile/'.Session::get('model_id'))}}"> My {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('agent-dashboard/'.Session::get('agent_id'))}}" >Dashboard</a></li>
              <li><a href="{{ URL::to('agent-profile/'.Session::get('agent_id'))}}"> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list">
              <a  href="{{ url('login') }}">JOIN US</a>
       <!--        <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a tabindex="-1" href="{{ url('/model/create') }}">Model Register</a></li>
              <li><a tabindex="-1" href="{{ url('/agent/create') }}">Agent Register</a></li>

              </ul> -->
              </li>

            @endif

       
            

  </ul>
  <div class="search"><a href="javascript:void(0)" id="search-toggle"></a></div>
  <div class="search_box">
                    <form action="">
                      <input type="search" placeholder="Search.." name="search">
                    </form>
  </div>
</div>

<!-- Mobile View -->
          <div class="mobile_menu">
              <div id="main" class="main_mobile">
                <ul class="mob-contant">
                  <li>
                  <div class="menu_btn" style="font-size:13px;cursor:pointer" onclick="openNav()">
                  <div class="mener">Menu</div>
                  <div class="rights_bar"><div class="icon-bar"></div>
                  <div class="icon-bar last_led"></div>
                </div>
                </div>
                </li>
                <li>
                  <div class="menu">
                    <div class="search"><a href="javascript:void(0)" id="search-toggle-mobile"></a></div>
                     <div class="search_box">
                          <form action="">
                              <input type="search" placeholder="Search.." name="search">
                          </form>
                    </div>
                </div>
                </li>
              </ul>
            </div>
              
            <div id="mySidenav" class="sidenav">
              <ul class="menus magelur_menulist"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                 <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Photo Gallery</a></li>
                
              </ul>
            </li>
            <li class="header_menu_list">
              <a  href="">EVENTS</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
           <li class="header_menu_list">
              <a  href="">ABOUT</a>
             <!--  <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">About Us</a></li>
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Be a Model</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            </li>
              @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))

              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('model-dashboard/'.Session::get('model_id'))}}" >My Dashboard</a></li>
              <li><a href="{{ URL::to('myprofile/'.Session::get('model_id'))}}"> My {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a href="{{ URL::to('agent-dashboard')}}" >Dashboard</a></li>
              <li><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list">
              <a  href="{{ url('login') }}">JOIN US</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a tabindex="-1" href="{{ url('/model/create') }}">Model Register</a></li>
              <li><a tabindex="-1" href="{{ url('/agent/create') }}">Agent Register</a></li>

              </ul> -->
              </li>

            @endif
            </div>
          </div>
</header>
				</div>
			</div>
		</section>

<div class="sm_bars">
	<div class="user_img">
		<a href="javascript:void(0)"><img src="images/barbie.jpg"></a>
	</div>	
 <div class="yell_bar clearfix">
  <div class="barbs_text">
  	<h1>Barbie | <span>Modelling, Photo Shoot,,Video Production, Websites  Online</span></h1>
  </div>
  <div class="socail_torend">
   <ul class="icones">
        <li class="fbook"><a href="javascript:void(0)"></a></li>
        <li class="gpluser"><a href="javascript:void(0)"></a></li>
        <li class="twittera"><a href="javascript:void(0)"></a></li>
        <li class="instagra"><a href="javascript:void(0)"></a></li>
   </ul>
  </div> 	
 </div>	

	<div class="after_login_menu clearfix">
		<ul class="loged_in">
			<li><a href="model-dashboard.htm">dashboard</a></li>
			<li><a href="model-aboutus.htm">About us</a></li>
			<li class="active"><a href="javascript:void(0)">Message</a></li>
			<li><a href="javascript:void(0)">Photos</a></li>
			<li><a href="javascript:void(0)">Video</a></li>
			<li><a href="javascript:void(0)">Network</a></li>
			<li><a href="model-membership.htm">membership</a></li>
		</ul>
		<div class="logout"><a href="index.htm">Logout</a></div>	
	</div>
</div>	
		<!-- Header End -->
</div>




{!! Form::open(array('url'=>'messagelabeldelete', 'class'=>'form-horizontal ajaxLabelDelete','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
	{!! Form::hidden('label_id', '' )  !!}
{!! Form::close() !!}
<input type="hidden" name="urlmodule" id="urlmodule" value="http://192.168.1.65:81/magelur/public/message">
	

<div class="aboutmodel">
 <div class="container">
  <div class="message_two_sec clearfix">
   
   <div class="col-sm-3">
   	<div class="message_left">
		<div class="">
		<div class="sbox-title" style="height:65px"><a href="javascript:void(0);" id="composeMessage" class="btn btn-block btn-lg btn-primary">Compose message</a></div>

		</div>
<ul class=" msaged">
<li class="@if($type == 0) active @endif"><a href="{{ URL::to('message?ty=0') }}">
Inbox
<span  data-inbox="{!! $totalType['0'] !!}" id="cinbox">{!! $totalType['0'] !!}</span>
</a></li>
<li class="@if($type == 1) active @endif"><a href="{{ URL::to('message?ty=1') }}">
Important
<span  data-star="{!! $totalType['1'] !!}" id="cstar">{!! $totalType['1'] !!}</span>
</a></li>
<li class=" @if($type == 2) active @endif"><a href="{{ URL::to('message?ty=2') }}">
Send item
<span  data-sent="{!! $totalType['2'] !!}" id="csent">{!! $totalType['2'] !!}</span>
</a></li>
<li class=" @if($type == 3) active @endif"><a href="{{ URL::to('message?ty=3') }}">
Draft
<span  data-draft="{!! $totalType['3'] !!}" id="cdraft">{!! $totalType['3'] !!}</span>
</a></li>
<li class=" @if($type == 4) active @endif"><a href="{{ URL::to('message?ty=4') }}">
Spam
<span  data-trash="{!! $totalType['4'] !!}" id="ctrash">{!! $totalType['4'] !!}</span>
</a></li>
</ul>
				
   	</div>	
   </div>

   <div class="col-sm-9">
   	<div class="message_box clearfix">
   	 <div class="left_sht_msg">
   	 	<div class="letmsg"><input type="text" id='search' name="search" placeholder="Search"></div>
   	 	<div class="fullmsg_blg">
					
					<table class="table" id='userTbl'>
						<tbody class="dataMessage">
						 @foreach($rowData as $row)
						 <input type="hidden" name="entryby" class="entryby" value="{{ $row->entry_by }}" />
							{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
							@if($row->read == 0 && ($type == 0 || $type == 1)) <tr class="mail-unread" id="msg_{!! $row->id !!}"> @else <tr class="mail" id="msg_{!! $row->id !!}"> @endif
								<td data-type="{!! $type !!}" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}" width="10" style="padding-bottom:10px; padding-right:0px;"></td>
								{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
								<td class="td-clickable text-success">
   	 	  
		 
   	 	   <div class="usertext">
		   <a >{!! $rowUser !!}</a>	
		   <p>{!! substr($row->subject, 0, 30) !!}...</p>
		   </div>
		   <div class="more_dots">
		   <a href="javascript:void(0)"></a>
		   <p>49 min</p>
		   </div>
		 				</td>
								{{--*/ if($isSender == 0 && isset($labels[$row->label_by_recipient])) { $rowLabel = $labels[$row->label_by_recipient]; $rowLabelId = $row->label_by_recipient; } else if($isSender == 1 && isset($labels[$row->label_by_sender])) { $rowLabel = $labels[$row->label_by_sender]; $rowLabelId = $row->label_by_sender; } else { $rowLabel = ''; $rowLabelId = ''; } /*--}}
								<!-- <td class="td-clickable text-"><label class="badge badge-inverse labelDisp" data-id="{!! $rowLabelId !!}" id="label_{!! $row->id !!}">{!! $rowLabel !!}</label> {!! substr($row->subject, 0, 30) !!}</td> -->
								<!-- <td class="td-clickable text-light">{!! substr(strip_tags($row->message), 0, 60) !!}</td> -->
								<!-- <td class="td-clickable text-light">{!! date("m/d/y h:i A", strtotime($row->createdOn)) !!}</td> -->
								<td>
								{!! Form::open(array('url'=>'messageupdate', 'class'=>'form-horizontal ajaxMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
									{{--*/ if($type == 0) $rowStarred = $row->starred_by_recipient; else if($type == 2) $rowStarred = $row->starred_by_sender; else $rowStarred = 0; /*--}}
									{!! Form::hidden('starred', $rowStarred )  !!}
									{!! Form::hidden('update_function', '' )  !!}
									{!! Form::hidden('msg_type', $type )  !!}
									{!! Form::hidden('msg_id', $row->id )  !!}
									@if($type == 0 || $type == 2)
									<!-- <i class="fa fa-star msgstar @if($rowStarred == 1) active @endif"></i> -->
									@endif
								{!! Form::close() !!}
								</td>
							</tr>
			
						 @endforeach
						</tbody>
					</table>
					{!! Form::open(array('url'=>'messagegroupupdate', 'class'=>'form-horizontal ajaxGroupMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('msg_ids', '' )  !!}
						{!! Form::hidden('is_sender', '' )  !!}
						{!! Form::hidden('label_id', '' )  !!}
						{!! Form::hidden('label_name', '' )  !!}
						{!! Form::hidden('group_update_function', '' )  !!}
						{!! Form::hidden('type', $type )!!}
					{!! Form::close() !!}
   	 	
		</div> 
   	 </div>
@foreach($rowData as $row)
		{{--*/ if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; /*--}}
		<div class="hidden viewMessageDiv" id="viewMessageDiv_{!! $row->id !!}">
		<div class="msg_right_box ">
   	  	
   	  <div class="total_newer clearfix">

	   	  <div class="renew ">
<button class="btn  gotodataMessageDiv" type="">
<i class="fa fa-times-circle"></i>
</button>
		   	  
					<h3>Re: New offer</h3>
	   	  	{{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
				{{--*/ if($isSender == 1 && isset($usersEmailList[$row->recipient])) { $rowUserEmail = $usersEmailList[$row->recipient]; } else if($isSender == 0 && isset($usersEmailList[$row->entry_by])) { $rowUserEmail = $usersEmailList[$row->entry_by]; } else { $rowUserEmail = ''; } /*--}}
				{{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}
	   	  	<p><span class="for_span">from</span> <span class="ed_span">{!! $rowUser." (".$rowUserEmail.")" !!}</span></p>
			<p><span class="for_span">to</span> <span class="ed_span">Bojan Herceg</span></p>
	   	  </div>
	   	  <div class="minew">
	   	   <p>Today 11:45</p>
	   	   <div class="model_icoded">
	   	   	<ul class="stuningeder">
	   	   	 <li class="shield"><a href="javascript:void(0)"></a></li>
	   	   	 <li class="warn"><a href="javascript:void(0)"></a></li>
      			@if($type != 4 )	
      					<li class="delets singleMessageDelete" type="button" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}"><a href="javascript:void(0)">
      			@endif
	   	   	 </a></li>
	   	   	</ul>
	   	   </div>
	   	  </div>
   	 </div>
   	 <div class="newtent_content viewMessageContent">
   	 	<p>{!! $row->message !!}</p>
   	 	
   	 </div>
   	 <div class="rader clearfix">
   	 <div class="minyues">
      
@if($row->ppt !='')
<div class="homepps">
<a href="javascript:void(0)"><span class='homeppt'>{!! $row->ppt !!}</span></a>
</div>
@else
<div class="right-wrapper">
<span class="full-border"></span>
<div class="uploads">
<div class="job_fileUploads btn hvr-shutter-in-vertical">
<span @if(count($row->ppt)>0) class="hide" @endif>{{ Lang::get('core.updateCV') }}</span>                    
<input type="file" name="pptupload" id="pptupload" data-url="{!! url().'/user/ppt/save/'.$row->id !!}" />                
</div> 
<div class="progress pro"><div class="progress-bar progress-bar-success progress-bar-striped" id="probars" style="width:0%"></div></div> 
</div>                         
<div class="upload-delete hide">
<a class="browse-butt" id="cvdelete"><i class="fa fa-retweet"></i>{!! Lang::get("core.Change")!!} </a>
</div>
</div>
<div class="homepps">
<a href="javascript:void(0)"><span class='homeppt'></span></a>
</div> 
@endif

@if($row->pdf !='')
<div class="aboutpdf">
<a href="javascript:void(0)"><span class='aboutpdfs'>{!! $row->pdf !!}</span></a>
</div>
@else
<div class="right-wrapper">
<span class="full-border"></span>
<div class="upload">
<div class="job_fileUpload btn hvr-shutter-in-vertical">
<span @if(count($row->pdf)>0) class="hide" @endif>{{ Lang::get('core.updateCV') }}</span>                    
<input type="file" name="cvupload" id="cvupload" data-url="{!! url().'/user/resume/save/'.$row->id !!}" />                
</div> 
<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" id="probar" style="width:0%"></div></div> 
</div>                         
<div class="upload-delete hide">
<a class="browse-butt" id="cvdelete"><i class="fa fa-retweet"></i>{!! Lang::get("core.Change")!!} </a>
</div>
</div>
<div class="aboutpdf">
<a href="javascript:void(0)"><span class='aboutpdfs'></span></a>
</div>
@endif

	   	 </div>
	   	 	 <div class="right_downlioad">
   	  <a href="javascript:void(0)"></a>
	</div> 
	</div>
@if($type != 4 && $type != 3)	
<div class="right_multi arros"> 
	<ul>
	<li class="right_multi_left sendReplyForward" type="button" data-btn="reply" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
	</a></li>
	<li class="right_multi_middle"><a href="javascript:void(0)"></a></li>
	<li class="right_multi_right sendReplyForward" type="button" data-btn="forward" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
		<a href="javascript:void(0)">
	</a></li>
	</ul>	
</div>
@endif
   	 </div>
		</div>
	
   	
	@endforeach
	</div>  	 	
   	</div>	
   </div>

  </div>	
 </div>	
</div>	
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="max-width:800px;width:90%;">
	  <div class="modal-content">
		<div class="modal-header bg-default">
			
			<button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-titles">{!! Lang::get('core.compose') !!} </h4>
		</div>
		<div class="load_notify"></div>
		<div class="modal-body" id="compose-modal-content">
			 {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<input type="hidden" name="typee" class="type" />
				<div class="form-group">
					<label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
					<div class="col-xs-10">
						
					<div><!-- {!! Form::select('recipient', $users, null, array('class'=>'form-control select2 width100p recipientselect2', 'style'=>'padding:0px;', 'required'=>'' ) ) !!} -->
<select name='recipient' class='form-control select2 width100p recipientselect2' style='padding:0px;' required>
@foreach($users as $key=>$val)
<option  value ="{{ $key }}" @if($key == 1) selected='selected' @endif>{{ $val }}</option>"; 
		
@endforeach 
</select>
				</div>
					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
					<div class="col-xs-10">
						<div>{!! Form::text('subject', null, array('class'=>'form-control', 'placeholder'=>Lang::get('core.fr_emailsubject'), 'required'=>'', 'pattern'=>'[a-zA-Z0-9]+')) !!}</div>
					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">Message</label>
					<div class="col-xs-10">
						<div><textarea name='message' rows='3' style="width:100%;" class='form-control mceEditor' placeholder="{{ Lang::get('core.message')}}"></textarea></div>
						<span class="error" style="color:red"></span>
					</div>
				</div>
				<div class="sending_loadergs"></div>
				<div class="form-group">
					<label class="col-xs-2"></label>
					<div class="col-xs-10">
						{!! Form::hidden('draft', '0' )  !!}
						{!! Form::hidden('messageid', '' )  !!}
						<button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i>Send message</button>
						<button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
					</div>
				</div>
			 {!! Form::close() !!}
		</div>

	  </div>
	</div>
</div>
<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header bg-default">
		
		<button type="button " class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">Modal title</h4>
	</div>
	<div class="modal-body" id="bsetec-modal-content">

	</div>

  </div>
</div>
</div>
<div class="map">
 <a href="javascript:void(0)"><img src="images/map.jpg"></a>
</div>

<!--  -->
<section class="about_footer">
		<div class="footer">
			 <div class="container">
			  <div class="clearfix">
			   <div class="col-sm-7 wow SlideInUp">
			    <div class="post">
			     <p>Po Box 315000 Great Avn New York / USA</p>
			     <ul>
			      <li><a href="index.htm">HOME</a></li>
			      <li><a href="javascript:void(0)">ABOUT US</a></li>
			      <li><a href="ourmodel.htm">OUR MODELS</a></li>
			      <li><a href="applynow.htm">apply now</a></li>
			      <li><a href="javascript:void(0)">news & press</a></li>
			      <li><a href="contactus.htm">CONTACT</a></li>
			     </ul>
			    </div>
			   </div>
			   <div class="col-sm-5  wow SlideInDown">
			    <div class="mail_adress clearfix ">
			     <div class="leftmail"><input type="text" name="" placeholder="Email Address"><!-- <p>Email Address</p> --></div>
			     <div class="submit_mail"><a href="javascript:void(0)">Submit</a></div>
			    </div>
			    <div class="social_icons">
			     <ul>
			      <li>we are social</li>
			      <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
			      <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
			      <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
			      <li><a href="javascript:void(0)"><i class="fa fa-youtube"></i></a></li>
			     </ul>
			    </div>
			   </div>
			  </div>
			 </div>
		</div>
		</section>
		<!--  -->


</html>
<script>
$(document).ready(function(){

	
	$('[name="recipient"]').select2();
	
	$(function(){
		tinymce.init({	
			mode : "specific_textareas",
			editor_selector : "mceEditor",
			theme : "advanced",
			theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
		 });	
	});
	
	$('.sendbtn').click(function(){
		$('.type').val(0);
	});
	$('.savedraft').click(function(){
		$('.type').val(3);
	});
	function tinyClean(value) {
	value = value.replace(/&nbsp;/ig, ' ');
	value = value.replace(/\s\s+/g, ' ');
	if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
		value = '';
	}
	return value;
}
	$('.sendMessage').submit(function(){

		var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
		var typee = $('[name="typee"]').val();
		
		if(value==''){
			$('.error').html("Message Field is required");
		}else{
			$('.error').html('');
		}
		
		if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !='' && value !=''){
			$('.sendbtn').attr('disabled','disabled');
			$('.savedraft').attr('disabled','disabled');
			var recipient = $('[name="recipient"]').val();
			var url = $(this).attr('action');
			tinyMCE.triggerSave();
			$.ajax({
				type: 'POST',
				url: url,
				data: $(".sendMessage").serialize(),
				beforeSend:function(){
					$('.sendbtn').hide();
					$('.savedraft').hide();
					$('.sending_loadergs').append('<h2><i class="fa fa-refresh fa-spin"></i> Loading...</h2>');					
				},
				success : function(response) {
					var data = jQuery.parseJSON( response );
					if(data.status == '1') {
						toastr.success("Message sent Successfully");
					} else {
						toastr.error("Label name already exist");
					}
					$('.sendbtn').removeAttr('disabled');
					$('.savedraft').removeAttr('disabled');
					$('#compose-modal').modal("toggle");					
					$('.sendbtn').show();
					$('.savedraft').show();
					if(typee==0){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)+parseInt(1);
						$('#csent').html(sentt);
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)+parseInt(1);
						$('#cdraft').html(draftt);
					}
				},complete: function() {
				$('.sending_loadergs').html('');
			}
			});
		}
		return false;
	});
	$('.ajaxMessageUpdate').submit(function(){
		var starred = $(this).children('[name="starred"]').val();
		if(starred == '1'){
			$(this).children('[name="starred"]').val('0');
		} else {
			$(this).children('[name="starred"]').val('1');
		}
		var url = $(this).attr('action');
			$(this).children('.msgstar').toggleClass('active');
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				var star =$('#cstar').attr('data-star');
				var starr;
				if(response==1){
					starr =parseInt(star)+parseInt(response);					
				}else if(response==0){
					 starr =parseInt(star);
				}
				 $('#cstar').html(starr);
				
			}
		});
		return false;
	});
	$('.msgstar').click(function(){
		$(this).parent('form').children('[name="update_function"]').val('starred');
		$(this).parent('form').submit();
	});
	
	$('.sendbtn').click(function(){
		$('[name="draft"]').val('0');
	});
	$('.savedraft').click(function(){
		$('[name="draft"]').val('1');
	});
	
	$('#composeMessage').click(function(){
		
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="recipient"]').select2('val','');
		$('#compose-modal').find('[name="messageid"]').val('');
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	$('.gotodataMessageDiv').click(function(){
		$('.viewMessageDiv').addClass('hidden');
		$('.dataMessageDiv').slideDown();
	});
	$('.sendReplyForward').click(function(){
		var msgid = $(this).attr('data-id');
		var msgtype = $(this).attr('data-type');
		var issender = $(this).attr('data-sender');
		var option = $(this).attr('data-btn');
		
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="recipient"]').select2('val','');
		$('#compose-modal').find('[name="messageid"]').val('');
		
		if(option == 'reply'){
			if(msgtype == 0) {
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
			} else if(msgtype == 2) {
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
			} else if(msgtype == 1 || msgtype == 5) {
				if(issender == 1){
					$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
				} else {
					$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
				}
			}
		} else if(option == 'forward'){
			$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
			tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
		}
		
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('.dataMessage tr .td-clickable').click(function(){
    
		var msgid = $(this).parent('tr').find('td:nth-child(1)').attr('data-id');
		var msgtype = $(this).parent('tr').find('td:nth-child(1)').attr('data-type');
		if(msgtype == '3'){
			$('.sendMessage')[0].reset();
			$('#compose-modal').find('[name="recipient"]').select2('val','');
			$('#compose-modal').find('[name="messageid"]').val('');
			
			$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
			var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
			var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
			$('#compose-modal').find('[name="recipient"]').select2('val',recipientid);
			$('#compose-modal').find('[name="messageid"]').val(msg_id);
			tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
			
			$('.sendbtn').removeAttr('disabled');
			$('.savedraft').removeAttr('disabled');
			$('#compose-modal').modal({
				backdrop: 'static',
				keyboard: false
			});
		} else {
			if($(this).parent('tr').hasClass('mail-unread')){
				$(this).parent('tr').find('form').children('[name="update_function"]').val('read');
				$(this).parent('tr').find('form').submit();
				$(this).parent('tr').attr('class','mail');
			}
			$('.dataMessageDiv').hide();
			$('#viewMessageDiv_'+msgid).removeClass('hidden');
		}
	});
	$('.ajaxGroupMessageUpdate').submit(function(){
		var url = $(this).attr('action');
		var ids = $(this).children('[name="msg_ids"]').val();
		var message_ids = ids.split(',');
		var typee=$('[name="type"]').val();
		if($(this).children('[name="group_update_function"]').val() == 'delete' || $(this).children('[name="group_update_function"]').val() == 'deleteTrash'){
			jQuery.each(message_ids, function(index, item) {
				$('#msg_'+item).remove();
			});
			$('.gotodataMessageDiv').trigger('click');
		} else if($(this).children('[name="group_update_function"]').val() == 'label'){
			var label_name = $(this).children('[name="label_name"]').val();
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text(label_name);
			});
		} else if($(this).children('[name="group_update_function"]').val() == 'removelabel'){
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text('');
			});
		}
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				$('.ajaxGroupMessageUpdate')[0].reset();
				if(response!='label')
				{
					var trash = $('#ctrash').attr('data-trash');
					var total =$('#page_total').attr('data-total');
					var res = parseInt(trash)+parseInt(response);
					//$('#page_total').html(res);
					$('#ctrash').html(res);
					if(typee==0){
						var inbox =$('#cinbox').attr('data-inbox');
						var inboxx = parseInt(inbox)-parseInt(response);					
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cinbox').attr('data-inbox',inboxx).text(inboxx);
					}else if(typee==1){
						var star =$('#cstar').attr('data-star');
						var starr =parseInt(star)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cstar').attr('data-star',starr).text(starr);
						
					}else if(typee==2){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#csent').attr('data-sent',sentt).text(sentt);
						
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cdraft').attr('data-draft',draftt).text(draftt);
					}else if(typee==4){
						trash = $('#ctrash').attr('data-trash');
						res = parseInt(trash)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#ctrash').attr('data-trash',res).text(res);
					}
				}
				
			}
		});
		return false;
	});
	$('#deleteMessage').click(function(){
		var msg_ids = '';
		var is_sender = '';

		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
					
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('#deleteFromTrash').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to remove selected messages permanently?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('deleteTrash');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('.singleMessageDelete').click(function(){
		var msg_ids = $(this).attr('data-id');
		var is_sender = $(this).attr('data-sender');
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
		
	$('#removeLabels').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to remove this label from selected messages?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('removelabel');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	
	$(document).on('click','ul#label-dropdown-menu li',function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to apply this label to selected messages?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="label_id"]').val($(this).children('a').attr('data-id'));
				$('[name="label_name"]').val($(this).children('a').text());
				$('[name="group_update_function"]').val('label');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	
	$('#AddLabel').click(function(){
		$('.saveLabel')[0].reset();
		$('.savelabelbtn').removeAttr('disabled');
		$('#label-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('#editLabel').click(function(){
		$('.saveLabel')[0].reset();
		$('.savelabelbtn').removeAttr('disabled');
		$('.saveLabel').find('[name="id"]').val($('ul#label-list-group').find('li.active').children('a').attr('data-id'));
		$('.saveLabel').find('[name="label"]').val($('ul#label-list-group').find('li.active').children('a').text());
		$('#label-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('#deleteLabel').click(function(){
		var currentLabel = $('ul#label-list-group').find('li.active').children('a').attr('data-id');
		if (currentLabel != '') {
			if (confirm("Are you sure you want to delete this label?")) {
				$('.ajaxLabelDelete').find('[name="label_id"]').val(currentLabel);
				$('.ajaxLabelDelete').submit();
			}
		}
		return false;
	});
	
	$('.ajaxLabelDelete').submit(function(){
		var url = $(this).attr('action');
		var urlmodule = $('[name="urlmodule"]').val();
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			beforeSend:function(){
				toastr.info("Label Deleted Successfully");
			},
			success : function(response) {
				window.location.href = urlmodule;
			}
		});
		return false;
	});
	
	$('.saveLabel').submit(function(){
		if($.trim($('[name="label"]').val()).length>1){
			$('.savelabelbtn').attr('disabled','disabled');
			var url = $(this).attr('action');
			var labelname = $('.saveLabel').find('[name="label"]').val();
			var labelid = $('.saveLabel').find('[name="id"]').val();
			var urlmodule = $('[name="urlmodule"]').val();
			$.ajax({
				type: 'POST',
				url: url,
				data: $(".saveLabel").serialize(),
				beforeSend:function(){
					// toastr.info('Sending Message');
				},
				success : function(response) {
					var data = jQuery.parseJSON( response );
					if(data.status == '1') {
						if(labelid == ''){
							$('ul#label-list-group').append('<li class="list-group-item"><a href="'+urlmodule+'?ty=5&lb='+data.newid+'" data-id="'+data.newid+'">'+labelname+'</a></li>');
							$('ul#label-dropdown-menu').append('<li><a href="javascript:void(0);" data-id="'+data.newid+'">'+labelname+'</a></li>');
						} else {
							$('.labelTitleDisp').text(labelname);
							$('.labelDisp').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
							$('ul#label-dropdown-menu li a').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
							$('ul#label-list-group li a').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
						}
						
						toastr.success("Label updated Successfully");
					} else {
						toastr.error("Label name already exist");
					}
					$('.savelabelbtn').removeAttr('disabled');
					$('#label-modal').modal("toggle");
				}
			});
		}
		return false;
	});
});	

// $('#search').on('keyup',function(){
// var value=$(this).val();
// console.log(value);
// $.ajax({
// type:'get',
// url: "{{URL::to('show')}}",
// data:{'search':value},
// success:function(data){
// $('tbody').html(data);
// }
// });
// })

$(document).ready(function(){
    $('#search').on('keyup',function(){
        var searchTerm = $(this).val().toLowerCase();
              // alert(searchTerm);

        $('#userTbl tbody tr').each(function(){
            var lineStr = $(this).text().toLowerCase();
            if(lineStr.indexOf(searchTerm) === -1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    });
});
    // cv upload process
  $(document).ready(function(){
      $('#cvupload').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(pdf)$/i,
        dataType:'json',
        maxFileSize: 1024000000, // 1 GB
        progress: function (e, data) {
                    $('.upload-delete').addClass('hide');
                    $('.job_fileUpload').addClass('hide');
                    $('.upload').removeClass('hide');
          $('#cv_name').text("");
                    $('#cv_date').html("");
                    $('.progress').removeClass('hide');
          var percentage = parseInt(data.loaded / data.total * 100);
          $('#probar').css('width',percentage+'%');
          if(percentage == '100') {
            $('#cv_name').text('{!! Lang::get("core.lecture_file_process")!!}');
            // $('.aboutpdf').text('uploaded');
          }
        },
        processfail: function (e, data) {
          file_name = data.files[data.index].name;
          alert("File not allowed");   
        },
        done: function(e, data){
          var return_data = data.result;
          if(return_data.status){
                        $('.progress').addClass('hide');
            $('#probar').css('width','0%');
            $(".uploadedfile").removeClass('hide');
            $('#cv_id').val(return_data.file_id);
            $('.aboutpdfs').text(return_data.file_title + '(' + return_data.file_size + ')');
            $('#cvdownload').attr('href',return_data.download_url);
              $('#cv_date').html('<span>On '+return_data.cv_date+'</span>');
            if(return_data.file_duration!='') 
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size +' - ' + return_data.file_duration + ' ) ');
            else
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size + ' ) ');

            $('.upload').addClass('hide');
            $('.upload-delete').removeClass('hide');
          }else{
            alert("invalid file.");
          }
        }
      });

   
  });

$(document).ready(function(){
      $('#pptupload').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(ppt)$/i,
        dataType:'json',
        maxFileSize: 1024000000, // 1 GB
        progress: function (e, data) {
                    $('.upload-delete').addClass('hide');
                    $('.job_fileUploads').addClass('hide');
                    $('.uploads').removeClass('hide');
          $('#cv_name').text("");
                    $('#cv_date').html("");
                    $('.pro').removeClass('hide');
          var percentage = parseInt(data.loaded / data.total * 100);
          $('#probars').css('width',percentage+'%');
          if(percentage == '100') {
            $('#cv_name').text('{!! Lang::get("core.lecture_file_process")!!}');
            // $('.aboutpdf').text('uploaded');
          }
        },
        processfail: function (e, data) {
          file_name = data.files[data.index].name;
          alert("File not allowed");   
        },
        done: function(e, data){
          var return_data = data.result;
          if(return_data.status){
             $('.pro').addClass('hide');
            $('#probars').css('width','0%');
            $(".uploadedfile").removeClass('hide');
            $('#cv_id').val(return_data.file_id);
            $('.homeppt').text(return_data.file_title + '(' + return_data.file_size + ')');
            $('#cvdownload').attr('href',return_data.download_url);
              $('#cv_date').html('<span>On '+return_data.cv_date+'</span>');
            if(return_data.file_duration!='') 
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size +' - ' + return_data.file_duration + ' ) ');
            else
              $('#cv_name').text(return_data.file_title +' ( ' +return_data.file_size + ' ) ');

            $('.uploads').addClass('hide');
            $('.upload-delete').removeClass('hide');
          }else{
            alert("invalid file.");
          }
        }
      });

   
  });
</script>	

<script type="text/javascript">
$(".lazy").lazyload({effect:"fadeIn"});
</script>
 
<span role="status" aria-live="polite" class="select2-hidden-accessible"></span>
