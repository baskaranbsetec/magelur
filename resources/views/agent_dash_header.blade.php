

<link href="{{ asset('frontend') }}/default/css/croppie.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/croppie.js"></script>
<script src="{{ asset('magelur/js/tagsly.js')}}"></script>
<link href="{{ asset('magelur/css/tagsly.css')}}" rel="stylesheet" type="text/css">
@foreach($agents as $agent)
	<div class="magelur_about model_abouts magelur_profile_blk">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class=" ">
				<div id="upload-demo-i-cover">			
					{!! SiteHelpers::user_avatars($agent->id,'agent',$agent->profile_cover,'/uploads/agency_profile/cover/') !!}
                </div>
				</div>
				<div class="editbtn_div_cover" data-toggle="modal" data-target="#myCover" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
			</div>
		</section>

<div class="sm_bars">
    <div class="user_img">
        <a href="javascript:void(0)">
            <div id="upload-demo-i">
            <!-- {!! SiteHelpers::user_profiles($agent->id,'agent',$agent->profile_photo,'/uploads/agency_profile/') !!} -->
           @if($agent->profile_photo !='')
           {!! SiteHelpers::user_profiles($agent->id,'agent',$agent->profile_photo,'/uploads/agency_profile/') !!}
           @else
            <img src="{!! \SiteHelpers::user_agent_profile_sizes($agent->id,'_small','png') !!}">
            @endif
           </div>   <!-- <img src="{{ asset('/images/barbie.jpg') }}"> -->
        <div class="editbtn_div" data-toggle="modal" data-target="#myAvatar" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
    </a>
    </div>  
		
 <div class="yell_bar clearfix">
  <div class="barbs_text">
  	<h1> @if ($agent->username == '') Not Specified @else {{ strtoupper($agent->username) }} @endif  @if ($agent->agency_name != '')  <span>  {{$agent->agency_name}} </span>  @else @endif | <span> 
      @if (is_numeric($agent->country))
        {{ \SiteHelpers::getName('country',$agent->country) }} 
      @else
     {{ $agent->country }}
      @endif
    </span></h1>
  </div>
  <div class="socail_torend">
   <ul class="icones">

        <!--<li class="fbook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ url('agent-profile/'.$agent->id)}}&t={{ url('agent-profile/'.$agent->id)}}"
            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
            target="_blank" title="Share on Facebook">
          </a></li>-->

          <!-- <li class="fbook">
            <a href="http://www.facebook.com/sharer.php?u={{ url('agent-dashboard/'.$agent->id)}}" target="_blank" title="Share on Facebook">
          </a></li>

          <li class="gpluser">
             <a href="https://plus.google.com/share?url={{ url('agent-dashboard/'.$agent->id)}}" target="_blank" title="Share on Gplus"></a>
           </li>
           <li class="twittera">
             <a href="https://twitter.com/share?url={{ url('agent-dashboard/'.$agent->id)}}" target="_blank" title="Share on Twitter">
             </a>
           </li> -->
  
        <!--<li class="gpluser">
        <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://plus.google.com/share?url={{ url('agent-profile/'.$agent->id)}}">
        </a>
        <li>
        <li class="twittera"><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/share?url={{ url('agent-profile/'.$agent->id)}}"></a></li>-->
   </ul>
  </div> 
 </div>	


<!--  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav> -->

 <nav class="navbar navbar-inverse model_dash_menu">
 <div class="logout"><a href="{{ URL::to('user/logout')}}">Logout</a></div>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="menu_responsive">Dashboard</span>
        <span class="icon-bar"><i class="fa fa-list"></i></span>                        
      </button>
    </div>
	<div class="after_login_menu clearfix collapse navbar-collapse" id="myNavbar">
          {{--*/ $AgentId=Session::get('url_securityID') /*--}}

		<ul class="loged_in nav navbar-nav ">
			<li class="{{ Request::path() == 'agent-dashboard/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('agent-dashboard/'.$AgentId)}}">Dashboard</a></li>
			<li class="{{ Request::path() == 'agent-profile-edit/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('agent-profile-edit/'.$AgentId)}}">Profile</a></li>
			<li class="{{ Request::path() == 'message' ? 'active' : '' }}"><a href="{{ URL::to('message')}}">Message</a></li>
			<li class="{{ Request::path() == 'events/update/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('my/events')}}">Event</a></li>
			<li class="{{ Request::path() == 'agent-photos' ? 'active' : '' }}"><a href="{{ URL::to('agent/album/'.$AgentId)}}">Photos</a></li>
			<li class="{{ Request::path() == 'agent_video/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('agent/video/'.$AgentId)}}">Video</a></li>
			<li class="{{ Request::path() == 'agent-network/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('agent-network/'.$AgentId)}}">Network</a></li>
			<li class="{{ Request::path() == 'agent-membership/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('agent-membership/'.$AgentId)}}">membership</a></li>
<li class="{{ Request::path() == 'favourite-photos/'.$AgentId ? 'active' : '' }}"><a href="{{ URL::to('favourite-photos/'.$AgentId)}}">Favourites</a></li>
		</ul>
  </div>
  </nav>
	
</div>	
    	<!-- Header End -->
 </div>


@endforeach
@include('profile_cover_popup')

<script type="text/javascript">
$('body').addClass('dash_nav');

</script>

<style type="text/css">
.empty_vertical {margin: 90px auto 0px;}
</style>