
<script src="{{ asset('magelur/js/videos/ajax.votes.js')}}"></script>
<script src="{{ asset('magelur/js/videos/ajax.comments.js')}}"></script>

<!-- start rating plugin -->
<script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
<link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>
<script src="{{ asset('magelur/js/star_rating.js')}}"></script>

<div class="watch_page video_view_innerdiv video_upload_page photo_view_page col-md-12 col-sm-12 col-xs-12">
	<div class="container video_view_container">
		<div class="col-md-8 col-sm-12 col-xs-12 video_upload_left ">
			<!--####################### Left Side DIV #######################-->
			<div class="w_leftSide col-md-12 col-sm-12 col-xs-12">
				<!--Photo Image Div -->
					<div class="photo_view_blk">	
										
					<img src="{!! \SiteHelpers::user_albums($image->ID,'',$image->Ext) !!}" alt="file_image" style="object-fit:fill" />
					</div>
				<!-- Video header -->
				<div class="video_header">
					<!-- Video Title -->
					<div class="video_title">
						<h2 title='{{ $image->Title }}'>{{ $image->Title }}</h2>

						@if($image->OwnerId == \Session::get('logged_id') && $image->user_type == \Session::get('user_type'))
						<!-- favourite -->
						@else
						<div class="favourites">
						  <a onclick="addToFavourite()" title="Add to favourite"><i class="fa fa-heart"></i></a>
						</div>
						@endif
						
						@if($image->OwnerId == $userId && $image->user_type == \Session::get('user_type'))
						<div class="del_image" style="float:right">
							<!-- {{ url('photo/photodelete/'.$image->ID.'/' )}} -->
						<h1><a href="javascript:void(0)" onclick="javascript:addToDelete()" title="Delete">
                  		<i class="fa fa-trash"></i></a></h1>
              			</div>@endif
              			
						<h3><i class="fa fa-eye" title='Views'></i>{{ App\Helper\Helper::count_photo_views($image->ID) }}</h3>

							<!-- Votting System -->
						<div class="votting_system">
							<input type="hidden" name="vote_type" id="vote_type" value="photo" />
							<!-- Like Button -->
							@if (\Session::get('logged_id'))
							@if (App\Helper\Helper::already_liked_photos($image->ID) == TRUE)
							<button style="opacity:1;" class="vote_like" data-video="{{ $image->ID }}" id="like">
								<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span>	{{ $likes }}
							</button>
							@else
							<button class="vote_like" data-video="{{ $image->ID }}" id="like">
								@if($likes==0)
								<span class="icon_span_div"><i class="fa fa-thumbs-up" title='Like'></i></span>
								@else
								<span class="icon_span_div"><i class="fa fa-thumbs-up" title='Already Liked'></i></span>
								@endif {{ $likes }}
							</button>
							@endif
							@else
							<button class="vote_like" data-video="{{ $image->ID }}" id="like">
								<span class="icon_span_div"><i class="fa fa-thumbs-up"></i></span>	{{ $likes }}
							</button>
							@endif


							<!-- Dislike Button -->
						<!-- 	@if (\Session::get('logged_id'))
							@if (App\Helper\Helper::already_disliked_photos($image->ID) == TRUE)
							<button style="opacity:1;" data-video="{{ $image->ID }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@else
							<button data-video="{{ $image->ID }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@endif
							@else
							<button data-video="{{ $image->ID }}" class="vote_dislike" id="dislike">
								<span class="icon_span_div"><i class="fa fa-thumbs-down"></i></span>{{ $dislikes }}
							</button>
							@endif	 -->
						</div>
						
              		

						
					</div>
					
					<!-- User panel -->
					<div class="user_panel">
						<div class="post-action">
							<!-- Rating -->
							<!-- <select class='rating' id='rating_'  data-id='rating_'>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							<option value="5" >5</option>
							</select> -->
							</div>
							<script type="text/javascript">
                            // set rating for each star
							// setRating($('.rating'),'{{$avg_ratings}}');
							</script>
						<!-- User channel avatar -->
						<div class="channel_avatar">
						<img src="{{ App\Helper\Helper::user_avatar($image->OwnerId,$image->user_type) }}">
						</div>

						<!-- Channel name and subscribers counter -->
						<div class="channel_counter">
							
							<!-- Channel name -->
							<div class="channel_name">
								{{--*/ $UName= App\Helper\Helper::channel_by_id($image->OwnerId,$image->user_type) /*--}}
								<a href="{{ App\Helper\Helper::get_user_profile_link($image->OwnerId,$image->user_type,$UName) }}">{{ App\Helper\Helper::channel_by_id($image->OwnerId,$image->user_type) }}</a>

								<!-- User Verification -->
								@if (App\Helper\Helper::is_verified($image->OwnerId,$image->user_type) == TRUE)
								<span class="verified" data-balloon="{{ Lang::get('core.verified') }}" data-balloon-pos="right">
									<span class="success_icon"><i class="fa fa-check-circle"></i></span>
								</span>
								@endif
								</div>

							<!-- Subscribers counter -->
							<div class="subscribers_counter">
								<span id="totalSubscribers">
									{!! nl2br(App\Helper\Helper::active_links($image->Description, $image->ID)) !!}
								</span>
								<!-- <a href="javascript:void(0);" class="read_more_tag">Read more</a> -->
							</div>
						</div>
						<span class="video_update_date">{{$avg_ratings}}</span>

						<!-- <span class="video_update_date">{{ Lang::get('core.uploaded_on') }} {{ App\Helper\Helper::description_date($image->Created) }}</span> -->
					</div>
					<!-- Views and votting counter -->
					<div class="viewVoteCounter">
						<!-- Votting  Line -->
						<div class="sparkbars">
							<div class="sparkbars-likes"></div>
							<div class="sparkbars-dislikes"></div>
						</div>
					
					</div>



				</div>	
					<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk video_upload_desc_blk">
					<!-- Video Description -->
					<div class="video_description">
						
						<!-- Publish Date -->
						<div class="publish_date">
							
							<h1>{{ Lang::get('core.share_your_photo') }}</h1>
						</div>

						<!-- Embed & Flag Video -->
						<div class="embed_flag" style="display:none">
							<a class="embed_vid" id="shareVid">{{ Lang::get('core.share_image') }}</a>
							<a href="#" class="flag_vid" id="reportVid" data-video="{{ $image->ID }}"></a>
						</div>

						<div class="v_description" id="videoDescription">
							
							<!-- Embed Video -->
							<div class="embedVideo">

								<ul>
									<li>
										<p>
										 Amazing. Here we had a numerous way to reach your image. The simple thing you need to is click on the below social sharing buttons, to post your links on other social sharing sites.By doing this you can get more attention from new users
									    </p>
								</li>

									<!-- Share on Facebook -->
									<li><!--<a href="http://www.facebook.com/sharer.php?u={{ url('/photo') }}/{{ $image->MediaUri }}" target="_blank">-->
										<a href="http://www.facebook.com/sharer.php?u={{ url('/photo') }}/{{ $photo_enc_id }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"	target="_blank" >
										<img src="{{ url('/') }}/content/assets/img/social/facebook.png">
									</a></li>

									<!-- Share on Twitter -->
									<li><a href="https://twitter.com/share?url={{ url('/photo') }}/{{ $image->ID }}&amp;text={{$image->Title}}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/twitter.png">
									</a></li>

									<!-- share on google plus -->
									<li><a href="https://plus.google.com/share?url={{ url('/photo') }}/{{ $photo_enc_id }}" target="_blank">
										<img src="{{ url('/') }}/content/assets/img/social/google.png">
									</a></li>
								</ul>
								<!-- <textarea>&lt;embed src="http://192.168.1.65:81/magelur/public/embed/57797EA90C" height="400" width="600"&gt;</textarea> -->
								</div>
							</div>

							

			<!-- <div class="show_more">
				<a id="showMore">Show more</a>
			</div> -->

		</div>
	</div>



	<!-- Video Comments -->
	<!-- Video Comments -->
	<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk nopadding_div">
		<div class="comments_box video_comments_box">
			<div class="comments_counter">
				<p>{{ Lang::get('core.comments') }} <span>•</span> {{ $total_comments }}</p>
			</div>

			<div class="cm_box">
				<div class="cm_avatar">
					@if (\Session::get('logged_id'))
					<img src="{{ App\Helper\Helper::user_avatar(\Session::get('logged_id'),\Session::get('user_type')) }}" alt="">
					@else
					<img src="{{ url('/') }}/content/uploads/avatars/avatar.png" alt="">
					@endif
				</div>

				<div class="cm_textarea">

					<form action="#" method="POST" id="SubmitComment">
						<meta name="csrf-token" content="{{ csrf_token() }}">
						<p class="rate_article" style="display:none;"> Rate Article</p>
						<input type="hidden" id="comment_type" name="type" value="photo" />
						<textarea name="comment" maxlength='500' placeholder="{{ Lang::get('core.add_comment') }}" id="commentsBox"></textarea>
						<select id="comment_rating" style="display: none;">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>


						<span style='display:none' class='loader'>
						<img  src="{{ asset('magelur/message/fancybox_loading.gif') }}" />
						</span>
						<button id="postComment" data-video="{{ $image->ID }}" type="submit">
							<span>{{ Lang::get('core.post_comment') }}</span></button>
					</form>

				</div>

				<div class="clear"></div>
			</div>

			<!-- List of Comments -->
			<div class="comments-container" id="commentScroll">

				<ul class="comments-list">
					@if (count($comments) > 0)
					@foreach ($comments as $comment)
					
					<li class="commentScroll">
						<div class="comment-main-level">
							<!-- Avatar -->
							<div class="comment-avatar">
								<img src="{{ App\Helper\Helper::user_avatar($comment->user_id,$comment->user_type) }}" alt=""></div>
								<!-- Comment Details -->
								<div class="comment-box">
									<div class="comment-head">
{{--*/ $UName= App\Helper\Helper::channel_by_id($comment->user_id,$comment->user_type) /*--}}
										<!-- Uploader -->
										<h6 class="comment-name by-author"><a href="{{ App\Helper\Helper::get_user_profile_link($comment->user_id,$comment->user_type,$UName) }}" target="_blank">{{ App\Helper\Helper::channel_by_id($comment->user_id,$comment->user_type) }}</a><span class="cm_date">{{ App\Helper\Helper::change_date_format($comment->created_at) }}</span></h6>

										<!-- Comment Date -->
										
									</div>

									

									<select class="not_rated" data-rate="{{$comment->rating}}" id="comment_id_{{ $comment->id }}" style="display: none;">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
									
									<script type="text/javascript">
										// set rating for each star
										setRating($('#comment_id_{{ $comment->id  }}'),'{{$comment->rating}}');
									</script>	

									<div class="comment-content">
										{!!
											nl2br( App\Helper\Helper::emoticons(App\Helper\Helper::active_links($comment->comment, $image->ID))) 
											!!}
									</div>


									
									</div>
								</div>
							</li>




							@endforeach

		    


							<div >
							</div>
							
							
							@else
							<div class="no_data_msg no_data_msg_div">
								<p><span><i class="fa fa-warning"></i></span>{{ Lang::get('core.no_comment') }}</p>
							</div>
							@endif

							<div style="display:none;">
								{!! str_replace('/?', '?', $comments->appends(Input::except('page'))->render()) !!}
							</div>

						</ul>
					</div>
				</div>
			</div>		

<!-- 
		<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk">
		<div class="cm_textarea">

				<form action="#" method="POST" id="SubmitComment">
				<meta name="csrf-token" content="C28fS8fXlBubfhBoXTNDMvZtOypUXvL1UsjCQal9">
				<div class="text_area_video">
					<textarea name="comment" rows="5" placeholder="Enter Your Comments" id="commentsBox"></textarea>
				</div>
				<div class="text-center">
					<button id="postComment" data-video="57797EA90C" type="submit"><span>Post Comment</span></button>
				</div>
				</form>

				</div>
			</div>	 -->


		</div>
	</div>


	<!-- Recommended Videos,  Similar Videos -->
	<div class="col-md-4 col-sm-12 col-xs-12 video_upload_right">
	<div class="col-md-12 col-sm-12 col-xs-12 upload_comment_blk video_upload_desc_blk tags_blk1">
	<!-- Video Description -->
	<div class="video_description">
	<div class="video_title">
		<h2>Tags</h2>
	</div>
	<div>
	
	<ul class="tags">
		<!-- Image Tags -->
		@if($image->Tags)
		{{ App\Helper\Helper::photo_tags($image->ID) }}
		@else
		No Tags in this Image
		@endif
	</ul>
	</div>
	</div>
	</div>

	<!-- Album Slider Block -->
	
	<div class="photo_album_blk col-md-12 col-xs-12 col-xs-12">
		<div class="video_title">
	<h2>Albums</h2>
	</div>
		<div class="album_sider_div">
			<div class="slider_blk owl-carousel">
				@foreach($albums as $iKey => $album)
				 {{--*/ $Ext = App\Helper\Helper::getLastedExt($album->album_id) /*--}} 

				<div class="item" data-foto="{{ App\Helper\Helper::getLastedPhoto($album->album_id).'_medium.'.$Ext }}" data-id="{{ $album->title }}">
					<!-- <img src="{{ url('uploads/model_gallery/'.App\Helper\Helper::getLastedPhoto($album->album_id).'_medium.'.$Ext) }}" alt="{{ $album->title }}"> -->
					@if(empty(App\Helper\Helper::getLastedPhoto($album->album_id)))
					<img src="{!! asset('/magelur/images/Coming-Soon.png') !!}" alt="{{ $album->title }}" class="gallery_images1">
					@else
					<img src="{!! \SiteHelpers::user_albums(App\Helper\Helper::getLastedPhoto($album->album_id),'_medium',$Ext) !!}" alt="{{ $album->title }}" class="">
					@endif
				</div>
				@endforeach
			</div>
		</div>
	</div>
	

	<div class="related_videos">
		<!-- Related Album Image -->
	<div class="video_title">
	<h2>Related Images</h2>
	</div>
	@if(!empty($images))	
		@foreach($images as $iKey => $img)
		<div class="r_video_box">
			<div class="r_video_bg">

				<img src="{!! \SiteHelpers::user_albums($img->ID,'_mediumX',$img->Ext) !!}" alt="{{ $img->Title }}" alt="">
				<!-- <span>{{App\Helper\Helper::description_date($img->Created)}}</span> -->
			</div>
			<div class="r_video_info">
			{{--*/ $ID =urlencode( base64_encode($img->ID));/*--}}

				<a href="{{ url('/photo/'.$ID) }}" class="r_title">{{ $img->Title }}</a>
			{{--*/ $UName= App\Helper\Helper::channel_by_id($img->OwnerId,$img->user_type) /*--}}

				<a href="{{ App\Helper\Helper::get_user_profile_link($img->OwnerId,$img->user_type,$UName) }}" class="r_channel">{{ App\Helper\Helper::channel_by_id($img->OwnerId,$img->user_type) }}</a>

				<a class="r_views"><i class="fa fa-eye"></i> {{ App\Helper\Helper::count_photo_views($img->ID) }} views</a>
			</div>
		</div>
		@endforeach
		@else
		<div class="empty-related-img">
		   <i class="fa fa-warning"></i> No More Related Images Found...
		</div>
		@endif
	</div>


	</div>

</div>
</div>


<style>
.page-header {
    display: none;
}
.map {

    display: none;

}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script> 
      var owl = $('.slider_blk');
      owl.owlCarousel({
      	margin: 0,
      	loop: false,
      	nav: true,
      	dots:true,
      	autoplay: true,
      	navigation : true,
      	navText: ["<p><i class='fa fa-angle-left'></p>", "<p><i class='fa fa-angle-right'></p>"],
      	autoplayTimeout:1000, 
      	autoplayHoverPause:true,
      	autoplaySpeed:1000,
      	responsive: { 
      		0: {
      			items: 1
      		},
      		480: {
      			items: 1
      		},       
      		1200: {
      			items: 1
      		}
      	}
      });

</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <script type="text/javascript">
   function addToDelete() {

   	swal({
   		title: "Are you sure?",
   		text: "Once deleted, you will not be able to recover this imaginary file!",
   		icon: "warning",
   		buttons: true,
   		dangerMode: true,
   	})
   	.then(function(willDelete) {
   		if (willDelete) {
   			$(location).attr('href', "{{ url('photo/photodelete/'.$image->ID.'/' )}}");
   		} else {
   			swal("Your imaginary file is safe!");
   		}
   	}); 	
   }
   if('{{$favourite}}' == ''){
      $('.favourites a').removeClass('favouriteActive');
      $('.favourites a').attr('title','Add to favourites');
   }else{
      $('.favourites a').addClass('favouriteActive');
      $('.favourites a').attr('title','Remove from favourites');
   }
   $('#postComment').click(function(e){
   	 location.reload();
   });
   function addToFavourite() {

      var favourite_id = '{{$image->ID}}';
      var favourite_type = 'image';
      
      $.ajax({
        url: '{{url("addToFavorites")}}',
        type: 'post',
        data: {favourite_id:favourite_id,favourite_type:favourite_type},
        dataType: 'json',
        success: function(data){
          if (data.status == 'added') {
              swal({
                title: "",
                text: "Photo added to your Favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').addClass('favouriteActive');
              $('.favourites a').attr('title','Remove from favourites');
          }else if(data.status == 'removed'){
              swal({
                title: "",
                text: "Photo removed from your favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').removeClass('favouriteActive');
              $('.favourites a').attr('title','Add to favourites');
          }else{
            swal({
              title: "",
              text: "Some error occured. Please try again after some time.",
              icon: "danger",
              dangerMode: true
            });
          }
        }
          
      });
    }
    var minLength = 3;
var maxLength =500	;

    $("#commentsBox").on("keypress", function(){
    var value = $(this).val();
    // alert(value.length);
 if (value.length == maxLength){
 	// toastr.warning("Comments should only 500 Characters");

        swal("Comments should only 500 Characters");
    }
   
	});
    </script>