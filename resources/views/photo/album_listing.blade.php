
<link href="{{ asset('magelur/css/uploader.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('magelur/js/uploader.js')}}"></script>
<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     
     <div class="banner_title wow bounce">
            <h1>{{ ucfirst($user_type) }} - Gallery</h1>
            <div class="two_laid"><a href="{{asset('/')}}">HOME</a><p>GALLERY</p></div>
          </div>

     <!-- <div class="banner_title wow bounce">
            <h1>{{ ucfirst($user_type) }} - Gallery</h1>
              <ul class="link_list">
                <li class="active"><a href="{{asset('/')}}">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">GALLERY</a></li>
              </ul>
          </div> -->

 <!--     <div class="banner_title wow bounce">
      <h1> {{ ucfirst($user_type) }} - Gallery</h1>
     </div> -->
    </div>
   </div>
  </section>
  <!-- show upload only for model & agent -->
<div class="container-fluid magelur_header">
@if(count($albums)>0)
<!-- <section>
    <div id="upload-demo-i-cover">

    </div>
    <div class="banner_title wow bounce">
      <h1>Model - Gallery</h1>      
     </div>
</section> -->
</div>
<div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">
<div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">
   @if($user_type== \Session::get('user_type'))

<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
    <a href="{{url('photo/upload')}}"><span class="span_upload_photo"><i class="fa fa-plus-circle"></i></span></a>
    <h1 class="span_heading">Add Your Album Here</h1>
  </div>

</div>
  @endif
@foreach($albums as $album)

<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
    <!-- <img src="{{ url('uploads/model_gallery/'.App\Helper\Helper::getLastPhoto($album->album_id).'_medium.jpg') }}" alt="{{ $album->title }}" class="gallery_images1"> -->
   {{--*/ $Ext=App\Helper\Helper::getLastedExt($album->album_id) /*--}} 
    @if(empty(App\Helper\Helper::getLastedPhoto($album->album_id)))
    <img src="{!! asset('/magelur/images/Coming-Soon.png') !!}" alt="{{ $album->title }}" class="gallery_images1">
  @else
    <img src="{!! \SiteHelpers::user_albums(App\Helper\Helper::getLastedPhoto($album->album_id),'_medium',$Ext) !!}" alt="{{ $album->title }}" class="gallery_images1">
   @endif
    <div class="img_hover_blk">
     <div class="dit_vertcial">
      <div class="hvr_icon_div">
       <a class="fancybox" rel="gallery1" href="{{ url('uploads/model_gallery/'.App\Helper\Helper::getLastedPhoto($album->album_id).'_medium'.$Ext) }}" title=""> 
        <span class="circle_white"><img src="{{ asset('images/expanded_img.png')}}"></span>
       </a>
        <a href="{{ url('photo/album/'.$album->uri) }}"><span class="circle_white"><img src="{{ asset('images/crop_img.png')}}"></span></a>
       </div>
      </div>
    </div>
  </div>
</div>
@endforeach

</div>
</div>
</div>

@else
<div class="magelur_gallery wow bounce col-md-12 col-sm-12 col-xs-12">
<div class="empty_vertical">
  <div class="empty_msg_details clearfix">
  <div class="empty_msg_blk clearfix">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>No More Images!</h1>
        <h2>
          You Have Not Uploaded Any Images Under Your Gallery.Click on the above image upload button to add images on gallery
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="{{asset('images/file_image1.png')}}" alt="file_image" />
        </div>
      </div>
    </div>
    <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
      </div>
      <div class="msg_next_page">

        <a href="{{ url('upload') }}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
      </div>
    </div>
  </div>
</div>
</div>
</div>

@endif


<style type="text/css">
  .map{display: none;}
</style>

<script type="text/javascript">
$(document).ready(function(){

    $('.preview_imges_scroll').slimScroll({
        height          : '310px',
        width           : '100%',
        allowPageScroll :  true
        
    });     

    $(document).on('click','input[name="album_type"]',function(){
      // get the choosed value
      var curVal = $(this).val();

      if(curVal == 'new'){
        // show album create form
        $('div.new-albums-form').show();
        // hide album pick list
        $('div.old-albums-form').hide();
      }else{
        // allow user to pick the album list
        $('div.old-albums-form').show();
        // hide album create form
        $('div.new-albums-form').hide();

      }
    })

    $(".fancybox").fancybox({});

});

function addAlbum(){
  var title    = $('input[name="album_name"]').val();
  var desc     = $('textarea[name="album_desc"]').val();
  $.post('{!! url("add/album")!!}',{title:title,desc:desc},function(data){

  });
}

</script>
<style type="text/css">
.footer_absolute .magelur_about.faq_sec .magelur_header {
    padding-bottom: 0px;
    height: auto;
    width: 100%;
    display: inline-block;
}.footer_absolute div.magelur_about.faq_sec{height: auto;}
</style>