@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">
    <!-- Page header -->
   
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
<div class="sbox-title"> 
		
		<h1>Mail Template</h1>
		

	</div>
	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
			<a href="{{ URL::to('mailtemplate/update') }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
	   		<!-- <a href="{{ URL::to('subscriber/update') }}" class="tips btn btn-sm btn-white"  title="Create">
			<i class="fa  fa-plus "></i>&nbsp;Create</a> -->
			@endif  
			@if($access['is_remove'] ==1)
			
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-sm btn-default" title="Delete">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 		
			
			
		</div>
	   		

	
	
	 {!! Form::open(array('url'=>'mailtemplate/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
<tr id="orderDetails">
<td class="number"></td>
					<td></td>		
						
        		<td id="title" class="fieldsearch"> <div class="select-style">{!! Form::text('title',"{$title}",array('class'=>'form-control', 'placeholder'=>'Subject', )) !!} </div></td>
				<td id="titles" class="fieldsearch"> <div class="select-style">{!! Form::text('titles',"{$titles}",array('class'=>'form-control', 'placeholder'=>'Message', )) !!} </div></td>
	        	<td id="startdate" class="fieldsearch"> <div class="select-style">{!! Form::text('startdate',"{$startdate}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Startdate', )) !!} </div></td>

					<td id="status" class="fieldsearch"> 
        			<div class="select-style">
        				<select name='status' class = 'select2 '>
        					<option value="">Select option</option>
        					<option value="1" {{ $status == '1' ? 'selected' : ''}}>Active</option>
        					<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>

        				</select>

        			</div>
        		</td>
					
<td class="icons">
<input type="hidden"  value="Search">
<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
<a href="{{ url('mailtemplate') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
</td>
</tr>       
         @if(count($model_filter)>0)

				             {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
		
            @foreach ($model_filter as $row)
                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->id }}" />  </td>									
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['field']=='status')
					 	@if( $row->status == 1)
					 	<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
					 	@else
					 	<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
					 	@endif	
					 	@else
						{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}		
			 			@endif
					 </td>
					 @endif					 
				 @endforeach
				 				 {{--*/ $i++ /*--}}

				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('mailtemplate/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('mailtemplate/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
                  @else  
		Data not available !
		@endif 
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
			@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
@endif
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("mailtemplate/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
$("#startdate").change(function() {
   
	$(".datetimepicker").hide();
});
$('.pagination_blk').hide();

</script>		
@stop