@extends('layouts.app')

@section('content')

<!--  -->
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

  <div class="page-content row">

 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> Mail Template </h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'mailtemplate/save/'.$row['id'], 'class'=>'form-horizontal mailtemplate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<!-- <fieldset><legend> {{ Lang::get('core.'.$pageTitle) }} </legend> -->
								
							<div class="form-group  " >
							<!-- <label for="Id" class=" control-label col-md-4 text-left">{{Lang::get('core.')}} Id </label> -->
							<div class="col-md-6">
							{!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Subject" class=" control-label col-md-4 text-left">Subject  <span class="asterix"> * </span> </label>
							<div class="col-md-6">
							{!! Form::text('subject', $row['subject'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'' )) !!} 
							<div class="text-danger"><p class="errors"></p></div>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Message" class=" control-label col-md-4 text-left">Message  <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::textarea('message', $row['message'],array('class'=>'mceEditor form-control', 'style'=>'width:100%;', 'placeholder'=>'',   )) !!} 
							
							<div class="text-danger"><p class="error"></p></div>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							 					
												
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Status  </label>
							<div class="col-md-6">
							{!! Form::checkbox('status',1, $row['status'],array('class'=>'form-control', 'placeholder'=>'', )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<!-- <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					 --><button type="submit" name="" class="btn btn-primary btn-sm temp" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					 <button type="button" onclick="location.href='{{ URL::to('mailtemplate?return='.$return) }}' " class="btn btn-warning btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>

					
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		$(function(){
			tinymce.init({	
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				 plugins : "openmanager",
				 file_browser_callback: "openmanager",
				 open_manager_upload_path: '../../../../../../../../uploads/images/',
				 onchange_callback : "getcontent",
				 setup : function (theEditor) {
				 	theEditor.onKeyPress.add(
				 		function (theEditor) {
				 			$('.error').html('');
				 		}
				 		);
				 },
			 });	
		}); 
	});
	$('.temp').click(function(){
var values = $('input[name=subject]').val();
$('input[name=subject]').keyup(function (e){
$('.errors').html('');
})


		if(values =='')
		{
		$('.errors').html("Subject field required");
		}
		else{
		$('.errors').html('');
		}
		var value = $.trim(tinyMCE.get('message').getContent());
		
		if(value==''){
			$('.error').html("Message field required");
			return false;
		}else{
			$('.error').html('');
			return true;
		}

	});
	
	</script>		 
@stop