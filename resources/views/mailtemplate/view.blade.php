
@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left">
	   		<a href="{{ url('mailtemplate?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('mailtemplate/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i>
	   		</a> mailtemplate
	   		@endif 					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('mailtemplate/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('mailtemplate/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
						
				<tr>
				<td width='30%' class='label-view text-right'>Subject</td>
				<td>{{ $row->subject }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Message</td>
				<td>{{ strip_tags($row->message) }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created_At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Updated_At</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
<td>
					@if( $row->status == 1)
					<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

					@else
					<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
					@endif
				</td>
				</tr>
				
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop