@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url('event/fields') }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Event Field Management</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
		 {!! Form::open(array('url'=>'event/fields/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset>
				{!! Form::hidden('id', $row['id']) !!}					
						
				<div class="form-group" >
				<label for="Fields" class="control-label col-md-4 text-left"> Fields <span class="asterix"> * </span></label>
				<div class="col-md-7">
				{!! Form::select('field_categories',array('' => 'Select Category') +\SiteHelpers::getEventFieldCategories('EventFields'),$row['field_categories'],array('class' => 'form-control','id' => 'country','required' => 'required')) !!}
		 
				</div> 
				</div>

				<div class="form-group  " >
				<label for="Values" class=" control-label col-md-4 text-left"> Values <span class="asterix"> * </span></label>
				<div class="col-md-7">
				<input  type='text' name='field_values' id='field_values' value='{{$row["field_values"]}}' 
				class='form-control ' required/>
				</div> 
				<div class="col-md-1">
				</div>
				</div>				
									
				<div class="form-group  " >
				<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
				<div class="col-md-7">
				<label class='radio radio-inline'>
				<input type='radio' name='status' value ='active' required @if($row['status'] == 'active') checked="checked" @endif > Active </label> 	
				<label class='radio radio-inline'>
				<input type='radio' name='status' value ='inactive' required @if($row['status'] == 'inactive') checked="checked" @endif > Inactive </label>
				</div> 
				<div class="col-md-1">
				</div>
				</div> 					
									  
				</fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
			  <div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
				<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
				<button type="submit" name="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
				<button type="button" onclick="location.href='{{ URL::to('event/fields?return='.$return) }}' " class="btn btn-warning btn-sm ">{{ Lang::get('core.sb_cancel') }} </button>
				</div>	  
		
			  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("categories/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop