@extends('layouts.app')

@section('content')
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Event Field Management</h1>
		

	</div>


	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >
			
	   		<a href="{{ URL::to('event/fields/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			

		</div>

{!! Form::open(array('url'=>'/event/fields/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}	
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
					<th> No </th> 
					<th> <input type="checkbox" class="checkall" /></th> 
					<th> Fields </th>
					<th> Values </th>
					<th> Status </th>
					<th> Action </th>   
					
			  </tr>
        </thead>

        <tbody>    

          <tr id="orderDetails">
		<td class="number"></td>
		<td></td>
		<td id="field_categories" class="fieldsearch"> 
			<div class="select-style"  style="width:80px">
				{!! Form::text('field_categories',"{$field_categories}",array('class'=>'form-control', 'placeholder'=>'Fields', )) !!} </div></td>
			<td id="field_values" class="fieldsearch"> 
			<div class="select-style"  style="width:80px">
				{!! Form::text('field_values',"{$field_values}",array('class'=>'form-control', 'placeholder'=>'Values', )) !!} </div></td>
		<td id="status" class="fieldsearch"> 
		<div class="select-style">
		<select name='status' class = 'select2 '>
		<option value="">Select option</option>
		<option value="active" {{ $status == 'active' ? 'selected' : ''}}>Active</option>
		<option value="inactive" {{ $status == 'inactive' ? 'selected' : ''}}>Inactive</option>
		</select>
		</div>
		</td>
		<td class="icons">
		    <input type="hidden"  value="Search">
		   <!--  <input type="hidden"  name="Tags" id="Alltags" value=""> -->

		    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
		    <a href="{{ url('event/fields') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
		</td>
		</tr>   						
           {{--*/ $i = ($results->currentPage() - 1) * $results->perPage() + 1;/*--}}
				@foreach($results as $result)
				<tr>
				    <td> {{ $i }} </td> 
				    <td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $result->id }}" />  </td>	 
				    <td> 
				    	{{$result->field_categories}}
				    </td>  
				    <td> 
				    	{{$result->field_values}}
				    </td> 
				    <td> 
				    	@if( $result->status == 'active')
						<span class="label label-success" >Active</span>
						@else
						<span class="label label-danger" >Inactive</span>
						@endif
				    </td>  
				     <td>
					 	
						
						<a  href="{{ URL::to('event/fields/update/'.$result->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						
												
					
				</td>
				     
				    {{--*/ $i++ /*--}}
				</tr>
				@endforeach                            

        </tbody>
      
    </table>
   
	<input type="hidden" name="md" value="" />
	@if(count($results) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	{!! Form::close() !!}
	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions form_footer">
	 
	   {!! Form::open(array('url'=>'event/fields','method'=>'GET')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="Sort" class="select-alt">
		  <option value=""> Sort </option>	    
		  <option value="field_categories" @if(isset($pager['sort']) && $pager['sort']=='field_categories') selected='selected' @endif>Fields</option>
		  <option value="field_values" @if(isset($pager['sort']) && $pager['sort']=='field_values') selected='selected' @endif>Values</option>
		  <option value="status" @if(isset($pager['sort']) && $pager['sort']=='status') selected='selected' @endif>Status</option>
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $results->total() }}</b>
		</p>		
	   </div>
		<div class="col-sm-4">	
		<div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  
 			<div class="pagination_s"> 
	  			{!! str_replace('/?', '?', $results->appends(Input::except('page'))->render()) !!}
	  		</div>
	  </div>
	  </div>
	</div>	
	</div>	
</div>	
	</div>	  
</div>	
	
@stop