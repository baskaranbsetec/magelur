@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
		<h1> @if($user_type=='agent') Agent Transactions @elseif($user_type=='model') Model Transactions @endif</h1>
	<div class="sbox-tools" >
			<!-- @if($access['is_excel'] ==1)
			<a href="{{ URL::to('transactions/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i></a>
			@endif

			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('module/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif  -->
		</div>
		

		
	</div>

	<div class="sbox-content"> 	

	<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<!-- <a href="{{ URL::to('transactions/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i>Create</a> -->
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
		<!-- 	<a href="{{ URL::to('transactions/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>	 -->			


		</div>
	

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'transactions/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				<!-- <th ><span>{{ Lang::get('core.btn_action') }}</span></th> -->
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
							@if($t['label']=='Id')
								<th><span>Transaction ID</span></th>
							@elseif($t['label']=='User Id')
								<th><span>Username</span></th>	
							@elseif($t['label']=='Membership Id')
								<th><span>Membership</span></th>
							@elseif($t['label']=='User Type')
								<th><span>Plan Duration</span></th>	
							@elseif($t['label']=='Order Details')
								

							@elseif($t['field']=='updated_at')
							@elseif($t['field']=='status')
							@elseif($t['field']=='payment_method')
							@elseif($t['field']=='purchase_type')
							@elseif($t['field']=='purchase_period')
							@elseif($t['field']=='webhook_check_status')
								

							@else
								<th><span>{{ $t['label'] }}</span></th>		
							@endif
								
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
				
			  </tr>
        </thead>

        <tbody> 
        <tr id="orderDetails">
<td class="number"></td>
<td ></td>
<td ></td>
<td id="username" class="fieldsearch"> <div class="select-style">
	@if($user_type=='agent')
	{!! Form::select('username',array('' => 'Select username') +\SiteHelpers::getTableValues('agents'),"{$username}",array('class' => 'select2','id' => 'username')) !!}
	@elseif($user_type=='model')
	{!! Form::select('username',array('' => 'Select username') +\SiteHelpers::getTableValues('models'),"{$username}",array('class' => 'select2','id' => 'username')) !!}
	@endif
	<!-- {!! Form::text('username',"",array('class'=>'form-control', 'placeholder'=>'User Name', )) !!} </div></td> -->
<td id="period" class="fieldsearch"> <div class="select-style">
	<select name='period' class = 'select2 '>
		<option value="">Select Period</option>
		<option value="month" {{ $period == 'month' ? 'selected' : ''}}>Monthly</option>
		<option value="year" {{ $period == 'year' ? 'selected' : ''}}>Yearly</option>
	</select>
	<!-- {!! Form::text('user_type',"",array('class'=>'form-control', 'placeholder'=>'User Type', )) !!} </div> -->
</td>
<td id="membership" class="fieldsearch"> <div class="select-style">
	@if($user_type=='agent')
	{!! Form::select('membership',array('' => 'Select membership') +\SiteHelpers::getMemPlanName('agent'),"{$membership}",array('class' => 'select2','id' => 'membership')) !!}

		@elseif($user_type=='model')
		{!! Form::select('membership',array('' => 'Select membership') +\SiteHelpers::getMemPlanName('model'),"{$membership}",array('class' => 'select2','id' => 'membership')) !!}

@endif

	<!-- {!! Form::text('membership',"",array('class'=>'form-control', 'placeholder'=>'Membership', )) !!}  -->
</div></td>
<td id="amount" class="fieldsearch"> <div class="select-style">{!! Form::text('amount',"{$amount}",array('class'=>'form-control', 'placeholder'=>'Amount', )) !!} </div></td>
<td id="created_At" class="fieldsearch"> <div class="select-style">{!! Form::text('created_At',"{$created_At}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'created Date', )) !!} </div></td>

<td id="status" class="fieldsearch"> 
	<div class="select-style">
	<select name='status' class = 'select2 '>
		<option value="">Select Status</option>
		<option value="cancelled">Completed</option>

	</select>

	</div>
</td>
<td class="icons">
<input type="hidden"  value="Search">
<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
@if($user_type=='agent')
<a href="{{ url('transactions/agent') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  

@else
<a href="{{ url('transactions/model') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
@endif
</td>
</tr>      
 						
              {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<input type="hidden" class="user_type" name="user_type" value="{{ $user_type }}" /> 
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('transactions/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('transactions/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->
 {{--*/ $i++ /*--}}

				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
					 		
							@if($field['field']=='user_id')
								<th><span>@if($row->status == 'deleted')
									User not found
									@else
									{!! SiteHelpers::getUsernamebyId($row->user_id,$row->user_type) !!}
									@endif
								</span></th>	
							@elseif($field['field']=='membership_id')
							{{--*/ $id=$row->membership_id; /*--}}
								<th><span>{!! SiteHelpers::getplanbyId($id) !!}</span></th>	
								@elseif($field['field']=='user_type')
							@if($row->purchase_period == 'month')
							{{--*/ $period='Monthly'; /*--}}
							@else
							{{--*/ $period='Yearly'; /*--}}

							@endif
								<th><span>{!! $period!!}</span></th>	
							@elseif($field['field']=='order_details')
							@elseif($field['field']=='updated_at')
							@elseif($field['field']=='status')
							@elseif($field['field']=='payment_method')
							@elseif($field['field']=='purchase_type')
							@elseif($field['field']=='purchase_period')
							@elseif($field['field']=='webhook_check_status')

							@else
								<td>					 
								 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
								 </td>		
							@endif


						 
						@endif	
					 @endif					 
				 @endforeach	

				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('transactions/show/'.$row->id.'?user_type='.$user_type.'&return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
					
				</td>	 
                </tr>
				
            @endforeach
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	@if(count($model_filter) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	{!! Form::close() !!}
	<!-- start footer -->
<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   <!-- {!! Form::open(array('url'=>$pageModule.'/filter/')) !!} -->

	   <form method="GET" action="{{ url('/transactions/'.$user_type) }}">
      
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		 
<!-- <input type='hidden' name='lll' value="model">
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select> -->
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	
		  <option value="id" <?php if(isset($_GET['sort']) && $_GET['sort']=='id') echo "selected" ?>>Transaction Id</option>
		  <option value="username"  <?php if(isset($_GET['sort']) && $_GET['sort']=='username') echo "selected" ?>>Username</option>
		  <option value="plan_name"  <?php if(isset($_GET['sort']) && $_GET['sort']=='plan_name') echo "selected" ?>>Membership</option> 
			  <option value="purchase_period"  <?php if(isset($_GET['sort']) && $_GET['sort']=='purchase_period') echo "selected" ?>>Plan Duration</option>
			  <option value="amount"  <?php if(isset($_GET['sort']) && $_GET['sort']=='amount') echo "selected" ?>>Amount</option>
			  <option value="created_at"  <?php if(isset($_GET['sort']) && $_GET['sort']=='created_at') echo "selected" ?>>Created At</option>
			  <option value="transaction_status"  <?php if(isset($_GET['sort']) && $_GET['sort']=='transaction_status') echo "selected" ?>>Transaction Status</option>
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm sdd"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	 </form>
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">

		Total : <b>{{ $model_filter->total() }}</b>
		</p>		
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
@endif
	  </div>
	</div>	
	<!-- end footer -->
	</div>
</div>	
	</div>	  
</div>	

<script type="text/javascript">
$( document ).ready(function() {
    
$("#created_At").change(function() {
   
	$(".datetimepicker").hide();
});

});
</script>

@stop