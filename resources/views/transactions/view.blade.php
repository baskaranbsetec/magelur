@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('transactions/'.$user_type.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('transactions/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
			<span class="titlu">@if($row->status == 'deleted')
									User not found
									@else
									{!! SiteHelpers::getUsernamebyId($row->user_id,$row->user_type) !!}'s Transaction Details
									@endif</span>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('transactions/show/'.$prevnext['prev'].'?user_type='.$user_type.'&return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('transactions/show/'.$prevnext['next'].'?user_type='.$user_type.'&return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
						<tr>
							<td width='30%' class='label-view text-right'>Id</td>
							<td>{{ $row->id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Name</td>
							<td>@if($row->status == 'deleted')
									User not found
									@else
									{!! SiteHelpers::getUsernamebyId($row->user_id,$row->user_type) !!}
									@endif
						    </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Plan Duration</td>
							<td>{{ $row->purchase_period}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Membership</td>
							<td>{!! SiteHelpers::getplanbyId($row->membership_id) !!} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Amount</td>
							<td>{{ $row->amount}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created At</td>
							<td>{{ $row->created_at}} </td>
							
						</tr>
					
					
						<tr>
							<td width='30%' class='label-view text-right'>Order Details</td>
							<td style="word-wrap:break-word;word-break:break-all;"><?php
					$json = json_decode($row->order_details);
					$json_string = json_encode($json, JSON_PRETTY_PRINT);
					echo "<pre>";echo $json_string;
					?></td>
				</tr>
							
						</tr>
					
						
					
						<tr>
							<td width='30%' class='label-view text-right'>Transaction Status</td>
							<td>{{ $row->transaction_status}} </td>
							
						</tr>
					
						
					
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop