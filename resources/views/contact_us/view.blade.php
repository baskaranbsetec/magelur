@extends('layouts.app')

@section('content')
<div class="page-content row">
 	<div class="page-content-wrapper m-t">   
		<div class="sbox ">
			<div class="sbox-title">
				<div class="sbox-tools pull-left" >
				<a href="{{ url('contactus-list') }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			   		Contact Us View
				</div>	

				<div class="sbox-tools " >
				</div>
			</div>
			<div class="sbox-content" > 	

				<table class="table table-striped table-bordered" >
					<tbody>	
						<tr>
							<td width='30%' class='label-view text-right'>User name</td>
							<td>@if($contactus->name) {{ $contactus->name}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>email</td>
							<td>@if($contactus->email) {{ $contactus->email}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>@if($contactus->user_type) {{ $contactus->user_type}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>Message</td>
							<td>@if($contactus->message) {{ $contactus->message}} @else Not Specified @endif</td>
						</tr>
						<tr>
							<td width='30%' class='label-view text-right'>City</td>
							<td>@if($contactus->city) {{ $contactus->city}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>Country</td>
							<td>@if($contactus->country)  {!! \SiteHelpers::getCountry($contactus->country) !!} @else Not Specified @endif</td>
						</tr>
					</tbody>	
				</table>   
			</div>
		</div>	
	</div>
</div>
	  
@stop