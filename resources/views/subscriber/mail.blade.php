@extends('layouts.app')

@section('content')
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

  <div class="page-content row">
    <!-- Page header -->
   
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> 
		
		<h1>@if(!empty($email)>0) Send to selected Mails  @else Send Bulk Mail @endif</h1>
		

	</div>
	<div class="sbox-content"> 	
	
		 {!! Form::open(array('url'=>'subscriber/sendmail','class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset></fieldset>		
							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
							<div class="col-md-6">
							@if(!empty($email)>0)
								{!! Form::text('tomail',$email,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'readonly'=>'true'   )) !!} 
							
							@else
								{!! Form::text('tomail','',array('class'=>'form-control', 'placeholder'=>'To: All Emails', 'readonly'=>'true'   )) !!} 
							
							@endif
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 
							
							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Template <span class="asterix"> </span></label>
							<div class="col-md-6">
							<select id="temp_select" class="form-control">
							<option data-sub="" data-message="">--Select Template--</option>
							@foreach($template as $templates)
							    <option data-id="{{$templates->id}}" data-sub="{{$templates->subject}}" data-message="{{$templates->message}}">{{$templates->subject}}</option>
							    @endforeach
							   </select>
							</div>
							</div>

							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Subject<span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('subject','',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true','id'=>'sub'  )) !!} 
							<div class="text-danger"><p class="errors"></p></div>

							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 
							<!-- <div class="form-group">
							<label for="Email" class=" control-label col-md-4 text-left">  <span class="asterix">  </span></label>
							<div class="col-md-6">
							{!! Lang::get('core.mail_hint') !!} : {email}
							</div>
							</div> -->
							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Message <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::textarea('message', '' ,array('class'=>'mceEditor form-control', 'style'=>'width:100%;','placeholder'=>'','id'=>'msg','required')) !!}
 <div class="go-btn btn btn-xs btn-info remove" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></div>  [Clear Content]
							<div class="text-danger"><p class="error"></p></div>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 
							
							</fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<!-- <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button> -->
					<button type="submit" name="" class="btn btn-primary btn-sm sendmail" ><i class="fa  fa-save "></i> Send</button>
					<button type="button" onclick="location.href='{{ URL::to('subscriber?return='.$return) }}' " class="btn btn-warning btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 

		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>	
  <script type="text/javascript">
	$(document).ready(function() { 
		 	
		$(function(){
			tinymce.init({	
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				 plugins : "openmanager",
				 file_browser_callback: "openmanager",
				 open_manager_upload_path: '../../../../../../../../uploads/images/',
				 onchange_callback : "getcontent",
				  setup : function (theEditor) {
				 	theEditor.onKeyPress.add(
				 		function (theEditor) {
				 			$('.error').html('');
				 		}
				 		);
				 },
			 });	
		});
	});
	$('.sendmail').click(function(){
		$('input[name=subject]').keyup(function (e){
			$('.errors').html('');
		})
		var values = $('input[name=subject]').val();
		if(values=='')
		{
		$('.errors').html("Subject field required");
		}
		else{
		$('.errors').html('');
		}
		var value = $.trim(tinyMCE.get('msg').getContent());
		if(value == ''){
			$('.error').html("Message field required");
			return false;
		}else{
			$('.error').html('');
			return true;
		}
	});


	$('#temp_select').change(function(){
		var id = $(this).find(':selected').attr('data-id');

		var sub = $(this).find(':selected').attr('data-sub');
		var msg = $(this).find(':selected').attr('data-message');
		$('#sub').val(sub);
		tinymce.editors[0].execCommand('mceInsertContent', false, msg);

		// tinymce.activeEditor.execCommand('mceInsertContent', false, msg);
	});
	$('.remove').click(function(){
tinyMCE.activeEditor.setContent('');
});

	</script>		 
@stop