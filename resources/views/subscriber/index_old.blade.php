@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ Lang::get('core.'.$pageTitle) }} <small>{{ Lang::get('core.'.$pageNote) }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> {!! Lang::get('core.home') !!} </a></li>
        <li class="active">{{ Lang::get('core.'.$pageTitle) }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div>
	</div>
	<div class="sbox-content"> 	
	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('subscriber/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			
			<a href="{{ URL::to('subscriber/mail') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_sendmail') }}">
			<i class="fa fa-envelope"></i>&nbsp;{{ Lang::get('core.btn_sendmail') }} </a>
			
			<a href="javascript:void(0)" class="tips btn btn-sm btn-white selectedmail" title="{{ Lang::get('core.btn_sendselectedmail') }}" >
			<i class="fa fa-envelope"></i>&nbsp;{{ Lang::get('core.btn_sendselectedmail') }} </a>

			<a href="{{ URL::to('cron/mail_cron.php') }}" target="_blank" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.cron') }}">
			{{ Lang::get('core.cron') }} </a>

		</div> 		

	
	
	 {!! Form::open(array('url'=>'subscriber/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {!! Lang::get('core.No') !!} </th>
				<th> </th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /></td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>		
						@if($t['field'] == 'status')
							<select name="status">
								<option value="">{!! Lang::get('core.please_select') !!}</option>
								<option value="1">{!! Lang::get('core.Enable') !!}</option>
								<option value="0">{!! Lang::get('core.disable') !!}</option>
							</select>
						@else
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
						@endif				
						
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> {!! Lang::get('core.GO') !!} </button></td>
			 </tr>	        
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50">
					<input type="hidden" class="email_check" name="email_check[]" value="{{ $row->email }}" />
					<input type="checkbox" class="ids" name="id[]" value="{{ $row->id }}" />  </td>									
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$field['field'],$field['attribute']['image']['path']) !!}
						@else	
							{{--*/ $conn = (isset($field['conn']) ? $field['conn'] : array() ) /*--}}
							@if($field['field']=='status')
								@if($row->$field['field']==1)
								{!! SiteHelpers::gridDisplay(Lang::get('core.Enable'),$field['field'],$conn) !!}
								@else
								{!! SiteHelpers::gridDisplay(Lang::get('core.disable'),$field['field'],$conn) !!}	
								@endif
							@else
								{!! SiteHelpers::gridDisplay($row->$field['field'],$field['field'],$conn) !!}	
							@endif
							
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('subscriber/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('subscriber/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>  </table>
      <ul class="parsley-error-list">
     @if(isset($no_record)) 
     <li>   {{$no_record}} </li>
        @endif 
        </ul>
  
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("subscriber/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	

$('.selectedmail').on('click',function(e){
	var b = new Array();
        $.each($("input[name='id[]']:checked"), function () {          
            
        	//b.push($(this).val())
            b.push($(this).val());
        });
        if (b.length > 0) {
        	window.location.href="subscriber/selected?id="+b+"";
        }
        else{
        	alert("{!! Lang::get('core.subscribe_msg') !!}")
        }
       	
        // $.ajax({
        //     type: "POST", 
        //     url: "{{ URL::to('subscriber/mail') }}", 
        //     data: "id=" + b, 
        //     success: function (a) {
        //         window.location.href="{{URL::to('subscriber/selected?id="+b+"')}}"
        //     }
        // });

});

</script>		
@stop
