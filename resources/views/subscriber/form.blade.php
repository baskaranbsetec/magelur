

@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Subscriber</h1> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
@if($row['id'])
		{{---*/ $pageurl = 'subscriber/save/'.$row['id'].'?return='.$return; /*---}}
	@else
		{{---*/ $pageurl = 'subscriber/save/' /*---}}
	@endif	
		 {!! Form::open(array('url'=>$pageurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ' ,'id'=>'subscriber')) !!}
<div class="col-md-12">
<!-- 			<fieldset><legend> {!! Lang::get('core.subscriber') !!} </legend>
 -->								
							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('email', $row['email'],array('class'=>'form-control', 'placeholder'=>'')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
							<div class="col-md-6">
							@if($row['status']=='1')
							<input type="checkbox" name='status' checked /> 
							@else
							<input type="checkbox" name='status' id='status'  /> 
							 <div id="errorToShow"></div>
							@endif

							 
							</div> 
							<div class="col-md-2">
														</div>
							</div>




							</fieldset>
		</div>

		
		
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('subscriber?return='.$return) }}' " class="btn btn-warning btn-sm ">  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 

<script type="text/javascript">
// $('#subscriber').on('click',function(){
// 	if($("#status").prop('checked') == true){

//           $('#errorToShow').hide()
//          }
//          else{
//           $('#errorToShow').show()

//          }
 
//       }); 

    $("#subscriber").validate({
      ignore: "not:hidden",
      rules: {
        email:{
          required:true,
          email:true
        },
        //   status:{
        //   required:true,
        // },
       
        

      },
       
     
   
    
   
      messages: {
        
      },
      // errorPlacement: function(error, element) {
      //   if (element.attr("name") == "status") {
      //       error.appendTo("#errorToShow");
      //   }
       
      //    else {
      //       error.insertAfter(element);
      //   }
      //   },
      
        
        
      submitHandler:function(form){
        

        form.submit();
      }
    }); 

</script>		 
@stop