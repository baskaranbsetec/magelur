

	<div class="magelur_about">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner">
					<header>
				
					</header>

					<div class="banner_title wow bounce">
						<h1>ABOUT US</h1>
						 <div class="two_laid"><a href="{{asset('home')}}">Home</a><p>ABOUT US</p></div>
							<!-- <ul class="link_list">
								<li class="active"><a href="{!! url('/home') !!}">HOME</a></li>
								<li>//</li>
								<li><a href="javascript:void(0)">ABOUT</a></li>
							</ul> -->
					</div>
				</div>
			</div>
		</section>
		<!-- Header End -->


		<section class="magelur_about_section">
			<div class="container">
				<div class="col-md-10 col-md-offset-2 col-sm-12">
					<div class="about_contant_section">
					<div class="about_title wow SlideInDown">
						<h1>Who<br> We Are</h1>
						<h3>THOUSANDS OF CASTINGS BY APPROVED PROFESSIONALS</h3>
					</div>

					<div class="about_details wow SlideInLeft">
						<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>

						<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>

						<p>Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, pendisse in orci enim.</p>
					</div>
					</div>
				</div>
			</div>
		</section>


		<!--  -->
		<section class="magelur_about_slider">
			<div class="container">
				<div class="col-md-6">
					<div class="choose_block wow SlideInLeft">
						<img src="images/choose.jpg" class="img-responsive">
						<div class="choose_details">
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>

							<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue.</p>
						</div>
					</div>
				</div>

				<div class="col-md-6 choose_slider_contant">
					<div class="wow pulse"><h1>Why choose<br> jmodels?</h1></div>

					<div class="owl-carousel owl-theme about_slide wow SlideInDown">
					    <div class="item">
					    	<img src="images/about_slider_1.jpg">
					    	<a href="javascript:void(0)"><p>Commercial Modelling</p></a>
					    </div>
					    <div class="item">
					    	<img src="images/about_slider_2.jpg">
					    	<a href="javascript:void(0)"><p>Catwalk modelling</p></a>
					    </div>
					    <div class="item">
					    	<img src="images/about_slider_3.jpg">
					    	<a href="javascript:void(0)"><p>Fitness Modelling</p></a>
					    </div>
					     <div class="item">
					    	<img src="images/about_slider_2.jpg">
					    	<a href="javascript:void(0)"><p>Catwalk modelling</p></a>
					    </div>
					     <div class="item">
					    	<img src="images/about_slider_3.jpg">
					    	<a href=""><p>Fitness Modelling</p></a>
					    </div>
					</div>

				<script>
					$('.about_slide').owlCarousel({
					    loop:true,
					    autoplay:true,
					    responsive:true,
					    margin:10,
					    nav:true,
					    dots:false,
					    transitionStyle : "goDown",
					    navText: [ '<img src="images/gold-prev-arrow.png">', '<img src="images/gold-next-arrow.png"><span class="slider_nav">NEXT</span>' ],
					    responsive:{
					        0:{
					            items:1
					        },
					        600:{
					            items:3
					        },
					        800:{
					            items:3
					        },
					        1000:{
					            items:3
					        }
					    }
					})
				</script>
			</div>
		</section>
		<!--  -->

		<!--  -->
		<section class="our_photo">
			<div class="container">
				<div class="col-md-12 col-sm-12">
					<div class="photo_title wow pulse">
						<h1>Our Best<br> Photographer</h1>
					</div>

					<div class="col-md-4 col-sm-12">
						<div class="our_photo_section">
						<div class="best_photo">
							<img src="images/photographer-1.jpg" class="img-responsive">
							<div class="best_photo_hover">
								<h3><a href="javascript:void(0)">Antonio Hurrison</a></h3>
							</div>
						</div>
						<div class="our_photo_content  wow SlideInLeft">
							<h1><a href="javascript:void(0)">Antonio Hurrison</a></h1>
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi </p>
							<ul class="our_social">
								<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-12">
						<div class="our_photo_section">
						<div class="best_photo">
							<img src="images/photographer-2.jpg" class="img-responsive">
							<div class="best_photo_hover">
								<h3><a href="javascript:void(0)">Antonio Hurrison</a></h3>
							</div>
						</div>
						<div class="our_photo_content  wow SlideInDown">
							<h1><a href="javascript:void(0)">Antonio Hurrison</a></h1>
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi </p>
							<ul class="our_social">
								<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-12">
						<div class="our_photo_section">
						<div class="best_photo">
							<img src="images/photographer-3.jpg" class="img-responsive">
							<div class="best_photo_hover">
								<h3><a href="javascript:void(0)">Antonio Hurrison</a></h3>
							</div>
						</div>
						<div class="our_photo_content  wow SlideInDown">
							<h1><a href="javascript:void(0)">Antonio Hurrison</a></h1>
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi </p>
							<ul class="our_social">
								<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--  -->

		<!--  -->
		<section class="service_work">
			<div class="container">

				<div class="col-md-2">
					<div class="test_title">
						<h1>What we do</h1>
					</div>
				</div>
				<div class="col-md-10">
					<div class="round-image">
						<!-- <img src="images/round.png"> -->
					</div>


					<ul id="what_vertical">
  						<li data-thumb="images/lap.png">
    					    	<div class="slider_content">
					    	<h3>Photography</h3>
					    	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>
					    	</div>
  						</li>
  						
  						<li data-thumb="images/camera.png">
    					    	<div class="slider_content">
					    	<h3>Photography</h3>
					    	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>
					    	</div>
					  	</li>
					  	
					  	<li data-thumb="images/bullhorn.png">
    					    <div class="slider_content">
					    	<h3>Photography</h3>
					    	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. </p>
					    	</div>
  						</li>
					</ul>
				</div>
			</div>
		</section>
		<!--  -->


		<!--  -->
		<section class="testimonial">
			<div class="container">
				<div class="test_title wow SlideInLeft">
					<p>WHAT OUR CLIENT SAY?</p>
					<h1>Testimonials</h1>
				</div>

					<div class="col-md-12">
						 <div class="test wow SlideInDown">
						  <ul id="vertical">
						  @foreach($testimonials as $testimonial)
     <li><p>{{$testimonial->description}} </p>
     <label>{{$testimonial->title}}</label></li>

						 <!--   <li><p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
						   <label>M Adnan <br/> J-Ltd</label></li>

						   <li><p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
						   <label>M Adnan <br/> J-Ltd</label></li> -->
						   @endforeach
						  </ul> 
						</div>
					</div>
		</section>
		<!--  -->


		<!--  -->
		
		<!--  -->

	</div>

		<!--  -->
	
		<!--  -->



