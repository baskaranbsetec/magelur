<div class="section_one">
 <div class="left_half">
  <img src="{{ asset('images/home_banner_left.jpg') }}">
 </div>
  <div class="container">
   <div class="cnt_txt wow Bounce">
    <h1>The Perfect <span>Resources for Models</span></h1>
   </div>
  </div>
</div>

<div class="section_two">
 <div class="full_view">
  <div class="container">
   <div class="historyer wow SlideInLeft">
    <h1>History <span>Since 2012</span></h1>
    <h3>Thousands of castings by approved <br/> professionals</h3>
    <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
    <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
    <p>Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, pendisse in orci enim.</p>
    <a href="http://demo.bsetec.com/demo/magelur/public/aboutus"><button><span>Continue</span></button></a>
   </div>
  </div>
 </div>
</div>

<div class="section_three">
 <div class="discover wow SlideInUp">
  <h3>Discover models or <br/> be discovered</h3>
 </div>
 <div class="disc_slides">
  <div class="owl-carousel homepage_owl_slider" id="">
    @foreach($models as $model)
    <div class="item"><img src="{{ asset('uploads/model_profile/'.$model->profile_photo) }}" class="home_model_image">
     <div class="colorbox">
      <div class="inner_shades">
        <h4>Real Life & People</h4>
        <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
        <p>Professional Model</p>
        <p><a href="#">Book model</a></p>
      </div>  
     </div>
    </div>
    @endforeach
</div>
 <div class="customNavigation">
          <a class="btn btn-primary prev"></a>
          <a class="btn btn-primary next"></a>
        </div>

 </div>
</div>

<div class="section_four">
 <div class="marlex clearfix">
   <div class="bc_mdl wow SlideInLeft">
    <h1>Become a Model</h1>
    <p>{{ strip_tags(CNF_APPDESC) }} </p>
    <h3>submit <br/> Application</h3>
    <a href="{{url('/model/create')}}"><button><span>Apply Now</span></button></a>
   </div>
   <div class="top_mdl">
    <img src="{{ asset('images/girl.jpg') }}">
    <div class="topmods"> <div class="stender wow SlideInUp"><h4>Top <br/> influencer models</h4></div></div>
   </div>
  </div>
</div>

<div class="section_five home_testimonial">
 <div class="container">
  <div class="clinets_says wow SlideInDown">
    <p>What our client say?</p>
    <h1>Testimonials</h1>
  </div>
  <div class="test">
  <ul id="vertical">
    @foreach($testimonials as $testimonial)
     <li><p>{!! ($testimonial->description) !!} </p>
     <label>{{$testimonial->title}}</label></li>
 <!--   <li><p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
   <label>M Adnan <br/> J-Ltd</label></li>
   <li><p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
   <label>M Adnan <br/> J-Ltd</label></li> -->
    @endforeach
  </ul> 
  </div>
 </div>
 <a class="testi-btn" href="#"><button><span>Continue</span></button></a>
</div>

<div class="section_six">
 <div class="marlex clearfix">
   <div class="bc_mdl wow SlideInLeft">
    <h1>News & Press</h1>
    <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
    <p>a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
    <a href="http://demo.bsetec.com/demo/magelur/public/blogs/list"><button><span>Continue</span></button></a>
    <!-- <button><span>Continue</span></button> -->
   </div>
   <div class="top_ladines">
    <!-- <img src="images/lady.jpg"> -->
    <img  src="{{ asset('images/lady.jpg') }}"  />
   </div>
  </div>
</div>

<div class="section_seven home_instagram_galley">
 <h1>Model Galleries</h1>
 @foreach($images as $image)
 {{--*/ $ID =urlencode( base64_encode($image->ID));/*--}}
 <ul class="instagramer">
   @if($image->ID % 2 == 0)
  <li class="wow SlideInLeft">
   <img  src="{{ asset('uploads/model_gallery/'.$image->ID.'.'.$image->Ext) }}"  />
  <div class="followme"><div class="submit_follow"><div class="main_follow">
    @if(SiteHelpers::checkLogged())
    <a href="{{ asset('photo/'.$ID) }}"><span></span> <p>Follow me</p></a>
    @else
    <a href="#"  class='follow_me'><span></span> <p>Follow me</p></a>
    @endif
  </div></div></div>
</li>
@else
<li class="wow SlideInRight">
  <img  src="{{ asset('uploads/model_gallery/'.$image->ID.'.'.$image->Ext) }}"  />
  <div class="followme"><div class="submit_follow"><div class="main_follow">
    @if(SiteHelpers::checkLogged())
    <a href="{{ asset('photo/'.$ID) }}"><span></span> <p>Follow me</p></a>
    @else
    <a href="#" class='follow_me'><span></span> <p>Follow me</p></a>
    @endif
  </div></div></div>
</li>
@endif
  <!-- <li class="wow SlideInLeft">
   <img  src="{{ asset('images/insta_one.jpg') }}"  />
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img  src="{{ asset('images/insta_two.jpg') }}"  />
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_three.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/insta_four.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/insta_five.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li> -->
 </ul>
  @endforeach

</div>
<script type="text/javascript">
 $(function(){
$('.event_map').hide();
});
$(document).ready(function(){
   var owl = $('.homepage_owl_slider').owlCarousel({


    margin:15,
    nav:false,
    autoplay:true,
    responsive:true,
    loop:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        768:{
            items:3
        },
        1000:{
            items:3
        }
    }
})

   $(".customNavigation .btn.btn-primary.next").click(function(){
      owl.trigger('next.owl.carousel');
      //$('#owl-properties .owl-next')[0].click();
    })
    $(".customNavigation .btn.btn-primary.prev").click(function(){
      owl.trigger('prev.owl.carousel');
      //$('#owl-properties .owl-prev')[0].click();
    })

    
   });
</script>
<script>
$('.follow_me').click(function(){
    toastr.info('Login to follow')
  });
    </script>