

<div class="ourmodel_banner advertisement">
  <div class="ourmod_cnt">
    <h1>tell a friend</h1>
    <div class="two_laid"><a href="index.html">Home</a><p>tell a friend</p></div>
  </div>
</div>

<div class="tellfriend">
  <h1>Tell a Friend</h1>
   <div class="tell_fd"><img src="images/tell_friend.png"></div>
  <div class="maingert"><p>Follow the 3 simple steps and get money</p>
  <div class="butsload clearfix"><button data-toggle="modal" data-target="#myModal"><span>Invite a Friend</span></button></div>
  </div>
</div> 

<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
 -->
<!-- Modal -->
<div id="myModal" class="modal invitefrnd_popup invite_popup_middle fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">

      <div class="invite_leftside">

      <h4 class="modal-title">Invite Friends</h4>

{!! Form::open(array('url'=>'invite/friend', 'class'=>'form-horizontal sendMessage invite_form','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

<div id='content-area'></div>
        

    <div class=" " >
		<label for="Your Name" class=" control-label col-md-12 text-left">{{ Lang::get('core.your_name') }}</label>
		<div class="col-md-12 nopadding_div">
    @if(\Session::get('model_id'))
		   {!! Form::text('name', \Session::get('model_name'),array('id'=>"name",'class'=>"form-control",'required')) !!}
    @elseif(\Session::get('agent_id'))
      {!! Form::text('name', \Session::get('agent_name'),array('id'=>"name",'class'=>"form-control",'required')) !!}
    @else
    {!! Form::text('name', '',array('id'=>"name",'class'=>"form-control",'required')) !!}
    @endif    
		</div> 
		</div>
		 <div class="" >
		<label for="Your Email" class=" control-label col-md-12 text-left">{{ Lang::get('core.your_email') }}  </label>
		<div class="col-md-12 nopadding_div">
    @if(\Session::get('model_id'))
       {!! Form::text('email', \Session::get('model_email'),array('id'=>"email",'class'=>"form-control",'required')) !!}
    @elseif(\Session::get('agent_id'))
      {!! Form::text('email', \Session::get('agent_email'),array('id'=>"email",'class'=>"form-control",'required')) !!}
    @else
    {!! Form::text('email', '',array('id'=>"email",'class'=>"form-control",'required')) !!}
    @endif  
		                                	 		
		</div> 
		</div>
		 <div class="" >
		<label for="Friend Email" class=" control-label col-md-12 text-left">{{ Lang::get('core.friend_email') }}  </label>
		<div class="col-md-12 nopadding_div">
		{!! Form::text('friend_email', '',array('id'=>"friend_email",'class'=>"form-control",'required')) !!}                                	 		
		</div> 
		</div>
     <div class="" >
    <label for="Description" class=" control-label col-md-12 text-left">{{ Lang::get('core.description') }} </label>
    <div class="col-md-12 nopadding_div">
    {!! Form::textarea('description', 'Here, you will find all the perfect faces for your next big production. Join our network and meet all the exceptional talent models you need right at your finger tip.',array('id'=>"description",'class'=>"form-control",'required','rows'=>"5")) !!}                                     
    </div> 
    </div>
    <div class="modal-footer">
      <button type="submit"  data-type="inboxx" class="btn-sm sendbtn " >{{ Lang::get('core.sb_send') }} </button>
    </div>
{!! Form::close() !!}
</div>
<div class="invite_rightside">
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
</div>

      </div>
      
    </div>

  </div>
</div>
<script type="text/javascript">
$(".invite_rightside").on("click", function(e) {
    e.preventDefault();

$('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
});
 
$(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       
       email:{
          required:true,
          email:true,
           
        },friend_email:{
          required:true,
          email:true,
           
        },
        name:{
          required:true,
           
        },
        description:{
          required:true,
           
        },
         
      },
      messages: {
       name:{
          required:"<i class='fa fa-warning error_icon'></i>{{ Lang::get('core.req_name') }}",
        },
        description:{
          required:"<i class='fa fa-warning error_icon'></i>{{ Lang::get('core.req_description') }}",
        },
        email:{
          required:"<i class='fa fa-warning error_icon'></i>{{ Lang::get('core.req_email') }}",
          email:"{{ Lang::get('core.invalid_email') }}",
        },
        friend_email:{
          required:"<i class='fa fa-warning error_icon'></i>{{ Lang::get('core.fill_friend_email') }} ",
          email:"{{ Lang::get('core.invalid_email') }}",
        },
        
       
      },
      
        submitHandler: function(form) {
          
          
          $.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),

          success: function(data) {

            if(data=='false'){
            }
            else{
              // alert(data);
               // $("#content-area").html('Incorrect Email ID / Password');
            $('#content-area').prepend('<a href="{{ url("login?tellfriend=") }}'+data+'" style="color:#bd8414">Your Invitation Link to login {{ url("login?tellfriend=") }}'+data+'</a>');

            toastr.success("Mail sent Successfully");
            // form.submit();

            }

          },
          });
        
        },
        });

</script>



