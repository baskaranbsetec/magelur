
<div class="ourmodel_banner advertisement">
  <div class="ourmod_cnt">
    <h1>feedbacks / suggestions</h1>
    <div class="two_laid"><a href="{{asset('home')}}">Home</a> <p> feedbacks / suggestions</p></div>
  </div>
</div>

<div class="adevrtise_with">
 <div class="container">
  <div class="feedback_suggestions">
    <div class="feedback_form">
      <h1>feedbacks/suggestions</h1>
      <p class="feedback-para">"we are here to serve you and we are looking for ways to serve you even better. Your feedback is very important to us and we want to hear from you"</p>
      <div id='content-area'></div>
          {!! Form::open(array('url'=>'feedback-form', 'class'=>'form-signup sendFeed','method'=>'post','parsley-validate'=>'','novalidate'=>'','id'=>'login')) !!}

      <div class="form-group form-group_full">
        <label>Your Name</label>
        <input type="text" name="name" placeholder="Enter your Name">
      </div>
      <div class="form-group form-group_full">
        <label>Email Address</label>
        @if(\Session::get('model_id'))
        {!! Form::text('email', \Session::get('model_email'),array('id'=>"email",'class'=>"form-control",'required')) !!}
        @elseif(\Session::get('agent_id'))
        {!! Form::text('email', \Session::get('agent_email'),array('id'=>"email",'class'=>"form-control",'required')) !!}
        @else
        {!! Form::text('email', '',array('id'=>"email",'class'=>"form-control",'placeholder'=>"Enter your email address here",'required')) !!}
        @endif  
      </div>
      <div class="form-group form-group_full">
        <label>Your Suggestions</label>
        <textarea name='suggestion' placeholder="Write your Feedback and Suggestions"></textarea>
      </div>
      <div class="form-group form-group_full">
        <label>Want to rate with us for customer services?</label>
        <div class="post-action-feed">
          <!-- Rating -->
          <select name='feed_rating' class='feed_rating' id='feed_rating'  data-id='feed_rating'>
            <option value="1" >1</option>
            <option value="2" >2</option>
            <option value="3" >3</option>
            <option value="4" >4</option>
            <option value="5" >5</option>
          </select>
          <input type='hidden' name='star_rate' value='1'>
          <script type="text/javascript">
          $('#feed_rating ').barrating({
            theme: 'fontawesome-stars',
            initialRating: '0',
            onSelect: function(value, text, event) {
              $('.br-widget a').addClass('br-selected')
              var rate = $( "#feed_rating option:selected" ).val();
              // alert(rate);
              $('input[name="star_rate"]').val(rate);
            }
          });
          </script>

        </div>
       <!--  <ul class="starer">
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
        </ul>   -->
      </div>
      <div class="butsload clearfix"><button class='FeedbacK' ><span>Send Feedback</span></button></div>
      {!! Form::close() !!} 

    </div>  
   </div> 
  </div>  
</div>  
<script type="text/javascript">
$(".sendFeed").validate({
     ignore: "not:hidden",
      rules: {
     
       name:{
          required:true,
         

        },
        email:{
          required:true,
          email:true,
           
        },
        suggestion:{
          required:true,
           
        },
        feed_rating:{
          required:true,
           
        },
         
      },
      messages: {
       name:{
          required:"<i class='fa fa-warning error_icon'></i>Please fill your name",
        },
        feed_rating:{
          required:"<i class='fa fa-warning error_icon'></i>Please rate us",
        },
        suggestion:{
          required:"<i class='fa fa-warning error_icon'></i>Please provide your suggestion",
        },
        email:{
          required:"<i class='fa fa-warning error_icon'></i>Please fill your email",
          email:"<i class='fa fa-warning error_icon'></i>Your Email is invalid",
        },
        
      },
      
        submitHandler: function(form) {

          $.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),
          beforeSend: function() {    
            $(".FeedbacK").attr("disabled", true);

          },
          success: function(data) {
            // reset form values

            $('input[name="name"]').val('');
            $('input[name="email"]').val('');
            $('textarea[name="suggestion"]').val('');
            $('input[name="star_rate"]').val('');
            $("div.br-widget a").removeClass("br-selected");
            $("div.br-current-rating").text("");
            toastr.success("Feedback sent Successfully");

          },
          });
        // form.submit();
        },
        });

</script>



