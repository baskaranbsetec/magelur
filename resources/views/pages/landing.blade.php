<div class="magelur_landing_pages">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_landing_banner">
				<div class="homepage-hero-module">
				    <div class="video-container">
				        <div class="filter"></div>

				        <div id="either-gif-or-video">
				            <video class="embed-responsive-item fillWidth" poster="images/landing_page.jpg" controls="false" autoplay loop autobuffer muted playsinline>
				            	<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" >
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" >
   									
				            </video>
				        </div>
				    </div>
				</div>

					

					<div class="banner_title wow bounce">
						<!-- <h1>ABOUT US</h1>
							<ul class="link_list">
								<li class="active"><a href="">HOME</a></li>
								<li>//</li>
								<li><a href="">ABOUT</a></li>
							</ul> -->
					</div>
				</div>
			</div>
		</section>
		<!-- Header End -->


		<section class="model_agent">
			<div class="container">
				<div class="row">
					<div class="model_agent_container" style="">
					<div class="col-md-6 col-sm-12">
				
				<div class="owl-carousel owl-theme model">
					<div class="item">
						<div class="landing_contant">
							<img src="images/landing1.jpg" class="img-responsive">
							<div class="landing_model_titles">
								<h1>{{ Lang::get('core.am_model') }}</h1>
								<p>- {{ Lang::get('core.looking_for_job') }} -</p>
							</div>
								<div class="landing_arrow">
									<a href="{{ url($modelurl) }}" class="next" ><i class="fa fa-angle-right fa-2x"></i></a>
									
								</div>
						</div>
					</div>

					<div class="item">
						<div class="landing_contant">
							<img src="images/landing1.jpg" class="img-responsive">
							<div class="landing_model_titles">
								<h1>{{ Lang::get('core.am_model') }}</h1>
								<p>- {{ Lang::get('core.looking_for_job') }} -</p>
							</div>
								<div class="landing_arrow">
									<a href="{{ url($modelurl) }}" class="next" ><i class="fa fa-angle-right fa-2x"></i></a>
								</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-sm-12">
				<div class="owl-carousel owl-theme agent" >
					<div class="item">
						<div class="landing_contant">
							<img src="images/landing2.jpg" class="img-responsive">
							<div class="landing_agent_titles">
								<h1>{{ Lang::get('core.am_agent') }}</h1>
								<p>- {{ Lang::get('core.looking_for_job') }} -</p>
							</div>
								<div class="landing_arrow">
									<a href="{{ url($agenturl) }}" class="next" ><i class="fa fa-angle-right fa-2x"></i></a>
								</div>
						</div>
					</div>

					<div class="item">
						<div class="landing_contant">
							<img src="images/landing2.jpg" class="img-responsive">
							<div class="landing_agent_titles">
								<h1>{{ Lang::get('core.am_agent') }}</h1>
								<p>- {{ Lang::get('core.looking_for_job') }} -</p>
							</div>
								<div class="landing_arrow">
									<a href="{{ url($agenturl) }}" class="next" ><i class="fa fa-angle-right fa-2x"></i></a>
								</div>
						</div>
					</div>

				</div>
					</div>
				</div>
			</div>
		</section>



		<section class="landing_section_three">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="display_content">
							<h1>{{ Lang::get('core.talent_models_network') }}</h1>

							<div class="display_details">
							<p>{{ Lang::get('core.are_you_a_model') }}</p>

							<p>{{ Lang::get('core.are_you_an_agent') }}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<section class="talent_section">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="landing_model">
							<h3><span class="box"></span>{{ Lang::get('core.model_network') }}</h3>
							<p>{{ Lang::get('core.model_network_desc') }}</p>
						</div>
					</div>

					<div class="col-md-4 col-sm-12">
						<div class="landing_model">
							<h3><span class="box"></span>{{ Lang::get('core.talent_models') }}</h3>
							<p>{{ Lang::get('core.talent_models_desc') }}</p>
						</div>
					</div>

					<div class="col-md-4 col-sm-12">
						<div class="landing_model">
							<h3><span class="box"></span>{{ Lang::get('core.model_agency') }}</h3>
							<p>{{ Lang::get('core.model_agency_desc') }}</p>
						</div>
					</div>
				</div>
			</div>
		</section>


</div>

<script>
	/** Document Ready Functions **/
/********************************************************************/

$( document ).ready(function() {

    // Resive video
    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');
        
    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});
</script>

<script>
	 var video;
  var canvas;

  function startPlayback()
  {
    if (!video) { 
      video = document.createElement('video');
      video.src = 'http://www.coffeecreamthemes.com/themes/modellic/wordpress/white/wp-content/uploads/sites/4/2016/06/model.mp4';
      video.loop = true;
      video.addEventListener('playing', paintVideo);
    }
    video.play();
  }

  function paintVideo()
  {
    if (!canvas) {
      canvas = document.createElement('canvas');
      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;
      document.body.appendChild(canvas);
    }
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    if (!video.paused)
      requestAnimationFrame(paintVideo);
  }
	</script>

	<script>
		$('.owl-carousel').owlCarousel({
		    loop:false,
		    margin:10,
		    dots:false,
		    autoplay:false,
		    touchDrag  : false,
            mouseDrag  : false,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:false
		        },
		        600:{
		            items:1,
		            nav:false
		        },
		        1000:{
		            items:1,
		            nav:false,
		            loop:false
		        }
		    }
		})
	</script>

	<script>
	 $(document).ready(function(){
    $(".model .next").click(function(){
      $('.model .owl-next')[0].click();
    })
    $(".model .prev").click(function(){
      $('.model .owl-prev')[0].click();
    })

        $(".agent .next").click(function(){
      $('.agent .owl-next')[0].click();
    })
    $(".agent .prev").click(function(){
      $('.agent .owl-prev')[0].click();
    })
	});
	</script>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
    document.getElementById("main").style.marginLeft= "";
}
</script>