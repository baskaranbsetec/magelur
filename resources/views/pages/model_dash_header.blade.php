<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/tagsly.js')}}"></script>
<link href="{{ asset('magelur/css/tagsly.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js')}}"></script>
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">

<link href="{{ asset('frontend') }}/default/css/croppie.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/croppie.js"></script>
<style type="text/css">
button:disabled,
button[disabled]{
 cursor: no-drop;
}
</style>
@foreach($models as $model)
	<div class="magelur_about model_abouts magelur_profile_blk">
		<section>
			<div class="container-fluid magelur_header">
				<div id="upload-demo-i-cover">
					<img src="{{ asset('/uploads/model_profile/cover/'.$model->profile_cover) }}">
				</div>
				<div class="editbtn_div_cover" data-toggle="modal" data-target="#myCover" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
			</div>
		</section>

<div class="sm_bars">
	<div class="user_img">
		<a href="javascript:void(0)">
			<div id="upload-demo-i">
			<img src="{{ asset('/uploads/model_profile/small/'.$model->profile_small) }}" id='prevavatar'>
	       </div>	<!-- <img src="{{ asset('/images/barbie.jpg') }}"> -->
		<div class="editbtn_div" data-toggle="modal" data-target="#myAvatar" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
	</a>
	</div>	
 <div class="yell_bar clearfix">

  <div class="barbs_text">
  	<h1>{{$model->username}} | <span>Modelling, Photo Shoot,,Video Production, Websites  Online</span></h1>
  </div>
  <div class="socail_torend">
   <ul class="icones">

        <li class="fbook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ url('myprofile/'.$model->id)}}&t={{ url('myprofile/'.$model->id)}}"
            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
            target="_blank" title="Share on Facebook">
          </a></li>
        <li class="gpluser">
        <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://plus.google.com/share?url={{ url('myprofile/'.$model->id)}}">
        </a>
        <li>
        <li class="twittera"><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/share?url={{ url('myprofile/'.$model->id)}}"></a></li>
   </ul>
  </div> 	
 </div>	

	<div class="after_login_menu clearfix">
		<ul class="loged_in">
			<li class="{{ Request::path() == 'model-dashboard/'.$model->id ? 'active' : '' }}"><a href="{{ URL::to('model-dashboard/'.$model->id)}}">Dashboard</a></li>
			<li class="{{ Request::path() == 'model-profile/'.$model->id ? 'active' : '' }}"><a href="{{ URL::to('model-profile/'.$model->id)}}">Profile</a></li>
			<li class="{{ Request::path() == 'message' ? 'active' : '' }}"><a href="{{ URL::to('message')}}">Message</a></li>
			<li class="{{ Request::path() == 'model-photos' ? 'active' : '' }}"><a href="javascript:void(0)">Photos</a></li>
			<li class="{{ Request::path() == 'model_video/'.$model->id ? 'active' : '' }}"><a href="{{ URL::to('model/video/'.$model->id)}}">Video</a></li>
			<li class="{{ Request::path() == 'model-network' ? 'active' : '' }}"><a href="javascript:void(0)">Network</a></li>
			<li class="{{ Request::path() == 'model-membership/'.$model->id ? 'active' : '' }}"><a href="{{ URL::to('model-membership/'.$model->id)}}">membership</a></li>
		</ul>
		<div class="logout"><a href="{{ URL::to('user/logout')}}">Logout</a></div>	
	</div>
	
</div>	
		<!-- Header End -->
 

</div>

@endforeach
@include('profile_cover_popup')
</body>
</html>     

<script type="text/javascript">
$('body').addClass('dash_nav');

</script>