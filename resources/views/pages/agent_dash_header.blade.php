

<link href="{{ asset('frontend') }}/default/css/croppie.css" rel="stylesheet">
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/croppie.js"></script>
<script src="{{ asset('magelur/js/tagsly.js')}}"></script>
<link href="{{ asset('magelur/css/tagsly.css')}}" rel="stylesheet" type="text/css">
@foreach($agents as $agent)
	<div class="magelur_about model_abouts">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class=" ">
				<div id="upload-demo-i-cover">
                    <img src="{{ asset('/uploads/agency_profile/cover/'.$agent->profile_cover) }}">
                </div>
				</div>
				<div class="editbtn_div_cover" data-toggle="modal" data-target="#myCover" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
			</div>
		</section>

<div class="sm_bars">
    <div class="user_img">
        <a href="javascript:void(0)">
            <div id="upload-demo-i">
            <img src="{{ asset('/uploads/agency_profile/'.$agent->profile_photo) }}" id='prevavatar'>
           </div>   <!-- <img src="{{ asset('/images/barbie.jpg') }}"> -->
        <div class="editbtn_div" data-toggle="modal" data-target="#myAvatar" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>  
    </a>
    </div>  
		
 <div class="yell_bar clearfix">
  <div class="barbs_text">
  	<h1>{{ $agent->agency_name }} | <span> {{ \SiteHelpers::getName('country',$agent->country) }} </span></h1>
  </div>
  <div class="socail_torend">
   <ul class="icones">
        <li class="fbook"><a href=""></a></li>
        <li class="gpluser"><a href=""></a></li>
        <li class="twittera"><a href=""></a></li>
        <li class="instagra"><a href=""></a></li>
   </ul>
  </div> 	
 </div>	

	<div class="after_login_menu clearfix">
		<ul class="loged_in">
			<li class="{{ Request::path() == 'agent-dashboard/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-dashboard/'.$agent->id)}}">Dashboard</a></li>
			<li class="{{ Request::path() == 'agent-profile-edit/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-profile-edit/'.$agent->id)}}">Profile</a></li>
			<li class="{{ Request::path() == 'message' ? 'active' : '' }}"><a href="{{ URL::to('message')}}">Message</a></li>
			<li class="{{ Request::path() == 'events/update/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('my/events')}}">Event</a></li>
			<li class="{{ Request::path() == 'agent-photos' ? 'active' : '' }}"><a href="javascript:void(0)">Photos</a></li>
			<li class="{{ Request::path() == 'agent_video/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent/video/'.$agent->id)}}">Video</a></li>
			<li class="{{ Request::path() == 'agent-network' ? 'active' : '' }}"><a href="javascript:void(0)">Network</a></li>
			<li class="{{ Request::path() == 'agent-membership/'.$agent->id ? 'active' : '' }}"><a href="{{ URL::to('agent-membership/'.$agent->id)}}">membership</a></li>
		</ul>
		<div class="logout"><a href="{{ URL::to('user/logout')}}">Logout</a></div>	
	</div>
	
</div>	
    	<!-- Header End -->
 </div>


@endforeach
@include('profile_cover_popup')

<script type="text/javascript">
$('body').addClass('dash_nav');

</script>
