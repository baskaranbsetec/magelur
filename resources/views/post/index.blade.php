<?php
use App\Library\Markdown;
?>

<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>Blog</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<div class="blog_post_page">
<div class="container m-t">	

	<div class="row" style="padding-top:25px;">
		<div class="col-md-9 m-t left_abt clearfix blog_left_side">
		<div class="posts_blog">

					@if(Session::has('messagetext'))	  
						   {!! Session::get('messagetext') !!}
					@endif	
			@foreach ($rowData as $row)


			<div class="posts">
				<div class="heading_date">
					<h1> {{ date(" j M. " , strtotime($row->created)) }} </h1> 
      			</div>
      			<div class="blog_right_side">
				<div class="title">
					<a href="{{ url('post/view/'.$row->pageID.'/'.$row->alias)}}" ><h4> {{ $row->title }} </h4></a>
					<h5>Category: 
					<?php 
				foreach(explode(',',$row->labels) as $val)
				{
					echo '<span href="'.url('post/label/'.trim($val)).'"> '.trim($val).' </span> ';
				}
				?> |  Author: JakubSpitzer</h5>
				</div>
				<div class="info" style="display:none;">
					<i class="fa fa-user"></i>  <span> {{ ucwords($row->username) }}  </span>   
					<i class="fa fa-calendar"></i>  <span> {{ date("M j, Y " , strtotime($row->created)) }} </span> 
					<i class="fa fa-comment-o "></i>   <span> <a href="{{ url('post/view/'.$row->pageID.'/'.$row->alias)}}#comments" > {{ $row->comments }}  comment(s) </a> </span> 
				</div>
				<div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
    				<a href="#">   
      				<img src="images/blog_img1.png" class="img-responsive" />
    				</a>
      			</div>
				<div class="note">
				<p>
				<?php

				$content = explode('<hr>', $row->note);
				if(count($content>=1))
				{
					echo PostHelpers::formatContent($content[0]);
				} else {
					//echo PostHelpers::formatContent($row->note);
				}
				?>
				</p>

					 
				</div>
				<div class="blog_button_blk">
      				<div class="blog_button_div1">         
						<button>Newyork</button>
      				</div>
      			<div class="blog_button_div2">
      			<div class="blog_twitter">
      			<div class="blog_twitter_left">
      			<img src="images/blog_fb_img.png">Like</div>
      			<div class="blog_twitter_right">1</div>
      			</div>
      			<div class="blog_twitter blog_twitter_blk">
      			<div class="blog_twitter_left">
      			<img src="images/blog_twitter_img.png">Tweet</div>
      			<div class="blog_twitter_right">1</div>
      			</div>
      			</div>
      			<div class="blog_button_div3">
      			<div class="view_all_btn">
     			 <a href="{{ url('post/view/'.$row->pageID.'/'.$row->alias)}}""><button>Read article...</button></a>
      			</div>
      			</div>
      			</div>

				<div class="labels" style="display:none"><br />
				<b>Articles In :  </b><br />
				<?php 
				foreach(explode(',',$row->labels) as $val)
				{
					echo '<a href="'.url('post/label/'.trim($val)).'" class="btn btn-xs btn-default"> '.trim($val).' </a> ';
				}
				?>
				</div>	

				<!-- <a href="{{ url('post/view/'.$row->pageID.'/'.$row->alias)}}" class="btn btn-default btn-sm pull-right">Read More <i class="fa fa-arrow-right"></i>  </a>  -->				
			 <hr />	
			 </div>
			 </div> 
			

			
            @endforeach
             </div>

            <div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  
 <div class="pagination_content"> 
 <h1>page 1 of  3
</h1>
 <ul class="pagination"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="#">2</a></li> <li><a href="#" rel="next">»</a></li></ul>

   <ul class="pagination last_page_blk">
   <!-- <li class="disabled"><span>«</span></li> 
   <li class="active"><span>1</span></li>
   <li><a href="model?page=2">2</a></li>
   <li><a href="model?page=3">3</a></li>
   <li><a href="model?page=4">4</a></li>
   <li><a href="model?page=5">5</a></li> 
   <li><a href="model?page=2" rel="next">»</a></li> -->
   <li class="yellow_bg_clr"><a href="#" rel="next">last</a></li>
   </ul>

 </div>
 </div>





		</div>

		<div class="col-md-3">
			@include('post.widget')
		</div>

	</div>
</div>

<div class="text-center"> {!! $pagination->render() !!}</div>



   <div class="load_more_btn_div col-md-12 col-sm-12 col-xs-12">
 <button><span>Load More</span></button>
 </div>
</div>
</div>