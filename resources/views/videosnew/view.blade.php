@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('videosnew?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('videosnew/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
	   		 @if($row->user_name != '')  {{$row->user_name }}'s Video
	   			@else Videos Management @endif
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('videosnew/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('videosnew/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
						<!-- <tr>
							<td width='30%' class='label-view text-right'>Id</td>
							<td>{{ $row->id}} </td>
							
						</tr> -->
					
						<tr>
							<td width='30%' class='label-view text-right'>User Name</td>
							<td>{{$row->user_name }}</td>
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>{{ $row->user_type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Video Id</td>
							<td>{{ $row->video_id}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Title</td>
							<td>{{ $row->title}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Description</td>
							<td>{{ $row->description}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Tags</td>
							<td>@if( $row->tags == '')
								-								
								@else
								{{$row->tags }}
								@endif  </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Category</td>
							<td>{{ \SiteHelpers::getName('vid_categories',$row->category) }} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Status</td>
							<td>@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif </td>
						
							
						</tr>
					
						<!-- <tr>
							<td width='30%' class='label-view text-right'>Vip</td>
							<td>{{ $row->vip}} </td>
							
						</tr> -->
					
						<tr>
							<td width='30%' class='label-view text-right'>Preview</td>
							<td>{{ $row->preview}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Thumb</td>
							<td>{{ $row->thumb}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Video Path</td>
							<td>{{ $row->video_path}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Duration</td>
							<td>{{ $row->duration}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Width</td>
							<td>{{ $row->width}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Height</td>
							<td>{{ $row->height}} </td>
							
						</tr>
					
						<!-- <tr>
							<td width='30%' class='label-view text-right'>Size</td>
							<td>{{ $row->size}} </td>
							
						</tr> -->
					
						<tr>
							<td width='30%' class='label-view text-right'>Type</td>
							<td>{{ $row->type}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Youtube Id</td>
							<td>@if($row->youtube_id!='') {{ $row->youtube_id}} @else - @endif</td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Vimeo Id</td>
							<td>@if($row->vimeo_id!='') {{ $row->vimeo_id}} @else - @endif </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Dailymotion Id</td>
							<td>@if($row->dailymotion_id!='') {{ $row->dailymotion_id}}  @else - @endif</td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Created At</td>
							<td>{{ $row->created_at}} </td>
							
						</tr>
					
						<tr>
							<td width='30%' class='label-view text-right'>Updated At</td>
							<td>{{ $row->updated_at}} </td>
							
						</tr>
					
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop