@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<style type="text/css">
.tagsinput {
min-height: 36px !important;
height: 0px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<div class="page-content row">
	<div class="page-content-wrapper m-t">	 	
		<div class="sbox">
			<div class="sbox-title">
				<h1>Videos Management</h1>

<!-- 		<div class="sbox-tools" >
@if($access['is_excel'] ==1)
<a href="{{ URL::to('events/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
<i class="fa fa-cloud-download"></i></a>
@endif

<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
@if(Session::get('gid') ==1)
<a href="{{ URL::to('module/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
@endif 
</div> -->
</div>
<div class="sbox-content"> 
	<div class="sbox-tools pull-left" >
		<!-- Approve/unapprove -->
<a href="javascript://ajax" class="tips btn btn-sm btn-default updatestatus" title="Approve" data-type="1">
<i class="fa fa-check"></i>&nbsp;Approve</a>
<a href="javascript://ajax" class="tips btn btn-sm btn-default updatestatus" title="Unapprove" data-type="2">
<i class="fa fa-times"></i>&nbsp;Unapprove</a>
		@if($access['is_add'] ==1)
		<!-- <a href="{{ URL::to('videosnew/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create </a> -->
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default updatestatus" data-type="0" title="{{ Lang::get('core.btn_remove') }}">
				<i class="fa fa-trash-o"></i> Delete </a>
				@endif
				<a href="{{ URL::to( 'events/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}" style="display:none"><i class="fa  fa-search"></i> </a>				
<!-- 
@if($access['is_excel'] ==1)
			<a href="{{ URL::to('agency/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif -->
			</div>
 
	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'videosnew/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	     {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}

	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				<!-- <th ><span>{{ Lang::get('core.btn_action') }}</span></th> -->
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
							@if($t['label']!='Id' && $t['label']!='User Id' && $t['label']!='Description' && $t['label']!='Vip' && $t['label']!='Created At' && $t['label']!='Updated At' && $t['label']!='Thumb' && $t['label']!='Video Path' && $t['label']!='Width' && $t['label']!='Height' && $t['label']!='Size' && $t['label']!='Type' && $t['label']!='Youtube Id' && $t['label']!='Vimeo Id' && $t['label']!='Dailymotion Id')
							<th><span>{{ $t['label'] }}</span></th>		
							@endif	
						@endif 
					@endif
				@endforeach
				<th><span>Owner Name</span></th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
				<th ><span>Message</span></th>
			  </tr>
        </thead>

        <tbody class="videsness">   
        <tr id="orderDetails">
		<td class="number"></td>
		<td></td>
		<td id="Usertype" class="fieldsearch"> <div class="select-style">{!! Form::text('Usertype',"{$Usertype}",array('class'=>'form-control', 'placeholder'=>'User Type', )) !!} </div></td>
		<td id="video_id" class="fieldsearch"> <div class="select-style">{!! Form::text('video_id',"{$video_id}",array('class'=>'form-control', 'placeholder'=>'VideoId', )) !!} </div></td>
		
		<td id="Title" class="fieldsearch"> <div class="select-style">{!! Form::text('Title',"{$Title}",array('class'=>'form-control', 'placeholder'=>'Title', )) !!} </div></td>
		<!-- <td id="Tags" class="fieldsearch"> <div class="select-style"><input type="text" name='tags' id='tags' class='form-control' value="" /> </div></td> -->
		<td id="Tags" class="fieldsearch" style="width:100px"> <div class="select-style">{!! Form::text('tags',"{$Tags}",array('id'=>'tags','class'=>'form-control', 'placeholder'=>'Tags', )) !!} {!! Form::hidden('Tags','',array('id'=>'Alltags','class'=>'form-control', 'placeholder'=>'Tags', )) !!}</div></td>
		<td id="categories" class="fieldsearch"> <div class="select-style">
		{!! Form::select('categories',array('' => 'Select categories') +\SiteHelpers::getTableValues('vid_categories'),"{$categories}",array('class' => 'select2','id' => 'categories')) !!}
		</div></td>

		<td id="status" class="fieldsearch"> 
        			<div class="select-style">
        				<select name='status' class = 'select2 '>
        					<option value="">Select option</option>
        					<option value="1" {{ $status == '1' ? 'selected' : ''}}>Active</option>
        					<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>
        					<option value="deleted" {{ $status == 'deleted' ? 'selected' : ''}}>Deleted</option>

        				</select>

        			</div>
        		</td>
		<td></td><td></td>
		<td id="ownername" class="fieldsearch"> <div class="select-style">{!! Form::text('ownername',"{$ownername}",array('class'=>'form-control', 'placeholder'=>'Ownername', )) !!} </div></td>

		<td class="icons">
		    <input type="hidden"  value="Search">
		    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
		    <a href="{{ url('videosnew') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
		</td>
		</tr>  
            {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu" style="background-color:#ea962e">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('videosnew/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('videosnew/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td> -->

				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
					 	@if($field['label']!='Id' && $field['label']!='User Id' && $field['label']!='Description' && $field['label']!='Vip' && $field['label']!='Created At' && $field['label']!='Updated At' && $field['label']!='Thumb' && $field['label']!='Video Path' && $field['label']!='Width' && $field['label']!='Height' && $field['label']!='Size' && $field['label']!='Type' && $field['label']!='Youtube Id' && $field['label']!='Vimeo Id' && $field['label']!='Dailymotion Id')
						 <td>	
						    @if($field['label']=='Preview')
						    	<a href="{{ URL::to('http://demo.bsetec.com/demo/magelur/public/video/'.$row->video_id) }}" target="_blank">
						    	 <img src="{{ $row->preview }}" alt="{{$row->title}}" width="120" height="120">
						    	</a>	
						    @elseif($field['label']=='Category')
						    	{{ App\Helper\Helper::category_by_id($row->category) }}
						    @elseif($field['field']=='status' )
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
								@elseif( $row->status == 'deleted')
								<span class="label label-danger">Deleted</span>
								@elseif( $row->status == '0')
								<span class="label label-warning">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
						    @else
						 		{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}		
						 	@endif				 
						 </td>
						 @endif
						@endif	
					 @endif					 
				 @endforeach	
				 <td>{{$row->user_name }}</td>	 <td>
				 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('videosnew/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
					@endif
					@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('videosnew/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
					@endif
				 </td>
				 <td><a  class="tips btn btn-xs btn-white" data-toggle="modal" data-target="#myModal" title="Send Mail"  onclick="getmailid('{!! SiteHelpers::getUserEmailbyId($row->user_id,$row->user_type) !!}');"><i class="fa fa-envelope "></i></a></td>
                </tr>
				 {{--*/ $i++ /*--}}
            @endforeach
  
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />
	@if(count($model_filter) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	{!! Form::close() !!}

	<!-- The Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
        <h4 class="modal-title" style="color:black">Send Mail</h4>
      </div>
      <div class="modal-body">
       <form role="form" method="get" id="reused_form" action="agentmail/videos">

            <div class="row">
            	<div class="col-sm-12 form-group">
                	<label for="subject" style="color:black">
                        Subject:</label>
                     <textarea class="form-control" type="textarea" name="subject" id="subject" maxlength="200" rows="1" style="background-color:white;color:black"></textarea>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="message" style="color:black">
                        Message:</label>
                    <textarea class="form-control" type="textarea" name="message" id="message" maxlength="6000" rows="7" style="background-color:white;color:black"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                         From:</label>
                    <input type="email" class="form-control" id="fromemail" name="fromemail" style="background-color:white;color:black" value = "{{ CNF_EMAIL }}" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                        To:</label>
                    <input type="email" class="form-control" id="email" name="email" style="background-color:white;color:black" required>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-white pull-right" >Send →</button>
                </div>
            </div>

        </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			  @if( $field['field'] == 'id' || $field['field'] == 'description' || $field['field'] == 'vip' || $field['field'] == 'preview'|| $field['field'] == 'thumb'|| $field['field'] == 'video_path'|| $field['field'] == 'width'|| $field['field'] == 'height'|| $field['field'] == 'size'|| $field['field'] == 'type'|| $field['field'] == 'youtube_id'|| $field['field'] == 'vimeo_id'|| $field['field'] == 'dailymotion_id'|| $field['field'] == 'created_at'|| $field['field'] == 'updated_at') {{ $field['field'] == '' }}  @elseif( $field['field'] == 'user_id')  <option value="user_name" @if(isset($pager['sort']) && $pager['sort']=='user_name')
					selected="selected" @endif @else
			  <option value="{{ $field['field'] }}" 
			  @endif 
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				
				@endif		
			  >@if($field['label'] == 'User Id'){{ 'User Name'}} @else {{ $field['label'] }} @endif</option>
			@endif	  
		  @endforeach
		 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-2">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
				
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
</p>
@endif
	  </div>
	</div>
	<!-- @include('footer') -->
	</div>
</div>	
	</div>	  
</div>	
<script type="text/javascript">
//  tag plugin
var alltags='';
$('#tags').tagsInput({
	width:'auto',
	onChange: function(elem, elem_tags)
	{
		// alert(elem_tags);
		alltags =elem_tags+','+alltags;
		document.getElementById("Alltags").value=alltags;
		// alert(alltags);
		
	}
});

$("#reused_form").validate({
			ignore: "not:hidden",
			rules: {
				message:{
					required:true,
				},
				subject:{
					required:true,
				},
		}
	});

function getmailid(mailid)
 {
 	// alert(mailid);
 	$('#email').val(mailid);
 }

$('.close').click(function(){
$('textarea[name=subject]').val('');
$('textarea[name=message]').val('');
$('label.error').hide();	

});
 $(document).on('click','.updatestatus',function(){
        var status = $(this).data('type');
        $('#actiontype').val(status);
        if(status=='0' && $("input[name='ids[]']:checked").length==0){
            alert('Please select atleast one checkbox!');
        }else if($("input[name='ids[]']:checked").length>0){
            $('#SximoTable').submit();
        }else{
            alert('select checkbox');
        }
        
    });
</script>
	
@stop