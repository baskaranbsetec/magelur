

		 {!! Form::open(array('url'=>'videosnew/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
							<fieldset><legend> Videosnew</legend>
										
										  <div class="form-group  " >
											<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
											<div class="col-md-7">
											  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="User Id" class=" control-label col-md-4 text-left"> User Id </label>
											<div class="col-md-7">
											  <input  type='text' name='user_id' id='user_id' value='{{ $row['user_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="User Type" class=" control-label col-md-4 text-left"> User Type </label>
											<div class="col-md-7">
											  <input  type='text' name='user_type' id='user_type' value='{{ $row['user_type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Video Id" class=" control-label col-md-4 text-left"> Video Id </label>
											<div class="col-md-7">
											  <input  type='text' name='video_id' id='video_id' value='{{ $row['video_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
											<div class="col-md-7">
											  <input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Description" class=" control-label col-md-4 text-left"> Description </label>
											<div class="col-md-7">
											  <textarea name='description' rows='5' id='description' class='form-control '  
					           >{{ $row['description'] }}</textarea> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
											<div class="col-md-7">
											  <input  type='text' name='tags' id='tags' value='{{ $row['tags'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Category" class=" control-label col-md-4 text-left"> Category </label>
											<div class="col-md-7">
											  <input  type='text' name='category' id='category' value='{{ $row['category'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
											<div class="col-md-7">
											  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Vip" class=" control-label col-md-4 text-left"> Vip </label>
											<div class="col-md-7">
											  <input  type='text' name='vip' id='vip' value='{{ $row['vip'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Preview" class=" control-label col-md-4 text-left"> Preview </label>
											<div class="col-md-7">
											  <input  type='text' name='preview' id='preview' value='{{ $row['preview'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Thumb" class=" control-label col-md-4 text-left"> Thumb </label>
											<div class="col-md-7">
											  <input  type='text' name='thumb' id='thumb' value='{{ $row['thumb'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Video Path" class=" control-label col-md-4 text-left"> Video Path </label>
											<div class="col-md-7">
											  <input  type='text' name='video_path' id='video_path' value='{{ $row['video_path'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Duration" class=" control-label col-md-4 text-left"> Duration </label>
											<div class="col-md-7">
											  <input  type='text' name='duration' id='duration' value='{{ $row['duration'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Width" class=" control-label col-md-4 text-left"> Width </label>
											<div class="col-md-7">
											  <input  type='text' name='width' id='width' value='{{ $row['width'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Height" class=" control-label col-md-4 text-left"> Height </label>
											<div class="col-md-7">
											  <input  type='text' name='height' id='height' value='{{ $row['height'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Size" class=" control-label col-md-4 text-left"> Size </label>
											<div class="col-md-7">
											  <input  type='text' name='size' id='size' value='{{ $row['size'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Type" class=" control-label col-md-4 text-left"> Type </label>
											<div class="col-md-7">
											  <input  type='text' name='type' id='type' value='{{ $row['type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Youtube Id" class=" control-label col-md-4 text-left"> Youtube Id </label>
											<div class="col-md-7">
											  <input  type='text' name='youtube_id' id='youtube_id' value='{{ $row['youtube_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Vimeo Id" class=" control-label col-md-4 text-left"> Vimeo Id </label>
											<div class="col-md-7">
											  <input  type='text' name='vimeo_id' id='vimeo_id' value='{{ $row['vimeo_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Dailymotion Id" class=" control-label col-md-4 text-left"> Dailymotion Id </label>
											<div class="col-md-7">
											  <input  type='text' name='dailymotion_id' id='dailymotion_id' value='{{ $row['dailymotion_id'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Created At" class=" control-label col-md-4 text-left"> Created At </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> </fieldset>
				</div>
				
				

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
