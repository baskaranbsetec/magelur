@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('categories?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('categories/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
	   		<span class="titlu"> Categories </span>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('categories/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('categories/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			<!-- @if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif --> 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<!-- <tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				 -->
					<tr>
						<td width='30%' class='label-view text-right'>Name</td>
						<td>{{ $row->name}} </td>
						
					</tr>
				
					<!-- <tr>
						<td width='30%' class='label-view text-right'>Slug</td>
						<td>{{ $row->slug}} </td>
						
					</tr> -->
				
<!-- <tr>
<td width='30%' class='label-view text-right'>Image</td>
<td>
<a href="{{ asset('uploads/categories/'.$row->image) }}" target="_blank" class="previewImage">

<img src="{{ asset('uploads/categories/'.$row->image) }}" border="0" width="50" class="img-circle">
</a> </td>

</tr> -->
				
					
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>{{ $row->created_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>{{ $row->last_updated}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop