<link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
<script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<style type="text/css">
ul.profile_details_list li, .profile_details h3{
	color:#fff;
	font-size: 14px;

}
 .content{
 padding: 10px -26px 20px; 

}
.content p {
    color: #8f8f8f;
    font-family: "Lato-Regular";
    font-size: 17px;
}

.content h4 {
    color: #ffffff;
    font-family: "Lato-Regular";
    font-size: 16px;
    text-transform: uppercase;
}
.top  {
    margin-top: 21px;
   
}
</style>
<div class="model_profile_ban membt  ">
 <section>
      <div class="container-fluid magelur_header">
        <div id="upload-demo-i-cover">
                {!! SiteHelpers::user_avatar($row->profile_cover,'/uploads/model_profile/cover/') !!}

        </div>
      </div>
    </section>
  <div class="ourmod_cnt wow bounce">
    <h1>Model - Profile</h1>
  </div>
   
</div>
                  


                  <section class="modelprofile_details">
                    <div class="container">
                      <div class="row">

                      <div class="col-md-offset-1 col-md-11 col-sm-12">
                      <div class="col-md-4 col-sm-12">
                        <div class="model_profile_pic">
                          <img src="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" class="img-responsive">
                        </div>

                        <div class="model_prof_btn">
                          <a href="javascript:void(0);" class="prof_btn" id="composeMessage">CONTACT THIS MODEL</a>
                          <a href="" class="prof_btn">GALLERY</a>
                          <a href="{{ url('model/video/'.$row->id) }}" class="prof_btn">VIDEO</a>
                        </div>
                      </div>

                      <div class="col-md-7 col-sm-12 model_prof_det">
                        <div class="profile_top_details">
                          <h3>{{$row->username}}</h3>
                          <p>Modelling, Photo Shoot, Video Production, Websites / Online</p>
                          <div class="model_social">
                          <ul class="model_social_ico">
                            <li class=""><a href="https://www.facebook.com/sharer/sharer.php?u={{ url('myprofile/'.$row->id)}}&t={{ url('myprofile/'.$row->id)}}"
                            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                            target="_blank" title="Share on Facebook">
                            <span class="facebook"></span></a></li>
                            <li class="">
                            <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://plus.google.com/share?url={{ url('myprofile/'.$row->id)}}">
                            <span class="google"></a>
                            <li>
                            <li class=""><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/share?url={{ url('myprofile/'.$row->id)}}"><span class="twitter"></span></a></li>

                          </ul>
                          </div>
                        </div>

                        <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Name <span class="my-ico">:</span></li>
                              <li><p>{{$row->username}}</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Gender <span class="my-ico">:</span></li>
                              <li><p>{{$row->gender}}</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Age <span class="my-ico">:</span></li>
                              <li><p>{{$row->age}}yrs</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>DOB <span class="my-ico">:</span></li>
                              <li><p>{{$row->DOB}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Ethnicity <span class="my-ico">:</span></li>
                              <li><p>{{$row->ethnicity}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Languages <span class="my-ico">:</span></li>
                              <li><p>{{$row->language}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Education <span class="my-ico">:</span></li>
                              <li><p>{{$row->education}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Profession <span class="my-ico">:</span></li>
                              <li><p>{{$row->profession}}</p></li>
                            </ul>
                            <ul class="profile_list_content">
                              <li>Location <span class="my-ico">:</span></li>
                              <li><p>{{$row->city}},{{$row->country}}</p></li>
                            </ul>
                        </div>


                          <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Height <span class="my-ico">:</span></li>
                              <li><p>Barbie</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Body Type <span class="my-ico">:</span></li>
                              <li><p>Female</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Bust Size <span class="my-ico">:</span></li>
                              <li><p>22Yr</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Waist <span class="my-ico">:</span></li>
                              <li><p>Caucasian</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Hip <span class="my-ico">:</span></li>
                              <li><p>English</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Shoe Size <span class="my-ico">:</span></li>
                              <li><p>High SChool Education</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Hair Type <span class="my-ico">:</span></li>
                              <li><p>Whetever You Want Me</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Eye Color <span class="my-ico">:</span></li>
                              <li><p>Nevada, United States</p></li>
                            </ul>
                        </div>

                       
                         <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Marital Status <span class="my-ico">:</span></li>
                              <li><p>{{$row->marital_status}}</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Children <span class="my-ico">:</span></li>
                              <li><p>{{$row->children}}</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Drink <span class="my-ico">:</span></li>
                              <li><p>{{$row->drinking}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Smoke <span class="my-ico">:</span></li>
                              <li><p>{{$row->smoking}}</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Do Drugs <span class="my-ico">:</span></li>
                              <li><p>{{$row->drugs}}</p></li>
                            </ul>
                             <ul class="profile_list_content">
                              <li>Body Piercing <span class="my-ico">:</span></li>
                              <li><p>{{$row->body_piercing}}</p></li>
                            </ul>
                             <ul class="profile_list_content">
                              <li>Tattoos <span class="my-ico">:</span></li>
                              <li><p>{{$row->tattoos}}</p></li>
                            </ul>

                        </div>

                      </div>
                      </div>
                    </div>


                    <div class="container">
                      <div class="row">
                      <div class="model_bio">
                        <h2>Model (Portfolio) Biography</h2>

                        {!! $row->biography!!}

                        <div class="col-md-4 col-sm-12 bio_dv">
                            <div class="bio_details">
                              <h3>Experiences</h3>
                              <span class="bor"></span>

                              <ul class="bio_list my_bio">
                                <li>Experience</li>
                                <li>Year</li>
                              </ul>

                              <ul class="bio_list my_bio_det">
                                <li>{{$row->model_experience}} </li>
                                <li>{{$row->year}} yrs</li>
                              </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Industries of Interest</h3>
                            <span class="bor"></span>

                            <ul class="bio_list">
                            @foreach($row->model_industries as $ids)
                            <li>@if($row->model_industries) {{$ids->name}} @else - @endif</li>
                            @endforeach
                            </ul>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Categories of Interest</h3>
                            <span class="bor"></span>

                            <ul class="bio_list">
                            @foreach($row->model_categories as $id)
                            <li>@if($row->model_categories)  {{$id->name}} @else - @endif</li>
                            @endforeach
                            </ul>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Hobbies</h3>
                            <span class="bor"></span>

                            <ul class="bio_list">
                            <li>{!! $row->hobbies !!}</li>
                            </ul>

                            </div>
                        </div>
                      </div>
                      </div>
                    </div>

                  </section>
                  <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="max-width:800px;width:90%;">
    <div class="modal-content">
    <div class="modal-header bg-default">
      
      <button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-titles">Compose Message </h4>
    </div>
    <div class="load_notify"></div>
   <div class="modal-body" id="compose-modal-content">
       {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
        <input type="hidden" name="typee" class="type" />
        <div class="form-group">
          <label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
          <div class="col-xs-10">
            <div>
              <input type='hidden' name='recipient' value='{!! $row->id !!}' class='form-control' >{!! $row->firstname !!} {!! $row->lastname !!}
              
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
          <div class="col-xs-10">
            <div>{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control','id' => 'subject')) !!}
</div>

          <span class="err_subject" style="color:red"></span>
          </div>
        </div>
        <div class="form-group">
          <label for="mail_subject" class="col-xs-2">Message</label>
          <div class="col-xs-10">
            <div><textarea name='message' rows='3' style="width:100%;" class='form-control mceEditor' placeholder="Enter your message"></textarea></div>
            <span class="error" style="color:red"></span>
          </div>
        </div>
        <div class="form-group">
        <label for="mail_subject" class="col-xs-2">Attach files</label>
        <div class="col-xs-10">
        <input  type="file" class="form-control" name="images[]" id='images' placeholder="" multiple>
       
        [Allow multiple image] 
        </div>
        </div>
        <div class="sending_loadergs"></div>
        <div class="form-group">
          <label class="col-xs-2"></label>
          <div class="col-xs-10">
            {!! Form::hidden('draft', '0' )  !!}
            {!! Form::hidden('messageid', '' )  !!}
            <button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i>Send message</button>
<!--             <button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
 -->          </div>
        </div>
       {!! Form::close() !!}
    </div>

    </div>
  </div>
</div>


<script>

 $(document).ready(function(){

  
  
  $(function(){
    tinymce.init({  
      mode : "specific_textareas",
      editor_selector : "mceEditor",
      theme : "advanced",
      theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
     });  
  });
  

  function tinyClean(value) {
  value = value.replace(/&nbsp;/ig, ' ');
  value = value.replace(/\s\s+/g, ' ');
  if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
    value = '';
  }
  return value;
}

   $(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       
      
        
      },
      messages: {
       
       
      },
      
        submitHandler: function(form) {

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
    var subject = $('[name="subject"]').val();
     if(subject==''){
      $('.err_subject').html("Subject Field is required");
    }
    else{
      $('.err_subject').hide();

    }
    if(value==''){
      $('.error').html("Message Field is required");
    }else{
      $('.error').hide();
    }
    
    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();
        toastr.success("Message sent Successfully");
         $('#compose-modal').modal("toggle"); 
         form.submit();
}
           
        },

        });
  

  $('#composeMessage').click(function(){
    
    $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
 


  

}); 



  </script>