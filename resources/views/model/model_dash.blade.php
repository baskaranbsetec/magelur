@include('model_dash_header')

<div class="dashboard-ders">
 <div class="container">  
  <div class="left_part_ders">
   <ul class="clearfix">
    <li class="abt_mtb">
     <div class="full_dash_bg">
      <div class="left_abt clearfix"><h4>About us</h4> <a href="{{ URL::to('aboutus')}}">More</a></div>
      <p>@if($row->biography !=''){{$row->biography}} @else
        <div class="empty_vertical">
    <div class="empty_msg_details clearfix">
    <div class="empty_msg_blk clearfix">
      <div class="heading_blk">
        <div class="heading_details">
          <h1>About Us is empty</h1>
          <h2>
            Add  content from profit edit page
          </h2>
          <div class="file_img_blk">
        <div class="file_img_div">
          <i class="fa fa-warning"></i>
      </div>
  </div>
        </div>
      </div>
    </div>
  </div>
  </div>
       @endif</p>
     <!--  <p>I'm a natural talent and I want to help the entertainment industry. I want to be known as the barbie of entertainment. I have danced before and done a few amateur work, as well as done some modelling. I'm a natural talent and I want to help the entertainment industry. I'm a natural talent and I want to help the entertainment industry. I want to be known as the barbie of entertainment. I have danced before and done a few amateur work, as well as done some modelling. I'm a natural talent and I want to help the entertainment industry.</p>
      <p> I'm a natural talent and I want to help the entertainment industry. I want to be known as the barbie of entertainment. I have danced before and done a few amateur work, as well as done some modelling. I'm a natural talent and I want to help the entertainment industry. </p>
      <p>I'm a natural talent and I want to help the entertainment industry. I want to be known as the barbie of entertainment. I have danced before and done a few amateur work, as well as done some modelling. I'm a natural talent and I want to help the entertainment industry. I'm a natural talent and I want to help the entertainment industry. I want to be known as the barbie of entertainment. I have danced before and done a few amateur work, as well as done some modelling. I'm a natural talent and I want to help the entertainment industry.</p> -->
     </div> 
    </li>
    <li class="right_mtb">
     <div class="full_dash_bg"> 
      <div class="left_abt clearfix"><h4>Message</h4> <a href="{{ URL::to('message')}}">More</a></div>

       <div class="moment clearfix " >  
            
      <table class="table table-hover model_dash_table" id='userTbl'>
        <tbody class="dataMessage model_dash_msg" >
        @foreach($results as $row)
          <input type="hidden" name="entryby" class="entryby" value="{{ $row->entry_by }}" />
          {{--*/ if($row->entry_by == \Session::get('agent_id')) $isSender = 1; else $isSender = 0; /*--}}
          <tr class="mail-unread" id="msg_{!! $row->id !!}"><tr class="mail" id="msg_{!! $row->id !!}">
          <td  data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}" class="model_dash_first"  width="10" style="padding-bottom:10px; padding-right:0px;"></td>
          {{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
          {{--*/ if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } /*--}}
          {{--*/ if($isSender == 1 && isset($usersModelAvatar[$row->recipient])) { $rowUserAvatarModel = $usersModelAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersModelAvatar[$row->entry_by])) { $rowUserAvatarModel = $usersModelAvatar[$row->entry_by]; } else { $rowUserAvatarModel = ''; } /*--}}
          {{--*/ if($isSender == 1 && isset($usersModelNameList[$row->recipient])) { $rowModelUser = $usersModelNameList[$row->recipient]; } else if($isSender == 0 && isset($usersModelNameList[$row->entry_by])) { $rowModelUser = $usersModelNameList[$row->entry_by]; } else { $rowModelUser = ''; } /*--}} 
          <td class="td-clickable text-success" data-username="{{$row->id}}">
          <div class="userimg onactive user_circle">
            @if($row->model_agent == '2')
          {!! SiteHelpers::user_profiles($row->id,'agent',$rowUserAvatar,'/uploads/agency_profile/') !!}
          @elseif($row->model_agent == '1')
          {!! SiteHelpers::user_profiles($row->id,'model',$rowUserAvatarModel,'/uploads/model_profile/') !!}
          @endif
            <!-- <img src="{{ asset('uploads/agency_profile/'.$rowUserAvatar) }}" border="0" width="25" height="25" class="img-circle"> -->
          </div>
          <div class="usertext">
          <a class="">
            @if($row->model_agent == '1') {!! $rowModelUser !!} @elseif($row->model_agent == '2') {!! $rowUser !!} @elseif($row->model_agent == '3') Admin @endif</a> 

          </a>  
          <input type="hidden" name="viewrowUser" class="viewrowUser" value="{!! $rowUser !!}" />
          {{---*/ $content = substr( $row->message,0,30 ) /*---}} 
            <p>{!! $content !!}...</p>
          </div>
          <div class="more_dots">
          <a href="javascript:void(0)"></a>
          <p>{{--*/$zone=date("Y-m-d g:i a", strtotime($row->createdOn));/*--}}
          {!! \SiteHelpers::time_ago($zone) !!}</p>
          </div>
          </td>
          </tr>

        @endforeach
          @if ( empty( $results) ) 
          No New Messages !
          @endif    
        </tbody>
      </table>  
    </div> 
  

   </div> 
    </li>
  </ul> 
 </div> 
 <div class="multimediasection">
  <ul class="multimedia clearfix">
  
   <li class="photo_media">
    <div class="medinet"> 
     <div class="left_abt clearfix"><h4>Photos</h4> <a href="{{url('/model/album/'.\Session::get('url_securityID'))}}">More</a></div>
     <ul class="multi_three_rows clearfix">
   <!--    <li class="add_photo1">
        <a href="{{url('photo/upload')}}"><span><i class="fa fa-plus-circle"></i></span></a>
      </li> -->
    @if(count($gallery)>0)
    @foreach($gallery as $gal)
     {{--*/ $ID =urlencode( base64_encode($gal->ID));/*--}}
       @if($gal->Status=='Active')
        <li>
            <a href="{{url('/photo/'.$ID)}}"><img src="{!! \SiteHelpers::user_albums($gal->ID,'_small',$gal->Ext) !!}"></a>
        </li>
       @endif
      @endforeach
      @else
    <div class="empty_vertical">  
    <div class="empty_msg_details clearfix">
    <div class="empty_msg_blk clearfix">
      <div class="heading_blk">
        <div class="heading_details">
          <h1>No more Photos!</h1>
          <h2>
            Upload Photos to show here
          </h2>
          <div class="file_img_blk">
            <div class="file_img_div">
              <i class="fa fa-warning"></i>
            </div>
          </div>
          <!-- <img src="{{asset('images/file_image1.png')}}"> -->
        </div>
      </div>
    </div>
  </div>
  </div>
    @endif
    </ul>
    </div>  
   </li>  
  
   <li class="video_media">
   <div class="medinet">  
    <div class="left_abt clearfix"><h4>Videos</h4> @if($views>0)<a href="{{ url('model/video/'.\Session::get('url_securityID')) }}">More</a>@else @endif</div>
    @if($videos)
    <div class="vide_pat">
      
        <!-- <img src="{{ asset('images/modenload.png') }}"> -->
        <!-- <img src="{{ $videos->preview }}" class="gallery_images1 gallery_video_images1"> -->
        @if (!is_null($videos->vimeo_id))
        <!-- Vimeo Video -->
        <iframe src="https://player.vimeo.com/video/{{ $videos->vimeo_id }}" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        @elseif (!is_null($videos->dailymotion_id))
        <!-- Dailymotion Video -->
        <iframe frameborder="0" width="100%" height="360" src="//www.dailymotion.com/embed/video/{{ $videos->dailymotion_id }}" allowfullscreen></iframe>
        @elseif (!is_null($videos->youtube_id))
        <iframe frameborder="0" width="100%" height="360" src="https://www.youtube.com/embed/{{ $videos->youtube_id }}" allowfullscreen></iframe>
        @else
        <iframe frameborder="0" width="100%" height="360" src="{{ $videos->video_path}}?rel=0" allowfullscreen></iframe>
        @endif
      <div class="here_afterplaty">
        <!-- <a href="{{url('video')}}/{{ $videos->video_id}}" class="hover-posts" target='_blank'></a> -->
      </div>
    </div>
    @else
     <div class="empty_msg_details clearfix">
  <div class="empty_msg_blk clearfix">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>No more Videos!</h1>
        <h2>
          You have not yet upload any video. Go to Video Page and upload Your Videos
        </h2>
        <div class="file_img_blk">
        <div class="file_img_div">
          <i class="fa fa-warning"></i>
      </div>
  </div>
        <!-- <img src="{{asset('images/file_image1.png')}}"> -->
      </div>
      
    </div>
    
  </div>
</div>
    @endif

    </div>  
   </li>
   <li class="video_node">
    <div class="medinet clearfix">    
    <div class="left_abt clearfix"><h4>Network</h4> <a href="{{url('/model-network/'.\Session::get('url_securityID'))}}">More</a></div>
   
     @if (count($a_messages) == 0 && count($messages)==0)
     <div class="empty_vertical">
    <div class="empty_msg_details clearfix">
    <div class="empty_msg_blk clearfix">
      <div class="heading_blk">
        <div class="heading_details">
          <h1>No more Network!</h1>
          <h2>
            You have not yet connect any of the Agents/Models. Go to Agent/Models Listing Page and get connected with more contacts  
          </h2>
          <div class="file_img_blk">
        <div class="file_img_div">
          <i class="fa fa-warning"></i>
      </div>
  </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  @else
      
    <ul class="multi_three_rows">

      @foreach($a_messages as $message)
      {{--*/ if(isset($a_mess_name[$message->recipient])) { $messageUser = $a_mess_name[$message->recipient]; } else if(isset($a_mess_name[$message->entry_by])) { $messageUser = $a_mess_name[$message->entry_by]; } else { $rowUser = ''; } /*--}}
      <li><a href="{{ url(App\Helper\Helper::get_user_profile_link($message->recipient,'agent',$messageUser)) }}">
        @if(\SiteHelpers::getagentnetworks($message->recipient))
        <img src="{{ asset('uploads/agency_profile/'.\SiteHelpers::getagentnetworks($message->recipient) ) }}">
        @endif
      </a></li>
      @endforeach
     
    </ul>
    <ul class="multi_three_rows">
      @foreach($messages as $message)
       {{--*/ if(isset($m_mess_name[$message->recipient])) { $messageUser = $m_mess_name[$message->recipient]; } else if(isset($m_mess_name[$message->entry_by])) { $messageUser = $m_mess_name[$message->entry_by]; } else { $rowUser = ''; } /*--}}
      <li><a href="{{ url(App\Helper\Helper::get_user_profile_link($message->recipient,'model',$messageUser)) }}">
        @if(\SiteHelpers::getmodelnetworks($message->recipient))
        <img src="{{ asset('uploads/model_profile/'.\SiteHelpers::getmodelnetworks($message->recipient) ) }}">
        @endif
        
      </a></li>
      @endforeach
    </ul>
      @endif 
  </div>
   </li>
  </ul>
  <div class="membershiper">
    <div class="left_abt clearfix"><h4>Membership</h4> <a href="{{url('/model-membership/'.\Session::get('url_securityID'))}}">More</a></div>
    @foreach($membershipplan as $plan)
    @if($model['pause_subscription'] =='paused')
    
        <p> No Plans right now. Please Subscribe and make use of New Plans right now! </p>


    @else
      <p><span class="sub_stateone">Subscription Plan</span>    
      <span class="sub_static">:</span>  {{$plan->plan_name}} Membership</p> 

       @if($purchase_period !='')
      <p><span class="sub_stateone">Plan Duration</span> 
      <span class="sub_static">:</span> One {{$purchase_period}} </p> @endif
   
      <p><span class="sub_stateone">Renewal date</span>      
      <span class="sub_static">:</span> @if($renewal_date !='') {{$renewal_date}} @else Till new subscription @endif</p>
    @endif
    <!-- <p>Subscription Plan     :  {{$plan->plan_name}} Membership</p>
    <p>Plan Duration  :  One  @if($purchase_period !='') {{$purchase_period}} @else month @endif</p>
    <p>Renewal date       :  @if($renewal_date !='') {{$renewal_date}} @else - @endif</p> -->
    @endforeach
  </div>  
  </div>  
 </div>
</div>  
@foreach($results as $row)
  {{--*/ if($row->entry_by == \Session::get('agent_id')) $isSender = 1; else $isSender = 0; /*--}}
  <div class="hidden viewMessageDiv" id="viewMessageDiv_{!! $row->id !!}">
    <div class="msg_right_box ">
      <div class="close_btn_div">
        <button class="btn  gotodataMessageDiv" type="">
        <i class="fa fa-close"></i>
        </button>
      </div>
      <div class="total_newer clearfix">
        <div class="renew ">
          <div id='start'>
          <input type="hidden" name="viewMessageimage_{!! $row->id !!}" class="viewMessageimage viewMessageimage_{!! $row->id !!} pull-left" value='{!! $row->attachments !!}'></div>
          <h3  >Subject:{!! \SiteHelpers::getName('message_subject',$row->subject) !!}</h3>
          {{--*/ if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } /*--}}
          <p><span class="for_span">from</span> <span class="ed_span">{!! $rowUser !!}</span></p>
        </div>
        <div class="minew">
          {{--*/$monthNum=date("m")/*--}}
          <p>
          @if(date('d')==date("d", strtotime($row->createdOn)))
          Today 
          @elseif((date('d')-1)==date("d", strtotime($row->createdOn)))
          Yesterday 
          @else
          {!! date('F', mktime(0, 0, 0, $monthNum, 10))!!}
          @endif
          {!! date(" h:i A", strtotime($row->createdOn)) !!}    </p>
        </div>
        <div class="newtent_content viewMessageContent">
          <p>{!! $row->message !!}</p>
        </div>
        <div class="rader clearfix rader_div rader_msg_div">
          <div class="minyues">
            {{--*/ $images = explode(',', $row->attachments); /*--}}
            @foreach($images as $info) 
              @if($row->attachments !='')
              <span>
              <a  rel='gallery2' href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
              <img src="{{ URL::to('uploads/message/') }}/{{ $info }}" border="0" width="30" class="img-circle" style="margin-right:2px;"></a> 
              </span>
              @else
              <span style='display:none'>
              <a  href="{{ URL::to('uploads/message/') }}/{{ $info }}" target="_blank" class="fancybox" >
              <img src="{{ URL::to('uploads/message/') }}/{{ $info }}" border="0" width="30" height="30" class="img-circle" style="margin-right:2px;"></a> 
              </span>
              @endif
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach
  @include('footer_map')    
      
<script type="text/javascript">
$('.dataMessage tr .td-clickable').click(function(){
var name = $(this).data('username');        
if (name) {
window.location = '{{asset("message?msg_id=")}}' + name;
}
});

</script>
