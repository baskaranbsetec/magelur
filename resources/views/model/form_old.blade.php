@extends('layouts.app')

@section('content')
<style type="text/css">
label.error,.asterix{
color: #cc0000;
font-family: 'Open Sans',Arial,Helvetica,sans-serif;
font-weight: normal;
font-size: 13px;
}
input.error{
border: 1px solid #cc0000;
}

</style>

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<link href="{{ asset('magelur/css/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/css/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>


  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		
		</div>
		<h1> @if($row['id']!='') {{ $row['firstname'] }}'s Profile @else Model Management @endif</h1>
		<div class="col-md-5">
	</div>
	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
 {!! Form::open(array('url'=>'model/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=>'register')) !!}

<!-- <form method="POST" action="{{ url('model/save?return='.$return) }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" "> -->
<div class="col-md-12">
<fieldset><legend> Basic Information</legend>

	<div class="col-md-6">
		{!! Form::hidden('id', $row['id']) !!}	

		<div class="form-group " >
		<label for="Username" class=" control-label col-md-4 text-left"> Username <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('username', $row['username'],array('id'=>"username",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Firstname" class=" control-label col-md-4 text-left"> Firstname <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('firstname', $row['firstname'],array('id'=>"firstname",'class'=>"form-control")) !!}                                	 		
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Lastname" class=" control-label col-md-4 text-left"> Lastname <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('lastname', $row['lastname'],array('id'=>"lastname",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="DOB" class=" control-label col-md-4 text-left"> DOB <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<div class="input-group m-b" >
		{!! Form::text('DOB', $row['DOB'],array('class'=>'form-control date','id'=>'')) !!}
		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		</div>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Gender" class=" control-label col-md-4 text-left"> Gender <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"male",($row['gender']=="male") ? true:false,array()) !!} Male </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"female",($row['gender']=="female") ? true:false,array()) !!} Female </label> 
		</div> 
		</div> 						

		<div class="form-group  " >
		<label for="Education" class=" control-label col-md-4 text-left"> Education <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('education', $row['education'],array('id'=>"education",'class'=>"form-control",'required'=>'required')) !!}                                	 		
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Profession" class=" control-label col-md-4 text-left"> Profession <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('profession', $row['profession'],array('id'=>"profession",'class'=>"form-control",'required'=>'required')) !!}                                	 		
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Hobbies" class=" control-label col-md-4 text-left"> Hobbies <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('hobbies', $row['hobbies'],array('id'=>"hobbies",'class'=>"form-control",'required'=>'required')) !!}                                	 	
		
		</div> 
		</div> 
		<br>
		
		<div class="form-group  " >
		<label for="Biography" class=" control-label col-md-4 text-left"> Biography <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('biography', $row['biography'],array('id'=>"biography",'class'=>"form-control",'required'=>'required')) !!}                                	 		
		</div> 
		</div>  	

	</div>

	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<input  type='file' name='profile_photo' id='profile_photo'  value="{{$row['profile_photo']}}" style='width:200px !important;' /><br>
		<div >
		{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/model_profile/') !!}
		</div>
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="City" class=" control-label col-md-4 text-left"> City <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('city', $row['city'],array('id'=>"city",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="State" class=" control-label col-md-4 text-left"> State <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('state', $row['state'],array('id'=>"state",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Country" class=" control-label col-md-4 text-left"> Country <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),$row['country'],array('class' => 'form-control','id' => 'country')) !!}
 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('zipcode', $row['zipcode'],array('id'=>"zipcode",'class'=>"form-control")) !!}                                	 	
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('email', $row['email'],array('id'=>"email",'class'=>"form-control")) !!}                                	 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('contact_number', $row['contact_number'],array('id'=>"contact_number",'class'=>"form-control")) !!}                                	
		</div> 
		</div>
		

	</div>
</fieldset>
</div>
			
			
	<div class="col-md-12">	<fieldset><legend>Other Information </legend> </fieldset> 	</div>

	<div class="col-md-6">		
		<div class="form-group  " >
		<label for="Ethnicity" class=" control-label col-md-4 text-left"> Ethnicity <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('ethnicity',array('' => 'Ethnicity') +\SiteHelpers::getTableValues('ethnicity') ,$row['ethnicity'],array('class' => 'select2','id' => 'ethnicity')) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Body Type" class=" control-label col-md-4 text-left"> Body Type <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('body_type',array('' => 'Body Type') +\SiteHelpers::getTableValues('body_type') ,$row['body_type'],array('class' => 'select2','id' => 'body_type')) !!} 
		</div> 
		</div> 		

		<div class="form-group  " >
		<label for="Hair Color" class=" control-label col-md-4 text-left"> Hair Color <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('hair_color',array('' => 'Hair Color') +\SiteHelpers::getTableValues('hair_color') ,$row['hair_color'],array('class' => 'select2','id' => 'hair_color')) !!}
		</div> 
		</div> 	


		<div class="form-group  " >
		<label for="Eye Color" class=" control-label col-md-4 text-left"> Eye Color <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('eye_color',array('' => 'Eye Color') +\SiteHelpers::getTableValues('eye_color') ,$row['eye_color'],array('class' => 'select2','id' => 'eye_color')) !!}
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Shoe-size" class=" control-label col-md-4 text-left"> Shoe-size <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('shoe_size', $row['shoe_size'],array('id'=>"shoe_size",'class'=>"form-control")) !!}                                	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Marital Status" class=" control-label col-md-4 text-left"> Marital Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('marital_status',array('' => 'Marital status') +\SiteHelpers::getTableValues('marital_status') ,$row['marital_status'],array('class' => 'select2','id' => 'marital_status')) !!} 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Model Categories" class=" control-label col-md-4 text-left"> Model Categories <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('model_categories',array('' => 'Model Categories') +\SiteHelpers::getTableValues('model_categories') ,$row['model_categories'],array('class' => 'select2','id' => 'model_categories')) !!}
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Drugs" class=" control-label col-md-4 text-left">Drugs <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('drugs',array('' => 'Drugs') +\SiteHelpers::getTableValues('drugs') ,$row['drugs'],array('class' => 'select2','id' => 'drugs')) !!}
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Model Industries" class=" control-label col-md-4 text-left">Model Industries <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('model_industries',array('' => 'Model Industries') +\SiteHelpers::getTableValues('model_industries') ,$row['model_industries'],array('class' => 'select2','id' => 'model_industries')) !!} 
		</div> 
		</div>
 			<div class="form-group  " >
		<label for="Body Piercing" class=" control-label col-md-4 text-left"> Body Piercing <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('body_piercing',1,($row['body_piercing']==1) ? true:false,array()) !!}Yes
		</label>  	
		<label class='radio radio-inline'>
		{!! Form::radio('body_piercing',0,($row['body_piercing']==0) ? true:false,array()) !!}No
		</label>	
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Tattoos" class=" control-label col-md-4 text-left"> Tattoos <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('tattoos',1,($row['tattoos']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('tattoos',0,($row['tattoos']==0) ? true:false,array()) !!} No </label> 
		</div> 
		</div>		
		<div class="form-group  " >
		<label for="Year of experience" class=" control-label col-md-4 text-left">Year of experience <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('years',array('' => 'Year of experience') +[0,1, 2, 3,'More than 3 years'] ,$row['year'],array('class' => 'select2','id' => 'years')) !!}
		</div> 
		</div>
	</div>


	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Publishes" class=" control-label col-md-4 text-left">Publishes <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('publishes',array('' => 'Publishes') +\SiteHelpers::getTableValues('publishes') ,$row['publishes'],array('class' => 'select2','id' => 'publishes')) !!}
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Children" class=" control-label col-md-4 text-left"> Children <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('children',array('' => 'Children') +\SiteHelpers::getTableValues('children') ,$row['children'],array('class' => 'select2','id' => 'children')) !!}
 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Drinking" class=" control-label col-md-4 text-left"> Drinking <span class="asterix"> * </span></label>
		<div class="col-md-7">
			{!! Form::select('drinking',array('' => 'Drinking') +\SiteHelpers::getTableValues('drinking') ,$row['drinking'],array('class' => 'select2','id' => 'drinking')) !!}

		</div> 
		</div>

		<div class="form-group  " >
		<label for="Smoking" class=" control-label col-md-4 text-left"> Smoking <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('smoking',array('' => 'Smoking') +\SiteHelpers::getTableValues('smoking') ,$row['smoking'],array('class' => 'select2','id' => 'smoking')) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Hip" class=" control-label col-md-4 text-left"> Hip <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="input-group">
		{!! Form::text('hip', $row['hip'],array('id'=>"hip",'class'=>"form-control")) !!}                                	
		<span class="input-group-addon">cm</span>
		</div>
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Waist" class=" control-label col-md-4 text-left"> Waist <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="input-group">
		{!! Form::text('waist', $row['waist'],array('id'=>"waist",'class'=>"form-control")) !!}                                	
		
		<span class="input-group-addon">cm</span>
		</div>
		</div> 
		</div> 			

		<div class="form-group  " >
		<label for="Bust Size" class=" control-label col-md-4 text-left"> Bust Size <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('bust_size',array('' => 'Chest') +\SiteHelpers::getTableValues('bust_size') ,$row['bust_size'],array('class' => 'select2','id' => 'bust_size')) !!} 
		</div> 
		</div> 


		<div class="form-group  " >
		<label for="Height" class=" control-label col-md-4 text-left"> Height <span class="asterix"> * </span></label>
		<div class="col-md-7">
		
		{!! Form::select('height',array('' => 'Height') +\SiteHelpers::getTableValues('heights') ,$row['height'],array('class' => 'select2','id' => 'height')) !!}
		</div> 
		</div> 						
													
										 					
		<div class="form-group  " >
		<label for="Language" class=" control-label col-md-4 text-left"> Language <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('language', $row['language'],array('id'=>"language",'class'=>"form-control",'required'=>'required')) !!}                                	

		</div> 
		</div> 	
		<div class="form-group  " >
		<label for="Model Experience" class=" control-label col-md-4 text-left"> Model Experience <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',1,($row['model_experience']==1) ? true:false,array()) !!} New Face </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',2,($row['model_experience']==2) ? true:false,array()) !!} Aspiring Model </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',3,($row['model_experience']==3) ? true:false,array()) !!} Experienced Model </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('model_experience',4,($row['model_experience']==4) ? true:false,array()) !!} Professional Model </label> 
		</div> 
		</div>
	


	</div>

	<div class="col-md-12">	<fieldset><legend></legend> </fieldset> </div>

	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Terms Agree" class=" control-label col-md-4 text-left"> Terms Agree <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('terms_agree',1,($row['terms_agree']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('terms_agree',0,($row['terms_agree']==0) ? true:false,array()) !!} No </label> 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active </label> 					  
		<label class='radio radio-inline'>
		{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive </label>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Approved Status" class=" control-label col-md-4 text-left"> Approved Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('approved_status',1,($row['approved_status']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('approved_status',0,($row['approved_status']==0) ? true:false,array()) !!} No </label> 		</div> 
		</div> 	
		

	 </div>

		
		<div style="clear:both"></div>	

		<div class="form-group">
		<label class="col-sm-4 text-right">&nbsp;</label>
		<div class="col-sm-8">	
		<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
		<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
		<button type="button" onclick="location.href='{{ URL::to('model?return='.$return) }}' " class="btn btn-warning btn-sm ">{{ Lang::get('core.sb_cancel') }} </button>
		</div>	  
		</div> 
		 
		{!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>

<script type="text/javascript">
	$(document).ready(function() { 

				
		$('.removeMultiFiles').on('click',function(){
		var removeUrl = '{{ url("model/removefiles?file=")}}'+$(this).attr('url');
		$(this).parent().remove();
		$.get(removeUrl,function(response){});
		$(this).parent('div').empty();	
		return false;
		});		

		});

		$(function(){

		tinyMCE.init({
		theme : "advanced",
		mode: "exact",
		elements : "biography",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		+ "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		+ "bullist,numlist,outdent,indent",
		theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		+"undo,redo,cleanup,code,separator,sub,sup,charmap",
		theme_advanced_buttons3 : "",
		height:"250px",
		width:"520px"
		});

		$('#hobbies').tagit({
		singleField: true,
		});
		$('#language').tagit({
		singleField: true,
		});

	})

		$("#register").validate({
     ignore: "not:hidden",
      rules: {
        contact_number:{
         required:true,
         number:true,
         minlength:10,
         
       },
      
       
        email:{
          required:true,
          email:true,
         
        },
        // profile_photo:{
        //   required:true,
          
         
        // },
        city:{
         required:true
        },
        zipcode:{
         required:true,
         number:true,
        },
        state:{
         required:true
        },
        country:{
         required:true
        },
        DOB:
        {
        required:true
        },
       
        shoe_size:{
        	required:true,
        	number:true,
        },
         hip:{
        	required:true,
        	number:true,
        },
         waist:{
        	required:true,
        	number:true,
        },
         height:{
        	required:true,
        	number:true,
        },
         marital_status:{
        	required:true,
        },
        smoking:{
        	required:true,
        },
        model_categories:{
        	required:true,
        },
        drinking:{
        	required:true,
        },
        drugs:{
        	required:true,
        },
        children:{
        	required:true,
        },
        model_industries:{
        	required:true,
        },
        publishes:{
        	required:true,
        },
        body_piercing:{
        	required:true,
        },
        tattoos:{
        	required:true,
        },
        model_experience:{
        	required:true,
        },


      },
       messages: {
      contact_number:{
         
         number:"Please enter valid phone number",
         minlength:"Phone number should be 10 number"
       },
       
       
        email:{
          required:"Please enter email id",
          email:"Please enter valid email id"
        },
        // profile_photo:{
        //   required:"Please choose image",
         
        // },
        city:{
         required:"Please select city"
        },
        zipcode:{
         required:"Please select zipcode",
          number:"Numbers only allowed",
        },
         state:{
         required:"Please select state"
        },
        country:{
         required:"Please select country"
        },
        DOB:
        {
        required:"Date of Birth required"
        },
       
        shoe_size:{
         
          number:"Size should be a number",
        },
        hip:{
         
          number:"Hip size should be a number",
        },
        waist:{
         
          number:"Waist size should be a number",
        },
        height:{
         
          number:"Height should be a number",
        },

       },
        submitHandler:function(form){
        $('#reg-button').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"> </i> {{ Lang::get("core.loading") }}</div>');
        $('#reg-button').prop("disabled",true);
        $('#date').removeAttr("disabled");
        form.submit();
        }
    });


		  
</script>		 
@stop