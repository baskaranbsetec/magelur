@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left" >
	   		<a href="{{ url('model?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('model/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a><span class="titlu">@if($row->username !='') {{ $row->username}}' Profile @else Models Management @endif</span>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('model/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('model/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			<!-- @if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif --> 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>@if($row->username) {{ $row->username}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Firstname</td>
						<td>@if($row->firstname) {{ $row->firstname}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Lastname</td>
						<td>@if($row->lastname) {{ $row->lastname}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>DOB</td>
						<td>@if($row->DOB) {{ $row->DOB}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Gender</td>
						<td>@if($row->gender) {{ $row->gender}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>@if($row->city) {{ $row->city}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>@if($row->state) {{ $row->state}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>@if($row->country) {{ $row->country}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>@if($row->zipcode) {{ $row->zipcode}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>@if($row->email)  {{ $row->email}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Biography</td>
						<td>@if($row->biography) {{ $row->biography}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>
						<a href="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" target="_blank" class="previewImage">

						<img src="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" border="0" width="50" class="img-circle">
						</a> 
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Ethnicity</td>
						<td>@if($row->ethnicity) {{ $row->ethnicity}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hair Color</td>
						<td>@if($row->hair_color) {{ $row->hair_color}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Eye Color</td>
						<td>@if($row->eye_color) {{ $row->eye_color}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Shoe Size</td>
						<td>@if($row->shoe_size) {{ $row->shoe_size}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hip</td>
						<td>@if($row->hip) {{ $row->hip}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Waist</td>
						<td>@if($row->waist) {{ $row->waist}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Bust Size</td>
						<td>@if($row->bust_size) {{ $row->bust_size}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Body Type</td>
						<td>@if($row->body_type) {{ $row->body_type}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Height</td>
						<td>@if($row->height) {{ $row->height}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Marital status</td>
						<td>@if($row->marital_status) {{ $row->marital_status}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Smoking</td>
						<td>@if($row->smoking) {{ $row->smoking}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Categories</td>
						<td>@if($row->model_categories) 
							@foreach($row->model_categories as $id)

							 {{$id->name}}  <br> 
							
							@endforeach
							@else Not Specified @endif
						</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Drinking</td>
						<td>@if($row->drinking) {{ $row->drinking}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Drugs</td>
						<td>@if($row->drugs) {{ $row->drugs}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Children</td>
						<td>@if($row->children) {{ $row->children}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Industries</td>
						<td>@if($row->model_industries) 
							@foreach($row->model_industries as $id)

							 {{$id->name}} <br> 
							
							@endforeach
							@else Not Specified @endif
						</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Publishes</td>
						<td>@if($row->publishes) {{ $row->publishes}} @else Not Specified @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Body Piercing</td>
						<td>
							@if( $row->body_piercing == '')
							<span >Not Specified</span>
							
							@elseif( $row->body_piercing == 1)
							<span >Yes</span>
							@elseif( $row->body_piercing == 0)
							<span >No</span>
							@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Tattoos</td>
						<td>
							@if( $row->tattoos =='')
							<span >Not Specified</span>
							
							@elseif( $row->tattoos == 1 )
							<span >Yes</span>
							@elseif( $row->tattoos == 0)
							<span >No</span>
							@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Model Experience</td>
						<td>@if( $row->model_experience == 1)
							<span > New Face </span>
							@elseif( $row->model_experience == 2)
							<span >Aspiring Model</span>
							@elseif( $row->model_experience == 3)
							<span > Experienced Model </span>
							@elseif( $row->model_experience == 4)
							<span >Professional Model</span>
							@else
							<span >Not specified</span>
							@endif
						</td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>Language</td>
						<td>@if($row->language) {{ $row->language}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Education</td>
						<td>@if($row->education) {{ $row->education}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profession</td>
						<td>@if($row->profession) {{ $row->profession}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hobbies</td>
						<td>@if($row->hobbies) {{ $row->hobbies}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>@if($row->contact_number) {{ $row->contact_number}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Terms Agree</td>
						<td>
							@if( $row->terms_agree == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>
							@if( $row->status == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif 
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Status</td>
						<td>
							@if( $row->approved_status == 1)
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
							@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
							@endif
						</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>@if($row->created_date) {{ $row->created_date}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>@if($row->last_updated) {{ $row->last_updated}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>@if($row->approved_status == 1){{ $row->approved_date}} @else Not Approved @endif </td>
						
					</tr>
				
					<!-- <tr>
						<td width='30%' class='label-view text-right'>Password</td>
						<td>{{ $row->password}} </td>
						
					</tr> -->
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop