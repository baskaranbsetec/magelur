
<link href="{{ asset('magelur/css/uploader.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('magelur/js/uploader.js')}}"></script>
<div class="magelur_about faq_sec">
  <!-- show upload only for model & agent -->
@if($user_type == 'model' || $user_type == 'agent')
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>{{ \Session::get('username') }} - Gallery</h1>
      <div class="editbtn_div" data-toggle="modal" data-target="#fotoUpload" data-backdrop="static" data-keyboard="false"><button><span>Image Upload</span></button></div>

      <div id="fotoUpload" class="modal fade my_profile_page" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
              <h4 class="modal-title">Photo Gallery</h4>
            </div>
            <div class="modal-body">
            <div class="upload_content">
                <h3>Upload Your Images Here....</h3>
                <span>
                  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution 
                </span>
              </div>
              <input style="display:none;" type="file" name="vpb-data-file" id="vpb-data-file" onchange="vpb_image_preview(this)" multiple="multiple"  />
              <input type="hidden" name="remaining_media_count" value="5">
              <div class="browse_btn">

                <div class="vpb_browse_file" onclick="document.getElementById('vpb-data-file').click();"><button><span>Browse</span></button>
                <div id="wave">
                  <span class="dot"></span>
                  <span class="dot"></span>
                  <span class="dot"></span>
                </div>
                </div> <!-- Browse File Button -->
                <div  onClick="vpb_upload_previewed_files('{{ url('/user/save/model/image') }}');" class="vpb_pagination_button_clicked"><button disabled><span>Upload</span></button></div> <!-- Upload File Button -->
                

              </div>
              <div  class="preview_imges_blk preview_imges_scroll" id="vpb-display-preview"></div>              
              
            </div>    
          </div>
        </div>
      </div>
     </div>
    </div>
   </div>
  </section>
  @endif
<div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
<div class="container gallery_blk_container">



<div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">

  <!-- show upload only for model & agent -->
@if($user_type == 'admin' || empty($logged_user))
<div class="empty_msg_details">
  <div class="empty_msg_blk">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>Access Denied!</h1>
        <h2>
          Only Model & Agent Users can able to upload photo to our site.Site Admin Can't able to upload images
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="images/file_image1.png" alt="file_image" />
        </div>
      </div>
    </div>
    <!-- <p>You can search on the bar below or return to home page</p> -->
    <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/') }}"><span>Home</span><img src="images/preview_image1.png"></a>
      </div>
      <div class="msg_next_page">

        <a href="{{ url('dashboard') }}"><span>Dashboard</span><img src="images/next_image1.png"></a>
      </div>
    </div>
  </div>
</div>
@endif



@if(!empty($gallery))
@foreach($gallery as $photo)
<div class="magelur_gallery_div col-md-4 col-sm-4 col-xs-12">
  <div class="magelur_image_blk">
    
    <img src="{{ asset('uploads/model_gallery/'.$photo->ID.'_medium.'.$photo->Ext) }}" class="gallery_images1">
    
    <div class="img_hover_blk">
      <div class="hvr_icon_div">
       <a class="fancybox" rel="gallery1" href="{{ asset('uploads/model_gallery/'.$photo->ID.'_large.'.$photo->Ext) }}" title=""> <span class="circle_white"><img src="images/expanded_img.png"></span>
       </a>
        <span class="circle_white"><img src="images/crop_img.png"></span>
      </div>
    </div>
  </div>
</div>
@endforeach
@else
<div class="empty_msg_details">
  <div class="empty_msg_blk">
    <div class="heading_blk">
      <div class="heading_details">
        <h1>No More Images!</h1>
        <h2>
          You Have Not Uploaded Any Images Under Your Gallery.Click on the above image upload button to add images on gallery
        </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <img src="images/file_image1.png" alt="file_image" />
        </div>
      </div>
    </div>
    <!-- <p>You can search on the bar below or return to home page</p> -->
    <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/') }}"><span>Home</span><img src="images/preview_image1.png"></a>
      </div>
      <div class="msg_next_page">

        <a href="{{ url('dashboard') }}"><span>Dashboard</span><img src="images/next_image1.png"></a>
      </div>
    </div>
  </div>
</div>
@endif


<div class="text-center pagination_content"> {!! $pagination->render() !!}</div>




</div>
</div>
</div>

</div>

<style type="text/css">
  .map{display: none;}
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('.preview_imges_scroll').slimScroll({
        height: '310px',
        width: '100%',
        allowPageScroll :true
        
    });     

    $(".fancybox").fancybox({});

});
</script>
