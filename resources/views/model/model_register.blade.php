 
<!-- <link href="{{ asset('magelur/css/datepicker.min.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/bootstrap-datepicker.js')}}" type="text/javascript" charset="utf-8"></script> -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>

<div class="model_registration magelur_about">
<div class="section_one">
 <div class="left_half">
  <img src="{{ asset('images/register_bg.png') }}">
 </div>
  <div class="container">
   <div class="cnt_txt wow Bounce">
    <h2>Talent Models Registration</h2>
    <!-- <h3><span></span>Promote Yourself</h3> -->
    <div class="two_laid"><a href="{{asset('/home')}}">HOME</a><p>MODELS REGISTRATION</p></div>
 <!--    <ul class="link_list" style="text-align: center;">
                <li class="active"><a href="http://demo.bsetec.com/demo/magelur/public">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">MODELS REGISTRATION</a></li>
              </ul> -->
   </div>
  </div>
</div><!--end section_one-->

<div class="register_wrap">
  <div class="container">
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="left-content">
        <h3><span>Become</span><br/> A Model today</h3>
        <div class="left-registerbg">
          <h4>Your road to stardom starts here... <span>Sign up now!</span></h4>
          <span class="asterix">
          @if(Session::has('message'))
          {!! Session::get('message') !!}
          @endif

          </span> 
        <form method="POST" action="{{ url('/model/signup') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">
     <div id='content-area'></div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="decoded_id" value="{{ app('request')->input('tellfriend') }}">

           <!--  <div class="about-form">
            <label class="label-head">your name</label>
            <div class="clearfix"></div>
            <div class="form-group first date">
            <input type="text" name='firstname' id='firstname' class="form-control" placeholder="Enter your first name" >
            </div> --><!--end form-group-->

            <!-- <div class="form-group">
            <input type="text" name='lastname' id='lastname' class="form-control" placeholder="Enter your last name" >
            </div> --><!--end form-group-->
            <!-- </div> -->
            <div class="about-form">
            <label class="label-head">your name</label>
            <div class="clearfix"></div>
             <div class="form-group first date" >
  
    <input type="text" name="firstname" id='firstname' class="form-control" placeholder="Enter your firstname">
    
           
            </div><!--end form-group-->
  
    
            <div class="form-group">
            <input type="text" name="lastname" id='lastname' class="form-control" placeholder="Enter your lastname">
            </div>
            </div><!--end about-form-->

            <div class="common-form">
            <!-- <label class="label-head"></label> -->[optional]
             <div class="form-group">
            <input type="text" name="username" id='username' class="form-control" placeholder="Enter your Username">
            </div><!--end form-group-->
            </div><!--end common-form-->

            <div class="about-form">
            <label class="label-head">about you</label>
            <div class="clearfix"></div>
             <div class="form-group first date" >
  
    <input type="text" name="DOB" id="datepicker" class="form-control" autocomplete='off' placeholder="Date of Birth"  >
    <span class="input-group-addon" style='display:none'></span>
    
           
            </div><!--end form-group-->
  
    
            <div class="form-group">
            <div class="select-style">
            <select name='gender' id='gender' class="form-control">
              <option value=''>Select Gender</option>
              <option value='male'>Male</option>
              <option value='female'>Female</option>
<!--               <option value='other'>Other</option>
 -->            </select>
            </div><!--end select-style-->
            </div>
            </div><!--end about-form-->

            <div class="common-form">
            <label class="label-head">Phone Number</label>[optional]
              <div class="form-group">
             <input type="text" name="contact_number" id='contact_number' class="form-control" placeholder="Enter your Contact number">
            </div>
            </div><!--end common-form-->

             <div class="common-form">
            <!-- <label class="label-head"></label> -->
              <div class="form-group">
             <input type="text" name="city" id='city' class="form-control" placeholder="Enter your City here">
            </div>
            </div><!--end common-form-->
           <!--  <div class="common-form">
              <div class="form-group">
             <input type="text" name="state" id='state' class="form-control" placeholder="Enter your State here">
            </div>
            </div> -->
            <!--end common-form-->

            <div class="common-form">
            <label class="label-head">select country</label>
              <div class="form-group">
            <div class="select-style">

             {!! Form::select('country',array('' => 'Select Country') +\SiteHelpers::getTableValues('country'),'',array('class' => 'form-control','id' => 'country')) !!}

            </div><!--end select-style-->
            </div>
            </div><!--end common-form-->

            <div class="common-form">
            <label class="label-head">email address</label>
             <div class="form-group">
            <input type="text" name="email" id='email' class="form-control" placeholder="Enter your email address here">
            </div><!--end form-group-->
            </div><!--end common-form-->

            <div class="common-form">
            <label class="label-head">biography</label>
             <div class="form-group">
            <textarea class="form-control" name='biography' placeholder="Write your biography here"></textarea>
        
            </div><!--end form-group-->
            </div><!--end common-form-->

            <p class="file-typetxt">What do you look like?: <span>[formats: jpg/jpeg,png] [Min size: 600*600px]</span></p>
            <div class="form-group">
              <div id="clickHere">
                 <input type="file"  name='profile_photo' id="profile_photo"  />
               </div>
            </div>

             <br />
          <br />
            <p class="terms-text">By registering with us, you agree that you are 14 years of age or older. You also agree to our <a href="{{ URL::to('privacy-policy') }}"><span>privacy policy</span></a> and <a href="{{ URL::to('terms') }}"><span>terms of service</span></a>. all information (including images) are true about you, and all images attached were taken at the age of 14 years old or older.</p>

              <label class="agree">I agree
                <input type='radio' name='terms_agree' value ='1'>  
                <span class="checkmark"></span>
              </label>

            <div class="submit-btn login-btn">
                <a><button type="submit" class='bef_subm'><span>submit</span></button></a>
                <a><button type="" class='aft_subm' hidden disabled><span>Submitting..</span></button>
                </a>
            </div><!--end submit-btn-->
          </form>
        </div><!--end left-registerbg-->
      </div><!--end left-content-->
    </div><!--end col-->
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="right-content">
          <h3>Let us discover you!</h3>
          <p>Not yet a member? Here is what you are missing out on!</p>

          <ul class="list-txt">
              <li><span></span>Promoting yourself on our vibrant and growing network</li>
              <li><span></span>Being the next top Model</li>
              <li><span></span>Share your talents for all to see</li>
              <li><span></span>Share your photos and videos</li>
              <li><span></span>Share your social pages to be discovered in more ways</li>
              <li><span></span>Commenting and liking other talents and productions</li>
          </ul>

          <p class="acc-txt">Already have an account?</p>
        {{--*/ $url=app('request')->input('tellfriend') /*--}}
          <div class="login-btn">
            @if( app('request')->input('tellfriend'))
            <a href="{{ url('/login?tellfriend=') }}{{$url}}"><button><span>Login Now!</span></button></a>
            @else
            <a href="{{ url('/login') }}"><button><span>Login Now!</span></button></a>
            @endif
          </div>

          <p class="add-optiontxt">Check out these additional options...</p>

          <div class="add-details">
            <div class="left-text">
                <p>Already Have an Account?</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
                @if( app('request')->input('tellfriend'))
                <a href="{{ url('/login?tellfriend=') }}{{$url}}">Login in now</a>
                @else
                <a href="{{ url('/login') }}">Login in now</a>
                @endif
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>About Us</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/aboutus') }}">learn more</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view agents in your industry</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/agencies') }}">view agents</a>
            </div>
          </div><!--end add-details-->

          <div class="add-details">
            <div class="left-text">
                <p>view events & castings</p>
                <span></span>
              </div>

              <div class="right-link">
               <span></span>
               <span class="second-skew"></span>
              <a href="{{ url('/events/list') }}">view events</a>
            </div>
          </div><!--end add-details-->

      </div><!--end right-content-->
    </div><!--end col-->
  </div><!--end container-->
</div><!--end register_wrap-->

<div class="safety-wrap">
  <div class="container">
      <h3>Safety, our top priority</h3>
      <p>Pure & Innocent - Talent Models Network mandates the safety and well-being of all users of this application as our top priority. We pride ourselves in our professionalism and transparency, and believe it is important to advise everyone about unsavoury people on the periphery of this industry, who prey on young people's ambitions. Please be aware that we do all communications through our portal application and we protect your information from all prying eyes.</p>
  </div>
</div><!--end safety-wrap-->

<div class="section_seven">
 <h1>Most Recent Registered Models</h1>
 <p class="sub-text">The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p>
 <div class="owl-carousel modelcreate_model_slider" id="">
        
        @foreach($models as $model)
        {{--*/ $username_str =str_replace(' ','',$model->username) /*--}}

        {{--*/ $url_id =urlencode( base64_encode($username_str.'/'.$model->id));/*--}}
        <div class="item">
          <ul class="instagramer item">
            @if($model->id % 2==0)
  <li class="wow SlideInLeft">
    @if($model->profile_photo !='')
    <img src="{!! SiteHelpers::user_model_profiles_home($model->profile_photo) !!}" >
    @else
     <img src="{{ \SiteHelpers::user_model_profile_sizes($model->id,'_large','png') }}" >
    @endif
    <div class="followme"><div class="submit_follow"><div class="main_follow">
      <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
        <span></span> <p>Book Model</p></a>
    </div></div></div>
  </li>
@else
  <li class="wow SlideInRight">
  @if($model->profile_photo !='')
  <img src="{!! SiteHelpers::user_model_profiles_home($model->profile_photo) !!}" >
  @else
  <img src="{{ \SiteHelpers::user_model_profile_sizes($model->id,'_large','png') }}" >
  @endif
  <div class="followme"><div class="submit_follow"><div class="main_follow">
    <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}"><span></span> <p>Book Model</p></a>
  </div></div></div>
  </li>
  @endif
          </ul></div>
          @endforeach
        </div>
<!--  <ul class="instagramer">
  <li class="wow SlideInLeft">
  <img src="{{ asset('images/model_img.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img2.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img3.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInRight"><img src="{{ asset('images/model-img4.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
  <li class="wow SlideInLeft"><img src="{{ asset('images/model-img5.jpg') }}">
  <div class="followme"><div class="submit_follow"><div class="main_follow"><a href=""><span><i class="fa fa-instagram"></i></span> <p>Follow me on Instagram</p></a></div></div></div>
  </li>
 </ul> -->
</div>



<script type="text/javascript">
$(document).ready(function(){   
      var owl = $('.modelcreate_model_slider').owlCarousel({


    margin:0,
    nav:false,
    autoplay:true,
    responsive:true,
    loop:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:4
        },
        1000:{
            items:5
        }
    }
});
    });
 // $('#selectdate').datepicker({
 //        format:'yyyy-mm-dd',
 //        autoclose: true,
             
 //      });
// $( "#datepicker" ).datepicker({
//       showOtherMonths:  true,
//             selectOtherMonths:  true,
//             changeMonth:      true,
//             changeYear:         true,
//             dateFormat:         "yy-mm-dd",
//             showAnim:         "slideDown",
//             defaultDate:        "-18y",
//             yearRange:        "-68: -18",
//             //minDate:          '-100y', 
//             //maxDate:          "-17y",

//     });
 var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 14);
$('#datepicker').datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
$("#register").validate({
     ignore: "not:hidden",
      rules: {
        firstname:{
          required:true,
        },
        lastname:{
          required:true,
        },
        // username:{
        //   required:true,
        // },
        contact_number:{
         // required:true,
         number:true,
         minlength:10,
        },
        city:{
          required:true,
        },
        // state:{
        //   required:true,
        // },
         captcha:{
         required:true,
         
        },
         DOB:{
          required:true,
          
        },
        gender:{
          required:true,
          
        },
        country:{
          required:true,
          
        },
         biography:{
          required:true,
          
        },
         profile_photo:{
          required:true,
          accept:"jpg,png,jpeg"
          
        },
        terms_agree:{
          required:true,
          
        },
         email:{
          required:true,
          email:true,
          remote:{
          url:"{!! url('model/checkuniqueemail')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#email').val();
            },
            
          }
        }
        },
        //  password:{
        //  required:true,
        //  rangelength:[6,12]
        // },
   

      },
      messages: {
        firstname:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required", 
        },
         lastname:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required", 
        },
          username:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required", 
        },
        contact_number:{
          number:"<span><i class='fa fa-warning'></i></span> Please enter a valid number",       
          minlength:"<span><i class='fa fa-warning'></i></span> Please enter at least 10 characters",       
        },
        DOB:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
         
        },
        gender:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
         
        },
        country:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
        },
        city:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
        },
        biography:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
         
        },
         profile_photo:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required", 
          accept: "<span><i class='fa fa-warning'></i></span> Please provide valid extension",      
         
        },
         terms_agree:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
         
        },
        email:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",
          email:"<span><i class='fa fa-warning'></i></span> Invalid Email address",
        },
        // password:{
        //  required:"<span><i class='fa fa-warning'></i></span> This field is required",
        //  rangelength:"<span><i class='fa fa-warning'></i></span> Password must between 6 to 12 characters"
        // },
      },
        submitHandler:function(form){
         // toastr.success("Registered Successfully");
        // setTimeout(function(){
          $('.bef_subm').hide();
          $('.aft_subm').show();
        form.submit();
      // },2000);
         /* $.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),
 beforeSend: function() {
$('.bef_subm').hide();
$('.aft_subm').show();
    },
          success: function(data) {
            // reset form values
            $('input[name="username"]').val('');
            // $('input[name="lastname"]').val('');
            $('input[name="email"]').val('');
            $('input[name="DOB"]').val('');
            $('textarea[name="biography"]').val('');
            $('input[name="profile_photo"]').val('');
            $('input[name="terms_agree"]').attr('checked', false);
            $("#country").val('');
            $("#gender").val('');
            $('.bef_subm').show();
            $('.aft_subm').hide();
            $('#content-area').prepend('<i class="fa fa-check" style="margin-right: 10px;"></i> Thanks for registering! . You will get mail soon after we active your account ');
            $('html, body').animate({
                    scrollTop: $("div.left-registerbg").offset().top
                  }, 1000);
            // toastr.success("Registered Successfully");
          },
          });*/
        }
    });
var _URL = window.URL || window.webkitURL;

$('#profile_photo').on('change', function () { 
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            // alert(this.width + " " + this.height);
            if(this.width < 600 || this.height < 600 )
            {
                swal('Please use Minimum  Image dimension 600 * 600px ')
               
                $('input[name=profile_photo]').val(null);
               
            }
        };
       
        img.src = _URL.createObjectURL(file);
    }

  

var reader = new FileReader();
    reader.onload = function (e) {
   
    }
    reader.readAsDataURL(this.files[0]);
});

 </script>
