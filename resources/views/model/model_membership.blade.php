	@include('model_dash_header')


<div class="subscription">
 <div class="container">
  <div class="subpanter clearfix">
    @foreach($models as $model)
  
     @if($model->plan_id !='')
       @foreach($membershipplans as $plan)
   <div class="subscript_left">	
    @if($model->pause_subscription == 'paused')

 
    @else
  	<a class="subcript" href="javascript:void(0)">Subscription</a>

    	<p><span class="sub_stateone">Subscription Plan</span>    
      <span class="sub_static">:</span> {{$plan->plan_name}} Membership</p> 
	    @if($purchase_period !='')
      <p><span class="sub_stateone">Plan Duration</span> 
      <span class="sub_static">:</span>  One {{$purchase_period}}</p> @endif
	    <p><span class="sub_stateone">Renewal date</span>      
      <span class="sub_static">:</span> @if($renewal_date !='') {{$renewal_date}} @else Till new subscription @endif</p>
	   @endif
      <div class="upmap">
       <!--  <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a> 
      <a href="javascript:void(0)" class="white_bg">Pause my subscription</a> -->
      @if(isset($membership->status) && $membership->status == 'cancelled' && $model->pause_subscription =='paused')
          <!-- <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a> -->
           <p> No Plans right now. Please Subscribe and make use of New Plans right now! </p>

      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
    
      </div>    
      @elseif(isset($membership->status) && $membership->status == 'active' && $model->pause_subscription =='not_paused')
          <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a>
          {!! Form::open(array('url'=>'membership/cancelmembership','id' =>'member-form')) !!}
              <button class="btn btn-color btn-color-warning m-b-sm confirmcancel" type='submit' >Pause my subscription</button>
          {!! Form::close() !!}
      @else
          <a href="{{ url('/membership-plan') }}">Upgrade My subscription</a>
      @endif

    </div>
     
     
   </div>
    @endforeach
     @elseif($model->pause_subscription == 'paused')
   <div class="subscript_left">  
              <p> No Plans right now. Please Subscribe and make use of New Plans right now! </p>

      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
      </div>
    
   </div>
   @else
   <div class="subscript_left">  
          <p> No Plans right now. Please Subscribe and make use of New Plans right now! </p>
      <div class="upmap"><a href="{{ url('/membership-plan') }}">Please Subscribe</a> 
      </div>
    
   </div>
   @endif
   @endforeach
   <div class="subscript_right">	
  	<img src="{{ asset('images/membership_brand.png') }}">
   </div>
  </div>	
 </div>	
</div>	

  @include('footer_map')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

// <script type="text/javascript">
//         $(document).ready(function() {
//         $('#vertical').lightSlider({
//           item:1,
//           vertical:true,
//           verticalHeight:250
//         });  
//       });
 


   $(document).on('click', '#member-form', function(e) {
    e.preventDefault();
    swal({
        title: "Are you sure?",
      text: "Once Subscription paused,you will not get benefits of basic plan again",
      buttons: true,
      dangerMode: true,
       
    }).then(function (result) {
      if(result==true){
        // alert('true')
        $('#member-form').submit();
      }else{
swal({ icon: "success", title:"Your plan is safe!"});
      }
    });

});
 </script>


// <script>
// function openNav() {
//     document.getElementById("mySidenav").style.width = "250px";
//     document.getElementById("main").style.marginLeft = "250px";
// }

// function closeNav() {
//     document.getElementById("mySidenav").style.width = "";
//     document.getElementById("main").style.marginLeft= "";
// }
// </script>
@if(Session::has('messagetext'))
<script>
toastr.success("{!! Session::get('messagetext') !!}");
</script>

@endif