<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/tagsly.js')}}"></script>
<link href="{{ asset('magelur/css/tagsly.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
		<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js')}}"></script>
		<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">

@include('model_dash_header')

<style type="text/css">
.applynow_form .selectdiv select,input{
	color:#777777;
}
</style>

<div class="dashboard-ders">
 <div class="container">
 <div class="subpanter clearfix">	
  <div class="left_part_ders">
  	 	<!--  -->
  	 	<section class="model_profile_page">
			<div class="container">
				<div class="row">
					<div class="applynow_form">
						<form method="POST" action="{{ url('/model-profile/save') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">
							{!! Form::hidden('id', $row['id']) !!}
							<div class="col-md-12 col-sm-12">
								<div class="select_gender wow bounce">
								<p>PLEASE SELECT YOUR GENDER</p>
								<ul class="gender_list">
									<li>    
							          <label for = "male">MALE</label>
							      	<!-- <input type="radio" name="radSize" id="male" value="small" checked="checked" /> -->
									<input type='radio' name='gender' value ='male' required @if($row['gender'] == 'male') checked="checked" @endif >
									</li>

									<li>
										<label for="female">FEMALE</label>
										<input type='radio' name='gender' value ='female' required @if($row['gender'] == 'female') checked="checked" @endif >
						    		</li>
						    			
						    		
						    	</ul>
								</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-12 col-sm-12"> 
								    <div class="group">      
								      {!! Form::text('username', $row['username'],array( 'placeholder'=>'User Name','required'=>"required")) !!} 
									  <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>User Name</label>
								    </div>
								</div> 
								<div class="col-md-6 col-sm-12"> 
								   
								</div> 
							</div>
							<div class="col-md-12 col-m-12">
								
								<div class="col-md-6 col-sm-12"> 
								    <div class="group"> 
								      {!! Form::text('firstname', $row['firstname'],array( 'placeholder'=>'First Name','required'=>"required")) !!}      
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>First Name</label>
								    </div>
								</div>

								<div class="col-md-6 col-sm-12"> 
									<div class="group"> 
									  {!! Form::text('lastname', $row['lastname'],array( 'placeholder'=>'Last Name','required'=>"required")) !!}           
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Last Name</label>
							    </div>
									
								</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								
								 <div class="group">  
								 	  {!! Form::text('profession', $row['profession'],array( 'placeholder'=>'Profession','required'=>"required")) !!}               
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Profession</label>
								    </div>
								</div> 
								<div class="col-md-6 col-sm-12">
								 <div class="group">   
								     <div class="selectdiv ">
										 {!! Form::select('education',array('' => 'Education') +\SiteHelpers::getTableValues('education') ,$row['education'],array('class' => 'select2','id' => 'education')) !!}   

									</div>
								    </div>
								</div> 
							</div>
					
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								     <p style='color:#777777;'>Language</p>
								<div class="group"> 
								  {!! Form::text('language', $row['language'],array( 'placeholder'=>'Language','required'=>"required",'id'=>"language")) !!}                    
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Language</label>
							    </div>
								 
								</div> 
								<div class="col-md-6 col-sm-12">
								 <p style='color:#777777;'>Hobbies</p> 
								   <div class="group">
								   	  {!! Form::text('hobbies', $row['hobbies'],array( 'placeholder'=>'Hobbies','required'=>"required",'id'=>"hobbies")) !!}                          
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Hobbies</label>
			
								    </div>
								</div> 
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								<div class="group">
								   {!! Form::text('email', $row['email'],array( 'placeholder'=>'Email Address','required'=>"required",'id'=>"email")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Email Address</label>
							    </div>

								</div>
								<div class="col-md-6 col-sm-12">
								<div class="group"> 
								  {!! Form::text('contact_number', $row['contact_number'],array( 'placeholder'=>'Contact Number','required'=>"required",'id'=>"contact_number")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Contact Number</label>
							    </div> 
							</div>
								
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">    
								  {!! Form::text('city', $row['city'],array( 'placeholder'=>'City','required'=>"required",'id'=>"city")) !!}                            
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>City</label>
							    </div>
							</div>

							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('insta_link', $row['insta_link'],array( 'placeholder'=>'Instagram Link','required'=>"required",'id'=>"insta_link")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Instagram Link</label>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('state', $row['state'],array( 'placeholder'=>'State','required'=>"required",'id'=>"state")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>State</label>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('twitter_link', $row['twitter_link'],array( 'placeholder'=>'Twitter Link','required'=>"required",'id'=>"twitter_link")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Twitter Link</label>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">
								  {!! Form::text('zipcode', $row['zipcode'],array( 'placeholder'=>'Zipcode','required'=>"required",'id'=>"zipcode")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Zipcode</label>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
								  {!! Form::text('fb_link', $row['fb_link'],array( 'placeholder'=>'Facebook Link','required'=>"required",'id'=>"fb_link")) !!}                          
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Facebook Link</label>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								
							</div>
							
							<div class="col-md-6 col-sm-12">
								
							</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
 									<div class="selectdiv ">
										{!! Form::select('hair_color',array('' => 'Hair Color') +\SiteHelpers::getTableValues('hair_color') ,$row['hair_color'],array('class' => 'select2','id' => 'hair_color')) !!}
									</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <!-- <label>Hair Color</label> -->
								    </div>

							</div>

							<div class="col-md-6 col-sm-12"> 
									<div class="group">      
								     <div class="selectdiv ">
								     	{!! Form::select('eye_color',array('' => 'Eye Color') +\SiteHelpers::getTableValues('eye_color') ,$row['eye_color'],array('class' => 'select2','id' => 'eye_color')) !!}

									</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								    </div>
								</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('height',array('' => 'Height') +\SiteHelpers::getTableValues('heights') ,$row['height'],array('class' => 'select2','id' => 'height')) !!}
									 
									</div>
							      <span class="highlight"></span>
							      <span class="bar"></span>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('body_type',array('' => 'Body Type') +\SiteHelpers::getTableValues('body_type') ,$row['body_type'],array('class' => 'select2','id' => 'body_type')) !!}
										
									</div>
							    </div>
							</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12">
									<div class="group">
									{!! Form::text('waist', $row['waist'],array( 'placeholder'=>'Waist','required'=>"required",'id'=>"waist")) !!}                                
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Waist in cm</label>
							  		</div>

									 <div class="group">      
								      <!-- <input type="text" placeholder="Suit" required> -->
								      <div class="selectdiv ">
								      	{!! Form::select('ethnicity',array('' => 'Ethnicity') +\SiteHelpers::getTableValues('ethnicity') ,$row['ethnicity'],array('class' => 'select2','id' => 'ethnicity')) !!}
										
										</div>
								      <span class="highlight"></span>
								      <span class="bar"></span>
								    </div>
								</div>

								<div class="col-md-6 col-sm-12">
     
							      <p class="dob_title">DOB</p>
  
									<div class="col-md-4 col-sm-4 dob_day">
										<div class="group">    
										 <div class="selectdiv">

									      <select name="day" required>

									        <option value='' selected> Day</option>
									        <?php
											$day=substr($row['DOB'], 8);
											for($x = 1; $x <= 31; $x++) {
 											$value = str_pad($x,2,"0",STR_PAD_LEFT);
 											?>
											<option value='{{ $value }}' @if($day == $value)  selected='selected' @endif>{{ $value }}</option>
											<?php
											}
											?>
									      </select>
										</div>
										</div>
									</div>

										<div class="col-md-4 col-sm-4 dob_month">
										<div class="group">    
										 <div class="selectdiv ">

									      <select name='month'>
									          <option value='' selected>Month</option>
												<?php
												$month=substr($row['DOB'], 5, -3);
												$months = array( '01' => 'January' ,  '02' => 'February' ,  '03' => 'March' ,  '04' => 'April',  '05' => 'May',  '06' => 'June',  '07' => 'July',  '08' => 'August',  
												'09' => 'September',  '10' => 'October',  '11' => 'November',  '12' => 'December' );
												foreach ($months as $key => $value)
												{
												?>
												<option value='{{$key}}' @if($month == $key)  selected='selected' @endif>{{ $value }}</option>
												<?php
												}
												?>
									      </select>
											</div>
										</div>
									</div>

										<div class="col-md-4 col-sm-4 dob_year">
										<div class="group">    
										 <div class="selectdiv ">
											<select name='year'>
											<option value='' selected> Year</option>
											<?php
											$year=substr($row['DOB'],0,-4);
											$firstYear = (int)date('Y') - 68;
											$lastYear = $firstYear + 50;
											for($i=$firstYear;$i<=$lastYear;$i++)
											{
											?>
											<option value='{{ $i }}' @if($year == $i)  selected='selected' @endif>{{ $i }}</option>
											<?php
											}
											?>
											</select>
										</div>
										</div>
									</div>
								
								<span class="highlight"></span>
							    <span class="bar"></span>
							</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('bust_size',array('' => 'Chest') +\SiteHelpers::getTableValues('bust_size') ,$row['bust_size'],array('class' => 'select2','id' => 'bust_size')) !!}
									
									</div>
							      <span class="highlight"></span>
							      <span class="bar"></span>
							    </div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								
							    <div class="group">
							      {!! Form::text('shoe_size', $row['shoe_size'],array( 'placeholder'=>'Shoe Size','required'=>"required",'id'=>"shoe_size")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Shoe Size</label>
							    </div>
							</div>
							</div>

						

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('marital_status',array('' => 'Marital status') +\SiteHelpers::getTableValues('marital_status') ,$row['marital_status'],array('class' => 'select2','id' => 'marital_status')) !!}
									
									</div>
							    </div>
							      
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('smoking',array('' => 'Smoking') +\SiteHelpers::getTableValues('smoking') ,$row['smoking'],array('class' => 'select2','id' => 'smoking')) !!}
									
									</div>
							    </div>
							    
							</div>
							</div>

							

							
									<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								
							   <p style='color:#777777;'>Biography</p>
							    <div class="group">
							      {!! Form::text('biography', $row['biography'],array( 'placeholder'=>'Biography','required'=>"required",'id'=>"biography")) !!}                                
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Biography</label>
							    </div>
							</div>

							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('model_categories',array('' => 'Model Categories') +\SiteHelpers::getTableValues('model_categories') ,$row['model_categories'],array('class' => 'select2','id' => 'model_categories')) !!}
									</div>
							    </div>
							     <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('drinking',array('' => 'Drinking') +\SiteHelpers::getTableValues('drinking') ,$row['drinking'],array('class' => 'select2','id' => 'drinking')) !!}
									
									</div>
							    </div>
							    <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('drugs',array('' => 'Drugs') +\SiteHelpers::getTableValues('drugs') ,$row['drugs'],array('class' => 'select2','id' => 'drugs')) !!}
									
									</div>
							    </div>
							    <div class="group">      
									<div class="selectdiv ">
										{!! Form::select('children',array('' => 'Children') +\SiteHelpers::getTableValues('children') ,$row['children'],array('class' => 'select2','id' => 'children')) !!}
									
									</div>
							    </div>
							</div>
							</div>

							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
							      <div class="selectdiv ">
							      	{!! Form::select('model_industries',array('' => 'Model Industries') +\SiteHelpers::getTableValues('model_industries') ,$row['model_industries'],array('class' => 'select2','id' => 'model_industries')) !!}
									
									</div>
							    </div>
							      
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">      
									<div class="selectdiv ">
										{!! Form::select('publishes',array('' => 'Publishes') +\SiteHelpers::getTableValues('publishes') ,$row['publishes'],array('class' => 'select2','id' => 'publishes')) !!}
							
									</div>
							    </div>
							    
							</div>
							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								<div class="group">      
								{!! Form::text('hip', $row['hip'],array( 'placeholder'=>'Hip','required'=>"required",'id'=>"hip")) !!}                                
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Hip in cm</label>
								</div>
							      
							</div>
							
							<div class="col-md-6 col-sm-12">
								<div class="group">
								<div class="selectdiv ">
										{!! Form::select('years',array('' => 'Year of experience') +[0,1, 2, 3,'More than 3 years'] ,$row['year'],array('class' => 'select2','id' => 'years')) !!}
							
									</div>      

							    </div>
							    
							</div>
							</div>
							<div class="col-md-12 col-m-12">
						
							<div class="col-md-6 col-sm-12">
									<p style="color:#777777;">Body Piercing</p>
								
								<ul class="gender_list">
									

									<li>
										<label for="yes">Yes</label>
										{!! Form::radio('body_piercing',1,($row['body_piercing']==1) ? true:false,array()) !!}  
						    		</li>
						    			
						    		<li>
						    			<label for = "no">No</label>
						    			{!! Form::radio('body_piercing',0,($row['body_piercing']==0) ? true:false,array()) !!}  
						    		</li>
						    	</ul>


							
							</div>
							<div class="col-md-6 col-sm-12">
									<p style="color:#777777;">Tattoos</p>
								
								<ul class="gender_list">
									

									<li>
										<label for="yes">Yes</label>
										{!! Form::radio('tattoos',1,($row['tattoos']==1) ? true:false,array()) !!}
						    		</li>
						    			
						    		<li>
						    			<label for = "no">No</label>
						    			{!! Form::radio('tattoos',0,($row['tattoos']==0) ? true:false,array()) !!}
						    		</li>
						    	</ul>


							
							</div>
								<div class="col-md-12 col-sm-12">
								<p style="color:#777777;">Model Experience</p>
								
								<ul class="gender_list">
									<li>    
							          <label for = "New Face">New Face</label>
										{!! Form::radio('model_experience',1,($row['model_experience']==1) ? true:false,array()) !!}
									</li>

									<li>
										<label for="Aspiring Model">Aspiring Model</label>
										{!! Form::radio('model_experience',2,($row['model_experience']==2) ? true:false,array()) !!}
						    		</li>
						    			
						    		<li>
						    			<label for = "Experienced Model">Experienced Model</label>
						    			{!! Form::radio('model_experience',3,($row['model_experience']==3) ? true:false,array()) !!}
						    		</li>
						    		<li>
						    			<label for = "Professional Model">Professional Model</label>
						    			{!! Form::radio('model_experience',4,($row['model_experience']==4) ? true:false,array()) !!}
						    		</li>
						    	</ul>
							
							    
							      
							</div>
							
							</div>


							<div class="col-md-12 col-sm-12">
								<div class="apply_almost wow SlideInLeft">
									

									<div class="load_button">
										<button type="submit"><span>Submit</span></button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!--  -->
</div>
</div>
</div>
</div>

		

		

	</div>

					<!-- <select id="" class="select2-multiple" multiple>
						<option></option>
						<option value="A">A</option>
						<option value="B">B</option>
						<option value="C">C</option>
					</select> -->
				
    <script>

		$(function(){
	

		tinyMCE.init({
		theme : "advanced",
		mode: "exact",
		elements : "biography",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		+ "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		+ "bullist,numlist,outdent,indent",
		theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		+"undo,redo,cleanup,code,separator,sub,sup,charmap",
		theme_advanced_buttons3 : "",
		height:"200px",
		width:"505px"
		});
		$('#hobbies').tagsly({});
		$('#language').tagsly({});


			$( " .select2-multiple" ).select2( {
			placeholder : ' Model Categories'
			} );
	
	})

	
	$("#register").validate({
     ignore: "not:hidden",
      rules: {
        contact_number: { required:true, number:true,minlength:10,},
		username: { required:true },
		firstname: { required:true },
		lastname: { required:true },
		insta_link: { required:true },
		twitter_link: { required:true },
		fb_link: { required:true },
		profession: {required:true },
		body_type: { required:true },
		eye_color: { required:true },
		education: { required:true },
		language: { required:true },
		hobbies: { required:true },
		day: { required:true },
		month: { required:true },
		year: { required:true },
		email: { required:true,email:true },
		biography: { required:true },
		city: { required:true },
		zipcode: { required:true, number:true,},
		state: { required:true },
		country: { required:true },
		hair_color: { required:true },
		bust_size: { required:true },
		ethnicity: { required:true },
		shoe_size: { required:true,number:true,},
		hip: { required:true,number:true, },
		waist: { required:true,number:true,},
		height: { required:true,number:true, },
		marital_status: { required:true, },
		smoking: { required:true,},
		model_categories: { required:true,},
		drinking: { required:true,},
		drugs: { required:true,},
		children: { required:true,},
		model_industries: { required:true,},
		publishes: { required:true,},
		body_piercing: { required:true,},
		tattoos: { required:true,},
		model_experience:{ required:true,},
		years:{ required:true,},

        


      },
       messages: {
        contact_number:{ number:"Please enter valid phone number",
        minlength:"Phone number should be 10 number" },
        email:{ required:"Please enter email id", email:"Please enter valid email id" },
        city:{ required:"Please select city" },
        zipcode:{ required:"Please select zipcode", number:"Numbers only allowed",  },
        state:{ required:"Please select state" },
        country: { required:"Please select country" },
        DOB: { required:"Date of Birth required"  },
        shoe_size:{ number:"Size should be a number", },
        hip:{ number:"Hip size should be a number", },
        waist:{ number:"Waist size should be a number", },
        height:{ number:"Height should be a number", },

       },
        submitHandler:function(form){
        $('#reg-button').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"> </i> {{ Lang::get("core.loading") }}</div>');
        $('#reg-button').prop("disabled",true);
        $('#date').removeAttr("disabled");
        form.submit();
        }
    });

    </script>

