
<style type="text/css">
ul.profile_details_list li, .profile_details h3{
	color:#fff;
	font-size: 14px;
}

</style>
<div class="magelur_about model_abouts magelur_profile_blk">
		
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_about_banner">
					

					<div class="banner_title wow bounce">
						<h1>MODEL PROFILE</h1>
							
					</div>
				</div>
			</div>
		</section>
	<div class="dashboard-ders">
	

		<section class="">

			<div class="container">
				
				<div class="col-md-5 col-sm-12">
					<div class="model_video">
						<img src="{{ asset('uploads/model_profile/small/'.$row->profile_small) }}" class="img-responsive">
						
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="profile_details">
						<!-- <h3>PROFESSIONAL MODEL</h3> -->
						<!-- <h1>Mahdis Rubia</h1> -->
 					<div class=" clearfix">	

						<ul class="profile_details_list">
							<li>NAME</li>
							<li>GENDER</li>
							<li>AGE</li>
							<li>DOB</li>
							<li>ETHNICITY</li>
							<li>LANGUAGES</li>
							<li>EDUCATION</li>
							<li>PROFESSION</li>
							<li>LOCATION</li>
							<li>HOBBIES</li>
						</ul>
				

						<ul class="profile_details_list">
							<li>@if($row->username){{$row->username}}@else - @endif</li>
							<li>@if($row->gender){{$row->gender}}@else - @endif</li>
							<li>@if($row->age){{$row->age}}yrs @else - @endif</li>
							<li>@if($row->DOB){{$row->DOB}} @else - @endif</li>
							<li>@if( $row->ethnicity){{$row->ethnicity}}@else - @endif</li>
							<li>@if($row->language){{$row->language}}@else - @endif</li>
							<li>@if($row->education){{$row->education}}@else - @endif</li>
							<li>@if($row->profession){{$row->profession}}@else - @endif</li>
							<li>@if($row->city){{$row->city}},{{$row->country}}@else - @endif</li>
							<li>@if($row->hobbies){{$row->hobbies}} @else - @endif</li>
						</ul>

					
					</div>
					</div>
				</div>

	<div class="col-md-4 col-sm-12">
					<div class="profile_details">
						<!-- <h3>PROFESSIONAL MODEL</h3> -->
						<!-- <h1>Mahdis Rubia</h1> -->
 					<div class=" clearfix">	

						<ul class="profile_details_list">
							<li>HEIGHT</li>
							<li>BODY TYPE</li>
							<li>BUST SIZE</li>
							<li>WAIST</li>
							<li>HIP</li>
							<li>SHOE SIZE</li>
							<li>HAIR TYPE</li>
							<li>EYE COLOR</li>
							<br>
							<li>MARITAL STATUS</li>
							<li>CHILDREN</li>
							<li>DRINK</li>
							<li>SMOKE</li>
							<li>DO DRUGS</li>
							<li>BODY PIERCING</li>
							<li>TATTOOS</li>
							<li>PUBLISHES</li>
						</ul>
					
						<ul class="profile_details_list">
							<li>@if($row->height){{ $row->height }}@else - @endif</li>
							<li>@if($row->body_type){{$row->body_type}}@else - @endif</li>
							<li>@if($row->bust_size){{$row->bust_size}}@else - @endif</li>
							<li>@if($row->waist){{$row->waist}} inches @else - @endif</li>
							<li>@if($row->hip){{$row->hip}} inches @else - @endif</li>
							<li>@if($row->shoe_size){{$row->shoe_size}}@else - @endif</li>
							<li>@if($row->hair_color){{$row->hair_color}}@else - @endif</li>
							<li>@if($row->eye_color){{$row->eye_color}}@else - @endif</li>
							<br>
							<li>@if($row->marital_status){{$row->marital_status}}@else - @endif</li>
							<li>@if($row->children){{$row->children}}@else - @endif</li>
							<li>@if($row->drinking){{$row->drinking}}@else - @endif</li>
							<li>@if($row->smoking){{$row->smoking}}@else - @endif</li>
							<li>@if($row->drugs){{$row->drugs}}@else - @endif</li>
							<li>@if($row->body_piercing){{$row->body_piercing}}@else - @endif</li>
							<li>@if($row->tattoos){{$row->tattoos}}@else - @endif</li>
							<li>@if($row->publishes){{$row->publishes}}@else - @endif</li>
						</ul>

					</div>
					</div>
				</div>			
				<div class="col-md-4 col-sm-12">
					<div class="profile_details">
						<!-- <h3>PROFESSIONAL MODEL</h3> -->
						<!-- <h1>Mahdis Rubia</h1> -->
 					<div class="subpanter clearfix">	
						<h3>Experiences</h3>
						<h1><hr></h1>
						<ul class="profile_details_list">
							<li>Experience</li><br>
							<li>Years</li>
							

						</ul>
						
						<ul class="profile_details_list">
							<li>@if($row->model_experience){{$row->model_experience}} @else - @endif</li><br>
							<li>@if($row->year){{$row->year}}yrs @else - @endif</li>
							
						</ul>
					</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="profile_details">
						<!-- <h3>PROFESSIONAL MODEL</h3> -->
						<!-- <h1>Mahdis Rubia</h1> -->
 					<div class="subpanter clearfix">	
						<h3>Industries of Interest</h3>
						<h1><hr></h1> 
						
						
							<ul class="profile_details_list">
							<li>@if($row->model_industries) {{$row->model_industries}} @else - @endif</li>
							
							
						</ul>
					</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="profile_details">
						<!-- <h3>PROFESSIONAL MODEL</h3> -->
						<!-- <h1>Mahdis Rubia</h1> -->
 					<div class="subpanter clearfix">	
						<h3>Categories of Interest</h3>
						<h1><hr></h1> 
						
						<ul class="profile_details_list">
							<li>@if($row->model_categories)  {{$row->model_categories}} @else - @endif</li>
							
							
						</ul>

					</div>
					</div>
				</div>
				

			</div>
		</section>

	</div>

</div>

