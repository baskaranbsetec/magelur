<link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
<script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/star_rating.js')}}"></script>

<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<style type="text/css">
ul.profile_details_list li, .profile_details h3{
  color:#fff;
  font-size: 14px;

}
 .content{
 padding: 10px -26px 20px; 

}
.content p {
    color: #8f8f8f;
    font-family: "Lato-Regular";
    font-size: 17px;
}

.content h4 {
    color: #ffffff;
    font-family: "Lato-Regular";
    font-size: 16px;
    text-transform: uppercase;
}
.top  {
    margin-top: 21px;
   
}
</style>
<div class=" membt  ">
    <section>
      <div class="container-fluid magelur_header">
        <div id="upload-demo-i-cover">
          {!! SiteHelpers::user_avatars($row->id,'model',$row->profile_cover,'/uploads/agency_profile/cover/') !!}
        </div>
      </div>
    </section>
  <div class="ourmod_cnt wow bounce">
    <h1>@if($row->firstname !='') {{$row->firstname.' '.$row->lastname}}'s @endif - Profile</h1>
    <div class="two_laid"><a href="{{ asset('/home') }}">Home</a><p>Model Profile</p></div>
  </div>
</div>
<section class="modelprofile_details">
<div class="container">
  <div class="row">

  <div class="col-md-offset-1 col-md-11 col-sm-12">
  <div class="col-md-4 col-sm-12">
    <div class="model_profile_pic">
@if($row->profile_photo !='')
{!! SiteHelpers::user_profiles($row->id,'model',$row->profile_photo,'/uploads/model_profile/') !!}
@else
<img src="{!! \SiteHelpers::user_model_profile_sizes($row->id,'_medium','png') !!}" >
@endif
       
      <!-- <img src="{{ asset('uploads/model_profile/'.$row->profile_photo) }}" class="img-responsive"> -->
      <!-- {!! SiteHelpers::user_profiles($row->id,'model',$row->profile_photo,'/uploads/model_profile/') !!} -->

    </div>

    <div class="model_prof_btn">
      @if(SiteHelpers::checkLogged()) 
      @if( \Session::get('logged_id') == $row->id && $user_type == \Session::get('user_type')) @else
      <a href="javascript:void(0);" class="prof_btn" id="composeMessage">CONTACT THIS MODEL</a>@endif 
      @endif
      {{--*/ $modelId =urlencode( base64_encode($row->id));/*--}}
@if(SiteHelpers::checkLogged() )   
<a href="{{ url('model/album/'.$modelId) }}" class="prof_btn">GALLERY</a>
@else
<a href="#" onclick="toastr.info('Please login to view Gallery');" class="prof_btn">GALLERY</a>
@endif
@if(SiteHelpers::checkLogged() )
<a href="{{ url('model/video/'.$modelId) }}" class="prof_btn">VIDEO</a>
@else
<a href="#" class="prof_btn" onclick="toastr.info('Please login to view Videos');">VIDEO</a>
@endif
      <!-- <a href="{{ url('model/album/'.$modelId) }}" class="prof_btn">GALLERY</a>
      <a href="{{ url('model/video/'.$modelId) }}" class="prof_btn">VIDEO</a> -->
    </div>
  </div>

  <div class="col-md-7 col-sm-12 model_prof_det">
    <div class="profile_top_details">
      <h3>{{$row->firstname}} {{$row->lastname}}</h3>
      <div class="myprf_model_desc"><p>Modelling, Photo Shoot, Video Production, Websites / Online</p></div>
      <div class="model_social">
      <ul class="model_social_ico">
    
        <li class="fav_heart_icon">
         <!-- favourite -->
         @if(SiteHelpers::checkLogged() )
         @if( \Session::get('logged_id') == $row->id && $user_type == \Session::get('user_type'))
         @else    
         <div class="favourites">
          <a onclick="addToFavourite()" title="Add to favourite"><i class="fa fa-heart"></i></a>
        </div>
        @endif
        @endif  
      </li>
               @if(SiteHelpers::checkLogged() )

        {{--*/ $username_str =str_replace(' ','',$row->username) /*--}}

        {{--*/ $url_id = urlencode( base64_encode($username_str.'/'.$row->id));/*--}}
        <li class="">
              <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('model-profile/'.$username_str.'/'.$url_id)}}&t={{ url('model-profile/'.$username_str.'/'.$url_id)}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on Facebook">
                <span class="facebook"></span>
              </a>
        </li>
        <li class="">
        <a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://plus.google.com/share?url={{ url('model-profile/'.$username_str.'/'.$url_id)}}">
        <span class="google"></a>
        </li>
        <li class=""><a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" href="https://twitter.com/share?url={{ url('model-profile/'.$username_str.'/'.$url_id)}}"><span class="twitter"></span></a></li>
@endif

      </ul>
      </div>
 @if(SiteHelpers::checkLogged())    

<div class="post-action">
<!-- Rating -->
<select class='rating' id='rating_{{$row->id}}'  data-id='rating_{{$row->id}}'>
<option value="1" >1</option>
<option value="2" >2</option>
<option value="3" >3</option>
<option value="4" >4</option>
<option value="5" >5</option>
</select>
<input type='hidden' name='post_type' value='model'>
@if($avg_ratings>0)
<p>{{number_format($avg_ratings,1, '.', '')}}<p>
@endif
</div>
@endif
@if($row->id == \Session::get('logged_id'))
<script type="text/javascript">
setRating($('#rating_{{$row->id}}'),Math.round({{$avg_ratings}}));
</script>
@endif


                        <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Name <span class="my-ico">:</span></li>
                              <li><p>@if($row->firstname || $row->lastname){{$row->firstname}} {{$row->lastname}}@else Not Specified @endif</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Gender <span class="my-ico">:</span></li>
                              <li><p>@if($row->gender){{$row->gender}}@else Not Specified @endif</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Age <span class="my-ico">:</span></li>
                              <li><p>@if($row->age!=''){{$row->age}}@else Not Specified @endif yrs</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>DOB <span class="my-ico">:</span></li>
                              <li><p>@if($row->DOB){{$row->DOB}}@else Not Specified @endif</p></li>
                            </ul>

                             

                             <ul class="profile_list_content">
                              <li>Languages <span class="my-ico">:</span></li>
                              <li><p>@if($row->language){{$row->language}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Education <span class="my-ico">:</span></li>
                              <li><p>@if($row->education){{$row->education}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Profession <span class="my-ico">:</span></li>
                              <li><p>@if($row->profession){{$row->profession}}@else Not Specified @endif</p></li>
                            </ul>
                            <ul class="profile_list_content">
                              <li>Location <span class="my-ico">:</span></li>
                              <li><p>@if($row->city){{$row->city}},{{$row->country}}@else Not Specified @endif</p></li>
                            </ul>
                        </div>


                          <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Ethnicity <span class="my-ico">:</span></li>
                              <li><p>@if($row->ethnicity){{$row->ethnicity}}@else Not Specified @endif</p></li>
                            </ul>
                            <ul class="profile_list_content">
                              <li>Height <span class="my-ico">:</span></li>
                              <li><p>@if($row->height){{$row->height}}@else Not Specified @endif</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Body Type <span class="my-ico">:</span></li>
                              <li><p>@if($row->body_type){{$row->body_type}}@else Not Specified @endif</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Bust Size <span class="my-ico">:</span></li>
                              <li><p>@if($row->bust_size){{$row->bust_size}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Waist <span class="my-ico">:</span></li>
                              <li><p>@if($row->waist){{$row->waist}}inches @else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Hip <span class="my-ico">:</span></li>
                              <li><p>@if($row->hip){{$row->hip}}inches @else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Shoe Size <span class="my-ico">:</span></li>
                              <li><p>@if($row->shoe_size){{$row->shoe_size}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Hair Color <span class="my-ico">:</span></li>
                              <li><p>@if($row->hair_color){{$row->hair_color}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Eye Color <span class="my-ico">:</span></li>
                              <li><p>@if($row->eye_color){{$row->eye_color}}@else Not Specified @endif</p></li>
                            </ul>
                        </div>

                       
                         <div class="profile_detail_content">
                            <ul class="profile_list_content">
                              <li>Marital Status <span class="my-ico">:</span></li>
                              <li><p>@if($row->marital_status){{$row->marital_status}}@else Not Specified @endif</p></li>
                            </ul>

                            <ul class="profile_list_content">
                              <li>Children <span class="my-ico">:</span></li>
                              <li><p>@if($row->children){{$row->children}}@else Not Specified @endif</p></li>
                            </ul>

                              <ul class="profile_list_content">
                              <li>Drink <span class="my-ico">:</span></li>
                              <li><p>@if($row->drinking){{$row->drinking}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Smoke <span class="my-ico">:</span></li>
                              <li><p>@if($row->smoking){{$row->smoking}}@else Not Specified @endif</p></li>
                            </ul>

                             <ul class="profile_list_content">
                              <li>Do Drugs <span class="my-ico">:</span></li>
                              <li><p>@if($row->drugs){{$row->drugs}}@else Not Specified @endif</p></li>
                            </ul>
                              <ul class="profile_list_content">
                              <li>Body Piercing <span class="my-ico">:</span></li>
                              <li><p>@if( $row->body_piercing == 1)
                                <span >Yes</span>
                                @elseif( $row->body_piercing == 2)
                                <span >No</span>
                                @else
                                <span >Not Specified</span>
                                @endif</p></li>
                              </ul>
                              <ul class="profile_list_content">
                                <li>Tattoos <span class="my-ico">:</span></li>
                                <li><p>@if( $row->tattoos == 1)
                                  <span >Yes</span>
                                  @elseif( $row->tattoos == 2)
                                  <span >No</span>
                                  @else
                                  <span >Not Specified</span>
                                  @endif</p></li>
                                </ul>

                        </div>

                      </div>
                      </div>
                    </div>


                    <div class="container">
                      <div class="row">
                      <div class="model_bio">
                        <h2>Model (Portfolio) Biography</h2>

                        <div class="model_desc_blk">@if($row->biography) {!! $row->biography!!}@else Not Specified @endif</div>

                        <div class="col-md-4 col-sm-12 bio_dv">
                            <div class="bio_details">
                              <h3>Experiences</h3>
                              <span class="bor"></span>

                              <ul class="bio_list my_bio">
                                <li>Experience</li>
                                <li>Year</li>
                              </ul>

                              <ul class="bio_list my_bio_det">
                                <li>@if($row->model_experience){{$row->model_experience}}@else Not Specified @endif </li>
                                <li>@if($row->year=='4') More than 3 years @elseif($row->year !='') {{$row->year}} yrs @else Not Specified @endif</li>
                              </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Industries of Interest</h3>
                            <span class="bor"></span>

                            <ul class="bio_list">
                              @if($row->model_industries)
                            @foreach($row->model_industries as $ids)
                            <li> {{$ids->name}} </li>
                            @endforeach
                            @else Not Specified @endif
                            </ul>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Categories of Interest
                               @if(count($row->model_categories) > 5)
                            <span class='more'><i class="fa fa-chevron-down"></i></span>
                            <span class='less'><i class="fa fa-chevron-up"></i></span>
                            @endif
                            </h3>

                            <span class="bor"></span>
                           
                            <ul class="bio_list">
                              @if($row->model_categories)
                            @foreach($row->model_categories as $id)
                            <li class='o'>  {{$id->name}} </li>
                            @endforeach
                            @else Not Specified @endif

                            </ul>
                           
<!-- Show and hide model categories -->                        
<script type="text/javascript">
$('ul.bio_list').find('li.o').each(function(i){
// alert(i)
var num = (i)+ 1;


if(num == 1 ||num == 2 ||num ==3 ||num == 4 ||num == 5){
$(this)
.addClass('class-' + num )
$(this)
.removeClass('o')

}
else{
$(this).hide();
}
});

$('.less').hide();
$('.more').click(function(){
$(this).hide();
$('li.o').show();
$('.less').show();
});

$('.less').click(function(e){
$(this).hide();
e.preventDefault();
$('html, body').animate({
scrollTop: $("div.bio_details").offset().top
}, 400);
$('li.o').hide();
$('.more').show();


});

</script>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 bio_dv">
                        <div class="bio_details">
                            <h3>Hobbies</h3>
                            <span class="bor"></span>

                            <ul class="bio_list">
                            <li>@if($row->hobbies){!! $row->hobbies !!} @else Not Specified @endif</li>
                            </ul>

                            </div>
                        </div>
                      </div>
                      </div>
                    </div>
					
  </section>
  @if($similarModelscount > 0)
  <div class="section_seven">
    <h1>View Similar Models</h1>
    <!-- <p class="sub-text">The newest added Models. Talent Models Network - Promoting Exceptional Talents.</p> -->
    <div class="owl-carousel modelpage_model_slider" id="">

     @foreach($similarModels as $model)
      {{--*/ $username_str =str_replace(' ','',$model->firstname) /*--}}

      {{--*/ $url_id =urlencode( base64_encode($username_str.'/'.$model->id));/*--}}
      <div class="item">
        <ul class="instagramer item">
          @if($model->id % 2==0)
           <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
          <li class="wow SlideInLeft">
            @if($model->profile_photo !='')
            <img src="{!! SiteHelpers::user_model_profiles_home($model->profile_photo) !!}" >
            @else
            <img src="{{ \SiteHelpers::user_model_profile_sizes($model->id,'_large','png') }}" >
            @endif
            <div class="colorbox">
                    <div class="inner_shades">
                      <h4>Real Life & People</h4>
                      <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
                      <p>Professional Model</p>
                      <p>Book model</p>
                    </div>  
                  </div>
            <!-- <div class="followme">
              <div class="submit_follow">
                <div class="main_follow">
                  <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
                    <span></span> <p>Book Model</p>
                  </a>
                </div>
              </div>
            </div> -->
            </li>
          </a>
            @else
             <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
            <li class="wow SlideInRight">
              @if($model->profile_photo !='')
              <img src="{!! SiteHelpers::user_model_profiles_home($model->profile_photo) !!}" >
              @else
              <img src="{{ \SiteHelpers::user_model_profile_sizes($model->id,'_large','png') }}" >
              @endif
                <div class="colorbox">
                    <div class="inner_shades">
                      <h4>Real Life & People</h4>
                      <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
                      <p>Professional Model</p>
                      <p>Book model</p>
                    </div>  
                  </div>
            </li>
          </a>
            @endif
          </ul></div>
          @endforeach
        </div>

      </div>

  <div class="section_seven home_instagram_galley">
    <!-- <div class="discover wow SlideInUp">
      <h3>View Similar Models</h3>
    </div>
    <div class="">
      @if($similarModelscount > 5)
      <div class="owl-carousel modelpage_model_slider" id="">
        @else
        <div class="Count_model_slider owl-carousel modelpage_model_slider" id="">
          @endif
          @foreach($similarModels as $model)
          {{--*/ $username_str =str_replace(' ','',$model->username) /*--}}

          {{--*/ $url_id =urlencode( base64_encode($username_str.'/'.$model->id));/*--}}
          <div class="item">
            <ul class="instagramer item">
              <li>
                <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
                  @if($model->profile_photo !='')
                  <img src="{!! SiteHelpers::user_model_profiles_home($model->profile_photo) !!}" class="home_model_image">
                  @else
                  <img src="{!! \SiteHelpers::user_model_profile_sizes($model->id,'_large','png') !!}" class="home_model_image">
                  @endif

                  <div class="colorbox">
                    <div class="inner_shades">
                      <h4>Real Life & People</h4>
                      <h1>{{ $model->firstname }} {{ $model->lastname }}</h1>
                      <p>Professional Model</p>
                      <p>Book model</p>
                    </div>  
                  </div>
                </a>
              </li>
            </ul></div>
            @endforeach
          </div>
        </div>
      </div>
    @endif -->
  </div>
<div class="modal msg_popup_model fade" id="compose-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="max-width:800px;width:90%;">
    <div class="modal-content">
    <div class="modal-header bg-default">
      
      <button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true"><span>&times;</span></button>
      <h4 class="modal-titles">Compose Message </h4>
    </div>
    <div class="load_notify"></div>
   <div class="modal-body" id="compose-modal-content">
       {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
        <input type="hidden" name="typee" class="type" />
        <div class="form-group form-group_full">
          <label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
          <div class="col-xs-10">
            <div>
              <input type='hidden' name='recipient_type' value='model' class='form-control' >
              <input type='hidden' name='recipient' value='{!! $row->id !!}' class='form-control' >{!! $row->firstname !!} {!! $row->lastname !!}
              
            </div>
          </div>
        </div>
        <div class="form-group form-group_full">
          <label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
          <div class="col-xs-10">
            <div>{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control','id' => 'subject')) !!}
</div>

          <span class="err_subject" style="color:red"></span>
          </div>
        </div>
        <div class="form-group form-group_full">
          <label for="mail_subject" class="col-xs-2">Message</label>
          <div class="col-xs-10">
            <div><textarea name='message' rows='3' style="width:100%;" class='form-control theme_textarea' placeholder="Enter your message"></textarea></div>
            <span class="error" style="color:red"></span>
          </div>
        </div>
        <div class="form-group form-group_full">
        <label for="mail_subject" class="col-xs-2">Attach files</label>
        <div class="col-xs-10">
        <input  type="file" class="form-control msgpopup_browse_btn" name="images[]" id='images' placeholder="" multiple style="color:transparent;">
       
        <span class="attach_files_blk">[Allow multiple image]</span>
        </div>
        </div>
        <div class="sending_loadergs"></div>
        <div class="form-group form-group_full">
          <div class="col-xs-12">
            {!! Form::hidden('draft', '0' )  !!}
            {!! Form::hidden('messageid', '' )  !!}
            <div class="col-xs-12 msg_button_blk">
            <button type="submit" name="submit" data-type="inboxx" class="btn-sm sendbtn" ><span><i class="fa  fa-envelope "></i>Send message</span></button>
            </div>
<!--   <button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i>Save as draft</button>
 -->          </div>
        </div>
       {!! Form::close() !!}
    </div>

    </div>
  </div>
</div>

@include('footer_map')

<script>
$(document).ready(function(){   
      var owl = $('.modelpage_model_slider').owlCarousel({


    margin:0,
    nav:false,
    autoplay:true,
    responsive:true,
    loop:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:4
        },
        1000:{
            items:5
        }
    }
});
    });
 $(document).ready(function(){

   
  $(function(){
    tinymce.init({  
      mode : "specific_textareas",
      editor_selector : "mceEditor",
      theme : "advanced",
      theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
     });  
  });
  

  function tinyClean(value) {
  value = value.replace(/&nbsp;/ig, ' ');
  value = value.replace(/\s\s+/g, ' ');
  if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
    value = '';
  }
  return value;
}

   $(".sendMessage").validate({
     ignore: "not:hidden",
      rules: {
       recipient1:{
      required:true
      },
      subject:{
      required:true
      },
      message:{
      required:true
      },
      
        
      },
      messages: {
       recipient1:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Recipient Field is required</span>',
      },
      subject:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Subject Field is required</span>',
      },
      message:  
      {
      required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Message Field is required</span>',
      }
       
      },
      
        submitHandler: function(form) {
        toastr.success("Message sent Successfully");

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
    var subject = $('[name="subject"]').val();
     if(subject==''){
      $('.err_subject').html("Subject Field is required");
    }
    else{
      $('.err_subject').hide();

    }
    if(value==''){
      $('.error').html("Message Field is required");
    }else{
      $('.error').hide();
    }
    
    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();
         $('#compose-modal').modal("toggle"); 
         form.submit();
}
           
        },

        });
  
 
  $('#composeMessage').click(function(){
    
    $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
 

var msg="<?php if(isset($_REQUEST['msg'])) echo $_REQUEST['msg']; else echo ''; ?>";
 
if(msg)
{
   $('.sendMessage')[0].reset();
    $('#compose-modal').find('[name="messageid"]').val('');
    $('.sendbtn').removeAttr('disabled');
    $('.savedraft').removeAttr('disabled');
    $('#compose-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  }
  

}); 



  </script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <script type="text/javascript">
   if('{{$favourite}}' == ''){
      $('.favourites a').removeClass('favouriteActive');
      $('.favourites a').attr('title','Add to favourites');
   }else{
      $('.favourites a').addClass('favouriteActive');
      $('.favourites a').attr('title','Remove from favourites');
   }
   function addToFavourite() {

      var favourite_id = '{{$row->id}}';
      var favourite_type = 'model';
      
      $.ajax({
        url: '{{url("addToFavorites")}}',
        type: 'post',
        data: {favourite_id:favourite_id,favourite_type:favourite_type},
        dataType: 'json',
        success: function(data){
          if (data.status == 'added') {
              swal({
                title: "",
                text: "Model added to your Favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').addClass('favouriteActive');
              $('.favourites a').attr('title','Remove from favourites');
          }else if(data.status == 'removed'){
              swal({
                title: "",
                text: "Model removed from your favourites",
                icon: "success",
                dangerMode: true
              });
              $('.favourites a').removeClass('favouriteActive');
              $('.favourites a').attr('title','Add to favourites');
          }else{
            swal({
              title: "",
              text: "Some error occured. Please try again after some time.",
              icon: "danger",
              dangerMode: true
            });
          }
        }
          
      });
    }
  
        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function(value, text, event) {

                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');

                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {
                        
                        var split_id = el_id.split("_");

                        var postid = split_id[1];  // postid
                        var post_type = $('input[name=post_type]').val(); // posttype
                        // AJAX Request
                        $.ajax({
                            url: '{{url("rating_ajax")}}',
                            type: 'post',
                            data: {post_type:post_type,postid:postid,rating:value},
                            dataType: 'json',
                            success: function(data){
                              // alert(data)
                             if (data.status == 'error') {

                              // Show Error Notification
                              toastr["error"](data.msg)
                              toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": true,
                                "progressBar": false,
                                "positionClass": "toast-top-full-width",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "3000",
                                "extendedTimeOut": "1000",
                                "showEasing": "linear",
                                "hideEasing": "swing",
                                "showMethod": "slideDown"
                              }
                             }else{
                                          
                              // Show success Notification
                                toastr["success"](data.msg)
                                toastr.options = {
                                  "closeButton": false,
                                  "debug": false,
                                  "newestOnTop": true,
                                  "progressBar": false,
                                  "positionClass": "toast-top-full-width",
                                  "preventDuplicates": false,
                                  "onclick": null,
                                  "showDuration": "300",
                                  "hideDuration": "1000",
                                  "timeOut": "3000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "linear",
                                  "hideEasing": "swing",
                                  "showMethod": "slideDown"
                                }
                              
                              }
            
          
                                
                          }
                        });
                    }
                }
            });
        });
   
        </script>
    <!-- Set rating -->
  <input type="hidden" name='book' value="{{ app('request')->input('book') }}">
<script type='text/javascript'>
// $(document).ready(function(){
// $('#rating_{{$row->id}}').barrating('set',Math.round({{$avg_ratings}}));
// });

$('#images').on('change', function(e) {
var files = e.target.files,
        filesLength = files.length;
var ext = $('#images').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['png','jpg','jpeg']) == -1 ) {
    swal("File format is invalid!");
    $('#images').val(null);

} 
// if(filesLength>=10)
// {
//  swal("Allow only 10 images");
//   $('#images').val(null);
// }
})
$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#images").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\ img-circle\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/ border='0' width='50' class='fancyboxa img-circle' style='margin-right:2px;'>" +
            "<span class=\"remove\" style='cursor: pointer;'>X</span>" +
            "</span>").insertAfter("#images");
          $(".remove").click(function(){
            $('#images').val(null);
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
    $('.close').click(function () {
      $('#auto-content').hide();
      $('#theDiv').empty();
      $(".pip").remove();
      $('label.error').text('');
      $("ul.dropdown-menu").css("display","none")
       });
</script>
@if(app('request')->input('book') == 'model')
<script type="text/javascript">
 $(window).load(function(){  
   $('#compose-modal').modal('show');
    });

</script>
@endif