

		 {!! Form::open(array('url'=>'model/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Models</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Username" class=" control-label col-md-4 text-left"> Username </label>
										<div class="col-md-7">
										  <input  type='text' name='username' id='username' value='{{ $row['username'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Firstname" class=" control-label col-md-4 text-left"> Firstname </label>
										<div class="col-md-7">
										  <input  type='text' name='firstname' id='firstname' value='{{ $row['firstname'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Lastname" class=" control-label col-md-4 text-left"> Lastname </label>
										<div class="col-md-7">
										  <input  type='text' name='lastname' id='lastname' value='{{ $row['lastname'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="DOB" class=" control-label col-md-4 text-left"> DOB </label>
										<div class="col-md-7">
										  <input  type='text' name='DOB' id='DOB' value='{{ $row['DOB'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-7">
										  <input  type='text' name='gender' id='gender' value='{{ $row['gender'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="City" class=" control-label col-md-4 text-left"> City </label>
										<div class="col-md-7">
										  <input  type='text' name='city' id='city' value='{{ $row['city'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="State" class=" control-label col-md-4 text-left"> State </label>
										<div class="col-md-7">
										  <input  type='text' name='state' id='state' value='{{ $row['state'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Country" class=" control-label col-md-4 text-left"> Country </label>
										<div class="col-md-7">
										  <input  type='text' name='country' id='country' value='{{ $row['country'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode </label>
										<div class="col-md-7">
										  <input  type='text' name='zipcode' id='zipcode' value='{{ $row['zipcode'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Email" class=" control-label col-md-4 text-left"> Email </label>
										<div class="col-md-7">
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Biography" class=" control-label col-md-4 text-left"> Biography </label>
										<div class="col-md-7">
										  <input  type='text' name='biography' id='biography' value='{{ $row['biography'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo </label>
										<div class="col-md-7">
										  <input  type='file' name='profile_photo' id='profile_photo' @if($row['profile_photo'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/model_profile/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ethnicity" class=" control-label col-md-4 text-left"> Ethnicity </label>
										<div class="col-md-7">
										  <input  type='text' name='ethnicity' id='ethnicity' value='{{ $row['ethnicity'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Hair Color" class=" control-label col-md-4 text-left"> Hair Color </label>
										<div class="col-md-7">
										  <input  type='text' name='hair_color' id='hair_color' value='{{ $row['hair_color'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Eye Color" class=" control-label col-md-4 text-left"> Eye Color </label>
										<div class="col-md-7">
										  <input  type='text' name='eye_color' id='eye_color' value='{{ $row['eye_color'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Shoe Size" class=" control-label col-md-4 text-left"> Shoe Size </label>
										<div class="col-md-7">
										  <input  type='text' name='shoe_size' id='shoe_size' value='{{ $row['shoe_size'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Hip" class=" control-label col-md-4 text-left"> Hip </label>
										<div class="col-md-7">
										  <input  type='text' name='hip' id='hip' value='{{ $row['hip'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Waist" class=" control-label col-md-4 text-left"> Waist </label>
										<div class="col-md-7">
										  <input  type='text' name='waist' id='waist' value='{{ $row['waist'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bust Size" class=" control-label col-md-4 text-left"> Bust Size </label>
										<div class="col-md-7">
										  <input  type='text' name='bust_size' id='bust_size' value='{{ $row['bust_size'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Body Type" class=" control-label col-md-4 text-left"> Body Type </label>
										<div class="col-md-7">
										  <input  type='text' name='body_type' id='body_type' value='{{ $row['body_type'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Height" class=" control-label col-md-4 text-left"> Height </label>
										<div class="col-md-7">
										  <input  type='text' name='height' id='height' value='{{ $row['height'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Language" class=" control-label col-md-4 text-left"> Language </label>
										<div class="col-md-7">
										  <input  type='text' name='language' id='language' value='{{ $row['language'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Education" class=" control-label col-md-4 text-left"> Education </label>
										<div class="col-md-7">
										  <input  type='text' name='education' id='education' value='{{ $row['education'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Profession" class=" control-label col-md-4 text-left"> Profession </label>
										<div class="col-md-7">
										  <input  type='text' name='profession' id='profession' value='{{ $row['profession'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Hobbies" class=" control-label col-md-4 text-left"> Hobbies </label>
										<div class="col-md-7">
										  <input  type='text' name='hobbies' id='hobbies' value='{{ $row['hobbies'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number </label>
										<div class="col-md-7">
										  <input  type='text' name='contact_number' id='contact_number' value='{{ $row['contact_number'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Terms Agree" class=" control-label col-md-4 text-left"> Terms Agree </label>
										<div class="col-md-7">
										  <input  type='text' name='terms_agree' id='terms_agree' value='{{ $row['terms_agree'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-7">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Approved Status" class=" control-label col-md-4 text-left"> Approved Status </label>
										<div class="col-md-7">
										  <input  type='text' name='approved_status' id='approved_status' value='{{ $row['approved_status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created Date" class=" control-label col-md-4 text-left"> Created Date </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_date', $row['created_date'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Last Updated" class=" control-label col-md-4 text-left"> Last Updated </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('last_updated', $row['last_updated'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Approved Date" class=" control-label col-md-4 text-left"> Approved Date </label>
										<div class="col-md-7">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('approved_date', $row['approved_date'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Password" class=" control-label col-md-4 text-left"> Password </label>
										<div class="col-md-7">
										  <input  type='text' name='password' id='password' value='{{ $row['password'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
