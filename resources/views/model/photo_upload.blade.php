        

<link href="{{ asset('magelur/css/uploader.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('magelur/js/uploader.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Tags script -->
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<div class="video_upload_form_blk  photo_upload_from col-md-12 col-sm-12 col-xs-12">
<div class="container">

<form id="Photo-Upload" class="photo_upload_form" action="javascript:void(0)" method="POST" novalidate="novalidate">

<div class="photo_upload_block1 photo_upload_box">

<h1 class="video_title">Pick Up The Album Type</h1>
<div class="video-init-desc">
    <p> Click on the create album to add new album and upload photo into your new album.If you no need to create new album click on the choose list and pick the already created album lists
    </p>  
    </div>
  <div class="upload_radio_btn">
    <label class="agree radio_button_div">
    <input type="radio" name="album_type" value="new" checked="checked"> Create New Album 
    <span class="checkmark"></span>
    </label>
    <label class="agree radio_button_div">
    <input type="radio" name="album_type" value="old"> Choose Existing Album
    <span class="checkmark"></span>
    </label>          
    </div>


  </div>

<div class="photo_upload_block2 photo_upload_box">
  <div class="vid_details">
    <div class="video_info">
    <div class="photo_upload_box">

 
  <div class="new-albums-form">

    @if(count($albums) < $album_limit)
    <div class="new-albums-form-list">


    <h1 class="video_title">Create Album</h1>
    <div class="video-init-desc">
     <!--  <p> Provide your relevant title and description about the album.
      </p>  --> 
    </div>

      <div class="small-6 columns">
        <label>Album Name</label>
        <input  type="text" name="album_name" id='album_name'/>
      </div>

      <div class="small-6 columns">
        <label>Album Description</label>
        <textarea name="album_desc"></textarea>
      </div>
      </div>
        <input type="hidden" id="album_limit" value="no">
      @else
      <div class="new-albums-form-list">
         <center><h1 class="video_title">Your memberhip doesn't allow you to create more album. If you wish to create more albums you have to upgrade you plan and then try to upload more images.Click on the below link to upgrade your memberhip</h1><br>
          {{--*/ $uN_ID=SiteHelpers::getEncodeUrlIDs($iLoggedId) /*--}}
          @if($sUserType=='agent')
            <button type="button" onclick="location.href='{{ URL::to('agent-membership/'.$uN_ID) }}' " class="btn btn-warning btn-sm "><font style="font-size:1.2em">Click Here</font></button>
          @elseif($sUserType=='model')
            <button type="button" onclick="location.href='{{ URL::to('model-membership/'.$uN_ID) }}' " class="btn btn-warning btn-sm "><font style="font-size:1.2em">Click Here</font></button>
          @endif

         <input type="hidden" id="album_limit" value="yes">
      </div>
      @endif
  </div>

  <div class="small-6 old-albums-form columns" style="display: none;">

    <h1 class="video_title">Choose Album</h1>
    <div class="video-init-desc">
   <!--    <p> We have listed your existing albums choose the album where you want to add you photos
      </p> -->  
    </div>
    <input type="hidden" id="imagelimit" value="{{ CNF_ALBUM }}">
    <input type="hidden" name = "uploadlimit" id="uploadlimit" value="{{ CNF_ALBUM }}">
    @if(!empty($albumss))
    <select name="album_id">
      <option value="" checked> Please Choose Album </option>
      @foreach($albumss as $iKey => $alb)
        @if($alb->count < CNF_ALBUM)
          <option value="{{ $alb->album_id }}" id= "{{ $alb->count }}"> {{ $alb->title }} </option>
        @else
          <option value="{{ $alb->album_id }}" disabled> {{ $alb->title }} - Album Limit Exceeded</option>
        @endif
      @endforeach
    </select> 
    @else
    No Album to Choose 
    @endif
    <!-- Check count of images in each album -->

  </div>
  </div>
  </div>


</div>
</div>



<div class="photo_upload_block3 photo_upload_box">
<div class="vid_details">
    <div class="video_info">
    <h1 class="video_title">Photo Upload</h1>
    <div class="video-init-desc">
      <!-- <p> Pickup the files from your local.Only JPG,GIF,PNG images are allowed..
      </p>   -->
    </div>
    <div class="video_info photo_details_preview video_info_nopadding">

    
    </div>
<span class="add_details" style='display:none'>
<div>Click on Image to Add Details</div>
    
    </span>


<input type="hidden" name="remaining_media_count" value="5">
  <div  class="preview_imges_blk preview_imges_scroll1"  id="vpb-display-preview"></div> 

<div class="photo_blk_upload_blk">
<span class="span_upload_div"><i class="fa fa-upload"></i></span>
<p class="image-titl">Only JPG,PNG images are allowed..</p>
Minimum allowed size 900 * 900px
 
  <div class="browse_btn file_browse_btn">
    <div class="vpb_browse_file" onclick="document.getElementById('vpb-data-file').click();"><button><span>Browse</span></button>
    </div> 
  </div>
<input accept="image/jpg, image/jpeg, image/png," style="opacity:0" type="file" class='form-control' name="vpb-data-file" id="vpb-data-file" onchange="vpb_image_preview(this)" multiple="multiple"  />
</div>
<div id="errorToShow"></div>




 
<!-- Browse File Button -->
  <div class="browse_btn post_btn text-center">

     <!-- Upload File Button -->
     <div style="display: none;"  onClick="vpb_upload_previewed_files('{{ url('/user/save/model/image') }}');" class="vpb_pagination_button_clicked"><button><span>Post</span></button></div>
    
     <div class="submit-wave ">
      <div id="wave">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
      </div>
      <div class="input_button_div">
      <span>
      <input type="submit" name="post" value="POST">
      </span>
      </div>
    </div>

   </div>


</div>
</div>
</div>
</form>
</div>
</div>



<script type="text/javascript">

  function triggerTags(){
        //  tag plugin
        setTimeout(function(){
          $('input.photo_tags').tagsInput({
            width:'auto'
          });   
        },500);
  }

  $(document).ready(function(){
    var alb_limit = $('#album_limit').val();
    // alert(alb_limit);
    if(alb_limit=='yes')
    {
      $('div.photo_upload_block3').hide();
    }
    else
    {
      $('div.photo_upload_block3').show();
    }
    //trigger tags
    triggerTags();

    $(document).on("change", "input[name='vpb-data-file']", function(evt) {

        var val= $("input[name='vpb-data-file']").val();
  if(val=='')
  {
      $('div#errorToShow').show();
      $('div.photo_blk_upload_blk').show();
  }
  else{
    
    $('div#errorToShow').hide(); 
    $('div.photo_blk_upload_blk').hide(); 
  }
    

    }); 

    $(document).on('click','input[name="album_type"]',function(){

      
      if(this.value=='old')
      {
        $('div.photo_upload_block3').show();
      }
      else if(alb_limit=='yes')
      {
        $('div.photo_upload_block3').hide();
      }
  
      // remove check
      $('input[name="album_type"]').removeAttr('checked')

      // Reset form errors
      var myForm = document.getElementById("Photo-Upload");
      // clearValidation(myForm);
      // auto check the attribute 
      $(this).prop('checked',true);

      // trigger validate
      $("#Photo-Upload").validate();

      // get the choosed value
      var curVal = $(this).val();

     if(curVal == 'new'){
        // show album create form
        $('div.new-albums-form-list').show();
        // hide album pick list
        $('div.old-albums-form').hide();
        // show remaining fields
        $('div.new-albums-form').show();        
      }else{
        // allow user to pick the album list
        $('div.old-albums-form').show();
        // hide album create form
        $('div.new-albums-form-list').hide();
        // show remaining fields
        $('div.new-albums-form').show();

      }
    })

    $(".fancybox").fancybox({});

});

function addAlbum(){
  var title    = $('input[name="album_name"]').val();
  var desc     = $('textarea[name="album_desc"]').val();
  // $.post('{!! url("add/album")!!}',{title:title,desc:desc},function(data){

  // });
}

  // Validation form
  $("#Photo-Upload").validate({
    rules: {
      album_type:{
        required:true,
      },
    
       album_name:{
          required:true,
          remote:{
          url:"{!! url('album/checkuniquename')!!}",
          type:'post',
          data:{
            album_name:function(){
              return $('#album_name').val();
            },
            
          }
        }
        },
      album_desc:{
        required:true,
         rangelength: [5, 400]
      },
      album_id:{
        required:true,
      },category:{
        required:true,
      },tags:{
        required:true,
      },description:{
        required:true,
      },
      'vpb-data-file':{
        required:true,
      }, 

    },
    messages: {
      album_type:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Pick the Album Type",
      },
      album_name:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide Album Name"
      },
      album_desc:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide the Album Description",
        rangelength:"<span><i class='fa fa-warning error_icon_div'></i></span>Please enter a value between 5 and 400 characters long."

      },
      album_id:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Choose Your Album"
      },category:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Select the category",
      },tags:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Enter Tags",
      },description:{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Enter Video Description",
      },
      'vpb-data-file':{
        required:"<span><i class='fa fa-warning error_icon_div'></i></span>Please Provide Album Photos",
      },
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "vpb-data-file") {
            error.appendTo("#errorToShow");

        } else {
            error.insertAfter(element);
        }
        },
    submitHandler: function (form) {
      
      // trigger upload form
      vpb_upload_previewed_files('{{ url("/user/save/model/image") }}');
    }
  }); 

  function clearValidation(formElement){
 //Internal $.validator is exposed through $(form).validate()
 var validator = $(formElement).validate();
 //Iterate through named elements inside of the form, and mark them as error free
 $('[name]',formElement).each(function(){
   validator.successList.push(this);//mark as error free
   validator.showErrors();//remove error messages if present
 });
 validator.resetForm();//remove error class on name elements and clear history
 validator.reset();//remove all error and success data
}

$('select').on('change', function() {

  var totimage = $(this).children(":selected").attr("id");
  var imagelimit = $('#imagelimit').val();
  $('#uploadlimit').val(imagelimit-totimage);
  // alert($('#uploadlimit').val());
  // alert(imagelimit-totimage);

});

</script>

<style type="text/css">
  .map{display: none;}
  .page-header {
    display: none;
}
</style>
