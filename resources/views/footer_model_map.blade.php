<div class="event_map">
  <input type="hidden" name="geocomplete" id="geocomplete" />
  <div class="event_map_canvas"></div>
</div>
<!-- <div id="map"></div> -->
@foreach($models as $model)
@if(($model->latitude) !='' || ($model->latitude)!='' )
    <script type="text/javascript">
    $(function(){
    var mapStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];

    var options = {
    map: ".event_map_canvas",
    location: ['{{ $model->latitude }}', '{{ $model->longitude }}' ],
    mapOptions: {
            styles: mapStyles
          },
    markerOptions: {
    draggable: true,
    icon: '{{asset("images/spotlight-poi2.png")}}',
      },
    };

    $("#geocomplete").geocomplete(options);
    $('.map').hide();

    });
    </script>
    @else
    <script type="text/javascript">
    $(function(){
    $('.map').hide();
$('.event_map').hide();
    });
    </script>
    @endif
@endforeach




