    @if($user_type=='model'){
		@include('model_dash_header')
    {{--*/ $url = 'model-dashboard' /*--}}
		@endif
		@if($user_type=='agent'){
		@include('agent_dash_header')
    {{--*/ $url = 'agent-dashboard' /*--}}
		@endif



<div class="model_photos fav_models_page">
<div class="container">
 <div class="modelspro clearfix">
 <div class="col-md-12 col-sm-12 col-xs-12 fav_first_part">
<span class="favourite_title">Favourites</span>
{{--*/$url_id=Session::get('url_securityID')/*--}}
<button onClick="location.href='{{url('favourite-photos/'.$url_id)}}'"><span>PHOTOS</span></button>
<button onClick="location.href='{{url('favourite-videos/'.$url_id)}}'"><span>VIDEOS</span></button>
<button class="active"><span>MODELS</span></button>
<button  onClick="location.href='{{url('favourite-agents/'.$url_id)}}'"><span>AGENTS</span></button>
</div>
 <div id="post">
	 
	 
	@if(count($models_fav)>0) 
     @foreach($models_fav as $iKey => $model)

     @if($iKey == 0 || (($iKey + 1 ) % 4 ) == 0)
      <div class="row-model">
      @endif  
      {{--*/ $username_str =str_replace(' ','',$model['username']) /*--}}

{{--*/ $url_id =urlencode( base64_encode($username_str.'/'.$model['id']));/*--}}
    <a href="{{ URL::to('model-profile/'.$username_str.'/'.$url_id)}}" >
    <div class="model_item our_model_item1" id="{{$model['favourite_id']}}">
     <img src="{{ asset('uploads/model_profile/'.$model['profile_photo']) }}" >
       <div class="colorbox">
        <div class="inner_shades">
          <h4>Real Life & People</h4>
          <h1>{{ $model['firstname'] }} {{ $model['lastname'] }}</h1>
          <p>Professional Model</p>
          <p>Book model</p>
        </div>
       </div>
    </div>
  </a>

    @if((($iKey + 1 ) % 3 ) == 0)
    </div>
    @endif 

    @endforeach
    @else
    <div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
      <div class="gallery_blk_container">
        <div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">

          <!-- show upload only for model & agent -->
          <div class="empty_vertical">
          <div class="empty_msg_details clearfix">
            <div class="empty_msg_blk clearfix">
              <div class="heading_blk">
                <div class="heading_details">
                  <h1>Access Denied!</h1>
                  <h2>
                    No more Favorites found
                  </h2>
                </div>
                <div class="file_img_blk">
                  <div class="file_img_div">
                    <!-- <img src="{{asset('images/file_image1.png')}}" alt="file_image" /> -->
                    <i class="fa fa-warning"></i>
                  </div>
                </div>
              </div>
              <!-- <p>You can search on the bar below or return to home page</p> -->
              <div class="pages_blk">
                <div class="msg_previous_page">
                  <a href="{{ url('/home') }}"><span>Home</span><img src="{{asset('images/preview_image1.png')}}"></a>
                </div>
                <div class="msg_next_page">

                  <a href="{{url($url)}}/{{request()->route('id')}}"><span>Dashboard</span><img src="{{asset('images/next_image1.png')}}"></a>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    @endif
</div>
<input type="hidden" id="row" value="0">
<input type="hidden" id="all" value="{{$count}}">


  <div class="butsload  model_pagination clearfix "  id="remove-row">
  <div class="fav_models_pagination">
<div class="text-center pagination_content"> {!! $pagination->render() !!}</div>

    </div>
 </div>
 </div> 
</div>  
</div>

</div>
@include('footer_map')

<script type="text/javascript">

$(document).ready(function(){

$('.row-model a').on({
    mouseenter: function() {
        // get parent element
        var parent = $(this).parent();
        // check element exists or not
        if(parent.length > 0){
          parent.addClass('hovered')
        }
        console.log($(this).parent());
        console.log('enter');
    },
    mouseleave: function() {
        // get parent element
        var parent = $(this).parent();
        // check element exists or not
        if(parent.length > 0){
          parent.removeClass('hovered')
        }      
        console.log($(this).parent());
        console.log('leave');        
    }
})  

// Load more data
$('.load-more').click(function(){
var row = Number($('#row').val());
var allcount = Number($('#all').val());
row = row + 6;

if(row <= allcount){
$("#row").val(row);

$.ajax({
url: '{{ url("demos/loaddata") }}',
type: 'post',
data: {row:row},
beforeSend:function(){
$(".load-more").text("Loading...");
},
success: function(response){

// Setting little delay while displaying new content
setTimeout(function() {
// appending posts after last post with class="post"
$("#post").after(response).show().fadeIn("slow");

var rowno = row + 6;

// checking row value is greater than allcount or not
if(rowno > allcount){

// Change the text and background
$('.load-more').hide();
}else{
$(".load-more").text("Load more");
}
}, 2000);


}
});
}

});

});
</script>


