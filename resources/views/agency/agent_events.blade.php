	@include('agent_dash_header')
<div class="container-fluid agent_event_list_page">
	<div class="container">
		<div class="event_list_blk col-md-12 col-sm-12 col-xs-12">
			<div class="event_list_blk1 col-md-12 col-sm-12 col-xs-12 wow FadeInLeft">
				<div class="event_list_left col-md-6 col-sm-6 col-xs-12">
					<div class="event_list_img col-md-12 col-sm-12 col-xs-12">
						<a href="javascript:void(0);"><img src="images/agent_list_img1.png"></a>
					</div>
				</div>
				<div class="event_list_right col-md-6 col-sm-6 col-xs-12">
					<h1>Trendy Workplace Collection</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac.</p>
					<p>Donec ut risus volutpat ex laoreet congue eu a dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.</p>
					<h3>6:30  PM |  New  York   </h3>
					<h4><a href="javascript:void(0)">Read more</a></h4>
				</div>
			</div>
			<div class="event_list_blk1 col-md-12 col-sm-12 col-xs-12 wow FadeInRight">
				<div class="event_list_left col-md-6 col-sm-6 col-xs-12">
					<div class="event_list_img col-md-12 col-sm-12 col-xs-12">
						<a href="javascript:void(0);"><img src="images/agent_list_img2.png"></a>
					</div>
				</div>
				<div class="event_list_right col-md-6 col-sm-6 col-xs-12">
					<h1>Trendy Workplace Collection</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac.</p>
					<p>Donec ut risus volutpat ex laoreet congue eu a dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.</p>
					<h3>6:30  PM |  New  York   </h3>
					<h4><a href="javascript:void(0)">Read more</a></h4>
				</div>
			</div>
			<div class="event_list_blk1 col-md-12 col-sm-12 col-xs-12 wow FadeInLeft">
				<div class="event_list_left col-md-6 col-sm-6 col-xs-12">
					<div class="event_list_img col-md-12 col-sm-12 col-xs-12">
						<a href="javascript:void(0);"><img src="images/agent_list_img3.png"></a>
					</div>
				</div>
				<div class="event_list_right col-md-6 col-sm-6 col-xs-12">
					<h1>Trendy Workplace Collection</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac.</p>
					<p>Donec ut risus volutpat ex laoreet congue eu a dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.</p>
					<h3>6:30  PM |  New  York   </h3>
					<h4><a href="javascript:void(0)">Read more</a></h4>
				</div>
			</div>
			<div class="event_list_blk1 col-md-12 col-sm-12 col-xs-12 wow FadeInRight">
				<div class="event_list_left col-md-6 col-sm-6 col-xs-12">
					<div class="event_list_img col-md-12 col-sm-12 col-xs-12">
						<a href="javascript:void(0);"><img src="images/agent_list_img4.png"></a>
					</div>
				</div>
				<div class="event_list_right col-md-6 col-sm-6 col-xs-12">
					<h1>Trendy Workplace Collection</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac.</p>
					<p>Donec ut risus volutpat ex laoreet congue eu a dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum consectetur neque, eu ullamcorper ligula finibus ac. Donec ut risus volutpat ex laoreet congue eu a dolor.</p>
					<h3>6:30  PM |  New  York   </h3>
					<h4><a href="javascript:void(0)">Read more</a></h4>
				</div>
			</div>
			<a href="javascript:void(0);">
			<div class="event_list_add_event col-md-12 col-sm-12 col-xs-12 wow FadeInLeft">
				<div class="add_event_blk col-md-12 col-sm-12 col-xs-12">
					<h4><span><i class="fa fa-plus"></i></span></h4>
					<h5>ADD EVENT</h5>
				</div>
			</div>
			</a>
		</div>
	</div>
</div>