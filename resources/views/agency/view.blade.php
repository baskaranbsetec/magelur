@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title">
		<div class="sbox-tools pull-left">
	   		<a href="{{ url('agency?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			@if($access['is_add'] ==1)
	   		<a href="{{ url('agency/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-pencil"></i>
	   		</a> <span class="titlu">@if($row->username !='') {{ $row->username}}' Profile @else Agent Management @endif</span>
			@endif 
					
		</div>	

		<div class="sbox-tools " >
			<a href="{{ ($prevnext['prev'] != '' ? url('agency/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('agency/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-xs btn-default"> <i class="fa fa-arrow-right"></i>  </a>
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div>


	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>@if($row->username) {{ $row->username}} @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>@if($row->email) {{ $row->email}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Name</td>
						<td>@if($row->agency_name) {{ $row->agency_name}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Description</td>
						<td>@if($row->agency_description) {{ $row->agency_description}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>{!! SiteHelpers::showUploadedFile($row->profile_photo,'/uploads/agency_profile/') !!}</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Banner</td>
						<td>{!! SiteHelpers::showUploadedFile($row->agency_banner,'/uploads/agency_banner/') !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Mobile</td>
						<td>@if($row->mobile) {{ $row->mobile}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Name</td>
						<td>@if($row->business_name) {{ $row->business_name}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Description</td>
						<td>@if($row->business_description) {{ $row->business_description}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Biography</td>
						<td>@if($row->business_biography) {{ $row->business_biography}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Looking For Desc</td>
						<td>@if($row->looking_for_desc) {{ $row->looking_for_desc}}  @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership</td>
						<td>@if($row->membership) {{ $row->membership}} @else Not Specified @endif </td>
						
					</tr>
				
					
					<tr>
						<td width='30%' class='label-view text-right'>Website</td>
						<td>@if($row->website) {{ $row->website}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>@if($row->contact_number) {{ $row->contact_number}} @else Not Specified @endif </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Categories needed</td>
						<td>  @if($row->categories_needed)
							@foreach($row->categories_needed as $id)
						       {{$id->name}} <br> 
						     @endforeach
						     @else Not Specified @endif
						 </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Experience needed</td>
						<td>@if($row->experience_needed)
						 @foreach($row->experience_needed as $ids)
  							 {!! $ids->name !!} <br>
  				 			    @endforeach 
  				 			    @else Not Specified @endif 
  				 			</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Our Industries</td>
						<td> @if($row->our_industries)
							@foreach($row->our_industries as $id)
						    {!! $id->name !!} <br>
						       @endforeach
						       @else Not Specified @endif
						   </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Our Categories</td>
						<td>   @if($row->our_categories)
							@foreach($row->our_categories as $id)
						    {!! $id->name !!} <br>
						       @endforeach
						       @else Not Specified @endif
						   </td>
						
					</tr>

				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Expiration</td>
					<td>@if($row->membership_expiration == 0000-00-00) Not Specified  @else {{ $row->membership_expiration}} @endif</td>	
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Id</td>
						<td>@if($row->membership_id) {{ $row->membership_id}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Rate</td>
						<td>@if($row->rate) {{ $row->rate}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Views</td>
						<td>@if($row->views) {{ $row->views}} @else Not Specified @endif </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>DOB</td>
						<td>@if($row->DOB == '0000-00-00') Not Specified @else {{ $row->DOB}}  @endif </td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Address</td>
						<td>@if($row->address) {{ $row->address}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>@if($row->city) {{ $row->city}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>@if($row->state) {{ $row->state}}  @else Not Specified @endif</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>@if($row->country) {{ $row->country}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>@if($row->zipcode) {{ $row->zipcode}} @else Not Specified @endif </td>
						
					</tr>
				<tr>
						<td width='30%' class='label-view text-right'>Verification Status</td>
						<td>@if( $row->verification_status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>@if($row->approved_date == 0000-00-00) Not Approved  @else {{ $row->approved_date}} @endif</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>@if($row->created_date) {{ $row->created_date}} @else Not Specified @endif </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>@if($row->last_updated) {{ $row->last_updated}} @else Not Specified @endif </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop