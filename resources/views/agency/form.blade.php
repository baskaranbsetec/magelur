@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="{{ asset('magelur/css/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/css/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script src="{{ asset('magelur/js/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>

<!-- <script type="text/javascript" src="{{ asset('magelur/js/multiple-select.js')}}"></script>	
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/multiple-select.css')}}" />
 -->

<style type="text/css">
label.error,.asterix{
color: #cc0000;
font-family: 'Open Sans',Arial,Helvetica,sans-serif;
font-weight: normal;
font-size: 13px;
}
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}
	
</style>
     <div class="page-content row agency_form_page">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>@if($row['username'] !='') {{$row['username']}}'s Profile @else Agency Management @endif</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'agency/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=>'register')) !!}
<div class="col-md-12">
<fieldset><legend> Basic Information</legend>

	<div class="col-md-6">
		{!! Form::hidden('id', $row['id']) !!}	
		<input type='hidden' name='created_date' value='{{$row["created_date"]}}'>				
		<input type='hidden' name="password" value='{{ $row["password"]}}'>
		@if($row["mail"] == '')
		<input type='hidden' name="mail" value='not_send'>
		@else
		<input type='hidden' name="mail" value='{{ $row["mail"]}}'>
		@endif

		<div class="form-group  " >
		<label for="Username" class=" control-label col-md-4 text-left"> Username <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('username', $row['username'],array('id'=>"username",'class'=>"form-control",)) !!}	
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Agency Name" class=" control-label col-md-4 text-left"> Agency Name <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('agency_name', $row['agency_name'],array('id'=>"agency_name",'class'=>"form-control",)) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('email', $row['email'],array('id'=>"email",'class'=>"form-control",)) !!}	
		<!-- <input  type='email' name='email' id='email' value='{{ $row['email'] }}' class='form-control ' required/>  -->
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Language" class=" control-label col-md-4 text-left"> Language <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('language', $row['language'],array('id'=>"language",'class'=>"form-control",)) !!}                                	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Contact Number" class=" control-label col-md-4 text-left"> Contact Number <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('contact_number', $row['contact_number'],array('id'=>"contact_number",'class'=>"form-control")) !!}	
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Gender" class=" control-label col-md-4 text-left"> Gender <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"male",($row['gender']=="male") ? true:false,array('')) !!} Male </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('gender',"female",($row['gender']=="female") ? true:false,array()) !!} Female </label> 
		<!-- <label class='radio radio-inline'>
		{!! Form::radio('gender',"other",($row['gender']=="other") ? true:false,array()) !!} Other </label>  -->
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="City" class=" control-label col-md-4 text-left"> City <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('city', $row['city'],array('id'=>"city",'class'=>"form-control")) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Business Name" class=" control-label col-md-4 text-left"> Business Name <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('business_name', $row['business_name'],array('id'=>"business_name",'class'=>"form-control")) !!}                                			
		</div> 
		</div>
		 

		<div class="form-group  " >
		<label for="State" class=" control-label col-md-4 text-left"> State <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('state', $row['state'],array('id'=>"state",'class'=>"form-control")) !!}	
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Country" class=" control-label col-md-4 text-left"> Country <span class="asterix"> * </span></label>
		<div class="col-md-7">

		@if (is_numeric($row['country']))
			{{--*/ $row['country']= \SiteHelpers::getName('country',$row['country']); /*--}}
			@else
			{{--*/	$row['country'] =$row['country']; /*--}}
			@endif
			<input name="country" type="text" class="form-control" placeholder="Country" value="{{ $row["country"] }}">	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Zipcode" class=" control-label col-md-4 text-left"> Zipcode <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('zipcode', $row['zipcode'],array('id'=>"zipcode",'class'=>"form-control")) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Address" class=" control-label col-md-4 text-left"> Address <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::textarea('address', $row['address'], array('id'=>"address",'class'=>"form-control",'size' => '30x5')) !!}	
		</div> 
		</div>
		

	</div>								
	
	<div class="col-md-6">

		<div class="form-group  " >
		<label for="Profile Photo" class=" control-label col-md-4 text-left"> Profile Photo <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="preview-events">
		{!! SiteHelpers::showUploadedFile($row['profile_photo'],'/uploads/agency_profile/') !!}
		</div>
		<input  type='file' name='profile_photo' id='profile_photo' @if($row['profile_photo'] =='') class=' dashboard_browse_btn' @endif />				
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Profile Banner" class=" control-label col-md-4 text-left"> Profile Banner <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="preview-cover">
		{!! SiteHelpers::showUploadedFile($row['profile_cover'],'/uploads/agency_profile/cover/') !!}
		</div>
		<input  type='file' name='profile_cover' id='profile_cover' @if($row['profile_cover'] =='') class=' dashboard_browse_btn' @endif  />				
		</div> 
		</div>

		 

		<div class="form-group  " >
		<label for="Agency Description" class=" control-label col-md-4 text-left"> Agency Description <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::textarea('agency_description', $row['agency_description'], array('id'=>"agency_description",'class'=>"form-control",'size' => '30x5')) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Mobile" class=" control-label col-md-4 text-left"> Mobile <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('mobile', $row['mobile'],array('id'=>"mobile",'class'=>"form-control")) !!}	
		</div> 
		</div> 					

		

		
		<div class="form-group  " >
		<label for="Business Description" class=" control-label col-md-4 text-left">Business Description <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::textarea('business_description', $row['business_description'], array('id'=>"business_description",'class'=>"form-control",'size' => '30x5')) !!}	
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Website" class=" control-label col-md-4 text-left"> Website <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::url('website', $row['website'],array('id'=>"website",'class'=>"form-control")) !!}                                		
		<!-- <input  type='url' name='website' id='website' value='{{ $row['website'] }}' class='form-control ' required/>  -->
		</div> 
		</div>


	</div>
<div class="form-group col-md-12" style="width: 100%;display: inline-block;">
		<label for="Business Biography" class=" control-label col-md-12 text-left"> Business Biography <span class="asterix"> * </span></label>
		<div class="col-md-12 msce_editor_div ">
		{!! Form::textarea('business_biography', $row['business_biography'], array('id'=>"business_biography",'class'=>"form-control",'size' => '30x5')) !!}	
		</div> 
		</div> 
</div>	

<div class="col-md-12">
<fieldset><legend>Other Information</legend>

		

		<div class="col-md-6">

		<div class="form-group  " >
		<label for="Membership" class=" control-label col-md-4 text-left"> Membership <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('membership', $row['membership'],array('id'=>"membership",'class'=>"form-control")) !!}	
	    </div> 
		</div>

		<div class="form-group  " >
		<label for="Membership Expiration" class=" control-label col-md-4 text-left"> Membership Expiration <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="input-group m-b" >
		{!! Form::text('membership_expiration', $row['membership_expiration'],array('class'=>'form-control date','id'=>'membership_expiration','disabled'=>'disabled')) !!}
		<!-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->
		</div>
		</div> 
		</div>
		<div class="form-group  " >
		<label for="Membership Id" class=" control-label col-md-4 text-left"> Membership Id <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<select name='membership_id' rows='5' id='membership_id'  class='select2' >
		<option value="">-- Select ID --</option>
		</select> 
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Experience" class=" control-label col-md-4 text-left"> Experience <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('experience', $row['experience'],array('id'=>"experience",'class'=>"form-control",)) !!}                                	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Years in business" class=" control-label col-md-4 text-left"> Years in business <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('years_in_business', $row['years_in_business'],array('id'=>"years_in_business",'class'=>"date-own form-control",)) !!}                                	
		</div> 
		</div>

				 
		<div class="form-group  " >
		<label for="Rate" class=" control-label col-md-4 text-left"> Rate <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('rate', $row['rate'],array('id'=>"rate",'class'=>"form-control")) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Views" class=" control-label col-md-4 text-left"> Views <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('views', $row['views'],array('id'=>"views",'class'=>"form-control")) !!}	
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Genders Needed" class=" control-label col-md-4 text-left"> Genders needed <span class="asterix"> * </span></label>
		<div class="col-md-7">
		@if($row['genders_needed'] !='')
		{{--*/ $idArray = explode(",",$row["genders_needed"]) /*--}}
		<label class='checkbox radio-inline'>
		{!! Form::checkbox('genders_needed[]',"Female Models",(in_array('Female Models',$idArray)) ? true:false,array()) !!} Female Models </label> 
		<label class='checkbox radio-inline'>
		{!! Form::checkbox('genders_needed[]',"Male Models",(in_array('Male Models',$idArray)) ? true:false,array()) !!} Male Models </label> 
		</label>

		@else
		<label class='checkbox radio-inline'>
		{!! Form::checkbox('genders_needed[]',"Female Models",($row['genders_needed']=="Female Models") ? true:false,array()) !!} Female Models </label> 
        <label class='checkbox radio-inline'>
		{!! Form::checkbox('genders_needed[]',"Male Models",($row['genders_needed']=="Male Models") ? true:false,array()) !!} Male Models </label> 
		</label>
		 @endif
		  <span id="errorToShow"></span> 
		</div> 
		</div>




	</div>
	<div class="col-md-6">
		{{--*/ $json_cat   = '['.$row["categories_needed"].']' /*--}}
		<div class="form-group  " >
		<label for="Categories needed" class=" control-label col-md-4 text-left">Categories needed<span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('categories_needed[]',\SiteHelpers::getTableValues('model_categories') ,explode(',',$row['categories_needed']),array('class' => 'select2 ','id' => 'categories_needed','multiple'=>'multiple')) !!}
		</div> 
		</div>
		{{--*/ $json_ourcat   = '['.$row["our_categories"].']' /*--}}
		<div class="form-group  " >
		<label for="Our Categories" class=" control-label col-md-4 text-left">Our Categories<span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('our_categories[]',\SiteHelpers::getTableValues('our_categories') ,explode(',',$row['our_categories']),array('class' => 'select2 our_categories','id' => 'our_categories','multiple'=>'multiple')) !!}
		</div> 
		</div>

		{{--*/ $json_expneeded   = '['.$row["experience_needed"].']' /*--}}	
		<div class="form-group  " >
		<label for="Experience needed" class=" control-label col-md-4 text-left"> Experience needed <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('experience_needed[]',\SiteHelpers::getTableValues('experience_needed'),explode(',',$row['experience_needed']) ,array('class' => 'select2 experience_needed','id' => 'experience_needed','multiple'=>'multiple')) !!}
		</div> 
		</div>
		{{--*/ $json_ourindus   = '['.$row["our_industries"].']' /*--}}
		<div class="form-group  " >
		<label for="Our Industries" class=" control-label col-md-4 text-left"> Our Industries <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::select('our_industries[]',\SiteHelpers::getTableValues('our_industries') ,explode(',',$row['our_industries']) ,array('class' => 'select2 our_industries','id' => 'our_industries','multiple'=>'multiple')) !!}
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Traveling" class=" control-label col-md-4 text-left"> Traveling <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('traveling', $row['traveling'],array('id'=>"traveling",'class'=>"form-control")) !!}	
		</div> 
		</div>	
		<div class="form-group  " >
		<label for="DOB" class=" control-label col-md-4 text-left"> DOB <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<input type="text" name="DOB" id="datepicker" value='{{$row["DOB"]}}' class="form-control"  autocomplete='off' placeholder="Date of Birth"  >
		<span class="input-group-addon" style='display:none'></span>							      
		</div> 
		</div> 
		

		

		
	</div>
	<div class="form-group col-md-12" style="width: 100%;display: inline-block;">
		<label for="Looking For Desc" class=" control-label col-md-12 text-left"> Looking For Desc <span class="asterix"> * </span></label>
		<div class="col-md-12 msce_editor_div">
		{!! Form::textarea('looking_for_desc', $row['looking_for_desc'], array('id'=>"looking_for_desc",'class'=>"form-control",'size' => '30x5')) !!}	
		</div> 
		</div>
</fieldset>
</div>

<div class="col-md-12">
<fieldset><legend> </legend>

	<!-- <div class="form-group  " >
	<label for="Verification Status" class=" control-label col-md-4 text-left"> Verification Status<span class="asterix"> * </span> </label>
	<div class="col-md-7">
	<label class='radio radio-inline'>
	<input type='radio' name='verification_status' value ='1' required @if($row['verification_status'] == '1') checked="checked" @endif > Active </label> 
	<label class='radio radio-inline'>
	<input type='radio' name='verification_status' value ='0' required @if($row['verification_status'] == '0') checked="checked" @endif checked> Inactive </label>
	</div> 
	</div> 

	<div class="form-group  " >
	<label for="Status" class=" control-label col-md-4 text-left"> Status<span class="asterix"> * </span> </label>
	<div class="col-md-7">
	<label class='radio radio-inline'>
	<input type='radio' name='status' value ='1' required @if($row['status'] == '1') checked="checked" @endif > Active </label> 
	<label class='radio radio-inline'>
	<input type='radio' name='status' value ='0' required @if($row['status'] == '0') checked="checked" @endif checked> Inactive </label>
	</div> 
	</div> -->
	<div class="form-group  " >
		<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active </label> 					  
		<label class='radio radio-inline'>
		{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive </label>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Approved Status" class=" control-label col-md-4 text-left"> Verification Status <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		{!! Form::radio('verification_status',1,($row['verification_status']==1) ? true:false,array()) !!} Yes </label> 
		<label class='radio radio-inline'>
		{!! Form::radio('verification_status',0,($row['verification_status']==0) ? true:false,array()) !!} No </label> 		</div> 
		</div> 


</fieldset>
</div>
			
			

		
	<div style="clear:both"></div>	
		
		<div class="form-group">
			<label class="col-sm-4 text-right">&nbsp;</label>
			<div class="col-sm-8">	
			<button type="submit" name="apply" class="btn btn-info btn-sm" ><!-- <i class="icon-checkmark-circle2"></i> --> {{ Lang::get('core.sb_apply') }}</button>
			<button type="submit" name="" class="btn btn-primary btn-sm" ><!-- <i class="icon-bubble-check"></i> --> {{ Lang::get('core.sb_save') }}</button>
			<button type="button" onclick="location.href='{{ URL::to('agency?return='.$return) }}' " class="btn btn-warning btn-sm "><!-- <i class="icon-cancel-circle2 "></i> -->  {{ Lang::get('core.sb_cancel') }} </button>
			</div>	  
	
		  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>		
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

   <script type="text/javascript">
   
	$(document).ready(function() { 
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("agency/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});	

		 $('.date-own').datepicker({
                    minViewMode: 'years',
                    autoclose: true,
                     format: 'yyyy'
                });  	
	});

	$(function(){

		 var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);

    $('#datepicker').datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
	    	$('#categories_needed').val({{ $json_cat }}).trigger('change');
	    	$('#our_categories').val({{ $json_ourcat }}).trigger('change');
	    	$('#our_industries').val({{ $json_ourindus }}).trigger('change');
	    	$('#experience_needed').val({{ $json_expneeded }}).trigger('change');

	// tinyMCE.init({
 //    theme : "advanced",
 //    mode: "exact",
 //    elements : "business_biography,looking_for_desc",
 //    theme_advanced_toolbar_location : "top",
 //    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
 //    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
 //    + "bullist,numlist,outdent,indent",
 //    theme_advanced_buttons2 : "link,unlink,anchor,separator,"
 //    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
 //    theme_advanced_buttons3 : "",
 //    height:"200px",
 //    width:"100%"
	// });
	})
$('#language').tagit({
		singleField: true,
		});


$("#register").validate({

     ignore: "not:hidden",

      rules: {
  //     	years_in_business:{
  //        required:true
  //       },
  //     	agency_name:{
  //        required:true
  //       },
       username:{
         required:true
        },
        gender:{
          required:true,       
         
        },
         biography:{
          required:true,       
         
        },
       
        business_biography:{
          required:true,       
         
        },
        website:{
           url: true
        },
        looking_for_desc:{
          required:true,       
         
        },
        
  //     	membership_expiration:{
  //        required:true
  //       },
  //     	membership:{
  //        required:true
  //       },
  //     	business_biography:{
  //        required:true
  //       },
  //     	website:{
  //        required:true
  //       },

  //     	business_description:{
  //        required:true
  //       },
  //     	mobile:{
  //        required:true
  //       },
  //     	agency_description:{
  //        required:true
  //       },
		// business_name:{
  //        required:true
  //       },
  //       contact_number:{
  //        required:true,
  //        number:true,
  //        minlength:10,
  //       },
      
		email:{
          required:true,
          email:true,
          remote:{
          url:"{!! url('/agent/checkuniqueemailadmin')!!}",
          type:'post',
          data:{
            email:function(){
              var email = $('#email').val();
            	var model_id = "{{ $row['id'] }}";
              return [email,model_id];
            },
            
          }
        }
        },
       
       	// profile_photo:{
        //   // required:true,
        //   accept:"jpg,png,jpeg",
         
        // },

        // agency_banner:{
        //   // required:true,
        //   accept:"jpg,png,jpeg,gif",
         
        // },
        city:{
         required:true
        },
        // zipcode:{
        //  required:true,
        //  number:true,
        // },
        state:{
         required:true
        },
        country:{
         required:true
        },
        DOB:
        {
        required:true
        },
       
        // views:{
        // 	required:true,
        	
        // },
        //  'genders_needed[]':{
        // 	required:true,
        	
        // },
        //  'categories_needed[]':{
        // 	required:true,
        	
        // },
        //  'experience_needed[]':{
        // 	required:true,
        	
        // },
        //  'our_industries[]':{
        // 	required:true,
        // },
        //  'our_categories[]':{
        // 	required:true,
        // },
        // traveling:{
        // 	required:true,
        // },
        // looking_for_desc:{
        // 	required:true,
        // },
       
        // experience:{
        // 	required:true,
        // },
        // address:{
        // 	required:true,
        // },
        // language:{
        // 	required:true,
        // },
        // rate:{
        // 	required:true,
        // },
        // views:{
        // 	required:true,
        // },

      },
       messages: {
      contact_number:{
         
         number:"Please enter valid phone number",
         minlength:"Phone number should be 10 number"
       },
       
       
        email:{
          // required:"Please enter email id",
          email:"Please enter valid email id"
        },
       profile_photo:{
          // required:"Please choose image",
         accept:"Please Select Only Images",
        },
        agency_banner:{
          // required:"Please choose image",
         accept:"Please Select Only Images",
        },
        city:{
         // required:"Please select city"
        },
        zipcode:{
         // required:"Please select zipcode",
          number:"Numbers only allowed",
        },
         state:{
         // required:"Please select state"
        },
        country:{
         // required:"Please select country"
        },
      

       },
       errorPlacement: function(error, element) {
		    if (element.attr("name") == "genders_needed[]") {
		        error.appendTo("#errorToShow");
		    } else {
		        error.insertAfter(element);
		    }
        },
        submitHandler:function(form){
        
        form.submit();
        }
    });

	$('#register').on('click',function(){
		if($('.previewImage').html()===undefined)
		{
			$('input[name^="profile_photo"]').rules('add', {
				required: true,
			})
		}
		else{
			return true;
		}
	}); 
var _URL = window.URL || window.webkitURL;


	// show preview image
		$(document).on('change','input[name="profile_photo"]',function(){
			var file, img;
			if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
			// alert(this.width + " " + this.height);
			if(this.width < 600 || this.height < 600 )
			{
			swal('Please use Minimum  Image dimension 600 * 600px ')
			$('.preview-events img').attr('src', '{{ url("uploads/images/no-image.png")}}');
			$('input[name=profile_photo]').val(null);

			}
			};

			img.src = _URL.createObjectURL(file);
			}
		  readURL(this);
		})
		// render image
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-events img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		} 
		// show preview image
		$(document).on('change','input[name="profile_cover"]',function(){
					  var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            // alert(this.width + " " + this.height);
            if(this.width < 1420 || this.height < 500 )
            {
                swal('Please use Minimum  Image dimension 1420 * 500px ')
			$('.preview-cover img').attr('src', '{{ url("uploads/images/no-image.png")}}');
			$('input[name=profile_cover]').val(null);
                
            }
        };
       
        img.src = _URL.createObjectURL(file);
    }
		  readURLs(this);
		})
		// render image
		function readURLs(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-cover img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}   

$('body').on('change','input:file',function(){
	var _URL = window.URL || window.webkitURL,
	file, img,height,file_html='',
	id = $(this).data('id'),
	height = $(this).data('height'),
	width = $(this).data('width'),
	attrName = $(this).attr('name');
	selector = $('#'+id);
	selector.addClass('hide');
	if (this.files && this.files[0]) {
		img = new Image();
		img.src = _URL.createObjectURL(this.files[0]);
		if(id=='fav') image_type = ['png','jpg','jpeg','ico']; else image_type = ['png','jpg','jpeg']; 
		ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, image_type) == -1) {
			alert("invalid format");
			$('input[name^="profile_photo"]').val(null);
			file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
			$('input[data-id="'+id+'"]').remove();
			$('#'+attrName).html(file_html);
			selector.attr('src','');
			return false;
		}else{
			selector.attr('src',img.src);
			img.onload = function(){
				if(this.width > width || this.height > height){
					alert('Your Selected Image size '+this.width+'px * '+this.height+'px.Please use image dimension '+width+'px * '+height+'px');
					file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
					$('input[data-id="'+id+'"]').remove();
					$('#'+attrName).html(file_html);
					selector.attr('src','{{ asset("magelur/images/file-broken.png")}}');
					return false;
				}
			}
		}

	}
	setTimeout(function(){
		selector.removeClass('hide');
	},1000);
});
	</script>		 
@stop