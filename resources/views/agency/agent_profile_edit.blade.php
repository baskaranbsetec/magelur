<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js')}}"></script>
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">

@include('agent_dash_header')

<style type="text/css">
.applynow_form .selectdiv select,input{
	color:#777777;
}
</style>


<div class="dashboard-ders">
 <div class="container">
 <div class="subpanter clearfix">	
  <div class="left_part_ders">
  	 	<!--  -->

  	 	<section class="model_profile_page agent_profile_page">
			<div class="container">
				<div class="row">
					<div class="applynow_form">
						<form method="POST" action="{{ url('/agent-profile/save') }}" enctype="multipart/form-data" accept-charset="UTF-8" class="form-signup" id="register" parsley-validate="" novalidate=" ">
							{!! Form::hidden('id', $row['id']) !!}
							<div class="col-md-12 col-sm-12">
								<div class="select_gender wow bounce">
								<p>PLEASE SELECT YOUR GENDER</p>
								<ul class="gender_list">
									<li>    
							          <label for = "male">MALE</label>
							      	<!-- <input type="radio" name="radSize" id="male" value="small" checked="checked" /> -->
									<input type='radio' name='gender' value ='male' required @if($row['gender'] == 'male') checked="checked" @endif >
									</li>

									<li>
										<label for="female">FEMALE</label>
										<input type='radio' name='gender' value ='female' required @if($row['gender'] == 'female') checked="checked" @endif >
						    		</li>
						    			
						    	<!-- 	<li>
										<label for="female">OTHER</label>
										<input type='radio' name='gender' value ='other' required @if($row['gender'] == 'other') checked="checked" @endif >
						    		</li> -->
						    		
						    	</ul>
								</div>
							</div>

							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								     <div class="group"> 
								      {!! Form::text('agency_name', $row['agency_name'],array( 'placeholder'=>'Agency Name','required'=>"required")) !!}      
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Agency Name</label>
								    </div>
								</div> 
								<div class="col-md-6 col-sm-12"> 
								   <div class="group">      
								      {!! Form::text('username', $row['username'],array( 'placeholder'=>'User Name','required'=>"required")) !!} 
									  <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>User Name</label>
								    </div> 
								</div> 
							</div>
							<div class="col-md-12 col-m-12">
								
								<div class="col-md-6 col-sm-12">
								<div class="group">
								   {!! Form::text('email', $row['email'],array( 'placeholder'=>'Email Address','required'=>"required",'id'=>"email")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Email Address</label>
							    </div>
								   
								</div>

								<div class="col-md-6 col-sm-12"> 
									 <div class="group">
									{!! Form::text('experience', $row['experience'],array('id'=>"experience",'placeholder'=>'Experience','required'=>"required")) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Experience</label>
							    </div>
									
									
								</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12"> 
								
								   <p>Language</p>
								<div class="group"> 
								  {!! Form::text('language', $row['language'],array( 'placeholder'=>'Language','required'=>"required",'id'=>"language")) !!}                    
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Language</label>
							    </div>
							    <div class="group">
									{!! Form::text('mobile', $row['mobile'],array('id'=>"mobile",'placeholder'=>'Mobile','required'=>"required")) !!}	
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Mobile</label>
			
								    </div>
								    <div class="group"> 
								  {!! Form::text('contact_number', $row['contact_number'],array( 'placeholder'=>'Contact Number','required'=>"required",'id'=>"contact_number")) !!}                               
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Contact Number</label>
							    </div> 
							    <div class="group">
									<input type="text" name="DOB" id="datepicker" value='{{$row->DOB}}' class=""  autocomplete='off' placeholder="Date of Birth"  >
									<span class="input-group-addon" style='display:none'></span>							      
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>DOB</label>
								</div>
							   <div class="group">
									 <div class="selectdiv ">
											<select name='years_in_business'>
											<option value='' selected> Years in business</option>
											<?php
											$year=$row['years_in_business'];
											$firstYear = (int)date('Y') - 60;
											$lastYear = $firstYear + 60;
											for($i=$firstYear;$i<=$lastYear;$i++)
											{
											?>
											<option value='{{ $i }}' @if($year == $i)  selected='selected' @endif>{{ $i }}</option>
											<?php
											}
											?>
											</select>
										</div>
										</div>
							    <!-- <div class="group">      
									{!! Form::text('membership', $row['membership'],array('id'=>"membership",'placeholder'=>'Membership','required'=>"required")) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Membership</label>
							    </div> -->
							    <!-- <div class="group ">      
									{!! Form::text('membership_expiration', $row['membership_expiration'],array('class'=>' date','id'=>'membership_expiration','placeholder'=>'Membership Expiration','required'=>"required",'disabled'=>"disabled")) !!}
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Membership Expiration</label>
							    </div> -->
							    <!-- <div class="group">
									{!! Form::text('views', $row['views'],array('id'=>"views",'placeholder'=>'Views','required'=>"required")) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Views</label>
							    </div> -->
							     <p>Business Biography</p>
								   <div class="group">
									{!! Form::textarea('business_biography', strip_tags($row['business_biography']), array('id'=>"business_biography",'class'=>"theme_textarea",'size' => '30x5')) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label></label>
							    </div>
							    <!--  <div class="group">
									{!! Form::text('rate', $row['rate'],array('id'=>"rate",'placeholder'=>'Rate','required'=>"required")) !!}	
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Rate</label>
							  	</div> -->
							  	<div class="group">
									{!! Form::text('traveling', $row['traveling'],array('id'=>"traveling",'placeholder'=>'Traveling','required'=>"required")) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Traveling</label>
							    </div>
							    <div class="group">
									{!! Form::text('website', $row['website'],array('id'=>"website",'placeholder'=>'Business URL','required'=>"required")) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label>Business URL</label>
							    </div>
								</div> 
								<div class="col-md-6 col-sm-12">
									<p>Agency Description</p>
								 <div class="group">   
								     
									{!! Form::textarea('agency_description', strip_tags($row['agency_description']), array('id'=>"agency_description",'class'=>"theme_textarea",'size' => '30x5')) !!}	

								
								    </div>
								    <p>Business Description</p>

								    <div class="group">
									{!! Form::textarea('business_description', strip_tags($row['business_description']), array('id'=>"business_description",'class'=>"theme_textarea",'size' => '30x5')) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label></label>
							    </div>
							    <div class="group"> 
									{!! Form::text('business_name', $row['business_name'],array('id'=>"business_name",'placeholder'=>'Business Name','required'=>"required")) !!}                                			
								      <span class="highlight"></span>
								      <span class="bar"></span>
								      <label>Business Name</label>
							    </div>
							    <p style='color:#777777;'>Looking for</p>
								   <div class="group">
									{!! Form::textarea('looking_for_desc', strip_tags($row['looking_for_desc']), array('id'=>"looking_for_desc",'class'=>"theme_textarea",'size' => '30x5')) !!}	
							      <span class="highlight"></span>
							      <span class="bar"></span>
							      <label></label>
							    </div>
								</div> 
							</div>
					
							
						
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								
							   
							</div>
							
							<div class="col-md-6 col-sm-12">
								
							</div>
							</div>
							<div class="col-md-12 col-m-12">
								<div class="col-md-6 col-sm-12">
									
 									
								</div>
								<div class="col-md-6 col-sm-12">
								
								</div>

							</div>
							<div class="col-md-12 col-m-12">
							<div class="col-md-6 col-sm-12">
								
							   
   							   {{--*/ $json_cat   = '['.$row->categories_needed.']' /*--}}
							    <p style='color:#777777;'>Categories needed</p>
							    <div class="group  " >
 								<div class="selectdiv ">		
								{!! Form::select('categories_needed[]',\SiteHelpers::getTableValues('model_categories') ,explode(',',$row->categories_needed),array('class' => ' categories_needed','id' => 'categories_needed','multiple'=>'multiple')) !!}
								</div> 
								</div>
								{{--*/ $json_ourcat   = '['.$row->our_categories.']' /*--}}
								<p style='color:#777777;'>Our Categories</p>
								<div class="group  " >
 								<div class="selectdiv ">		
								{!! Form::select('our_categories[]',\SiteHelpers::getTableValues('our_categories') ,$row->our_categories,array('class' => ' our_categories','id' => 'our_categories','multiple'=>'multiple')) !!}
								</div> 
								</div>
								
								<div class="">
								<p style='color:#777777;'>Genders needed</p>
								<ul class="gender_list">
								<li>    
								{{--*/ $idArray = explode(",",$row->genders_needed) /*--}}
								
								<label class="agree">Female Models
    							{!! Form::checkbox('genders_needed[]',"Female Models",(in_array('Female Models',$idArray)) ? true:false,array()) !!}   
    							<span class="checkmark"></span>
								</label>
								</li>
								<li>
								
								<label class="agree">Male Models
    							{!! Form::checkbox('genders_needed[]',"Male Models",(in_array('Male Models',$idArray)) ? true:false,array()) !!} 
    							<span class="checkmark"></span>
								</label>
								</li>
								</ul>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-12">
								 
								{{--*/ $json_ourindus   = '['.$row->our_industries.']' /*--}}
							    <p style='color:#777777;'>Our Industries</p>
								<div class="group  " >
 								<div class="selectdiv ">		
								{!! Form::select('our_industries[]',\SiteHelpers::getTableValues('our_industries') ,$row->our_industries ,array('class' => ' our_industries','id' => 'our_industries','multiple'=>'multiple')) !!}
								</div> 
								</div>
								{{--*/ $json_expneeded   = '['.$row->experience_needed.']' /*--}}
								<p style='color:#777777;'>Experience needed</p>
								<div class="group  " >
 								<div class="selectdiv ">		
								{!! Form::select('experience_needed[]',\SiteHelpers::getTableValues('experience_needed'),$row->experience_needed ,array('class' => ' experience_needed','id' => 'experience_needed','multiple'=>'multiple')) !!}
								</div> 
								</div>
								
								
							</div>
							</div>

					<div class="events_map_form events_map_form_model col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 col-sm-12 col-xs-12 event_form_fields">
							<div class="events_label_blk col-md-12 col-sm-12 col-xs-12">
							<!-- <div class="col-md-12 col-sm-12 col-xs-12 form-group  ">
								<label for="Enddate" class=" control-label text-left"> Select Your Location </label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name='geocomplete' id="geocomplete" type="text" placeholder="Select Your Location" class="form-control" value="{{ $row["address"] }}" />
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Select Your Location</label>
							</div>
							<!-- <div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Address</label>

							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name="formatted_address" type="text" class="form-control" placeholder="Address" value="{{ $row["address"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Address</label>

							</div>
							<!-- <div class="col-md-7 form-group  ">
								<label class=" control-label text-left">City</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name="locality" type="text" class="form-control" placeholder="City" value="{{ $row["city"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>City</label>
							</div>	
							<!-- <div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Zip Code</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name="postal_code" type="text" class="form-control" placeholder="Zip Code" value="{{ $row['zipcode'] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Zip Code</label>
							</div>
							<!-- <div class="col-md-7 form-group  ">
								<label class=" control-label text-left">State</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name="administrative_area_level_1" class="form-control" placeholder="State" type="text" value="{{ $row["state"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>State</label>
							</div>
							<!-- <div class="col-md-7 form-group  ">
								<label class=" control-label text-left">Country</label>
							</div> -->
							<div class="col-md-12 col-sm-12 col-xs-12  group form-group  event_input_field_blk">
								<input name="country" type="text" class="form-control" placeholder="Country" value="{{ $row["country"] }}">
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Country</label>	
							</div>
						<input name="ids" type="hidden" class="form-control" value="{{ $row["id"] }}">	
						<input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
						<input type="hidden" name="lng" value="{{ $row['longitude'] }}" />
						</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 map_blk">
							<div class="input-group m-b" style="width:100% !important;">
							<div id="map_canvas" class="map_canvas" style="width: 100%;height: 290px;display:none;"></div>
						</div>
						</div>
						</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="apply_almost wow SlideInLeft">
									

									<div class="load_button">
										<button type="submit"><span>Submit</span></button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!--  -->
</div>
</div>
</div>
</div>

		

		

	</div>
	<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
    <script>
    $('#geocomplete').on('input', function() {
      	$('.map_canvas').css('display','block');

      });
      if($('#geocomplete').val()!='')
      {
      	$('.map_canvas').css('display','block');

      }
    $(document).ready(function(){
    	// set default values for categories
    	
    	$('#categories_needed').val({{ $json_cat }}).trigger('change');
    	$('#our_categories').val({{ $json_ourcat }}).trigger('change');
    	$('#our_industries').val({{ $json_ourindus }}).trigger('change');
    	$('#experience_needed').val({{ $json_expneeded }}).trigger('change');
    });

      var formid = $('form#register').find('input[name="ids"]').val();			
		if(formid ){
			var mapStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];
				var options = {
					map: ".map_canvas",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					details: "form",					
					types: ["geocode", "establishment"],
					mapOptions: {
			            styles: mapStyles
			          },
					markerOptions: {
					draggable: true,
					icon: '{{asset("images/spotlight-poi2.png")}}',
					}
				};
				$("#geocomplete").geocomplete(options);
			}
				$("#geocomplete").geocomplete({
					map: ".map_canvas",
					details: "form",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					types: ["geocode", "establishment"],

				});	
		$("#geocomplete").bind("geocode:dragged", function (event, latLng)
		{
          $("input[name=lat]").val(latLng.lat());
          $("input[name=lng]").val(latLng.lng());

            var latlng = new google.maps.LatLng(latLng.lat(), latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status)
            {
                if (status == google.maps.GeocoderStatus.OK)
                {

                    if (results[0])
                    {

                        $("#geocomplete").val(results[0].formatted_address)
                        console.log(results[0])
                        $.each(results[0].address_components, function (index, data)
                        {
                           
                            if (data.types[0] == "administrative_area_level_1")
                            {
                                $("input[name=administrative_area_level_1]").val(data.long_name)
                            }
                           
                            if (data.types[0] == "administrative_area_level_2")
                            {
                                $("input[name=locality]").val(data.long_name)
                            }
                           
                            
                            if (data.types[0] == "country")
                            {
                            	 $("input[name=country]").val(data.long_name)
                            }
                            
                            if (data.types[0] == "postal_code")
                            {
                                $("input[name=postal_code]").val(data.long_name)
                            }
                            
                            
                                $("input[name=formatted_address]").val(results[0].formatted_address)
                                // alert("Location: " + results[1].formatted_address);
                        });
                    }
                }
            });		
            });		
		
		$(function(){
	
	    $('.map').hide();

		// tinyMCE.init({
		// theme : "advanced",
		// mode: "exact",
		// elements : "agency_description,business_description,business_biography,looking_for_desc",
		// theme_advanced_toolbar_location : "top",
		// theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
		// + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
		// + "bullist,numlist,outdent,indent",
		// theme_advanced_buttons2 : "link,unlink,anchor,separator,"
		// +"undo,redo,cleanup,code,separator,sub,sup,charmap",
		// theme_advanced_buttons3 : "",
		// height:"190px",
		// width:"505px"
		// });
			// $('#language').tagsly({});

			$( " .our_industries" ).select2( {
			placeholder : ' '
			} );
			$( " .categories_needed" ).select2( {
			placeholder : ''
			} );
			$( " .our_categories" ).select2( {
			placeholder : ''
			} );
			$( " .experience_needed" ).select2( {
			placeholder : ''
			} );
	$( "#membership_expiration" ).datepicker({
      		showOtherMonths:  true,
            selectOtherMonths:  true,
            changeMonth:      true,
            changeYear:         true,
            dateFormat:         "yy-mm-dd",
            showAnim:         "slideDown",
            defaultDate:        "-18y",
            yearRange:        "-100: -0",
            //minDate:          '-100y', 
            //maxDate:          "-17y",

    });
    
    // $( "#datepicker" ).datepicker({
    //   showOtherMonths:  true,
    //         selectOtherMonths:  true,
    //         changeMonth:      true,
    //         changeYear:         true,
    //         dateFormat:         "yy-mm-dd",
    //         showAnim:         "slideDown",
    //         defaultDate:        "-18y",
    //         yearRange:        "-68: -18",
    //         //minDate:          '-100y', 
    //         //maxDate:          "-17y",

    // });
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);

    $('#datepicker').datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        changeYear: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
	})



$('input[name="language"]').tagsInput({
        width:'auto'
      });
$('#register').on('click',function(){
         if($('#language_tagsinput span.tag').text() != '')
         {
        $(this).valid();
         }
       $('select').on('change', function() {
	$(this).valid();
}); 

      }); 
$("#register").validate({

     ignore: "not:hidden",

      rules: {
      	years_in_business:{
         required:true
        },
      	agency_name:{
         required:true
        },
       username:{
         required:true
        },

      	business_biography:{
         required:true
        },
      	website:{
      	   required:true,
           url: true
        },
		geocomplete: { required:true },

      	business_description:{
         required:true
        },
      	mobile:{
         required:true
        },
      	agency_description:{
         required:true
        },
		business_name:{
         required:true
        },
        contact_number:{
         required:true,
         number:true,
         minlength:10,
        },
      
		email:{
		required:true,
		email:true,

		},
       
        locality:{
         required:true
        },
        postal_code:{
         required:true,
         number:true,
        },
        administrative_area_level_1:{
         required:true
        },
        country:{
         required:true
        },
       
         'genders_needed[]':{
        	required:true,
        	
        },
         'categories_needed[]':{
        	required:true,
        	
        },
         'experience_needed[]':{
        	required:true,
        	
        },
         'our_industries[]':{
        	required:true,
        },
         'our_categories[]':{
        	required:true,
        },
        traveling:{
        	required:true,
        },
        looking_for_desc:{
        	required:true,
        },
       
        experience:{
        	required:true,
        },
        formatted_address:{
        	required:true,
        },
        language:{
        	required:true,
        },
        

      },
       messages: {
      years_in_business:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      	agency_name:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
       username:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },

      	business_biography:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      	website:{
      		required:"<span><i class='fa fa-warning'></i></span> This field is required",
           url: "<span><i class='fa fa-warning'></i></span> Please enter a valid URL",
        },
		geocomplete: { 
			required:"<span><i class='fa fa-warning'></i></span> This field is required", 
		},
      	business_description:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      	mobile:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      	agency_description:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
		business_name:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        contact_number:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
         number:"<span><i class='fa fa-warning'></i></span> Please enter a valid number",
         minlength:"<span><i class='fa fa-warning'></i></span> Please enter a atleast 10 numbers",
        },
      
		email:{
		required:"<span><i class='fa fa-warning'></i></span> This field is required",
		email:"<span><i class='fa fa-warning'></i></span> Invalid Email address", 

		},
       
        locality:{
        required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        postal_code:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
         number:"<span><i class='fa fa-warning'></i></span> Please enter a valid zipcode",
        },
        administrative_area_level_1:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        country:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
       
         'genders_needed[]':{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        	
        },
         'categories_needed[]':{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        	
        },
         'experience_needed[]':{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        	
        },
         'our_industries[]':{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
         'our_categories[]':{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        traveling:{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        looking_for_desc:{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
       
        experience:{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        formatted_address:{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
        language:{
        	required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      

       },
        submitHandler:function(form){
       // toastr.success("Saved Successfully");
        // setTimeout(function(){
        form.submit();
      // },2000);
        }
    });



    </script>

@if(Session::get('msgstatus')=='success')
@if(Session::has('messagetext'))
<script>
toastr.success("{!! Session::get('messagetext') !!}");
</script>

@endif
@else
@if(Session::has('messagetext'))
<script>
toastr.error("{!! Session::get('messagetext') !!}");
</script>

@endif
@endif