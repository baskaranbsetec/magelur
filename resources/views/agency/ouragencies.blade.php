<!-- <script type="text/javascript" src="{{ asset('magelur/js/lazyload.js')}}"></script>
 -->
<div class="ourmodel_banner ouragent_banner">
  <div class="ourmod_cnt">
    <h1>Talent Model's Agencies</h1>
    <div class="two_laid"><a href="{!! url('home') !!}">Home</a><p>our agencies</p></div>
  </div>
</div>

<div class="model_photos mainder_pent our_agencies_blk">
<div class="container">
 <div class="modelspro clearfix">
 
 <div id="post">
     @foreach($agencies as $agency)
      {{--*/ $username_str =str_replace(' ','',$agency->username) /*--}}
      {{--*/ $url_id =urlencode( base64_encode($username_str.'/'.$agency->id));/*--}}
      
    <a href="{{ URL::to('agent-profile/'.$username_str.'/'.$url_id)}}" >
    <div class="model_item agent_item" id="{{$agency->id}}">
     <img src="{{ \SiteHelpers::user_agent_profile_sizes($agency->id,'_medium','png') }}" >
       <div class="colorbox">
        <div class="inner_shades">
          <h4>{{ $agency->agency_name }}</h4>
          <p>{{ $agency->username }},{{ \SiteHelpers::getName('country',$agency->country) }}</p>
        </div>
       </div>
    </div>
  </a>
    @endforeach
</div>
<div id="posts">
  </div>
            <input type="hidden" id="row" value="0">
            <input type="hidden" id="all" value="{{$count}}">


  <div class="butsload clearfix "  id="remove-row">
@if($count > 6)
      <button class="load-more"> <span>Load More</span></button>
@endif
    </div>
 
 </div> 
</div>  
</div>



<script type="text/javascript">

$(document).ready(function(){
$('.map').hide();

// Load more data
$('.load-more').click(function(){
var row = Number($('#row').val());
var allcount = Number($('#all').val());
row = row + 6;

if(row <= allcount){
$("#row").val(row);

$.ajax({
url: '{{ url("agency/loaddata") }}',
type: 'post',
data: {row:row},
beforeSend:function(){
$(".load-more").text("Loading...");
},
success: function(response){

// Setting little delay while displaying new content
setTimeout(function() {
// appending posts after last post with class="post"
$("#posts").before(response).show().fadeIn("slow");

var rowno = row + 6;
// alert(rowno)
// checking row value is greater than allcount or not
if(rowno >= allcount){

// Change the text and background
$('.load-more').hide();
}else{
$(".load-more").text("Load more");
}
}, 2000);


}
});
}

});

});
</script>








