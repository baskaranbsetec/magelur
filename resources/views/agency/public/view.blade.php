<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Username</td>
						<td>{{ $row->username}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Email</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Name</td>
						<td>{{ $row->agency_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Description</td>
						<td>{{ $row->agency_description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profile Photo</td>
						<td>{{ $row->profile_photo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Agency Banner</td>
						<td>{!! SiteHelpers::formatRows($row->agency_banner,$fields['agency_banner'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Mobile</td>
						<td>{{ $row->mobile}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Name</td>
						<td>{{ $row->business_name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Description</td>
						<td>{{ $row->business_description}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Business Biography</td>
						<td>{{ $row->business_biography}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Looking For Desc</td>
						<td>{{ $row->looking_for_desc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership</td>
						<td>{{ $row->membership}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Approved Date</td>
						<td>{{ $row->approved_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Verification Status</td>
						<td>{{ $row->verification_status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Website</td>
						<td>{{ $row->website}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Contact Number</td>
						<td>{{ $row->contact_number}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Expiration</td>
						<td>{{ $row->membership_expiration}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Membership Id</td>
						<td>{{ $row->membership_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Rate</td>
						<td>{{ $row->rate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Views</td>
						<td>{{ $row->views}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Address</td>
						<td>{{ $row->address}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>City</td>
						<td>{{ $row->city}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>State</td>
						<td>{{ $row->state}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Country</td>
						<td>{{ $row->country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Zipcode</td>
						<td>{{ $row->zipcode}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created Date</td>
						<td>{{ $row->created_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Last Updated</td>
						<td>{{ $row->last_updated}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	