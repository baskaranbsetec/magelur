@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Agent Management</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>


	<div class="sbox-content"> 	
		<div class="sbox-tools pull-left" >

			<!-- @if($access['is_add'] ==1)
	   		<a href="{{ URL::to('agency/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create</a>
			@endif   -->
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" data-type="0" class="updatestatus tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete</a>
			@endif 
			<!-- <a href="{{ URL::to( 'agency/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->
			<!-- @if($access['is_excel'] ==1)
			<a href="{{ URL::to('agency/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i> Download</a>
			@endif -->

		</div>

	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'agency/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
		     {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}

	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Business Name' ||$t['label']=='Verification Status' ||$t['label']=='Agency Description' || $t['label']=='Agency Banner' || $t['label']=='Business Biography' || $t['label']=='Looking For Desc'|| $t['label']=='Membership')
							{{--*/ $t['label']=" " /*--}}
								
							@endif
							<th><span>{{ $t['label'] }}</span></th>			
						@endif 
					@endif
				@endforeach
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody class="agnetmag">   
<tr id="orderDetails">
<td class="number"></td>
<td></td>
<td id="username" class="fieldsearch"> <div class="select-style">{!! Form::text('username',"{$username}",array('class'=>'form-control', 'placeholder'=>'Name', )) !!} </div></td>
<td id="email" class="fieldsearch"> <div class="select-style">{!! Form::text('email',"{$email}",array('class'=>'form-control', 'placeholder'=>'Email', )) !!} </div></td>
<td id="agency_name" class="fieldsearch"> <div class="select-style">{!! Form::text('agency_name',"{$agency_name}",array('class'=>'form-control', 'placeholder'=>'Agency Name', )) !!} </div></td>
<td></td>
<td class="profilesed"></td>
<td></td>

<td id="mobile" class="fieldsearch"> <div class="select-style">{!! Form::text('mobile',"{$mobile}",array('class'=>'form-control', 'placeholder'=>'Mobile', )) !!} </div></td>
<td></td>
<td></td>
<td></td>
<td></td>

<td id="approved_date" class="fieldsearch"> <div class="select-style">{!! Form::text('approved_date',"{$approved_date}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Approved Date', )) !!} </div></td>
<td></td>


<td id="status" class="fieldsearch"> 
	<div class="select-style">
	<select name='status' class = 'select2 '>
		<option value="">Select option</option>
		<option value="1" {{ $status == '1' ? 'selected' : ''}}>Active</option>
		<option value="0" {{ $status == '0' ? 'selected' : ''}}>Inactive</option>

	</select>

	</div>
</td>
<td class="icons">
<input type="hidden"  value="Search">
<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
<a href="{{ url('agency') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
</td>
</tr>  
					
            {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
@foreach($model_filter as $row)                <tr>
					<td width="30"> {{ $i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					<!-- <td>
					 	<div class="dropdown">
						  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
						  <span class="caret"></span></button>
						  <ul class="dropdown-menu">
						 	@if($access['is_detail'] ==1)
							<li><a href="{{ URL::to('agency/show/'.$row->id.'?return='.$return)}}" class="tips" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i> {{ Lang::get('core.btn_view') }} </a></li>
							@endif
							@if($access['is_edit'] ==1)
							<li><a  href="{{ URL::to('agency/update/'.$row->id.'?return='.$return) }}" class="tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i> {{ Lang::get('core.btn_edit') }} </a></li>
							@endif
						  </ul>
						</div>

					</td>
 -->
 {{--*/ $i++ /*--}}
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>
						  @if($field['field']=='business_name'||$field['field']=='verification_status'||$field['field']=='agency_description' || $field['field']=='agency_banner' || $field['field']=='business_biography' || $field['field']=='looking_for_desc'|| $field['field']=='membership' )
								{{--*/$field['field']=='';/*--}}
								@elseif($field['field']=='status')
								
								@if( $row->status == 1)
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
								@elseif($field['field']=='approved_date')
								@if( $row->approved_date == '0000-00-00 00:00:00')
								Not approved

								@else
								{{$row->approved_date}}

								@endif
							@elseif($field['field']=='profile_photo')	
							{!! SiteHelpers::showUploadedFile($row->profile_photo,'/uploads/agency_profile/') !!}
							@else						 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}						 
						 @endif
						 </td>
						@endif	
					 @endif					 
				 @endforeach	
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('agency/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('agency/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
						<a  class="tips btn btn-xs btn-white" data-toggle="modal" data-target="#myModal" title="Send Mail"  onclick="getmailid('{{ $row->email }}');"><i class="fa fa-envelope "></i></a>											
					
				</td>	
                </tr>
				
            @endforeach
 
        </tbody>

    </table>
	<input type="hidden" name="md" value="" />
	@if(count($model_filter) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	{!! Form::close() !!}
		<!-- The Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:black">Send Mail</h4>
      </div>
      <div class="modal-body">
       <form role="form" method="get" id="reused_form" action="agentmail/agents">

            <div class="row">
                <div class="col-sm-12 form-group">
                	<label for="subject" style="color:black">
                        Subject:</label>
                     <textarea class="form-control" type="textarea" name="subject" id="subject" maxlength="200" rows="1" style="background-color:white;color:black"></textarea>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="message" style="color:black">
                        Message:</label>
                    <textarea class="form-control" type="textarea" name="message" id="message" maxlength="6000" rows="7" style="background-color:white;color:black"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                         From:</label>
                    <input type="email" class="form-control" id="fromemail" name="fromemail" style="background-color:white;color:black" value = "{{ CNF_EMAIL }}" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email" style="color:black">
                        To:</label>
                    <input type="email" class="form-control" id="emails" name="email" style="background-color:white;color:black" required>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-white pull-right" >Send →</button>
                </div>
            </div>

        </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
	<!-- start footer -->
<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			@if( $field['field'] == 'profile_photo' || $field['field'] == 'agency_banner' || $field['field'] == 'business_name' || $field['field'] == 'business_biography'|| $field['field'] == 'looking_for_desc'|| $field['field'] == 'membership'|| $field['field'] == 'verification_status'|| $field['field'] == 'agency_description') {{ $field['field'] == '' }} @else
			  <option value="{{ $field['field'] }}" 
			  @endif
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				@endif	
			  > {{ $field['label']}} </option>
			@endif	  
		  @endforeach
		 
		</select>		
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
		</p>		
	   </div>
	   @if(count($model_filter) >0)
	   <div class="pagination_s">
	   	{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
	   </div>
	   @endif
	  </div>
	</div>	
	<!-- end footer -->
	</div>
</div>	
	</div>	  

<script type="text/javascript">
$("#reused_form").validate({
			ignore: "not:hidden",
			rules: {
				message:{
					required:true,
				},
				subject:{
					required:true,
				},
		}
	});
 	function getmailid(mailid)
 {
 	// alert(mailid);
 	$('#emails').val(mailid);
 }
 $('.close').click(function(){
$('textarea[name=subject]').val('');
$('textarea[name=message]').val('');
$('label.error').hide();	

});
$('.pagination_blk').hide();

$( document ).ready(function() {
    
$("#approved_date").change(function() {
   
	$(".datetimepicker").hide();
});

});


</script>	
@stop