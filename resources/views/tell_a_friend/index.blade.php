@extends('layouts.app')

@section('content')
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title"> 
		
		<h1>Tell a friend</h1>
		

	</div>


	<div class="sbox-content"> 	
		

	
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
					<th> No </th>  
					<th> Inviter Name </th>
					<th> Referral Name </th>
					<th> Status </th>   
					<th> User type </th>                                
					<th> Invited On </th>
					<th> Registered On </th>
			  </tr>
        </thead>

        <tbody>        
        <tr id="orderDetails">
        		<td class="number"></td>
        		<td id="I_name" class="fieldsearch"> <div class="select-style">{!! Form::text('I_name',"{$I_name}",array('class'=>'form-control', 'placeholder'=>'Inviter Name', )) !!} </div></td>
        		<td id="R_name" class="fieldsearch"> <div class="select-style">{!! Form::text('R_name',"{$R_name}",array('class'=>'form-control', 'placeholder'=>'Referral Name', )) !!} </div></td>
        		<td id="status" class="fieldsearch"> <div class="select-style">
        			<select name='status' class = 'select2 '>
        				<option value="">Select option</option>
        				<option value="requested">Requested</option>
        				<option value="logged">Logged</option>

        			</select>

        			 </div>
        		</td>
        		   <td id="user_type" class="fieldsearch"> <div class="select-style">{!! Form::text('user_type',"{$user_type}",array('class'=>'form-control', 'placeholder'=>'User type', )) !!} </div></td>

        			<td id="invited_on" class="fieldsearch"> <div class="select-style">{!! Form::text('invited_on',"{$invited_on}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Invited On', )) !!} </div></td>
        			<td id="registered_on" class="fieldsearch"> <div class="select-style">{!! Form::text('registered_on',"{$registered_on}",array('autocomplete'=>'off','class'=>'form-control datetime', 'placeholder'=>'Registered On', )) !!} </div></td>
        			<td class="icons">
        				<input type="hidden"  value="Search">
        				<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
        				<a href="{{ url('tell_a_friend') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
        			</td>
        		</tr> 						
           {{--*/ $i = ($results->currentPage() - 1) * $results->perPage() + 1;/*--}}
				@foreach($results as $invite)
				<tr>
				    <td> {{ $i }} </td>  
				    <td> 
				    	@if($invite->user_type=='agent')
				    	<a href="{{url('agent-profile/'.$invite->your_id)}}">{{ $invite->your_name}}</a> </td> 
						@else
						<a href="{{url('myprofile/'.$invite->your_id)}}">{{ $invite->your_name}}</a> </td> 
						@endif   
				    <td> 
				    	@if($invite->friend_name!='')
				    	{{$invite->friend_name}} 
				    	@else
				    	-
				    	@endif
				    </td> 
				    <td> 
						@if( $invite->status == 'logged')
						<span class="label label-success" >Logged</span>
						@else
						<span class="label label-danger" style="width: 55px;">Requested</span>
						@endif
					</td>                                
				    <td> @if($invite->user_type == '') - @else {{$invite->user_type}} @endif</td>
				    <td> {{$invite->created}} </td>
				    <td> {{$invite->registered}} </td>    
				    <td></td> 
				    {{--*/ $i++ /*--}}
				</tr>
				@endforeach                            
             
        </tbody>
      
    </table>
   
	<input type="hidden" name="md" value="" />
	@if(count($results) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions form_footer">
	 
	   {!! Form::open(array('url'=>'tell_a_friend','method'=>'GET')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="Sort" class="select-alt">
		  <option value=""> Sort </option>	    
		  <option value="your_name" @if(isset($pager['sort']) && $pager['sort'] == 'your_name') 
								selected="selected"
								@endif>Inviter Name</option>
		  <option value="friend_name" @if(isset($pager['sort']) && $pager['sort'] == 'friend_name') 
								selected="selected"
								@endif> Referral Name </option>
		  <option value="user_type" @if(isset($pager['sort']) && $pager['sort'] == 'user_type') 
								selected="selected"
								@endif>User type</option>
		  <option value="status" @if(isset($pager['sort']) && $pager['sort'] == 'status') 
								selected="selected"
								@endif>Status</option>
			<option value="created" @if(isset($pager['sort']) && $pager['sort'] == 'created') 
								selected="selected"
								@endif>Invited On</option>
			<option value="registered" @if(isset($pager['sort']) && $pager['sort'] == 'registered') 
								selected="selected"
								@endif>Registered On</option>

		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $results->total() }}</b>
		</p>		
	   </div>
		<div class="col-sm-4">	
		<div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  
 			<div class=""> 
	  			{!! str_replace('/?', '?', $results->appends(Input::except('page'))->render()) !!}
	  		</div>
	  </div>
	  </div>
	</div>	
	</div>	
</div>	
	</div>	  
</div>	

<script type="text/javascript">
$( document ).ready(function() {
    
$("#invited_on").change(function() {
   
	$(".datetimepicker").hide();
});

$("#registered_on").change(function() {
   
	$(".datetimepicker").hide();
});

});
</script>
	
@stop