 
 <div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>Blog</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<div class="dashboard-ders blog_block">
 <div class="container">	
  <div class="left_part_ders">
   <ul class="clearfix">
  	<li class="abt_mtb">
  	 <div class="full_dash_bg">
      @foreach($blogs as $blog) 
  	 	<div class="left_abt clearfix blog_left_side">
      <div class="heading_date">
      <h1>{!! date("d" , strtotime($blog->created_date)) !!}</h1>
      <h2>{!! date("M" , strtotime($blog->created_date)) !!}.</h2>
      </div>
      <div class="blog_right_side">
     <a href="{{ URL::to('/blog-details/'.$blog->id)}}"> 
      <h4>{{$blog->title}}</h4>
    </a>
      <h5>Category: {{\SiteHelpers::getName('category',$blog->category)}}  |  Author: JakubSpitzer</h5>
      <div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
      <!-- <img src="images/blog_img1.png"> -->
    <a href="{{ URL::to('/blog-details/'.$blog->id)}}">   
      <img src="{{ asset('uploads/blog/'.$blog->blog_image) }}" class="img-responsive">
    </a>
      </div>
        {{---*/ $content = substr( $blog->description,0,250 ) /*---}}
      {!! $content !!}
    
      <div class="blog_button_blk">
      <div class="blog_button_div1">
        <?php
$str= $blog['tags'] ;
$arr=explode(",",$str);
 
?>
@foreach($arr as $tag) 
@if($tag)
<?php
$tagg=str_replace(' ', '-', trim(strtolower($tag)));
?>
<button>{!! $tag !!}</button>

@endif
@endforeach


      <!-- <button>Tutorial</button>
      <button>Bokeh</button> -->
      </div>
      <div class="blog_button_div2">
      <div class="blog_twitter">
      <div class="blog_twitter_left">
      <img src="images/blog_fb_img.png">Like</div>
      <div class="blog_twitter_right">1</div>
      </div>
      <div class="blog_twitter blog_twitter_blk">
      <div class="blog_twitter_left">
      <img src="images/blog_twitter_img.png">Tweet</div>
      <div class="blog_twitter_right">1</div>
      </div>
      </div>
      <div class="blog_button_div3">
      <div class="view_all_btn">
      <a href="{{ URL::to('/blog-details/'.$blog->id)}}"><button>Read article...</button></a>
      </div>
      </div>
      </div>
      </div>
      </div>  
      @endforeach
  	 	
  	 	  	 	 	
  	 </div>	
     @if(str_replace('/?', '?', $blogs->render()))
     <div class="full_dash_bg">
      <div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  
 <div class="pagination_content"> 
 <h1>page {!! str_replace('/?', '?', $blogs->currentPage()) !!} of  {!! str_replace('/?', '?', $blogs->count()) !!}
</h1>
 {!! str_replace('/?', '?', $blogs->render()) !!}

   <ul class="pagination last_page_blk">
   <!-- <li class="disabled"><span>«</span></li> 
   <li class="active"><span>1</span></li>
   <li><a href="model?page=2">2</a></li>
   <li><a href="model?page=3">3</a></li>
   <li><a href="model?page=4">4</a></li>
   <li><a href="model?page=5">5</a></li> 
   <li><a href="model?page=2" rel="next">»</a></li> -->
   <li class="yellow_bg_clr"><a href="http://192.168.1.65:81/magelur/public/blogs?page={!! str_replace('/?', '?', $blogs->lastPage()) !!}
" rel="next">last</a></li>
   </ul>

 </div>
 </div>
 </div>
    @endif
  	</li>
  	<li class="right_mtb">
  	 <div class="full_dash_bg">	
  	  <div class="left_abt clearfix"><h4>Categories</h4></div> 
      @foreach($blogcategories as $cat)      
         
  	  <div class="blog_category">
  	  <p>{{ $cat->name }}<span>({{ $cat->count }})</span></p>
  	 
  	  </div>
      @endforeach  
	 </div>	
	 <div class="full_dash_bg">	
  	  <div class="left_abt clearfix"><h4>Tag Cloud</h4></div> 
  	  <div class="blog_category tag_cloud_blk">
@foreach($tags as $tag)
{{--*/ $str= $tag['tags']; /*--}}
{{--*/ $arr=explode(",",$str); /*--}}

@if($tag->id % 2 == 0)

@foreach($arr as $tag) 
@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
<span>{!! $tag !!}</span>@endif
@endforeach

@elseif($tag->id / 5 == 1)

@foreach($arr as $tag) 
@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
<sub>{!! $tag !!}</sub>@endif
@endforeach

@else

@foreach($arr as $tag) 
@if($tag){{--*/ $tagg=str_replace(' ', '-', trim(strtolower($tag)));/*--}}
{!! $tag !!}@endif
@endforeach



@endif

@endforeach
        <!-- web <span>photography</span> red tutorials <sub>mastering</sub> apple <span>bokeh</span> fundamental <span>eye</span> facebook twitter <span>life</span> freebie music <span>old</span></div> -->
	  
    </div>	

  	 <!--  <div class="left_abt clearfix"><h4>Categories</h4></div> 
  	  <div class="blog_category photography_blk">
  	  <div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="photography_img_div">
  	 	<img src="images/blog_photography1.png">
  	 	</div>
  	 	<div class="view_all_btn">
  	 	<button>View all...</button>
  	 	</div>

  	  </div> -->
	 	
  	</li>
  </ul>	
   <div class="load_more_btn_div load_more_btn_post col-md-12 col-sm-12 col-xs-12">
 <button><span>Load More</span></button>
 </div>
 </div>		
 </div>
</div>	
</div>


