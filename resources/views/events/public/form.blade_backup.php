

{!! Form::open(array('url'=>'events/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

@if(Session::has('messagetext'))

{!! Session::get('messagetext') !!}

@endif
	
<div class="magelur_about faq_sec">	
<section>
   	<div class="container-fluid magelur_header">
   		<div class="magelur_agent_event_banner">
			<div class="video-container">
				<div id="either-gif-or-video">
				            <!-- <video class="embed-responsive-item fillWidth" poster="images/landing_page.jpg" autobuffer loop autoplay playsinline controls >
				            	<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" media="all and (max-width:480px)">
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.mp4" type="video/mp4" >
   								<source src="http://demo.bsetec.com/demo/magelur/images/model.ogv" type="video/ogg" >	
				            </video> -->
				</div>
				<div class="event_banner_title">
				    <h1>Crave - Event</h1>				        	
				</div>
			/div&gt;
		</div>
   </div>
</div></section>
<div class="container">
<div class="col-md-12 event_page_frontend">
<div class="sbox-title">
	<h1>Events Management</h1>
</div>
<div class="sbox-content">
<ul class="parsley-error-list">
	@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</ul>
	<fieldset><!-- <legend> Events</legend> -->
		<!-- hidden fields -->
		<input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' class='form-control ' /> 

		<!-- Title -->
		<div class="form-group  " >
			<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
			<div class="col-md-8">
				<input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 		

		<!-- Description -->
		<div class="form-group  " >
			<label for="Note" class=" control-label col-md-4 text-left"> Note </label>
			<div class="col-md-8">
				<textarea name='note' rows='5' id='note' class='form-control '  
				>{{ $row['note'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 	
		<!-- event status -->
		<div class="form-group  " >
			<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
			<div class="col-md-8">
				<input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
				class='form-control ' /> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 

		<!-- event start date -->
		<div class="form-group  " >
			<label for="Startdate" class=" control-label col-md-4 text-left"> Startdate </label>
			<div class="col-md-8">

				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('startdate', $row['startdate'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>

			</div> 
			<div class="col-md-1">

			</div>
		</div> 	

		<!-- event end date -->
		<div class="form-group  " >
			<label for="Enddate" class=" control-label col-md-4 text-left"> Enddate </label>
			<div class="col-md-8">

				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('enddate', $row['enddate'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>

			</div> 
			<div class="col-md-1">

			</div>
		</div> 	

		<!-- event Tags -->				
		<div class="form-group  " >
			<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
			<div class="col-md-8">
				<textarea name='tags' rows='5' id='tags' class='form-control '  
				>{{ $row['tags'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div>

		<!-- event Categories --> 					
		<div class="form-group  " >
			<label for="Categories" class=" control-label col-md-4 text-left"> Categories </label>
			<div class="col-md-8">
				<textarea name='categories' rows='5' id='categories' class='form-control '  
				>{{ $row['categories'] }}</textarea> 
			</div> 
			<div class="col-md-1">

			</div>
		</div> 	

		<!-- image file -->		
		<div class="form-group  " >
			<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
			<div class="col-md-8">
				<input type="file"  name="image" />
			</div> 
			<div class="col-md-1">

			</div>
		</div> 	
		<!-- video embed link -->
		<div class="form-group  " >
			<label for="Image" class=" control-label col-md-4 text-left"> Video Embed </label>
			<div class="col-md-8">
				<input type="input" name="embed" class='form-control ' />
			</div> 
			<div class="col-md-1">
			</div>
		</div> 							

	</fieldset>




<div style="clear:both"></div>	


<div class="form-group">
	<label class="col-sm-4 text-right">&nbsp;</label>
	<div class="col-sm-8">	
		<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
		<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
	</div>	  
	
</div> 
</div>
</div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function() { 
		
		

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
</script>		 
