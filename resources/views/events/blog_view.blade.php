<!-- <section class="page-header">
	<div class="container">
		<h1> {{ strtoupper($pageTitle) }}</h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="{{ url('post')}}"> Posts </a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ol>
	</div>
</section> -->
<div class="magelur_about faq_sec">
<section>
   <div class="container-fluid magelur_header">
    <div class="magelur_about_banner faq_banner model_gallery_banner">
     

     <div class="banner_title wow bounce">
      <h1>Blog</h1>
<!--        <ul class="link_list">
        <li class="active"><a href="{!! url('') !!}"> <div class="magelur_about faq_sec"></a></li>
        <li>//</li>
        <li><a href="javascript:void(0)">CONTACT US</a></li>
       </ul> -->
     </div>
    </div>
   </div>
  </section>
<div class="blog_post_page">
<div class="container m-t">
	<div class="row"  style="padding:25px 0;">
		<div class="col-md-9 left_abt clearfix blog_left_side">
		<div class="posts">
		<div class="heading_date">
			<h1>  {{ date("M j " , strtotime($row->created)) }}  </h1> 
      	</div>
      	<div class="blog_right_side blog_details_right_side">
      	<h4>{{ $row->title }}</h4>
      	<h5>Category: {{ $cate }}  |  Author: {{ $author }}</h5>
      	<div class="blog_img_blk col-md-12 col-sm-12 col-xs-12">
      	<a href="javascript:void(0);">
      		<img src="{{ asset('uploads/blogs/'.$row->image) }}" class="img-responsive">
      	</a>
      	</div>

      	<div class="desc">
      		{{ $row->note }}
      	</div>

      	<hr class="yellow_line">
      	<div class="blog_button_blk">
      	<div class="blog_button_div1">       
		<button>Newyork</button>      
      </div>
      <div class="blog_button_div2 blog_details_button_div2">
      <div class="blog_twitter">
      <div class="blog_twitter_left">
      <img src="http://192.168.1.65:81/magelur/public/images/blog_fb_img.png">Like</div>
      <div class="blog_twitter_right">1</div>
      </div>
      <div class="blog_twitter blog_twitter_blk">
      <div class="blog_twitter_left">
      <img src="http://192.168.1.65:81/magelur/public/images/blog_twitter_img.png">Tweet</div>
      <div class="blog_twitter_right">1</div>
      </div>
      </div>
      </div>      
      </div>
      <div class="blog_comments_blk">


		@if($conpost['commsys'] ==1)
		<div class="comments">
<!-- 		<h3> Comment(s) </h3> -->

		@foreach($comments as $comm)

		      <div class="comment_blk1">
      <h1>{{ ucwords($comm->username) }} | Varanasi, India</h1>
<!--       <div class="rating_blk">
      <ul class="starer">
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_yell"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
          <li class="star_gray"><a href="javascript:void(0)"></a></li>
        </ul>
      </div> -->
      <div class="content" style="display:block">
			<p>{!! PostHelpers::formatContent($comm->comments) !!}</p>
			<div class="tools" style="display:none">
				@if(Session::get('gid') == '1' OR $comm->userID == Session::get('uid')) 
				<a href="{{ url('blogs/remove/'.$row->id.'/'. $row->uri.'/'.$comm->commentID) }}"><i class="fa fa-minus-circle"></i> Remove  </a>
				@endif
			</div>
		</div>
      </div>



		<div class="info" style="display:none;">
		<div class="avatar" >
		<?php if( file_exists( './uploads/users/'.$comm->profile_photo) && $comm->profile_photo !='') { ?>
			<img src="{{ URL::to('uploads/users').'/'.$comm->avatar }} " border="0" width="40" class="img-circle" />
		<?php  } else { ?> 
			<img alt="" src="http://www.gravatar.com/avatar/{{ md5($comm->email) }}" width="40" class="img-circle" />
		<?php } ?> 
		</div>

			 
			<!--  {{ date("M j, Y " , strtotime($comm->posted)) }} -->
			
		</div>
		

		@endforeach
		<div class="comment_text_area">
		<form method="post"  action="{{ url('blogs/comment') }}" parsley-validate novalidate class="form">
		<div class="note">
			@if(Session::has('messagetext'))	  
				{!! Session::get('messagetext') !!}
			@endif	
<!-- 
				{!! PostHelpers::formatContent($row->note) !!} -->
		</div>
		<textarea rows="5" placeholder="Enter Your Comments" required name="comments"></textarea>
		<div class="comment_submit_btn">
		<button type="submit"><span>Submit Comment</span></button>	
		</div>
		<input type="hidden" name="pageID" value="{{ $row->id }}" />	
		<input type="hidden" name="uri" value="{{ $row->uri }}" />						
		</form>
		</div>

		</div>
		@endif
      </div>

				
				<div class="info" style="display:none">
					<i class="fa fa-eye "></i>  <span>  Views (<b> {{ $row->views }} </b>)  </span>   
					 
					<i class="fa fa-calendar "></i>  <span> {{ date("M j, Y " , strtotime($row->created)) }} </span> 
					
				</div>
			


				@if($conpost['commimage'] ==1 )
				<div class="image">
				<img src="{{ asset('uploads/images/'.$row->image)}}" class="img-responsive" />
				</div>	
				@endif

				<div class="labels" style="display:none"><br />
				<b>Articles In :  </b><br /><br />
				{!! $labels !!}
				
				<a href="{{ url('post')}}" class="btn btn-default btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back </a>

				</div>	
			</div>   
		</div>

		<div class="col-md-3">
			@include('events.widget',array("conpost"=>$conpost))
		</div>

	</div>
</div>
</div>
</div>

