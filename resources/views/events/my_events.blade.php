<?php
use App\Library\Markdown;
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{{\Session::get('messagetext')}}">
@elseif (\Session::has('messagedelete'))
    <input type="hidden" id="msg" value="{{\Session::get('messagedelete')}}">   
@else
    <input type="hidden" id="msg" value="">  
@endif
@include('agent_dash_header')
<div class="magelur_about faq_sec">
 
 @if(empty($rowData))
 <div class="magelur_gallery col-md-12 col-sm-12 col-xs-12">
  <div class="container gallery_blk_container">

   <div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">
   <div class="empty_vertical">
    <div class="empty_msg_details clearfix">
      <div class="empty_msg_blk clearfix">
        <div class="heading_blk">
          <div class="heading_details">
            <h1>No More Events !</h1>
            <h2>Sorry Haven't add any events under your profile Or Admin Needs to Approve you Events.Create a Event through this website  to connect to your audience and empower them to spend time together in the real world</h2>
          </div>
          <div class="file_img_blk">
            <div class="file_img_div">
             <!--  <img src="{{ asset('images/file_image1.png')}}" alt="file_image" /> -->
             <i class="fa fa-warning"></i>
           </div>
         </div>
       </div>
       <!-- <p>You can search on the bar below or return to home page</p> -->
       <div class="pages_blk">
        <div class="msg_previous_page">
          <a href="{{ url('events/list')}}"><span>Event List</span><img src="{{ asset('images/preview_image1.png')}}"></a>
        </div>
        <div class="msg_next_page">

          <a href="{{ url('events/update')}}"><span>Create Event</span><img src="{{ asset('images/next_image1.png')}}"></a>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
</div>
</div>
@endif
  
   @if(!empty($rowData))
   	<section class="agent_news_event agent_news_myevents">
   		<div class="container event_news_container">
        <div class="myevents_add col-md-12 col-sm-12 col-xs-12">
        <a href="{{ URL::to('events/update')}}">
        <div class="add_myevents_blk col-md-12 col-sm-12 col-xs-12">
          <div class="add_icon_div col-md-12 col-sm-12 col-xs-12">
          <span class="add_icon_span"><i class="fa fa-plus"></i></span>
          </div>
          <h1>{{ Lang::get('core.add_events') }}</h1>
        </div>
        </a>
        </div>
        <div class="myevents_page_blk">
   			<div class="news_event">
   				<p>{{ ucfirst($agentname) }} {{ Lang::get('core.events') }}</p>
   			</div>

   			<div class="col-md-12 col-sm-12 col-xs-12 event_news_blk">
          <input type="hidden" name="tot_eve" id="tot_eve" value="{{ $total_events }}">
        <input type="hidden" name="disp_eve" id="disp_eve" value="{{ count($rowData) }}">
   				@foreach ($rowData as $row)
   				<div class="event-unit">
   					<div class="col-md-7 col-sm-12 col-xs-12 event_news">
   						<div class="event_news_image">
   							<a href="{{ url('events/view/'.base64_encode($row->id).'/'.$row->uri)}}">
                   @if(is_file('uploads/events/'.$row->image))
      <img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
      @else
      <img src="{{ asset('/images/thumb.png') }}" class="img-responsive">
      @endif
   								
   							</a>
   						</div>
   					</div>

   					<div class="col-md-5 col-sm-12 col-xs-12 event_news">
   						<div class="event_news_content">

   							<a class="event-link" href="{{ url('events/view/'.base64_encode($row->id).'/'.$row->uri)}}">
   								<h1>{{ $row->title }}</h1>
   							</a>	
                <!-- show edit button only for owner & admin -->
                @if(\Session::get('agent_id') == $row->ownerid || \Session::get('user_type') == 'admin'  )
                <div class="event_close_btn">
                  <a href="{{ url('events/update/'.$row->id.'/' )}}">
                  <i class="fa fa-pencil"></i>
                  </a>
                  <br><br>
                  <a href="#"  onclick=" addToDelete()" title="Delete">
                  <i class="fa fa-trash"></i>
                  </a>
                </div>
                <script type="text/javascript">
                function addToDelete() {

                swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover Your Event",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                $(location).attr('href', "{{ url('events/delete/'.$row->id.'/' )}}");
                } else {
                swal("Your Event is safe!");
                }
                });   
                }
                </script>
                @endif
                <span style=@if($row->status=='Active') 'color:green' @else 'color:red' @endif>Approval : {{ $row->status }}</span>
   							@if(strlen($row->note) > 780)
   							<p>{{ mb_substr($row->note,0,600) }}....</p>
   							@else
   							<p>{{ $row->note }}</p>
   							@endif

   							<span>{{ date("F d , Y " , strtotime($row->created)) }}{{ date("h:i A" , strtotime($row->created)) }}<!-- {{ date(" dS M y h:i  A" , strtotime($row->created)) }} --></span>
   							<a href="{{ url('events/view/'.base64_encode($row->id).'/'.$row->uri)}}">{{ Lang::get('core.read_more') }}</a>
   						</div>
   					</div>
   				</div>

   				@endforeach

   			</div>
        </div>
        </div>
   		<div class="pagination_content text-center"> {!! $pagination->render() !!}</div>
   	</section>
    @endif
</div>
<div class="event_map">
  <input type="hidden" name="geocomplete" id="geocomplete" />
  <div class="event_map_canvas"></div>
</div>
@foreach($images as $agent)
@if($agent->latitude || $agent->longitude)

    <script type="text/javascript">

    $(function(){
var mapStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];

    var options = {
    map: ".event_map_canvas",
    location: ['{{ $agent->latitude }}', '{{ $agent->longitude }}' ],
     mapOptions: {
            styles: mapStyles
          },
    markerOptions: {
    draggable: true,
    icon: '{{asset("images/spotlight-poi2.png")}}',
      },
    };

    $("#geocomplete").geocomplete(options);
    $('.map').hide();

    });
    </script>
  @else
    <script type="text/javascript">
    $(function(){
    $('.map').hide();
$('.event_map').hide();
    });

    
    </script>
    @endif
@endforeach

<script type="text/javascript">
if($('#msg').val()!="")
{
  swal($('#msg').val());
}

// else if($('#tot_eve').val()!=$('#disp_eve').val())
// {
//   swal("All Your Event will be Visible only after Admin approves your Events");
// }

</script>