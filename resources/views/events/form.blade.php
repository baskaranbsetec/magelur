@extends('layouts.app')

@section('content')
<style type="text/css">
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}
.map {
    display: none;
}
</style>


<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script>
<div class="page-content row">
	<!-- Page header -->


	<div class="page-content-wrapper m-t">


		<div class="sbox">
			<div class="sbox-title"> 
				<div class="sbox-tools pull-left" >
					<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
				</div>
				<h1> @if( $row["title"]!='') {{ $row["title"] }} @else Events @endif</h1>
				<!-- <div class="sbox-tools " >
					@if(Session::get('gid') ==1)
					<a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
					@endif 			
				</div>  -->

			</div>
			<div class="sbox-content"> 	

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>	

				{!! Form::open(array('url'=>'events/save?return='.$return, 'class'=>'form-horizontal','id'=>'events','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="col-md-12">

					<fieldset><!-- <legend> Events</legend> -->
						<!-- hidden fields -->
						    <input type="hidden" name="agree" value="0" />            

						<input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' class='form-control ' /> 
						<input  type='hidden' name='post_id' id='post_id' value='{{ $row['id'] }}' class='form-control ' /> 	
						<!-- get lat & longitute -->
						<input type="hidden" name="lat" value="{{ $row['latitude'] }}" />
						<input type="hidden" name="lng" value="{{ $row['longitude'] }}" />
						<!-- Title -->
						<div class="form-group  " >
							<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
							<div class="col-md-7">
								<input  type='text' name='title' id='title' value='{{ $row["title"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 		

						<!-- Description -->
						<div class="form-group  " >
							<label for="Note" class=" control-label col-md-4 text-left"> Note </label>
							<div class="col-md-7">
								<textarea name='note' minlength="20" rows='5' id='note' class='form-control input-sm    '  
								>{{ $row['note'] }}</textarea> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 	


						<!-- Email -->
						<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> Email </label>
							<div class="col-md-7">
								<input  type='text' name='email' id='email' value='{{ $row["email"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 

						<!-- Mobile -->
						<div class="form-group  " >
							<label for="Mobile" class=" control-label col-md-4 text-left"> Mobile </label>
							<div class="col-md-7">
								<input  type='text' name='mobile' id='mobile' value='{{ $row["mobile"] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 						

							 

						<!-- event start date -->
						<div class="form-group  " >
							<label for="Startdate" class=" control-label col-md-4 text-left"> Startdate </label>
							<div class="col-md-7">

								<div class="input-group m-b">
									{!! Form::text('startdate', $row['startdate'],array('class'=>'form-control datetime')) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>

							</div> 
							<div class="col-md-1">

							</div>
						</div> 	

						<!-- event end date -->
						<div class="form-group  " >
							<label for="Enddate" class=" control-label col-md-4 text-left"> Enddate </label>
							<div class="col-md-7">

								<div class="input-group m-b">
									{!! Form::text('enddate', $row['enddate'],array('class'=>'form-control datetime')) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>

							</div> 
							<div class="col-md-1">

							</div>
						</div> 	



						<div class="events_map_form events_map_form_backend col-md-12">
						<div class=" event_form_fields form-group">
							<div class="events_label_blk col-md-12">
							<!-- <div class="col-md-7 form-group  ">
								<label for="Enddate" class=" control-label text-left"> Select Your Location </label>
							</div> -->
							<!-- <div class="col-md-5 form-group  event_input_field_blk">
								<input name='geocomplete' id="geocomplete" type="text" placeholder="" class="form-control" value="{{ $row["address"] }}" />
							</div> -->
							<div class="col-md-4 form-group  ">
								<label class=" control-label text-left">Address</label>
							</div>
							<div class="col-md-7 form-group  event_input_field_blk">
								<input name="formatted_address" type="text" class="form-control" value="{{ $row["address"] }}">
							</div>
							<div class="col-md-4 form-group  ">
								<label class=" control-label text-left">City</label>
							</div>
							<div class="col-md-7 form-group  event_input_field_blk">
								<input name="locality" type="text" class="form-control" value="{{ $row["city"] }}">
							</div>	
							<div class="col-md-4 form-group  ">
								<label class=" control-label text-left">Zip Code</label>
							</div>
							<div class="col-md-7 form-group  event_input_field_blk">
								<input name="postal_code" type="text" class="form-control" value="{{ $row['zipcode'] }}">
							</div>
							<div class="col-md-4 form-group  ">
								<label class=" control-label text-left">State</label>
							</div>
							<div class="col-md-7 form-group  event_input_field_blk">
								<input name="administrative_area_level_1" class="form-control" type="text" value="{{ $row["state"] }}">
							</div>
							<div class="col-md-4 form-group  ">
								<label class=" control-label text-left">Country</label>
							</div>
							<div class="col-md-7 form-group  event_input_field_blk">
								<input name="country" type="text" class="form-control" value="{{ $row["country"] }}">	
							</div>
						</div>
						</div>
						<div class="col-md-4 map_blk">
							<div class="input-group m-b" style="width:100% !important;">
							<!-- <div id="map_canvas" class="map_canvas" style="width: 100%;height: 290px;"></div> -->
						</div>
						</div>
						</div>

						<!-- event link -->				
						<div class="form-group  " >
							<label for="Tags" class=" control-label col-md-4 text-left"> Event Link </label>
							<div class="col-md-7">
								<input name='link' id='link' class='form-control' value="{{ $row['event_link'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>


						<!-- event Tags -->				
						<div class="form-group  " >
							<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
							<div class="col-md-7 theme_tags">
								<input name='tags' id='tags' class='form-control' value="{{ $row['tags'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>

						<!-- event Categories --> 					
						<div class="form-group  " >
							<label for="Categories" class=" control-label col-md-4 text-left"> Categories </label>
							<div class="col-md-7" data-val="{{ $row['categories'] }}">
									{!! Form::select('categories[]',\SiteHelpers::getTableValues('category') ,explode(',',$row['categories']),array('class' => 'select2 ','id' => 'categories','multiple'=>'multiple')) !!}	
							</div> 
							<div class="col-md-1">

							</div>
						</div> 	
						<!-- image file -->		
						<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
							
							<div class="col-md-7">
								<input type="file"  name="image" class=" form-control" accept=".jpg, .jpeg, .png" />
								<div class="col-md-7">
								<div class="preview-events event_form_img">
									@if(empty($row["image"]))
									<img src="{{ asset('uploads/images/no-image.png')}}" border="0" width="50" height="50" class="img-circle">
									@else
									<img src="{{ asset('uploads/events/'.$row['image'])}}" border="0" width="50" height="50" class="img-circle">
									@endif
								</div>
							</div>
							</div> 
							
						</div> 	
						<!-- video embed link -->
						<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> Video Embed </label>
							<div class="col-md-7">
								<input type="file" name="video" accept="video/mp4,video/x-m4v,video/*" value="{{ $row['video'] }}"  class=" form-control"  />
								
								@if(empty($row["video"]))
								<video class="video-preview" width="400" controls style="display:none;">
									<source src="mov_bbb.mp4" id="video_here">
										Your browser does not support HTML5 video.
								</video>
								@else
								<input type="hidden" name="video_id" value="{{ $row['video'] }}" />
								<input type="hidden" name="video_ext" value="{{ $row['video_ext'] }}" />
								<video class="video-preview" width="400" controls>
									<source src="{{ asset('uploads/events/videos/'.$row['video'].'/video.'.$row['video_ext'])}}" id="video_here">
										Your browser does not support HTML5 video.
								</video>	
								@endif							

							</div> 
							<div class="col-md-1">
							</div>

						</div> 
						<div class="form-group  " >
							<label for="Entry cost" class=" control-label col-md-4 text-left"> Entry cost </label>
							<div class="col-md-7">
								<input type='text' name='entrycost' id='entrycost' class='form-control' value="{{ $row['entrycost'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>
						<div class="form-group  " >
							<label for="publishID" class=" control-label col-md-4 text-left">publishID </label>
							<div class="col-md-7">
							{!! Form::select('publishID',array(''=>'Select publishID')+\SiteHelpers::getEventFieldCategories('publishID'), $row['publishID'], array('class'=>'form-control publishID' ) ) !!}							
							</div> 
							<div class="col-md-1">
							</div>
						</div>
						<div class="form-group" >
							<label for="Get Ticket" class=" control-label col-md-4 text-left"> Get Ticket </label>
							<div class="col-md-7">
								<input type='url' name='get_ticket' id='get_ticket' class='form-control' value="{{ $row['get_tickets'] }}" />
							</div> 
							<div class="col-md-1">

							</div>
						</div>
						 
						<!-- Need models with following fields -->
						<div class="form-group  " >
							<label for="Need Models" class=" control-label col-md-4 text-left">  Need Models </label>
							<div class="col-md-7">
							<label class='radio radio-inline'>
							{!! Form::radio('need_models','yes',($row['need_model']=="yes") ? true:false,array()) !!} Yes </label> 
							<label class='radio radio-inline'>
							{!! Form::radio('need_models','no',($row['need_model']=="no") ? true:false,array()) !!} No </label> 		
							</div> 
						</div> 
					<div class='models_types' style='display:none'>	 	
						<div class="form-group" >
							<label for="Talent Needed" class="control-label col-md-4 text-left"> Talent Needed </label>
							<div class="col-md-7">
							{!! Form::select('talent_needed',array(''=>'Select Talent Needed')+\SiteHelpers::getEventCategories('talent_needed'),$row['talent_needed'], array('class'=>'form-control talent_needed') ) !!}
							</div> 
						</div>
						<div class="form-group" >
							<label for="Travel Package" class="control-label col-md-4 text-left"> Travel Package </label>
							<div class="col-md-7">
							{!! Form::select('travel_package',array(''=>'Select Travel Package')+\SiteHelpers::getEventCategories('travel_package'), $row['travel_package'], array('class'=>'form-control travel_package' ) ) !!}
							</div> 
						</div>
						<div class="form-group" >
							<label for="Accommodation Package" class="control-label col-md-4 text-left"> Accommodation Package </label>
							<div class="col-md-7">
							{!! Form::select('accomm_package',array(''=>'Select Accommodation Package')+\SiteHelpers::getEventCategories('accomm_package'), $row['accomm_package'], array('class'=>'form-control accomm_package') ) !!}
							</div> 
						</div>	
						<div class="form-group" >
							<label for="Benefit Package" class="control-label col-md-4 text-left"> Benefit Package </label>
							<div class="col-md-7">
							{!! Form::select('benefit_package',array(''=>'Select Benefit Package')+\SiteHelpers::getEventCategories('benefit_package'), $row['benefit_package'], array('class'=>'form-control benefit_package' ) ) !!}
							</div> 
						</div>
						<div class="form-group  " >
							<label for="Dress code" class=" control-label col-md-4 text-left"> Dress code </label>
							<div class="col-md-7 theme_tags">
							<input type='text' name='dress_code' id='dress_code' class='form-control dresscode' placeholder="Enter dress code" value="{{$row['dresscode']}}" />
							</div> 
							<div class="col-md-1">
							</div>
						</div>
						
						
					</div>
					<!-- End Need model -->
					<!-- event status -->
						<div class="form-group  " >
							<label for=" Status" class=" control-label col-md-4 text-left">  Status </label>
							<div class="col-md-7 nopadding-left">
							<label class='radio radio-inline'>
							{!! Form::radio('status','Active',($row['status']=='Active') ? true:false,array()) !!} Active </label> 
							<label class='radio radio-inline'>
							{!! Form::radio('status','Inactive',($row['status']=='Inactive') ? true:false,array()) !!} Inactive </label> 		
							
							</div> 
						</div>

					</fieldset>
					</div>


					<div style="clear:both"></div>	

					
					<div class="form-group backend_button">
						<label class="col-sm-4 text-right">&nbsp;</label>
						<div class="col-sm-8">	
							<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
							<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
							<button type="button" onclick="location.href='{{ URL::to('events?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
						</div>	  
					</div> 

					{!! Form::close() !!}
				</div>
			</div>		 
		</div>	
	</div>		
	 
	<script type="text/javascript">
		$(document).ready(function() { 
			$('.removeMultiFiles').on('click',function(){
				var removeUrl = '{{ url("events/removefiles?file=")}}'+$(this).attr('url');
				$(this).parent().remove();
				$.get(removeUrl,function(response){});
				$(this).parent('div').empty();	
				return false;
			});

			// set selected values 
			var categories     = '{{ $row["categories"] }}';
			if(categories){
			var selectedValues = categories.split(',');
			$("select#categories").select2('val',selectedValues);	
			}
			
			//  tag plugin
			$('input[name="tags"]').tagsInput({
				width:'auto'
			});	
			$('input[name="dress_code"]').tagsInput({
				width:'auto',
		});	


			var formid = $('form#events').find('input[name="id"]').val();			
			// if(!formid){
			// 	// render map application
			// 	$("#geocomplete").geocomplete({
			// 		map: ".map_canvas",
			// 		details: "form",
			// 		types: ["geocode", "establishment"],
			// 	});	
			// }


			if(formid > 0){
				var options = {
					map: ".map_canvas",
					location: ['{{ $row["latitude"] }}', '{{ $row["longitude"] }}' ],
					details: "form",					
					types: ["geocode", "establishment"],
				};
				$("#geocomplete").geocomplete(options);
			}

			var country = '{{ $row["country"] }}';	
			var city    = '{{ $row["city"] }}';	
			var zipcode = '{{ $row["zipcode"] }}';	
			var state   = '{{ $row["state"] }}';	
			var address = '{{ $row["address"] }}';	

			// set the country fields
			if(!country){
				$('form#events').find('input[name="country"]').val(country);
			}	
			// set the city fields
			if(!city){
				$('form#events').find('input[name="city"]').val(city);
			}	
			// set the zipcode fields
			if(!zipcode){
				$('form#events').find('input[name="zipcode"]').val(zipcode);
			}
			// set the state fields
			if(!state){
				$('form#events').find('input[name="state"]').val(state);
			}	
			// set the address fields
			if(!address){
				$('form#events').find('input[name="formatted_address"]').val(address);
			}														

		});

		// show preview image
		$(document).on('change','input[name="image"]',function(){
		  readURL(this);
		})
		// render image
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-events img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}
		// show video preview
		$(document).on("change", "input[name='video']", function(evt) {
		  var $source = $('#video_here');
		  $('.video-preview').show();	
		  $source[0].src = URL.createObjectURL(this.files[0]);
		  $source.parent()[0].load();
		});		
		$(document).on("change", ".file_multi_video", function(evt) {
		  var $source = $('#video_here');
		  $source[0].src = URL.createObjectURL(this.files[0]);
		  $source.parent()[0].load();
		});
		var val = $("input[name='need_models']:checked").val();

		// event form validation
		$("#events").validate({
			ignore: "not:hidden",
			rules: {
				'categories[]':{
					required:true,
				},
				title:{
					required:true,
				},
				note:{
					required:true,
				},
				labels:{
					required:true,
				},
				startdate:{
					required:true,
				},
				enddate:{
					required:true,
				},
				link:{
					required:true,
					url: true
				},
				/*image:{
					required:true,
				},*/
				/*video:{
					required:true,
					video:true
				},*/
				email:{
					required:true,
					email: true
				},
				geocomplete:{
					required:true,
				},

				formatted_address:{
					required:true,
				},
				locality:{
					required:true,
				},
				postal_code:{
					required:true,
				},
				administrative_area_level_1:{
					required:true,
				},
				country:{
					required:true,
				},
				mobile:{
					required:true,
					number:true,
					minlength:10,
				},
				need_models:{
					required:true,
				},
				get_ticket:{
					required:true,
					url:true
				},
				entrycost:{
					required:true,
					number:true,
				},
				talent_needed: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
        		accomm_package: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
        		travel_package: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
        		benefit_package: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
        		dress_code: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
        		publishID: {
	            required: {
	                depends: function (element) {
	                    return $("input[name='need_models'][value='yes']").is(":checked");
	                }
	            }
        		},
				

			},
			messages: {
				'categories[]':{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Categories "
				},
				title:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Title"
				},
				note:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Description"
				},
				labels:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Tags"
				},
				startdate:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event StartDate"
				},
				enddate:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event EventDate"
				},
				link:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Website",
					url:"<span><i class='fa fa-warning'></i></span>Please Provide valid URL"	
				},
				// image:{
				// 	required:"Please Event Banner Image"
				// },
				email:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Event Contact Email",
					email:"<span><i class='fa fa-warning'></i></span>Please enter a valid email address"
				},
				geocomplete:{
					required:"<span><i class='fa fa-warning'></i></span>Please Choose an Event Location"
				},

				formatted_address:{
					required:"<span><i class='fa fa-warning'></i></span>Event Address required"
				},	
				locality:{
					required:"<span><i class='fa fa-warning'></i></span>Event Locality required"
				},	
				postal_code:{
					required:"<span><i class='fa fa-warning'></i></span>Event Postal Code is required"
				},	
				administrative_area_level_1:{
					required:"<span><i class='fa fa-warning'></i></span>Event City is Required"
				},	
				country:{
					required:"<span><i class='fa fa-warning'></i></span>Event Country is required"
				},	
				mobile:{
					required:"<span><i class='fa fa-warning'></i></span>Please Provide Contact Number",
					number:"<span><i class='fa fa-warning'></i></span>Please Provide Valid Contact Number"
				},
				  need_models:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Option",
        },
        get_ticket:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Ticket URL",
          url:"<span><i class='fa fa-warning'></i></span>Please Provide valid URL" 
        },
        talent_needed:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Talent Needed",
        },
        travel_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Travel Package",
        },
        benefit_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Benefit Package",
        },
        dress_code:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Dress Code",
        },
         publishID:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Publishes",
        },
        accomm_package:{
          required:"<span><i class='fa fa-warning'></i></span>Please Choose Accommodation Package",
        },
        entrycost:{
          required:"<span><i class='fa fa-warning'></i></span>Please Provide Entry cost",
          number:"<span><i class='fa fa-warning'></i></span>Please Provide Cost in Number"
        },
			},
			submitHandler:function(form){

				form.submit();
			}
		});	
 $('select').on('change', function() {
        $(this).valid();
   });
	$(function(){
		var max = parseInt($("#note").attr("maxlength"));
	  $("#count").text("Characters left: " + max);
		$("#note").keyup(function(e){
			$("#count").text("Characters left: " + (max - $(this).val().length));
	    if($(this).val().length==max)
	    	$("#msg").text("500 words allowed");
			else
	    	$("#msg").text("");
		});

	});	
	
if($("input:radio[name='need_models']").is(":checked") && val=='yes') {
$('.models_types').show();
}
			

	$('input[name="need_models"]').on('ifChecked', function(event){
			// get the selected value
			var Val = $.trim($(this).val());
			if(Val == 'yes'){
				$('.models_types').show();
			}else{
				$(".talent_needed").val('');
				$(".travel_package").val('');
				$(".benefit_package").val('');
				$(".dress_code").val('');
				$(".accomm_package").val('');
				$(".publishID").val('');
				$('.models_types').hide();
			}
		});
	</script>		 
	@stop