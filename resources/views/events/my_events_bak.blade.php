<?php
use App\Library\Markdown;
?>
@include('agent_dash_header')
<div class="magelur_about faq_sec">
 
 @if(empty($rowData))

 <div class="gallery_images wow bounce col-md-12 col-sm-12 col-xs-12">
  <div class="empty_msg_details">
    <div class="empty_msg_blk">
      <div class="heading_blk">
        <div class="heading_details">
          <h1>Gate 404 Error !</h1>
          <h2>Sorry the page you are looking is broken.Our team is working on fixing this problem</h2>
        </div>
        <div class="file_img_blk">
          <div class="file_img_div">
            <img src="images/file_image1.png" alt="file_image" />
          </div>
        </div>
      </div>
      <p>You can search on the bar below or return to home page</p>
      <div class="pages_blk">
        <div class="msg_previous_page">
          <a href="javascript:void(0);"><span>Home</span><img src="images/preview_image1.png"></a>
        </div>
        <div class="msg_next_page">

          <a href="javascript:void(0);"><span>Popular pages</span><img src="images/next_image1.png"></a>
        </div>
      </div>
    </div>
  </div>
</div>

@endif
  
   @if(!empty($rowData))
   	<section class="agent_news_event agent_news_myevents">
   		<div class="container event_news_container">
        <div class="myevents_add col-md-12 col-sm-12 col-xs-12">
        <a href="{{ URL::to('events/update')}}">
        <div class="add_myevents_blk col-md-12 col-sm-12 col-xs-12">
          <div class="add_icon_div col-md-12 col-sm-12 col-xs-12">
          <span class="add_icon_span"><i class="fa fa-plus"></i></span>
          </div>
          <h1>{{ Lang::get('core.add_events') }}</h1>
        </div>
        </a>
        </div>
        <div class="myevents_page_blk">
   			<div class="news_event">
   				<p>{{ ucfirst($agentname) }} {{ Lang::get('core.events') }}</p>
   			</div>

   			<div class="col-md-12 col-sm-12 col-xs-12 event_news_blk">

   				@foreach ($rowData as $row)
   				<div class="event-unit">
   					<div class="col-md-7 col-sm-12 col-xs-12 event_news">
   						<div class="event_news_image">
   							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
   								<img src="{{ asset('uploads/events/'.$row->image) }}" class="img-responsive">
   							</a>
   						</div>
   					</div>

   					<div class="col-md-5 col-sm-12 col-xs-12 event_news">
   						<div class="event_news_content">

   							<a class="event-link" href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">
   								<h1>{{ $row->title }}</h1>
   							</a>	
                <!-- show edit button only for owner & admin -->
                @if(\Session::get('agent_id') == $row->ownerid || \Session::get('user_type') == 'admin'  )
                <div class="event_close_btn">
                  <a href="{{ url('events/update/'.$row->id.'/' )}}">
                  <i class="fa fa-pencil"></i>
                  </a>
                </div>
                @endif

   							@if(strlen($row->note) > 780)
   							<p>{{ mb_substr($row->note,0,600) }}....</p>
   							@else
   							<p>{{ $row->note }}</p>
   							@endif

   							<span>{{ date(" dS M y h:i  A" , strtotime($row->created)) }}</span>
   							<a href="{{ url('events/view/'.$row->id.'/'.$row->uri)}}">{{ Lang::get('core.read_more') }}</a>
   						</div>
   					</div>
   				</div>

   				@endforeach

   			</div>
        </div>
        </div>
   		<div class="pagination_content text-center"> {!! $pagination->render() !!}</div>




   	</section>
    @endif
</div>


    <style>
    .map{
    	display:none;
    }
    </style>