@extends('layouts.app')
@section('content')
<div class="page-content row">
<div class="page-content-wrapper m-t">	 	
<div class="sbox">
	<div class="sbox-title"> 
		<h1>Feedbacks</h1>
	</div>
	<div class="sbox-content"> 	
		<div class="table-responsive" style="min-height:300px;">
			<table class="table table-striped ">
				<thead>
					<tr>
						<th> No </th>  
						<th> Name </th>
						<th> Email </th>
						<th> User type </th>
						<th> Mail Status </th>                                 
						<th> Actions </th> 
					</tr>
				</thead>
				<tbody>  
				<tr id="orderDetails">
        		<td class="number"></td>

        		<td id="name" class="fieldsearch"> <div class="select-style">{!! Form::text('name',"{$name}",array('class'=>'form-control', 'placeholder'=>'Name', )) !!} </div></td>
        		<td id="email" class="fieldsearch"> <div class="select-style">{!! Form::text('email',"{$email}",array('class'=>'form-control', 'placeholder'=>'Email', )) !!} </div></td>
        		<td id="user_type" class="fieldsearch"> <div class="select-style">
						<select name='user_type' class = 'select2 '>
        					<option value="">Select Type</option>
        					<option value="model" {{ $user_type == 'model' ? 'selected' : ''}}>Model</option>
        					<option value="agent" {{ $user_type == 'agent' ? 'selected' : ''}}>Agent</option>

        				</select>
        		</div></td>

        		<td id="status" class="fieldsearch"> 
        			<div class="select-style">
        				<select name='status' class = 'select2 '>
        					<option value="">Select option</option>
        					<option value="send" {{ $status == 'send' ? 'selected' : ''}}>Send</option>
        					<option value="nosend" {{ $status == 'nosend' ? 'selected' : ''}}>Not Send</option>

        				</select>

        			</div>
        		</td>
        		<td class="icons">
        			<input type="hidden"  value="Search">
        			<a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
        			<a href="{{ url('feedback-list') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
        		</td>
        	</tr> 

					{{--*/ $i = ($results->currentPage() - 1) * $results->perPage() + 1;/*--}}
					@foreach($results as $invite)
					<tr>
						<td> {{ $i }} </td>  
						<td> {{$invite->name}} </td>
						<td> {{$invite->email}} </td>
						<td> {{$invite->user_type}} </td>
						<td> {{$invite->mail_status}} </td> 
						<td class="icons">
						    <a href="{{ url('feedback/view/'.$invite->id) }}" type="button" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search" aria-hidden="true"></i></a>  
						</td> 
						<td></td>   
						{{--*/ $i++ /*--}}
						</tr>
						@endforeach 
						                           
					</tbody>
				</table>
				<input type="hidden" name="md" value="" />
				@if(count($results) == 0)
				<p class="nodata-txt">Data not available ! </p>	 
				@endif	
			</div>
			<div class="table-footer">
				<div class="row">
					<div class="col-sm-5">
						<div class="table-actions form_footer">
							{!! Form::open(array('url'=>'feedback-list','method'=>'GET')) !!}
							{{--*/ $pages = array(5,10,20,30,50) /*--}}
							{{--*/ $orders = array('asc','desc') /*--}}
							<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
								<option value=""> {{ Lang::get('core.grid_page') }} </option>
								@foreach($pages as $p)
								<option value="{{ $p }}" 
								@if(isset($pager['rows']) && $pager['rows'] == $p) 
								selected="selected"
								@endif	
								>{{ $p }}</option>
								@endforeach
							</select>
							<select name="sort" data-placeholder="Sort" class="select-alt">
								<option value=""> Sort </option>	    
								<option value="name" @if(isset($pager['sort']) && $pager['sort'] == 'name') 
								selected="selected"
								@endif>Name</option>
								<option value="email" @if(isset($pager['sort']) && $pager['sort'] == 'email') 
								selected="selected"
								@endif>Email</option>
								<option value="user_type" @if(isset($pager['sort']) && $pager['sort'] == 'user_type') 
								selected="selected"
								@endif>User Type</option>
								<option value="mail_status" @if(isset($pager['sort']) && $pager['sort'] == 'mail_status') 
								selected="selected"
								@endif>Mail Status</option>
							</select>	
							<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
								<option value=""> {{ Lang::get('core.grid_order') }}</option>
								@foreach($orders as $o)
								<option value="{{ $o }}"
								@if(isset($pager['order']) && $pager['order'] == $o)
								selected="selected"
								@endif	
								>{{ ucwords($o) }}</option>
								@endforeach
							</select>	
							<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	

							{!! Form::close() !!}
						</div>					
					</div>
					<div class="col-sm-3">
						<p class="text-center" style=" padding: 25px 0">
							Total : <b>{{ $results->total() }}</b>
						</p>		
					</div>
					<div class="col-sm-4">	
						<div class="col-md-12 col-sm-12 col-xs-12">  
							<div class="pagination_s"> 
								{!! str_replace('/?', '?', $results->appends(Input::except('page'))->render()) !!}
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
	</div>	  
</div>	

@stop