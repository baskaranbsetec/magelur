@extends('layouts.app')

@section('content')
<div class="page-content row">
 	<div class="page-content-wrapper m-t">   
		<div class="sbox ">
			<div class="sbox-title">
				<div class="sbox-tools pull-left" >
				<a href="{{ url('feedback-list') }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa  fa-arrow-left"></i></a>
			   		Feedback View
				</div>	

				<div class="sbox-tools " >
				</div>
			</div>
			<div class="sbox-content" > 	

				<table class="table table-striped table-bordered" >
					<tbody>	
						<tr>
							<td width='30%' class='label-view text-right'>User name</td>
							<td>@if($feedback->name) {{ $feedback->name}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>email</td>
							<td>@if($feedback->email) {{ $feedback->email}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>User Type</td>
							<td>@if($feedback->user_type) {{ $feedback->user_type}} @else Not Specified @endif</td>
						</tr>

						<tr>
							<td width='30%' class='label-view text-right'>Suggestion</td>
							<td>@if($feedback->suggestion) {{ $feedback->suggestion}} @else Not Specified @endif</td>
						</tr>
						<tr>
							<td width='30%' class='label-view text-right'>Rating</td>
							<td>@if($feedback->star_rate) {{ $feedback->star_rate.' Star(s)'}} @else Not Specified @endif</td>
						</tr>
					</tbody>	
				</table>   
			</div>
		</div>	
	</div>
</div>
	  
@stop