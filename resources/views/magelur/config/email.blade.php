
 @extends('layouts.app')

@section('content')
  <div class="page-content row">


 <div class="page-content-wrapper">  
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('magelur.config.tab')	
<div class="tab-content m-t">
	  <div class="tab-pane active use-padding" id="info">	
	 {!! Form::open(array('url'=>'magelur/config/email/', 'class'=>'form-vertical row')) !!}
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> {{ Lang::get('core.registernew') }}  </div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="regEmail" class="form-control input-sm  markItUp">{{ $regEmail }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
		


</div> 


	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {{ Lang::get('core.forgotpassword') }}</div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="resetEmail" class="form-control input-sm markItUp">{{ $resetEmail }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 	
 </div>

 <div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> Contact-us Email  </div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="contactusEmail" class="form-control input-sm  markItUp">{{ $contactusEmail }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
		


</div>
 <div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> Invite Friend  </div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="invite_friend" class="form-control input-sm  markItUp">{{ $invite_friend }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
		


</div>
<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> Feedback Suggestion  </div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="feed_back" class="form-control input-sm  markItUp">{{ $feed_back }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
</div>
<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> Admin Email for Users</div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="email_user" class="form-control input-sm  markItUp">{{ $email_user }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
</div>
<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> Subscribe Email</div>
			<div class="sbox-content"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" name="sub_email" class="form-control input-sm  markItUp">{{ $sub_email }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
</div>
 {!! Form::close() !!}
</div>
</div>
</div>
</div>
@stop





