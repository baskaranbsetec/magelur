
 @extends('layouts.app')

@section('content')
  <div class="page-content row">


 <div class="page-content-wrapper">  
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('magelur.config.tab')	
<div class="tab-content m-t">
	  <div class="tab-pane active use-padding" id="info">	
{!! Form::open(array('url'=>'updatePaymentsettings', 'class'=>'form-vertical row')) !!}
<div class="col-sm-6 animated fadeInRight ">
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Stripe Secret Key</label>
			<div class="col-md-8">
			<input name="secret_key" type="text" id="secret_key" class="form-control input-sm" required  value="{{ $secret_key }}" />  
			 </div> 
		  </div><br><br>
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">Stripe Publishable Key</label>
			<div class="col-md-8">
			<input name="publishable_key" type="text" id="publishable_key" class="form-control input-sm" required  value="{{ $publishable_key }}" />  
			 </div> 
		  </div><br><br>
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> <br><br> 
		</div>

 {!! Form::close() !!}
</div>
</div>
</div>
</div>

@stop





