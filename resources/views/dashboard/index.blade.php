@extends('layouts.app')
@section('content')
 <link href="{{ asset('frontend/default/css/agency.css') }}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('magelur/js/plugins/chartjs/Chart.min.js') }}"></script>
<div class="page-content row ">
	<div class="page-content-wrapper m-t">  	
        <div class="col-md-12 col-sm-12 col-xs-12 admin_dashboard_page">
        <div class="col-md-12 col-sm-12 col-xs-12 admin_dashboard_eventpage">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_leftside">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_header">
                <div class="event_header_left">
                <h1>Events</h1>
                <h2>From your Agent Management</h2>
                </div>
                <div class="event_header_right">
               <a href="{{url('events')}}"> <p>More Events</p></a>
                               
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_content">
            <?php $c=1; ?>
                @foreach($events as $event)
                
                <div class="magelur_event_content col-md-12 col-sm-12 col-xs-12">
                <div class="admin_event_views">
                <h1>#{{ $c++ }}</h1>
                <!--<p class="view_icon_blk"><i class="fa fa-eye"></i></p>-->
                </div>
                <div class="event_right_content">
                <a href="{{url('events/update/'.$event->id)}}">

                <h1>{{ substr($event->title, 0, 30) }}</h1>
                <p>{{ substr($event->note, 0, 50) }} .... </p>
                </a>
                </div>
                </div>
                
                @endforeach


              
            </div>

            </div>
            <!-- <div class="col-md-5 col-sm-5 col-xs-12 admin_event_rightside">
            
            </div> -->
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 admin_dashboard_eventpage admin_dashboard_message">
            <div class="col-md-6 col-sm-6 col-xs-12 admin_msg_leftside">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_header">
                <div class="event_header_left">
                <h1>Messages</h1>
                <h2>Current Conversations</h2>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 admin_msg_content">
                <!-- <div class="add_user_blk">
                
                </div> -->
                @foreach($message as $value)
                <div class="magelur_msg_content col-md-12 col-sm-12 col-xs-12">
                <div class="admin_profile_image">
                @if($value->recipient_type=='model')
                {!! SiteHelpers::user_avatar($value->recipient,'model') !!}
                @else
                {!! SiteHelpers::user_avatar($value->recipient,'agent') !!}
                @endif
                <!-- {!! SiteHelpers::useravatar($value->id) !!}  --> 
                <!--<span class="user_status_icon user_online_icon"></span>-->
                </div>
                <div class="msg_right_content">
                <h1>
                    @if($value->recipient_type=='model')
                    <!-- {{ $value->recipient_type}}  -->
                      {!! SiteHelpers::getUsernamebyId($value->recipient,'model') !!}
                    @else
                    <!-- {{ $value->recipient_type}} -->
                      {!! SiteHelpers::getUsernamebyId($value->recipient,'agent') !!}

                    @endif

                </h1>                
                <h2>{{ $value->message }}</h2>
                <h3>{!! SiteHelpers::time_ago($value->updatedOn) !!} ago</h3>
                </div>
                </div>
                @endforeach
              
            </div>
            </div>
             <div class="col-md-6 col-sm-6 col-xs-12 admin_msg_leftside">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_header">
                <div class="event_header_left">
                <h1>Blogs</h1>
                <h2>Your Current Blogs </h2>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 admin_msg_content">
               <!--  <div class="add_user_blk">
                <a href="{{url('blogs')}}"> </a>
                </div> -->
                 <div class="event_header_right">
                    <a href="{{url('blogs')}}"> <p>More blogs</p></a>
                               
                </div>
                @foreach($Blogs as $value)
                <div class="magelur_msg_content col-md-12 col-sm-12 col-xs-12">
                <div class="admin_profile_image">
                
                <!--<span class="user_status_icon user_online_icon"></span>-->
                </div>
                <a href="{{url('blogs/update/'.$value->id)}}">
                <div class="msg_right_content">
                <h1>{{ substr($value->title, 0, 10) }}</h1>                
                <h2>{{ substr($value->note, 0, 70) }}</h2>
                </div>
                </a>
                </div>
                @endforeach
              
            </div>
            </div>
        </div>
 <div class="col-md-12 col-sm-12 col-xs-12 admin_dashboard_eventpage admin_dashboard_message">
            <div class="col-md-4 col-sm-4 col-xs-12 admin_msg_leftside">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_header">
                <div class="event_header_left stoped">
                <h1>Photos</h1>
                <!-- <h2>Current Conversations</h2> -->
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 admin_msg_content">
                <!-- <div class="add_user_blk">
                    <a href="{{url('imagesnew')}}"> <i class="fa fa-plus"></i></a>
                </div> -->
                <div class="event_header_right">
                    <a href="{{url('imagesnew')}}"> <p>More photos</p></a>
                               
                </div>
                <ul class="multimedia clearfix">
                    <li class="">
                        <div class="trumb"> 
                            <ul class="multi_three_rows clearfix">
                                @foreach($gallery as $gal)
                                @if($gal->Status=='Active')
                                <li>
                                    {{--*/ $url_id =urlencode( base64_encode($gal->ID));/*--}}
                                    <a href="{{url('/imagesnew/update/'.$gal->ID)}}"><img src="{!! \SiteHelpers::user_albums($gal->ID,'_small',$gal->Ext) !!}"></a>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>    
                    </li>
                </ul>
            </div>
            </div>
             <div class="col-md-8 col-sm-8 col-xs-12 admin_msg_leftside">
            <div class="col-md-12 col-sm-12 col-xs-12 admin_event_header">
                <div class="event_header_left stoped">
                <h1>Videos</h1>
                <!-- <h2>Current Conversations</h2> -->
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 admin_msg_content">
                <!-- <div class="add_user_blk">
                <a href="{{url('videosnew')}}"> <i class="fa fa-plus"></i></a>
                </div> -->
                <div class="event_header_right">
                    <a href="{{url('videosnew')}}"> <p>More videos</p></a>
                               
                </div>
            {{--*/$files =  '/uploads/video/'.$videos->video_id.'/video.'.$videos->type ;/*--}}
                <ul class="lastblkes clearfix">
                    <li class="">
                        <div class="">  
                            <div class="vide_pat">
                                @if (!is_null($videos->vimeo_id))
                                <!-- Vimeo Video -->
                                <iframe src="https://player.vimeo.com/video/{{ $videos->vimeo_id }}" width="100%" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                @elseif (!is_null($videos->dailymotion_id))
                                <!-- Dailymotion Video -->
                                <iframe frameborder="0" width="100%" height="380" src="//www.dailymotion.com/embed/video/{{ $videos->dailymotion_id }}" allowfullscreen></iframe>
                                @elseif (!is_null($videos->youtube_id))
                                <iframe frameborder="0" width="100%" height="380" src="https://www.youtube.com/embed/{{ $videos->youtube_id }}" allowfullscreen></iframe>
                                @else
                                <iframe frameborder="0" width="100%" height="380" src="{{ $videos->video_path}}?rel=0" allowfullscreen></iframe>
                                
                                @endif
                            </div>
                        </div>    
                    </li>
                </ul>
            </div>
            </div>
        </div>




</div>
</div>		
</div>



@stop