<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="modal msg_popup_model fade" id="compose-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="max-width:800px;width:90%;">
	  <div class="modal-content">
		<div class="modal-header bg-default">
			
			<button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true" ><span>&times;</span></button>
			<h4 class="modal-titles">Compose message</h4>
		</div>
		<div class="load_notify"></div>
		<div class="modal-body" id="compose-modal-content">
			 {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<input type="hidden" name="typee" class="type" value="{{$type}}" />

				<div class="form-group form-group_full">
				<label for="mail_to" class="col-xs-2">Send To</label>
				<div class="col-xs-10">
				<div>
				<input type='hidden' name='recipient_session' value="{{Session::get('user_type')}}" class='form-control' >

				<input type='hidden' name='recipient_typee' id="recipient_type" value='' class='form-control'>
				<input type='hidden' name='recipient_type' id="recipient_type" value='' class='form-control'>
							
				{!! Form::text('recipient1','',array('class'=>'job name form-control','placeholder'=>'Select user','id'=>'recipient1','required'=>'')) !!}
				{!! Form::hidden('recipient','',array('class'=>'job id form-control','id'=>'recipient')) !!}  
				</div>
				<div id='auto-content' style='display:none;'><i class="fa fa-warning"></i>&nbsp;No user found!</div>
				<span class="err_recipient err_span"></span>
				</div>
				</div>
				<div class="form-group form-group_full">
					<label for="mail_subject" class="col-xs-2">Subject</label>
					<div class="col-xs-10">
				{!! Form::select('subject',array('' => 'Choose Subject') +\SiteHelpers::getTableValues('message_subject'),'',array('class' => 'form-control subject','id' => 'subject','required'=>'')) !!}
					<span class="err_subject err_span"></span>

					</div>
				</div>
				<div class="form-group form-group_full form_text_editor">
					<label for="mail_subject" class="col-xs-2">Message</label>
					<div class="col-xs-10">
						<div><textarea name='message' rows='3' style="width:100%;" class='form-control theme_textarea' placeholder="Message Here"></textarea></div>
						<span class="error err_span"></span>
					</div>
				</div>
				<div class="form-group form-group_full">
				<label for="mail_subject" class="col-xs-2">Attach files</label>
				<div class="col-xs-10">
				<input  type="file" class="form-control msgpopup_browse_btn" name="images[]" id='images' accept=".png,.jpg,.jpeg,.gif" placeholder="" multiple style="color:transparent;">
				<div id='theDiv'></div>
				<input type="hidden" name="duplicate" value='' class='duplicate'>
				<input type="hidden" name="remaining" value='' class='remaining'>
			    <input type="hidden" name="deleted" value='' class='deleted'>

				<span class="attach_files_blk">[Allow multiple images]</span>
				<span class="err_image" style="color:red"></span>

				</div>
				</div>
				<div class="form-group form-group_full">
				<!-- <label class="col-xs-2"></label> -->
					<div class="col-xs-12 msg_button_blk">
						{!! Form::hidden('draft', '0' )  !!}
						{!! Form::hidden('messageid', '' )  !!}
						<button type="submit" name="submit" data-type="inboxx" class="btn-sm sendbtn" ><span><i class="fa  fa-envelope "></i> Send Message</span></button>
						<button type="submit" name="submitdraft" data-type="draftt" class="btn-sm savedraft" ><span><i class="fa  fa-save "></i>Save as draft</span></button>
					</div>
				</div>
								<div class="sending_loadergs"></div>

			 {!! Form::close() !!}
		</div>

	  </div>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function(){
// var count = $('.table-hover tbody tr.mail-unread').length;
//        if(count>0){
//          $("#count").html(count);
//        }
	
	
	$(function(){
		tinymce.init({	
			mode : "specific_textareas",
			editor_selector : "mceEditor",
			theme : "advanced",
			theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
			// theme_advanced_toolbar_location : "top",
			// theme_advanced_toolbar_align : "left",
			// theme_advanced_statusbar_location : "bottom",
		 });	
	});
	
	$('.sendbtn').click(function(){
		$('.type').val(0);
	});
	$('.savedraft').click(function(){
		$('.type').val(3);
	});
	function tinyClean(value) {
	value = value.replace(/&nbsp;/ig, ' ');
	value = value.replace(/\s\s+/g, ' ');
	if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
		value = '';
	}
	return value;
}
   

   $(".sendMessage").validate({
   	
     ignore: "not:hidden",
     rules: {
			// recipient1:{
			// required:true
			// },
			 recipient1:{
          required:true,
        //   remote:{
        //   url:"{!! url('agent/checkuniqueName')!!}",
        //   type:'post',
        //   data:{
        //     recipient1:function(){
        //       return $('#recipient1').val();
        //     },
            
        //   }
        // }
        },
			subject:{
			required:true
			},
			message:{
			required:true
			},

		},
		messages: {
			recipient1:  
			{
			required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Recipient Field is required</span>',
			},
			subject:  
			{
			required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Subject Field is required</span>',
			},
			message:  
			{
			required:'<span class="error err_span"><i class="fa fa-warning msg_error_icon"></i>Message Field is required</span>',
			}
		},
      
        submitHandler: function(form) {

    var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
    var typee = $('[name="typee"]').val();
    var recipients = $('[name="recipient1"]').val();
    var subject = $('[name="subject"]').val();
    var image = $('input[type="file"]').val();
    var img = $("#start").find('.viewMessageTitles').val();

    // if(subject==''){
    //   $('.err_subject').html("<i class='fa fa-warning msg_error_icon'></i>Subject Field is required");
    // }
    // else{
    //   $('.err_subject').hide();

    // }
    if(value==''){
      $('.error').html("<i class='fa fa-warning msg_error_icon'></i>Message Field is required");
    }else{
      $('.error').hide();
    }
    
    if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !=''  && value !=''){
      $('.sendbtn').attr('disabled','disabled');
      $('.savedraft').attr('disabled','disabled');
      var recipient = $('[name="recipient"]').val();
      var url = $(this).attr('action');
      tinyMCE.triggerSave();

        toastr.success("Message sent Successfully");
         $('#compose-modal').modal("toggle"); 
         form.submit();
	}
           
        },

        });
	
	$('.ajaxMessageUpdate').submit(function(){
		var starred = $(this).children('[name="starred"]').val();
		if(starred == '1'){
			$(this).children('[name="starred"]').val('0');
		} else {
			$(this).children('[name="starred"]').val('1');
		}
		var url = $(this).attr('action');
			$(this).children('.msgstar').toggleClass('active');
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				var star =$('#cstar').attr('data-star');
				var starr;
				if(response==1){
					var s = parseInt($('#cstar').attr('data-star')) + parseInt(response);
					$('#cstar').attr('data-star', s);
					starr =parseInt(star)+parseInt(response);
										
				}else if(response==0){
					var s = parseInt($('#cstar').attr('data-star')) - parseInt(1);	
					 $('#cstar').attr('data-star', s);		 
					 starr =parseInt(star)-parseInt(1);	
				}
				 $('#cstar').html(starr);
				
			}
		});
		return false;
	});
	$('.msgstar').click(function(){
		$(this).parent('form').children('[name="update_function"]').val('starred');
		$(this).parent('form').submit();
	});
	
	$('.sendbtn').click(function(){
		$('[name="draft"]').val('0');
	});
	$('.savedraft').click(function(){
		$('[name="draft"]').val('1');
	});
	
	$('#composeMessage').click(function(){
	if($('input[name="recipient_session"]').val() == 'agent')
	{
	$('input[name="recipient_typee"]').val('model')
	}
	else{
	$('input[name="recipient_typee"]').val('agent')
	}
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="messageid"]').val('');
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	$('.gotodataMessageDiv').click(function(){
		$('.viewMessageDiv').addClass('hidden');
		$('.dataMessageDiv').slideDown();
	});
	$('.sendReplyForward').click(function(){
		var msgid = $(this).attr('data-id');
		var msgtype = $(this).attr('data-type');
		var issender = $(this).attr('data-sender');
		var option = $(this).attr('data-btn');
		var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
				var entryby = $('#viewMessageDiv_'+msgid).find('.senderid').val();

		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="messageid"]').val('');
			if(option == 'reply'){

				var user_type = $('#viewMessageDiv_'+msgid).find('.viewMessageusertype').text();
				if($('[name="typee"]').val() == '0' && user_type =='2'){
					$('[name="recipient_type"]').val('agent')
				}
				else if($('[name="typee"]').val() == '0' && user_type =='1'){
					$('[name="recipient_type"]').val('model')
				}
				
				else{
				$('#compose-modal').find('[name="recipient_type"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagerecptype').text());
				}
	// alert($('[name="typee"]').val())

			if(msgtype == 0) {

				$('#recipient').val(entryby);
				// $('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
				$('#compose-modal').find('[name="recipient1"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagename').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				
				// $('#recipient').val(recipientid); 
				} else if(msgtype == 2) {
				$('#recipient').val(recipientid);	
				$('#compose-modal').find('[name="recipient1"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagename').text());
				$('#compose-modal').find('[name="recipient_type"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagerecptype').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				} else if(msgtype == 1 || msgtype == 5) {
				if(issender == 1){
				$('#recipient').val(recipientid);	
				$('#compose-modal').find('[name="recipient1"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
				} else {
				// $('#compose-modal').find('[name="recipient"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitles').text());
				$('#compose-modal').find('[name="recipient1"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagename').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
				$('#recipient').val(recipientid);	
				$('#compose-modal').find('[name="recipient_type"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagerecptype').text());		
					}
			}
			} else if(option == 'forward'){
				$('#theDiv').empty();
				$('.drop').show();
				$('.drop1').hide();
				if(msgtype == 0) {
				$('#recipient').val(entryby);
				}
				else{
					$('#recipient').val(recipientid);
				}
					
				$('#compose-modal').find('[name="recipient1"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagename').text());
				$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
					var user_type = $('#viewMessageDiv_'+msgid).find('.viewMessageusertype').text();
				if($('[name="typee"]').val() == '0' && user_type =='2'){
					$('[name="recipient_type"]').val('agent')
				}
				else if($('[name="typee"]').val() == '0' && user_type =='1'){
					$('[name="recipient_type"]').val('model')
				}
				
				else{
				$('#compose-modal').find('[name="recipient_type"]').val($('#viewMessageDiv_'+msgid).find('.viewMessagerecptype').text());
				}
				
				$('#compose-modal').find('[name="message"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageContent').text());
				var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
				var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
				var img = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
				var array =$('#viewMessageDiv_'+msgid).find('.viewMessageimage').val().split(",");
				$.each(array,function(i){
				var targetPath = "uploads/message/"+array[i]+"";
				if(img){
				$('#theDiv').prepend('<div class="image-delete" data-id='+recipientid+' data-msgid='+msg_id+' data-src='+array[i]+' data-path='+targetPath+' onClick=deleteImage(this,"' +targetPath+ '")><img border="0" width="50" class="fancyboxa img-circle" style="margin-right:2px;" src="{{ asset("uploads/message/") }}/'+array[i]+'"/><span style="cursor: pointer;">X</span></div>');
				$('input.duplicate').val($('#viewMessageDiv_'+msgid).find('.viewMessageimage').val());
				}
				else{
				$('#theDiv').hide();

				}
				});
			}
		
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});

$('tr').click(function(e){
e.preventDefault();
$('tr').removeClass('mail-click'); // removes all highlights from tr's
$(this).addClass('mail-click'); // adds the highlight to this row
});
	$('.dataMessage tr .td-clickable').click(function(){
		// $('table tr').addClass('mail-click');
		var member="<?php echo $membership; ?>";
		var agent_id="<?php echo \Session::get('agent_id') ?>";
		var msgtype = $(this).parent('tr').find('td:nth-child(1)').attr('data-type');
		if(agent_id && !member && msgtype == '0')
		{
		swal("Upgrade your membership to view mails !");
		}
		else
		{
		var msgid = $(this).parent('tr').find('td:nth-child(1)').attr('data-id');
		var msgtype = $(this).parent('tr').find('td:nth-child(1)').attr('data-type');
		if(msgtype == '3'){

			$('.sendMessage')[0].reset();
			// $('#compose-modal').find('[name="recipient"]').select2('val','');
			$('#compose-modal').find('[name="messageid"]').val('');
			
			var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
			var subjectid = $('#viewMessageDiv_'+msgid).find('.subj_id').val();
			var parti_mes = $('#viewMessageDiv_'+msgid).find('.parti_mes').val();
			var parti_recip = $('#viewMessageDiv_'+msgid).find('.parti_recip').val();
			var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
			var user_type_rec = $('#viewMessageDiv_'+msgid).find('.recip_type').val();
				
			$('.subject').val(subjectid); 
			$('#recipient').val(recipientid); //alert(recipientid);
			$('#compose-modal').find('[name="messageid"]').val(msg_id);
			$('#compose-modal').find('[name="message"]').val(parti_mes);
			$('#compose-modal').find('[name="recipient1"]').val(parti_recip);//alert(parti_recip);
			$('#compose-modal').find('[name="recipient_type"]').val(user_type_rec);
			var img = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
			// alert(img[i]);
			var array =$('#viewMessageDiv_'+msgid).find('.viewMessageimage').val().split(",");
			$.each(array,function(i){
			// alert(array[i]);
			var targetPath = "uploads/message/"+array[i]+"";
			// alert(targetPath);
			if(img){
			$('#theDiv').prepend('<div class="image-delete" data-id='+recipientid+' data-msgid='+msg_id+' data-src='+array[i]+' data-path='+targetPath+' onClick=deleteImage(this,"' +targetPath+ '")><img border="0" width="50" class="fancyboxa img-circle" style="margin-right:2px;" src="{{ asset("uploads/message/") }}/'+array[i]+'"/><span style="cursor: pointer;">X</span></div>');
			// $('input.duplicate').val($('#viewMessageDiv_'+msgid).find('.viewMessageimage').val());
			}
			else{
			$('#theDiv').hide();

			}
			});


			$('.sendbtn').removeAttr('disabled');
			$('.savedraft').removeAttr('disabled');
			$('#compose-modal').modal({
				backdrop: 'static',
				keyboard: false
			});


		} else {
			$('div.viewMessageDiv').addClass('hidden'); 
// $(this).addClass('hidden');
			if($(this).parent('tr').hasClass('mail-unread')){
				$(this).parent('tr').find('form').children('[name="update_function"]').val('read');
				// $(this).parent('tr').find('form').submit();
				$(this).parent('tr').attr('class','mail');
			}
			$('.dataMessageDiv').hide();
			$('#viewMessageDiv_'+msgid).removeClass('hidden');
		}
	}
	});
	$('.ajaxGroupMessageUpdate').submit(function(){
		var url = $(this).attr('action');
		var ids = $(this).children('[name="msg_ids"]').val();
		var message_ids = ids.split(',');
		var typee=$('[name="type"]').val();

		if($(this).children('[name="group_update_function"]').val() == 'delete' || $(this).children('[name="group_update_function"]').val() == 'deleteTrash'){
			jQuery.each(message_ids, function(index, item) {
				$('#msg_'+item).remove();
			});
			$('.gotodataMessageDiv').trigger('click');
		} else if($(this).children('[name="group_update_function"]').val() == 'label'){
			var label_name = $(this).children('[name="label_name"]').val();
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text(label_name);
			});
		} else if($(this).children('[name="group_update_function"]').val() == 'removelabel'){
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text('');
			});
		}
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				$('.ajaxGroupMessageUpdate')[0].reset();
				if(response!='label')
				{
					var trash = $('#ctrash').attr('data-trash');
					var total =$('#page_total').attr('data-total');
					var res = parseInt(trash)+parseInt(response);
					//$('#page_total').html(res);
					$('#ctrash').html(res);
					if(typee==0){
						var inbox =$('#cinbox').attr('data-inbox');
						var inboxx = parseInt(inbox)-parseInt(response);					
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cinbox').attr('data-inbox',inboxx).text(inboxx);
					}else if(typee==1){
						var star =$('#cstar').attr('data-star');
						var starr =parseInt(star)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cstar').attr('data-star',starr).text(starr);
						
					}else if(typee==2){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#csent').attr('data-sent',sentt).text(sentt);
						
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cdraft').attr('data-draft',draftt).text(draftt);
					}else if(typee==4){
						trash = $('#ctrash').attr('data-trash');
						res = parseInt(trash)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#ctrash').attr('data-trash',res).text(res);
					}
				}
				
			}
		});
		return false;
	});
	$('#deleteMessage').click(function(){
		var msg_ids = '';
		var is_sender = '';

		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
					
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('#deleteFromTrash').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to remove selected messages permanently?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('deleteTrash');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('.singleMessageDelete').click(function(){

		var msg_ids = $(this).attr('data-id');
		var is_sender = $(this).attr('data-sender');
		if (msg_ids != '' && is_sender != '') {
			if (confirm("Are you sure you want to move selected messages into Trash?")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
		


});	


// $('#search').on('keyup',function(){
// var value=$(this).val();
// console.log(value);
// $.ajax({
// type:'get',
// url: "{{URL::to('show')}}",
// data:{'search':value},
// success:function(data){
// $('tbody').html(data);
// }
// });
// })


$(document).ready(function(){
	$('#search').on('keyup',function(){
		var searchTerm = $(this).val().toLowerCase();
		var resultCount = 0;
		$('#userTbl tbody tr').each(function(){
			var lineStr = $(this).text().toLowerCase();
			if(lineStr.indexOf(searchTerm) === -1){
				$(this).hide();
				$('.search_msg').hide();
			}else{
				$('.search_msg').hide();
				$(this).show();
				resultCount++;
			}
		});


		if (resultCount == 0) {
			$('.search_msg').show();
			$('.no_msg').hide();
			
		}
	});
});





		$(".fancybox").fancybox({
    afterLoad: function() {
        this.title = '<i class="fa fa-download" aria-hidden="true"><a href="' + this.href + '" download>Download</a></i>' + this.title;
    },
    helpers : {
        title: {
            type: 'inside'
        }
    }
});

	



						
    $('.rader_div').slimScroll({
        height: '86px',
        width: '100%',
        allowPageScroll :true
        
    });  
     $('.msg_blk_slimscroll').slimScroll({
        height: 'auto',
        width: '100%',
        allowPageScroll :true
        
    });    
   $(document).ready(function () {
        $('.close').click(function () {
            $('#auto-content').hide();
			$('#theDiv').empty();
			$('#theDiv').show();
			$(".pip").remove();
			$('label.error').text('');
			$("ul.dropdown-menu").css("display","none")



        });
    });

var removeCommaSeperatedValue = function(list, value, separator) {
  separator = separator || ",";
  var values = list.split(separator);
  for(var i = 0 ; i < values.length ; i++) {
    if(values[i] == value) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }
  return list;
}

function deleteImage(ele,path) {
	//  get imagename
	var imageName = $(ele).data("src");
	var id        = $(ele).data("id");
	var msgid     = $(ele).data("msgid");

	// console.log(ele)
	// get all messages
	var allImages = $('#viewMessageDiv_'+msgid).find('.viewMessageimage').val();
	// Removed images

	var updateImages = removeCommaSeperatedValue(allImages,imageName);
	 // console.log(imageName+',')
	$('input.viewMessageimage_'+msgid).val(updateImages);
	//console.log($('input.viewMessageimage_'+msgid))

	// Save remaining values
	$('input.remaining').val(updateImages);
	// console.log($('input.remaining').val(updateImages))
	// Save deleted values
	$('input.deleted').val(imageName);

// console.log($('input.deleted').val(imageName))
	// return;

	$(ele).addClass('click');
	$('.click').remove();
	
}
	
$(document).ready(function(){

var param1var = getQueryVariable("msg_id");
$('#viewMessageDiv_'+param1var).removeClass('hidden');
function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];

    }
  } 

  //alert(' Variable ' + variable + ' not found');

}
});
if ($(window).width() < 767) {
$(function() {
	$(".gotodataMessageDiv").click(function(e) {
	    e.preventDefault();
	    $('html, body').animate({
	      scrollTop: $("div.left_sht_msg").offset().top
	    }, 1000);
  });
  // Generic selector to be used anywhere
  $(".dataMessage tr .td-clickable").click(function(e) {
	// Get the id dynamically
	var msgid = $(this).parent('tr').find('td:nth-child(1)').attr('data-id');
	// Prevent href=“#” link from changing the URL hash (optional)
	e.preventDefault();
	// Animate scroll to destination
	$('html, body').animate({
	scrollTop: $("div.msg_right_box#"+msgid).offset().top
	}, 1000);
  });
});
}
$('#images').on('change', function(e) {
var files = e.target.files,
        filesLength = files.length;
var ext = $('#images').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['png','jpg','jpeg']) == -1 ) {
    swal("File format is invalid!");
    $('#images').val(null);

}	
// if(filesLength>=10)
// {
// 	swal("Allow only 10 images");
// 	 $('#images').val(null);
// }
})

$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#images").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\ img-circle\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/ border='0' width='50' class='fancyboxa img-circle' style='margin-right:2px;'>" +
            "<span class=\"remove\" style='cursor: pointer;'>X</span>" +
            "</span>").insertAfter("#images");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
</script>	

 @if(Session::get('msgstatus') =='success')
      @if(Session::has('messagetext'))
      <script>
      toastr.success("{!! Session::get('messagetext') !!}");
      </script>
      @endif
      @else
      @if(Session::has('messagetext'))
      <script>
      toastr.error("{!! Session::get('messagetext') !!}");
      </script>
      @endif
      @endif