

		 {!! Form::open(array('url'=>'imagesnew/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
							<fieldset><legend> Imagesnew</legend>
										
										  <div class="form-group  " >
											<label for="ID" class=" control-label col-md-4 text-left"> ID </label>
											<div class="col-md-7">
											  <input  type='text' name='ID' id='ID' value='{{ $row['ID'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="OwnerId" class=" control-label col-md-4 text-left"> OwnerId </label>
											<div class="col-md-7">
											  <input  type='text' name='OwnerId' id='OwnerId' value='{{ $row['OwnerId'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="AlbumId" class=" control-label col-md-4 text-left"> AlbumId </label>
											<div class="col-md-7">
											  <input  type='text' name='AlbumId' id='AlbumId' value='{{ $row['AlbumId'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="User Type" class=" control-label col-md-4 text-left"> User Type </label>
											<div class="col-md-7">
											  <input  type='text' name='user_type' id='user_type' value='{{ $row['user_type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="MediaUri" class=" control-label col-md-4 text-left"> MediaUri </label>
											<div class="col-md-7">
											  <input  type='text' name='MediaUri' id='MediaUri' value='{{ $row['MediaUri'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
											<div class="col-md-7">
											  <input  type='text' name='Title' id='Title' value='{{ $row['Title'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Description" class=" control-label col-md-4 text-left"> Description </label>
											<div class="col-md-7">
											  <textarea name='Description' rows='5' id='Description' class='form-control '  
					           >{{ $row['Description'] }}</textarea> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Tags" class=" control-label col-md-4 text-left"> Tags </label>
											<div class="col-md-7">
											  <textarea name='Tags' rows='5' id='Tags' class='form-control '  
					           >{{ $row['Tags'] }}</textarea> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Ext" class=" control-label col-md-4 text-left"> Ext </label>
											<div class="col-md-7">
											  <input  type='text' name='Ext' id='Ext' value='{{ $row['Ext'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Type" class=" control-label col-md-4 text-left"> Type </label>
											<div class="col-md-7">
											  <input  type='text' name='Type' id='Type' value='{{ $row['Type'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
											<div class="col-md-7">
											  <input  type='text' name='Status' id='Status' value='{{ $row['Status'] }}' 
							     class='form-control ' /> 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="LastEdit" class=" control-label col-md-4 text-left"> LastEdit </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('LastEdit', $row['LastEdit'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> 					
										  <div class="form-group  " >
											<label for="Created" class=" control-label col-md-4 text-left"> Created </label>
											<div class="col-md-7">
											  
					<div class="input-group m-b" style="width:150px !important;">
						{!! Form::text('Created', $row['Created'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					 
											 </div> 
											 <div class="col-md-1">
											 	
											 </div>
										  </div> </fieldset>
				</div>
				
				

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
