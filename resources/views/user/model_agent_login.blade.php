
<script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('magelur/js/valid.js')}}"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style type="text/css">

.alert {
    padding: 15px;
    margin-bottom: 15px;
    margin-top: -60px;
    border: 1px solid transparent;
    border-radius: 4px;
}
.alert_danger {
    /* background-color: #151515; */
    /* border-color: #151515; */
    color: #ffb524;
    top: -8px;
    font-size: 20px;
    font-family: "Lato-Regular";
    color: #bd8414;
}
</style>
	<div class="magelur_about">
		<!-- Header Start -->
		<section>
			<div class="container-fluid magelur_header">
				<div class="magelur_login_banner">
				
					<div class="banner_logintxt">
					  <div class="signins wow FadeInLeft">
					   <h1>Sign In </h1>
					   <!-- <h3>And start networking</h3> -->
             <div class="two_laid"><a href="{{asset('/home')}}">HOME</a><p>LOGIN</p></div>
            <!--  <ul class="link_list">
                <li class="active"><a href="http://demo.bsetec.com/demo/magelur/public">HOME</a></li>
                <li>//</li>
                <li><a href="javascript:void(0)">LOGIN</a></li>
              </ul> -->
					  </div>
					 </div>

					 <div class="enter_log wow FadeInUp">
					  <p>Login</p>
					  <h1>and start networking</h1>
					 </div>

					</div>
				</div>
			</section>
		</div>
		<!-- Header End -->



<div class="login_base">
 <div class="first_claerfix clearfix">
  <div class="col-sm-6 wow FadeInLeft">
  <div class="montertes">

    <p class="message alert alert-danger " style="display:none;"></p> 
  @if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{{\Session::get('messagetext')}}">
    @endif
        
        @if(Session::has('message'))
        {!! Session::get('message') !!}
      @endif
    <ul class="parsley-error-list">
      @foreach($errors->all() as $error)
        <li style='color: #ffb524;'>{{ $error }}</li>
      @endforeach
    </ul>

    <div class='login1'>
<form method="post" name="myForm" action="{{ url('model-agent/signin')}}" class="form-vertical" id="LoginAjax" accept-charset="UTF-8" class="form-signup" parsley-validate="" novalidate=" " >
  <div class="strendng magelur_login_page">
 <b> <div id='message' style='top: -8px;font-size: 15px; color: #bd8414;margin-bottom: 17px;'></div></b>

   <div class="mainlog">
    <a href="javascript:void(0)">Login As</a>
    <br><br>

    <label class="agree">Agent
    <input type='radio' name='agree' value ='agent' @if (Cookie::get('userType') == 'agent') checked="checked" @endif @if (!Cookie::get('userType')) checked="checked" @endif>  
    <span class="checkmark"></span>
    </label>  <label class="agree">Model
    <input type='radio' name='agree' value ='model' @if (Cookie::get('userType') == 'model') checked="checked" @endif>  
    <span class="checkmark"></span>
    </label>

   </div>


   <div class="user_login strikes">
   	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="inputslogin">
      {!! Form::text('email', Cookie::get('email'),array( 'placeholder'=>'Enter your Email','id'=>"email")) !!} 
    </div>
    <div class="inputslogin second_inputs_lg">
       <input type="password" id='password' name="password" value="{{ Cookie::get('password') }}" placeholder="Enter your Password"   />
    </div>
    
    <div class="rember_me">
     <div class="checkbox checkbox-anr case_mix_check" >
       <input type="checkbox" name="terms_agree" value="1" />
       <label><span>Remember me</span></label>
      </div>
      <p>* By clicking Sign In you agree to our <a href="http://demo.bsetec.com/demo/magelur/public/terms">Terms of Use</a> and <a href="http://demo.bsetec.com/demo/magelur/public/privacy-policy">Privacy</a> Statement.</p>
    </div>
     <div class="forget_password">
     <a href="javascript:void(0)"  onClick="showDiv();" class="forget_pass">Forget your password?</a>
     </div>

    </div>
   </div>
   <div class="login_signin_btn">
     <!-- <a href="javascript:void(0)">Sign In</a> -->
     <button type="submit" class="full_btn ajaxLoading" style="display: none;"> <div class="ajaxLoading" >   
      <img src="{{ asset('magelur/images/lg.palette-rotating-ring-loader.gif')}}"  style="width:50px;height:50px"/>
    </div> Please wait.. </button>
    <button type="submit" class="full_btn sigg"> Sign In</button>
    </div>

</form>
</div>
<div class='login2' style='display:none;'>
  <form method="post" name="myForms" action="{{ url('user/email/request')}}" class="form-vertical" id="LoginAjaxs" accept-charset="UTF-8" class="form-signup" parsley-validate="" novalidate=" " >
  <div class="strendng magelur_login_page">
 <b> <div id='message' style='top: -8px;font-size: 15px; color: #bd8414;margin-bottom: 17px;'></div></b>

 <div class="mainlog">
  <a href="javascript:void(0)">Forget Password</a>
  <br><br>
    <label class="agree">Agent
    <input type='radio' name='agree' value ='agent' checked>  
    <span class="checkmark"></span>
    </label>  <label class="agree">Model
    <input type='radio' name='agree' value ='model' >  
    <span class="checkmark"></span>
    </label> 
</div>


   <div class="user_login">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    
    <div class="inputslogin ">
        <input type="text" name="credit_email" placeholder="{{ Lang::get('core.email') }}" class="" required/>
    </div>
    
     <br>
    <br>
     <div class="forget_password">
     <a href="javascript:void(0)"  onClick="showDivs();" class="">Login As</a>
     </div>

    </div>
   </div>
   <div class="login_signin_btn">
     <!-- <a href="javascript:void(0)">Sign In</a> -->
   
    <button type="submit" class="full_btn"> Submit</button>
    </div>
</form>
</div>

   </div>
  </div>
  <div class="col-sm-6 wow FadeInRight">
   <div class="dnthave">
    <h1>Don't have an account</h1>
    <p>Not yet a member? Here is what you are missing out on!</p>
    <ul class="mantres">
     <li>Promoting yourself on our vibrant and growing network</li>
     <li>Being the next top Model</li>
     <li>Share your talents for all to see</li>
     <li>Share your photos and videos</li>
     <li>Share your social pages to be discovered in more ways</li>
     <li>Commenting and liking other talents and productions</li>
    </ul>
   </div>
   <div class="signupnow">
    <h1>Sign Up Now!</h1>
   </div>
    <div class="signupsagent">
      @if( app('request')->input('tellfriend'))
      {{--*/ $url=app('request')->input('tellfriend') /*--}}
       <a href="{{ url('/model/create?tellfriend=') }}{{$url}}">I am a Model</a>
       <a href="{{ url('/agent/create?tellfriend=') }}{{$url}}">I am an Agent</a>
      @else
     <a href="{{ url('/model/create') }}"><button><span>I am a Model</span></button></a>
     <a href="{{ url('/agent/create') }}"><button><span>I am an Agent</span></button></a>
     @endif

     
    </div>
  </div>
  </div>
 </div>

<div class="our_terms_use">
 <div class="maintinused wow FadeInUp">
 <h2>Our Terms of use</h2>
 <h4>If you need immediate assistance login in, you may contact our IT Services help desk on our contact page: by clicking here.</h4>
 <p>You are attempting to enter Pure & Innocent - Talent Models Network portal and to access it's information (collectively "Pure And Innocent.net Systems and Information"), which are private. Access to our Systems and Information is restricted to authorized users only and your actions will be monitored.</p>
 <p>Your username and password are for your use only. Do not share your login credentials with any other party. If you believe that another party has knowledge of your login credentials, contact our help desk immediately at models@PureAndInnocent.net</p>
 </div>
</div>


<script type="text/javascript">
    $("#LoginAjaxs").validate({
     ignore: "not:hidden",
      rules: {

         credit_email:{
         required:true,
         email:true,
         
        },

        //  new_password:{
        //  required:true,
         
        // },
        // confirm_password:{
        //   required:true,
        // }
        
      },
      messages: {
              
        credit_email:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",  
          email:"<span><i class='fa fa-warning'></i></span> Invalid Email address",     

        },
      },
      
        submitHandler: function(form) {
                      form.submit();

         
        },
        });

$("#LoginAjax").validate({
     ignore: "not:hidden",
      rules: {
       agree:{
          required:true,
          
        },
       email:{
          required:true,
          email:true,
            remote:{
          url:"{!! url('model/checkaccount')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#email').val();
            },
            agree:function(){
              return $('input[name=agree]:checked').val();
            },
            
          }
        }
        
        },
         password:{
         required:true,
         
        },
       
      },
      messages: {
         agree:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",       
         
        },
        
        email:{
          required:"<span><i class='fa fa-warning'></i></span> This field is required",
          email:"<span><i class='fa fa-warning'></i></span> Invalid Email address",
        },
        
        password:{
         required:"<span><i class='fa fa-warning'></i></span> This field is required",
        },
      },
      
        submitHandler: function(form) {
          $.ajax({
          url: form.action, 
          type: form.method,             
          data:$(form).serialize(),
         beforeSend: function() {

$('.sigg').hide();
            $('.ajaxLoading').show();   

            },
          success: function(data) {
            $('.sigg').show();

             $('.ajaxLoading').hide();   

            if(data==''){
              swal('Incorrect Email ID / Password')
              $('input[name=email]').val(''); 
              $('input[name=password]').val(''); 
            // $("#message").html('Incorrect Email ID / Password');
            }
            else{
                           $('.ajaxLoading').hide();   

               $("#message").hide();
              form.submit();
            }

          },
          });
        },
        });
if($('#msg').val()!="")
{
  swal($('#msg').val());
}
</script>
<script type="text/javascript">
/*Forget password*/
function showDiv(pageid)
{
$('input[name=email]').val(''); 
$('input[name=password]').val(''); 

$('.login2').show();
$('.login1').hide();
}
/*Login As*/
function showDivs(pageid)
{
  $('input[name=credit_email]').val(''); 

$('.login2').hide();
$('.login1').show();

}
</script>
@if(Session::has('messageetext'))
<script>
toastr.success("{!! Session::get('messageetext') !!}");
</script>
@endif
<!-- Reset password flash message -->
@if(Session::has('message_reset') && Session::get('msgstatus') =='success')
<script>
toastr.success("{!! Session::get('message_reset') !!}");
</script>
@elseif(Session::has('message_reset') && Session::get('msgstatus') =='error')

<script>
toastr.error("{!! Session::get('message_reset') !!}");
</script>
@endif