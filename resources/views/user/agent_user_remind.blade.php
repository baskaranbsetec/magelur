
<div class="login_base">
 <div class="first_claerfix clearfix">
  <div class="col-sm-6 wow FadeInLeft">
  <div class="montertes">
		{!! Form::open(array('url' => 'user/agent/doreseted/'.$verCode, 'class'=>'form-vertical sky-form boxed ','id'=>'LoginAjaxs')) !!}

			    	@if(Session::has('message'))
						{!! Session::get('message') !!}
					@endif

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
		  <div class="strendng magelur_login_page">
  <div class="mainlog">
    <a href="javascript:void(0)">Reset Password</a>
    <br><br>

   
   </div>
<div class="user_login">
   	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="inputslogin">
    	{!! Form::password('password',  array('class'=>'', 'placeholder'=>'New Password')) !!}


	  </div>
    <div class="inputslogin second_inputs_lg">
	{!! Form::password('password_confirmation', array('class'=>'', 'placeholder'=>'Confirm Password')) !!}
    </div>
</div>
</div>


			<div class="login_signin_btn">
     <!-- <a href="javascript:void(0)">Sign In</a> -->
 
    <button type="submit" class="full_btn">Submit</button>
    </div>			

		
			
			 {!! Form::close() !!}
  </div>
  </div>
   <div class="col-sm-6 wow FadeInRight">
   <div class="dnthave">
    <h1>Don't have an account</h1>
    <p>Not yet a member? Here is what you are missing out on!</p>
    <ul class="mantres">
     <li>Promoting yourself on our vibrant and growing network</li>
     <li>Being the next top Model</li>
     <li>Share your talents for all to see</li>
     <li>Share your photos and videos</li>
     <li>Share your social pages to be discovered in more ways</li>
     <li>Commenting and liking other talents and productions</li>
    </ul>
   </div>
   <div class="signupnow">
    <h1>Sign Up Now!</h1>
   </div>
    <div class="signupsagent">
      @if( app('request')->input('tellfriend'))
      {{--*/ $url=app('request')->input('tellfriend') /*--}}
       <a href="{{ url('/model/create?tellfriend=') }}{{$url}}">I am a Model</a>
       <a href="{{ url('/agent/create?tellfriend=') }}{{$url}}">I am an Agent</a>
      @else
     <a href="{{ url('/model/create') }}">I am a Model</a>
     <a href="{{ url('/agent/create') }}">I am an Agent</a>
     @endif

     
    </div>
  </div>
  </div>
 </div>
<script type="text/javascript">
    $("#LoginAjaxs").validate({
     ignore: "not:hidden",
      rules: {

         password:{
         required:true,
          minlength : 6
         
        },

        password_confirmation:{
         required:true,
          equalTo : '[name="password"]'
         
        }, 
        // confirm_password:{
        //   required:true,
        // }
        
      },
      messages: {
              
        password:{
         required:"Please provide New Password ",
        },
        password_confirmation:{
         required:"Please provide Confirm Password ",
         equalTo:"Confirm Password should same as New Password",
        },
      },
      
        submitHandler: function(form) {
                      form.submit();

         
        },
        });


</script>

