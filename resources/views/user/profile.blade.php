@extends('layouts.app')

@section('content')


  <div class="page-content row">
		
	<div class="page-content-wrapper m-t">
	@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	
	<ul>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>	
	<ul class="nav nav-tabs" >
	  <li class="active"><a href="#info" data-toggle="tab"> {{ Lang::get('core.personalinfo') }} </a></li>
	  <li ><a href="#pass" data-toggle="tab">{{ Lang::get('core.changepassword') }} </a></li>
	</ul>	
	
	<div class="tab-content">
	  <div class="tab-pane active m-t" id="info">
		{!! Form::open(array('url'=>'user/saveprofile/', 'class'=>'form-horizontal ' ,'files' => true)) !!}  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4"> Username </label>
			<div class="col-md-8">
			<input name="username" type="text" id="username" disabled="disabled" class="form-control input-sm" required  value="{{ $info->username }}" />  
			 </div> 
		  </div>  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.email') }} </label>
			<div class="col-md-8">
			<input name="email" type="text" id="email"  class="form-control input-sm" value="{{ $info->email }}" /> 
			 </div> 
		  </div> 	  
	  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.firstname') }} </label>
			<div class="col-md-8">
			<input name="first_name" type="text" id="first_name" class="form-control input-sm" required value="{{ $info->first_name }}" /> 
			 </div> 
		  </div>  
		  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.lastname') }} </label>
			<div class="col-md-8">
			<input name="last_name" type="text" id="last_name" class="form-control input-sm" required value="{{ $info->last_name }}" />  
			 </div> 
		  </div>    
	
		  <div class="form-group  " >
			<label for="ipt" class=" control-label col-md-4 text-right"> Avatar </label>
			<div class="col-md-8">
			
					<input type="file" name="avatar" data-width="80" data-height="80" data-id="blogo_src">
				
			 Image Dimension 80 x 80 px <br />
			 			  <div class="preview-events ">

			{!! SiteHelpers::showUploadedFile($info->avatar,'/uploads/users/') !!}
						</div>

			 </div> 
		  </div>  
	
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button type="submit" name="" id="" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_savechanges') }}</button>

				<!-- <button class="btn btn-success" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button> -->
			 </div> 
		  </div> 	
		
		{!! Form::close() !!}	
	  </div>
  
	  <div class="tab-pane  m-t" id="pass">
		{!! Form::open(array('url'=>'user/savepassword/', 'class'=>'form-horizontal ')) !!}    
		  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.newpassword') }} </label>
			<div class="col-md-8">
			<input name="password" type="password" id="password" class="form-control input-sm" value="" /> 
			 </div> 
		  </div>  
		  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.conewpassword') }}  </label>
			<div class="col-md-8">
			<input name="password_confirmation" type="password" id="password_confirmation" class="form-control input-sm" value="" />  
			 </div> 
		  </div>    
		 
		
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button type="submit" name="" id="" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_savechanges') }}</button>

<!-- 				<button class="btn btn-danger" type="submit"> {{ Lang::get('core.sb_savechanges') }} </button>
 -->			 </div> 
		  </div>   
		{!! Form::close() !!}	
	  </div>
  


</div>
</div>
 
 </div>
<script type="text/javascript">
$('body').on('change','input:file',function(){
	var _URL = window.URL || window.webkitURL,
	file, img,height,file_html='',
	id = $(this).data('id'),
	height = $(this).data('height'),
	width = $(this).data('width'),
	attrName = $(this).attr('name');
	selector = $('#'+id);
	selector.addClass('hide');
	if (this.files && this.files[0]) {
		img = new Image();
		img.src = _URL.createObjectURL(this.files[0]);
		if(id=='fav') image_type = ['gif','png','jpg','jpeg','ico']; else image_type = ['gif','png','jpg','jpeg']; 
		ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, image_type) == -1) {
			alert("invalid image");
			// file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
			// $('input[data-id="'+id+'"]').remove();
			$('input[name=avatar]').val(null);
			// $('#'+attrName).html(file_html);
			selector.attr('src','');
			return false;
		}else{
			selector.attr('src',img.src);
			img.onload = function(){
				if(this.width > width || this.height > height){
					alert('Your Selected Image size '+this.width+'px * '+this.height+'px.Please use image dimension '+width+'px * '+height+'px');
					// file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
					// $('input[data-id="'+id+'"]').remove();
					// $('#'+attrName).html(file_html);
					$('.preview-events').hide();
					$('input[name=avatar]').val(null);
					selector.attr('src','{{ asset("magelur/images/file-broken.png")}}');
					return false;
				}
				else{
					$('.preview-events').show();
				}
			}
		}

	}
	setTimeout(function(){
		selector.removeClass('hide');
	},1000);
});
// show preview image
		$(document).on('change','input[name="avatar"]',function(){
		  readURL(this);
		})
		// render image
		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-events img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);

			}
		}
</script>
	 
@endsection