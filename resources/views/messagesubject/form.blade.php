@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1> Message Subjects</h1>
		<div class="sbox-tools " >
			@if(Session::get('gid') ==1)
				<!-- <a href="{{ URL::to('magelur/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
			@endif 			
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'messagesubject/save?return='.$return, 'class'=>'form-horizontal','files' => true ,'id' => 'register' , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><!-- <legend> Message Subjects</legend> -->
									
									 <!--  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-7"> -->
										  <input  type='hidden' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control ' /> 
										<!--  </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> --> 					
									  <div class="form-group  " >
										<label for="Name" class=" control-label col-md-4 text-left"> Name <span class="asterix"> * </span></label>
										<div class="col-md-7">
										  <input  type='text' name='name' id='name' value='{{ $row['name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									 <div class="form-group  " >
							<label for=" Status" class=" control-label col-md-4 text-left">  Status <span class="asterix"> * </span> </label>
							<div class="col-md-7 nopadding-left">
							<label class='radio radio-inline'>
							{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active </label> 
							<label class='radio radio-inline'>
							{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive </label> 		
							</div> 
						</div> 	 
						
				
									</fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('messagesubject?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("messagesubject/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});

		$("#register").validate({
     ignore: "not:hidden",
      rules: {
        name:{
         required:true,
         
         
       },
         status:{
         required:true,
         
         
       },
      
    


      },
       messages: {
      name:{
         
         required:'Please provide Subject name'
       },
    
    status:{
         
         required:'Please provide Status'
       },

       },
        submitHandler:function(form){
       
        form.submit();
        }
    });
	</script>		 
@stop