@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 	

	<div class="sbox">
	<div class="sbox-title">
		<h1>Blogs Management</h1>
		

		<!-- <div class="sbox-tools" >
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('blogs/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-cloud-download"></i></a>
			@endif

			<a href="{{ url($pageModule) }}" class=" tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_clearsearch') }}" ><i class="fa fa-spinner"></i>  </a>		
			@if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 
		</div> -->
	</div>

	<div class="sbox-content"> 	
	<div class="sbox-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('blogs/update?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa  fa-plus "></i> Create </a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-trash-o"></i> Delete </a>
			@endif 
			<!-- <a href="{{ URL::to( 'blog/search?return='.$return) }}" class="btn btn-xs btn-default" onclick="SximoModal(this.href,'Advance Search'); return false;" title="{{ Lang::get('core.btn_search') }}"><i class="fa  fa-search"></i> </a>				 -->

			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('blogs/download?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_download') }}" style="display:none">
			<i class="fa fa-cloud-download"></i> Download </a>
			@endif

		</div>


	 <!-- {!! (isset($search_map) ? $search_map : '') !!} -->
	
	 {!! Form::open(array('url'=>'blogs/delete/0?return='.$return, 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;  padding-bottom:60px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"><span> No </span> </th>
				<th> <input type="checkbox" class="checkall" /></th>
								
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						@if($t['label']=='Alias')
						<th><span >{{ $t['label'] ==''}}</span></th>	
						@else
						<th><span >{{ $t['label'] }}</span></th>	
						@endif		
						@endif 
					@endif
				@endforeach
				<th ><span style='padding:25px'>Categories</span></th>
				<th ><span>{{ Lang::get('core.btn_action') }}</span></th>
			  </tr>
        </thead>

        <tbody> 
          <tr id="orderDetails">
<td class="number"></td>
<td></td>
<td id="title" class="fieldsearch stumeder"> <div class="select-style">{!! Form::text('title',$title,array('class'=>'form-control', 'placeholder'=>'Name', )) !!} </div></td>
<td></td>
<td id="description" class="fieldsearch"> <div class="select-style">{!! Form::text('description',$description,array('class'=>'form-control', 'placeholder'=>'Description', )) !!} </div></td>
<td id="status" class="fieldsearch"> <div class="select-style">
<select name='status' class = 'select2 '>
	  <option value="">Select option</option>

  <option value="enable">Active</option>
  <option value="disable">Inactive</option>

</select> </div></td>
<td id="Tags" class="fieldsearch" style="width:100px"> <div class="select-style">{!! Form::text('tags',"{$labels}",array('id'=>'tags','class'=>'form-control', 'placeholder'=>'Tags', )) !!} {!! Form::hidden('Tags','',array('id'=>'Alltags','class'=>'form-control', 'placeholder'=>'Tags', )) !!}</div></td>

<td></td>
<td id="categories" class="fieldsearch"> <div class="select-style">	
{!! Form::select('categories',array(''=>'Select Category')+\SiteHelpers::getTableValues('category') ,$categories,array('class' => 'select2 ','id' => 'categories',)) !!}	
 </div></td>
<td class="icons">
    <input type="hidden"  value="Search">
    <a href=""><button type="button" title="Go" name="search" class="goSearch btn go-btn btn-xs btn-info"><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>  
    <a href="{{ url('blogs') }}" type="button" class="go-btn btn btn-xs btn-info" title="Clear search"><i class="fa fa-undo" aria-hidden="true"></i></a>  
</td>

</tr>       
            {{--*/ $i = ($model_filter->currentPage() - 1) * $model_filter->perPage() + 1;/*--}}
			@foreach($model_filter as $row)
                <tr>
					<td width="30"> {{$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id }}" />  </td>	
					
				{{--*/ $i++ /*--}}

				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>		
						 @if($field['field']=='image')
							<a href="{{ asset('uploads/blogs/'.$row->image) }}" target="_blank" class="previewImage">

							<img src="{{ asset('uploads/blogs/'.$row->image) }}" border="0" width="50" class="img-circle">
							</a>					
						 @elseif($field['field']=='status')
								
								@if( $row->status == 'enable')
								<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>

								@else
								<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
								@endif
						@elseif($field['field']=='alias')
							{{--*/ $field['field']==''/*--}}
						@elseif($field['field']=='tags')
							{{$row->tags}}			
						@elseif($field['field']=='note')
							{{---*/ $content = substr( $row->note,0,250 ) /*---}}
							{!! $content !!}

						@else			 
						 	{!! SiteHelpers::formatRows($row->{$field['field']},$field ,$row ) !!}	
						 	@endif					 
						 </td>
						@endif	
					 @endif					 
				 @endforeach
						<td >
						{{--*/ $idz=\SiteHelpers::getcolumnvalue('category',$row->categories) /*--}}

						@foreach($idz as $id)
						<ul>
						<li>@if($row->categories) {{$id->name}} @endif</li>
						</ul>
						@endforeach
						</td>
				 <td>
					 	@if($access['is_detail'] ==1)
					 	{{--*/ $url_id =urlencode( base64_encode($row->id));/*--}}
					 	
						<a href="{{ url('blogs/article/'.$row->title.'/'.$url_id.'/'.$row->alias)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('blogs/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif

					</td>	 
                </tr>
				
            @endforeach

        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	@if(count($model_filter) == 0)
	<p class="nodata-txt">Data not available ! </p>	 
	@endif
	</div>
	{!! Form::close() !!}
	<div class="table-footer">
	<div class="row">
	 <div class="col-sm-4">
	  <div class="table-actions" style=" padding: 10px 0">
	 
	   {!! Form::open(array('url'=>$pageModule.'/filter/')) !!}
		   {{--*/ $pages = array(5,10,20,30,50) /*--}}
		   {{--*/ $orders = array('asc','desc') /*--}}
		<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_page') }} </option>
		  @foreach($pages as $p)
		  <option value="{{ $p }}" 
			@if(isset($pager['rows']) && $pager['rows'] == $p) 
				selected="selected"
			@endif	
		  >{{ $p }}</option>
		  @endforeach
		</select>
		<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
		  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
		  @foreach($tableGrid as $field)
		   @if($field['view'] =='1' && $field['sortable'] =='1') 
			@if( $field['field'] == 'alias' || $field['field'] == 'image' || $field['field'] == 'MediaUri' || $field['field'] == 'Description'|| $field['field'] == 'Ext'|| $field['field'] == 'Type'|| $field['field'] == 'LastEdit') {{ $field['field'] == '' }} @else
			  <option value="{{ $field['field'] }}" 
			  @endif
				@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
					selected="selected"
				@endif	
			  >
			  {{ $field['label']}}  </option>
			@endif	  
		  @endforeach
		 
		</select>	
		<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
		  <option value=""> {{ Lang::get('core.grid_order') }}</option>
		   @foreach($orders as $o)
		  <option value="{{ $o }}"
			@if(isset($pager['order']) && $pager['order'] == $o)
				selected="selected"
			@endif	
		  >{{ ucwords($o) }}</option>
		 @endforeach
		</select>	
		<button type="submit" class="btn btn-white btn-sm"><i class="icon-bubble-right"></i> GO</button>	
		<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
		<input type="hidden" name="sc" value="{{ @$_GET['search'] }}" />		
	  {!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-2">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ $pagination->total() }}</b>
				
	   </div>
		@if(count($model_filter) >0)
	<div class="pagination_s">
{!! str_replace('/?', '?', $model_filter->appends(Input::except('page'))->render()) !!}
</div>
</p>
@endif
	  </div>
	</div>

	<!-- @include('footer') -->
	</div>
</div>	
	</div>	  
</div>	
	<script>
	var alltags='';
$('#tags').tagsInput({
	width:'auto',
	onChange: function(elem, elem_tags)
	{
		// alert(elem_tags);
		alltags =elem_tags+','+alltags;
		document.getElementById("Alltags").value=alltags;
		// alert(alltags);
		
	}
});
$('.map').hide();
$('.pagination_blk').hide();
</script>
@stop

