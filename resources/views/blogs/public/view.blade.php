<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>PageID</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Title</td>
						<td>{{ $row->title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Alias</td>
						<td>{{ $row->alias}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Note</td>
						<td>{{ $row->note}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Created</td>
						<td>{{ $row->created}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Updated</td>
						<td>{{ $row->updated}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Filename</td>
						<td>{{ $row->filename}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Access</td>
						<td>{{ $row->access}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Allow Guest</td>
						<td>{{ $row->allow_guest}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Template</td>
						<td>{{ $row->template}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Metakey</td>
						<td>{{ $row->metakey}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Metadesc</td>
						<td>{{ $row->metadesc}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Default</td>
						<td>{{ $row->default}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Pagetype</td>
						<td>{{ $row->pagetype}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Labels</td>
						<td>{{ $row->labels}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Views</td>
						<td>{{ $row->views}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Userid</td>
						<td>{{ $row->userid}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Image</td>
						<td>{{ $row->image}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	