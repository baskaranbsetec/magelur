<div class="right_mtb blog_post_widget">
<div class="left_abt clearfix">
<h4><!-- <i class="fa fa-tags"></i> -->{{ Lang::get('core.widget_categories') }} </h4>
<div class="category_list_post">
{!! PostHelpers::cloudtags() !!}
</div>
<!-- <hr /> -->


<h3 style="display:none">{{ Lang::get('core.latest_post') }} </h3>

<ul class="widgeul" style="display:none">
@foreach($latestposts as $row)
	<li>
		@if($conpost['commimage'] ==1 )
		<div class="image">
			<img src="{{ asset('uploads/images/'.$row->image)}}"  width="50" />
		</div>	
		@endif

		<div class="post">
			<b><a href="{{ url('post/view/'.$row->id.'/'.$row->alias)}}"> {{ $row->title }}</a></b><br />
			<span> {{ date("M j, Y " , strtotime($row->created)) }} </span>
		</div>	
	</li>
@endforeach
</ul>

</div>
<div class="left_abt clearfix">
<h4>{{ Lang::get('core.tag_cloud') }}</h4>
<div class="blog_category tag_cloud_blk">

<div class="category_list_post">
<span> {!! PostHelpers::cloudLabels() !!} </span>
</div>
<!-- Newyork
<span>events</span> 
<span>blogs</span> 
hai all 
<sub>events</sub> 
<sub>blogs</sub>
<span>hai</span> 
<span>all</span> 
<span>yes</span>-->
</div>
</div>
</div> 

<style>
.map {
    display: none;
}
</style>