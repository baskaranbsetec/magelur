@extends('layouts.app')

@section('content')


<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>

<style type="text/css">
label.error,.asterix{
color: #cc0000;
font-family: 'Open Sans',Arial,Helvetica,sans-serif;
font-weight: normal;
font-size: 13px;
}
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}
.map {
    display: none;
}
</style>
<div class="page-content row">
	<!-- Page header -->


	<div class="page-content-wrapper m-t">


		<div class="sbox">
			<div class="sbox-title"> 
				<div class="sbox-tools pull-left" >
					<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
				</div>
				<h1>Blogs Management</h1>
				<!-- <div class="sbox-tools " >
					@if(Session::get('gid') ==1)
					<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
					@endif 			
				</div>  -->

			</div>
			<div class="sbox-content"> 	

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>	

				{!! Form::open(array('url'=>'blogs/save?return='.$return, 'class'=>'form-horizontal','id'=>'blogs','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="col-md-12">
					<fieldset>

						<input type="hidden" name="id" value="{{ $row['id'] }}">

						<div class="form-group  " >
							<label for="Title" class=" control-label col-md-4 text-left"> Title <span class="asterix"> * </span></label>
							<div class="col-md-7">
								<input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
								class='form-control ' /> 
							</div> 
							<div class="col-md-1">

							</div>
						</div> 


						<div class="form-group  " >
							<label for="Note" class=" control-label col-md-4 text-left"> Description <span class="asterix"> * </span></label>
							<div class="col-md-7">
								<textarea name='note' minlength="20" rows='5' id='note' class='form-control input-sm  max markItUp markItUpEditor'  
								>{{ $row['note'] }}</textarea> <div id="msg"></div>

							</div> 
							<div class="col-md-1">

							</div>
						</div> 		
						<div class="form-group  " >
						<label for=" Categories" class=" control-label col-md-4 text-left"> Categories <span class="asterix"> * </span></label>
						<div class="col-md-7">
						{!! Form::select('categories[]',\SiteHelpers::getTableValues('category') ,explode(',',$row['categories']),array('class' => 'select2 ','id' => 'categories','multiple'=>'multiple')) !!}	
						</div> 
						</div> 
						<!-- categories -->
						<!-- @if(count($categories) > 0 )
						<div class="form-group  " >
							<label for="Title" class=" control-label col-md-4 text-left"> Categories </label>
							<div class="col-md-7">
								<select name="categories[]" class='form-control' id='label' multiple="multiple">
									@foreach($categories as $cate)
									<option value="{{ $cate->id }}">{{ $cate->name }}</option>
									@endforeach
								</select>
							</div> 
							<div class="col-md-1">

							</div>
						</div> 
						@endif -->
						<div class="form-group  " >
							<label for="Title" class=" control-label col-md-4 text-left"> Tags <span class="asterix"> * </span></label>
							<div class="col-md-7">
								<input  type='text' name='labels' id='tags' value='{{ $row['labels'] }}' 
								class='form-control ' /> 
								<div id="errorToShow"></div>
							</div> 
							<div class="col-md-1">

							</div>
						</div> 


							


						<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> Image <span class="asterix"> * </span></label>
							<div class="col-md-7">
								<input  type='file' name='image' id='image' @if($row['image'] =='') class='required' accept="image/*" @endif />
								<div class="preview-blogs">
									{!! SiteHelpers::showUploadedFile($row['image'],'/uploads/blogs/') !!}

								</div>					

							</div> 
							<div class="col-md-1">

							</div>
						</div> 
						<div class="form-group  " >
							<label for=" Status" class=" control-label col-md-4 text-left">  Status <span class="asterix"> * </span></label>
							<div class="col-md-7">
							<label class='radio radio-inline'>
							{!! Form::radio('status','enable',($row['status']=='enable') ? true:false,array('id'=>'status')) !!} Active </label> 
							<label class='radio radio-inline'>
							{!! Form::radio('status','disable',($row['status']=='disable') ? true:false,array('id'=>'status')) !!} Inactive </label> 		
							</div> 
						</div> 


					  </fieldset>
					</div>

					<div style="clear:both"></div>	
					
					<div class="form-group">
						<label class="col-sm-4 text-right">&nbsp;</label>
						<div class="col-sm-8">	
							<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
							<button type="submit" name="" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
							<button type="button" onclick="location.href='{{ URL::to('blogs?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
						</div>	  

					</div> 

					{!! Form::close() !!}
				</div>
			</div>		 
		</div>	
	</div>	
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">

	$(document).ready(function() { 
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("blogs/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
//  tag plugin
		$('input[name="labels"]').tagsInput({
			width:'auto'
		});
		var categories     = '{{ $row["categories"] }}';
		if(categories){
			var selectedValues = categories.split(',');
			$("select#categories").select2('val',selectedValues);	
		}
		$("#categories").select2({
			placeholder: "Select categories",
			allowClear: true
		});

		});
		// show preview image
		$(document).on('change','input[name="image"]',function(){
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
				swal("Invalid extension!");
				$(this).val(null);


			}
		  readURL(this);
		})

		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.preview-blogs img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}	
		$("#blogs").validate({
			ignore: "not:hidden",
			rules: {


				'categories[]':{
					required:true,
				},
				title:{
					required:true,
				},
				note:{
					required:true,

				},
				labels:{
					required:true,
				},
				status:{
					required:true,
				},


			},
			messages: {



			},
			errorPlacement: function(error, element) {
				if (element.attr("name") == "labels") {
					error.appendTo("#errorToShow");
				}

				else {
					error.insertAfter(element);
				}
			},
			submitHandler:function(form){

				form.submit();
			}
		});	
		$('select').on('change', function() {
			$(this).valid();
		});
		 $('#blogs').on('click',function(){
		 	if($('#tags_tagsinput span.tag').text() != '')
		 	{
		 		$('#errorToShow').hide()
		 	}
		 	else{
		 		$('#errorToShow').show()

		 	}
		 	if($('#status').val() != '')
		 	{
		 		$(this).valid();

		 	}


		 }); 
		  $(function(){
		  	var max = parseInt($("#note").attr("maxlength"));
		  	$("#count").text("Characters left: " + max);
		  	$("#note").keyup(function(e){
		  		$("#count").text("Characters left: " + (max - $(this).val().length));
		  		if($(this).val().length==max)
		  			$("#msg").text("500 words allowed");
		  		else
		  			$("#msg").text("");
		  	});
		  });
	</script>	

	@stop





