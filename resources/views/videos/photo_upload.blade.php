        

<link href="{{ asset('magelur/css/uploader.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('magelur/js/uploader.js')}}"></script>

<!-- Tags script -->
<link rel="stylesheet" type="text/css" href="{{ asset('magelur/css/tags.css') }}">
<script type="text/javascript" src="{{ asset('magelur/js/tags.js') }}"></script>
<div class="photoupload_page  col-md-12 col-sm-12 col-xs-12">
<div class="photoupload_blk col-md-12 col-sm-12 col-xs-12">
<form id="payment-stripe" class="photo_upload_form" action="javascript:void(0)" method="POST" novalidate="novalidate">
  <input type="hidden" name="album_id" value="" />
    <div class="small-12 columns upoload_radio_blk">
    <label class="agree round_radio_btn">
    <input type="radio" name="album_type" value="new"> Create New Album 
    <span class="checkmark"></span>
    </label>
    <label class="agree round_radio_btn">
    <input type="radio" name="album_type" value="old"> Choose From List
    <span class="checkmark"></span>
    </label>          
    </div>

  <div class="small-6 old-albums-form columns" style="display: none;">
    <p>
      It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
    </p>
    <select name="album_id">
      <option value="1"> Summer </option>
      <option value="2"> Winter </option>
      <option value="3"> Autmn  </option>
      <option value="4"> Spring </option>
    </select>
  </div>  

  <div class="new-albums-form" >

    <div class="row new-albums-form-list" style="display: none;">
      <div class="small-6 columns">
        <label>Album Name</label>
        <input  type="text" name="album_name" />
      </div>

      <div class="small-6 columns">
        <label>Album Description</label>
        <textarea name="album_desc"></textarea>
      </div>
    </div>

<!--     <div class="row ">
      <div class="small-12 columns payment_submit_btn">
        <div class="album_submit_div">
          <button class="button postfix" onclick="addAlbum();">Submit</button>
          <input type="submit" value="Submit" class="button postfix" /> 
        </div>
      </div>  
    </div>  
  </div> -->


    <div class="video_info">

      <!-- Photo Title -->
      <input type="text" placeholder="Video Title" name="title" id="title">

      <!-- Photo Tags -->
      <input type="text" placeholder="Video Tags" name="tags" id="tags" >

      <!-- Video Description -->
      <textarea placeholder="Video Description" name="description" id="description"></textarea>

    </div>


<input style="display: none;" type="file" name="vpb-data-file" id="vpb-data-file" onchange="vpb_image_preview(this)" multiple="multiple"  />
<input type="hidden" name="remaining_media_count" value="5">



  <div  class="preview_imges_blk preview_imges_scroll1"  id="vpb-display-preview"></div>  

  <div class="browse_btn">
    <div class="vpb_browse_file" onclick="document.getElementById('vpb-data-file').click();"><button><span>Browse</span></button>
      <div id="wave">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
      </div>
    </div> <!-- Browse File Button -->
    <div  onClick="vpb_upload_previewed_files('{{ url('/user/save/model/image') }}');" class="vpb_pagination_button_clicked"><button><span>Upload</span></button></div> <!-- Upload File Button -->
  </div>

</form>
</div>
</div>




<script type="text/javascript">
$(document).ready(function(){
 
    //  tag plugin
    $('input[name="tags"]').tagsInput({
      width:'auto'
    });   
    

    $(document).on('click','input[name="album_type"]',function(){
      // get the choosed value
      var curVal = $(this).val();

      if(curVal == 'new'){
        // show album create form
        $('div.new-albums-form-list').show();
        // hide album pick list
        $('div.old-albums-form').hide();
        // show remaining fields
        $('div.new-albums-form').show();        
      }else{
        // allow user to pick the album list
        $('div.old-albums-form').show();
        // hide album create form
        $('div.new-albums-form-lis').hide();
        // show remaining fields
        $('div.new-albums-form').show();

      }
    })

    $(".fancybox").fancybox({});

});

function addAlbum(){
  var title    = $('input[name="album_name"]').val();
  var desc     = $('textarea[name="album_desc"]').val();
  $.post('{!! url("add/album")!!}',{title:title,desc:desc},function(data){

  });
}

</script>

<style type="text/css">
  .map{display: none;}
  .page-header {
    display: none;
}
</style>