<!-- Check Agent Exists Or Not -->

@if(!empty($user))
<script src="{{ asset('magelur/js/uploader.js')}}" type="text/javascript"></script>
<div class="container-fluid magelur_header">
  <section>
    <div class="container-fluid magelur_header">
      <div class=" ">
        <div id="upload-demo-i-cover">
        @if($user_type == 'agent')
        {!! SiteHelpers::user_avatars($user->id,'agent',$user->profile_cover,'/uploads/agency_profile/cover/') !!}
        @elseif($user_type == 'model')
        {!! SiteHelpers::user_avatars($user->id,'model',$user->profile_cover,'/uploads/agency_profile/cover/') !!}
        @endif
        </div>
      </div>
    </div>
    <div class="banner_title wow bounce ourmod_cnt pents">
    <h1> @if($user_type == 'agent') {{$user->username}}  @elseif($user_type == 'model')  {{$user->firstname}} @endif {{ Lang::get('core.video_gallery') }}</h1>
    <!-- <ul class="link_list">
      <li class="active"><a href="{{asset('/home')}}">HOME</a></li>
      <li>//</li>
      <li><a href="javascript:void(0)">Videos</a></li>
    </ul> -->
    <div class="two_laid"><a href="{{asset('/home')}}">HOME</a><p>Videos</p></div>
    </div>
  </section>

</div>
<!-- Display Error message for Video limit -->
  @if (\Session::has('messagetext'))
    <input type="hidden" id="msg" value="{!! Session::get('messagetext') !!}">
  @elseif(\Session::has('messagedelete'))
    <input type="hidden" id="msg" value="{!! Session::get('messagedelete') !!}">
  @else
      <input type="hidden" id="msg" value="">   
  @endif
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- end -->

<!-- Home Page -->
<div id="home">
<div class="video_listing_page">
<div class="agency_video_blk">
<div class="container">
<div class="agent_video_content col-md-12 col-sm-12 col-xs-12">
<div class="crave_image_div">

@if($user_type == 'agent')
<img src="{{ asset('uploads/agency_profile/'.$user->profile_photo) }}" alt="{{$user->username}}"/>
@elseif($user_type == 'model')
<img src="{{ asset('uploads/model_profile/'.$user->profile_photo) }}" alt="{{$user->username}}"/>
@else
<img src="" alt="No Preview"/>
@endif
</div>

@if($user_type == 'agent')
<div class="agency_video_title">
<h1>{{$user->username}} {{ Lang::get('core.agent_video') }} </h1>
</div>
@elseif($user_type == 'model')
<div class="agency_video_title">
<h1>{{$user->firstname}} {{ Lang::get('core.model_video') }}</h1>
</div>
@endif
</div>

</div>
</div>
<div class="agency_video_lisiting">
<div id="videosScroll">

        @if (count($videos) > 0)
         <div id="post">
            <input type="hidden" name="tot_vid" id="tot_vid" value="{{ $total_videos }}">
            <input type="hidden" name="disp_vid" id="disp_vid" value="{{ count($videos) }}">
            <input type="hidden" name="userid" id="userid" value="{{ $userId }}">
            <input type="hidden" name="videouserid" id="videouserid" value="{{ $videos[0]->user_id }}">
            @if($owner == 1)

           <div class=" gallery_video">
            <div class="magelur_gallery_div magelur_gallery_video_div  magelur_add_video_div col-md-4 col-sm-4 col-xs-12">
              <div class="magelur_image_blk magelur_video_blk">
                <div class="add_videos_blk">
                    <a href="{{ url('upload') }}" class="video_click_div">
                    <span class="add_icon_blk"><i class="fa fa-plus-circle"></i>
                        
                    </span>
                    <h1>{{ Lang::get('core.add_videos') }}</h1>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif

        @foreach ($videos as $video)

        <!-- Random Video Box -->






        <div class=" gallery_video">
            
                    <div class="magelur_gallery_div magelur_gallery_video_div col-md-4 col-sm-4 col-xs-12">

                        <div class="magelur_image_blk magelur_video_blk">
                            <!-- Video Thumb -->
                            <img src="{{ $video->preview }}" class="gallery_images1 gallery_video_images1">
                            <!-- <img src="{{ $video->thumb }}" title="{{ $video->title }}"> -->
                            <div class="img_hover_blk">
                            <a href="{{ url('/video/'.$video->video_id) }}" class="hover-posts" >
                                <div class="gallery_video_hvr">                                
                                    <span class="video_bg_clr">                                        
                                        <span></span>
                                        <i class="fa fa-play"></i>                                        
                                    </span>                                  
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>

                   

                    @if ($video->vip == 1)
                    <!-- Only VIP user -->
                    <div class="vip_only">
                        <h6>{{ Lang::get('core.vip_users') }}</h6>
                    </div>
                    @endif

                    
                    <div class="video-stats clearfix" style="display:none">

                        <!-- Video quality -->
                        @if (App\Helper\Helper::video_quality($video->video_id) == TRUE)
                        <div class="thumb-stats pull-left">
                            <h6>HD</h6>
                        </div>
                        @endif

                        <div class="thumb-stats pull-right">
                            <span>{{ $video->duration }}</span>
                </div>
                <div class="post-des" style="display:none">
                    <h6><a href="{{ url('/video') }}/{{ $video->video_id }}">{{ $video->title }}</a></h6>
                    <div class="post-stats clearfix">
                        <p class="pull-left">
                            <span><a class="uploader" href="{{ url('/user') }}/{{ App\Helper\Helper::username_by_id($video->user_id,$video->user_type) }}">{{ App\Helper\Helper::channel_by_id($video->user_id,$video->user_type) }}</a></span>
                        </p>
                        <p class="pull-right">
                            <span class="views">{{ App\Helper\Helper::count_views($video->video_id) }}</span>
                        </p>
                    </div>
                    <div class="post-summary">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
        </div>

        @else
        <!-- If No Agent Exists Show Error Msg -->
        <div class="error_msg_page error_msg_page_space">
        <div class="empty_vertical">
        <div class="empty_msg_details clearfix">
          <div class="empty_msg_blk clearfix">
            <div class="heading_blk">
              <div class="heading_details">
                <h1>Notice!</h1>
                <h2>
                 Oops! No Videos Right Now Or Admin needs to Approve your Videos!.Goto the upload page and more videos. So your videos will get more attention from models and agents...
              </h2>
          </div>
          <div class="file_img_blk">
            <div class="file_img_div">
<!--               <img src="{{ asset('images/file_image1.png')}}" alt="file_image" /> -->
              <i class="fa fa-warning"></i>
          </div>
      </div>
  </div>
  <!-- <p>You can search on the bar below or return to home page</p> -->
  <div class="pages_blk">
      <div class="msg_previous_page">
        <a href="{{ url('/home') }}"><span>Home</span><img src="{{ asset('images/preview_image1.png')}}" alt="next_image" /></a>
    </div>
    @if($user_type == 'agent'  && $user_vid_id == \Session::get('logged_id'))
    <div class="msg_next_page">

        <a href="{{ url('/upload') }}"><span>Video-Upload</span><img src="{{ asset('images/next_image1.png')}}" alt="next_image" /></a>    
    </div>

    @endif
    @if($user_type == 'model'  && $user_vid_id == \Session::get('logged_id'))
    <div class="msg_next_page">

        <a href="{{ url('/upload') }}"><span>Video-Upload</span><img src="{{ asset('images/next_image1.png')}}" alt="next_image" /></a>    
    </div>

    @endif
</div>
</div>
</div>
</div>
</div>


        @endif

<!-- <div class="col-sm-4">  
<div class="col-md-12 col-sm-12 col-xs-12 pagination_blk">  

</div>
</div> -->
 @if (count($videos) > 0)
<div class="load_more_btn text-center col-md-12 col-sm-12 col-xs-12">
<div class="pagination_content"> 
{!! str_replace('/?', '?', $videos->appends(Input::except('page'))->render()) !!}
</div>
</div>
@endif
</div>
</div>
    
    </div>

</div>

@else
<!-- If No Agent Exists Show Error Msg -->
<div class="error_msg_page">
 <div class="empty_vertical">
    <div class="empty_msg_details clearfix">
      <div class="empty_msg_blk clearfix">
        <div class="heading_blk">
          <div class="heading_details">
            <h1>Access Denied!</h1>
            <h2>
              You have selected the invalid agent profile.Please Go Back to Agent Listing to select the valid agent profile
          </h2>
      </div>
      <div class="file_img_blk">
        <div class="file_img_div">
          <i class="fa fa-warning"></i>
      </div>
  </div>
</div>
<!-- <p>You can search on the bar below or return to home page</p> -->
<div class="pages_blk">
  <div class="msg_previous_page">
    <a href="{{ url('/home') }}"><span>Home</span><img src="{{ asset('images/preview_image1.png')}}" alt="next_image" /></a>
</div>
<div class="msg_next_page">

    <a href="{{ url('/agencies') }}"><span>Agent-Listing</span><img src="{{ asset('images/next_image1.png')}}" alt="next_image" /></a>    
</div>
</div>
</div>
</div>
</div>
</div>
<!-- < -->
@endif
<script type="text/javascript">
$(function(){
$('.map').hide();
});

if($('#msg').val()!="")
{
  swal($('#msg').val());
}

else if($('#tot_vid').val()!=$('#disp_vid').val() && $('#userid').val()==$('#videouserid').val())
{
  swal("All Your Videos will be Visible only after Admin approves your video");
}
</script>
