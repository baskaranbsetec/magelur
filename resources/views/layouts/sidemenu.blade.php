	  <?php $sidebar = SiteHelpers::menus('sidebar') ;?>
<nav role="navigation" class="navbar-default navbar-static-side magelur_left_navbar">
     <div class="sidebar-collapse">				  
       <ul id="sidemenu" class="nav expanded-menu">
		<li class="logo-header" >
		 <a  href="{{ URL::to('dashboard')}}" >
		 	@if(file_exists(public_path().'/magelur/images/'.CNF_LOGO) && CNF_LOGO !='')
		 	<img src="{{ asset('magelur/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
		 	@else
			<img src="{{ asset('magelur/images/logo.png')}}" alt="{{ CNF_APPNAME }}"  />
			@endif
		 </a>
		 <!-- <a data-toggle=""  href="{{ url('dashboard') }}"> <i class="icon-home3"></i> </a>
		 <a class="dropdown-toggle" href="{{ url('notification') }}"> <i class="icon-bell"></i><span class="notif-alert ">0</span>  </a> -->
		</li>
		<!-- <li class="nav-header" style="display:none !mportant;">
			<div class="dropdown profile-element" style="text-align:center;"> <span>
				{!! SiteHelpers::avatar() !!} 
				 </span>
			
				<a href="{{ URL::to('user/profile') }}" >
				<span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Session::get('fid') }}</strong>
				 <br />
				{{ Lang::get('core.lastlogin') }} : <br />
				<small>{{ date("H:i F j, Y", strtotime(Session::get('ll'))) }}</small>				
				 </span> 
				 </span>
				 </a>
			</div>
			<div class="photo-header "> {!! SiteHelpers::avatar( 40 ) !!} </div>
		</li>  -->
		<li>
		<div class="prf_image">
		 	
			{!! SiteHelpers::avatar( 40 ) !!}			
<!-- 		<img src="images/dashboard_img1.png">
 -->		<h1>{{ Session::get('fid')}}</h1>
		<h2><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Ho Chi Minh City, Vietnam</h2>
		</div>
		</li>


		@foreach ($sidebar as $menu)
			
{{--*/ $activeSideMenu = SiteHelpers::checkActiveSideMenu() /*--}}

@if(Request::segment(2) !='' && Request::segment(1) !='') 
{{--*/ $segment= (Request::segment(1)."/".Request::segment(2) )/*--}}

			 <li @if($segment == $menu['module'] || $activeSideMenu == $menu['menu_id']) class="active" @endif>
@else
			 <li @if(Request::segment(1) == $menu['module'] || $activeSideMenu == $menu['menu_id']) class="active" @endif>
			 	
@endif


			@if($menu['module'] =='separator')
			<li class="separator"> <span> {{$menu['menu_name']}} </span></li>
				
			@else
			 	<a 
					@if($menu['menu_type'] =='external')
						href="{{ $menu['url'] }}" 
					@else
						href="{{ URL::to($menu['module'])}}" 
					@endif				
			 	
				 @if(count($menu['childs']) > 0 ) class="expand level-closed" @endif>
				 	<i class="{{$menu['menu_icons']}}"></i> <span class="nav-label">
					
					@if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]))
						{{ $menu['menu_lang']['title'][Session::get('lang')] }}
					@else
						{{$menu['menu_name']}}
					@endif						
					
					</span> @if(count($menu['childs']) > 1 )<span class="fa arrow"></span> @endif	 
				</a> 
				@endif	
				@if(count($menu['childs']) > 0)
					<ul class="nav nav-second-level">
						@foreach ($menu['childs'] as $menu2)
						{{--*/ $activeSideChildMenu = SiteHelpers::checkActiveSideChildMenu() /*--}}
						 @if(Request::segment(2) !='' && Request::segment(1) !='') 


{{--*/ $segment= (Request::segment(1)."/".Request::segment(2) )/*--}}

			 <li @if($segment == $menu2['module'] || $activeSideChildMenu == $menu2['menu_id']) class="active" @endif>
@else
			 <li @if(Request::segment(1) == $menu2['module'] || $activeSideChildMenu == $menu2['menu_id']) class="active" @endif>
			 	
@endif
						 	<a 
								@if($menu2['menu_type'] =='external')
									href="{{ $menu2['url']}}" 
								@else
									href="{{ URL::to($menu2['module'])}}"  
								@endif									
							>
							
							<i class="{{$menu2['menu_icons']}}"></i>
							@if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
								{{ $menu2['menu_lang']['title'][Session::get('lang')] }}
							@else
								{{$menu2['menu_name']}}
							@endif	
							</a> 
							@if(count($menu2['childs']) > 0)
							<ul class="nav nav-third-level">
								@foreach($menu2['childs'] as $menu3)
									<li @if(Request::segment(1) == $menu3['module']) class="active" @endif>
										<a 
											@if($menu['menu_type'] =='external')
												href="{{ $menu3['url'] }}" 
											@else
												href="{{ URL::to($menu3['module'])}}" 
											@endif										
										
										>
										<i class="menu-icon fa fa-caret-right"></i>
										<i class="{{$menu3['menu_icons']}}"></i> 
										@if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][Session::get('lang')]))
											{{ $menu3['menu_lang']['title'][Session::get('lang')] }}
										@else
											{{$menu3['menu_name']}}
										@endif											
											
										</a>
									</li>	
								@endforeach
							</ul>
							@endif							
						</li>							
						@endforeach
					</ul>
				@endif
			</li>
		@endforeach
      </ul>
	</div>
</nav>	  
	  
