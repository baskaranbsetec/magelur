<!DOCTYPE>
<html lang="en">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head >
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
   
    <title>{{ CNF_APPNAME }} | {{ $pageTitle}}</title>
    <meta name="keywords" content="{{ $pageMetakey }}" />
    <meta name="description" content="{{ $pageMetadesc }}" />
    <meta name="Author" content="Mangopik [www.mangopik.com]" />
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon"> 

<meta property="og:title" content="Magelur"/>

@if ($urls)
<meta property="og:type" content="product"/>
<meta property="og:url" content="{{ $urls }}"/>
@endif 

@if ($fbimg)
<meta property="og:image" content="{{ $fbimg }}"/>
<meta property="og:image:secure_url" content="{{ $fbimg }}" />

<meta itemprop="name" content="Magelur">
<meta itemprop="image" content="{{ $fbimg }}">
@endif 
<meta property="og:description" content="Magelur Share"/>

@if ($v_url)
<meta property="og:type" content="video"> <!-- site/page type more information http://ogp.me/ -->
<meta name="twitter:site" content="@nytimes">
<meta name="twitter:creator" content="@SarahMaslinNir">
<meta property="og:video:type" content="application/x-shockwave-flash"> 
<meta property="og:video:width" content="100px"> <!-- player width -->
<meta property="og:video:height" content="100px"> <!-- player height -->
<meta property="og:video" content="{{ $v_url }}"> 
<meta property="og:url" content="{{ $v_url }}"/>
@endif 

@if ($tweetimg)
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Magelur">
<meta name="twitter:description" content="Magelur Share">
<meta name="twitter:image" content="{{ $tweetimg }}">
@endif

@if ($gplusimg)
<meta itemprop="name" content="Magelur">
<meta itemprop="image" content="{{ $gplusimg }}">
@endif

      <link href="{{ asset('frontend') }}/default/css/owl.theme.default.min.css" rel="stylesheet">
      <link href="{{ asset('frontend') }}/default/css/style.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/animate.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/lightslider.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="{{ asset('frontend') }}/default/css/agency.css" rel="stylesheet">

         <!-- video upload Styles -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/reset.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('magelur/css/video/style.css')}}"> -->
    <link rel="stylesheet" href="{{ url('/') }}/content/assets/css/watch.css">
    <link rel="stylesheet" type="text/css"  href="{{ asset('magelur/css/video/forms.css')}}">

    <link rel="stylesheet" type="text/css"  href="{{ asset('magelur/css/video/black.css')}}">

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <link rel="stylesheet" href="{{ asset('magelur/css/video/tooltip.css')}}">

    <link rel="stylesheet" href="{{ asset('magelur/css/video/custombox.css')}}">

    <!-- HTML5 Player Style -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/html5player.css')}}" type="text/css" media="screen"/>

    <!-- HTML5 Player Font Awesome -->
    <link rel="stylesheet" href="{{ asset('magelur/css/video/player-font-awesome.css')}}" type="text/css" media="screen"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/toastr.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/jquery.fileuploader.min.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('magelur/css/video/jquery.fileuploader-theme-dragdrop.css')}}" type="text/css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('magelur/js/jquery-ui.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/bootstrap.min.js"></script>


    <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script> -->

    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/lightslider.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/custom.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/modernizr.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/default/js/owl.carousel.js"></script>

    <script type="text/javascript" src="{{ asset('magelur/js/slim.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('magelur/js/plugins/toastr/toastr.css')}}" />
    <script type="text/javascript" src="{{ asset('magelur/js/plugins/toastr/toastr.js')}}"></script>

    <script>
      new WOW().init();
    </script>
      <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBm_C6H7PSs46kG6BxxgR4BzmRU_BJ8Nmo&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset('magelur/js/geocomplete.js') }}"></script>
<!-- Message -->
<script type="text/javascript" src="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
<link href="{{ asset('magelur/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<link href="{{ asset('frontend') }}/default/css/lightslider.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/lightslider.js"></script>
<script type="text/javascript" src="{{ asset('magelur/js/plugins/select2/select2.min.js')}}"></script>
<link href="{{ asset('magelur/js/plugins/select2/select2.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('magelur/js/autocomplete.js')}}"></script>

<!-- <script src="{{ asset('magelur/js/jquery.validate.min.js')}}"></script> -->
<!-- <script src="{{ asset('magelur/js/valid.js')}}"></script> -->
<script src="{{ asset('magelur/js/jquery_1.11.1.validate.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('magelur/js/plugins/jquery.form.js')}}"></script>  
<script type="text/javascript" src="{{ asset('frontend') }}/default/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="{{ asset('magelur/message/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-process.js')}}"></script>
<script type="text/javascript" src="{{ asset('magelur/message/jquery.fileupload-validate.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- <script src="{{asset('frontend/default/js/jquery.barrating.min.js')}}" type="text/javascript"></script>
<link href='{{asset("frontend/default/css/fontawesome-stars.css")}}' rel='stylesheet' type='text/css'>
 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.1/jquery.barrating.min.js" type="text/javascript"></script>
<link href='https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.1/themes/fontawesome-stars.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.1/themes/bars-reversed.min.css' rel='stylesheet' type='text/css'>

</head>
<body data-root="{{ url('/') }}">
  <header>
  <div class="header_block col-md-12 col-sm-12 col-xs-12">
<div class="logo">
            <h1>
              <!-- <a href="javascript:void(0)"><img src="{{ asset('images/Asset 2.png') }}"></a> -->
<a  href="{{ URL::to('/home')}}" >
      @if(file_exists(public_path().'/magelur/images/'.CNF_FLOGO) && CNF_FLOGO !='')
      <img src="{{ asset('magelur/images/'.CNF_FLOGO)}}" alt="{{ CNF_APPNAME }}" />
      @else
      <img src="{{ asset('images/Asset 2.png') }}" alt="{{ CNF_APPNAME }}"  />
      @endif
     </a>
            </h1>
          </div>  
<div class="menu clearfix wow SlideInDown">
  {{--*/ $menus = SiteHelpers::menus('top') /*--}}


    <ul class="nav navbar-nav magelur_menulist">
      @foreach ($menus as $menu)
      {{--*/ $activeParentMenu = SiteHelpers::checkActiveParentMenu($menu['menu_id']) /*--}}
      <li class="header_menu_list @if(Request::is($menu['module'])) active @endif @if($activeParentMenu) active @endif"><!-- HOME -->
        <a 
        @if($menu['menu_type'] =='external')
          href="{{ URL::to($menu['url'])}}" 
        @else
          href="{{ URL::to($menu['module'])}}" 
        @endif
       
         @if(count($menu['childs']) > 0 ) class="dropdown-toggle" data-toggle="" @endif>
          <i class="{{$menu['menu_icons']}}"></i> 
          
          @if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]) && $menu['menu_lang']['title'][Session::get('lang')]!='')
            {{ $menu['menu_lang']['title'][Session::get('lang')] }}
          @else
            {{$menu['menu_name']}}
          @endif  
        
          
         <!--  @if(count($menu['childs']) > 0 )
           <b class="caret"></b> 
          @endif  --> 
        </a> 

        @if(count($menu['childs']) > 0)
           <ul class="dropdown-menu pull-right menu_drop_down">


            @foreach ($menu['childs'] as $menu2)
             <li class="@if(Request::is($menu2['module'])) active @endif">
              <a 
                @if($menu2['menu_type'] =='external')
                  href="{{ URL::to($menu2['url'])}}" 
                @else
                  href="{{ URL::to($menu2['module'])}}" 
                @endif
                      
              >
                <i class="{{$menu2['menu_icons']}}"></i> 
                  @if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
                    {{ $menu2['menu_lang']['title'][Session::get('lang')] }}
                  @else
                    {{$menu2['menu_name']}}
                  @endif
                
              </a> 
              @if(count($menu2['childs']) > 0)
              <ul class="dropdown-menu pull-right menu_drop_down">
                @foreach($menu2['childs'] as $menu3)
                  <li @if(Request::is($menu3['module'])) class="active" @endif>
                    <a 
                      @if($menu3['menu_type'] =='external')
                        href="{{ URL::to($menu3['url'])}}" 
                      @else
                        href="{{ URL::to($menu3['module'])}}" 
                      @endif                    
                    
                    >
                      <span>
                      @if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][Session::get('lang')]))
                        {{ $menu3['menu_lang']['title'][Session::get('lang')] }}
                      @else
                        {{$menu3['menu_name']}}
                      @endif
                      
                      </span>  
                    </a>
                  </li> 
                @endforeach
              </ul>
              @endif              
              
            </li>             
            @endforeach
          </ul>
        @endif

      </li>
      @endforeach                 
    </ul>   
 
  <ul class="nav navbar-nav magelur_menulist">
           <!--  <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li>
            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/model') }}">Photo Gallery</a></li>
                
              </ul>
            </li> -->
            <!-- <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ouragencies') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/agent') }}">Photo Gallery</a></li>
                
              </ul>
            </li> -->
            <!--<li class="header_menu_list">
              <a href="{{ url('events/list') }}">EVENTS</a>
               <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> 
            </li>-->
        <!--   <li class="header_menu_list">
            <a href="#">ABOUT</a>
             <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('contactus') }}">Contact Us</a></li>
                <li><a tabindex="-1" href="{{ url('tellfriend') }}">Tell a friend</a></li>
                <li><a tabindex="-1" href="{{ url('terms') }}">Terms & conditions</a></li>
                <li><a tabindex="-1" href="{{ url('privacy-policy') }}">Privay policy</a></li>
                <li><a tabindex="-1" href="{{ url('faqs') }}">FAQs</a></li>
                <li><a tabindex="-1" href="{{ url('feedback') }}">Feedbacks/Suggestions</a></li>
                <li><a tabindex="-1" href="{{ url('advertise') }}">Advertise with us</a></li>
                <li><a tabindex="-1" href="{{ url('blogs/list') }}">Blogs</a></li>
                
              </ul>
            </li>-->
            
            @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li class="{{ Request::path() == 'dashboard' ? 'active' : '' }}"><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))
 
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline myaccount" data-toggle="dropdown" href="#">MY ACCOUNT</a>

              <ul class="dropdown-menu pull-right menu_drop_down">
              <li class="{{ Request::path() == 'model-dashboard/'.Session::get('url_securityID') ? 'active' : '' }}" id="child_1" data-id="1"><a href="{{ URL::to('model-dashboard/'.Session::get('url_securityID'))}}" >My Dashboard</a></li>
              <li class="{{ Request::path() == 'model-profile/'.Session::get('model_name').'/'.Session::get('url_security') ? 'active' : '' }}" id="child_2" data-id="2"><a href="{{ URL::to('model-profile/'.Session::get('model_name').'/'.Session::get('url_security'))}}"> Model {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline myaccount" data-toggle="dropdown" href="#">MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li class="{{ Request::path() == 'agent-dashboard/'.Session::get('url_securityID') ? 'active' : '' }}" id="child_1" data-id="1"><a href="{{ URL::to('agent-dashboard/'.Session::get('url_securityID'))}}" >Dashboard</a></li>
              <li class="{{ Request::path() == 'agent-profile/'.Session::get('agent_name').'/'.Session::get('url_security') ? 'active' : '' }}" id="child_2" data-id="2"><a href="{{ URL::to('agent-profile/'.Session::get('agent_name').'/'.Session::get('url_security'))}}">Agent {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list {{ Request::path() == 'login' ? 'active' : '' }} {{ Request::path() == 'agent/create' ? 'active' : '' }} {{ Request::path() == 'model/create' ? 'active' : '' }}">
              <a  href="{{ url('login') }}">JOIN US</a>
              </li>

            @endif

       
            

  </ul>
  <div class="search"><a href="{{ url('model/filter') }}" id="search-toggle"></a></div>
  
</div>

<!-- Mobile View -->
          <div class="mobile_menu">
              <div id="main" class="main_mobile">
                <ul class="mob-contant">
                  <li>
                  <div class="menu_btn" style="font-size:13px;cursor:pointer" onclick="openNav()">
                  <div class="mener">Menu</div>
                  <div class="rights_bar"><div class="icon-bar"></div>
                  <div class="icon-bar last_led"></div>
                </div>
                </div>
                </li>
                <li>
                  <div class="menu">
                    <div class="search"><a href="{{ url('model/filter') }}" id="search-toggle-mobile"></a></div>
                     <!-- <div class="search_box">
                          <form action="">
                              <input type="search" placeholder="Search.." name="search">
                          </form>
                    </div> -->
                </div>
                </li>
              </ul>
            </div>
              
            <div id="mySidenav" class="sidenav">
              <ul class="menus magelur_menulist">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                {{--*/ $menus = SiteHelpers::menus('top') /*--}}

    <ul class="menus magelur_menulist">
      @foreach ($menus as $menu)
      <li class="header_menu_list"><!-- HOME -->
        <a 
        @if($menu['menu_type'] =='external')
          href="{{ URL::to($menu['url'])}}" 
        @else
          href="{{ URL::to($menu['module'])}}" 
        @endif
       
         @if(count($menu['childs']) > 0 ) class="dropdown-toggle" data-toggle="" @endif>
          <i class="{{$menu['menu_icons']}} fa fa-plus"></i> 
          
          @if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]) && $menu['menu_lang']['title'][Session::get('lang')]!='')
            {{ $menu['menu_lang']['title'][Session::get('lang')] }}
          @else
            {{$menu['menu_name']}}
          @endif  
        
          
         <!--  @if(count($menu['childs']) > 0 )
           <b class="caret"></b> 
          @endif  --> 
        </a> 

        @if(count($menu['childs']) > 0)
           <ul class="dropdown-menu pull-right menu_drop_down">
            @foreach ($menu['childs'] as $menu2)
             <li>
              <a 
                @if($menu2['menu_type'] =='external')
                  href="{{ URL::to($menu2['url'])}}" 
                @else
                  href="{{ URL::to($menu2['module'])}}" 
                @endif
                      
              >
                <i class="{{$menu2['menu_icons']}}"></i> 
                  @if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
                    {{ $menu2['menu_lang']['title'][Session::get('lang')] }}
                  @else
                    {{$menu2['menu_name']}}
                  @endif
                
              </a> 
              @if(count($menu2['childs']) > 0)
              <ul class="dropdown-menu pull-right menu_drop_down">
                @foreach($menu2['childs'] as $menu3)
                  <li @if(Request::is($menu3['module'])) class="active" @endif>
                    <a 
                      @if($menu3['menu_type'] =='external')
                        href="{{ URL::to($menu3['url'])}}" 
                      @else
                        href="{{ URL::to($menu3['module'])}}" 
                      @endif                    
                    
                    >
                      <span>
                      @if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][Session::get('lang')]))
                        {{ $menu3['menu_lang']['title'][Session::get('lang')] }}
                      @else
                        {{$menu3['menu_name']}}
                      @endif
                      
                      </span>  
                    </a>
                  </li> 
                @endforeach
              </ul>
              @endif              
              
            </li>             
            @endforeach
          </ul>
        @endif

      </li>
      @endforeach                 
    </ul> 
                 <!-- <li class="header_menu_list"><a href="{!! url('') !!}">HOME</a></li> -->
           <!--  <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">MODELS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ourmodel') }}">Models Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/model') }}">Photo Gallery</a></li>
                
              </ul>
            </li> -->
           <!--  <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">AGENCIES</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('ouragencies') }}">Agents Listing</a></li>
                <li><a tabindex="-1" href="{{ url('album/agent') }}">Photo Gallery</a></li>
                
              </ul>
            </li> -->
           <!--  <li class="header_menu_list">
              <a  href="{{ url('events/list') }}">EVENTS</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Upcoming Events</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> 
            </li> -->
           <!-- <li class="header_menu_list"> -->
              <!-- <a  href="#" data-toggle="dropdown" class="dropdown-toggle no-text-underline" >ABOUT</a> -->
                 <!-- <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('contactus') }}">Contact Us</a></li>
                <li><a tabindex="-1" href="{{ url('tellfriend') }}">Tell a friend</a></li>
                <li><a tabindex="-1" href="{{ url('terms') }}">Terms & conditions</a></li>
                <li><a tabindex="-1" href="{{ url('privacy-policy') }}">Privay policy</a></li>
                <li><a tabindex="-1" href="{{ url('faqs') }}">FAQs</a></li>
                <li><a tabindex="-1" href="{{ url('feedback') }}">Feedbacks/Suggestions</a></li>
                <li><a tabindex="-1" href="{{ url('advertise') }}">Advertise with us</a></li>
             <li><a tabindex="-1" href="{{ url('blogs/list') }}">Blogs</a></li> 
                
              </ul>-->
               
             <!--  <ul class="dropdown-menu pull-right menu_drop_down">
                <li><a tabindex="-1" href="{{ url('dashboard') }}">About Us</a></li>
                <li><a tabindex="-1" href="{{ url('dashboard') }}">Be a Model</a></li>
                <li><a tabindex="-1" href="{{ url('user/profile?view=frontend') }}">Looking for Models?</a></li>
                
              </ul> -->
            <!-- </li> -->
              @if(\Auth::check())
    
              <li class="header_menu_list">
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">{{ Session::get('fid')}} </a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li class="{{ Request::path() == 'dashboard' ? 'active' : '' }}"><a href="{{ URL::to('dashboard')}}" >Dashboard</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href=""> {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>

            @elseif( \Session::has('model_id'))

              <li class="header_menu_list">
              <span class="layover"></span>
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-plus"></i>MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
               <li class="{{ Request::path() == 'model-dashboard/'.Session::get('url_securityID') ? 'active' : '' }}"><a href="{{ URL::to('model-dashboard/'.Session::get('url_securityID'))}}" >My Dashboard</a></li>
                
              <li class="{{ Request::path() == 'model-profile/'.Session::get('model_name').'/'.Session::get('url_security') ? 'active' : '' }}"><a href="{{ URL::to('model-profile/'.Session::get('model_name').'/'.Session::get('url_security'))}}"> Model {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>
              </ul>
              </li>
            @elseif( \Session::has('agent_id'))

            <li class="header_menu_list">
            <span class="layover"></span>
              <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-plus"></i>MY ACCOUNT</a>
              <ul class="dropdown-menu pull-right menu_drop_down">
              <li class="{{ Request::path() == 'agent-dashboard/'.Session::get('url_securityID') ? 'active' : '' }}"><a href="{{ URL::to('agent-dashboard/'.Session::get('url_securityID'))}}" >Dashboard</a></li>
              <li class="{{ Request::path() == 'agent-profile/'.Session::get('agent_name').'/'.Session::get('url_security') ? 'active' : '' }}"><a href="{{ URL::to('agent-profile/'.Session::get('agent_name').'/'.Session::get('url_security'))}}">Agent {{ Lang::get('core.m_profile') }}</a></li>
              <li class="{{ Request::path() == 'user/logout' ? 'active' : '' }}"><a href="{{ URL::to('user/logout')}}">{{ Lang::get('core.m_logout') }}</a></li>

              </ul>
              </li>
            @else
              <li class="header_menu_list">
              <a  href="{{ url('login') }}">JOIN US</a>
              <!-- <ul class="dropdown-menu pull-right menu_drop_down">
              <li><a tabindex="-1" href="{{ url('/model/create') }}">Model Register</a></li>
              <li><a tabindex="-1" href="{{ url('/agent/create') }}">Agent Register</a></li>

              </ul> -->
              </li>
              </ul>

            @endif
            </div>
          </div>
          </div>
</header>

@include($pages)

<!-- <div class="map">
 <a href=""><img src="{{ asset('images/map.jpg') }}"></a>
</div> -->

<footer>
<div class="footer">
 <div class="container">
  <div class="clearfix">
   <div class="col-sm-7 wow SlideInUp">
    <div class="post">
     <p>{{$links['address']}}</p>
     <ul>
      <li><a href="{{ url('home') }}">HOME</a></li>
            <li><a href="{!! url('aboutus') !!}">ABOUT US</a></li>
            <li><a href="{{ url('/models') }}">OUR MODELS</a></li>
            <li><a href="{{ url('/privacy-policy') }}">PRIVACY</a></li>
            <li><a href="{{ url('/login') }}">APPLY NOW</a></li>
            <li><a href="{{ url('contactus') }}">CONTACT</a></li>
           
          
     </ul>
    </div>
   </div>
   <div class="col-sm-5 wow SlideInDown">
    <!-- start newsletter -->
     {!! Form::open(array('url'=>'newsletter/subscribe', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=>'newsletter')) !!}

    <div class="mail_adress clearfix">
     <div class="leftmail">
      <input type="text" name="newsletter" id="newsletter" placeholder="Email Address"><!-- <p>Email Address</p>  -->
     </div>
     <div class="submit_mail"><input type='submit' value='submit'></div>
    </div>

    {!! Form::close() !!}
    <!-- end newsletter -->
    <div class="social_icons">
     <ul>
      <li>we are social</li>

      <li><a href="{{ url($links['fb']) }}" target='_blank'><i class="fa fa-facebook"></i></a></li>
      <li><a href="{{$links['twitter']}}" target='_blank'><i class="fa fa-twitter"></i></a></li>
      <li><a href="{{$links['insta']}}" target='_blank'><i class="fa fa-instagram"></i></a></li>
      <li><a href="{{$links['ytube']}}" target='_blank'><i class="fa fa-linkedin"></i></a></li>
     </ul>
    </div>
   </div>
  </div>
 </div>
</div>
</footer>
</body>

<script type="text/javascript">
 
  var owl = $('.owl-carousel.models').owlCarousel({
    loop:true,
    margin:25,
    nav:false,
    autoplay:true,
    responsive:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
         800:{
            items:3
        },
        1000:{
            items:3
        }
    }
})

$(document).ready(function(){
    $(".customNavigation .btn.btn-primary.next").click(function(){
      // $('#owl-properties .owl-next')[0].click();
    })
    $(".customNavigation .btn.btn-primary.prev").click(function(){
      // $('#owl-properties .owl-prev')[0].click();
    })

    var winhgt = $(document).height();
    var winbody = $("body").height();
    if (winbody <= winhgt){
      $('body').addClass('footer_absolute');
    }
  
});
</script>

<script type="text/javascript">
        $(document).ready(function() {
        $('#vertical').lightSlider({
          item:1,
          vertical:true,
          adaptiveHeight:true,
          verticalHeight:180

        });  
      });
</script>

<script>
// document.getElementById("uploadBtn").onchange = function () {
// document.getElementById("uploadFile").value = this.value; 
// };

// document.getElementById("uploadProfileBtn").onchange = function () {
// document.getElementById("uploadProfileFile").value = this.value; 
// };

// document.getElementById("uploadLengthBtn").onchange = function () {
// document.getElementById("uploadLengthFile").value = this.value; 
// };
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "";
    document.getElementById("main").style.marginLeft= "";
}
$("#newsletter").validate({
     ignore: "not:hidden",
      rules: {

        newsletter: { 
          required:true,
          email:true,
          remote:{
          url:"{!! url('newsletter/checkuniqueEmail')!!}",
          type:'post',
          data:{
            email:function(){
              return $('#newsletter').val();
            },
            
          }
        } 
      },
      },

       messages: {
      newsletter: { required:'Please provide valid email to subscribe' },
       },
        submitHandler:function(form){
       
        form.submit();
        }
    });
var id=$('li.active').data("id"); 
// alert($('li#model_child_'+id).attr('class'))
if($('li#child_'+id).attr('class') == 'active')
{
  $('li.header_menu_list a.myaccount').addClass('active')
}
</script>
@if(Session::has('subscribed'))
<script>
toastr.success("{!! Session::get('subscribed') !!}");
</script>
@endif
</html>

