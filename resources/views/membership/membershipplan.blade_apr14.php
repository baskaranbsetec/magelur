
<div class="ourmodel_banner membt">
  <div class="ourmod_cnt">
    <h1>membership Plan</h1>
    <div class="two_laid"><a href="{{ url('/') }}">Home</a> <p> membership Plan</p></div>
  </div>
</div>

<div class="adevrtise_with memvr_adver membership_plan_blk">
 <div class="container">
   <div class="member_tab_structure">
     @if(!empty($loggedId))  
     <ul class="nav nav-tabs search_tabs">
      <li class="basic_search_li active"><a data-toggle="tab" href="#monthly_search">Monthly</a></li>
      <li class="advanced_search_li"><a data-toggle="tab" href="#yearly_search">Yearly</a></li>
     </ul>
     @else
     <ul class="nav nav-tabs search_tabs">
      <li class="basic_search_li active"><a data-toggle="tab" href="#model_search">Model</a></li>
      <li class="advanced_search_li"><a data-toggle="tab" href="#agent_search">Agent</a></li>
     </ul>
     @endif

    <div class="col-md-offset-0 col-md-12 col-sm-12">
      <div class="terms_contant">
        <div class="terms_details">
          <h3><span></span>Our membership Plan</h3>
          <p>We are seeing an increasing number of people wanting to break away from more traditional models and rather than just call this a hybrid model as we have previously, we decided that what was actually needed was a new approach to membership models.</p>

        </div>
      </div>
    </div>
    <!-- show membership for logged users -->
    @if(!empty($loggedId))
    <div class="tab-content">
      <div id="monthly_search" class="tab-pane fade in search_user_div active">
        <!-- Monthly Plan Details -->
        <div class="three_plans clearfix">
         @foreach($memberships as $membership)
         {!! Form::open(array('url' => url('membership/buy-membership-plan'),'id'=>'payment-stripe' )) !!}
         <input type="hidden" name="plan_membership" id="plan_membership" value="">
         @if($membership->plan_id == $plan_id)
         <div class="first_plan highlight plan-id-{{$membership->plan_id}}">
         @else
         <div class="first_plan plan-id-{{$membership->plan_id}}">
         @endif
          <div class="start_bg">
            <h1>{{$membership->plan_name}}</h1>
            <div class="sbase"> 
              <div class="traingle">
               <div class="mini_traingle"></div>  
             </div>
             @if($membership->status == 1)
             <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
               <p>MONTHLY</p>
             </div>
             <input type="hidden" name="membership_type" value="m">
             @endif
           </div>
           <div class="content-withs">  
            <ul>
              <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
              @if($membership->type == 'agent')
              <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
              @elseif($membership->type == 'model') 
              <li class="model-desc">Contact Agents & Models </li>
              @endif 
              <li class="photo-limit">Can upload {{ $membership->image_limit }} images</li>
            </ul>
          </div> 
          @if($membership->plan_id != $plan_id)
          <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}'>BUY NOW</a></div> 
          @else
           <div class="fre_try"><a href="#" onclick="return false;">ACTIVE PLAN</a></div>
          @endif
        </div>
      </div> 
      {!! Form::close() !!}
      @endforeach
    </div> 
  </div>

  <div id="yearly_search" class="tab-pane fade advanced_user_div">
    <!-- Yearly Plan Details -->
    <div class="three_plans clearfix">
     @foreach($memberships as $membership)
     {!! Form::open(array('url' => url('membership/buy-membership-plan'),'id'=>'payment-stripe' )) !!}
     <input type="hidden" name="plan_membership" id="plan_membership" value="">

     <div class="first_plan plan-id-{{$membership->plan_id}}">
      <div class="start_bg">
        <h1>{{$membership->plan_name}}</h1>
        <div class="sbase"> 
          <div class="traingle">
           <div class="mini_traingle"></div>  
         </div>
         @if($membership->status_y == 1)
         <div class="pre_price"><h1>${{ ($membership->plan_amount_y)}}</h1>
           <p>YEARLY</p>
         </div>
         <input type="hidden" name="membership_type" value="m">
         @endif
       </div>
       <div class="content-withs">  
        <ul>
          <li>Use {{ ($membership->video_limit * 12) }} Videos</li>
          @if($membership->type == 'agent')
          <li>Advertise {{ ($membership->event_limit * 12) }} events </li>
          @elseif($membership->type == 'model') 
          <li>Contact Agents & Models </li>
          @endif 
          <li>Can upload {{ ($membership->image_limit * 12) }} images</li>
        </ul>
      </div> 
      <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}'>BUY NOW</a></div> 
    </div>
  </div> 

  {!! Form::close() !!}
  @endforeach

</div> 
</div>
</div>

@endif


@if(empty($loggedId))

<div class="tab-content">
  <div id="model_search" class="tab-pane fade in search_user_div active">
    <!-- Monthly Plan Details -->
    <div class="three_plans clearfix">
     @foreach($memberships as $membership)
     <!-- list only membership model -->
     @if($membership->type == 'model')
     {!! Form::open(array('url' => url('model/create'),'id'=>'payment-stripe' )) !!}
     <input type="hidden" name="plan_membership" id="plan_membership" value="">
     <div class="first_plan plan-id-{{$membership->plan_id}}">
      <div class="start_bg">
        <h1>{{$membership->plan_name}}</h1>
        <div class="sbase"> 
          <div class="traingle">
           <div class="mini_traingle"></div>  
         </div>
         @if($membership->status == 1)
         <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
           <p>MONTHLY</p>
         </div>
         <input type="hidden" name="membership_type" value="m">
         @endif
         <div class="triangle_radio">
         <label class='agree radio radio-inline' onclick="showPlanDetails('monthly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
           <input type='radio' name='agree' 
           data-event = "{{ $membership->event_limit }}" 
           data-photo = "{{ $membership->image_limit }}" 
           data-video = "{{ $membership->video_limit }}" 
           data-price = "{{ $membership->plan_amount }}" 
           value ='agent' />  Monthly
           <span class="checkmark"></span>
         </label>
         <label class='agree radio radio-inline' onclick="showPlanDetails('yearly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','')">
           <input type='radio' name='agree' 
           data-price = "{{ $membership->plan_amount }}" 
           data-photo = "{{ $membership->image_limit }}" 
           data-video = "{{ $membership->video_limit }}" 
           value ='model' />  Yearly
           <span class="checkmark"></span>
         </label>
         </div>
       </div>
       <div class="content-withs">  
        <ul>
          <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
          @if($membership->type == 'agent')
          <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
          @elseif($membership->type == 'model') 
          <li class="model-desc">Contact Agents & Models </li>
          @endif 
          <li class="photo-limit">Can upload {{ $membership->image_limit }} images</li>
        </ul>
      </div> 

      <div class="fre_try">
        <a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}'>Login</a>
      </div> 

    </div>
  </div> 
  {!! Form::close() !!}
  @endif  
  @endforeach
</div> 
</div>

<div id="agent_search" class="tab-pane fade advanced_user_div">
  <!-- Yearly Plan Details -->
  <div class="three_plans clearfix">
   @foreach($memberships as $membership)
   <!-- list only membership model -->
   @if($membership->type == 'agent')
   {!! Form::open(array('url' => url('model/create'),'id'=>'payment-stripe' )) !!}
   <input type="hidden" name="plan_membership" id="plan_membership" value="">
   <div class="first_plan plan-id-{{$membership->plan_id}}">
    <div class="start_bg">
      <h1>{{$membership->plan_name}}</h1>
      <div class="sbase"> 
        <div class="traingle">
         <div class="mini_traingle"></div>  
       </div>
       @if($membership->status == 1)
       <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
         <p>MONTHLY</p>
       </div>
       <input type="hidden" name="membership_type" value="y">
       @endif
       <div class="triangle_radio">
       <label class='agree radio radio-inline' onclick="showPlanDetails('monthly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
         <input type='radio' name='agree' 
         data-event = "{{ $membership->event_limit }}" 
         data-photo = "{{ $membership->image_limit }}" 
         data-video = "{{ $membership->video_limit }}" 
         data-price = "{{ $membership->plan_amount }}" 
         value ='agent' />  YEARLY
         <span class="checkmark"></span>
       </label>
       <label class='agree radio radio-inline' onclick="showPlanDetails('yearly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
         <input type='radio' name='agree' 
         data-price = "{{ $membership->plan_amount }}" 
         data-photo = "{{ $membership->image_limit }}" 
         data-video = "{{ $membership->video_limit }}" 
         value ='model' />  Yearly
         <span class="checkmark"></span>
       </label>
       </div>
     </div>
     <div class="content-withs">  
      <ul>
          <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
          @if($membership->type == 'agent')
          <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
          @elseif($membership->type == 'model') 
          <li class="model-desc">Contact Agents & Models </li>
          @endif 
          <li class="photo-limit">Can upload {{ $membership->image_limit }} images</li>
      </ul>
    </div> 
    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}'>Login</a></div> 

  </div>
</div> 

{!! Form::close() !!}
@endif
@endforeach

</div> 
</div>
</div>

@endif


</div>


</div>  

</div>  


<style type="text/css">
.map{display: none;}
</style>

<script type="text/javascript">

  function showPlanDetails(type,id,price,photo,video,event){
    // get the selected plan div
    var parentEle = $('.plan-id-'+id);

    var activeTab = $('.basic_search_li').hasClass('active');

    if(activeTab){
      activeTab = "Model";
    }
    else{
      activeTab = "Agent";
    }

    if(type == 'yearly'){
      console.log(price)
       price = (price*12);
       video = (video*12);
       photo = (photo*12);
       
       if(event.match(/^-{0,1}\d+$/))
       event = (event*12);
       console.log(price)
    }
    console.log(parentEle.find('.pre_price h1'))
    // set price
    parentEle.find('.pre_price h1').html('$'+price);

    // set video count
    parentEle.find('.video-limit').html('Use '+ video +' Videos');

    // set photo count
    parentEle.find('.photo-limit').html('Can upload '+photo+' images');

    if(activeTab == 'Model'){
     // show model description
     parentEle.find('.model-desc').show();    
     // hide event count
     parentEle.find('.event-limit').html('Advertise '+event+' events').hide();
    }else{
     // hide model description
     parentEle.find('.model-desc').hide();    
     // hide event count
     parentEle.find('.event-limit').html('Advertise '+event+' events').show();
    }
    // alert(type);
    return false;
  }


 $(document).ready(function() {

  $('.add').click(function () {
   var vl = $(this).attr('data-value');
   var pl = $(this).attr('data-plan');

   $('#plan_membership').val(vl);
   $('#payment-stripe').submit();

 });

});
</script>
