@extends('layouts.app')

@section('content')
<style type="text/css">
/*label.error,.asterix{
color: #cc0000;
font-family: 'Open Sans',Arial,Helvetica,sans-serif;
font-weight: normal;
font-size: 13px;
}
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}*/
</style>

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Membership plan</h1> 
		<div class="sbox-tools " >
			<!-- @if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 	 -->		
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				sgsd<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'membership/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
	<fieldset>
		{!! Form::hidden('plan_id', $row['plan_id']) !!}

		<div class="form-group  " >
		<div class="col-md-7">
		<label class="agree">Agent
		<input type='radio' name='agree' value ='agent'  @if($row['type'] == 'agent') checked="checked" @else checked @endif >  
		<span class="checkmark"></span>
		</label>  
		<label class="agree">Model
		<input type='radio' name='agree' value ='model' @if($row['type'] == 'model') checked="checked" @endif >  
		<span class="checkmark"></span>
		</label>
		</div>
		</div>

		<div class="form-group  " >
		<label for="Plan Name" class=" control-label col-md-4 text-left"> Plan Name </label>
		<div class="col-md-7">
		{!! Form::text('plan_name', $row['plan_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Plan Statement" class=" control-label col-md-4 text-left"> Plan Statement </label>
		<div class="col-md-7">
		<textarea name='plan_statement' rows='2' id='plan_statement' class='form-control '
		required  >{{ $row['plan_statement'] }}</textarea> 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Monthly Plan Amount" class=" control-label col-md-4 text-left">Monthly Plan Amount </label>
		<div class="col-md-7">
		{!! Form::text('plan_amount', $row['plan_amount'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Status" class=" control-label col-md-4 text-left"> Monthly Plan Status </label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		<input type='radio' name='status' value ='1'  @if($row['status'] != '0') checked="checked" @endif > Active </label>
		<label class='radio radio-inline'>
		<input type='radio' name='status' value ='0'  @if($row['status'] == '0') checked="checked" @endif > Inactive </label> 
		</div> 
		</div>	

		<div class="form-group  " >
		<label for="Plan Amount Y" class=" control-label col-md-4 text-left"> Yearly Plan Amount</label>
		<div class="col-md-7">
		{!! Form::text('plan_amount_y', $row['plan_amount_y'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Status Y" class=" control-label col-md-4 text-left"> Yearly Plan Status  </label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		<input type='radio' name='status_y' value ='1'  @if($row['status_y'] != '0') checked="checked" @endif > Active </label>
		<label class='radio radio-inline'>
		<input type='radio' name='status_y' value ='0'  @if($row['status_y'] == '0') checked="checked" @endif > Inactive </label>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Image Limit" class=" control-label col-md-4 text-left"> Image Limit</label>
		<div class="col-md-7">
		
		@if($row['image_limit'] == 'unlimited' || empty($row['image_limit']))
		{!! Form::select('image_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		{!! Form::hidden('image_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		@else
		{!! Form::select('image_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control  margin_top_space', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		
		<!-- <span>Note:User can upload only limited images</span> -->

		{!! Form::text('image_limit', $row['image_limit'],array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 		
		
		@endif

		</div> 
		</div>

		<div class="form-group " >
		<label for="Video Limit" class=" control-label col-md-4 text-left"> Video Limit</label>
		<div class="col-md-7">
		@if($row['video_limit'] == 'unlimited' || empty($row['video_limit']))
		{!! Form::select('video_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		{!! Form::hidden('video_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		@else
		{!! Form::select('video_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		{!! Form::text('video_limit', $row['video_limit'],array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 		
		@endif
		</div> 
		</div> 		

		<div class="form-group event_limit_div" @if($row['type'] == 'model') style="display: none;" @endif >
		<label for="Video Limit" class=" control-label col-md-4 text-left"> Event Limit</label>
		<div class="col-md-7">
		@if($row['event_limit'] == 'unlimited' || empty($row['event_limit']) )
		{!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited ',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		{!! Form::hidden('event_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		@else
		{!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		<!-- set hidden field to store the value -->
		{!! Form::text('event_limit', $row['event_limit'],array('class'=>'form-control','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 		
		@endif
		</div> 
		</div> 						


	</fieldset>
</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('membership?return='.$return) }}' " class="btn btn-warning btn-sm "> {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("membership/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});	

		// onchange to show the selected image values
		$(document).on('change','select[name="image_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="image_limit"]').removeAttr('type');
				$('input[name="image_limit"]').attr('type','hidden').val('unlimited');
			}else{
				// remove hidden attribute & add value for hidden field
	
				$('input[name="image_limit"]').removeAttr('type');
				$('<h6><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="image_limit"]');
				$('input[name="image_limit"]').attr('type','text').val('');	
			}
		});

		// onchange to show the selected videos values
		$(document).on('change','select[name="video_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="video_limit"]').removeAttr('type');
				$('input[name="video_limit"]').attr('type','hidden').val('unlimited');
			}else{
				// remove hidden attribute & add value for hidden field
				$('<h6><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="video_limit"]');
				$('input[name="video_limit"]').removeAttr('type');
				$('input[name="video_limit"]').attr('type','text').val('');				
			}
		});

		// onchange to show the selected event values
		$(document).on('change','select[name="event_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="event_limit"]').removeAttr('type');
				$('input[name="event_limit"]').attr('type','hidden').val('unlimited');
			}else{
				// remove hidden attribute & add value for hidden field
				$('<h6><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="event_limit"]');
				$('input[name="event_limit"]').removeAttr('type');
				$('input[name="event_limit"]').attr('type','text').val('');				
			}
		});	
		console.log($('input[type="radio"]'))
		// check the user type is agent or model
		$('input[name="agree"]').on('ifChecked', function(event){
			// get the selected value
			var curVal = $.trim($(this).val());			
			
			if(curVal == 'model'){
				$('.event_limit_div').hide();
			}else{
				$('.event_limit_div').show();
			}
		});
		
	});
	</script>		 
@stop