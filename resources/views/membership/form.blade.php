@extends('layouts.app')

@section('content')
<style type="text/css">
/*label.error,.asterix{
color: #cc0000;
font-family: 'Open Sans',Arial,Helvetica,sans-serif;
font-weight: normal;
font-size: 13px;
}
input.error{
border: 1px solid #cc0000;
}
legend {
	color:white;	
}*/
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> 
		<div class="sbox-tools pull-left" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-xs btn-default"  title="{{ Lang::get('core.btn_back') }}" ><i class="fa  fa-arrow-left"></i></a> 
		</div>
		<h1>Membership plan</h1> 
		<div class="sbox-tools " >
			<!-- @if(Session::get('gid') ==1)
				<a href="{{ URL::to('sximo/module/config/'.$pageModule) }}" class="tips btn btn-xs btn-default" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a>
			@endif 	 -->		
		</div> 

	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				sgsd<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'membership/save?return='.$return,'id'=>'membership','class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
	<fieldset>
		{!! Form::hidden('plan_id', $row['plan_id']) !!}
<input type='hidden' name='created_at' value ='{{$row["created_at"]}}' >
		<div class="form-group  " >
		<div class="col-md-7">
		<label class="agree">Agent
		<input type='radio' name='agree' value ='agent'  @if($row['type'] == 'agent') checked="checked" @else checked @endif >  
		<span class="checkmark"></span>
		</label>  
		<label class="agree">Model
		<input type='radio' name='agree' value ='model' @if($row['type'] == 'model') checked="checked" @endif >  
		<span class="checkmark"></span>
		</label>
		</div>
		</div>

		<div class="form-group  " >
		<label for="Plan Name" class=" control-label col-md-4 text-left"> Plan Name <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		{!! Form::text('plan_name', $row['plan_name'],array('class'=>'form-control', 'placeholder'=>''  )) !!}
		</div> 
		</div> 	

		<div class="form-group  " >
		<label for="Plan Statement" class=" control-label col-md-4 text-left"> Plan Statement <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<textarea name='plan_statement' rows='2' id='plan_statement' class='form-control '
		  >{{ $row['plan_statement'] }}</textarea> 
		</div> 
		</div> 
<!-- <div class="form-group  " >
		<label for="Plan Image" class=" control-label col-md-4 text-left"> Plan Image <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<div class="agent_prof_img">
		{!! SiteHelpers::showUploadedFile($row['plan_image'],'/uploads/membership/') !!}
		</div>
		<input  type='file' name='plan_image' id='plan_image' @if($row['plan_image'] =='') class=' dashboard_browse_btn' @endif  />				
		</div> 
		</div>  -->
		<div class="form-group  " >
		<label for="Monthly Plan Amount" class=" control-label col-md-4 text-left">Monthly Plan Amount <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('plan_amount', $row['plan_amount'],array('id'=>'plan_amt','class'=>'form-control', 'placeholder'=>''  )) !!} 
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Status" class=" control-label col-md-4 text-left"> Monthly Plan Status <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		<input type='radio' name='status' value ='1'  @if($row['status'] != '0') checked="checked" @endif > Active </label>
		<label class='radio radio-inline'>
		<input type='radio' name='status' value ='0'  @if($row['status'] == '0') checked="checked" @endif > Inactive </label> 
		</div> 
		</div>	

		<div class="form-group  " >
		<label for="Plan Amount Y" class=" control-label col-md-4 text-left"> Yearly Plan Amount <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::text('plan_amount_y', $row['plan_amount_y'],array('id'=>'plan_amt_y','class'=>'form-control', 'placeholder'=>'' )) !!} 
		</div> 
		</div>

		<div class="form-group  " >
		<label for="Status Y" class=" control-label col-md-4 text-left"> Yearly Plan Status <span class="asterix"> * </span> </label>
		<div class="col-md-7">
		<label class='radio radio-inline'>
		<input type='radio' name='status_y' value ='1'  @if($row['status_y'] != '0') checked="checked" @endif > Active </label>
		<label class='radio radio-inline'>
		<input type='radio' name='status_y' value ='0'  @if($row['status_y'] == '0') checked="checked" @endif > Inactive </label>
		</div> 
		</div> 

		<div class="form-group  " >
		<label for="Image Limit" class=" control-label col-md-4 text-left"> Album Limit <span class="asterix"> * </span></label>
		<div class="col-md-7">
		
		@if($row['image_limit'] == 'unlimited' || empty($row['image_limit']))
		<select name='image_limit_select' class = 'form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" >Limited</option>
		</select>
		<!-- {!! Form::select('image_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited',array('class'=>'form-control', 'placeholder'=>'')) !!}  -->
		<!-- set hidden field to store the value -->
		{!! Form::hidden('image_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',   )) !!} 
		@else
		<select name='image_limit_select' class = 'margin_top_space form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" selected>Custom</option>
		</select>
		<!-- {!! Form::select('image_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control  margin_top_space', 'placeholder'=>'', )) !!}  -->
		<!-- set hidden field to store the value -->
		
		<!-- <span>Note:User can upload only limited images</span> -->

		{!! Form::text('image_limit', $row['image_limit'],array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',   )) !!} 		
		
		@endif

		</div> 
		</div>

		<div class="form-group " >
		<label for="Video Limit" class=" control-label col-md-4 text-left"> Video Limit <span class="asterix"> * </span></label>
		<div class="col-md-7">
		@if($row['video_limit'] == 'unlimited' || empty($row['video_limit']))
		<select name='video_limit_select' class = 'form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" >Limited</option>
		</select>
		<!-- {!! Form::select('video_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited',array('class'=>'form-control', 'placeholder'=>'',  )) !!}  -->
		<!-- set hidden field to store the value -->
		{!! Form::hidden('video_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',   )) !!} 
		@else
		<select name='video_limit_select' class = 'form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" selected>Custom</option>
		</select>
		<!-- {!! Form::select('video_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control', 'placeholder'=>'',   )) !!}  -->
		<!-- set hidden field to store the value -->
		{!! Form::text('video_limit', $row['video_limit'],array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',  )) !!} 		
		@endif
		</div> 
		</div> 	

		<!-- Video duration field -->
		<div class="form-group  " >
		<label for="Video duration" class=" control-label col-md-4 text-left"> Video Duration Limit <span class="asterix"> * </span></label>
		<div class="col-md-7">
		{!! Form::number('video_duration_limit', $row['video_duration_limit'],array('id'=>'vid_dur','class'=>'form-control', 'placeholder'=>'', 'min'=>'1', 'max' => '60')) !!} max-limit 60 min
		</div> 
		</div>	
		<!-- end -->
		<div class="form-group event_limit_div" @if($row['type'] == 'model') @endif >
		<input type="hidden" id="user_type" value="{{ $row['type'] }}">
		<label for="Video Limit" class=" control-label col-md-4 text-left"> Event Limit <span class="asterix"> * </span></label>
		<div class="col-md-7">
		<!-- @if($row['event_limit'] == 'unlimited' || empty($row['event_limit']) )
		{!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited ',array('id'=>'select1','class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		set hidden field to store the value
		{!! Form::hidden('event_limit', 'unlimited',array('id'=>'select2','class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		@else
		{!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('id'=>'select1','class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
		 set hidden field to store the value
		{!! Form::text('event_limit', $row['event_limit'],array('id'=>'select2','class'=>'form-control','type' => 'hidden', 'placeholder'=>'', 'required'=>'true'  )) !!} 		
		@endif -->
		@if($row['event_limit'] == 'unlimited' || empty($row['event_limit']))
		<select name='event_limit_select' class = 'form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" >Limited</option>
		</select>
		<!-- {!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','unlimited' => 'Unlimited','custom' => 'Limited' ), 'unlimited',array('class'=>'form-control', 'placeholder'=>'',  )) !!}  -->
		<!-- set hidden field to store the value -->
		{!! Form::hidden('event_limit', 'unlimited',array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',   )) !!} 
		@else
		<select name='event_limit_select' class = 'form-control  '>
		<option value="" disabled>Select option</option>
		<option value="unlimited" >Unlimited</option>
		<option value="custom" selected>Custom</option>
		</select>
		<!-- {!! Form::select('event_limit_select', array('unlimited' => 'Unlimited','custom' => 'Custom'), 'custom',array('class'=>'form-control', 'placeholder'=>'',  )) !!}  -->
		<!-- set hidden field to store the value -->
		{!! Form::text('event_limit', $row['event_limit'],array('class'=>'form-control  margin_top_space','type' => 'hidden', 'placeholder'=>'',  )) !!} 		
		@endif
		</div> 
		</div> 						


	</fieldset>
</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" > {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" > {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('membership?return='.$return) }}' " class="btn btn-warning btn-sm "> {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
   $("#membership").validate({

     ignore: "not:hidden",

      rules: {
      	plan_amount:{
         required:true,
         number:true
        },
        plan_amount_y:{
         required:true,
         number:true
        },
        plan_name:{
         required:true,
        },
         plan_statement:{
         required:true,
        }, image_limit_select:{
         required:true,
        }, image_limit:{
         required:true,
        }, video_limit_select:{
         required:true,
        },
        video_limit:{
         required:true,
        },
         video_duration_limit:{
         required:true,
         number:true
        },
        event_limit_select:{
         required:true,
        },
		event_limit:{
         required:true,
        },
},
	messages: {
	   
	        },
        });

	$(document).ready(function() { 
		var user_tyep = $('#user_type').val();
		// alert(user_tyep);
		if(user_tyep=='model')
		{
			$("#select1").removeAttr('required');
			$("#select2").removeAttr('required');
			$('.event_limit_div').hide();
		}
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("membership/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});	

		// onchange to show the selected image values
		$(document).on('change','select[name="image_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="image_limit"]').removeAttr('type');
				$('.i1').hide();
				$('input[name="image_limit"]').attr('type','hidden').val('unlimited');

			}else{
				// remove hidden attribute & add value for hidden field
	
				$('input[name="image_limit"]').removeAttr('type');
				$('<h6 class="i1"><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="image_limit"]');
				$('input[name="image_limit"]').attr('type','text').val('');	
			}
		});

		// onchange to show the selected videos values
		$(document).on('change','select[name="video_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="video_limit"]').removeAttr('type');
				$('.i2').hide();
				$('input[name="video_limit"]').attr('type','hidden').val('unlimited');
			}else{
				// remove hidden attribute & add value for hidden field
				$('<h6 class="i2"><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="video_limit"]');
				$('input[name="video_limit"]').removeAttr('type');
				$('input[name="video_limit"]').attr('type','text').val('');				
			}
		});

		// onchange to show the selected event values
		$(document).on('change','select[name="event_limit_select"]',function(){
			// get the selected value
			var curVal = $.trim($(this).val());
			if(curVal == 'unlimited'){
				// remove text attribute & add value for text field
				$('input[name="event_limit"]').removeAttr('type');
				$('.i3').hide();
				$('input[name="event_limit"]').attr('type','hidden').val('unlimited');
			}else{
				// remove hidden attribute & add value for hidden field
				$('<h6 class="i3"><span>*Note:</span>Limit is based on per month calculation. it will be multiplied by 12 for Yearly Plan</h6>').insertBefore('input[name="event_limit"]');
				$('input[name="event_limit"]').removeAttr('type');
				$('input[name="event_limit"]').attr('type','text').val('');				
			}
		});	
		console.log($('input[type="radio"]'))
		// check the user type is agent or model
		$('input[name="agree"]').on('ifChecked', function(event){
			// get the selected value
			var curVal = $.trim($(this).val());			
			
			if(curVal == 'model'){
				$("#select1").removeAttr('required');
				$("#select2").removeAttr('required');
				$('.event_limit_div').hide();
				
			}else{
				$("#select1").attr('required',true);
				$("#select2").attr('required',true);
				$('.event_limit_div').show();
				
			}
		});

		$('input[name="image_limit"]').keypress(function(e){ 
			var charCode = (e.which) ? e.which : e.keyCode;
			if(charCode > 31 && (charCode < 48 || charCode > 57)){
				swal("Please enter valid limit");
				return false;
			}

		});

		$('input[name="video_limit"]').keypress(function(e){ 
		   // if (this.value.length == 0 && e.which == 48 ){
		   // 	  swal("Please enter valid amount");
		   //    return false;

		   // }
		   var charCode = (e.which) ? e.which : e.keyCode;
		   if(charCode > 31 && (charCode < 48 || charCode > 57)){
		   	swal("Please enter valid limit");
		   	return false;
		   }
		});
		$('input[name="event_limit"]').keypress(function(e){ 
		   // if (this.value.length == 0 && e.which == 48 ){
		   // 	  swal("Please enter valid amount");
		   //    return false;

		   // }
		   var charCode = (e.which) ? e.which : e.keyCode;
		   if(charCode > 31 && (charCode < 48 || charCode > 57)){
		   	swal("Please enter valid limit");
		   	return false;
		   }
		});
		
	});
	</script>		 
@stop