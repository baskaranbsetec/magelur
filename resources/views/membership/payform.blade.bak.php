
<div class="ourmodel_banner membt">
  <div class="ourmod_cnt">
    <h1>Billing Information</h1>
    <div class="two_laid"><a href="index.html">Home</a> <p> membership Plan</p></div>
  </div>
</div>

<div class="adevrtise_with memvr_adver">
 <div class="container">
  <div class="three_plans clearfix">

  {!! Form::open(array('url' => url('membership/stripe'),'id'=>'payment-stripe' )) !!}

        <input type="hidden" name="membership_type" value="{{$membership_type}}">
        <input type="hidden" name="plan_membership" value="{{$plan_membership}}">
        <input name='stripeToken' id="stripeToken" type="hidden" value=""/>

     {!! Form::text('billingAddress1', '' , array('class'=>'form-control','id'=>'billingAddress1','required'=>true,'placeholder'=>'Address','autocomplete'=>'off')) !!}

     {!! Form::text('billingCity', '' , array('class'=>'form-control','id'=>'billingCity','required'=>true,'placeholder'=>'City','autocomplete'=>'off')) !!}

     {!! Form::text('billingState', '' , array('class'=>'form-control req','id'=>'billingState','placeholder'=>'State','autocomplete'=>'off')) !!}


     <select class="form-control required" name="billingCountry" id="billingCountry" data-country="US">
         <option value="" selected="selected">Country</option>
         <option value="US">USA</option>
     </select>

     {!! Form::text('billingPostcode', '' , array('class'=>'form-control','id'=>'billingPostcode','required'=>true,'placeholder'=>'Post code','autocomplete'=>'off')) !!}


     <div class="clearfix m-b-sm">
    <div class="col-sm-12">
     <h4>Card Info</h4>
    </div>
    </div>

    {!! Form::text('nameOnCard', '' , array('class'=>'form-control','id'=>'nameOnCard','required'=>true,'placeholder'=>'Name on card','autocomplete'=>'off')) !!}

    {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>'Card number','autocomplete'=>'off')) !!}


    <select class="form-control required" name="card-expiry-month" id="card-expiry-month">
    <option value="" selected="selected">Month</option>
    </select>
   
    <script type="text/javascript">
    var select = $("#card-expiry-month"),
    month = new Date().getMonth() + 1;
    for (var i = 1; i <= 12; i++) {
    select.append($("<option value='"+i+"'>"+i+"</option>"))
    }
    </script>


    <select class="form-control required" name="card-expiry-year" id="card-expiry-year">
    <option value="" selected="selected">Year</option>
    </select>
    
    <script type="text/javascript">
    var select = $("#card-expiry-year"),
    year = new Date().getFullYear();
    for (var i = 0; i < 30; i++) {
    select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
    }
    </script>


    {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'4','id'=>'card-cvc','required'=>true,'placeholder'=>'CVC','autocomplete'=>'off')) !!}

    <input type="submit" id="submit_button" value="Pay now" class="btn btn-mixed animated fadeInUp">

    {!! Form::close() !!}
   
  </div>  
 </div>  
</div>  

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<script type="text/javascript">

// payment options script start
Stripe.setPublishableKey("{{ $publishable_key }}");
var payment_error = $('.payment-errors');
$(document).ready(function() {
    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
            },
        },
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


</script>