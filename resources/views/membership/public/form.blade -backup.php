

		 {!! Form::open(array('url'=>'membership/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Membership plan</legend>
									
									  <div class="form-group  " >
										<label for="Plan Id" class=" control-label col-md-4 text-left"> Plan Id </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_id' id='plan_id' value='{{ $row['plan_id'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Name" class=" control-label col-md-4 text-left"> Plan Name </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_name' id='plan_name' value='{{ $row['plan_name'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Statement" class=" control-label col-md-4 text-left"> Plan Statement </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_statement' id='plan_statement' value='{{ $row['plan_statement'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Amount" class=" control-label col-md-4 text-left"> Plan Amount </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_amount' id='plan_amount' value='{{ $row['plan_amount'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Amount Y" class=" control-label col-md-4 text-left"> Plan Amount Y </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_amount_y' id='plan_amount_y' value='{{ $row['plan_amount_y'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Interval" class=" control-label col-md-4 text-left"> Plan Interval </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_interval' id='plan_interval' value='{{ $row['plan_interval'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Plan Periods" class=" control-label col-md-4 text-left"> Plan Periods </label>
										<div class="col-md-7">
										  <input  type='text' name='plan_periods' id='plan_periods' value='{{ $row['plan_periods'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-7">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status Y" class=" control-label col-md-4 text-left"> Status Y </label>
										<div class="col-md-7">
										  <input  type='text' name='status_y' id='status_y' value='{{ $row['status_y'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Level" class=" control-label col-md-4 text-left"> Level </label>
										<div class="col-md-7">
										  <input  type='text' name='level' id='level' value='{{ $row['level'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Stripe Plan" class=" control-label col-md-4 text-left"> Stripe Plan </label>
										<div class="col-md-7">
										  <input  type='text' name='stripe_plan' id='stripe_plan' value='{{ $row['stripe_plan'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Stripe Plan Y" class=" control-label col-md-4 text-left"> Stripe Plan Y </label>
										<div class="col-md-7">
										  <input  type='text' name='stripe_plan_y' id='stripe_plan_y' value='{{ $row['stripe_plan_y'] }}' 
						     class='form-control ' /> 
										 </div> 
										 <div class="col-md-1">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
