
<div class="ourmodel_banner membt">
  <div class="ourmod_cnt">
    <h1>membership Plan</h1>
    <div class="two_laid"><a href="{{ url('home') }}">Home</a> <p> membership Plan</p></div>
  </div>
</div>

<div class="adevrtise_with memvr_adver membership_plan_blk">
 <div class="container">
   <div class="member_tab_structure">
     @if(!empty($loggedId))  
     <ul class="nav nav-tabs search_tabs">
      <li class="basic_search_li active"><a data-toggle="tab" href="#monthly_search">Monthly</a></li>
      <li class="advanced_search_li"><a data-toggle="tab" href="#yearly_search">Yearly</a></li>
     </ul>
     @else
     <ul class="nav nav-tabs search_tabs">
      <li class="basic_search_li active"><a data-toggle="tab" href="#model_search">Model</a></li>
      <li class="advanced_search_li"><a data-toggle="tab" href="#agent_search">Agent</a></li>
     </ul>
     @endif

    <div class="col-md-offset-0 col-md-12 col-sm-12">
      <div class="terms_contant">
        <div class="terms_details">
          <h3><span></span>Our membership Plan</h3>
          <p>We are seeing an increasing number of people wanting to break away from more traditional models and rather than just call this a hybrid model as we have previously, we decided that what was actually needed was a new approach to membership models.</p>

        </div>
      </div>
    </div>
    <!-- show membership for logged users -->
    @if(!empty($loggedId))
    <div class="tab-content">
      <div id="monthly_search" class="tab-pane fade in search_user_div active">
        <!-- Monthly Plan Details -->
        <div class="three_plans clearfix">
         @foreach($memberships as $membership)
          @if($membership->status == 1)
         {!! Form::open(array('url' => url('membership/buy-membership-plan'),'id'=>'payment-stripe' )) !!}
         <input type="hidden" name="plan_membership" id="plan_membership" value="">
         <input type="hidden" name="membership_type" id="membership_type" value="">

         @if($membership->plan_id == $plan_id  && $current_purchase_period == 'month')
         <div class="first_plan highlight plan-id-{{$membership->plan_id}}">
         @elseif($membership->plan_name=='Basic' && $pause_subscription != 'paused' && $membership->plan_id == $plan_id)
          <div class="first_plan highlight plan-id-{{$membership->plan_id}}">
         @else
         <div class="first_plan plan-id-{{$membership->plan_id}}">
         @endif
          <div class="start_bg">
            <h1>{{$membership->plan_name}}</h1>
            <div class="sbase"> 
              <div class="traingle">
               <div class="mini_traingle"></div>  
             </div>
            
             <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
               <p>MONTHLY</p>
             </div>
             
           </div>
           <div class="content-withs">  
            <ul>
              <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
              @if($membership->type == 'agent')
              <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
              @elseif($membership->type == 'model') 
              <li class="model-desc">Contact Agents & Models </li>
              @endif 
              <li class="photo-limit">Can upload {{ $membership->image_limit }} Albums</li>
            </ul>
          </div> 
          
            @if($membership->plan_id != $plan_id )
                @if($current_amount > $membership->plan_amount && $current_purchase_period == 'month' || $current_purchase_period == 'year')
                    <div class="fre_try"><a href="#" class="downgrade_plan" id="downgrade_plan_{{$membership->plan_id}}" onclick="return false;">BUY NOW</a></div>
                @else
                    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}' data-type='m'>BUY NOW</a></div> 
                @endif
            @else

               @if($membership->plan_name=='Basic' && $pause_subscription == 'paused' || $current_purchase_period == 'year')
                <div class="fre_try"><a href="#" class="downgrade_plan" id="downgrade_plan_{{$membership->plan_id}}" onclick="return false;">BUY NOW</a></div>
                @elseif($current_purchase_period == 'month' || $membership->plan_name=='Basic')
                  <div class="fre_try"><a href="#" onclick="return false;">ACTIVE PLAN</a></div>
                @else
                    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}' data-type='m'>BUY NOW</a></div> 
                @endif
            @endif
          
        </div>
      </div> 
      @endif
      {!! Form::close() !!}
      @endforeach
    </div> 
  </div>

  <div id="yearly_search" class="tab-pane fade advanced_user_div">
    <!-- Yearly Plan Details -->
    <div class="three_plans clearfix">
     @foreach($memberships as $membership)
      @if($membership->status_y == 1)
     {!! Form::open(array('url' => url('membership/buy-membership-plan'),'id'=>'payment-stripe' )) !!}
     <input type="hidden" name="plan_membership" id="plan_membership" value="">
     <input type="hidden" name="membership_type" id="membership_type"  value="">
      @if($membership->plan_id == $plan_id  && $current_purchase_period == 'year')
          <div class="first_plan highlight plan-id-{{$membership->plan_id}}">
       @else
          <div class="first_plan plan-id-{{$membership->plan_id}}">
       @endif
      <div class="start_bg">
        <h1>{{$membership->plan_name}}</h1>
        <div class="sbase"> 
          <div class="traingle">
           <div class="mini_traingle"></div>  
         </div>
        
         <div class="pre_price"><h1>${{ ($membership->plan_amount_y)}}</h1>
           <p>YEARLY</p>
         </div>
         
       </div>
       <div class="content-withs">  
        <ul>
          <li>Use @if($membership->video_limit == 'unlimited') {{ ($membership->video_limit) }} @else {{ ($membership->video_limit * 12) }} @endif Videos</li>
          @if($membership->type == 'agent')
          <li>Advertise @if($membership->event_limit == 'unlimited') {{ ($membership->event_limit) }} @else {{ ($membership->event_limit * 12) }} @endif events </li>
          @elseif($membership->type == 'model') 
          <li>Contact Agents & Models </li>
          @endif 
          <li>Can upload @if($membership->image_limit == 'unlimited') {{ ($membership->image_limit) }} @else {{ ($membership->image_limit * 12) }} @endif Albums</li>
        </ul>
      </div> 
          @if($membership->plan_id != $plan_id )
                @if($current_amount > $membership->plan_amount_y  && $current_purchase_period == 'year')
                    <div class="fre_try"><a href="#" class="downgrade_plan" id="downgrade_plan_{{$membership->plan_id}}" onclick="return false;">BUY NOW</a></div>
                @else
                    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}' data-type='y'>BUY NOW</a></div> 
                @endif
            @else
                @if($current_purchase_period == 'year')
                  <div class="fre_try"><a href="#" onclick="return false;">ACTIVE PLAN</a></div>
                @else
                    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}' data-type='y'>BUY NOW</a></div> 
                @endif
            @endif
    </div>
  </div> 
@endif
  {!! Form::close() !!}
  @endforeach

</div> 
</div>
</div>

@endif


@if(empty($loggedId))

<div class="tab-content">
  <div id="model_search" class="tab-pane fade in search_user_div active">
    <!-- Monthly Plan Details -->
    <div class="three_plans clearfix">
     @foreach($memberships as $membership)
     <!-- list only membership model -->
      @if($membership->status == 1)
     @if($membership->type == 'model')
     {!! Form::open(array('url' => url('model/create'),'id'=>'payment-stripe' )) !!}
     <input type="hidden" name="plan_membership" id="plan_membership" value="">
     <input type="hidden" name="membership_type" id="membership_type"  value="">
     <div class="first_plan plan-id-{{$membership->plan_id}}">
      <div class="start_bg">
        <h1>{{$membership->plan_name}}</h1>
        <div class="sbase"> 
          <div class="traingle">
           <div class="mini_traingle"></div>  
         </div>
        
         <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
           <p>MONTHLY</p>
         </div>
        
         <div class="triangle_radio">
         <label class='agree radio radio-inline' onclick="showPlanDetails('monthly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
           <input type='radio' name='agree' 
           data-event = "{{ $membership->event_limit }}" 
           data-photo = "{{ $membership->image_limit }}" 
           data-video = "{{ $membership->video_limit }}" 
           data-price = "{{ $membership->plan_amount }}" 
           value ='agent' />  Monthly
           <span class="checkmark"></span>
         </label>
         <label class='agree radio radio-inline' onclick="showPlanDetails('yearly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','')">
           <input type='radio' name='agree' 
           data-price = "{{ $membership->plan_amount }}" 
           data-photo = "{{ $membership->image_limit }}" 
           data-video = "{{ $membership->video_limit }}" 
           value ='model' />  Yearly
           <span class="checkmark"></span>
         </label>
         </div>
       </div>
       <div class="content-withs">  
        <ul>
          <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
          @if($membership->type == 'agent')
          <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
          @elseif($membership->type == 'model') 
          <li class="model-desc">Contact Agents & Models </li>
          @endif 
          <li class="photo-limit">Can upload {{ $membership->image_limit }} Albums</li>
        </ul>
      </div> 

      <div class="fre_try">
        <a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name_Y !!}" data-value='{{$membership->plan_id}}' data-type='m'>Login</a>
      </div> 

    </div>
  </div> 
   @endif
  {!! Form::close() !!}
  @endif  
  @endforeach
</div> 
</div>

<div id="agent_search" class="tab-pane fade advanced_user_div">
  <!-- Yearly Plan Details -->
  <div class="three_plans clearfix">
   @foreach($memberships as $membership)
   <!-- list only membership model -->

   @if($membership->type == 'agent')
    @if($membership->status == 1)
   {!! Form::open(array('url' => url('model/create'),'id'=>'payment-stripe' )) !!}
   <input type="hidden" name="plan_membership" id="plan_membership" value="">
   <input type="hidden" name="membership_type" id="membership_type" value="">
   <div class="first_plan plan-id-{{$membership->plan_id}}">
    <div class="start_bg">
      <h1>{{$membership->plan_name}}</h1>
      <div class="sbase"> 
        <div class="traingle">
         <div class="mini_traingle"></div>  
       </div>
      
       <div class="pre_price"><h1>${{$membership->plan_amount}}</h1>
         <p>MONTHLY</p>
       </div>
       
       
       <div class="triangle_radio">
       <label class='agree radio radio-inline' onclick="showPlanDetails('monthly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
         <input type='radio' name='agree' 
         data-event = "{{ $membership->event_limit }}" 
         data-photo = "{{ $membership->image_limit }}" 
         data-video = "{{ $membership->video_limit }}" 
         data-price = "{{ $membership->plan_amount }}" 
         value ='agent' />  YEARLY
         <span class="checkmark"></span>
       </label>
       <label class='agree radio radio-inline' onclick="showPlanDetails('yearly','{{$membership->plan_id}}','{{ $membership->plan_amount }}','{{ $membership->image_limit }}','{{ $membership->video_limit }}','{{ $membership->event_limit }}')">
         <input type='radio' name='agree' 
         data-price = "{{ $membership->plan_amount }}" 
         data-photo = "{{ $membership->image_limit }}" 
         data-video = "{{ $membership->video_limit }}" 
         value ='model' />  Yearly
         <span class="checkmark"></span>
       </label>
       </div>
     </div>
     <div class="content-withs">  
      <ul>
          <li class="video-limit">Use {{ $membership->video_limit }} Videos</li>
          @if($membership->type == 'agent')
          <li class="event-limit">Advertise {{ $membership->event_limit }} events </li>
          @elseif($membership->type == 'model') 
          <li class="model-desc">Contact Agents & Models </li>
          @endif 
          <li class="photo-limit">Can upload {{ $membership->image_limit }} Albums</li>
      </ul>
    </div> 
    <div class="fre_try"><a href="javascript:void(0)" class='add' data-plan = "{!! $membership->plan_name !!}" data-value='{{$membership->plan_id}}' data-type='y'>Login</a></div> 

  </div>
</div> 
@endif
{!! Form::close() !!}
@endif
@endforeach

</div> 
</div>
</div>

@endif


</div>


</div>  

</div>  
<link href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet"> 
<!-- Modal -->
<div id="alertDowngrade" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h1>Downgrade is not possible as per our site`s policy</h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-color btn-color-warning m-b-sm confirmcancel" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<style type="text/css">
.map{display: none;}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

  function showPlanDetails(type,id,price,photo,video,event){
    // get the selected plan div
    var parentEle = $('.plan-id-'+id);

    var activeTab = $('.basic_search_li').hasClass('active');

    if(activeTab){
      activeTab = "Model";
    }
    else{
      activeTab = "Agent";
    }

    if(type == 'yearly'){
      console.log(price)
       price = (price*12);
       video = (video*12);
       photo = (photo*12);
       
       if(event.match(/^-{0,1}\d+$/))
       event = (event*12);
       console.log(price)
    }
    console.log(parentEle.find('.pre_price h1'))
    // set price
    parentEle.find('.pre_price h1').html('$'+price);

    // set video count
    parentEle.find('.video-limit').html('Use '+ video +' Videos');

    // set photo count
    parentEle.find('.photo-limit').html('Can upload '+photo+' Albums');

    if(activeTab == 'Model'){
     // show model description
     parentEle.find('.model-desc').show();    
     // hide event count
     parentEle.find('.event-limit').html('Advertise '+event+' events').hide();
    }else{
     // hide model description
     parentEle.find('.model-desc').hide();    
     // hide event count
     parentEle.find('.event-limit').html('Advertise '+event+' events').show();
    }
    // alert(type);
    return false;
  }


 $(document).ready(function() {

  $('.add').click(function () {
   var vl = $(this).attr('data-value');
   var pl = $(this).attr('data-plan');
    var tl = $(this).attr('data-type');

   $('#plan_membership').val(vl);
   $('#membership_type').val(tl);
   $('#payment-stripe').submit();

 });

 //  $('.downgrade_plan').mouseover(function () {
 //    var idName =  $(this).attr('id');
 //    $('#'+idName).text('Can not downgrade to this plan');
 // }).mouseout(function () {
 //    var idName =  $(this).attr('id');
 //    $('#'+idName).text('BUY NOW');
 // });

  $('.downgrade_plan').click(function () {
     var idName =  $(this).attr('id');
     if(idName == 'downgrade_plan_1' || idName == 'downgrade_plan_5'){
      swal({
      title: "",
      text: "Downgrade is not possible as per our site`s policy!",
      icon: "warning",
      dangerMode: true
    });
     }
     else{
    swal({
      title: "",
      text: "Downgrade is not possible as per our site`s policy!",
      icon: "warning",
      dangerMode: true
    });
  }
 });

});
</script>
